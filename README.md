# FRONTEND-API #

[![Build Status](http://bcn-jenkins-01.odigeo.org/jenkins/buildStatus/icon?job=frontend-api)](http://bcn-jenkins-01.odigeo.org/jenkins/job/frontend-api/)

**Frontend-api implements backend layer to be used by frontend modules to access to the different apis of the company.**

### Technological stack ###
This module is built with the following main technologies:

* [Graphql-java](https://www.graphql-java.com/)
* [Rest-easy](https://resteasy.github.io/)
* [Google-guice](https://github.com/google/guice)
* [Jboss Enterprise Application Platform 6.4.19](https://developers.redhat.com/products/eap/overview)
* [Java JDK 1.8.0_241](https://www.oracle.com/technetwork/java/javase/overview/index.html)

For testing purpouses we will use the following main technologies:

* [TestNG](https://github.com/cbeust/testng)
* [Mockito](https://github.com/mockito/mockito)

### How to deploy the project in docker ###

* Deploy the project in docker:
  * build docker image: 
    * normal way: ``mvn clean install -P build-container-image``
    * fast way: ``mvn clean install --offline -DskipTests -P build-container-image``
  * run docker image: ``docker-compose up``
  * stop docker image: ``docker-compose down``

* You will be able to load the following resources:
  * base endpoints:
    * frontend-api: http://localhost:8080/frontend-api
    * frontend-api debug: http://localhost:8787/frontend-api
      * services
        * graphql service: frontend-api/service/graphql
        * graphql service debug system activated: frontend-api/engineering/service/graphql
        * metrics service: frontend-api/service/metrics
      * engineering
        * build info: frontend-api/engineering/build
        * metrics info: frontend-api/engineering/metrics
        * healthcheck info: frontend-api/engineering/healthcheck
        * escalation matrix: frontend-api/engineering/escalation-matrix
        * visit deserializer: frontend-api/engineering/visit/deserialize

### How to deploy the project to development ###

The following tools are recommended to dev into frontend-api:

* [Intellij Idea](https://www.jetbrains.com/idea)
* [Insomnia](https://insomnia.rest/)

### How to promote a new version to canary and stable ###

Currently the project is deployed in Cloud servers, so we can use Jenkins to make new releases.
Just follow these steps:

* ``git tag 1.x.0``
* ``git push origin --tags``
* go to [Jenkins CI](http://bcn-jenkins-01.odigeo.org/jenkins/job/frontend-api/)
* wait to "promote to stable" step and then push the "proceed" button

And the end of the process you will see that the project is deployed at production and qa environment and also the poms
version has been incremented by one 

### Project's environment ###
* Production: http://lb.frontend-api.gke-apps.edo.sv/frontend-api
* QA: http://lb.frontend-api.gke01-1.edo.qa/frontend-api

### Who do I talk to? ###

* Owner: [FronTech POD](https://jira.odigeo.com/wiki/display/KB/OF2.0)

### Do I need more info? ###

* Take a look to our [Confluence page](https://jira.odigeo.com/wiki/display/ARCH/OF2.0)
