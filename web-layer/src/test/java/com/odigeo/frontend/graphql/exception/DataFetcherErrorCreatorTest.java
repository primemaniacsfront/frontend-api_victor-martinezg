package com.odigeo.frontend.graphql.exception;

import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import graphql.execution.DataFetcherExceptionHandlerParameters;
import graphql.execution.ResultPath;
import graphql.language.OperationDefinition;
import graphql.schema.DataFetchingEnvironment;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ClassUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class DataFetcherErrorCreatorTest {

    private static final String EXTENSIONS_EXCEPTION_LABEL = "exception";

    private static final ErrorCodes DEFAULT_ERROR = ErrorCodes.INTERNAL_GENERIC;
    private static final String EXTENSIONS_ERROR_CODE = "code";
    private static final String EXTENSIONS_ERROR_DESC = "description";
    private static final String EXTENSIONS_ERROR_SERVICE = "service";

    @Mock
    private DataFetcherExceptionHandlerParameters params;
    @Mock
    private DataFetchingEnvironment environment;
    @Mock
    private OperationDefinition operationDefinition;
    @Mock
    private ResolverContext context;
    @Mock
    private DebugInfo debugInfo;

    private DataFetcherErrorCreator creator;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        creator = new DataFetcherErrorCreator();

        when(params.getDataFetchingEnvironment()).thenReturn(environment);
        when(environment.getContext()).thenReturn(context);
        when(context.getDebugInfo()).thenReturn(debugInfo);
    }

    @Test
    public void testCreatePath() {
        ResultPath resultPath = mock(ResultPath.class);
        List<Object> path = new ArrayList<>(0);

        when(params.getPath()).thenReturn(resultPath);
        when(resultPath.toList()).thenReturn(path);

        assertSame(creator.createPath(params), path);
    }

    @Test
    public void testCreatePathEmpty() {
        assertTrue(creator.createPath(params).isEmpty());
    }

    @Test
    public void testCreateExtensions() {
        when(params.getDataFetchingEnvironment()).thenReturn(environment);
        when(environment.getOperationDefinition()).thenReturn(operationDefinition);
        when(operationDefinition.getName()).thenReturn("query");
        Map<String, Object> extensions = creator.createExtensions(params);

        assertNull(extensions.get(EXTENSIONS_EXCEPTION_LABEL));
        assertEquals(extensions.get(EXTENSIONS_ERROR_CODE), DEFAULT_ERROR.getCode());
        assertEquals(extensions.get(EXTENSIONS_ERROR_DESC), DEFAULT_ERROR.getDescription());
    }

    @Test
    public void testCreateExtensionsDebugEnabled() {
        Throwable exception = mock(Throwable.class);
        String trace = "trace";

        when(debugInfo.isEnabled()).thenReturn(true);
        when(exception.toString()).thenReturn(trace);
        when(params.getException()).thenReturn(exception);
        when(params.getDataFetchingEnvironment()).thenReturn(environment);
        when(environment.getOperationDefinition()).thenReturn(operationDefinition);
        when(operationDefinition.getName()).thenReturn("query");

        Map<String, Object> extensions = creator.createExtensions(params);
        List<?> stackTrace = (List<?>) extensions.get(EXTENSIONS_EXCEPTION_LABEL);

        assertSame(stackTrace.get(0), trace);
    }

    @Test
    public void testCreateExtensionsErrorCode() {
        ServiceException serviceException = mock(ServiceException.class);
        ErrorCodes errorCode = mock(ErrorCodes.class);
        String code = "code";
        String desc = "desc";
        ServiceName serviceName = mock(ServiceName.class);
        String service = "search-engine";

        when(params.getException()).thenReturn(serviceException);
        when(serviceException.getErrorCode()).thenReturn(errorCode);
        when(errorCode.getCode()).thenReturn(code);
        when(errorCode.getDescription()).thenReturn(desc);
        when(serviceException.getServiceName()).thenReturn(serviceName);
        when(serviceName.getLabel()).thenReturn(service);

        Map<String, Object> extensions = creator.createExtensions(params);

        assertEquals(extensions.get(EXTENSIONS_ERROR_CODE), code);
        assertEquals(extensions.get(EXTENSIONS_ERROR_DESC), desc);
        assertEquals(extensions.get(EXTENSIONS_ERROR_SERVICE), service);
    }

    @Test
    public void testCreateMessage() {
        String message = "message";
        Throwable exception = mock(Throwable.class);
        String className = ClassUtils.getShortClassName(exception, null);
        String errorMessage = className + ": " + message;

        when(params.getException()).thenReturn(exception);
        when(exception.getMessage()).thenReturn(message);

        assertEquals(creator.createMessage(params), errorMessage);
    }

    @Test
    public void testCreateException() {
        Throwable exception = mock(Throwable.class);
        when(params.getException()).thenReturn(exception);
        assertSame(creator.createException(params), exception);
    }

}
