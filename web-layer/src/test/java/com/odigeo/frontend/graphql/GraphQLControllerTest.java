package com.odigeo.frontend.graphql;

import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResolverContextInitializer;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.logs.LogDetailsTracer;
import com.odigeo.frontend.monitoring.MetricsHandler;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.ExecutionResultImpl;
import graphql.GraphQL;
import graphql.GraphqlErrorException;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class GraphQLControllerTest {

    private static final String DEBUG_INFO_PARAMETER = "debugInfo";

    @Mock
    private GraphQLBuilder builder;
    @Mock
    private ResolverContextInitializer contextInitializer;
    @Mock
    private GraphQLResponseHandler responseHandler;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private LogDetailsTracer logsTracer;
    @Mock
    private GraphQL build;
    @Mock
    private ExecutionResult executionResult;
    @Mock
    private HttpServletRequest servletRequest;
    @Mock
    private HttpServletResponse servletResponse;
    @Mock
    private GraphQLRequest graphqlRequest;
    @Mock
    private MetricsHandler metricsHandler;

    private GraphQLController controller;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        controller = spy(GraphQLController.class);

        Guice.createInjector(binder -> {
            binder.bind(ResolverContextInitializer.class).toProvider(() -> contextInitializer);
            binder.bind(GraphQLBuilder.class).toProvider(() -> builder);
            binder.bind(GraphQLResponseHandler.class).toProvider(() -> responseHandler);
            binder.bind(JsonUtils.class).toProvider(() -> jsonUtils);
            binder.bind(LogDetailsTracer.class).toProvider(() -> logsTracer);
            binder.bind(MetricsHandler.class).toProvider(() -> metricsHandler);
        }).injectMembers(controller);
    }

    @Test
    public void testDoPost() {
        Response response = mock(Response.class);

        doReturn(response).when(controller).executePost(eq(graphqlRequest), eq(servletRequest), eq(servletResponse),
                argThat(debugInfo -> !debugInfo.isEnabled()));

        assertSame(controller.doPost(graphqlRequest, servletRequest, servletResponse), response);
    }

    @Test
    public void testDebugPost() {
        Response response = mock(Response.class);
        UriInfo uriInfo = mock(UriInfo.class);
        MultivaluedMap<String, String> queryParameters = mock(MultivaluedMap.class);
        DebugInfo debugInfo = mock(DebugInfo.class);
        String debugInfoJson = "debugInfo";

        when(uriInfo.getQueryParameters()).thenReturn(queryParameters);
        when(queryParameters.getFirst(DEBUG_INFO_PARAMETER)).thenReturn(debugInfoJson);
        when(jsonUtils.fromJson(debugInfoJson, DebugInfo.class)).thenReturn(debugInfo);

        doReturn(response).when(controller).executePost(eq(graphqlRequest), eq(servletRequest), eq(servletResponse),
                argThat(argDebugInfo -> argDebugInfo == debugInfo));

        assertSame(controller.doDebugPost(graphqlRequest, servletRequest, servletResponse, uriInfo), response);
        verify(debugInfo).setEnabled(true);
    }

    @Test
    public void testExecutePost() {
        ResolverContext context = mock(ResolverContext.class);
        Map<String, Object> variables = Collections.singletonMap("param", "value");
        Map<String, Object> responseBody = new HashMap<>(0);
        String query = "query";
        String operation = "operation";
        int responseCode = 1;

        Class<String> contextClass = String.class;
        String contextValue = "contextValue";
        Map<Class<?>, Object> contextMap = Collections.singletonMap(contextClass, contextValue);

        when(builder.getBuild()).thenReturn(build);
        when(contextInitializer.createContext(eq(servletRequest), any())).thenReturn(context);
        when(contextInitializer.createContextMap(eq(servletRequest))).thenReturn(contextMap);
        when(graphqlRequest.getQuery()).thenReturn(query);
        when(graphqlRequest.getOperationName()).thenReturn(operation);
        when(graphqlRequest.getVariables()).thenReturn(variables);
        when(build.execute(argThat((ArgumentMatcher<ExecutionInput.Builder>) argument -> {
            ExecutionInput executionInput = argument.build();
            return Objects.equals(executionInput.getQuery(), query)
                    && Objects.equals(executionInput.getOperationName(), operation)
                    && Objects.equals(executionInput.getVariables(), variables)
                    && Objects.equals(executionInput.getContext(), context)
                    && Objects.equals(executionInput.getGraphQLContext().get(contextClass), contextValue);
        }))).thenReturn(executionResult);

        when(responseHandler.buildResponseStatus(executionResult)).thenReturn(responseCode);
        when(responseHandler.buildResponseBody(executionResult, context)).thenReturn(responseBody);

        Response response = controller.doPost(graphqlRequest, servletRequest, servletResponse);

        assertEquals(response.getStatus(), responseCode);
        assertEquals(response.getEntity(), responseBody);
        verifyNoInteractions(metricsHandler);
        verify(logsTracer).initTracer(servletRequest, graphqlRequest, context);
        verify(logsTracer).endTracer();
    }

    @Test
    public void testMetricsErrors() {
        ResolverContext context = mock(ResolverContext.class);
        when(builder.getBuild()).thenReturn(build);

        String path = "path";
        String extensionKey = "extensionKey";
        String extensionValue = "extensionValue";

        GraphqlErrorException.Builder errorExceptionBuilder = new GraphqlErrorException.Builder()
                .extensions(Collections.singletonMap(extensionKey, extensionValue))
                .path(Collections.singletonList(path));

        ExecutionResult result = new ExecutionResultImpl(Collections.singletonList(errorExceptionBuilder.build()));
        when(build.execute(any(ExecutionInput.Builder.class))).thenReturn(result);

        when(responseHandler.buildResponseStatus(executionResult)).thenReturn(1);
        when(responseHandler.buildResponseBody(executionResult, context)).thenReturn(new HashMap<>(0));

        controller.doPost(graphqlRequest, servletRequest, servletResponse);

        verify(metricsHandler).trackCounter(any(), any(Map.class));
    }

}
