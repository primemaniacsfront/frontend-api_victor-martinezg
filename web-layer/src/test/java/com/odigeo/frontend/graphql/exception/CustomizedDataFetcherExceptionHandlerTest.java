package com.odigeo.frontend.graphql.exception;

import graphql.execution.DataFetcherExceptionHandlerParameters;
import graphql.execution.DataFetcherExceptionHandlerResult;
import graphql.execution.DataFetcherExceptionHandlerResult.Builder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class CustomizedDataFetcherExceptionHandlerTest {

    @Mock
    private DataFetcherErrorCreator errorCreator;

    @InjectMocks
    private CustomizedDataFetcherExceptionHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testOnException() {
        String message = "message";
        List<Object> path = new ArrayList<>(0);
        Map<String, Object> extensions = new HashMap<>(0);
        Throwable exception = mock(Throwable.class);

        DataFetcherExceptionHandlerParameters params = mock(DataFetcherExceptionHandlerParameters.class);
        DataFetcherExceptionHandlerResult result = mock(DataFetcherExceptionHandlerResult.class);
        Builder resultBuilder = mock(Builder.class);

        when(errorCreator.createMessage(params)).thenReturn(message);
        when(errorCreator.createPath(params)).thenReturn(path);
        when(errorCreator.createExtensions(params)).thenReturn(extensions);
        when(errorCreator.createException(params)).thenReturn(exception);

        try (MockedStatic<DataFetcherExceptionHandlerResult> handlerResultMockedStatic =
                 mockStatic(DataFetcherExceptionHandlerResult.class)) {

            handlerResultMockedStatic.when(DataFetcherExceptionHandlerResult::newResult).thenReturn(resultBuilder);
            when(resultBuilder.error(argThat(error -> error instanceof DataFetcherError
                && error.getMessage().equals(message)
                && error.getPath() == path
                && error.getExtensions() == extensions
                && ((DataFetcherError) error).getException() == exception
            ))).thenReturn(resultBuilder);
            when(resultBuilder.build()).thenReturn(result);

            assertSame(handler.onException(params), result);
        }
    }

}

