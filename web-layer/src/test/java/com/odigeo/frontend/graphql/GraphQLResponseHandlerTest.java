package com.odigeo.frontend.graphql;

import com.google.inject.Guice;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResponseInfo;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.graphql.exception.DataFetcherError;
import graphql.ErrorType;
import graphql.ExecutionResult;
import graphql.GraphQLError;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response.Status;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class GraphQLResponseHandlerTest {

    private static final String DEBUG_INFO_LABEL = "debugInfo";
    private static final String HEADER_VALUE = "headerValue";
    private static final SiteHeaders TRACE_HEADER = SiteHeaders.TRACE_ID;
    private static final SiteHeaders SB_HEADER = SiteHeaders.SB_JSESSIONID;

    @Mock
    private Logger logger;
    @Mock
    private ExecutionResult executionResult;
    @Mock
    private GraphQLError error;
    @Mock
    private Map<String, Object> errorSpec;
    @Mock
    private ResolverContext context;
    @Mock
    private DebugInfo debugInfo;
    @Mock
    private HttpServletResponse servletResponse;
    @Mock
    private ResponseInfo responseInfo;

    private GraphQLResponseHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new GraphQLResponseHandler();

        Guice.createInjector(binder -> {
            binder.bind(Logger.class).toProvider(() -> logger);
        }).injectMembers(handler);

        when(executionResult.getErrors()).thenReturn(Collections.singletonList(error));
        when(error.toSpecification()).thenReturn(errorSpec);
        when(context.getDebugInfo()).thenReturn(debugInfo);
        when(context.getResponseInfo()).thenReturn(responseInfo);
    }

    @Test
    public void testBuildResponseStatus() {
        when(executionResult.getErrors()).thenReturn(Collections.emptyList());
        assertEquals(handler.buildResponseStatus(executionResult), Status.OK.getStatusCode());
    }

    @Test
    public void testBuildResponseStatusWithInvalidSyntax() {
        checkStatusCode(ErrorType.InvalidSyntax, Status.BAD_REQUEST);
    }

    @Test
    public void testBuildResponseStatusWithValidationError() {
        checkStatusCode(ErrorType.ValidationError, Status.BAD_REQUEST);
    }

    @Test
    public void testBuildResponseStatusWithServiceException() {
        DataFetcherError error = mock(DataFetcherError.class);
        ServiceException exception = mock(ServiceException.class);
        Status status = Status.FORBIDDEN;

        when(executionResult.getErrors()).thenReturn(Collections.singletonList(error));
        when(error.getException()).thenReturn(exception);
        when(exception.getStatus()).thenReturn(status);

        checkStatusCode(ErrorType.DataFetchingException, status);
    }

    @Test
    public void testBuildResponseStatusInternalError() {
        checkStatusCode(ErrorType.DataFetchingException, Status.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testBuildResponseStatusInternalErrorWithDataFetcher() {
        DataFetcherError error = mock(DataFetcherError.class);
        when(executionResult.getErrors()).thenReturn(Collections.singletonList(error));
        checkStatusCode(ErrorType.DataFetchingException, Status.INTERNAL_SERVER_ERROR);
    }

    private void checkStatusCode(ErrorType errorType, Status expectedStatus) {
        when(error.getErrorType()).thenReturn(errorType);
        assertEquals(handler.buildResponseStatus(executionResult), expectedStatus.getStatusCode());
    }

    @Test
    public void testBuildResponseBody() {
        Map<String, Object> data = new HashMap<>(0);

        when(executionResult.getErrors()).thenReturn(Collections.emptyList());
        when(executionResult.getData()).thenReturn(data);

        Map<String, Object> response = handler.buildResponseBody(executionResult, context);

        assertSame(response.get("data"), data);
        assertNull(response.get("errors"));
        assertNull(response.get("extensions"));
        verify(logger, never()).error(any(), any());
        verify(logger, never()).error(any(), any(), any());
    }

    @Test
    public void testBuildResponseBodyWithErrors() {
        Map<String, Object> response = handler.buildResponseBody(executionResult, context);
        List<?> responseErrors = (List<?>) response.get("errors");

        assertNull(response.get("data"));
        assertSame(responseErrors.get(0), errorSpec);
        verify(logger).error(GraphQLResponseHandler.class, errorSpec);
    }

    @Test
    public void testBuildResponseBodyWithDataFetcherError() {
        Throwable exception = mock(Throwable.class);
        StackTraceElement stackTraceElement = mock(StackTraceElement.class);
        Class<?> rootClass = String.class;
        String rootClassName = rootClass.getName();

        when(exception.getStackTrace()).thenReturn(ArrayUtils.toArray(stackTraceElement));
        when(stackTraceElement.getClassName()).thenReturn(rootClassName);

        dataFetcherErrorTest(exception);
        verify(logger).error(rootClass, errorSpec, exception);
    }

    @Test
    public void testBuildResponseBodyWithDataFetcherWithoutException() {
        dataFetcherErrorTest(null);
        verify(logger).error(GraphQLResponseHandler.class, errorSpec);
    }

    @Test
    public void testBuildResponseBodyWithDataFetcherWithoutStackTrace() {
        Throwable exception = mock(Throwable.class);

        dataFetcherErrorTest(exception);
        verify(logger).error(GraphQLResponseHandler.class, errorSpec, exception);
    }

    @Test
    public void testBuildResponseBodyWithDataFetcherInvalidRootClass() {
        Throwable exception = mock(Throwable.class);
        StackTraceElement stackTraceElement = mock(StackTraceElement.class);

        when(exception.getStackTrace()).thenReturn(ArrayUtils.toArray(stackTraceElement));
        when(stackTraceElement.getClassName()).thenReturn("unknown");

        dataFetcherErrorTest(exception);
        verify(logger).error(GraphQLResponseHandler.class, errorSpec, exception);
    }

    private void dataFetcherErrorTest(Throwable exception) {
        DataFetcherError dataFetcherError = mock(DataFetcherError.class);

        when(executionResult.getErrors()).thenReturn(Collections.singletonList(dataFetcherError));
        when(dataFetcherError.toSpecification()).thenReturn(errorSpec);
        when(dataFetcherError.getException()).thenReturn(exception);

        Map<String, Object> response = handler.buildResponseBody(executionResult, context);
        List<?> responseErrors = (List<?>) response.get("errors");

        assertNull(response.get("data"));
        assertSame(responseErrors.get(0), errorSpec);
    }

    @Test
    public void testBuildResponseBodyWithExtensions() {
        Map<String, Object> debugInfo = Collections.singletonMap("debugLabel", "debugValue");

        when(this.debugInfo.getDebugInfoMap()).thenReturn(debugInfo);

        Map<String, Object> response = handler.buildResponseBody(executionResult, context);
        Map<?, ?> extensions = MapUtils.getMap(response, "extensions");
        assertSame(extensions.get(DEBUG_INFO_LABEL), debugInfo);
    }

    @Test
    public void testBuildResponseCookies() {
        Cookie cookie = mock(Cookie.class);
        when(responseInfo.getCookies()).thenReturn(Collections.singletonList(cookie));

        handler.buildResponseCookies(context, servletResponse);

        verify(servletResponse).addCookie(cookie);
    }

    @Test
    public void buildResponseHeadersWithHeadersShouldCallToAdHeader() {
        Map<SiteHeaders, String> headers = new HashMap<>();
        headers.put(TRACE_HEADER, HEADER_VALUE);
        headers.put(SB_HEADER, HEADER_VALUE);
        when(responseInfo.getHeaders()).thenReturn(headers);

        handler.buildResponseHeaders(context, servletResponse);

        verify(servletResponse, times(2)).addHeader(anyString(), anyString());
        verify(servletResponse).addHeader(TRACE_HEADER.value(), HEADER_VALUE);
        verify(servletResponse).addHeader(SB_HEADER.value(), HEADER_VALUE);
    }

    @Test
    public void buildResponseHeadersWithNullHeadersShouldNotCallToAddHeader() {
        when(responseInfo.getHeaders()).thenReturn(null);

        handler.buildResponseHeaders(context, servletResponse);

        verify(servletResponse, never()).addHeader(anyString(), anyString());
    }

    @Test
    public void buildResponseHeadersWithEmptyHeadersShouldNotCallToAddHeader() {
        Map<SiteHeaders, String> headers = new HashMap<>();
        when(responseInfo.getHeaders()).thenReturn(headers);

        handler.buildResponseHeaders(context, servletResponse);

        verify(servletResponse, never()).addHeader(anyString(), anyString());
    }

    @Test
    public void buildResponseHeadersWithNullResponseInfoShouldNotCallToAddHeader() {
        when(context.getResponseInfo()).thenReturn(null);

        handler.buildResponseHeaders(context, servletResponse);

        verify(servletResponse, never()).addHeader(anyString(), anyString());
    }

    @Test
    public void buildResponseHeadersWithNullContextShouldNotCallToAddHeader() {
        handler.buildResponseHeaders(null, servletResponse);

        verify(servletResponse, never()).addHeader(anyString(), anyString());
    }

}
