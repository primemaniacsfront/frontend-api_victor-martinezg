package com.odigeo.frontend.graphql;

import bean.test.BeanTest;
import java.util.HashMap;

public class GraphQLRequestTest extends BeanTest<GraphQLRequest> {

    @Override
    public GraphQLRequest getBean() {
        GraphQLRequest bean = new GraphQLRequest();

        bean.setQuery("query");
        bean.setOperationName("operation");
        bean.setVariables(new HashMap<>(0));
        return bean;
    }

}
