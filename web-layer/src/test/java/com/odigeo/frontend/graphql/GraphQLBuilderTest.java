package com.odigeo.frontend.graphql;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.frontend.graphql.exception.CustomizedDataFetcherExceptionHandler;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.ResolverLoader;
import com.odigeo.frontend.schemas.SchemaLoader;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mockStatic;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class GraphQLBuilderTest {

    private static final String TEST_NAME = "testName";
    private static final String TEST_ID = "testId";
    private static final String SCHEMA = "schemas/test.schema.graphql";
    private static final String QUERY = "{\n person {\n name \n id \n}\n}";

    private static final class TestResolver implements Resolver {
        public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
            resolversBuilder
                .type(TypeRuntimeWiring.newTypeWiring("Query")
                    .dataFetcher("person", personResolver));
        }

        private final DataFetcher personResolver = environment ->
            new Person(GraphQLBuilderTest.TEST_ID, GraphQLBuilderTest.TEST_NAME);
    }

    private static final class Person {
        private final String id;
        private final String name;

        Person(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    @Mock
    private CustomizedDataFetcherExceptionHandler exceptionHandler;

    @InjectMocks
    private GraphQLBuilder builder;

    private MockedStatic<ConfigurationEngine> configurationEngine;
    private MockedStatic<SchemaLoader> schemaLoader;
    private MockedStatic<ResolverLoader> resolverLoader;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        TestResolver resolver = new TestResolver();

        configurationEngine = mockStatic(ConfigurationEngine.class);
        schemaLoader = mockStatic(SchemaLoader.class);
        resolverLoader = mockStatic(ResolverLoader.class);

        InputStreamReader in = new InputStreamReader(
            this.getClass().getClassLoader().getResourceAsStream(SCHEMA), StandardCharsets.UTF_8);

        configurationEngine.when(() -> ConfigurationEngine.getInstance(TestResolver.class)).thenReturn(resolver);
        schemaLoader.when(SchemaLoader::loadReaders).thenReturn(Collections.singletonList(in));
        resolverLoader.when(ResolverLoader::load).thenReturn(Collections.singletonList(TestResolver.class));
    }

    @AfterMethod
    public void close() {
        configurationEngine.close();
        schemaLoader.close();
        resolverLoader.close();
    }

    @Test
    public void testGetBuild() {
        ExecutionResult executionResult = builder.getBuild().execute(QUERY);
        Map<String, Map<String, String>> result = executionResult.getData();

        assertTrue(executionResult.getErrors().isEmpty());
        assertEquals(result.get("person").get("name"), TEST_NAME);
        assertEquals(result.get("person").get("id"), TEST_ID);
    }

    @Test
    public void testGetBuildWhenIsNotNull() {
        GraphQL build1 = builder.getBuild();
        GraphQL build2 = builder.getBuild();

        assertSame(build1, build2);
    }

}
