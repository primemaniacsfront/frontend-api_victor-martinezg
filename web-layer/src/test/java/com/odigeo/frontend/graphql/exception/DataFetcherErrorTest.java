package com.odigeo.frontend.graphql.exception;

import graphql.ErrorType;
import java.util.List;
import java.util.Map;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class DataFetcherErrorTest {

    @Mock
    private String message;
    @Mock
    private List<Object> path;
    @Mock
    private Map<String, Object> extensions;
    @Mock
    private Throwable exception;

    private DataFetcherError error;

    @BeforeMethod
    public void init() {
        error = new DataFetcherError(message, path, extensions, exception);
    }

    @Test
    public void testGetMessage() {
        assertSame(error.getMessage(), message);
    }

    @Test
    public void testGetLocations() {
        assertNull(error.getLocations());
    }

    @Test
    public void testGetErrorType() {
        assertEquals(error.getErrorType(), ErrorType.DataFetchingException);
    }

    @Test
    public void testGetPath() {
        assertSame(error.getPath(), path);
    }

    @Test
    public void testGetExtensions() {
        assertSame(error.getExtensions(), extensions);
    }

    @Test
    public void testGetException() {
        assertSame(error.getException(), exception);
    }

}
