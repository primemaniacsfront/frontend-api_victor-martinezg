package com.odigeo.frontend.checkout;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResolverContextInitializer;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeToken;
import com.odigeo.frontend.services.checkout.UserInteractionNeededService;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class UserInteractionNeededControllerTest {

    private static final String REDIRECT_URL = "redirectUrl";
    private static final long BOOKING_ID = 1L;
    private static final String ACCEPTED = "accepted";
    private static final String GBP = "GBP";
    private static final String CURRENCY = "currency";
    private static final String ORDER_ID = "orderId";
    private static final String STATUS = "status";

    @Mock
    private HttpServletResponse httpResponse;
    @Mock
    private ResolverContextInitializer contextInitializer;
    @Mock
    private ResolverContext context;
    @Mock
    private Logger logger;
    @Mock
    private CheckoutResumeToken token;
    @Mock
    private UserInteractionNeededService service;
    @Mock
    private HttpServletRequest httpRequest;
    @Captor
    private ArgumentCaptor<Map<String, String>> paramsCaptor;

    @InjectMocks
    private UserInteractionNeededController controller;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(contextInitializer.createContext(any(), any())).thenReturn(context);
    }

    @Test
    public void testResumeBookingGet() {
        when(service.resume(any(), any(), any(), any())).thenReturn(REDIRECT_URL);
        controller.resumeBookingGet(httpRequest, httpResponse, token);
        verifyRedirect();
    }

    @Test
    public void testResumeBookingWithCaptor() {
        Map<String, String[]> params = getStringMapWithArrayValues();
        when(httpRequest.getParameterMap()).thenReturn(params);
        when(service.resume(paramsCaptor.capture(), any(), any(), any())).thenReturn(REDIRECT_URL);
        controller.resumeBookingGet(httpRequest, httpResponse, token);
        verifyRedirect();
        assertParameters(paramsCaptor.getValue());
    }

    @Test
    public void testResumeBookingPostUrlEncoded() {
        when(service.resume(any(), any(), any(), any())).thenReturn(REDIRECT_URL);
        controller.resumeBookingPostUrlEncoded(httpRequest, httpResponse, token);
        verifyRedirect();
    }

    @Test
    public void testResumeBookingPostMultiPart() {
        when(service.resume(any(), any(), any(), any())).thenReturn(REDIRECT_URL);
        controller.resumeBookingPostMultiPart(Collections.emptyMap(), httpRequest, httpResponse, token);
        verifyRedirect();
    }

    @Test
    public void testRedirectException() throws Exception {
        when(service.resume(any(), any(), any(), any())).thenReturn(REDIRECT_URL);
        doThrow(IOException.class)
                .when(httpResponse)
                .sendRedirect(any());
        controller.resumeBookingGet(httpRequest, httpResponse, token);
        verify(logger).error(eq(UserInteractionNeededController.class), anyString());
    }

    private Map<String, String[]> getStringMapWithArrayValues() {
        Map<String, String[]> params = new HashMap<>();

        params.put(STATUS, new String[]{ACCEPTED});
        params.put(ORDER_ID, new String[]{String.valueOf(BOOKING_ID)});
        params.put(CURRENCY, new String[]{GBP});
        return params;
    }

    private void verifyRedirect() {
        try {
            verify(httpResponse).sendRedirect(REDIRECT_URL);
        } catch (IOException e) {
            Assert.fail();
        }
    }

    private void assertParameters(Map<String, String> params) {
        assertEquals(params.get(STATUS), ACCEPTED);
        assertEquals(params.get(ORDER_ID), String.valueOf(BOOKING_ID));
        assertEquals(params.get(CURRENCY), GBP);
    }

}
