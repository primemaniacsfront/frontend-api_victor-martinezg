package com.odigeo.frontend.bootstrap;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.frontend.checkout.UserInteractionNeededController;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutFactory;
import com.odigeo.frontend.services.TestTokenService;
import com.odigeo.frontend.services.VisitEngineService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ServiceApplicationTest {

    private ServiceApplication serviceApplication;

    @BeforeClass
    public void init() {
        ConfigurationEngine.init(module -> {
            module.bind(VisitEngineService.class).toProvider(() -> mock(VisitEngineService.class));
            module.bind(TestTokenService.class).toProvider(() -> mock(TestTokenService.class));
            module.bind(CheckoutFactory.class).toProvider(() -> mock(CheckoutFactory.class));
            module.bind(UserInteractionNeededController.class).toProvider(() -> mock(UserInteractionNeededController.class));
        });

        serviceApplication = new ServiceApplication();
    }

    @Test
    public void testGetSingletons() {
        assertFalse(serviceApplication.getSingletons().isEmpty());
    }

    @Test
    public void testGetClasses() {
        assertTrue(serviceApplication.getClasses().isEmpty());
    }

}
