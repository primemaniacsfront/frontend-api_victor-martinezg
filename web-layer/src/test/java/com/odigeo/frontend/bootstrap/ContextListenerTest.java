package com.odigeo.frontend.bootstrap;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Module;
import com.odigeo.commons.monitoring.dump.DumpServlet;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import com.odigeo.frontend.modules.configuration.ModuleLoader;
import com.odigeo.frontend.monitoring.CommonsMonitoringConfiguration;
import com.odigeo.frontend.monitoring.MetricsHandler;
import java.net.URL;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import org.apache.log4j.PropertyConfigurator;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ContextListenerTest {

    private static final String LOG4J_FILENAME = "log4j.properties";

    @Mock
    private ServletContextEvent servletContextEvent;
    @Mock
    private ServletContext servletContext;

    private ContextListener contextListener;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        contextListener = new ContextListener();

        when(servletContextEvent.getServletContext()).thenReturn(servletContext);
    }

    @Test
    public void testContextInitialized() {
        try (MockedStatic<ModuleLoader> moduleLoaderStatic = mockStatic(ModuleLoader.class);
             MockedStatic<ConfigurationEngine> configurationEngineStatic = mockStatic(ConfigurationEngine.class);
             MockedStatic<PropertyConfigurator> propertyConfiguratorStatic = mockStatic(PropertyConfigurator.class);
             MockedStatic<MetricsManager> metricsManagerStatic = mockStatic(MetricsManager.class)) {

            Module[] modules = new Module[0];
            moduleLoaderStatic
                .when(() -> ModuleLoader.load(any()))
                .thenReturn(modules);

            CommonsMonitoringConfiguration metricsConfig = mock(CommonsMonitoringConfiguration.class);
            MetricsManager metricsManager = mock(MetricsManager.class);
            ReporterStatus metricsReporter = mock(ReporterStatus.class);

            when(metricsConfig.getInitialReporterStatus()).thenReturn(metricsReporter);

            configurationEngineStatic
                .when(() -> ConfigurationEngine.getInstance(CommonsMonitoringConfiguration.class))
                .thenReturn(metricsConfig);
            metricsManagerStatic
                .when(MetricsManager::getInstance)
                .thenReturn(metricsManager);

            contextListener.contextInitialized(servletContextEvent);

            verify(servletContext).setAttribute(eq(HealthCheckServlet.REGISTRY_KEY), any());
            verify(servletContext).setAttribute(eq(DumpServlet.REGISTRY_KEY), any());

            configurationEngineStatic.verify(() -> ConfigurationEngine.init(modules));

            URL log4jUrl = contextListener.getClass().getClassLoader().getResource(LOG4J_FILENAME);
            propertyConfiguratorStatic.verify(() -> PropertyConfigurator.configure(log4jUrl.getFile()));

            verify(metricsManager).addMetricsReporter(MetricsHandler.REGISTRY_NAME);
            verify(metricsManager).changeReporterStatus(metricsReporter);
        }
    }

    @Test
    public void testContextDestroyed() {
        contextListener.contextDestroyed(servletContextEvent);
        verify(servletContext).log(anyString());
    }

}
