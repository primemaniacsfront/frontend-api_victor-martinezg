package com.odigeo.frontend.monitoring;

import bean.test.BeanTest;
import java.util.HashMap;

public class TrackCounterRequestTest extends BeanTest<TrackCounterRequest> {

    @Override
    public TrackCounterRequest getBean() {
        TrackCounterRequest bean = new TrackCounterRequest();
        bean.setMetricName("metricName");
        bean.setDirectTags(new HashMap<>(0));

        return bean;
    }

}
