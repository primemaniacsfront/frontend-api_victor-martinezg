package com.odigeo.frontend.monitoring;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResolverContextInitializer;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MetricsControllerTest {

    private static final String METRIC_NAME = "metricName";
    private static final String UNKNOWN_METRIC_NAME = "unknown";
    private static final long TIME = 1L;
    private static final Map<String, String> DIRECT_TAGS = new HashMap<>(0);

    @Mock
    private ResolverContextInitializer contextInitializer;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private MeasureConstants measureConstants;
    @Mock
    private Measure measure;
    @Mock
    private HttpServletRequest servletRequest;
    @Mock
    private VisitInformation visit;
    @Mock
    private TrackTimeRequest timeRequest;
    @Mock
    private TrackCounterRequest counterRequest;
    @Mock
    private Logger logger;

    @InjectMocks
    private MetricsController controller;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(timeRequest.getMetricName()).thenReturn(METRIC_NAME);
        when(timeRequest.getTime()).thenReturn(TIME);
        when(timeRequest.getDirectTags()).thenReturn(DIRECT_TAGS);
        when(counterRequest.getMetricName()).thenReturn(METRIC_NAME);
        when(counterRequest.getDirectTags()).thenReturn(DIRECT_TAGS);

        ResolverContext context = mock(ResolverContext.class);
        Map<Class<?>, Object> contextMap = Collections.singletonMap(MetricsHandler.class, metricsHandler);

        when(contextInitializer.createContext(eq(servletRequest), argThat(matcher -> !matcher.isEnabled()))).thenReturn(context);
        when(contextInitializer.createContextMap(eq(servletRequest))).thenReturn(contextMap);
        when(measureConstants.getMeasure(METRIC_NAME)).thenReturn(measure);
        when(context.getVisitInformation()).thenReturn(visit);
    }

    @Test
    public void testTrackTime() {
        controller.trackTime(timeRequest, servletRequest);
        verify(metricsHandler).trackTime(measure, TIME, DIRECT_TAGS, visit);
    }

    @Test
    public void testTrackTimeInvalidMeasure() {
        when(timeRequest.getMetricName()).thenReturn(UNKNOWN_METRIC_NAME);
        controller.trackTime(timeRequest, servletRequest);
        verify(metricsHandler, never()).trackTime(any(), anyLong(), any(), any());
    }

    @Test
    public void testTrackTimeInvalidTime() {
        when(timeRequest.getTime()).thenReturn(0L);
        controller.trackTime(timeRequest, servletRequest);
        verify(metricsHandler, never()).trackTime(any(), anyLong(), any(), any());
    }

    @Test
    public void testTrackCounter() {
        controller.trackCounter(counterRequest, servletRequest);
        verify(metricsHandler).trackCounter(measure, DIRECT_TAGS, visit);
    }

    @Test
    public void testTrackCounterInvalidMeasure() {
        when(counterRequest.getMetricName()).thenReturn(UNKNOWN_METRIC_NAME);
        controller.trackCounter(counterRequest, servletRequest);
        verify(metricsHandler, never()).trackCounter(any(), any(), any());
    }

}
