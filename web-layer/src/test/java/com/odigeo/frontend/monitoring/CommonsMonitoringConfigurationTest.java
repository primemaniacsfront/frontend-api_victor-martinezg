package com.odigeo.frontend.monitoring;

import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CommonsMonitoringConfigurationTest {

    private CommonsMonitoringConfiguration configuration;

    @BeforeMethod
    public void init() {
        configuration = new CommonsMonitoringConfiguration();
    }

    @Test
    public void testDefaultStopped() {
        assertEquals(configuration.getInitialReporterStatus(), ReporterStatus.STOPPED);
    }

    @Test
    public void testSetReporterStatus() {
        ReporterStatus status = ReporterStatus.STARTED;
        configuration.setInitialReporterStatusValue(status.name());

        assertEquals(configuration.getInitialReporterStatus(), status);
    }

}
