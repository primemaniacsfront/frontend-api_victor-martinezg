package com.odigeo.frontend.monitoring;

import bean.test.BeanTest;
import java.util.HashMap;

public class TrackTimeRequestTest extends BeanTest<TrackTimeRequest> {

    @Override
    public TrackTimeRequest getBean() {
        TrackTimeRequest bean = new TrackTimeRequest();
        bean.setMetricName("metricName");
        bean.setTime(1L);
        bean.setDirectTags(new HashMap<>(0));

        return bean;
    }

}
