package com.odigeo.frontend.logs;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Browser;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.graphql.GraphQLRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.MDC;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class LogDetailsTracerTest {

    private static final String REFERER = "referer";
    private static final String CLIENT_USER_AGENT = "client.user_agent";
    private static final String CLIENT_IP = "client.ip";
    private static final String CLIENT_TEST_TOKEN_SET = "client.test_token_set";
    private static final String CLIENT_HEADERS = "client.headers";
    private static final String REQUEST_QUERY = "request.query";
    private static final String REQUEST_VARIABLES = "request.variables";

    private static final String VISIT_CODE = "visit.code";
    private static final String VISIT_DEVICE = "visit.device";
    private static final String VISIT_BROWSER = "visit.browser";
    private static final String VISIT_BRAND = "visit.brand";
    private static final String VISIT_USER_DEVICE = "visit.user_device";

    private static final String INIT_BRACKET = "[";
    private static final String END_BRACKET = "]";

    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private ResolverContext context;
    @Mock
    private RequestInfo requestInfo;
    @Mock
    private GraphQLRequest graphqlRequest;
    @Mock
    private HttpServletRequest servletRequest;

    @InjectMocks
    private LogDetailsTracer logsTracer;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(context.getRequestInfo()).thenReturn(requestInfo);
        when(context.getVisitInformation()).thenThrow(ServiceException.class);
    }

    @AfterMethod
    public void end() {
        MDC.clear();
    }

    @Test
    public void testInitTracer() {
        String referer = "referer";
        when(requestInfo.getHeader(SiteHeaders.REFERER)).thenReturn(referer);

        logsTracer.initTracer(servletRequest, graphqlRequest, context);
        assertEquals(MDC.get(REFERER), referer);
    }

    @Test
    public void testInitTracerClientInfo() {
        String userAgent = "userAgent";
        String clientIp = "clientIp";
        String testToken = "testToken";
        String headerName = "header";

        when(requestInfo.getHeader(SiteHeaders.USER_AGENT)).thenReturn(userAgent);
        when(servletRequest.getRemoteAddr()).thenReturn(clientIp);
        when(requestInfo.getCookieValue(SiteCookies.TEST_TOKEN_SPACE)).thenReturn(testToken);
        when(servletRequest.getHeaderNames())
            .thenReturn(Collections.enumeration(Collections.singletonList(headerName)));

        logsTracer.initTracer(servletRequest, graphqlRequest, context);

        assertEquals(MDC.get(CLIENT_USER_AGENT), userAgent);
        assertEquals(MDC.get(CLIENT_IP), clientIp);
        assertEquals(MDC.get(CLIENT_TEST_TOKEN_SET), testToken);
        assertEquals(MDC.get(CLIENT_HEADERS), INIT_BRACKET + headerName + END_BRACKET);
    }

    @Test
    public void testInitTracerRequestInfo() {
        String query = "query";
        String variables = "variables";
        Map<String, Object> varMap = new HashMap<>(0);

        when(graphqlRequest.getQuery()).thenReturn(query);
        when(graphqlRequest.getVariables()).thenReturn(varMap);
        when(jsonUtils.toJson(varMap)).thenReturn(variables);

        logsTracer.initTracer(servletRequest, graphqlRequest, context);

        assertEquals(MDC.get(REQUEST_QUERY), query);
        assertEquals(MDC.get(REQUEST_VARIABLES), variables);
    }

    @Test
    public void testInitTracerVisitInfo() {
        ResolverContext context = mock(ResolverContext.class);
        VisitInformation visit = mock(VisitInformation.class);

        Device device = mock(Device.class);
        Browser browser = mock(Browser.class);
        Brand brand = mock(Brand.class);
        String visitCode = "visitCode";
        String deviceName = "device";
        String browserName = "browser";
        String brandName = "brand";
        String userDevice = "userDevice";

        when(device.name()).thenReturn(deviceName);
        when(browser.name()).thenReturn(browserName);
        when(brand.name()).thenReturn(brandName);
        when(visit.getVisitCode()).thenReturn(visitCode);
        when(visit.getDevice()).thenReturn(device);
        when(visit.getBrowser()).thenReturn(browser);
        when(visit.getBrand()).thenReturn(brand);
        when(visit.getUserDevice()).thenReturn(userDevice);

        when(context.getRequestInfo()).thenReturn(requestInfo);
        when(context.getVisitInformation()).thenReturn(visit);

        logsTracer.initTracer(servletRequest, graphqlRequest, context);

        assertEquals(MDC.get(VISIT_CODE), visitCode);
        assertEquals(MDC.get(VISIT_DEVICE), deviceName);
        assertEquals(MDC.get(VISIT_BROWSER), browserName);
        assertEquals(MDC.get(VISIT_BRAND), brandName);
        assertEquals(MDC.get(VISIT_USER_DEVICE), userDevice);
    }

    @Test
    public void testInitTracerWithoutVisit() {
        String visitCode = "visitCode";
        when(requestInfo.getHeaderOrCookie(SiteHeaders.VISIT_INFORMATION, SiteCookies.VISIT_INFORMATION))
            .thenReturn(visitCode);

        logsTracer.initTracer(servletRequest, graphqlRequest, context);
        assertEquals(MDC.get(VISIT_CODE), visitCode);
    }

    @Test
    public void testEndTracer() {
        logsTracer.endTracer();
        assertNull(MDC.getCopyOfContextMap());
    }

}
