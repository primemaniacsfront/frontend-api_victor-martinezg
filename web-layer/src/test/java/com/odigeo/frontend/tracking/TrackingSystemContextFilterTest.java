package com.odigeo.frontend.tracking;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.trackingapi.client.v2.context.FlowContextManager;
import javax.servlet.ServletRequest;
import org.mockito.MockedStatic;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class TrackingSystemContextFilterTest {

    private static final String FLOW_NAMESPACE = "frontend-api";
    private TrackingSystemContextFilter filter;

    @BeforeClass
    public void init() {
        filter = new TrackingSystemContextFilter();
    }

    @Test
    public void testGetFlowName() {
        assertNotNull(filter.getFlowName(mock(ServletRequest.class)));
    }

    @Test
    public void testGetFlowNamespace() {
        assertEquals(filter.getFlowNamespace(), FLOW_NAMESPACE);
    }

    @Test
    public void testGetFlowContextManager() {
        try (MockedStatic<ConfigurationEngine> configurationEngineMock = mockStatic(ConfigurationEngine.class)) {
            FlowContextManager flowContextManager = mock(FlowContextManager.class);
            configurationEngineMock.when(() -> ConfigurationEngine.getInstance(FlowContextManager.class))
                .thenReturn(flowContextManager);

            assertSame(filter.getFlowContextManager(), flowContextManager);
        }
    }

}
