package com.odigeo.frontend.graphql.exception;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import graphql.execution.DataFetcherExceptionHandler;
import graphql.execution.DataFetcherExceptionHandlerParameters;
import graphql.execution.DataFetcherExceptionHandlerResult;

@Singleton
public class CustomizedDataFetcherExceptionHandler implements DataFetcherExceptionHandler {

    @Inject
    private DataFetcherErrorCreator errorCreator;

    @Override
    public DataFetcherExceptionHandlerResult onException(DataFetcherExceptionHandlerParameters params) {
        DataFetcherError error = new DataFetcherError(
            errorCreator.createMessage(params),
            errorCreator.createPath(params),
            errorCreator.createExtensions(params),
            errorCreator.createException(params));

        return DataFetcherExceptionHandlerResult.newResult().error(error).build();
    }

}
