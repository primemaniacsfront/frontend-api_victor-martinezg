package com.odigeo.frontend.graphql;

import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResolverContextInitializer;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.logs.LogDetailsTracer;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MeasureTags;
import com.odigeo.frontend.monitoring.MetricsHandler;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQLError;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.annotations.GZIP;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Path("/")
public class GraphQLController {

    private static final String DEBUG_INFO_PARAMETER = "debugInfo";

    private static final String EXTENSIONS_ERROR_CODE = "code";
    private static final String EXTENSIONS_ERROR_SERVICE = "service";
    private static final String UNKNOWN_PATH = "unknown_path";
    private static final String ERROR_SERVICE = ServiceName.FRONTEND_API.getLabel();
    private static final String VALIDATION_ERROR_CODE = ErrorCodes.VALIDATION.getCode();

    @Inject
    private GraphQLBuilder builder;
    @Inject
    private ResolverContextInitializer contextInitializer;
    @Inject
    private GraphQLResponseHandler responseHandler;
    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private LogDetailsTracer logsTracer;
    @Inject
    private MetricsHandler metricsHandler;

    @POST
    @GZIP
    @Path("/service/graphql")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response doPost(GraphQLRequest parameters,
                           @Context HttpServletRequest servletRequest,
                           @Context HttpServletResponse servletResponse) {

        return executePost(parameters, servletRequest, servletResponse, new DebugInfo());
    }

    @POST
    @GZIP
    @Path("/engineering/service/graphql")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response doDebugPost(GraphQLRequest graphqlRequest,
                                @Context HttpServletRequest servletRequest,
                                @Context HttpServletResponse servletResponse,
                                @Context UriInfo uriInfo) {

        String debugInfoParam = uriInfo.getQueryParameters().getFirst(DEBUG_INFO_PARAMETER);
        DebugInfo debugInfo = ObjectUtils.defaultIfNull(jsonUtils.fromJson(debugInfoParam, DebugInfo.class), new DebugInfo());
        debugInfo.setEnabled(true);

        return executePost(graphqlRequest, servletRequest, servletResponse, debugInfo);
    }

    Response executePost(GraphQLRequest graphqlRequest,
                         HttpServletRequest servletRequest,
                         HttpServletResponse servletResponse,
                         DebugInfo debugInfo) {

        ResolverContext context = contextInitializer.createContext(servletRequest, debugInfo);
        Map<Class<?>, Object> contextMap = contextInitializer.createContextMap(servletRequest);

        logsTracer.initTracer(servletRequest, graphqlRequest, context);

        ExecutionInput.Builder executionInput = ExecutionInput.newExecutionInput()
                .query(StringUtils.defaultString(graphqlRequest.getQuery()))
                .operationName(StringUtils.defaultString(graphqlRequest.getOperationName()))
                .variables(MapUtils.emptyIfNull(graphqlRequest.getVariables()))
                .graphQLContext(contextMap)
                .context(context);

        ExecutionResult executionResult = builder.getBuild().execute(executionInput);

        responseHandler.buildResponseCookies(context, servletResponse);
        responseHandler.buildResponseHeaders(context, servletResponse);

        if (!executionResult.getErrors().isEmpty()) {
            executionResult.getErrors().stream().forEach(graphQLError -> metricError(graphQLError, graphqlRequest));
        }

        Response response = Response
                .status(responseHandler.buildResponseStatus(executionResult))
                .entity(responseHandler.buildResponseBody(executionResult, context))
                .build();

        logsTracer.endTracer();

        return response;
    }

    private void metricError(GraphQLError graphQLError, GraphQLRequest graphqlRequest) {
        Map<String, String> tags = new HashMap<>();

        tags.put(MeasureTags.QUERY.getLabel(), getErrorPath(graphQLError, graphqlRequest));

        tags.put(MeasureTags.SERVICE.getLabel(), Optional.ofNullable(graphQLError.getExtensions())
                .map(extensionsMap -> getExtensionValue(extensionsMap, EXTENSIONS_ERROR_SERVICE, ERROR_SERVICE)).orElse(ERROR_SERVICE));

        tags.put(MeasureTags.CODE.getLabel(), Optional.ofNullable(graphQLError.getExtensions())
                .map(extensionsMap -> getExtensionValue(extensionsMap, EXTENSIONS_ERROR_CODE, VALIDATION_ERROR_CODE)).orElse(VALIDATION_ERROR_CODE));

        metricsHandler.trackCounter(MeasureConstants.GENERIC_ERROR_COUNTER, tags);
    }

    private String getErrorPath(GraphQLError graphQLError, GraphQLRequest graphqlRequest) {
        return Optional.ofNullable(graphQLError.getPath()).
                filter(CollectionUtils::isNotEmpty).map(path -> getErrorPath(path, graphqlRequest)).orElse(getPath(graphqlRequest));
    }

    private String getExtensionValue(Map<String, Object> extensionsMap, String extensionsValue, String defaultValue) {
        return !extensionsMap.isEmpty() ? Optional.ofNullable(extensionsMap.get(extensionsValue)).map(Object::toString).orElse(defaultValue) : defaultValue;
    }

    private String getErrorPath(List<Object> paths, GraphQLRequest graphqlRequest) {
        return !paths.isEmpty() ? Optional.ofNullable(paths.get(0)).map(Object::toString).orElse(getPath(graphqlRequest)) : getPath(graphqlRequest);
    }

    private String getPath(GraphQLRequest graphqlRequest) {
        return Optional.ofNullable(graphqlRequest.getVariables()).orElse(Collections.singletonMap(UNKNOWN_PATH, UNKNOWN_PATH)).keySet().stream().findFirst().orElse(UNKNOWN_PATH);
    }

}
