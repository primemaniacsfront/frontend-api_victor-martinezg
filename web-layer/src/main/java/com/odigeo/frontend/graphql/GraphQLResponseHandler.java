package com.odigeo.frontend.graphql;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResponseInfo;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.graphql.exception.DataFetcherError;
import graphql.ErrorClassification;
import graphql.ErrorType;
import graphql.ExecutionResult;
import graphql.ExecutionResultImpl;
import graphql.ExecutionResultImpl.Builder;
import graphql.GraphQLError;
import graphql.GraphQLException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ClassUtils;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response.Status;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@Singleton
class GraphQLResponseHandler {

    private static final String DEBUG_INFO_LABEL = "debugInfo";

    @Inject
    private Logger logger;

    public int buildResponseStatus(ExecutionResult executionResult) {
        Status status = Status.OK;

        if (CollectionUtils.isNotEmpty(executionResult.getErrors())) {
            GraphQLError error = executionResult.getErrors().get(0);

            if (isGraphQLSyntaxError(error)) {
                status = Status.BAD_REQUEST;

            } else if (isDataFetcherError(error)) {
                DataFetcherError dataFetcherError = (DataFetcherError) error;
                Throwable exception = dataFetcherError.getException();

                status = exception instanceof ServiceException
                        ? ((ServiceException) exception).getStatus()
                        : Status.INTERNAL_SERVER_ERROR;

            } else {
                status = Status.INTERNAL_SERVER_ERROR;
            }
        }

        return status.getStatusCode();
    }

    public void buildResponseCookies(ResolverContext context, HttpServletResponse servletResponse) {
        context.getResponseInfo().getCookies().forEach(servletResponse::addCookie);
    }

    public void buildResponseHeaders(ResolverContext context, HttpServletResponse servletResponse) {
        Optional.ofNullable(context)
                .map(ResolverContext::getResponseInfo)
                .map(ResponseInfo::getHeaders)
                .map(Map::entrySet)
                .orElse(Collections.emptySet())
                .stream()
                .forEach(header -> servletResponse.addHeader(header.getKey().value(), header.getValue()));
    }

    public Map<String, Object> buildResponseBody(ExecutionResult executionResult, ResolverContext context) {
        ExecutionResultImpl.Builder builder = new Builder();

        addResponseData(builder, executionResult);
        addResponseExtensions(builder, context);
        addResponseErrors(builder, executionResult);

        logErrors(executionResult);

        return builder.build().toSpecification();
    }

    private void addResponseData(Builder builder, ExecutionResult executionResult) {
        if (MapUtils.isNotEmpty(executionResult.getData()) || CollectionUtils.isEmpty(executionResult.getErrors())) {
            builder.data(executionResult.getData());
        }
    }

    private void addResponseErrors(Builder builder, ExecutionResult executionResult) {
        if (CollectionUtils.isNotEmpty(executionResult.getErrors()) || MapUtils.isEmpty(executionResult.getData())) {
            builder.errors(executionResult.getErrors());
        }
    }

    private void addResponseExtensions(Builder builder, ResolverContext context) {
        DebugInfo debugInfo = context.getDebugInfo();
        Map<String, Object> debugInfoMap = debugInfo.getDebugInfoMap();

        if (MapUtils.isNotEmpty(debugInfoMap)) {
            builder.extensions(Collections.singletonMap(DEBUG_INFO_LABEL, debugInfoMap));
        }
    }

    private void logErrors(ExecutionResult executionResult) {
        if (CollectionUtils.isNotEmpty(executionResult.getErrors())) {
            executionResult.getErrors().forEach(this::logErrors);
        }
    }

    private void logErrors(GraphQLError error) {

        if (isDataFetcherWithException(error)) {
            DataFetcherError dataFetcherError = (DataFetcherError) error;
            Throwable exception = dataFetcherError.getException();
            logger.error(calculateErrorRootClass(exception), dataFetcherError.toSpecification(), exception);

        } else if (error instanceof GraphQLException) {
            GraphQLException graphQLException = (GraphQLException) error;
            logger.error(calculateErrorRootClass(graphQLException), graphQLException.getMessage(), graphQLException);

        } else {
            logger.error(this.getClass(), error.toSpecification());
        }
    }

    private Class<?> calculateErrorRootClass(Throwable exception) {
        return Optional.ofNullable(exception.getStackTrace())
                .map(stackTrace -> ArrayUtils.get(stackTrace, 0))
                .map(StackTraceElement::getClassName)
                .map(traceName -> {
                    try {
                        return ClassUtils.getClass(traceName);
                    } catch (ClassNotFoundException ex) {
                        return this.getClass();
                    }
                }).orElseGet(this::getClass);
    }

    private boolean isGraphQLSyntaxError(GraphQLError error) {
        ErrorClassification errorType = error.getErrorType();

        return errorType == ErrorType.InvalidSyntax
                || errorType == ErrorType.ValidationError;
    }

    private boolean isDataFetcherError(GraphQLError error) {
        return error instanceof DataFetcherError;
    }

    private boolean isDataFetcherWithException(GraphQLError error) {
        return isDataFetcherError(error) && ((DataFetcherError) error).getException() != null;
    }

}
