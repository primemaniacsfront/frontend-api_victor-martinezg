package com.odigeo.frontend.graphql.scalars;

import graphql.scalars.ExtendedScalars;
import graphql.schema.GraphQLScalarType;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface ScalarLoader {

    static List<GraphQLScalarType> load() {
        return Stream.of(
            ExtendedScalars.GraphQLLong

        ).collect(Collectors.toList());
    }

}
