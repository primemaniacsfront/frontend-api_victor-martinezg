package com.odigeo.frontend.graphql.exception;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import graphql.execution.DataFetcherExceptionHandlerParameters;
import graphql.execution.ResultPath;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

@Singleton
public class DataFetcherErrorCreator {

    private static final String EXTENSIONS_EXCEPTION_LABEL = "exception";

    private static final ErrorCodes DEFAULT_ERROR = ErrorCodes.INTERNAL_GENERIC;
    private static final ServiceName DEFAULT_SERVICE_NAME = ServiceName.FRONTEND_API;

    private static final String EXTENSIONS_ERROR_CODE = "code";
    private static final String EXTENSIONS_ERROR_DESC = "description";

    private static final String EXTENSIONS_ERROR_SERVICE = "service";

    List<Object> createPath(DataFetcherExceptionHandlerParameters params) {
        return Optional.ofNullable(params.getPath())
            .map(ResultPath::toList)
            .orElse(Collections.emptyList());
    }

    Map<String, Object> createExtensions(DataFetcherExceptionHandlerParameters params) {
        Map<String, Object> extensions = new HashMap<>();
        Throwable exception = params.getException();
        ResolverContext context = params.getDataFetchingEnvironment().getContext();

        if (context.getDebugInfo().isEnabled()) {
            extensions.put(EXTENSIONS_EXCEPTION_LABEL, Stream.of(ExceptionUtils.getRootCauseStackTrace(exception))
                .map(StringUtils::trim)
                .collect(Collectors.toList()));
        }

        ErrorCodes error = exception instanceof ServiceException
            ? ((ServiceException) exception).getErrorCode()
            : DEFAULT_ERROR;

        ServiceName serviceName = exception instanceof ServiceException
                ? ((ServiceException) exception).getServiceName()
                : DEFAULT_SERVICE_NAME;

        extensions.put(EXTENSIONS_ERROR_CODE, error.getCode());
        extensions.put(EXTENSIONS_ERROR_DESC, error.getDescription());
        extensions.put(EXTENSIONS_ERROR_SERVICE, serviceName.getLabel());
        return extensions;
    }

    String createMessage(DataFetcherExceptionHandlerParameters params) {
        return ExceptionUtils.getMessage(params.getException());
    }

    Throwable createException(DataFetcherExceptionHandlerParameters params) {
        return params.getException();
    }
}
