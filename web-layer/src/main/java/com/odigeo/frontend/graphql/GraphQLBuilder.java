package com.odigeo.frontend.graphql;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.retail.frontendgraphql.schema.ValidationDirectiveWiring;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.graphql.directives.DirectiveLoader;
import com.odigeo.frontend.graphql.exception.CustomizedDataFetcherExceptionHandler;
import com.odigeo.frontend.graphql.scalars.ScalarLoader;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.ResolverLoader;
import com.odigeo.frontend.schemas.SchemaLoader;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

@Singleton
class GraphQLBuilder {

    @Inject
    private CustomizedDataFetcherExceptionHandler exceptionHandler;

    private GraphQL build;

    GraphQL getBuild() {
        if (build == null) {
            SchemaGenerator schemaGenerator = new SchemaGenerator();
            TypeDefinitionRegistry typeDefinitionRegistry = new TypeDefinitionRegistry();
            RuntimeWiring.Builder runtimeWiringBuilder = RuntimeWiring.newRuntimeWiring();

            addSchemas(typeDefinitionRegistry);
            addResolvers(runtimeWiringBuilder);
            addMapFieldDirectives(runtimeWiringBuilder);
            addValidationFieldDirectives(runtimeWiringBuilder);
            addScalars(runtimeWiringBuilder);

            RuntimeWiring runtimeWiring = runtimeWiringBuilder.build();

            GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

            build = GraphQL.newGraphQL(graphQLSchema)
                    .defaultDataFetcherExceptionHandler(exceptionHandler)
                    .build();
        }

        return build;
    }

    private void addSchemas(TypeDefinitionRegistry typeDefinitionRegistry) {
        SchemaParser schemaParser = new SchemaParser();
        SchemaLoader.loadReaders().stream()
                .map(schemaParser::parse)
                .forEach(typeDefinitionRegistry::merge);
    }

    private void addResolvers(RuntimeWiring.Builder runtimeWiringBuilder) {
        ResolverLoader.load().forEach(resolverClass -> {
            Resolver resolver = ConfigurationEngine.getInstance(resolverClass);
            resolver.addToBuilder(runtimeWiringBuilder);
        });
    }

    private void addMapFieldDirectives(RuntimeWiring.Builder runtimeWiringBuilder) {
        DirectiveLoader.load().forEach(runtimeWiringBuilder::directive);
    }

    private void addValidationFieldDirectives(RuntimeWiring.Builder runtimeWiringBuilder) {
        ValidationDirectiveWiring validationSchema = new ValidationDirectiveWiring();
        runtimeWiringBuilder.directiveWiring(validationSchema);
    }

    private void addScalars(RuntimeWiring.Builder runtimeWiringBuilder) {
        ScalarLoader.load().forEach(runtimeWiringBuilder::scalar);
    }

}
