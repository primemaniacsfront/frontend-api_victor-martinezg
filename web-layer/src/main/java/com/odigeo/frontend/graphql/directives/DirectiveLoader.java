package com.odigeo.frontend.graphql.directives;

import com.edreamsodigeo.retail.frontendgraphql.directive.map.DirectiveBigDecimal;
import com.edreamsodigeo.retail.frontendgraphql.directive.map.DirectiveDateISO8601;
import graphql.schema.idl.SchemaDirectiveWiring;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface DirectiveLoader {
    //Add here the list of common directives for the schema
    static Map<String, SchemaDirectiveWiring> load() {
        return Stream.of(
            new SimpleEntry<>(DirectiveDateISO8601.NAME, new DirectiveDateISO8601()),
            new SimpleEntry<>(DirectiveBigDecimal.NAME, new DirectiveBigDecimal())

        ).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));
    }

}
