package com.odigeo.frontend.graphql.exception;

import graphql.ErrorClassification;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;
import java.util.List;
import java.util.Map;

public class DataFetcherError implements GraphQLError {

    private final String message;
    private final List<Object> path;
    private final Map<String, Object> extensions;
    private final Throwable exception;

    DataFetcherError(String message, List<Object> path, Map<String, Object> extensions,
                     Throwable exception) {
        this.message = message;
        this.path = path;
        this.extensions = extensions;
        this.exception = exception;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorClassification getErrorType() {
        return ErrorType.DataFetchingException;
    }

    @Override
    public List<Object> getPath() {
        return path;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return extensions;
    }

    public Throwable getException() {
        return exception;
    }

}
