package com.odigeo.frontend.tracking;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.trackingapi.client.v2.context.FlowContextManager;
import com.odigeo.trackingapi.client.v2.context.TrackingExecutionContextFilter;
import java.util.UUID;
import javax.servlet.ServletRequest;

public class TrackingSystemContextFilter extends TrackingExecutionContextFilter {
    private static final String FLOW_NAMESPACE = "frontend-api";

    @Override
    protected String getFlowName(ServletRequest servletRequest) {
        return UUID.randomUUID().toString();
    }

    @Override
    protected String getFlowNamespace() {
        return FLOW_NAMESPACE;
    }

    @Override
    protected FlowContextManager getFlowContextManager() {
        return ConfigurationEngine.getInstance(FlowContextManager.class);
    }

}
