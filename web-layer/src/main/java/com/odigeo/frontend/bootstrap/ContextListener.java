package com.odigeo.frontend.bootstrap;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.config.files.ConfigurationFilesManager;
import com.odigeo.commons.monitoring.dump.DumpServlet;
import com.odigeo.commons.monitoring.dump.DumpStateRegistry;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.rest.monitoring.dump.AutoDumpStateRegister;
import com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister;
import com.odigeo.frontend.modules.configuration.ModuleLoader;
import com.odigeo.frontend.monitoring.CommonsMonitoringConfiguration;
import com.odigeo.frontend.monitoring.MetricsHandler;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.net.URL;

public class ContextListener implements ServletContextListener {

    private static final String LOG4J_FILENAME = "/log4j.properties";

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();
        HealthCheckRegistry healthRegistry = new HealthCheckRegistry();
        DumpStateRegistry dumpRegistry = new DumpStateRegistry();

        servletContext.log("Bootstrapping .....");

        initLog4J(servletContext);

        initConfigurationEngine(servletContext, healthRegistry, dumpRegistry);

        initMonitoringServlets(servletContext, healthRegistry, dumpRegistry);

        initMetrics(servletContext);

        servletContext.log("Bootstrapping finished!");
    }

    private void initMonitoringServlets(ServletContext servletContext,
                                        HealthCheckRegistry healthRegistry, DumpStateRegistry dumpRegistry) {

        servletContext.setAttribute(HealthCheckServlet.REGISTRY_KEY, healthRegistry);
        servletContext.setAttribute(DumpServlet.REGISTRY_KEY, dumpRegistry);

        servletContext.log("Monitoring servlets have been initialized");
    }

    private void initConfigurationEngine(ServletContext servletContext,
                                         HealthCheckRegistry healthRegistry, DumpStateRegistry dumpRegistry) {

        AutoHealthCheckRegister healthRegister = new AutoHealthCheckRegister(healthRegistry);
        AutoDumpStateRegister dumpRegister = new AutoDumpStateRegister(dumpRegistry);

        ConfigurationEngine.init(ModuleLoader.load(healthRegister, dumpRegister));
        servletContext.log("ConfigurationEngine has been initialized");
    }

    private void initLog4J(ServletContext servletContext) {
        ConfigurationFilesManager configurationFilesManager = new ConfigurationFilesManager();
        URL configurationFileUrl = configurationFilesManager.getConfigurationFileUrl(LOG4J_FILENAME, this.getClass());
        if (configurationFileUrl == null) {
            servletContext.log("Log4j cannot be initialized " + LOG4J_FILENAME + " not found");
        } else {
            PropertyConfigurator.configure(configurationFileUrl.getFile());
            servletContext.log("Log4j has been initialized");
        }
    }

    private void initMetrics(ServletContext servletContext) {
        CommonsMonitoringConfiguration config = ConfigurationEngine.getInstance(CommonsMonitoringConfiguration.class);
        MetricsManager metricsManager = MetricsManager.getInstance();

        metricsManager.addMetricsReporter(MetricsHandler.REGISTRY_NAME);
        metricsManager.changeReporterStatus(config.getInitialReporterStatus());

        servletContext.log("Metrics have been initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        event.getServletContext().log("Context destroyed");
    }

}
