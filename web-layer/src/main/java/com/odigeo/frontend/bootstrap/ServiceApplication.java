package com.odigeo.frontend.bootstrap;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.frontend.checkout.UserInteractionNeededController;
import com.odigeo.frontend.engineering.EngineeringController;
import com.odigeo.frontend.graphql.GraphQLController;
import com.odigeo.frontend.monitoring.MetricsController;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ws.rs.core.Application;

public class ServiceApplication extends Application {

    private final Set<Object> singletons;

    public ServiceApplication() {
        singletons = Stream.of(
            GraphQLController.class,
            MetricsController.class,
            EngineeringController.class,
            UserInteractionNeededController.class
        ).map(ConfigurationEngine::getInstance).collect(Collectors.toSet());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<>();
    }

}
