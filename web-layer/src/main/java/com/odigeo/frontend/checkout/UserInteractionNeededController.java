package com.odigeo.frontend.checkout;

import com.google.inject.Inject;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResolverContextInitializer;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeToken;
import com.odigeo.frontend.services.checkout.UserInteractionNeededService;
import com.odigeo.tracking.client.track.method.Track;
import org.jboss.resteasy.annotations.GZIP;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Path("/service")
@Produces(MediaType.APPLICATION_JSON)
public class UserInteractionNeededController {

    @Inject
    private Logger logger;
    @Inject
    private ResolverContextInitializer contextInitializer;
    @Inject
    private UserInteractionNeededService service;

    @GET
    @GZIP
    @Path("/userinteractionreturn/{token}{path:(/.*)?}")
    public void resumeBookingGet(@Context HttpServletRequest httpRequest, @Context HttpServletResponse httpResponse,
                                 @NotNull @PathParam("token") CheckoutResumeToken token) {
        Map<String, String[]> params = httpRequest.getParameterMap();
        ResolverContext context = contextInitializer.createContext(httpRequest, new DebugInfo());
        String redirectUrl = service.resume(createParamsFromArrayForResume(params), context, token, httpRequest.getServerName());
        redirect(httpResponse, redirectUrl);
    }

    @POST
    @GZIP
    @Path("/userinteractionreturn/{token}{path:(/.*)?}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Track(name = "resumeBookingPostUrlEncoded")
    public void resumeBookingPostUrlEncoded(@Context HttpServletRequest httpRequest, @Context HttpServletResponse httpResponse,
                                            @NotNull @PathParam("token") CheckoutResumeToken token) {
        Map<String, String[]> params = httpRequest.getParameterMap();
        ResolverContext context = contextInitializer.createContext(httpRequest, new DebugInfo());
        String redirectUrl = service.resume(createParamsFromArrayForResume(params), context, token, httpRequest.getServerName());
        redirect(httpResponse, redirectUrl);
    }

    @POST
    @GZIP
    @Path("/userinteractionreturn/{token}{path:(/.*)?}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Track(name = "resumeBookingPostMultiPart")
    public void resumeBookingPostMultiPart(Map<String, String> params, @Context HttpServletRequest httpRequest, @Context HttpServletResponse httpResponse,
                                           @NotNull @PathParam("token") CheckoutResumeToken token) {
        ResolverContext context = contextInitializer.createContext(httpRequest, new DebugInfo());
        String redirectUrl = service.resume(params, context, token, httpRequest.getServerName());
        redirect(httpResponse, redirectUrl);
    }

    private void redirect(@Context HttpServletResponse httpResponse, String redirectionUrl) {
        try {
            httpResponse.sendRedirect(redirectionUrl);
        } catch (IOException e) {
            logger.error(this.getClass(), "Error resume redirect: " + redirectionUrl + ";" + e.getMessage());
        }
    }

    private Map<String, String> createParamsFromArrayForResume(Map<String, String[]> params) {
        return params
                .entrySet()
                .stream()
                .flatMap(e -> Arrays.stream(e.getValue()).map(v -> new AbstractMap.SimpleEntry<>(e.getKey(), v)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

}
