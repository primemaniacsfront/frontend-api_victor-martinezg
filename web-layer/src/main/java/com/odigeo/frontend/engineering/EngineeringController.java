package com.odigeo.frontend.engineering;

import com.google.inject.Inject;
import com.odigeo.frontend.services.VisitEngineService;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/engineering/service")
public class EngineeringController {

    @Inject
    private VisitEngineService visitService;

    @Path("visit/deserialize")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public VisitResponse deserialize(@QueryParam("visit") String visit) {
        return visitService.deserialize(visit);
    }

}
