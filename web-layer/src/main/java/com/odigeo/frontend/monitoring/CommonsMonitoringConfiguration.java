package com.odigeo.frontend.monitoring;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;

@Singleton
@ConfiguredInPropertiesFile
public class CommonsMonitoringConfiguration {

    private String initialReporterStatusValue = ReporterStatus.STOPPED.name();

    public void setInitialReporterStatusValue(String initialReporterStatusValue) {
        this.initialReporterStatusValue = initialReporterStatusValue;
    }

    public ReporterStatus getInitialReporterStatus() {
        return ReporterStatus.valueOf(initialReporterStatusValue);
    }

}
