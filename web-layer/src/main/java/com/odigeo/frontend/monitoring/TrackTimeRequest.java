package com.odigeo.frontend.monitoring;

import java.util.Collections;
import java.util.Map;

public class TrackTimeRequest {

    private static final Map<String, String> DEFAULT_DIRECT_TAGS = Collections.emptyMap();

    private String metricName;
    private long time;
    private Map<String, String> directTags = DEFAULT_DIRECT_TAGS;

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Map<String, String> getDirectTags() {
        return directTags;
    }

    public void setDirectTags(Map<String, String> directTags) {
        this.directTags = directTags;
    }

}
