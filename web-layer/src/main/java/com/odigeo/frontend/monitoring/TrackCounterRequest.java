package com.odigeo.frontend.monitoring;

import java.util.Collections;
import java.util.Map;

public class TrackCounterRequest {

    private static final Map<String, String> DEFAULT_DIRECT_TAGS = Collections.emptyMap();

    private String metricName;
    private Map<String, String> directTags = DEFAULT_DIRECT_TAGS;

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public Map<String, String> getDirectTags() {
        return directTags;
    }

    public void setDirectTags(Map<String, String> directTags) {
        this.directTags = directTags;
    }

}
