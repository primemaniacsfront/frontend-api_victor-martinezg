package com.odigeo.frontend.monitoring;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResolverContextInitializer;
import java.util.Map;
import java.util.Objects;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/service/metrics")
public class MetricsController {

    @Inject
    private ResolverContextInitializer contextInitializer;
    @Inject
    private MeasureConstants measureConstants;
    @Inject
    private Logger logger;

    @POST
    @Path("/trackTime")
    @Consumes(MediaType.APPLICATION_JSON)
    public void trackTime(TrackTimeRequest request, @Context HttpServletRequest servletRequest) {
        String metricName = request.getMetricName();
        long time = request.getTime();

        Measure measure = measureConstants.getMeasure(metricName);
        ResolverContext context = contextInitializer.createContext(servletRequest, new DebugInfo());
        Map<Class<?>, Object> contextMap = contextInitializer.createContextMap(servletRequest);

        if (Objects.nonNull(measure) && time > 0) {
            MetricsHandler metricsHandler = (MetricsHandler) contextMap.get(MetricsHandler.class);
            metricsHandler.trackTime(measure, time, request.getDirectTags(), context.getVisitInformation());
        } else {
            logger.error(this.getClass(), "Metric with an invalid value. Time: " + time + ", name: " + metricName);
        }
    }

    @POST
    @Path("/trackCounter")
    @Consumes(MediaType.APPLICATION_JSON)
    public void trackCounter(TrackCounterRequest request, @Context HttpServletRequest servletRequest) {
        String metricName = request.getMetricName();

        Measure measure = measureConstants.getMeasure(metricName);
        ResolverContext context = contextInitializer.createContext(servletRequest, new DebugInfo());
        Map<Class<?>, Object> contextMap = contextInitializer.createContextMap(servletRequest);

        if (Objects.nonNull(measure)) {
            MetricsHandler metricsHandler = (MetricsHandler) contextMap.get(MetricsHandler.class);
            metricsHandler.trackCounter(measure, request.getDirectTags(), context.getVisitInformation());
        } else {
            logger.error(this.getClass(), "Metric with an invalid name: " + metricName);
        }
    }

}
