package com.odigeo.frontend.logs;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.graphql.GraphQLRequest;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.MDC;

@Singleton
public class LogDetailsTracer {

    private static final String REFERER = "referer";

    private static final String CLIENT_USER_AGENT = "client.user_agent";
    private static final String CLIENT_IP = "client.ip";
    private static final String CLIENT_TEST_TOKEN_SET = "client.test_token_set";
    private static final String CLIENT_HEADERS = "client.headers";

    private static final String REQUEST_QUERY = "request.query";
    private static final String REQUEST_VARIABLES = "request.variables";

    private static final String VISIT_CODE = "visit.code";
    private static final String VISIT_DEVICE = "visit.device";
    private static final String VISIT_BROWSER = "visit.browser";
    private static final String VISIT_BRAND = "visit.brand";
    private static final String VISIT_USER_DEVICE = "visit.user_device";

    private static final String INIT_BRACKET = "[";
    private static final String END_BRACKET = "]";

    private static final String UNKNOWN = "unknown";

    @Inject
    private JsonUtils jsonUtils;

    public void initTracer(HttpServletRequest servletRequest, GraphQLRequest graphqlRequest, ResolverContext context) {
        RequestInfo requestInfo = context.getRequestInfo();

        addMdcEntry(REFERER, requestInfo.getHeader(SiteHeaders.REFERER));

        addMdcEntry(CLIENT_USER_AGENT, requestInfo.getHeader(SiteHeaders.USER_AGENT));
        addMdcEntry(CLIENT_IP, servletRequest.getRemoteAddr());
        addMdcEntry(CLIENT_TEST_TOKEN_SET, requestInfo.getCookieValue(SiteCookies.TEST_TOKEN_SPACE));
        addMdcEntry(CLIENT_HEADERS, enumToString(servletRequest.getHeaderNames()));

        addMdcEntry(REQUEST_QUERY, StringUtils.normalizeSpace(graphqlRequest.getQuery()));
        addMdcEntry(REQUEST_VARIABLES, jsonUtils.toJson(graphqlRequest.getVariables()));

        addVisitInfo(context);
    }

    private void addVisitInfo(ResolverContext context) {
        String visitCode;

        try {
            VisitInformation visit = context.getVisitInformation();
            visitCode = visit.getVisitCode();

            addMdcEntry(VISIT_DEVICE, visit.getDevice().name());
            addMdcEntry(VISIT_BROWSER, visit.getBrowser().name());
            addMdcEntry(VISIT_BRAND, visit.getBrand().name());
            addMdcEntry(VISIT_USER_DEVICE, visit.getUserDevice());

        } catch (ServiceException ex) {
            visitCode = context.getRequestInfo()
                .getHeaderOrCookie(SiteHeaders.VISIT_INFORMATION, SiteCookies.VISIT_INFORMATION);
        }

        addMdcEntry(VISIT_CODE, visitCode);
    }

    private String enumToString(Enumeration<String> values) {
        StringBuilder builder = new StringBuilder();
        Enumeration<String> valuesNotNull = Optional.ofNullable(values).orElse(Collections.emptyEnumeration());

        while (valuesNotNull.hasMoreElements()) {
            String value = valuesNotNull.nextElement();
            builder.append(INIT_BRACKET).append(value).append(END_BRACKET);
        }

        return builder.toString();
    }

    private void addMdcEntry(String key, String value) {
        MDC.put(key, StringUtils.defaultString(value, UNKNOWN));
    }

    public void endTracer() {
        MDC.clear();
    }

}
