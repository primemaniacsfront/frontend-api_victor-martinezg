package com.odigeo.frontend.monitoring;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import org.apache.commons.lang3.StringUtils;

public enum MeasureTags {
    BRAND,
    BROWSER,
    DEVICE,
    OS,
    WEBSITE,
    CODE,
    SERVICE,
    QUERY;

    public String getLabel() {
        return StringUtils.lowerCase(name());
    }

    String getValue(VisitInformation visit) {
        switch (this) {
        case BRAND: return visit.getBrand().name();
        case BROWSER: return visit.getBrowser().name();
        case DEVICE: return visit.getWebInterface().name();
        case OS: return visit.getOs().name();
        case WEBSITE: return visit.getSite().name();
        default: return null;
        }
    }

}
