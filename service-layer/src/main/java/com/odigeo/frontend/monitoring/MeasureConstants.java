package com.odigeo.frontend.monitoring;

import com.google.inject.Singleton;
import com.odigeo.commons.monitoring.metrics.FixedWidthBuckets;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.reflect.FieldUtils;

@Singleton
public class MeasureConstants {

    //OF2 METRICS
    public static final Measure SEARCH_BACKEND_TOTAL_TIME = composeBackendMetric("search_backend_total_time");
    public static final Measure SEARCH_SEND_TO_ONEFRONT = composeBackendMetric("search_send_to_onefront");
    public static final Measure SEARCH_ACCOMMODATION_BACKEND_TOTAL_TIME = composeBackendMetric("search_accommodation_backend_total_time");
    public static final Measure SEARCH_STATIC_CONTENT_BACKEND_TOTAL_TIME = composeBackendMetric("search_static_content_backend_total_time");
    public static final Measure SEARCH_AIRLINE_STEERING_TOTAL_TIME = composeBackendMetric("search_airline_steering_total_time");
    public static final Measure SEARCH_STATIC_FOOTPRINT = composeBackendMetric("response_includes_carbon_footprints_counter");
    public static final Measure SEARCH_ERROR_COUNTER = composeSearchErrorMetric("search_error_counter");
    public static final Measure GENERIC_ERROR_COUNTER = composeGenericErrorMetric("generic_error_counter");
    public static final Measure SEARCH_WARNING_COUNTER = composeGenericErrorMetric("search_warning_counter");

    //PRIME METRICS
    public static final Measure MEMBERSHIP_UPDATE_ENABLE_RENEWAL_STATUS = composeGenericErrorMetric("membership_update_enable_renewal_status");
    public static final Measure MEMBERSHIP_UPDATE_DISABLE_RENEWAL_STATUS = composeGenericErrorMetric("membership_update_disable_renewal_status");
    public static final Measure MEMBERSHIP_UPDATE_SET_REMINDER_LATER = composeGenericErrorMetric("membership_update_set_reminder_later");
    public static final Measure MEMBERSHIP_UPDATE_FAILED = composeGenericErrorMetric("membership_update_error");
    public static final Measure MEMBERSHIP_GET_CURRENT_MEMBERSHIP = composeGenericErrorMetric("membership_get_current_membership");
    public static final Measure MOS_CREATE_OFFER = composeGenericErrorMetric("mos_create_offer");
    public static final Measure MOS_CREATE_OFFERS = composeGenericErrorMetric("mos_create_offers");
    public static final Measure MOS_GET_OFFER = composeGenericErrorMetric("mos_get_offer");
    public static final Measure MEMBERSHIP_USER_AREA_GET_FUTURE_FLIGHTS = composeGenericErrorMetric("membership_user_area_get_future_flights");
    public static final Measure MEMBERSHIP_PAGE_RECAPTURE = composeGenericErrorMetric("membership_page_recapture");
    public static final Measure MEMBERSHIP_PAGE_SUBSCRIPTION = composeGenericErrorMetric("membership_page_subscription");
    public static final Measure MEMBERSHIP_PAGE_SUBSCRIPTION_TIERS = composeGenericErrorMetric("membership_page_subscription_tiers");
    public static final Measure MEMBERSHIP_PAGE_SUBSCRIPTION_FTL = composeGenericErrorMetric("membership_page_subscription_ftl");
    public static final Measure MEMBERSHIP_IS_ELIGIBLE_FREE_TRIAL = composeGenericErrorMetric("membership_is_eligible_free_trial");
    public static final Measure MEMBERSHIP_DECRYPT_TOKEN = composeGenericErrorMetric("membership_decrypt_token");
    public static final Measure MEMBERSHIP_FREE_TRIAL_ABUSER = composeGenericErrorMetric("membership_free_trial_abuser");
    public static final Measure MEMBERSHIP_FREE_TRIAL_ABUSER_FAILED = composeGenericErrorMetric("membership_free_trial_abuser_validation_error");

    private static final Map<String, Measure> MEASURE_MAP = new HashMap<>();
    public MeasureConstants() throws IllegalAccessException {
        for (Field field : FieldUtils.getAllFieldsList(this.getClass())) {
            Object fieldValue = FieldUtils.readStaticField(field, true);
            if (fieldValue instanceof Measure) {
                MEASURE_MAP.put(field.getName(), (Measure) fieldValue);
            }
        }
    }

    public Measure getMeasure(String metricName) {
        return MEASURE_MAP.get(metricName);
    }

    private static Measure composeBackendMetric(String name) {
        return new Measure.Builder(name)
            .tag(MeasureTags.WEBSITE)
            .tag(MeasureTags.DEVICE)
            .tag(MeasureTags.BROWSER)
            .buckets(FixedWidthBuckets.FIXED_WIDTH_2000)
            .build();
    }

    private static Measure composeGenericErrorMetric(String name) {
        return new Measure.Builder(name)
                .tag(MeasureTags.QUERY)
                .tag(MeasureTags.SERVICE)
                .tag(MeasureTags.CODE)
                .buckets(FixedWidthBuckets.FIXED_WIDTH_2000)
                .build();
    }

    private static Measure composeSearchErrorMetric(String name) {
        return new Measure.Builder(name)
                .tag(MeasureTags.CODE)
                .buckets(FixedWidthBuckets.FIXED_WIDTH_2000)
                .build();
    }
}
