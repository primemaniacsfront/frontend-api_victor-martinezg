package com.odigeo.frontend.monitoring.buckets;

public enum FixedBuckets implements AbstractFixedBuckets {

    FIXED_BUCKETS_EXAMPLE {
        public BucketCalculator getBucketCalculator() {
            return FIXED_BUCKETS_EXAMPLE_CALCULATOR;
        }
    };

    static final BucketCalculator FIXED_BUCKETS_EXAMPLE_CALCULATOR = new BucketCalculator(FixedBucketsExample.values());

}
