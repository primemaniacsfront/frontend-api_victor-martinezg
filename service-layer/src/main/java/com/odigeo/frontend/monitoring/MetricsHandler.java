package com.odigeo.frontend.monitoring;

import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MetricsHandler {

    public static final String REGISTRY_NAME = "frontend-api";

    private static final Logger logger = LoggerFactory.getLogger(MetricsHandler.class.getName());
    private final Map<Measure, Long> initTimes = new HashMap<>();

    public void trackTime(Measure measure, long time, VisitInformation visit) {
        trackTime(measure, time, Collections.emptyMap(), visit);
    }

    public void trackTime(Measure measure, long time, Map<String, String> directTags, VisitInformation visit) {
        if (time > 0) {
            Metric metric = measure.getMetric(visit, directTags);
            addValueToHistogram(metric, time);
        } else {
            logger.warn("Metric with an invalid value. Time: " + time + ", constant: " + measure.getName());
        }
    }

    public void startMetric(Measure measure) {
        initTimes.put(measure, System.currentTimeMillis());
    }

    public void stopMetric(Measure measure, VisitInformation visit) {
        stopMetric(measure, Collections.emptyMap(), visit);
    }

    public void stopMetric(Measure measure, Map<String, String> directTags, VisitInformation visit) {
        Long initTime = initTimes.get(measure);

        if (initTime != null) {
            trackTime(measure, System.currentTimeMillis() - initTime, directTags, visit);
        } else {
            logger.warn("Metric stopped before starting. Constant: " + measure.getName());
        }
    }

    public void trackCounter(Measure measure, VisitInformation visit) {
        trackCounter(measure, Collections.emptyMap(), visit);
    }

    public void trackCounter(Measure measure) {
        Metric metric = measure.getMetric(Collections.emptyMap());
        incrementCounter(metric);
    }

    public void trackCounter(Measure measure, Map<String, String> directTags, VisitInformation visit) {
        Metric metric = measure.getMetric(visit, directTags);
        incrementCounter(metric);
    }

    public void trackCounter(Measure measure, Map<String, String> directTags) {
        Metric metric = measure.getMetric(directTags);
        incrementCounter(metric);
    }

    void addValueToHistogram(Metric metric, long time) {
        MetricsUtils.addValueToHistogram(metric, REGISTRY_NAME, time);
    }

    void incrementCounter(Metric metric) {
        MetricsUtils.incrementCounter(metric, REGISTRY_NAME);
    }

}
