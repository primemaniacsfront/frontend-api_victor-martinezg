package com.odigeo.frontend.monitoring.buckets;

import com.google.common.collect.Sets;
import com.odigeo.commons.monitoring.metrics.Buckets;
import com.odigeo.commons.monitoring.metrics.bucket.Bucket;
import java.util.Set;

public interface AbstractFixedBuckets extends Buckets {

    BucketCalculator getBucketCalculator();

    default String getBucketName(long value) {
        return getBucketCalculator().calculate(value);
    }

    default Set<Bucket> getBucketSet() {
        return Sets.newHashSet(getBucketCalculator().getBuckets());
    }

}
