package com.odigeo.frontend.monitoring;

import com.odigeo.commons.monitoring.metrics.Buckets;
import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.frontend.commons.context.visit.VisitInformation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("PMD.MissingStaticMethodInNonInstantiatableClass")
public class Measure {

    private final String name;
    private final List<MeasureTags> tags;
    private final Buckets buckets;

    private Measure(String name, List<MeasureTags> tags, Buckets buckets) {
        this.name = name;
        this.tags = tags;
        this.buckets = buckets;
    }

    static class Builder {
        private final String name;
        private final List<MeasureTags> tags;
        private Buckets buckets;

        Builder(String name) {
            this.name = name;
            this.tags = new ArrayList<>();
        }

        Builder tag(MeasureTags tag) {
            tags.add(tag);
            return this;
        }

        Builder buckets(Buckets buckets) {
            this.buckets = buckets;
            return this;
        }

        @SuppressWarnings("PMD.AccessorClassGeneration")
        Measure build() {
            return new Measure(name, tags, buckets);
        }
    }

    Metric getMetric(VisitInformation visit, Map<String, String> directValues) {
        Metric.Builder builder = new Metric.Builder(name);

        tags.forEach(tag -> builder.tag(tag.getLabel(), tag.getValue(visit)));

        return builder.tag(directValues)
            .buckets(buckets)
            .build();
    }

    Metric getMetric(Map<String, String> tagValues) {
        Metric.Builder builder = new Metric.Builder(name);
        return builder.tag(tagValues)
                .buckets(buckets)
                .build();
    }

    String getName() {
        return name;
    }

}
