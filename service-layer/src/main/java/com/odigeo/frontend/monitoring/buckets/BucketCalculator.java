package com.odigeo.frontend.monitoring.buckets;

import com.odigeo.commons.monitoring.metrics.bucket.Bucket;
import org.apache.commons.math3.util.ArithmeticUtils;

class BucketCalculator {
    private int gcd;
    private final int[] bucketArray;
    private final Bucket[] buckets;

    BucketCalculator(Bucket... buckets) {
        int[] bucketValues = new int[buckets.length];

        for (int i = 0; i < buckets.length; i++) {
            int value = Integer.parseInt(buckets[i].getName().split("[a-z]+")[0]);
            bucketValues[i] = value;
            gcd = ArithmeticUtils.gcd(gcd, value);
        }

        int maxLimit = bucketValues[bucketValues.length - 1];
        int bucketArrayLength = maxLimit / gcd;

        if (bucketArrayLength > 100) {
            throw new IllegalArgumentException();
        }

        bucketArray = new int[bucketArrayLength];
        this.buckets = buckets;

        for (int limitIndex = 0, bucketIndex = 0, bucketArrayIndex = 0; limitIndex < maxLimit; limitIndex += gcd) {
            if (limitIndex >= bucketValues[bucketIndex + 1]) {
                bucketIndex++;
            }
            bucketArray[bucketArrayIndex] = bucketIndex;
            bucketArrayIndex++;
        }
    }

    public String calculate(long value) {
        int index = (int) ((value) / (long) gcd);

        return index >= bucketArray.length || index < 0
            ? buckets[buckets.length - 1].getName()
            : buckets[bucketArray[index]].getName();
    }

    public Bucket[] getBuckets() {
        return buckets;
    }

}
