package com.odigeo.frontend.monitoring.buckets;

import com.odigeo.commons.monitoring.metrics.bucket.Bucket;

public enum FixedBucketsExample implements Bucket {
    FROM_0_TO_1999("0to1999", "1999"),
    FROM_2000_TO_3999("2000to3999", "3999"),
    FROM_4000_TO_INF("4000orMore", "+Inf");

    private final String name;
    private final String upperBound;

    FixedBucketsExample(String name, String upperBound) {
        this.name = name;
        this.upperBound = upperBound;
    }

    public String getName() {
        return this.name;
    }

    public String getUpperBound() {
        return this.upperBound;
    }
}
