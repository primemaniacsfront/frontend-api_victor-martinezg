package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.google.common.collect.ArrayListMultimap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationImageQualityDTO;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class AccommodationImageHandler {

    private static final Comparator<AccommodationImageDTO> IMAGE_QUALITY_COMPARATOR = new ImageQualityComparator();
    private static final Comparator<AccommodationImageDTO> IMAGE_NAME_COMPARATOR = new ImageNameComparator();
    private static final String INVALID_IMAGE_S_URL = "Invalid image url";

    @Inject
    private Logger logger;

    public void removeDuplicatedImages(AccommodationDetailDTO accommodationDetail) {
        ArrayListMultimap<String, AccommodationImageDTO> imagesMap = groupImagesPerName(accommodationDetail);
        AccommodationDealInformationDTO accommodationDealInformation = accommodationDetail.getAccommodationDealInformation();
        boolean hasImage = hasMainAccommodationImage(accommodationDealInformation);

        List<AccommodationImageDTO> imagesWithoutDuplicates = !hasImage
                ? imagesMap.keySet().stream()
                .map(imageName -> getImageWithHigherQuality(imagesMap, imageName))
                .collect(Collectors.toList())
                : imagesMap.keySet().stream()
                .filter(imageName -> !imageName.equals(getFileName(accommodationDealInformation.getMainAccommodationImage())))
                .map(imageName -> getImageWithHigherQuality(imagesMap, imageName))
                .collect(Collectors.toList());

        Collections.sort(imagesWithoutDuplicates, IMAGE_NAME_COMPARATOR);
        if (hasImage) {
            imagesWithoutDuplicates.add(0, accommodationDealInformation.getMainAccommodationImage());
        }
        accommodationDetail.setAccommodationImages(imagesWithoutDuplicates);
    }

    private ArrayListMultimap<String, AccommodationImageDTO> groupImagesPerName(AccommodationDetailDTO accommodationDetail) {
        ArrayListMultimap<String, AccommodationImageDTO> imagesMap = ArrayListMultimap.create();
        for (AccommodationImageDTO image : accommodationDetail.getAccommodationImages()) {
            String fileName = getFileName(image);
            if (StringUtils.isNotEmpty(fileName)) {
                imagesMap.put(fileName, image);
            }
        }
        return imagesMap;
    }

    private String getFileName(AccommodationImageDTO image) {
        try {
            URL url = new URL(image.getUrl());
            return url.getFile();
        } catch (MalformedURLException e) {
            logger.warning(this.getClass(), INVALID_IMAGE_S_URL + image.getUrl(), e);
            return StringUtils.EMPTY;
        }
    }

    private boolean hasMainAccommodationImage(AccommodationDealInformationDTO accommodationDealInformation) {
        return accommodationDealInformation != null && accommodationDealInformation.getMainAccommodationImage() != null;
    }

    private AccommodationImageDTO getImageWithHigherQuality(ArrayListMultimap<String, AccommodationImageDTO> imagesMap, String imageName) {
        List<AccommodationImageDTO> images = imagesMap.get(imageName);
        Collections.sort(images, IMAGE_QUALITY_COMPARATOR);
        return images.get(0);
    }


    private static class ImageQualityComparator implements Comparator<AccommodationImageDTO>, Serializable {
        @Override
        public int compare(AccommodationImageDTO o1, AccommodationImageDTO o2) {
            if (o1 != null && o2 != null) {
                int o1Quality = getQualityValue(o1);
                int o2Quality = getQualityValue(o2);
                return Integer.compare(Integer.valueOf(o2Quality), o1Quality);
            }
            return 0;
        }

        private int getQualityValue(AccommodationImageDTO image) {
            int qualityValue = image.getQuality().ordinal();
            if (image.getQuality() == AccommodationImageQualityDTO.UNKNOWN) {
                qualityValue = -1;
            }
            return qualityValue;
        }
    }

    private static class ImageNameComparator implements Comparator<AccommodationImageDTO>, Serializable {
        @Override
        public int compare(AccommodationImageDTO o1, AccommodationImageDTO o2) {
            if (o1 != null && o2 != null) {
                String o1Url = o1.getUrl();
                String o2Url = o2.getUrl();
                return o1Url.compareTo(o2Url);
            }
            return 0;
        }
    }

}
