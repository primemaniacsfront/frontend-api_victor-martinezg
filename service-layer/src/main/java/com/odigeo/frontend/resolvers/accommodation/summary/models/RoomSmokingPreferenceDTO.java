package com.odigeo.frontend.resolvers.accommodation.summary.models;

public enum RoomSmokingPreferenceDTO {
    UNKNOWN,
    INDIFFERENT,
    NON_SMOKING,
    SMOKING;

    public String value() {
        return this.name();
    }

}

