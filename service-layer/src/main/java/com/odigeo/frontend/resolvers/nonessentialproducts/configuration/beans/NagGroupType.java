package com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans;

public enum NagGroupType {
    CANX, INSURANCE, REBOOKING, SERVICE_OPTIONS
}
