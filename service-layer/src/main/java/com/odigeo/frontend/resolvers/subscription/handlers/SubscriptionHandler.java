package com.odigeo.frontend.resolvers.subscription.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionFdoResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.handlers.alert.PointOfSaleMapper;
import com.odigeo.frontend.services.CrmSubscriptionService;
import com.odigeo.marketing.subscriptions.rest.v1.parameters.FdoSubscription;

import java.time.LocalDateTime;

@Singleton
public class SubscriptionHandler {

    private static final String SUBSCRIPTION_KEY_FORMAT = "%s|%s";
    @Inject
    private CrmSubscriptionService crmSubscriptionService;
    @Inject
    private PointOfSaleMapper posWebsiteMapper;

    public SubscriptionFdoResponse subscribeToFdo(String email, ResolverContext context) {
        crmSubscriptionService.subscribeToFdo(mapToFdoSubscription(email, context));
        return new SubscriptionFdoResponse(email);
    }

    private FdoSubscription mapToFdoSubscription(String email, ResolverContext context) {
        VisitInformation visit = context.getVisitInformation();
        return new FdoSubscription.Builder()
                .withEmail(email)
                .withSubscriptionKey(buildSubscriptionKey(email, visit))
                .withBrand(visit.getBrand().name())
                .withClientInterface(visit.getWebInterface().name())
                .withIp(visit.getUserIp())
                .withCreationDate(LocalDateTime.now())
                .withMarket(visit.getSite().name())
                .withUserDeviceId(visit.getUserDevice())
                .withVisitId(visit.getVisitId()).build();
    }

    private String buildSubscriptionKey(String email, VisitInformation visit) {
        return String.format(SUBSCRIPTION_KEY_FORMAT, email, posWebsiteMapper.map(visit));
    }
}
