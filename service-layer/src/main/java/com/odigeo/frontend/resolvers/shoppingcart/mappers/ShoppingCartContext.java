package com.odigeo.frontend.resolvers.shoppingcart.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.odigeo.dapi.client.Itineraries;
import com.odigeo.dapi.client.ItinerariesLegend;
import com.odigeo.dapi.client.ItineraryProvider;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.itinerary.mappers.section.SectionMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.carrier.CarrierMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.location.LocationMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.factory.Mappers;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toMap;

@SuppressFBWarnings("UWF_FIELD_NOT_INITIALIZED_IN_CONSTRUCTOR")
public class ShoppingCartContext {

    private final CarrierMapper carrierMapper;
    private final SectionMapper sectionMapper;
    private final LocationMapper locationMapper;
    private final VisitInformation visit;

    private Map<Integer, Carrier> carrierMap;
    private Map<Integer, Location> locationMap;
    private Map<Integer, Section> sectionMap;

    public ShoppingCartContext(VisitInformation visit) {
        carrierMapper = Mappers.getMapper(CarrierMapper.class);
        sectionMapper = Mappers.getMapper(SectionMapper.class);
        locationMapper = Mappers.getMapper(LocationMapper.class);
        this.visit = visit;
    }

    @BeforeMapping
    public void fillMaps(com.odigeo.dapi.client.Itinerary itinerary) {
        carrierMap = Optional.ofNullable(itinerary)
            .map(com.odigeo.dapi.client.Itinerary::getLegend)
            .map(ItinerariesLegend::getCarriers)
            .orElse(Collections.emptyList())
            .stream()
            .collect(toMap(com.odigeo.dapi.client.Carrier::getId, carrier -> carrierMapper.carrierContractToModel(carrier)));

        locationMap = Optional.ofNullable(itinerary)
            .map(com.odigeo.dapi.client.Itinerary::getLegend)
            .map(ItinerariesLegend::getLocations)
            .orElse(Collections.emptyList())
            .stream()
            .collect(toMap(com.odigeo.dapi.client.Location::getGeoNodeId, location -> locationMapper.locationContractToModel(location)));

        List<ItineraryProvider> providerList = Optional.ofNullable(itinerary)
            .map(com.odigeo.dapi.client.Itinerary::getLegend)
            .map(ItinerariesLegend::getItineraries)
            .map(Itineraries::getSimples)
            .orElse(Collections.emptyList());
        Map<Integer, String> itineraryProvider = mapItineraryProviderAllSections(providerList);

        sectionMap = Optional.ofNullable(itinerary)
            .map(com.odigeo.dapi.client.Itinerary::getLegend)
            .map(ItinerariesLegend::getSectionResults)
            .orElse(Collections.emptyList())
            .stream()
            .collect(toMap(com.odigeo.dapi.client.SectionResult::getId,
                section -> sectionMapper.sectionContractToModel(section, itineraryProvider.get(section.getId()), this)));
    }

    @AfterMapping
    public Carrier getCarrierFromId(Integer id) {
        return carrierMap.get(id);
    }

    @AfterMapping
    public Location getLocationFromId(Integer id) {
        return locationMap.get(id);
    }

    @AfterMapping
    public Section getSectionFromId(Integer id) {
        Section section = sectionMap.get(id);
        String flightCode = section.getCarrier().getId().concat(section.getFlightNumber());
        section.setFlightCode(flightCode);
        return section;
    }

    private Map<Integer, String> mapItineraryProviderAllSections(List<ItineraryProvider> providerList) {
        Map<Integer, String> providers = new HashMap<>(providerList.size());
        providerList.stream().forEach(itinProvider -> {
            itinProvider.getSections().stream().forEach(secId -> providers.put(secId, itinProvider.getProvider()));
        });
        return providers;
    }

    public VisitInformation getVisit() {
        return visit;
    }

    public String getWebSiteCode() {
        return Optional.ofNullable(visit)
            .map(VisitInformation::getSite)
            .map(Enum::toString)
            .orElse(null);
    }
}
