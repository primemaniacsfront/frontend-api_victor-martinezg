package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.odigeo.frontend.resolvers.prime.utils.MembershipUtils;
import com.odigeo.membership.response.search.MembershipResponse;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper
public interface MembershipMapper {

    @Mapping(source = "membershipType", target = "type")
    @Mapping(source = "totalPrice", target = "totalPrice.amount")
    @Mapping(source = "currencyCode", target = "totalPrice.currency")
    @Mapping(source = "duration", target = "duration.amount")
    @Mapping(source = "durationTimeUnit", target = "duration.timeUnit")
    @Mapping(source = "renewalPrice", target = "renewalPrice.amount")
    @Mapping(source = "currencyCode", target = "renewalPrice.currency")
    @Mapping(source = "autoRenewal", target = "autoRenewalStatus")
    @Mapping(source = "memberAccount.lastNames", target = "lastNames")
    @Mapping(source = "memberAccount.name", target = "name")
    Membership membershipContractToModel(MembershipResponse membership);

    @AfterMapping
    default void after(@MappingTarget Membership membership) {
        membership.setIsFreeTrial(MembershipUtils.isFreeTrial(membership));
    }
}
