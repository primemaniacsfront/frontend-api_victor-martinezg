package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentToken;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCards;
import com.google.inject.Inject;
import com.odigeo.frontend.services.PaymentInstrumentService;
import com.odigeo.userprofiles.api.v2.model.CreditCard;

public abstract class CreditCardsMapperDecorator implements CreditCardsMapper {

    @Inject
    private PaymentInstrumentService paymentInstrumentService;

    private final CreditCardsMapper delegate;

    public CreditCardsMapperDecorator(CreditCardsMapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public CreditCards creditCardsContractToModel(CreditCard creditCard) {
        CreditCards creditCards = delegate.creditCardsContractToModel(creditCard);
        if (creditCards != null) {
            PaymentInstrumentToken tokenWithoutCvv = paymentInstrumentService.tokenize(creditCard.getCreditCardNumber(),
                    creditCard.getOwner(), creditCard.getExpirationDateMonth(), creditCard.getExpirationDateYear());
            creditCards.setPaymentInstrumentTokenWithoutCvv(tokenWithoutCvv.getUuid().toString());
        }
        return creditCards;
    }
}
