package com.odigeo.frontend.resolvers.search.models;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelection;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchCode;

import java.util.List;
import java.util.Set;

public class SearchResponseDTO {

    private Long searchId;
    private SearchCode searchCode;
    private List<SearchItineraryDTO> itineraries;
    private String currencyCode;
    private FeeType defaultFeeType;
    private Boolean isRebookingAvailable;
    private Set<String> carriersWithRebooking;
    private final TrackingInfoDTO trackingInfo;
    private ExternalSelection externalSelection;

    public SearchResponseDTO() {
        trackingInfo = new TrackingInfoDTO();
    }

    public Long getSearchId() {
        return searchId;
    }

    public void setSearchId(Long searchId) {
        this.searchId = searchId;
    }

    public SearchCode getSearchCode() {
        return searchCode;
    }

    public void setSearchCode(SearchCode searchCode) {
        this.searchCode = searchCode;
    }

    public List<SearchItineraryDTO> getItineraries() {
        return itineraries;
    }

    public void setItineraries(List<SearchItineraryDTO> itineraries) {
        this.itineraries = itineraries;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public FeeType getDefaultFeeType() {
        return defaultFeeType;
    }

    public void setDefaultFeeType(FeeType defaultFeeType) {
        this.defaultFeeType = defaultFeeType;
    }

    public Boolean getIsRebookingAvailable() {
        return isRebookingAvailable;
    }

    public void setIsRebookingAvailable(Boolean isRebookingAvailable) {
        this.isRebookingAvailable = isRebookingAvailable;
    }

    public Set<String> getCarriersWithRebooking() {
        return carriersWithRebooking;
    }

    public void setCarriersWithRebooking(Set<String> carriersWithRebooking) {
        this.carriersWithRebooking = carriersWithRebooking;
    }

    public TrackingInfoDTO getTrackingInfo() {
        return trackingInfo;
    }

    public ExternalSelection getExternalSelection() {
        return externalSelection;
    }

    public void setExternalSelection(ExternalSelection externalSelection) {
        this.externalSelection = externalSelection;
    }

}
