package com.odigeo.frontend.resolvers.emailsuggestions.configuration;

import com.edreams.configuration.ConfiguredInJsonFile;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
@ConfiguredInJsonFile
public class EmailSuggestionsConfiguration {

    private Map<String, List<String>> emailProvidersAutocomplete = new HashMap<>();
    private List<String> suggestEmailGenericDomains = new ArrayList<>();

    public Map<String, List<String>> getEmailProvidersAutocomplete() {
        return emailProvidersAutocomplete;
    }

    public void setEmailProvidersAutocomplete(Map<String, List<String>> emailProvidersAutocomplete) {
        this.emailProvidersAutocomplete = emailProvidersAutocomplete;
    }

    public List<String> getSuggestEmailGenericDomains() {
        return suggestEmailGenericDomains;
    }

    public void setSuggestEmailGenericDomains(List<String> suggestEmailGenericDomains) {
        this.suggestEmailGenericDomains = suggestEmailGenericDomains;
    }
}
