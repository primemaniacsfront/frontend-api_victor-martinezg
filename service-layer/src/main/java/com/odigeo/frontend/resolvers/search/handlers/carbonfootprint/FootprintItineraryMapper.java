package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.Itinerary;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
class FootprintItineraryMapper {

    private static final int ITINERARIES_TO_CALCULATE = 300;

    @Inject
    private FootprintSegmentResultMapper footprintSegmentResultMapper;

    public List<Itinerary> map(List<SearchItineraryDTO> searchItineraryDTOList) {

        boolean isAllVIN = searchItineraryDTOList.stream().allMatch(this::isVIN);

        List<Itinerary> itineraryList = new ArrayList<>();
        for (int i = 0; i < searchItineraryDTOList.size() && itineraryList.size() < ITINERARIES_TO_CALCULATE; i++) {
            if (isAllVIN || !isVIN(searchItineraryDTOList.get(i))) {
                itineraryList.add(map(searchItineraryDTOList.get(i), i));
            }
        }
        return itineraryList;

    }

    private boolean isVIN(SearchItineraryDTO searchItineraryDTO) {
        return Boolean.TRUE.equals(searchItineraryDTO.isVin());
    }

    private Itinerary map(SearchItineraryDTO searchItineraryDTO, int index) {

        Itinerary itinerary = new Itinerary();
        itinerary.setItineraryIndex(index);
        itinerary.setSegmentResults(searchItineraryDTO.getLegs().stream()
                .map(footprintSegmentResultMapper::map).collect(Collectors.toList()));
        return itinerary;
    }

}
