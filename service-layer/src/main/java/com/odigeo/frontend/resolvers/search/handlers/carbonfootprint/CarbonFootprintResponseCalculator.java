package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.ItineraryCo2FootprintResult;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.response.ItineraryFootprintCalculatorResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CarbonFootprint;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class CarbonFootprintResponseCalculator {

    @Inject
    private CarbonFootprintHelper carbonFootprintHelper;

    public CarbonFootprintResponse calculateFootprintMap(ItineraryFootprintCalculatorResponse response) {

        return Optional.ofNullable(response)
                .map(carbonFootprintHelper::getSearchKilosAverage)
                .map(kilos -> calculateFootprintMap(response, kilos))
                .orElseGet(CarbonFootprintResponse::new);
    }

    private CarbonFootprintResponse calculateFootprintMap(ItineraryFootprintCalculatorResponse calculatorResponse,
                                                          BigDecimal searchKilosAverage) {

        Map<Integer, CarbonFootprint> itineraries = calculatorResponse.getItineraryCo2FootprintResults()
                .stream().filter(co2FootprintResult -> co2FootprintResult.getKilosCo2e() != null)
                .collect(Collectors.toMap(ItineraryCo2FootprintResult::getItineraryIndex, co2FootprintResult -> getCarbonFootprint(co2FootprintResult, searchKilosAverage)));

        return new CarbonFootprintResponse(itineraries, searchKilosAverage);
    }

    private CarbonFootprint getCarbonFootprint(ItineraryCo2FootprintResult co2FootprintResult, BigDecimal searchKilosAverage) {
        CarbonFootprint carbonFootprint = new CarbonFootprint();
        int ecoPercentage = carbonFootprintHelper.calculatePercentageEco(co2FootprintResult.getKilosCo2e(), searchKilosAverage);
        carbonFootprint.setEcoPercentageThanAverage(ecoPercentage);
        carbonFootprint.setIsEco(ecoPercentage > 0);
        carbonFootprint.setTotalCo2eKilos(co2FootprintResult.getKilosCo2e());
        carbonFootprint.setTotalCo2Kilos(co2FootprintResult.getKilosCo2());
        return carbonFootprint;
    }

}
