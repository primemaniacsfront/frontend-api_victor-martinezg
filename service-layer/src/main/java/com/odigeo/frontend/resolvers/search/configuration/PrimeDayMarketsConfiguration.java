package com.odigeo.frontend.resolvers.search.configuration;

import com.edreams.configuration.ConfiguredInJsonFile;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayFareType;
import com.odigeo.frontend.commons.context.visit.types.Site;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
@ConfiguredInJsonFile
public class PrimeDayMarketsConfiguration {

    private Map<Site, Map<String, PrimeDayFareType>> marketsCarriers = new HashMap<>();

    public void setMarketsCarriers(Map<Site, Map<String, PrimeDayFareType>> marketsCarriers) {
        this.marketsCarriers = marketsCarriers;
    }

    public Map<Site, Map<String, PrimeDayFareType>> getMarketsCarriers() {
        return marketsCarriers;
    }
}
