package com.odigeo.frontend.resolvers.insurance.mappers;

import com.google.inject.Singleton;
import com.odigeo.insurance.api.last.request.PostBookingOfferRequest;

@Singleton
public class PostBookingOfferRequestMapper {

    public PostBookingOfferRequest map(long bookingId, String visitInformation) {
        PostBookingOfferRequest postBookingOfferRequest = new PostBookingOfferRequest();

        postBookingOfferRequest.setBookingId(String.valueOf(bookingId));
        postBookingOfferRequest.setVisitInformation(visitInformation);

        return postBookingOfferRequest;
    }
}
