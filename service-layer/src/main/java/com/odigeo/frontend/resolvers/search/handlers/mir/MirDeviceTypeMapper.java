package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.DeviceType;

@Singleton
public class MirDeviceTypeMapper {

    public DeviceType map(VisitInformation visit) {
        switch(visit.getDevice()) {
        case DESKTOP: return DeviceType.COMPUTER;
        case MOBILE: return DeviceType.SMARTPHONE;
        case TABLET: return DeviceType.TABLET;
        default: return DeviceType.OTHER;
        }
    }

}
