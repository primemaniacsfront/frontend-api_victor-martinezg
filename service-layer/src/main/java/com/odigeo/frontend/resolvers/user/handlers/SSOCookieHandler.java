package com.odigeo.frontend.resolvers.user.handlers;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.userprofiles.api.v2.model.SSOToken;

import java.util.Optional;

public class SSOCookieHandler {
    private static final int MAX_EXPIRATION_AGE_SSO_TOKEN_COOKIE = 60 * 60;
    private static final int MAX_EXPIRATION_AGE_SSO_TOKEN_PERSISTENT_LOGIN_COOKIE = 60 * 60 * 24 * 180;

    public static void createSSOCookie(ResolverContext context, SSOToken ssoToken, Boolean persistentLogin) {
        Optional.ofNullable(context)
                .map(ResolverContext::getResponseInfo)
                .ifPresent(responseInfo -> responseInfo.addCookie(SiteCookies.SSO_TOKEN, ssoToken.getToken(),
                        persistentLogin ? MAX_EXPIRATION_AGE_SSO_TOKEN_PERSISTENT_LOGIN_COOKIE : MAX_EXPIRATION_AGE_SSO_TOKEN_COOKIE));
    }
}
