package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSummaryRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.mappers.RoomDealSummaryMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.RoomDealSummaryDTO;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class RoomDealSummaryHandler {

    @Inject
    private SearchService searchService;
    @Inject
    private RoomDealSummaryMapper roomDealSummaryMapper;

    public List<RoomDealSummaryDTO> getRoomsDealsByRoomKey(AccommodationSummaryRequest requestDTO, AccommodationDetailDTO accommodationDetail, VisitInformation visitInformation) {
        SearchRoomResponse searchRoomResponse = searchService.getRoomsAvailability(requestDTO.getSearchId(), Long.parseLong(requestDTO.getAccommodationDealKey()), visitInformation.getVisitCode());

        Set<String> roomKeys = new HashSet<>(requestDTO.getRoomKeys());
        boolean payAtDestination = Optional.ofNullable(accommodationDetail)
            .map(AccommodationDetailDTO::getAccommodationDealInformation)
            .map(AccommodationDealInformationDTO::isPaymentAtDestination)
            .orElse(false);

        Map<String, RoomDealSummaryDTO> roomDealSummaryMap = searchRoomResponse.getRoomDealLegend().stream()
            .filter(roomDeal -> roomKeys.contains(roomDeal.getKey()))
            .map(roomDeal -> roomDealSummaryMapper.map(roomDeal, payAtDestination))
            .collect(Collectors.toMap(RoomDealSummaryDTO::getRoomKey, room -> room, (roomOne, roomTwo) -> roomOne));

        return requestDTO.getRoomKeys().stream()
            .map(key -> roomDealSummaryMap.get(key))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }
}
