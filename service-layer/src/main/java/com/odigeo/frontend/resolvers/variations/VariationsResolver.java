package com.odigeo.frontend.resolvers.variations;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.services.TestTokenService;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;
import java.util.List;

@Singleton
public class VariationsResolver implements Resolver {

    private static final String VARIATIONS_QUERY = "variations";

    @Inject
    private TestTokenService testTokenService;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type(TypeRuntimeWiring.newTypeWiring("Query")
                .dataFetcher(VARIATIONS_QUERY, this::variationsDataFetcher));
    }

    protected List<Integer> variationsDataFetcher(DataFetchingEnvironment env) {
        List<String> aliases = jsonUtils.fromDataFetching(env, "aliases");
        ResolverContext context = env.getContext();

        return testTokenService.findMultiple(aliases, context.getVisitInformation());
    }

}
