package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.insuranceproduct.api.last.request.SelectRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectInsuranceOfferRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class SelectRequestMapper {

    @Inject
    private OfferIdMapper offerIdMapper;

    @Inject
    private ShoppingBasketIdMapper shoppingBasketIdMapper;

    public SelectRequest map(SelectInsuranceOfferRequest selectInsuranceOfferRequest) {
        SelectRequest selectRequest = new SelectRequest();
        selectRequest.setOfferId(offerIdMapper.map(selectInsuranceOfferRequest.getOfferId()));
        selectRequest.setShoppingBasketId(shoppingBasketIdMapper.map(selectInsuranceOfferRequest.getShoppingBasketId()));
        return selectRequest;
    }
}
