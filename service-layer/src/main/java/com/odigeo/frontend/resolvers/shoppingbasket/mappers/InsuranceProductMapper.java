package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceProduct;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.shoppingbasket.v2.model.Product;

@Singleton
public class InsuranceProductMapper {

    @Inject
    private MoneyMapper moneyMapper;

    public InsuranceProduct map(Product product) {
        InsuranceProduct insuranceProduct = new InsuranceProduct();

        insuranceProduct.setProductId(product.getId());
        insuranceProduct.setPrice(moneyMapper.map(product.getSellingPrice()));

        return insuranceProduct;
    }

}
