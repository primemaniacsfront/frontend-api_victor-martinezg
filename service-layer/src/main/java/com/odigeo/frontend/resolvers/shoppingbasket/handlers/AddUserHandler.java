package com.odigeo.frontend.resolvers.shoppingbasket.handlers;

import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.userprofiles.api.v2.model.User;

public interface AddUserHandler {
    Boolean addUser(User user, ResolverContext context);
}
