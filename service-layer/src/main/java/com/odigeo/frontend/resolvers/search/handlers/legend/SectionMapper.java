package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.location.LocationDTO;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.itinerary.Section;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;

import java.util.List;
import java.util.Map;

@Singleton
class SectionMapper {

    @Inject
    private DateUtils dateUtils;
    @Inject
    private TransportHandler transportHandler;

    public SectionDTO buildSection(SectionResult sectionResult, SearchEngineContext seContext) {
        Map<Integer, Carrier> carrierMap = seContext.getCarrierMap();
        Map<Integer, LocationDTO> locationMap = seContext.getLocationMap();
        Map<Integer, List<TechnicalStop>> techStopsMap = seContext.getTechStopsMap();

        Section engineSection = sectionResult.getSection();
        LocationDTO departure = locationMap.get(engineSection.getFrom());
        LocationDTO destination = locationMap.get(engineSection.getTo());

        SectionDTO sectionDTO = new SectionDTO();
        sectionDTO.setKey(engineSection.getId());;
        sectionDTO.setDepartureDate(dateUtils.convertFromCalendar(engineSection.getDepartureDate()));
        sectionDTO.setArrivalDate(dateUtils.convertFromCalendar(engineSection.getArrivalDate()));
        sectionDTO.setDeparture(departure);
        sectionDTO.setDestination(destination);
        sectionDTO.setCarrier(carrierMap.get(engineSection.getCarrier()));
        sectionDTO.setOperatingCarrier(carrierMap.get(engineSection.getOperatingCarrier()));
        sectionDTO.setCabinClass(engineSection.getCabinClass());
        sectionDTO.setFlightCode(engineSection.getFlightCode());
        sectionDTO.setArrivalTerminal(engineSection.getArrivalTerminal());
        sectionDTO.setDepartureTerminal(engineSection.getDepartureTerminal());
        sectionDTO.setTechnicalStops(techStopsMap.get(sectionResult.getId()));
        sectionDTO.setBaggageAllowance(engineSection.getBaggageAllowanceQuantity());
        sectionDTO.setVehicleModel(engineSection.getVehicleModel());
        sectionDTO.setTransportType(transportHandler.calculateSectionTransportType(departure, destination));
        sectionDTO.setDuration(engineSection.getDuration());

        return sectionDTO;
    }

}
