package com.odigeo.frontend.resolvers.prime.models;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionActions;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;

public class ModifyMembershipSubscription {
    private final String names;
    private final String lastNames;
    private final String email;
    private final ShoppingInfo shoppingInfo;
    private final String offerId;
    private final MembershipSubscriptionActions action;

    private ModifyMembershipSubscription(Builder builder) {
        email = builder.email;
        shoppingInfo = builder.shoppingInfo;
        lastNames = builder.lastNames;
        names = builder.names;
        offerId = builder.offerId;
        action = builder.action;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getOfferId() {
        return offerId;
    }

    public String getEmail() {
        return email;
    }

    public ShoppingInfo getShoppingInfo() {
        return shoppingInfo;
    }

    public String getNames() {
        return names;
    }

    public String getLastNames() {
        return lastNames;
    }

    public MembershipSubscriptionActions getAction() {
        return action;
    }

    public static final class Builder {
        private String names;
        private String lastNames;
        private String email;
        private ShoppingInfo shoppingInfo;
        private String offerId;
        private MembershipSubscriptionActions action;

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder shoppingInfo(ShoppingInfo shoppingInfo) {
            this.shoppingInfo = shoppingInfo;
            return this;
        }

        public Builder names(String names) {
            this.names = names;
            return this;
        }

        public Builder lastNames(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        public Builder offerId(String offerId) {
            this.offerId = offerId;
            return this;
        }

        public Builder action(MembershipSubscriptionActions action) {
            this.action = action;
            return this;
        }

        @SuppressWarnings("PMD.AccessorClassGeneration")
        public ModifyMembershipSubscription build() {
            return new ModifyMembershipSubscription(this);
        }
    }
}
