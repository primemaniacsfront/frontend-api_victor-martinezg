package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

class ItinerarySteeringUtils {

    private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);

    static Optional<BigDecimal> getPrice(SearchItineraryDTO searchItineraryDTO, String feeTypeId) {
        Optional<Fee> fee = searchItineraryDTO.getFees().stream().filter(f -> f.getType().getId().equals(feeTypeId)).findFirst();
        return fee.map(Fee::getPrice).map(Money::getAmount);
    }

    static boolean isPriceLowerPercentage(BigDecimal priceItinerary, BigDecimal priceToCompare, BigDecimal percentage) {
        return priceItinerary.compareTo(priceToCompare) <= 0
                || calculateDifferencePercentage(priceItinerary, priceToCompare).compareTo(percentage) <= 0;
    }

    private static BigDecimal calculateDifferencePercentage(BigDecimal priceItinerary, BigDecimal priceItinerarySteering) {
        return priceItinerarySteering.divide(priceItinerary, 4, RoundingMode.HALF_DOWN).multiply(ONE_HUNDRED)
                .subtract(ONE_HUNDRED).negate();
    }

}
