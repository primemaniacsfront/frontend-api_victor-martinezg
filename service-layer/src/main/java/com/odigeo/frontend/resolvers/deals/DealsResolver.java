package com.odigeo.frontend.resolvers.deals;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Deal;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.DealsRequest;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.deals.handlers.DealsHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.List;

public class DealsResolver implements Resolver {

    protected static final String DEALS_QUERY = "deals";

    @Inject
    private DealsHandler handler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher(DEALS_QUERY, this::dealsFetcher));
    }

    List<Deal> dealsFetcher(DataFetchingEnvironment env) {
        DealsRequest dealsRequest = jsonUtils.fromDataFetching(env, DealsRequest.class);
        ResolverContext context = env.getContext();

        return handler.getDeals(dealsRequest, context.getVisitInformation());
    }
}
