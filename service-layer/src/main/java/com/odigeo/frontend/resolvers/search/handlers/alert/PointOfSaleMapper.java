package com.odigeo.frontend.resolvers.search.handlers.alert;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Site;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;

@Singleton
public class PointOfSaleMapper {

    private static final int SITE_OFFSET = 2;

    private static final String POINT_OF_SALE_FORMAT = "%s.%s";
    private static final String NON_ALPHABETICAL_PATTERN = "[^A-Z]";

    private static final String ED_DOMAIN = "edreams";
    private static final String GO_DOMAIN = "govoyages";
    private static final String OP_DOMAIN = "opodo";
    private static final String TR_DOMAIN = "travellink";

    public String map(VisitInformation visit) {
        Site site = visit.getSite();
        Brand brand = visit.getBrand();

        return buildPointOfSale(site, brand);
    }

    private String buildPointOfSale(Site site, Brand brand) {
        return siteAndBrandNotUnknown(site, brand)
            ? String.format(POINT_OF_SALE_FORMAT, getBrandDomain(brand), getSiteDomain(site, brand))
            : StringUtils.EMPTY;
    }

    private boolean siteAndBrandNotUnknown(Site site, Brand brand) {
        return site != Site.UNKNOWN && brand != Brand.UNKNOWN;
    }

    private String getBrandDomain(Brand brand) {
        switch (brand) {
        case ED: return ED_DOMAIN;
        case GV: return GO_DOMAIN;
        case OP: return OP_DOMAIN;
        default: return TR_DOMAIN;
        }
    }

    private String getSiteDomain(Site site, Brand brand) {
        return site.name()
            .replaceAll(NON_ALPHABETICAL_PATTERN, StringUtils.EMPTY)
            .substring(brand == Brand.ED ? 0 : SITE_OFFSET)
            .toLowerCase(Locale.ROOT);
    }
}
