package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.model.DiscountDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;
import com.odigeo.frontend.services.currencyconvert.CurrencyConvertHandler;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDeal;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDealPriceCalculatorAdapter;
import com.odigeo.searchengine.v2.responses.accommodation.RoomGroupDeal;
import com.odigeo.searchengine.v2.responses.accommodation.RoomGroupDealInformation;
import com.odigeo.searchengine.v2.responses.fee.FeeDetails;
import com.odigeo.searchengine.v2.responses.fee.FeeInfo;
import com.odigeo.searchengine.v2.responses.membership.MembershipPerks;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Singleton
public class PriceBreakdownHandler {

    private static final BigDecimal ONE_HUNDRED_PERCENT = new BigDecimal(100);
    @Inject
    private CurrencyConvertHandler currencyConvertHandler;

    public PriceBreakdownDTO mapPriceBreakdown(AccommodationDeal accommodationDeal, VisitInformation visitInformation, Integer numNights) {
        BigDecimal totalPrice = accommodationDeal.getPrice().getTotalPrice();
        BigDecimal primeDiscount = Optional.ofNullable(accommodationDeal.getMembershipPerks())
            .map(MembershipPerks::getFee)
            .map(BigDecimal::abs)
            .orElse(BigDecimal.ZERO);
        BigDecimal packageDiscount = convertToSiteCurrency(visitInformation, accommodationDeal.getProviderCurrency(), accommodationDeal.getDynpackFareDiscount());

        PriceBreakdownDTO priceBreakdownDTO = mapPriceBreakdown(totalPrice, packageDiscount, primeDiscount);
        setPricePerNight(numNights, priceBreakdownDTO);
        return priceBreakdownDTO;
    }

    private void setPricePerNight(Integer numNights, PriceBreakdownDTO priceBreakdown) {
        priceBreakdown.setPricePerNight(priceBreakdown.getPrice().divide(new BigDecimal(numNights), 2, RoundingMode.FLOOR));
    }

    public PriceBreakdownDTO mapPriceBreakdown(RoomGroupDeal groupDeal, String providerCurrency, VisitInformation visitInformation) {
        AccommodationDealPriceCalculatorAdapter price = groupDeal.getRoomGroupDealInformation().getPrice();
        BigDecimal packageDiscountPrice = convertToSiteCurrency(visitInformation, providerCurrency, groupDeal.getDynpackFareDiscount());
        BigDecimal primeDiscountPrice = Optional.of(groupDeal)
            .map(RoomGroupDeal::getRoomGroupDealInformation)
            .map(RoomGroupDealInformation::getPrice)
            .map(AccommodationDealPriceCalculatorAdapter::getFeeInfo)
            .map(FeeInfo::getSearchFee)
            .map(FeeDetails::getDiscount)
            .map(BigDecimal::abs)
            .orElse(BigDecimal.ZERO);
        BigDecimal totalPrice = price.getTotalPrice();

        PriceBreakdownDTO priceBreakdown = mapPriceBreakdown(totalPrice, packageDiscountPrice, primeDiscountPrice);
        // TODO remove after migrate FE to use new model
        priceBreakdown.setCurrency(visitInformation.getCurrency());
        // TODO remove after migrate FE to use new model
        return priceBreakdown;
    }

    private PriceBreakdownDTO mapPriceBreakdown(BigDecimal totalPrice, BigDecimal packageDiscount, BigDecimal primeDiscount) {
        PriceBreakdownDTO priceBreakdownDTO = new PriceBreakdownDTO();

        BigDecimal totalDiscount = valueConverter(packageDiscount).add(valueConverter(primeDiscount));
        BigDecimal priceWithoutDiscount = totalPrice.add(totalDiscount);

        priceBreakdownDTO.setPrice(totalPrice);
        priceBreakdownDTO.setPriceWithoutDiscount(priceWithoutDiscount);
        DiscountDTO discountDTO = new DiscountDTO();
        discountDTO.setPackageAmount(packageDiscount);
        discountDTO.setPrimeAmount(primeDiscount);
        discountDTO.setPercentage(calculateDiscountPercentage(totalPrice, totalPrice.add(totalDiscount)));
        priceBreakdownDTO.setDiscount(discountDTO);

        return priceBreakdownDTO;
    }

    private BigDecimal convertToSiteCurrency(VisitInformation visitInformation, String providerCurrency, Float value) {
        return value != null ? currencyConvertHandler.convertToSiteCurrency(visitInformation, BigDecimal.valueOf(value), providerCurrency) : BigDecimal.ZERO;
    }

    private BigDecimal calculateDiscountPercentage(BigDecimal amount, BigDecimal amountPlusDiscount) {
        return amountPlusDiscount.subtract(amount).divide(amountPlusDiscount, 2, RoundingMode.FLOOR).multiply(ONE_HUNDRED_PERCENT);
    }

    private BigDecimal valueConverter(BigDecimal value) {
        return value != null ? value : BigDecimal.ZERO;
    }
}
