package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.services.MirService;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.request.ItineraryRatingRequest;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.response.ItineraryRatingResponse;

import java.util.concurrent.Callable;

public class MirCallable implements Callable<ItineraryRatingResponse> {

    private final SearchRequest searchRequest;
    private final SearchResponseDTO searchResponse;
    private final ResolverContext context;

    public MirCallable(SearchRequest searchRequestDTO, SearchResponseDTO searchResponseDTO, ResolverContext context) {
        this.context = context;
        this.searchRequest = searchRequestDTO;
        this.searchResponse = searchResponseDTO;
    }

    @Override
    public ItineraryRatingResponse call() {
        ItineraryRatingRequestMapper requestMapper = ConfigurationEngine.getInstance(ItineraryRatingRequestMapper.class);
        ItineraryRatingRequest itineraryRatingRequest = requestMapper.map(searchRequest, searchResponse, context);
        MirService service = ConfigurationEngine.getInstance(MirService.class);
        return service.getRatings(itineraryRatingRequest);
    }

}
