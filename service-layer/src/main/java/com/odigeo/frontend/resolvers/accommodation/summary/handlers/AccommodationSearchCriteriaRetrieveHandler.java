package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.DynPackSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.SearchCriteria;

@Singleton
public class AccommodationSearchCriteriaRetrieveHandler {

    @Inject
    private SearchService searchService;

    public AccommodationSearchCriteria getSearchCriteria(long searchId, Integer accommodationDealKey) {
        SearchResults searchResults = searchService.getSearchResult(searchId, null, accommodationDealKey);
        SearchCriteria searchCriteria = searchResults.getSearchCriteria();
        return searchCriteria instanceof DynPackSearchCriteria ? ((DynPackSearchCriteria) searchCriteria).getAccommodationSearchCriteria() : (AccommodationSearchCriteria) searchCriteria;
    }

    public SearchResults getSearchResultsBySearchId(long searchId, String visitCode) {
        return searchService.getSearchResults(searchId, visitCode);
    }
}
