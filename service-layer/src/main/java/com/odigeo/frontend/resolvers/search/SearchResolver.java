package com.odigeo.frontend.resolvers.search;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamilyRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PriceAlertRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.onefront.OFDebugInfoIntegration;
import com.odigeo.frontend.onefront.OFSearchIntegration;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.search.handlers.SearchChecker;
import com.odigeo.frontend.resolvers.search.handlers.SearchDebugHandler;
import com.odigeo.frontend.resolvers.search.handlers.SearchRequestHandler;
import com.odigeo.frontend.resolvers.search.handlers.SearchResponseHandler;
import com.odigeo.frontend.resolvers.search.handlers.alert.PriceAlertHandler;
import com.odigeo.frontend.resolvers.search.handlers.farefamily.FareFamilyHandler;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.tracking.client.track.method.Track;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.List;

@Singleton
public class SearchResolver implements Resolver {

    private static final String SEARCH_QUERY = "search";
    private static final String PRICE_ALERT_QUERY = "priceAlert";
    private static final String FARE_FAMILY_QUERY = "fareFamilies";

    @Inject
    private SearchRequestHandler searchRequestHandler;
    @Inject
    private SearchResponseHandler searchResponseHandler;
    @Inject
    private FareFamilyHandler fareFamilyHandler;
    @Inject
    private PriceAlertHandler priceAlertHandler;
    @Inject
    private SearchService searchService;
    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private OFSearchIntegration ofSearchIntegration;
    @Inject
    private OFDebugInfoIntegration ofDebugInfoIntegration;
    @Inject
    private SearchDebugHandler debugHandler;
    @Inject
    private SearchChecker searchChecker;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher(SEARCH_QUERY, this::searchFetcher)
                .dataFetcher(PRICE_ALERT_QUERY, this::priceAlertFetcher)
                .dataFetcher(FARE_FAMILY_QUERY, this::fareFamilyFetcher));
    }

    @Track(name = "search")
    public SearchResponseDTO searchFetcher(DataFetchingEnvironment env) {
        SearchRequest requestParams = jsonUtils.fromDataFetching(env, SearchRequest.class);
        ResolverContext resolverContext = env.getContext();
        GraphQLContext context = env.getGraphQlContext();

        searchChecker.check(requestParams, resolverContext);

        VisitInformation visit = resolverContext.getVisitInformation();
        MetricsHandler metricsHandler = context.get(MetricsHandler.class);

        metricsHandler.startMetric(MeasureConstants.SEARCH_BACKEND_TOTAL_TIME);

        ofDebugInfoIntegration.requestDebugInfo(resolverContext);

        SearchMethodRequest searchRequest = searchRequestHandler.buildSearchRequest(requestParams, resolverContext);
        SearchResponse searchResponse = searchService.executeSearch(searchRequest);

        debugHandler.addApiCaptureInfo(resolverContext, searchRequest, searchResponse);
        debugHandler.addQaModeSearchInfo(resolverContext, searchResponse);

        ofSearchIntegration.sendSearchResponse(requestParams, searchResponse, resolverContext);

        SearchResponseDTO response = searchResponseHandler.buildSearchResponse(searchResponse, requestParams, resolverContext);

        metricsHandler.stopMetric(MeasureConstants.SEARCH_BACKEND_TOTAL_TIME, visit);

        return response;
    }

    Boolean priceAlertFetcher(DataFetchingEnvironment env) {
        PriceAlertRequest requestParams = jsonUtils.fromDataFetching(env, PriceAlertRequest.class);
        ResolverContext context = env.getContext();
        VisitInformation visit = context.getVisitInformation();

        priceAlertHandler.subscribePriceAlerts(requestParams, visit);

        return true;
    }

    List<FareFamily> fareFamilyFetcher(DataFetchingEnvironment env) {
        FareFamilyRequest requestDTO = jsonUtils.fromDataFetching(env, FareFamilyRequest.class);
        ResolverContext context = env.getContext();
        VisitInformation visit = context.getVisitInformation();

        return fareFamilyHandler.retrieveFareFamilies(requestDTO, visit);
    }

}
