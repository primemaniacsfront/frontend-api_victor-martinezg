package com.odigeo.frontend.resolvers.shoppingcart.perks.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerks;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.odigeo.frontend.resolvers.Resolver;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;


@Singleton
public class MembershipPerksResolver implements Resolver {

    static final String SHOPPING_CART_TYPE = "ShoppingCart";
    static final String MEMBERSHIP_PERKS_FIELD = "membershipPerks";
    @Inject
    @Named("ShoppingCartPerksHandler")
    private PerksHandler perksHandler;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type(TypeRuntimeWiring.newTypeWiring(SHOPPING_CART_TYPE)
                .dataFetcher(MEMBERSHIP_PERKS_FIELD, this::membershipPerksDataFetcher));
    }

    MembershipPerks membershipPerksDataFetcher(DataFetchingEnvironment env) {
        GraphQLContext context = env.getGraphQlContext();
        return (MembershipPerks) perksHandler.map(context);
    }
}
