package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.search.handlers.PrimeHandler;

@Singleton
public class PricePolicyFactory {

    @Inject
    private MinPricePolicy minPricePolicy;
    @Inject
    private PrimePricePolicy primePricePolicy;
    @Inject
    private PrimeHandler primeHandler;

    public AbstractPricePolicy getPricePolicy(ResolverContext context) {
        return primeHandler.isPrimeMarket(context) ? primePricePolicy : minPricePolicy;
    }

}
