package com.odigeo.frontend.resolvers.accommodation.search.mappers;

import com.edreams.util.money.MoneySettings;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.accommodation.scoring.service.Accommodation;
import com.odigeo.accommodation.scoring.service.AccommodationProviderKey;
import com.odigeo.accommodation.scoring.service.Coordinates;
import com.odigeo.accommodation.scoring.service.DeviceType;
import com.odigeo.accommodation.scoring.service.GetScoresRequest;
import com.odigeo.accommodation.scoring.service.Review;
import com.odigeo.accommodation.scoring.service.RoomCriterion;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.resolvers.accommodation.commons.exceptions.CityNotFoundException;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationScoringRequestDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.GeoNode;
import com.odigeo.searchengine.v2.requests.AccommodationRoomRequest;
import com.odigeo.searchengine.v2.requests.LocationRequest;
import com.odigeo.searchengine.v2.responses.accommodation.SearchType;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Singleton
public class AccommodationScoringRequestMapper {

    private static final Map<String, DeviceType> DEVICE_TYPE_MAPPING;

    @Inject
    private Logger logger;

    static {
        Map<String, DeviceType> deviceTypeMapping = new HashMap<>();
        deviceTypeMapping.put("COMPUTER", DeviceType.COMPUTER);
        deviceTypeMapping.put("DESKTOP", DeviceType.DESKTOP);
        deviceTypeMapping.put("SMARTPHONE", DeviceType.SMARTPHONE);
        deviceTypeMapping.put("TABLET", DeviceType.TABLET);

        DEVICE_TYPE_MAPPING = Collections.unmodifiableMap(deviceTypeMapping);
    }

    @Inject
    private GeoLocationHandler geoLocationHandler;

    public GetScoresRequest map(AccommodationScoringRequestDTO accommodationScoringRequestDTO) {
        LocationRequest destination = accommodationScoringRequestDTO.getDestination();
        Locale locale = new Locale(accommodationScoringRequestDTO.getVisit().getLocale());
        LocalDate checkIn = accommodationScoringRequestDTO.getCheckInDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate checkOut = accommodationScoringRequestDTO.getCheckOutDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Map<String, Integer> dimensionPartitionMap = accommodationScoringRequestDTO.getVisit().getDimensionPartitionMap();
        GetScoresRequest getScoresRequest = new GetScoresRequest();
        getScoresRequest.setSearchId(accommodationScoringRequestDTO.getSearchId());
        getScoresRequest.setAccommodations(createAccommodations(accommodationScoringRequestDTO.getResponse().getAccommodations(), accommodationScoringRequestDTO.getVisit().getCurrency(), new BigDecimal(DAYS.between(checkIn, checkOut))));
        getScoresRequest.setCheckInDate(DateTimeFormatter.ISO_LOCAL_DATE.format(checkIn));
        getScoresRequest.setCheckOutDate(DateTimeFormatter.ISO_LOCAL_DATE.format(checkOut));
        getScoresRequest.setWebsite(accommodationScoringRequestDTO.getVisit().getSite().name());
        getScoresRequest.setRoomCriteria(createRoomCriterias(accommodationScoringRequestDTO.getRoomRequests()));
        getScoresRequest.setSearchType(calculateSearchType(accommodationScoringRequestDTO.getSearchType()));
        try {
            GeoNode geoNode = geoLocationHandler.getCityGeonode(destination.getGeoNodeId(), destination.getIataCode());
            getScoresRequest.setDestinationName(geoNode.getName().getText(locale));
            Coordinates coordinates = new Coordinates();
            coordinates.setLatitude(geoNode.getCoordinates().getLatitude());
            coordinates.setLongitude(geoNode.getCoordinates().getLongitude());
            getScoresRequest.setDestinationCoordinates(coordinates);
        } catch (CityNotFoundException e) {
            logger.warning(this.getClass(), "Error retrieving citygeonode", e);
        }
        getScoresRequest.setDevice(calculateDevice(accommodationScoringRequestDTO.getVisit().getDevice().name()));
        getScoresRequest.setTestDimensionPartitions(dimensionPartitionMap.entrySet().stream().map(k -> k.getKey() + "#" + k.getValue()).collect(Collectors.toList()));

        return getScoresRequest;
    }

    private DeviceType calculateDevice(String deviceTypeName) {
        if (DEVICE_TYPE_MAPPING.containsKey(deviceTypeName)) {
            return DEVICE_TYPE_MAPPING.get(deviceTypeName);
        }
        return DeviceType.OTHERS;
    }

    private com.odigeo.accommodation.scoring.service.SearchType calculateSearchType(SearchType searchType) {
        switch (searchType) {
        case DYNPACK:
            return com.odigeo.accommodation.scoring.service.SearchType.DYNPACK;
        case UPSELL:
            return com.odigeo.accommodation.scoring.service.SearchType.UPSELL;
        case POST_BOOKING:
            return com.odigeo.accommodation.scoring.service.SearchType.POST_BOOKING;
        case STANDALONE:
        default:
            return com.odigeo.accommodation.scoring.service.SearchType.STANDALONE;
        }
    }

    private List<Accommodation> createAccommodations(List<SearchAccommodationDTO> accommodationDealDTOs, String userCurrency, BigDecimal durationDays) {
        List<Accommodation> accommodations = new ArrayList<>();
        for (SearchAccommodationDTO accommodationDealDTO : accommodationDealDTOs) {
            Accommodation accommodation = new Accommodation();
            accommodation.setInternalId(accommodationDealDTO.getDedupId());
            String[] contentKey = accommodationDealDTO.getContentKey().split("#");
            AccommodationProviderKey accommodationProviderKey = new AccommodationProviderKey();
            accommodationProviderKey.setProvider(contentKey[0]);
            accommodationProviderKey.setAccommodationId(contentKey[1]);
            accommodation.setAccommodationProviderKey(accommodationProviderKey);
            accommodation.setAverageNightlyPrice(accommodationDealDTO.getProviderPrice().getAmount().divide(durationDays, MoneySettings.DEFAULT_ROUNDING_MODE));
            accommodation.setCurrency(accommodationDealDTO.getProviderPrice().getCurrency());
            accommodation.setAverageUserNightlyPrice(accommodationDealDTO.getPriceCalculator().getSortPrice().divide(durationDays, MoneySettings.DEFAULT_ROUNDING_MODE));
            accommodation.setUserCurrency(userCurrency);
            accommodation.setDedupId(accommodationDealDTO.getDedupId().toString());
            accommodation.setReviews(createReviews(accommodationDealDTO.getUsersRates()));
            accommodations.add(accommodation);
        }
        return accommodations;
    }

    private List<Review> createReviews(List<AccommodationReview> accommodationSearchReviewDTOS) {
        List<Review> reviews = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(accommodationSearchReviewDTOS)) {
            for (AccommodationReview accommodationSearchReviewDTO : accommodationSearchReviewDTOS) {
                Review review = new Review();
                review.setTotal(accommodationSearchReviewDTO.getTotalReviews());
                review.setSource(accommodationSearchReviewDTO.getProviderCode());
                review.setAvgTotalRating(accommodationSearchReviewDTO.getRating());
                reviews.add(review);
            }
        }
        return reviews;
    }

    private List<RoomCriterion> createRoomCriterias(List<AccommodationRoomRequest> roomRequests) {
        List<RoomCriterion> roomCriterions = new ArrayList<>();
        for (AccommodationRoomRequest accommodationRoomRequestDTO : roomRequests) {
            RoomCriterion roomCriterion = new RoomCriterion();
            roomCriterion.setAdultNumber(accommodationRoomRequestDTO.getNumAdults());
            roomCriterion.setChildNumber(accommodationRoomRequestDTO.getChildrenAges().size());
            roomCriterion.setChildAges(accommodationRoomRequestDTO.getChildrenAges());
            roomCriterions.add(roomCriterion);
        }
        return roomCriterions;
    }
}
