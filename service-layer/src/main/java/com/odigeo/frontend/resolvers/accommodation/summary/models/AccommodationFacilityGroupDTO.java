package com.odigeo.frontend.resolvers.accommodation.summary.models;

public class AccommodationFacilityGroupDTO {
    private String code;
    private String description;

    public AccommodationFacilityGroupDTO() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
