package com.odigeo.frontend.resolvers.geolocation.country.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country;
import org.mapstruct.Context;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Locale;

@Mapper
@DecoratedWith(CountryMapperDecorator.class)
public interface CountryMapper {

    @Mapping(source = "country.geoNodeId", target = "id")
    @Mapping(source = "country.countryCode", target = "code")
    @Mapping(source = "country.phonePrefix", target = "phonePrefix")
    @Mapping(target = "name", ignore = true)
    Country mapModelToContract(com.odigeo.geoapi.v4.responses.Country country, @Context Locale locale);
}
