package com.odigeo.frontend.resolvers.shoppingcart.traveller.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCartTraveller;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerInformationDescription;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.dapi.client.Traveller;
import com.odigeo.frontend.resolvers.shoppingcart.traveller.mappers.ShoppingTravellerMapper;
import graphql.GraphQLContext;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class TravellerHandler {

    @Inject
    private ShoppingTravellerMapper mapper;

    public List<TravellerInformationDescription> mapTravellerInformation(GraphQLContext graphQLContext) {
        List<com.odigeo.dapi.client.TravellerInformationDescription> traveller = getTravellerInformationDescription(getShoppingCart(graphQLContext));
        return mapper.travellerListInfoToModel(traveller);
    }

    private List<com.odigeo.dapi.client.TravellerInformationDescription> getTravellerInformationDescription(ShoppingCart shoppingCart) {
        return Optional.ofNullable(shoppingCart)
            .map(ShoppingCart::getRequiredTravellerInformation)
            .orElse(null);
    }

    private ShoppingCart getShoppingCart(GraphQLContext graphQLContext) {
        return Optional.ofNullable(graphQLContext)
            .map(context -> context.get(ShoppingCartSummaryResponse.class))
            .map(ShoppingCartSummaryResponse.class::cast)
            .map(ShoppingCartSummaryResponse::getShoppingCart)
            .orElse(null);
    }

    public List<ShoppingCartTraveller> mapTravellers(GraphQLContext graphQLContext) {
        List<Traveller> travellers = getTravellers(getShoppingCart(graphQLContext));
        return mapper.travellerListContractToModel(travellers);
    }

    private List<Traveller> getTravellers(ShoppingCart shoppingCart) {
        return Optional.ofNullable(shoppingCart)
            .map(ShoppingCart::getTravellers)
            .map(travellers -> cleanTravellers(travellers, shoppingCart))
            .orElse(null);
    }

    private List<Traveller> cleanTravellers(List<Traveller> travellers, ShoppingCart shoppingCart) {
        return shoppingCart.getItineraryShoppingItem() != null
            ? travellers
            : travellers.stream().filter(traveller -> StringUtils.isNotEmpty(traveller.getName())).collect(Collectors.toList());
    }

}
