package com.odigeo.frontend.resolvers.shoppingcart.collectionmethods;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.shoppingcart.collectionmethods.handlers.CollectionMethodsHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;

@Singleton
public class CollectionMethodsResolver implements Resolver {
    @Inject
    private CollectionMethodsHandler handler;

    static final String SHOPPING_CART_TYPE = "ShoppingCart";
    static final String COLLECTION_METHODS = "collectionMethods";

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type(TypeRuntimeWiring.newTypeWiring(SHOPPING_CART_TYPE)
                .dataFetcher(COLLECTION_METHODS, this::availableCollectionMethod));
    }

    AvailableCollectionMethodsResponse availableCollectionMethod(DataFetchingEnvironment env) {
        return handler.map(env.getGraphQlContext());
    }
}
