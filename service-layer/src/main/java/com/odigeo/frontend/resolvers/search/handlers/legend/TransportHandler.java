package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.google.inject.Singleton;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.contract.location.LocationDTO;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class TransportHandler {

    public TransportType calculateSectionTransportType(LocationDTO departure, LocationDTO destination) {
        return isAirport(departure) && isAirport(destination)
            ? TransportType.PLANE
            : TransportType.TRAIN;
    }

    private boolean isAirport(LocationDTO location) {
        return location.getLocationType() == LocationType.AIRPORT;
    }

    public Set<TransportType> calculateSegmentTransportTypes(List<SectionDTO> segmentSections) {
        return segmentSections.stream()
            .map(SectionDTO::getTransportType)
            .collect(Collectors.toSet());
    }

    public Set<TransportType> calculateItineraryTransportTypes(List<LegDTO> itineraryLegs) {
        return itineraryLegs.stream()
            .map(LegDTO::getSegments)
            .flatMap(List::stream)
            .map(SegmentDTO::getTransportTypes)
            .flatMap(Set::stream)
            .collect(Collectors.toSet());
    }

}
