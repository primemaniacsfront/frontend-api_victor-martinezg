package com.odigeo.frontend.resolvers.accommodation.rooms.handlers;

import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationDataHandler;
import com.odigeo.hcsapi.v9.beans.RoomsDetailResponse;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;

public class RoomsHandlerImpl implements RoomsHandler {

    @Inject
    private AccommodationDataHandler accommodationDataHandler;

    public SearchRoomResponse getRoomAvailability(Long searchId, Long accommodationDealKey, VisitInformation visit) {
        return accommodationDataHandler.getRoomAvailability(searchId, accommodationDealKey, visit);
    }

    public RoomsDetailResponse getStaticRoomDetails(String providerId, String providerHotelId, VisitInformation visit, String... roomId) {
        return accommodationDataHandler.getStaticRoomDetails(providerId, providerHotelId, visit, roomId);
    }


}
