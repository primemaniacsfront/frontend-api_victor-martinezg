package com.odigeo.frontend.resolvers.prime.models;

import java.util.Optional;

public enum MembershipType {

    BASIC_FREE,
    BUSINESS,
    BASIC,
    PLUS,
    EMPLOYEE,
    UNKNOWN;

    public static MembershipType fromValue(String value) {
        return Optional.ofNullable(value).map(MembershipType::valueOf).orElse(null);
    }

}
