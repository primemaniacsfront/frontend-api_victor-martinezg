package com.odigeo.frontend.resolvers.itinerary.mappers.leg;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Leg;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import com.odigeo.dapi.client.ItinerariesLegend;
import com.odigeo.dapi.client.SegmentResult;
import com.odigeo.frontend.resolvers.itinerary.mappers.segment.SegmentMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mapstruct.Context;
import org.mapstruct.Mapper;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mapper(uses = SegmentMapper.class)
public interface LegMapper {

    default List<Leg> itinerariesLegendToModel(ItinerariesLegend legend, @Context ShoppingCartContext context) {
        if (legend == null) {
            return null;
        }

        List<Segment> segments = this.mapSegmentListContractToModel(legend.getSegmentResults(), context);
        List<Leg> legs = Optional.ofNullable(segments)
            .map(List::stream)
            .orElseGet(Stream::empty)
            .map(segment -> {
                Leg model = new Leg();
                model.setSegments(Arrays.asList(segment));
                return model;
            })
            .collect(Collectors.toList());
        return legs;
    }

    List<Segment> mapSegmentListContractToModel(List<SegmentResult> segments, @Context ShoppingCartContext context);

}
