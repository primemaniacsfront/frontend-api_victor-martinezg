package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.criteria.TripType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Singleton;

@Singleton
class CarbonFootprintTripTypeMapper {

    public TripType map(SearchRequest searchRequestDTO) {
        return searchRequestDTO.getTripType() == com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType.ONE_WAY
                ? TripType.ONE_WAY
                : TripType.ROUND_TRIP;
    }

}
