package com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedSeatRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.ancillary.seat.mappers.MoneyMapper;
import com.odigeo.itineraryapi.v1.request.Money;
import com.odigeo.itineraryapi.v1.request.SeatMapSelectionRequest;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class SeatMapSelectionRequestMapper {

    @Inject
    private MoneyMapper moneyMapper;

    public List<SeatMapSelectionRequest> map(List<SelectedSeatRequest> selectedSeatRequestList) {

        return CollectionUtils.emptyIfNull(selectedSeatRequestList)
                .stream()
                .map(this::mapSeatMapSelectionRequest)
                .collect(Collectors.toList());
    }

    private SeatMapSelectionRequest mapSeatMapSelectionRequest(SelectedSeatRequest selectedSeatRequest) {
        SeatMapSelectionRequest seatMapSelectionRequest = new SeatMapSelectionRequest();
        SeatRequest seatRequest = selectedSeatRequest.getSeat();

        seatMapSelectionRequest.setSection(selectedSeatRequest.getSection());
        seatMapSelectionRequest.setColumn(seatRequest.getColumn());
        seatMapSelectionRequest.setFloor(seatRequest.getFloor());
        seatMapSelectionRequest.setRow(seatRequest.getRow());
        seatMapSelectionRequest.setSeatMapRow(seatRequest.getSeatMapRow());
        seatMapSelectionRequest.setTotalPrice(moneyMapper.map(seatRequest.getDisplayedPrice()));
        seatMapSelectionRequest.setSeatFee(new Money());

        return seatMapSelectionRequest;
    }
}
