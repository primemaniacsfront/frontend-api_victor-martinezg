package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageCondition;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.resolvers.search.configuration.BaggageConfiguration;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.baggage.BaggageConditions;
import com.odigeo.searchengine.v2.responses.baggage.BaggageDescriptor;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Singleton
public class BaggageHandler {

    @Inject
    private BaggageConfiguration baggageConfiguration;

    public void populateItineraryBaggageConditions(SearchEngineContext seContext, FareItinerary itinerary,
                                                   List<LegDTO> legs) {

        if (hasItineraryBaggages(itinerary, seContext.getBaggageMap())) {
            legs.forEach(leg -> leg.getSegments()
                .forEach(segment -> segment.setBaggageCondition(BaggageCondition.CHECKIN_INCLUDED)));
        }
    }

    private boolean hasItineraryBaggages(FareItinerary fareItinerary, Map<Integer, BaggageConditions> baggageMap) {
        return Optional.ofNullable(baggageMap.get(fareItinerary.getBaggageFees()))
            .map(BaggageConditions::getBaggageDescriptorIncludedInPrice)
            .map(this::hasPiecesOrKilos)
            .orElse(false);
    }

    private boolean hasPiecesOrKilos(BaggageDescriptor descriptor) {
        return Optional.ofNullable(descriptor.getPieces()).map(pieces -> pieces > 0).orElse(false)
            || Optional.ofNullable(descriptor.getKilos()).map(kilos -> kilos > 0).orElse(false);
    }

    public void populateSegmentBaggageConditions(Map<Integer, SegmentDTO> segments) {
        segments.values().stream()
            .filter(segment -> segment.getBaggageCondition() == null)
            .forEach(segment ->
                segment.setBaggageCondition(calculateSegmentBaggageConditions(segment)));
    }

    private BaggageCondition calculateSegmentBaggageConditions(SegmentDTO segment) {
        boolean allBaggagesIncluded = segment.getSections().stream().allMatch(this::hasSectionBaggages);
        BaggageCondition baggageCondition = BaggageCondition.CABIN_INCLUDED;

        if (allBaggagesIncluded) {
            baggageCondition = BaggageCondition.CHECKIN_INCLUDED;
        } else if (baggageConfiguration.getAirlinesWithSmallBagInCabin().contains(segment.getCarrier().getId())) {
            baggageCondition = BaggageCondition.BASED_ON_AIRLINE;
        }

        return baggageCondition;
    }

    private boolean hasSectionBaggages(SectionDTO section) {
        Integer baggageQuantity = section.getBaggageAllowance();

        return Optional.ofNullable(baggageQuantity).map(quantity -> quantity > 0).orElse(false)
            || Optional.ofNullable(section.getCarrier()).map(this::isRenfeCode).orElse(false);
    }

    private boolean isRenfeCode(Carrier carrierDTO) {
        return baggageConfiguration.getRenfeCode().equals(carrierDTO.getId());
    }
}
