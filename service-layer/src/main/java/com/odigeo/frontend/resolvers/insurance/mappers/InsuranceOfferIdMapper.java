package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferId;
import com.google.inject.Singleton;
import com.odigeo.insurance.api.last.request.OfferId;

@Singleton
public class InsuranceOfferIdMapper {

    public InsuranceOfferId map(OfferId offerId) {
        InsuranceOfferId insuranceOfferId = new InsuranceOfferId();

        insuranceOfferId.setId(offerId.getId());
        insuranceOfferId.setVersion(offerId.getVersion());

        return insuranceOfferId;
    }
}
