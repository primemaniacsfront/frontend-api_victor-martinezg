package com.odigeo.frontend.resolvers.deals.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ApparentPrice;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.marketing.search.price.v1.util.Money;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Singleton
public class ApparentPricesMapper {

    @Inject
    private MoneyMapper moneyMapper;

    List<ApparentPrice> map(Map<String, Money> apparentPrices) {
        return apparentPrices.entrySet()
            .stream()
            .map(this::mapApparentPrice)
            .collect(Collectors.toList());
    }

    private ApparentPrice mapApparentPrice(Map.Entry<String, Money> entry) {
        ApparentPrice apparentPrice = new ApparentPrice();
        apparentPrice.setId(entry.getKey());
        apparentPrice.setPrice(moneyMapper.moneyContractToModel(entry.getValue()));
        return apparentPrice;
    }

}
