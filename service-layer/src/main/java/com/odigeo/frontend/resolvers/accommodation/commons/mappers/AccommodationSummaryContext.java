package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.odigeo.dapi.client.AccommodationDealInformation;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.geoapi.v4.responses.City;
import org.mapstruct.BeforeMapping;

import java.util.Optional;

public class AccommodationSummaryContext {

    private boolean paymentAtDestination;
    private final City city;

    @BeforeMapping
    public void fillPaymentAtDestination(AccommodationShoppingItem accommodationShoppingItem) {
        paymentAtDestination = Optional.ofNullable(accommodationShoppingItem)
            .map(AccommodationShoppingItem::getAccommodationDealInformation)
            .map(AccommodationDealInformation::isPaymentAtDestination)
            .orElse(false);
    }

    public AccommodationSummaryContext(City city) {
        this.city = city;
    }

    public boolean isPaymentAtDestination() {
        return paymentAtDestination;
    }

    public City getCity() {
        return city;
    }
}
