package com.odigeo.frontend.resolvers.shoppingcart.buyer.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Buyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BuyerInformationDescription;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.resolvers.shoppingcart.buyer.mappers.BuyerMapper;
import graphql.GraphQLContext;

import java.util.Optional;

@Singleton
public class BuyerHandler {

    @Inject
    private BuyerMapper buyerMapper;

    public Buyer map(GraphQLContext graphQLContext) {
        com.odigeo.dapi.client.Buyer buyer = getBuyer(getShoppingCart(graphQLContext));
        return buyerMapper.map(buyer);
    }

    private com.odigeo.dapi.client.Buyer getBuyer(ShoppingCart shoppingCart) {
        return Optional.ofNullable(shoppingCart)
            .map(ShoppingCart::getBuyer)
            .orElse(null);
    }

    private ShoppingCart getShoppingCart(GraphQLContext graphQLContext) {
        return Optional.ofNullable(graphQLContext)
                .map(context -> context.get(ShoppingCartSummaryResponse.class))
                .map(ShoppingCartSummaryResponse.class::cast)
                .map(ShoppingCartSummaryResponse::getShoppingCart)
                .orElse(null);
    }

    public BuyerInformationDescription mapBuyerInfo(GraphQLContext graphQLContext) {
        com.odigeo.dapi.client.BuyerInformationDescription buyerInfo = getBuyerInfo(getShoppingCart(graphQLContext));
        return buyerMapper.mapBuyerInformationDescription(buyerInfo);
    }

    private com.odigeo.dapi.client.BuyerInformationDescription getBuyerInfo(ShoppingCart shoppingCart) {
        return Optional.ofNullable(shoppingCart)
            .map(ShoppingCart::getRequiredBuyerInformation)
            .orElse(null);
    }
}
