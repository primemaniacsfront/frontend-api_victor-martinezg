package com.odigeo.frontend.resolvers.shoppingcart.collectionmethods.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.resolvers.accommodation.product.mappers.AvailableCollectionMethodsMapper;
import graphql.GraphQLContext;

import java.util.Optional;

@Singleton
public class CollectionMethodsHandler {

    @Inject
    private AvailableCollectionMethodsMapper mapper;

    public AvailableCollectionMethodsResponse map(GraphQLContext graphQLContext) {
        ShoppingCart shoppingCart = getShoppingCart(graphQLContext);
        return mapper.paymentInformationContractToModel(shoppingCart);
    }

    private ShoppingCart getShoppingCart(GraphQLContext graphQLContext) {
        return Optional.ofNullable(graphQLContext)
                .map(context -> context.get(ShoppingCartSummaryResponse.class))
                .map(ShoppingCartSummaryResponse.class::cast)
                .map(ShoppingCartSummaryResponse::getShoppingCart)
                .orElse(null);
    }
}
