package com.odigeo.frontend.resolvers.accommodation.details.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationDetailsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImage;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImageQuality;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImages;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.details.models.AccommodationDetailsResponseDTO;
import org.apache.commons.collections.CollectionUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Mapper
public interface AccommodationDetailResponseMapper {

    @Mapping(source = "accommodationDetail.accommodationImages", target = "accommodationDetail.images", qualifiedByName = "mapImagesByQuality")
    AccommodationDetailsResponse map(AccommodationDetailsResponseDTO response);

    @Named("mapImagesByQuality")
    default List<AccommodationImages> mapImagesByQuality(List<AccommodationImageDTO> accommodationImages) {
        Map<AccommodationImageQuality, List<AccommodationImage>> imagesGrouped = new HashMap<>();
        if (CollectionUtils.isNotEmpty(accommodationImages)) {
            imagesGrouped = accommodationImages.stream()
                .filter(item -> item.getQuality() != null)
                .sorted(Comparator.comparing(AccommodationImageDTO::getType))
                .map(this::mapAccommodationImage)
                .collect(Collectors.groupingBy(AccommodationImage::getQuality, Collectors.mapping(images -> images, Collectors.toList())));
        }
        return imagesGrouped.values().stream().map(images -> new AccommodationImages(images.get(0).getQuality(), images)).collect(Collectors.toList());
    }

    AccommodationImage mapAccommodationImage(AccommodationImageDTO accommodationImageDTO);
}
