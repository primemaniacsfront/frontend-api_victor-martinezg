package com.odigeo.frontend.resolvers.itinerary.mappers.segment;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import com.odigeo.dapi.client.SegmentResult;
import com.odigeo.frontend.resolvers.itinerary.mappers.TransportTypeMapper;
import com.odigeo.frontend.resolvers.itinerary.mappers.section.SectionMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.baggage.BaggageConditionMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.carrier.CarrierMapper;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {CarrierMapper.class, SectionMapper.class, TransportTypeMapper.class, BaggageConditionMapper.class, StopMapper.class})
public interface SegmentMapper {

    @Mapping(source = "source.segment.carrier", target = "carrier")
    @Mapping(source = "source.segment.duration", target = "duration")
    @Mapping(source = "source.segment.seats", target = "seats")
    @Mapping(source = "source.segment.sections", target = "sections")
    Segment segmentContractToModel(SegmentResult source, @Context ShoppingCartContext context);

}
