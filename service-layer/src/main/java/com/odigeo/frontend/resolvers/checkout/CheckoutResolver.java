package com.odigeo.frontend.resolvers.checkout;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutFactory;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutHandler;
import graphql.execution.DataFetcherResult;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;


@Singleton
public class CheckoutResolver implements Resolver {

    @VisibleForTesting
    static final String CHECKOUT = "checkout";
    public static final String PATH = "path";
    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private CheckoutFactory checkoutFactory;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Mutation", typeWiring -> typeWiring
                        .dataFetcher(CHECKOUT, this::checkoutFetcher));
    }

    DataFetcherResult<CheckoutResponse> checkoutFetcher(DataFetchingEnvironment env) {
        env.getGraphQlContext().put(PATH, env.getExecutionStepInfo().getPath());
        CheckoutRequest request = jsonUtils.fromDataFetching(env, CheckoutRequest.class);
        CheckoutHandler handler = checkoutFactory.getInstance(request.getShoppingType());
        CheckoutFetcherResult response = handler.checkout(request, env.getContext(), env.getGraphQlContext());
        return new DataFetcherResult.Builder<CheckoutResponse>().data(response.getData()).errors(response.getErrors()).build();
    }

}
