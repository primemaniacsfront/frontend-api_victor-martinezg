package com.odigeo.frontend.resolvers.accommodation.product.handlers.productapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationBuyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateAccommodationProductResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Guest;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.CreateProductAccommodationHandler;
import com.odigeo.frontend.services.accommodation.AccommodationProductService;

import java.util.List;

public class CreateAccommodationProduct implements CreateProductAccommodationHandler {

    @Inject
    private AccommodationProductService accommodationProductService;

    @Override
    public CreateAccommodationProductResponse createProductAccommodation(String dealId, List<Guest> guests, AccommodationBuyer buyer, ResolverContext resolverContext) {
        String productId = accommodationProductService.createProduct(dealId, buyer);
        AccommodationProduct accommodationProduct = new AccommodationProduct();
        accommodationProduct.setProductId(productId);
        CreateAccommodationProductResponse response = new CreateAccommodationProductResponse();
        response.setAccommodationProduct(accommodationProduct);
        return response;
    }
}
