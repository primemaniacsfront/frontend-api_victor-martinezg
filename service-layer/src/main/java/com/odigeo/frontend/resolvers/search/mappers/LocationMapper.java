package com.odigeo.frontend.resolvers.search.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.google.inject.Singleton;
import com.odigeo.searchengine.v2.responses.gis.Location;
import org.apache.commons.lang3.StringUtils;

@Singleton
public class LocationMapper {

    private static final String AIRPORT = "Airport";
    private static final String TRAIN_STATION = "Train Station";
    private static final String BUS_STATION = "Bus Station";

    public LocationType map(Location location) {
        String locationType = StringUtils.defaultString(location.getType());

        switch(locationType) {
        // TODO prepare funnel for bus (current bus content is provided by airlines)
        case AIRPORT:
        case BUS_STATION:
            return LocationType.AIRPORT;
        case TRAIN_STATION: return LocationType.TRAIN_STATION;
        default: return LocationType.OTHER;
        }
    }

}
