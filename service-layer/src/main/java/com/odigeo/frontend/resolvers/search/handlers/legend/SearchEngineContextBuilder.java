package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.contract.location.LocationDTO;
import com.odigeo.searchengine.v2.common.ItinerarySortCriteria;
import com.odigeo.searchengine.v2.responses.baggage.BaggageConditions;
import com.odigeo.searchengine.v2.responses.collection.CollectionEstimationFees;
import com.odigeo.searchengine.v2.responses.collection.CollectionMethod;
import com.odigeo.searchengine.v2.responses.itinerary.CrossFaringItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.HubItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerariesLegend;
import com.odigeo.searchengine.v2.responses.itinerary.ItineraryProvider;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerarySearchResults;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;
import com.odigeo.searchengine.v2.responses.itinerary.SegmentResult;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

@Singleton
public class SearchEngineContextBuilder {

    @Inject
    private SearchEngineLegendMapper legendMapper;

    @SuppressWarnings("PMD.AccessorMethodGeneration")
    public SearchEngineContext build(ItinerarySearchResults itinerarySearchResults) {
        ItinerariesLegend legend = itinerarySearchResults.getLegend();

        SearchEngineContext context = SearchEngineContext.getSearchEngineContext();
        context.segmentResults = legend.getSegmentResults();
        context.itineraryResults = itinerarySearchResults.getItineraryResults();
        context.hubs = legend.getItineraries().getHubs();
        context.currency = itinerarySearchResults.getPriceCurrency().getCurrencyCode();
        context.itinerarySortCriteria = itinerarySearchResults.getSortCriteria();

        context.sectionResultMap = legendMapper.buildSectionResultMap(legend);
        context.freeCancellationMap = legendMapper.buildFreeCancellationMap(legend);
        context.baggageMap = legendMapper.buildBaggageMap(legend);
        context.collectionMethodMap = legendMapper.buildCollectionMethodMap(legend);
        context.collectionFeesMap = legendMapper.buildCollectionFeesMap(legend);
        context.hubMap = legendMapper.buildHubMap(legend);
        context.simplesMap = legendMapper.buildSimplesMap(legend);
        context.crossFaringMap = legendMapper.buildCrossFaringMap(legend);

        context.carrierMap = legendMapper.buildCarrierMap(legend);
        context.locationMap = legendMapper.buildLocationMap(legend);
        context.techStopsMap = legendMapper.buildTechStopsMap(legend, context.locationMap);

        return context;
    }

    public static class SearchEngineContext {
        private List<SegmentResult> segmentResults;
        private List<FareItinerary> itineraryResults;
        private List<HubItinerary> hubs;
        private String currency;
        private ItinerarySortCriteria itinerarySortCriteria;

        private Map<Integer, SectionResult> sectionResultMap;
        private Map<Integer, ZonedDateTime> freeCancellationMap;
        private Map<Integer, BaggageConditions> baggageMap;
        private Map<Integer, CollectionMethod> collectionMethodMap;
        private Map<Integer, CollectionEstimationFees> collectionFeesMap;
        private Map<Integer, HubItinerary> hubMap;
        private Map<Integer, ItineraryProvider> simplesMap;
        private Map<Integer, CrossFaringItinerary> crossFaringMap;

        private Map<Integer, Carrier> carrierMap;
        private Map<Integer, LocationDTO> locationMap;
        private Map<Integer, List<TechnicalStop>> techStopsMap;

        private SearchEngineContext() {
        }

        private static SearchEngineContext getSearchEngineContext() {
            return new SearchEngineContext();
        }

        public List<SegmentResult> getSegmentResults() {
            return segmentResults;
        }

        public List<FareItinerary> getItineraryResults() {
            return itineraryResults;
        }

        public List<HubItinerary> getHubs() {
            return hubs;
        }

        public String getCurrency() {
            return currency;
        }

        public ItinerarySortCriteria getItinerarySortCriteria() {
            return itinerarySortCriteria;
        }

        public Map<Integer, SectionResult> getSectionResultMap() {
            return sectionResultMap;
        }

        public Map<Integer, ZonedDateTime> getFreeCancellationMap() {
            return freeCancellationMap;
        }

        public Map<Integer, BaggageConditions> getBaggageMap() {
            return baggageMap;
        }

        public Map<Integer, CollectionMethod> getCollectionMethodMap() {
            return collectionMethodMap;
        }

        public Map<Integer, CollectionEstimationFees> getCollectionFeesMap() {
            return collectionFeesMap;
        }

        public Map<Integer, HubItinerary> getHubMap() {
            return hubMap;
        }

        public Map<Integer, ItineraryProvider> getSimplesMap() {
            return simplesMap;
        }

        public Map<Integer, CrossFaringItinerary> getCrossFaringMap() {
            return crossFaringMap;
        }

        public Map<Integer, Carrier> getCarrierMap() {
            return carrierMap;
        }

        public Map<Integer, LocationDTO> getLocationMap() {
            return locationMap;
        }

        public Map<Integer, List<TechnicalStop>> getTechStopsMap() {
            return techStopsMap;
        }
    }

}
