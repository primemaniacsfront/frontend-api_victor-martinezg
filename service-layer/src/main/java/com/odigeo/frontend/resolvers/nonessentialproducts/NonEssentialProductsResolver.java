package com.odigeo.frontend.resolvers.nonessentialproducts;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductOffer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsIdRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsOfferResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectNonEssentialProductsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectNonEssentialProductsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductSelection;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.nonessentialproducts.handlers.NonEssentialProductsHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class NonEssentialProductsResolver implements Resolver {

    @Inject
    private NonEssentialProductsHandler nonEssentialProductsHandler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher("getAvailableNonEssentialProducts", this::nonEssentialProductsOffersFetcher))
            .type("Mutation", typeWiring -> typeWiring
                    .dataFetcher("selectNonEssentialProducts", this::selectNonEssentialProducts));
    }

    NonEssentialProductsOfferResponse nonEssentialProductsOffersFetcher(DataFetchingEnvironment env) {
        NonEssentialProductsRequest additionalProductsOffersRequest = jsonUtils.fromDataFetching(env, NonEssentialProductsRequest.class);

        NonEssentialProductsOfferResponse nonEssentialProductsOfferResponse = nonEssentialProductsHandler.getAllNonEssentialProductsOffers(additionalProductsOffersRequest.getBookingId(), env.getContext());

        List<String> productIds = Optional.ofNullable(additionalProductsOffersRequest.getProducts())
            .orElse(Collections.emptyList())
            .stream()
            .map(NonEssentialProductsIdRequest::getId)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(productIds)) {
            filterProductsNotAsked(nonEssentialProductsOfferResponse, productIds);
        }

        return nonEssentialProductsOfferResponse;
    }

    SelectNonEssentialProductsResponse selectNonEssentialProducts(DataFetchingEnvironment env) {
        SelectNonEssentialProductsRequest nonEssentialProductsRequest = jsonUtils.fromDataFetching(env, SelectNonEssentialProductsRequest.class);

        List<String> productsPolicies = Optional.ofNullable(nonEssentialProductsRequest.getProducts())
                .orElse(Collections.emptyList())
                .stream()
                .map(ProductSelection::getOfferId)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        return nonEssentialProductsHandler.getSelectNonEssentialProductsResponse(productsPolicies);
    }

    private void filterProductsNotAsked(NonEssentialProductsOfferResponse nonEssentialProductsOfferResponse, List<String> productIds) {
        Predicate<NonEssentialProductOffer> nonEssentialProductPredicate = nonEssentialProduct -> !productIds.contains(nonEssentialProduct.getProduct().getOfferId());

        List<NonEssentialProductOffer> guarantees = new ArrayList<>(nonEssentialProductsOfferResponse.getGuarantees());
        guarantees.removeIf(nonEssentialProductPredicate);
        nonEssentialProductsOfferResponse.setGuarantees(guarantees);

        List<NonEssentialProductOffer> rebooking = new ArrayList<>(nonEssentialProductsOfferResponse.getRebooking());
        rebooking.removeIf(nonEssentialProductPredicate);
        nonEssentialProductsOfferResponse.setRebooking(rebooking);
    }
}
