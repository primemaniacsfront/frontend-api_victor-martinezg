package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.handlers.SearchDebugHandler;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.resolvers.search.models.TrackingInfoDTO;
import com.odigeo.frontend.services.MirService;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.Itinerary;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.ItineraryRatingResult;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.SearchCriteria;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.request.ItineraryRatingRequest;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.response.ItineraryRatingResponse;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Optional;

@Singleton
public class MirHandler {

    @Inject
    private MirSearchCriteriaHandler mirSearchCriteriaHandler;
    @Inject
    private MirItineraryHandler mirItineraryHandler;
    @Inject
    private MirService mirService;
    @Inject
    private SearchDebugHandler debugHandler;

    public void addRatingsAndTrackingInfo(SearchRequest searchRequest, SearchResponseDTO searchResponse,
                                          ResolverContext context) {

        ItineraryRatingRequest ratingRequest = new ItineraryRatingRequest();
        VisitInformation visit = context.getVisitInformation();
        List<Itinerary> ratingItineraries = mirItineraryHandler.buildRatingItineraries(searchResponse);
        SearchCriteria searchCriteria = mirSearchCriteriaHandler
                .buildRatingSearchCriteria(searchRequest, searchResponse, visit);

        ratingRequest.setVisitInformation(visit.getVisitCode());
        ratingRequest.setSearchCriteria(searchCriteria);
        ratingRequest.setItineraries(ratingItineraries);

        ItineraryRatingResponse response = mirService.getRatings(ratingRequest);

        Optional.ofNullable(response)
                .ifPresent(ratingResponse -> addRatingAndReturningUserDeviceTracking(context, searchResponse, ratingResponse));
    }

    public void addRatingAndReturningUserDeviceTracking(ResolverContext context, SearchResponseDTO searchResponse, ItineraryRatingResponse ratingResponse) {
        addRating(searchResponse.getItineraries(), ratingResponse.getItineraryRatingResults());
        addUserDeviceId(searchResponse.getTrackingInfo(), ratingResponse.getUserDeviceId());
        debugHandler.addQaModeMeRatingInfo(context, searchResponse.getItineraries());
    }

    private void addRating(List<SearchItineraryDTO> itineraries, List<ItineraryRatingResult> meResults) {

        List<ItineraryRatingResult> ratings = ListUtils.emptyIfNull(meResults);

        for (int index = 0; index < ratings.size(); index++) {
            ItineraryRatingResult rating = ratings.get(index);
            SearchItineraryDTO itinerary = itineraries.get(index);
            itinerary.setMeRating(rating.getRating());
        }
    }

    private void addUserDeviceId(TrackingInfoDTO trackingInfo, String userDeviceId) {
        String returningUserDeviceId = Optional.ofNullable(userDeviceId).orElse(StringUtils.EMPTY);
        trackingInfo.setReturningUserDeviceId(returningUserDeviceId);
    }
}
