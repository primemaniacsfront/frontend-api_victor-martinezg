package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentRequest;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.Response.Status;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Objects;

@Singleton
public class SearchChecker {

    public void check(SearchRequest searchRequest, ResolverContext context) {
        ItineraryRequest itineraryRequest = searchRequest.getItinerary();

        validateJsessionId(context);
        validatePassengers(itineraryRequest);
        validateSegments(itineraryRequest);
    }

    private void validateJsessionId(ResolverContext context) {
        RequestInfo requestInfo = context.getRequestInfo();
        String jsessionId = requestInfo.getHeaderOrCookie(SiteHeaders.OF_JSESSIONID, SiteCookies.OF_JSESSIONID);

        if (StringUtils.isEmpty(jsessionId)) {
            throw new ServiceException("Request does not contain header and cookie OF1JSESSIONID", Status.BAD_REQUEST, ErrorCodes.COOKIE_HEADER_INVALID, ServiceName.FRONTEND_API);
        }
    }

    private void validatePassengers(ItineraryRequest itineraryRequest) {
        Integer numAdults = itineraryRequest.getNumAdults();
        Integer numChildren = itineraryRequest.getNumChildren();
        Integer numInfants = itineraryRequest.getNumInfants();

        if (numAdults <= 0 || numChildren < 0 || numInfants < 0) {
            throw new ServiceException("Invalid passengers: Invalid number of passengers",
                    Status.BAD_REQUEST, ErrorCodes.VALIDATION, ServiceName.FRONTEND_API);
        }

        if (numAdults + numChildren + numInfants > 9) {
            throw new ServiceException("Invalid passengers: Too many passengers",
                    Status.BAD_REQUEST, ErrorCodes.VALIDATION, ServiceName.FRONTEND_API);
        }

        if (numChildren > numAdults * 2) {
            throw new ServiceException("Invalid passengers: Too many children",
                    Status.BAD_REQUEST, ErrorCodes.VALIDATION, ServiceName.FRONTEND_API);
        }

        if (numAdults < numInfants) {
            throw new ServiceException("Invalid passengers: Too many infants",
                    Status.BAD_REQUEST, ErrorCodes.VALIDATION, ServiceName.FRONTEND_API);
        }
    }

    private void validateSegments(ItineraryRequest itineraryRequest) {
        LocalDate prevDate = null;

        for (SegmentRequest segmentRequest : itineraryRequest.getSegments()) {
            validateLocation(segmentRequest.getDeparture());
            validateLocation(segmentRequest.getDestination());

            try {
                String date = segmentRequest.getDate();
                LocalDate currentDate = LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);

                if (Objects.nonNull(prevDate) && prevDate.isAfter(currentDate)) {
                    throw new ServiceException("Invalid date: previous segment date is greater",
                            Status.BAD_REQUEST, ErrorCodes.VALIDATION, ServiceName.FRONTEND_API);
                }

                prevDate = currentDate;

            } catch (DateTimeParseException ex) {
                throw new ServiceException("Invalid date: format is not valid", ex,
                        Status.BAD_REQUEST, ErrorCodes.VALIDATION);
            }
        }
    }

    private void validateLocation(LocationRequest locationRequest) {
        String iata = locationRequest.getIata();
        Integer geoId = locationRequest.getGeoNodeId();

        if (StringUtils.isEmpty(iata) && Objects.isNull(geoId)) {
            throw new ServiceException("Invalid location: iata or geonode shouldnt be null",
                    Status.BAD_REQUEST, ErrorCodes.VALIDATION, ServiceName.FRONTEND_API);
        }
    }

}
