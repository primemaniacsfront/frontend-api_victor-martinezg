package com.odigeo.frontend.resolvers.accommodation.rooms.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRoomAvailabilityResponse;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.AccommodationRoomsResponseDTO;
import org.mapstruct.Mapper;

@Mapper
public interface SearchRoomAvailabilityResponseMapper {

    SearchRoomAvailabilityResponse mapModelToContract(AccommodationRoomsResponseDTO response);
}
