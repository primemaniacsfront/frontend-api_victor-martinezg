package com.odigeo.frontend.resolvers.trip;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Message;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MessageFilter;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Trip;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripFilter;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.trip.bookingimportantmessage.BookingImportantMessageHandler;
import com.odigeo.frontend.resolvers.trip.handlers.TripHandler;
import graphql.execution.DataFetcherResult;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;

import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;

@Singleton
public class TripResolver implements Resolver {

    private static final String GET_TRIP_QUERY = "getTrip";
    private static final String TRIP_QUERY = "trip";
    private static final String GET_TRIP_BY_TOKEN_QUERY = "getTripByToken";
    private static final String MESSAGE_QUERY = "messages";
    private static final String BOOKING_ID = "bookingId";
    private static final String EMAIL = "email";

    private final BookingImportantMessageHandler bookingImportantMessageHandler;
    private final TripHandler tripHandler;
    @Inject
    private final JsonUtils jsonUtils;

    @Inject
    public TripResolver(BookingImportantMessageHandler bookingImportantMessageHandler, TripHandler tripHandler, JsonUtils jsonUtils) {
        this.bookingImportantMessageHandler = bookingImportantMessageHandler;
        this.tripHandler = tripHandler;
        this.jsonUtils = jsonUtils;
    }

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", getQueryDataFetcher())
                .type("TripResponse", getMessageDataFetcher())
                .type("Message", getCancellationMessageDataFetcher());
    }

    private UnaryOperator<TypeRuntimeWiring.Builder> getQueryDataFetcher() {
        return typeWiring -> typeWiring
                .dataFetcher(GET_TRIP_QUERY, this::getTripResponse)
                .dataFetcher(GET_TRIP_BY_TOKEN_QUERY, this::getTripResponse);
    }

    private UnaryOperator<TypeRuntimeWiring.Builder> getMessageDataFetcher() {
        return typeWiring -> typeWiring
                .dataFetcher(TRIP_QUERY, this::getTrip)
                .dataFetcher(MESSAGE_QUERY, this::getCancellationMessages);
    }

    private UnaryOperator<TypeRuntimeWiring.Builder> getCancellationMessageDataFetcher() {
        return typeWiring -> typeWiring
                .typeResolver(env -> env.getSchema().getObjectType("CancellationMessage"));
    }

    DataFetcherResult<Trip> getTrip(DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        VisitInformation visitInformation = context.getVisitInformation();
        String email = jsonUtils.getParameterFromLocalContext(env, String.class, EMAIL).orElse(null);
        Long bookingId = jsonUtils.getParameterFromLocalContext(env, Long.class, BOOKING_ID).orElse(null);


        return new DataFetcherResult.Builder<>(tripHandler.getTrip(bookingId, email, visitInformation))
                .localContext(env.getArguments())
                .build();
    }

    DataFetcherResult<TripResponse> getTripResponse(DataFetchingEnvironment env) {
        return new DataFetcherResult.Builder<>(new TripResponse())
                .localContext(env.getArguments())
                .build();
    }

    List<Message> getCancellationMessages(DataFetchingEnvironment env) {
        Optional<String> token = jsonUtils.getParameterFromLocalContext(env, String.class, "token");
        MessageFilter messageFilter = jsonUtils.getParameterFromLocalContext(env, TripFilter.class, "filter")
                .map(TripFilter::getMessageFilter)
                .orElseGet(MessageFilter::new);
        if (token.isPresent()) {
            return bookingImportantMessageHandler.getAllMessagesForToken(token.orElse(null), messageFilter);
        } else {
            String email = jsonUtils.getParameterFromLocalContext(env, String.class, EMAIL).orElse(null);
            Long bookingId = jsonUtils.getParameterFromLocalContext(env, Long.class, BOOKING_ID).orElse(null);
            return bookingImportantMessageHandler.getAllMessagesForBookingIdAndEmail(bookingId, email, messageFilter);
        }
    }

}
