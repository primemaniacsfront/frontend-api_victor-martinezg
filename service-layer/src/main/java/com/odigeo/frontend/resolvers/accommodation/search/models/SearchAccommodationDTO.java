package com.odigeo.frontend.resolvers.accommodation.search.models;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ThreeValuedLogic;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.BoardTypeDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;

import java.math.BigDecimal;
import java.util.List;

public class SearchAccommodationDTO {

    private String key;
    private Integer dedupId;
    private BigDecimal score;
    private AccommodationPriceCalculatorDTO priceCalculator;
    private Money providerPrice;
    private PriceBreakdownDTO priceBreakdown;
    private String contentKey;
    private List<AccommodationReview> usersRates;
    private ThreeValuedLogic cancellationFree;
    private boolean paymentAtDestination;
    private AccommodationDealInformationDTO accommodationDeal;
    private List<String> accommodationFacilities;
    private AccommodationReview userReview;
    private Double distanceFromCityCenter;
    private Integer roomsLeft;
    private BoardTypeDTO boardType;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getDedupId() {
        return dedupId;
    }

    public void setDedupId(Integer dedupId) {
        this.dedupId = dedupId;
    }


    public AccommodationPriceCalculatorDTO getPriceCalculator() {
        return priceCalculator;
    }

    public void setPriceCalculator(AccommodationPriceCalculatorDTO priceCalculator) {
        this.priceCalculator = priceCalculator;
    }

    public PriceBreakdownDTO getPriceBreakdown() {
        return priceBreakdown;
    }

    public void setPriceBreakdown(PriceBreakdownDTO priceBreakdown) {
        this.priceBreakdown = priceBreakdown;
    }

    public List<AccommodationReview> getUsersRates() {
        return usersRates;
    }

    public void setUsersRates(List<AccommodationReview> usersRates) {
        this.usersRates = usersRates;
    }

    public Money getProviderPrice() {
        return providerPrice;
    }

    public void setProviderPrice(Money providerPrice) {
        this.providerPrice = providerPrice;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public ThreeValuedLogic getCancellationFree() {
        return cancellationFree;
    }

    public void setCancellationFree(ThreeValuedLogic cancellationFree) {
        this.cancellationFree = cancellationFree;
    }

    public boolean isPaymentAtDestination() {
        return paymentAtDestination;
    }

    public void setPaymentAtDestination(boolean paymentAtDestination) {
        this.paymentAtDestination = paymentAtDestination;
    }

    public String getContentKey() {
        return contentKey;
    }

    public void setContentKey(String contentKey) {
        this.contentKey = contentKey;
    }

    public AccommodationDealInformationDTO getAccommodationDeal() {
        return accommodationDeal;
    }

    public void setAccommodationDeal(AccommodationDealInformationDTO accommodationDeal) {
        this.accommodationDeal = accommodationDeal;
    }


    public List<String> getAccommodationFacilities() {
        return accommodationFacilities;
    }

    public void setAccommodationFacilities(List<String> accommodationFacilities) {
        this.accommodationFacilities = accommodationFacilities;
    }

    public AccommodationReview getUserReview() {
        return userReview;
    }

    public void setUserReview(AccommodationReview userReview) {
        this.userReview = userReview;
    }

    public Double getDistanceFromCityCenter() {
        return distanceFromCityCenter;
    }

    public void setDistanceFromCityCenter(Double distanceFromCityCenter) {
        this.distanceFromCityCenter = distanceFromCityCenter;
    }

    public Integer getRoomsLeft() {
        return roomsLeft;
    }

    public void setRoomsLeft(Integer roomsLeft) {
        this.roomsLeft = roomsLeft;
    }

    public BoardTypeDTO getBoardType() {
        return boardType;
    }

    public void setBoardType(BoardTypeDTO boardType) {
        this.boardType = boardType;
    }
}
