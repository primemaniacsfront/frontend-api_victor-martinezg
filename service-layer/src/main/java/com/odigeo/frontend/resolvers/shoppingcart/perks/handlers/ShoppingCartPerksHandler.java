package com.odigeo.frontend.resolvers.shoppingcart.perks.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerk;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerks;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerksType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.google.inject.Singleton;
import graphql.GraphQLContext;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerksType.ITINERARY_PERKS;

@Singleton
public class ShoppingCartPerksHandler extends PerksHandler<MembershipPerks> {

    @Override
    public MembershipPerks map(GraphQLContext graphQlContext) {
        List<MembershipPerk> perksList = Stream.of(MembershipPerksType.values())
                .map(perksType -> this.buildMembershipPerk(perksType, graphQlContext))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
        return new MembershipPerks(getPrimeDiscountFee(graphQlContext), perksList);
    }

    private Optional<MembershipPerk> buildMembershipPerk(MembershipPerksType membershipPerksType, GraphQLContext graphQLContext) {
        return ITINERARY_PERKS.equals(membershipPerksType)
                ? Optional.of(new MembershipPerk(
                        buildMoneyDiscount(graphQLContext),
                getPerksIfDiscountApplied(graphQLContext) != null,
                        membershipPerksType))
                : Optional.empty();
    }

    private Money buildMoneyDiscount(GraphQLContext graphQLContext) {
        return new Money(getPrimeDiscountFee(graphQLContext), getCurrency(graphQLContext));
    }

}
