package com.odigeo.frontend.resolvers.trip.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Trip;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.trip.mapper.TripMapper;
import com.odigeo.frontend.services.trip.TripService;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import com.odigeo.travelcompanion.v2.model.user.Brand;
import org.apache.commons.lang.LocaleUtils;

import java.util.Locale;
import java.util.Optional;

@Singleton
public class TripHandler {

    private final TripService tripService;
    private final TripMapper tripMapper;

    @Inject
    public TripHandler(TripMapper tripMapper, TripService tripService) {
        this.tripMapper = tripMapper;
        this.tripService = tripService;
    }

    public Trip getTrip(final Long bookingId, final String email, VisitInformation visitInformation) {
        Brand brand = Optional.ofNullable(visitInformation.getBrand())
            .map(Enum::name)
            .map(Brand::valueOf)
            .orElse(null);
        Locale locale = LocaleUtils.toLocale(visitInformation.getLocale());
        BookingDetail bookingDetail = tripService.getTrip(bookingId, locale, email, brand);
        return tripMapper.mapToTripContract(bookingDetail);
    }
}
