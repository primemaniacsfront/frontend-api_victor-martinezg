package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentRequest;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Set;

import static com.odigeo.frontend.commons.context.visit.types.Site.*;

@Singleton
public class RebookingHandler {

    private static final LocalDate MAX_DATE = LocalDate.of(2021, 9, 30);

    @Inject
    private DateUtils dateUtils;

    private static final Set<String> CARRIERS_WITH_FREE_REBOOKING = Sets.newHashSet(
        "BA", "FI", "AF", "KL", "DL", "LV", "EW", "LH", "LX", "OS", "SN", "EN", "QR", "UX", "SQ", "EI", "WY", "VS",
        "U2", "TO", "AT", "TX", "DE", "EY", "KF", "SS", "TN", "AZ", "TP", "KE", "PC", "AA", "JU", "0B"
    );

    private static final Set<String> CARRIERS_WITH_STEERING_RELIABLE = Sets.newHashSet(
        "0B", "3O", "AF", "AZ", "BT", "DE", "DL", "ET", "EW", "FB", "KL", "KM", "LG", "LS", "MS", "OU", "QF", "QR",
        "TB", "TK", "TO", "TU", "U2", "W6", "W9", "XK", "ZU"
    );
    private static final Set<String> CARRIERS_WITH_STEERING_REFUND = Sets.newHashSet(
        "1L", "2L", "4O", "5J", "6H", "6I", "7C", "7G", "7R", "8Y", "9C", "9M", "9U", "A3", "A9", "AC", "AF",
        "AK", "AM", "AR", "AS", "AV", "AZ", "B2", "BC", "BF", "BI", "BJ", "BL", "BR", "BT", "BV", "BW", "BX", "BY", "CA",
        "CI", "CX", "CY", "CZ", "DE", "DG", "DL", "DP", "DU", "E9", "EB", "EK", "EN", "EW", "EY", "F2", "F3", "FB", "FH",
        "FI", "FM", "FY", "G4", "G8", "GF", "GK", "GL", "GM", "GP", "GR", "H6", "HA", "HC", "HD", "HO", "HR", "HU", "HV",
        "HX", "I5", "IB", "IT", "IX", "J5", "JA", "JH", "JJ", "JL", "JU", "KC", "KE", "KL", "KM", "KP", "KQ", "KU", "LA",
        "LG", "LH", "LM", "LO", "LS", "LX", "LY", "ME", "MF", "MH", "MI", "MM", "MO", "MU", "MV", "N4", "NH", "NK", "NP",
        "NZ", "OS", "OZ", "P6", "PM", "PR", "QF", "QG", "QR", "RC", "RJ", "RS", "S4", "S7", "SC", "SK", "SN", "SP", "SQ",
        "SS", "SV", "T7", "TB", "TC", "TK", "TL", "TN", "TS", "TW", "U2", "U6", "UA", "UG", "VN", "VS", "VT", "VY", "WB",
        "WF", "WK", "WM", "WN", "WS", "X3", "XC", "XE", "XG", "XJ", "XK", "YU", "Z2", "ZL", "ZU"
    );

    private static final Set<Site> SITES_WITH_FREE_REBOOKING = Sets.newHashSet(
        AT, AR, AU, CA, CH, DE, ES, FR, GB, GR, IT, MX, NL, NZ, PT, UK, US,
        GOFR, GOFRV2,
        OPAT, OPAU, OPCH, OPDE, OPDK2, OPES, OPFI2, OPFR, OPGB, OPIT, OPNL, OPNO2, OPSE2, OPUK,
        TRDE, TRDK, TRFI, TRNO, TRSE,

        CZ, EG, HK, HU, ID, IN, PH, SG, TH, ZA, MA,
        OPAE, SA, QA
    );

    public Boolean isRebookingAvailable(SearchRequest searchRequest, VisitInformation visit) {
        return SITES_WITH_FREE_REBOOKING.contains(visit.getSite())
            && areDatesIncluded(searchRequest);
    }

    private boolean areDatesIncluded(SearchRequest searchRequest) {
        return searchRequest.getItinerary().getSegments().stream()
            .map(SegmentRequest::getDate)
            .map(dateUtils::convertFromIsoToLocalDate)
            .allMatch(searchDate -> dateUtils.isBeforeOrEquals(searchDate, MAX_DATE));
    }

    public Set<String> getCarriersWithRebooking(SearchRequest searchRequestDTO, VisitInformation visit) {
        return isRebookingAvailable(searchRequestDTO, visit)
            ? CARRIERS_WITH_FREE_REBOOKING
            : Collections.emptySet();
    }

    public void populateHasFreeRebooking(SearchItineraryDTO itinerary, SearchRequest searchRequest,
                                         VisitInformation visit) {

        boolean hasFreeRebooking = isRebookingAvailable(searchRequest, visit)
            && CARRIERS_WITH_FREE_REBOOKING.containsAll(itinerary.getSegmentsCarrierIds());

        itinerary.setHasFreeRebooking(hasFreeRebooking);
    }

    public void populateHasReliableCarriers(SearchItineraryDTO itinerary) {
        boolean hasReliableCarriers = CARRIERS_WITH_STEERING_RELIABLE.containsAll(itinerary.getSegmentsCarrierIds());

        itinerary.setHasReliableCarriers(hasReliableCarriers);
    }

    public void populateHasRefundCarriers(SearchItineraryDTO itinerary) {
        boolean hasRefundCarriers = CARRIERS_WITH_STEERING_REFUND.containsAll(itinerary.getSegmentsCarrierIds());

        itinerary.setHasRefundCarriers(hasRefundCarriers);
    }

}
