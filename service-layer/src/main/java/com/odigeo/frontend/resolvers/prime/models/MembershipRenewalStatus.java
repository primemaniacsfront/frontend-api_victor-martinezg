package com.odigeo.frontend.resolvers.prime.models;

public enum MembershipRenewalStatus {
    ENABLED, DISABLED
}
