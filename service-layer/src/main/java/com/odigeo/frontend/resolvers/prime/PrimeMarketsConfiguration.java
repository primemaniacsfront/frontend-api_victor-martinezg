package com.odigeo.frontend.resolvers.prime;

import com.edreams.configuration.ConfiguredInJsonFile;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.prime.models.PrimeMarket;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
@ConfiguredInJsonFile
public class PrimeMarketsConfiguration {
    private Map<Site, PrimeMarket> primeMarkets = new HashMap<>();

    public PrimeMarket getPrimeMarket(Site site) {
        return primeMarkets.get(site);
    }

    public void setPrimeMarkets(Map<Site, PrimeMarket> primeMarkets) {
        this.primeMarkets = primeMarkets;
    }
}
