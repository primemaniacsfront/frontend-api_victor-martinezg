package com.odigeo.frontend.resolvers.accommodation.search.models;

public enum FilterType {
    PROPERTY_TYPE,
    FACILITY,
    NEIGHBOURHOOD,
    HEALTH,
    STARS,
    RESERVATION_POLICY,
    GUEST_RATING,
    DISTANCE_TO_CENTER,
    OTHER;

}
