package com.odigeo.frontend.resolvers.user.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ChangePasswordWithCodeRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.Brand;
import com.odigeo.userprofiles.api.v2.model.SSOToken;
import com.odigeo.userprofiles.api.v2.model.TrafficInterface;
import com.odigeo.userprofiles.api.v2.model.User;
import com.odigeo.userprofiles.api.v2.model.requests.PasswordChangeRequest;
import com.odigeo.userprofiles.api.v2.model.requests.credentials.PasswordUserCredentials;
import com.odigeo.userprofiles.api.v2.model.requests.credentials.SetPassTokenUserCredentials;
import com.odigeo.userprofiles.api.v2.model.requests.credentials.UserCredentials;
import graphql.schema.DataFetchingEnvironment;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Optional;

@Singleton
public class UserPasswordHandler {
    @Inject
    private UserDescriptionService userDescriptionService;
    private static final Boolean PERSISTENT_LOGIN_DEFAULT = false;

    public User changePasswordWithCode(ChangePasswordWithCodeRequest request, DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        VisitInformation visitInformation = env.getGraphQlContext().get(VisitInformation.class);

        return Optional.ofNullable(request)
                .map(changePasswordWithCodeRequest -> changePassword(changePasswordWithCodeRequest, visitInformation, context))
                .orElse(null);
    }

    private User changePassword(ChangePasswordWithCodeRequest changePasswordWithCodeRequest, VisitInformation visitInformation, ResolverContext context) {
        PasswordChangeRequest passwordChangeRequest = getPasswordChangeRequest(visitInformation, changePasswordWithCodeRequest);
        User user = setUserPassword(passwordChangeRequest);
        SSOToken ssoToken = generateToken(user, passwordChangeRequest, changePasswordWithCodeRequest.getPersistentLogin(), visitInformation);
        SSOCookieHandler.createSSOCookie(context, ssoToken, changePasswordWithCodeRequest.getPersistentLogin());
        return user;
    }

    private PasswordChangeRequest getPasswordChangeRequest(VisitInformation visitInformation, ChangePasswordWithCodeRequest changePasswordWithCodeRequest) {
        SetPassTokenUserCredentials setPassTokenUserCredentials = createSetPasswordTokenUserCredentials(visitInformation, changePasswordWithCodeRequest.getHashCode());
        return createPasswordChangeRequest(setPassTokenUserCredentials, changePasswordWithCodeRequest.getNewPassword());
    }

    private User setUserPassword(PasswordChangeRequest passwordChangeRequest) {
        return userDescriptionService.setUserPassword(passwordChangeRequest);
    }

    private SSOToken generateToken(User user, PasswordChangeRequest passwordChangeRequest, Boolean persistentLogin, VisitInformation visitInformation) {
        UserCredentials userCredentials = getUserCredentialsFromChangePasswordResponse(
                passwordChangeRequest.getNewPassword(), user.getEmail(), persistentLogin, visitInformation
        );
        return userDescriptionService.generateToken(userCredentials);
    }

    private UserCredentials getUserCredentialsFromChangePasswordResponse(String newPassword, String email, Boolean persistentLogin, VisitInformation visitInformation) {
        PasswordUserCredentials userCredentials = new PasswordUserCredentials();
        userCredentials.setPassword(newPassword);
        userCredentials.setLogin(email);
        userCredentials.setPersistentLogin(ObjectUtils.defaultIfNull(persistentLogin, PERSISTENT_LOGIN_DEFAULT));
        userCredentials.setBrand(Brand.fromString(visitInformation.getBrand().name()));
        userCredentials.setTrafficInterface(getTrafficInterface(visitInformation));
        userCredentials.setWebsite(visitInformation.getSite().name());
        userCredentials.setLocale(visitInformation.getLocale());
        return userCredentials;
    }

    private SetPassTokenUserCredentials createSetPasswordTokenUserCredentials(VisitInformation visitInformation, String hashCode) {
        SetPassTokenUserCredentials setPassTokenUserCredentials = new SetPassTokenUserCredentials();
        setPassTokenUserCredentials.setBrand(Brand.fromString(visitInformation.getBrand().name()));
        setPassTokenUserCredentials.setTrafficInterface(getTrafficInterface(visitInformation));
        setPassTokenUserCredentials.setToken(hashCode);
        return setPassTokenUserCredentials;
    }

    private PasswordChangeRequest createPasswordChangeRequest(UserCredentials userCredentials, String newPassword) {
        PasswordChangeRequest passwordChangeRequest = new PasswordChangeRequest();
        passwordChangeRequest.setCredentials(userCredentials);
        passwordChangeRequest.setNewPassword(newPassword);
        return passwordChangeRequest;
    }

    private TrafficInterface getTrafficInterface(VisitInformation visit) {
        switch (visit.getWebInterface()) {
        case ONE_FRONT_DESKTOP:
            return TrafficInterface.ONE_FRONT_DESKTOP;
        case ONE_FRONT_SMARTPHONE:
            return TrafficInterface.ONE_FRONT_SMARTPHONE;
        default:
            return TrafficInterface.OTHERS;
        }
    }
}
