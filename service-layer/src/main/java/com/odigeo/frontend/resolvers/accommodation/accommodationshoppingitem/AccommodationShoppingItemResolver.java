package com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationShoppingItem;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.handlers.AccommodationShoppingItemHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;

@Singleton
public class AccommodationShoppingItemResolver implements Resolver {

    static final String SHOPPING_CART_TYPE = "ShoppingCart";
    static final String ACCOMMODATION_ITEM_FIELD = "accommodationShoppingItem";

    @Inject
    private AccommodationShoppingItemHandler handler;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type(TypeRuntimeWiring.newTypeWiring(SHOPPING_CART_TYPE)
                .dataFetcher(ACCOMMODATION_ITEM_FIELD, this::accommodationShoppingItemDataFetcher));
    }

    AccommodationShoppingItem accommodationShoppingItemDataFetcher(DataFetchingEnvironment env) {
        return handler.map(env.getGraphQlContext());
    }
}
