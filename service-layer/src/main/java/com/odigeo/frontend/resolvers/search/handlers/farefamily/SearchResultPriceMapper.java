package com.odigeo.frontend.resolvers.search.handlers.farefamily;

import com.google.inject.Singleton;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.product.ProductCategorySearchResult;
import com.odigeo.searchengine.v2.searchresults.product.itinerary.AbstractFareItinerary;
import com.odigeo.searchengine.v2.searchresults.product.itinerary.pricing.FareItineraryPriceCalculator;
import com.odigeo.searchengine.v2.searchresults.util.money.SingleCurrencyMoney;

import java.math.BigDecimal;
import java.util.Optional;

@Singleton
class SearchResultPriceMapper {

    private static final String ITINERARY_KEY = "ITINERARY";
    private static final SingleCurrencyMoney EMPTY_CURRENCY_MONEY = new SingleCurrencyMoney();

    public BigDecimal map(SearchResults searchResults) {
        return getFareItinerary(searchResults)
            .map(AbstractFareItinerary::getFareItineraryPriceCalculator)
            .map(FareItineraryPriceCalculator::getApparentPrice)
            .map(money -> money.isSingleMoney() ? (SingleCurrencyMoney) money : EMPTY_CURRENCY_MONEY)
            .map(SingleCurrencyMoney::getAmount)
            .orElse(null);
    }

    private Optional<AbstractFareItinerary> getFareItinerary(SearchResults searchResults) {
        return Optional.ofNullable(searchResults)
            .map(SearchResults::getCategoryResultsMap)
            .map(map -> map.get(ITINERARY_KEY))
            .map(ProductCategorySearchResult::getSearchItems)
            .map(searchResultItems -> searchResultItems.get(0))
            .map(item -> (AbstractFareItinerary) item);
    }

}
