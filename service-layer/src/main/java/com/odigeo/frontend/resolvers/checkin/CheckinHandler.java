package com.odigeo.frontend.resolvers.checkin;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinAvailability;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateCheckinResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.checkin.api.v1.AutomaticCheckInApiService;
import com.odigeo.checkin.api.v1.CheckinStatusApiService;
import com.odigeo.checkin.api.v1.exceptions.AutomaticCheckInException;
import com.odigeo.checkin.api.v1.model.CheckinNotAvailableException;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.resolvers.checkin.mappers.CheckinAvailabilityMapper;
import com.odigeo.frontend.resolvers.checkin.mappers.CheckinStatusMapper;
import com.odigeo.frontend.resolvers.checkin.mappers.CreateCheckinRequestMapper;
import com.odigeo.frontend.resolvers.checkin.models.CheckinQuery;
import org.apache.commons.lang.LocaleUtils;

import java.util.ArrayList;


@Singleton
public class CheckinHandler {

    @Inject
    private AutomaticCheckInApiService automaticCheckInApiService;
    @Inject
    private CheckinStatusApiService checkinStatusApiService;
    @Inject
    private CheckinStatusMapper statusMapper;
    @Inject
    private CheckinAvailabilityMapper availabilityMapper;
    @Inject
    private CreateCheckinRequestMapper createCheckinMapper;


    public CheckinStatus getCheckinStatus(CheckinQuery checkinQuery, VisitInformation visitInfo) throws AutomaticCheckInException {
        String format = visitInfo.getDevice() == Device.MOBILE ? "mobile" : null;

        try {
            return statusMapper.map(checkinStatusApiService.getCheckinStatus(
                    checkinQuery.getBookingID(),
                    LocaleUtils.toLocale(visitInfo.getLocale()),
                    format
            ));
        } catch (CheckinNotAvailableException e) {
            CheckinStatus status = new CheckinStatus();
            status.setSections(new ArrayList<>());
            return status;
        }
    }

    public CheckinAvailability getCheckinAvailability(CheckinQuery checkinRequest) throws AutomaticCheckInException {
        return availabilityMapper.map(automaticCheckInApiService.retrieveAvailability(checkinRequest.getBookingID(), checkinRequest.getCitizenship()));
    }

    public CreateCheckinResponse createCheckin(CheckinRequest checkinRequest, VisitInformation visitInfo) {

        try {
            automaticCheckInApiService.createAutomaticCheckIn(createCheckinMapper.map(checkinRequest, visitInfo.getLocale()));
        } catch (AutomaticCheckInException e) {
            return new CreateCheckinResponse(false);
        }

        return new CreateCheckinResponse(true);
    }

}
