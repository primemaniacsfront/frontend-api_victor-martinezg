package com.odigeo.frontend.resolvers.accommodation.commons.model;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImageType;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationImageQualityDTO;

public class AccommodationImageDTO {


    private String url;
    private String thumbnailUrl;
    private AccommodationImageType type;
    private AccommodationImageQualityDTO quality;

    public AccommodationImageDTO() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public AccommodationImageType getType() {
        return type;
    }

    public void setType(AccommodationImageType type) {
        this.type = type;
    }

    public AccommodationImageQualityDTO getQuality() {
        return quality;
    }

    public void setQuality(AccommodationImageQualityDTO quality) {
        this.quality = quality;
    }

}
