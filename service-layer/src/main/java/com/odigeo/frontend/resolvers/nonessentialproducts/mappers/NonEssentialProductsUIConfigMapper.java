package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsUIConfig;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import org.apache.commons.lang3.BooleanUtils;

@Singleton
public class NonEssentialProductsUIConfigMapper {

    @Inject
    private NonEssentialProductsModalMapper nonEssentialProductsModalMapper;
    @Inject
    private NagTypesMapper nagTypesMapper;
    @Inject
    private NonEssentialProductsThemeTypeMapper nonEssentialProductsThemeTypeMapper;

    public NonEssentialProductsUIConfig map(AncillaryConfiguration ancillaryConfiguration) {
        NonEssentialProductsUIConfig nonEssentialProductsUIConfig = new NonEssentialProductsUIConfig();

        nonEssentialProductsUIConfig.setModal(nonEssentialProductsModalMapper.map(ancillaryConfiguration));
        nonEssentialProductsUIConfig.setPrechecked(BooleanUtils.isTrue(ancillaryConfiguration.getPrechecked()));
        nonEssentialProductsUIConfig.setAvailableNags(nagTypesMapper.map(ancillaryConfiguration));
        nonEssentialProductsUIConfig.setTheme(nonEssentialProductsThemeTypeMapper.map(ancillaryConfiguration));

        return nonEssentialProductsUIConfig;
    }

}
