package com.odigeo.frontend.resolvers.checkin.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinAvailability;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinWindow;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredAdvancedPaxInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredCheckinAddress;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredCheckinResidentPermit;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredCheckinTravelPermissions;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredEmergencyContact;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredOnwardTicket;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredTravelDocument;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredVisaDetails;
import com.google.inject.Singleton;
import com.odigeo.checkin.api.v1.model.TravelDocumentType;
import com.odigeo.checkin.api.v1.model.response.AutomaticCheckInAvailability;
import com.odigeo.checkin.api.v1.model.response.CheckInWindow;
import com.odigeo.checkin.api.v1.model.response.RequiredArrivalDetails;
import com.odigeo.checkin.api.v1.model.response.RequiredContactDetails;
import com.odigeo.checkin.api.v1.model.response.RequiredOnwardFlight;
import com.odigeo.checkin.api.v1.model.response.RequiredPassengerDetails;
import com.odigeo.checkin.api.v1.model.response.RequiredResidentPermit;
import com.odigeo.checkin.api.v1.model.response.RequiredTravelDocumentDetails;
import com.odigeo.checkin.api.v1.model.response.RequiredTravelPermissions;

import java.util.ArrayList;
import java.util.Optional;

@Singleton
public class CheckinAvailabilityMapper {

    public CheckinAvailability map(AutomaticCheckInAvailability availabilityResponse) {
        CheckinAvailability availability = new CheckinAvailability();
        availability.setAvailable(availabilityResponse.isAvailable());
        if (availabilityResponse.isAvailable()) {
            availability.setCheckinWindow(mapCheckinWindow(availabilityResponse.getCheckInWindow()));
            Optional.ofNullable(availabilityResponse.getRequiredPassengerDetails())
                    .ifPresent(paxDetails -> {
                        availability.setPaxInfo(mapRequiredPaxInfo(paxDetails));
                        availability.setNationalId(mapTravelDocument(
                                TravelDocumentType.NATIONAL_ID,
                                paxDetails.getRequiredTravelDocumentDetails().get(TravelDocumentType.NATIONAL_ID)
                        ));
                        availability.setPassport(mapTravelDocument(
                                TravelDocumentType.PASSPORT,
                                paxDetails.getRequiredTravelDocumentDetails().get(TravelDocumentType.PASSPORT)
                        ));
                        availability.setAddress(mapAddress(paxDetails.getRequiredArrivalDetails()));
                        availability.setTravelPermissions(mapTravelPermissions(paxDetails.getRequiredTravelPermissions()));
                    });
        }

        return availability;
    }

    private CheckinWindow mapCheckinWindow(CheckInWindow window) {
        return Optional.ofNullable(window)
                .map(inWindow -> {
                    CheckinWindow checkinWindow = new CheckinWindow();
                    checkinWindow.setClosingTimeInMinutes(inWindow.getClosingTimeInMinutes());
                    checkinWindow.setOpeningTimeInMinutes(inWindow.getOpeningTimeInMinutes());
                    checkinWindow.setDataSyncOffsetInMinutes(inWindow.getDataSyncOffset());
                    return checkinWindow;
                }).orElse(null);
    }

    private RequiredAdvancedPaxInfo mapRequiredPaxInfo(RequiredPassengerDetails requiredPassengerDetails) {
        return Optional.ofNullable(requiredPassengerDetails)
                .map(requiredPaxDetails -> {
                    RequiredAdvancedPaxInfo advPaxInfo = new RequiredAdvancedPaxInfo();
                    advPaxInfo.setGender(requiredPaxDetails.isGender());
                    advPaxInfo.setDateOfBirth(requiredPaxDetails.isDateOfBirth());
                    advPaxInfo.setLocale(requiredPaxDetails.isLocale());
                    advPaxInfo.setMobilePhone(requiredPaxDetails.isMobilePhone());
                    advPaxInfo.setEmail(requiredPaxDetails.isEmail());
                    advPaxInfo.setCitizenship(requiredPaxDetails.isCitizenship());
                    advPaxInfo.setCountryOfResidence(requiredPaxDetails.isCountryOfResidence());
                    advPaxInfo.setEmergencyContact(mapEmergencyContact(requiredPaxDetails.getRequiredContactDetails()));
                    return advPaxInfo;
                }).orElse(null);
    }

    private RequiredEmergencyContact mapEmergencyContact(RequiredContactDetails contactDetails) {
        return Optional.ofNullable(contactDetails)
                .map(details -> {
                    RequiredEmergencyContact emergencyContact = new RequiredEmergencyContact();
                    emergencyContact.setFullName(details.isFullName());
                    emergencyContact.setPhone(details.isPhone());
                    emergencyContact.setRelation(details.isRelation());
                    return emergencyContact;
                }).orElse(null);
    }

    private RequiredTravelDocument mapTravelDocument(TravelDocumentType type, RequiredTravelDocumentDetails travelDocumentDetails) {
        return Optional.ofNullable(travelDocumentDetails)
                .map(documentDetails -> {
                    RequiredTravelDocument travelDocument = new RequiredTravelDocument();
                    travelDocument.setType(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravelDocumentType.valueOf(type.name()));
                    travelDocument.setNumber(documentDetails.isNumber());
                    travelDocument.setMonthsDocumentValidity(documentDetails.getMonthsDocumentValidity() != null ? documentDetails.getMonthsDocumentValidity() : 0);
                    travelDocument.setExpirationDate(documentDetails.isExpiration());
                    travelDocument.setIssueDate(documentDetails.isIssued());
                    travelDocument.setIssuingCountry(documentDetails.isIssuingCountry());
                    return travelDocument;
                }).orElse(null);
    }

    private RequiredCheckinAddress mapAddress(RequiredArrivalDetails arrivalDetails) {
        return Optional.ofNullable(arrivalDetails)
                .map(requiredArrivalDetails -> {
                    RequiredCheckinAddress address = new RequiredCheckinAddress();
                    address.setState(requiredArrivalDetails.isState());
                    address.setCity(requiredArrivalDetails.isCity());
                    address.setAddress(requiredArrivalDetails.isAddress());
                    address.setZip(requiredArrivalDetails.isZip());
                    return address;
                }).orElse(null);
    }

    private RequiredCheckinTravelPermissions mapTravelPermissions(RequiredTravelPermissions requiredTravelPermissions) {
        return Optional.ofNullable(requiredTravelPermissions)
                .map(travelPermissions -> {
                    RequiredCheckinTravelPermissions permissions = new RequiredCheckinTravelPermissions();
                    permissions.setNoVisaReason(travelPermissions.isNoVisaReason());
                    permissions.setOnwardTicket(mapOnwardTicket(travelPermissions.getRequiredOnwardFlight()));
                    permissions.setResidentPermit(mapResidentPermit(travelPermissions.getRequiredResidentPermit()));
                    permissions.setVisaDetails(mapVisaDetails(travelPermissions.getRequiredVisaDetails()));
                    return permissions;
                }).orElse(null);
    }

    private RequiredOnwardTicket mapOnwardTicket(RequiredOnwardFlight flight) {
        return Optional.ofNullable(flight)
                .map(onwardFlight -> {
                    RequiredOnwardTicket ticket = new RequiredOnwardTicket();
                    ticket.setId(onwardFlight.isNumber());
                    ticket.setDate(onwardFlight.isDate());
                    ticket.setDestination(onwardFlight.isAirportDestination());
                    return ticket;
                }).orElse(null);
    }

    private RequiredCheckinResidentPermit mapResidentPermit(RequiredResidentPermit residentPermit) {
        return Optional.ofNullable(residentPermit)
                .map(requiredResidentPermit -> {
                    RequiredCheckinResidentPermit checkinResidentPermit = new RequiredCheckinResidentPermit();
                    checkinResidentPermit.setExpirationDate(requiredResidentPermit.isExpiration());
                    checkinResidentPermit.setExpires(requiredResidentPermit.isExpires());
                    checkinResidentPermit.setIssuingCountry(requiredResidentPermit.isIssueCountry());
                    checkinResidentPermit.setNumber(requiredResidentPermit.isNumber());
                    return checkinResidentPermit;
                }).orElse(null);
    }

    private RequiredVisaDetails mapVisaDetails(com.odigeo.checkin.api.v1.model.response.RequiredVisaDetails visaDetails) {
        return Optional.ofNullable(visaDetails)
                .map(requiredVisaDetails -> {
                    RequiredVisaDetails visa = new RequiredVisaDetails();
                    visa.setPossibleTypes(new ArrayList<>(requiredVisaDetails.getPossibleTypes()));
                    visa.setNumber(requiredVisaDetails.isNumber());
                    visa.setExpirationDate(requiredVisaDetails.isExpiration());
                    visa.setIssueCountry(requiredVisaDetails.isIssueCountry());
                    visa.setIssueDate(requiredVisaDetails.isIssueDate());
                    return visa;
                }).orElse(null);
    }
}
