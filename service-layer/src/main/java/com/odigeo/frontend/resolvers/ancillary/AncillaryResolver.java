package com.odigeo.frontend.resolvers.ancillary;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AncillaryResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedAncillariesResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectAncillariesRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.ancillary.handlers.AncillaryHandler;
import com.odigeo.itineraryapi.v1.request.AncillariesServicesType;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.HashSet;
import java.util.Set;

@Singleton
public class AncillaryResolver implements Resolver {
    private static final String ANCILLARY_OPTIONS_QUERY = "ancillaryOptions";
    private static final String SELECT_ANCILLARIES_QUERY = "selectAncillaries";
    private static final String SEATS = "seats";
    private static final String BOOKING_ID = "bookingID";

    @Inject
    private AncillaryHandler ancillaryHandler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query",
                typeWiring -> typeWiring.dataFetcher(ANCILLARY_OPTIONS_QUERY, this::ancillariesFetcher))
            .type("Mutation",
                typeWiring -> typeWiring.dataFetcher(SELECT_ANCILLARIES_QUERY, this::selectAncillariesFetcher));
    }

    AncillaryResponse ancillariesFetcher(DataFetchingEnvironment env) {

        long bookingID = jsonUtils.fromDataFetching(env, Long.class, BOOKING_ID);
        ResolverContext context = env.getContext();
        Set<AncillariesServicesType> ancillariesServices = new HashSet<>();
        if (env.getSelectionSet().contains(SEATS)) {
            ancillariesServices.add(AncillariesServicesType.SEATS);
        }

        return ancillaryHandler.getAncillaries(
            bookingID,
            ancillariesServices,
            context
        );
    }

    SelectedAncillariesResponse selectAncillariesFetcher(DataFetchingEnvironment env) {

        ResolverContext context = env.getContext();
        SelectAncillariesRequest selectAncillariesRequest = jsonUtils.fromDataFetching(env, SelectAncillariesRequest.class);

        return ancillaryHandler.selectAncillaries(
                selectAncillariesRequest,
                context
        );
    }
}
