package com.odigeo.frontend.resolvers.checkin.models;

public class CheckinQuery {
    private Long bookingID;
    private String citizenship;

    public CheckinQuery(Long bookingID, String citizenship) {
        this.bookingID = bookingID;
        this.citizenship = citizenship;
    }

    public Long getBookingID() {
        return bookingID;
    }

    public void setBookingID(Long bookingID) {
        this.bookingID = bookingID;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }
}
