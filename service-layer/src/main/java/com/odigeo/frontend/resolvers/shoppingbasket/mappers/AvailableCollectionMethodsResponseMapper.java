package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CollectionMethod;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCardType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.odigeo.shoppingbasket.v3.model.CreditCardCustomerCollectionOption;
import com.odigeo.shoppingbasket.v3.model.CustomerCollectionOptions;

import java.util.ArrayList;
import java.util.List;

public class AvailableCollectionMethodsResponseMapper {
    public AvailableCollectionMethodsResponse map(CustomerCollectionOptions availableCollectionMethods) {
        AvailableCollectionMethodsResponse response = new AvailableCollectionMethodsResponse();
        List<CollectionMethod> collectionMethods = new ArrayList<>();
        availableCollectionMethods.getCreditCardCustomerCollectionOptions().stream().map(this::buildFromCreditCardCollectionOption).forEach(collectionMethods::add);
        response.setCollectionOptions(collectionMethods);
        return response;
    }

    private CollectionMethod buildFromCreditCardCollectionOption(CreditCardCustomerCollectionOption creditCardCustomerCollectionOption) {
        CollectionMethod collectionMethod = new CollectionMethod();
        collectionMethod.setType(PaymentMethod.CREDITCARD);
        collectionMethod.setCreditCardType(new CreditCardType(creditCardCustomerCollectionOption.getCategory(), creditCardCustomerCollectionOption.getScheme()));
        return collectionMethod;
    }
}
