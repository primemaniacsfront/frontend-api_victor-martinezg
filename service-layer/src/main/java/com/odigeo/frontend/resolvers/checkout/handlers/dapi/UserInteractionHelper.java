package com.odigeo.frontend.resolvers.checkout.handlers.dapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeToken;

@Singleton
public class UserInteractionHelper {

    @VisibleForTesting
    static final int STEP = 1;
    @VisibleForTesting
    static final String RETURN_URL_TAG = "#FINISH_URL#";
    @VisibleForTesting
    static final String SLASH = "/";
    @VisibleForTesting
    static final String CC_PAYMENT_METHOD = "creditcard";
    @VisibleForTesting
    static final String ALT_PAYMENT_METHOD = "alternative";
    @VisibleForTesting
    static final String RETURN_URL = "service/userinteractionreturn";
    @VisibleForTesting
    static final String SCHEME = "https://";

    public String buildReturnUrl(CheckoutRequest request, String serverName, String contextPath, String userPaymentInteractionId) {
        CheckoutResumeToken token = new CheckoutResumeToken(Long.parseLong(request.getShoppingId()), request.getShoppingType(),
                getPaymentMethodForUrl(request.getCollectionMethod().getCollectionMethodType()), STEP, userPaymentInteractionId);
        return SCHEME + serverName + contextPath + SLASH + RETURN_URL + SLASH + token;
    }

    private String getPaymentMethodForUrl(PaymentMethod paymentMethod) {
        if (paymentMethod.equals(PaymentMethod.CREDITCARD)) {
            return CC_PAYMENT_METHOD;
        }
        return ALT_PAYMENT_METHOD;
    }

    public String getReturnUrlTag() {
        return RETURN_URL_TAG;
    }

}
