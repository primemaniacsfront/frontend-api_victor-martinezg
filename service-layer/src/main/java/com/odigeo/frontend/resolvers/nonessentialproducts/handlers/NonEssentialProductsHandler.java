package com.odigeo.frontend.resolvers.nonessentialproducts.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductOffer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsOfferResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductIdRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectNonEssentialProductsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketActionStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.AvailableProductsResponse;
import com.odigeo.dapi.client.Insurance;
import com.odigeo.dapi.client.InsuranceOffer;
import com.odigeo.dapi.client.Insurances;
import com.odigeo.dapi.client.OtherProductsShoppingItems;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsGroupsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.mappers.NonEssentialProductOfferMapper;
import com.odigeo.frontend.resolvers.nonessentialproducts.mappers.SelectNonEssentialProductsResponseMapper;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Singleton
public class NonEssentialProductsHandler {

    @Inject
    private NonEssentialProductOfferMapper nonEssentialProductOfferMapper;
    @Inject
    private NonEssentialProductsGroupsConfiguration nonEssentialProductsGroupsConfiguration;
    @Inject
    private SelectNonEssentialProductsResponseMapper selectNonEssentialProductsResponseMapper;
    @Inject
    private ShoppingCartHandler shoppingCartHandler;

    public SelectNonEssentialProductsResponse getSelectNonEssentialProductsResponse(List<String> productsPolicies) {
        return selectNonEssentialProductsResponseMapper.map(productsPolicies);
    }

    public NonEssentialProductsOfferResponse getAllNonEssentialProductsOffers(Long bookingId, ResolverContext context) {
        NonEssentialProductsOfferResponse nonEssentialProductsOfferResponse = new NonEssentialProductsOfferResponse();

        List<InsuranceOffer> insuranceOffers = getAllAvailableInsuranceOffers(bookingId, context);

        nonEssentialProductsOfferResponse.setInsurances(Collections.emptyList());
        nonEssentialProductsOfferResponse.setRebooking(getRebookingOffers(insuranceOffers, context.getVisitInformation()));
        nonEssentialProductsOfferResponse.setGuarantees(getGuaranteesOffers(insuranceOffers, context.getVisitInformation()));
        nonEssentialProductsOfferResponse.setServiceOptions(Collections.emptyList());
        nonEssentialProductsOfferResponse.setAdditions(Collections.emptyList());
        nonEssentialProductsOfferResponse.setFareRules(Collections.emptyList());

        return nonEssentialProductsOfferResponse;
    }

    public List<NonEssentialProductOffer> getGuaranteesOffers(List<InsuranceOffer> insuranceOffers, VisitInformation visitInformation) {
        Predicate<Insurance> insurancePredicate = insurance -> nonEssentialProductsGroupsConfiguration.isGuarantee(insurance.getPolicy());
        return getNonEssentialProductsByPredicate(insuranceOffers, insurancePredicate, visitInformation);
    }

    public List<NonEssentialProductOffer> getRebookingOffers(List<InsuranceOffer> insuranceOffers, VisitInformation visitInformation) {
        Predicate<Insurance> insurancePredicate = insurance -> nonEssentialProductsGroupsConfiguration.isRebooking(insurance.getPolicy());
        return getNonEssentialProductsByPredicate(insuranceOffers, insurancePredicate, visitInformation);
    }

    private List<InsuranceOffer> getAllAvailableInsuranceOffers(Long bookingId, ResolverContext context) {
        AvailableProductsResponse availableProductsResponse = shoppingCartHandler.getAvailableProductsFromDapi(bookingId, context);
        return getInsuranceOffers(availableProductsResponse);
    }

    public List<UpdateProductsInShoppingBasketActionStatus> getNonEssentialProductsActionStatus(List<String> nonEssentialProductsIdsToRemove,
                                                                                                ShoppingCartSummaryResponse shoppingCartSummaryResponse,
                                                                                                AvailableProductsResponse availableProductsResponse) {
        List<InsuranceOffer> insuranceOffers = getInsuranceOffers(availableProductsResponse);
        return Optional.ofNullable(insuranceOffers)
            .orElse(Collections.emptyList())
            .stream()
            .filter(insuranceOffer -> nonEssentialProductsIdsToRemove.contains(insuranceOffer.getId()))
            .map(insuranceOffer -> {
                ProductId productId = new ProductId(insuranceOffer.getInsurances().get(0).getPolicy(), ProductType.INSURANCE);
                return new UpdateProductsInShoppingBasketActionStatus(productId, getInsuranceFromShoppingCart(insuranceOffer.getId(), shoppingCartSummaryResponse));
            })
            .collect(Collectors.toList());
    }

    public List<String> getNonEssentialProductsIdsToRemove(UpdateProductsInShoppingBasketRequest updateShoppingBasketRequest, AvailableProductsResponse availableProductsResponse) {
        List<InsuranceOffer> insuranceOffers = getInsuranceOffers(availableProductsResponse);
        List<String> policiesToRemove = Optional.ofNullable(updateShoppingBasketRequest.getProductsToRemove())
                .orElse(Collections.emptyList())
                .stream()
                .map(ProductIdRequest::getId)
                .collect(Collectors.toList());

        return getProductIdFilterByPolicy(insuranceOffers, policiesToRemove);
    }

    public List<String> getNonEssentialProductsIdsToAdd(UpdateProductsInShoppingBasketRequest updateShoppingBasketRequest, AvailableProductsResponse availableProductsResponse) {
        List<InsuranceOffer> insuranceOffers = getInsuranceOffers(availableProductsResponse);
        List<String> policiesToAdd = Optional.ofNullable(updateShoppingBasketRequest.getProductsToAdd())
            .orElse(Collections.emptyList())
            .stream()
            .map(ProductIdRequest::getId)
            .collect(Collectors.toList());

        return getProductIdFilterByPolicy(insuranceOffers, policiesToAdd);
    }

    private boolean getInsuranceFromShoppingCart(String productId, ShoppingCartSummaryResponse shoppingCartResponse) {
        return Optional.ofNullable(shoppingCartResponse)
            .map(ShoppingCartSummaryResponse::getShoppingCart)
            .map(com.odigeo.dapi.client.ShoppingCart::getOtherProductsShoppingItemContainer)
            .map(OtherProductsShoppingItems::getInsuranceShoppingItems)
            .orElse(Collections.emptyList())
            .stream()
            .anyMatch(insuranceShoppingItem -> insuranceShoppingItem.getId().equals(productId));
    }

    private List<NonEssentialProductOffer> getNonEssentialProductsByPredicate(List<InsuranceOffer> insuranceOffers, Predicate<Insurance> insurancePredicate, VisitInformation visitInformation) {
        List<InsuranceOffer> insuranceOfferList = getInsurancesFilterByPredicate(insuranceOffers, insurancePredicate);
        return insuranceOfferList
            .stream()
            .map(insuranceOffer -> nonEssentialProductOfferMapper.map(insuranceOffer, visitInformation))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    private List<InsuranceOffer> getInsurancesFilterByPredicate(List<InsuranceOffer> insuranceOffers, Predicate<Insurance> insurancePredicate) {
        return Optional.ofNullable(insuranceOffers)
            .orElse(Collections.emptyList())
            .stream()
            .filter(insuranceOffer -> Optional.ofNullable(insuranceOffer)
                .map(InsuranceOffer::getInsurances)
                .orElse(Collections.emptyList())
                .stream()
                .anyMatch(insurancePredicate)
            )
            .collect(Collectors.toList());
    }

    private List<InsuranceOffer> getInsuranceOffers(AvailableProductsResponse availableProductsResponse) {
        return Optional.ofNullable(availableProductsResponse)
                .map(AvailableProductsResponse::getInsurances)
                .map(Insurances::getInsuranceOffers)
                .orElse(Collections.emptyList());
    }

    private List<String> getProductIdFilterByPolicy(List<InsuranceOffer> insuranceOffers, List<String> requestedProductsPolicies) {
        return Optional.ofNullable(insuranceOffers)
            .orElse(Collections.emptyList())
            .stream()
            .filter(insuranceOffer -> Optional.ofNullable(insuranceOffer)
                .map(InsuranceOffer::getInsurances)
                .orElse(Collections.emptyList())
                .stream()
                .anyMatch(insurance -> requestedProductsPolicies.contains(insurance.getPolicy()))
            )
            .map(InsuranceOffer::getId)
            .collect(Collectors.toList());
    }
}
