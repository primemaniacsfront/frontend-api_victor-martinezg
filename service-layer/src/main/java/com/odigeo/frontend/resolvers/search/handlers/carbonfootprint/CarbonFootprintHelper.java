package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.ItineraryCo2FootprintResult;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.response.ItineraryFootprintCalculatorResponse;
import com.google.inject.Singleton;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Singleton
class CarbonFootprintHelper {

    private static final int ONE_HUNDRED = -100;
    private static final int ECO_THRESHOLD = 9;

    public BigDecimal getSearchKilosAverage(ItineraryFootprintCalculatorResponse response) {

        return BigDecimal.valueOf(Optional.ofNullable(response.getItineraryCo2FootprintResults())
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(ItineraryCo2FootprintResult::getKilosCo2e)
                .filter(Objects::nonNull)
                .mapToDouble(Number::doubleValue).average().orElse(0));
    }

    public int calculatePercentageEco(BigDecimal totalKiloseItinerary, BigDecimal averageKilosSearch) {
        double percentage = ((totalKiloseItinerary.doubleValue() / averageKilosSearch.doubleValue()) - 1) * ONE_HUNDRED;
        return (int) (percentage >= ECO_THRESHOLD ? percentage : 0);
    }
}
