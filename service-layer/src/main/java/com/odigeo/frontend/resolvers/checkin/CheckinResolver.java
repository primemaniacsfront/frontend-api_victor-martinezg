package com.odigeo.frontend.resolvers.checkin;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinAvailability;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateCheckinResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.checkin.api.v1.exceptions.AutomaticCheckInException;
import com.odigeo.checkin.api.v1.model.CheckinNotAvailableException;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.checkin.models.CheckinQuery;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;


@Singleton
public class CheckinResolver implements Resolver {

    private static final String BOOKING_ID = "bookingID";
    private static final String CITIZENSHIP = "citizenship";
    private static final String CHECKIN_STATUS_QUERY = "checkinStatus";
    private static final String CHECKIN_AVAILABILITY_QUERY = "checkinAvailability";
    private static final String CREATE_CHECKIN_MUTATION = "createCheckinRequest";

    @Inject
    private CheckinHandler checkinHandler;
    @Inject
    private JsonUtils jsonUtils;


    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", typeWiring -> typeWiring.dataFetcher(CHECKIN_AVAILABILITY_QUERY, this::checkinAvailabilityFetcher))
                .type("Query", typeWiring -> typeWiring.dataFetcher(CHECKIN_STATUS_QUERY, this::checkinStatusFetcher))
                .type("Mutation", typeWiring -> typeWiring.dataFetcher(CREATE_CHECKIN_MUTATION, this::createCheckinFetcher));
    }

    CheckinAvailability checkinAvailabilityFetcher(DataFetchingEnvironment env) throws AutomaticCheckInException {
        return checkinHandler.getCheckinAvailability(createCheckinRequest(env));
    }

    CheckinStatus checkinStatusFetcher(DataFetchingEnvironment env) throws CheckinNotAvailableException, AutomaticCheckInException {
        ResolverContext context = env.getContext();
        VisitInformation visitInfo = context.getVisitInformation();

        return checkinHandler.getCheckinStatus(createCheckinRequest(env), visitInfo);
    }

    CreateCheckinResponse createCheckinFetcher(DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        VisitInformation visitInfo = context.getVisitInformation();
        CheckinRequest request = jsonUtils.fromDataFetching(env, CheckinRequest.class);

        return checkinHandler.createCheckin(request, visitInfo);
    }

    private CheckinQuery createCheckinRequest(DataFetchingEnvironment env) {
        return new CheckinQuery(
                jsonUtils.fromDataFetching(env, Long.class, BOOKING_ID),
                jsonUtils.fromDataFetching(env, String.class, CITIZENSHIP)
        );
    }
}
