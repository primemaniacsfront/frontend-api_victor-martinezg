package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MoneyRequest;
import com.google.inject.Singleton;

import java.math.BigDecimal;

@Singleton
public class MoneyMapper {

    public Money map(com.odigeo.itineraryapi.v1.response.Money money) {
        Money moneyResult = new Money();

        moneyResult.setAmount(money.getAmount());
        moneyResult.setCurrency(money.getCurrencyCode());

        return moneyResult;
    }

    public com.odigeo.itineraryapi.v1.request.Money map(MoneyRequest money) {
        com.odigeo.itineraryapi.v1.request.Money moneyResult = new com.odigeo.itineraryapi.v1.request.Money();

        moneyResult.setAmount(BigDecimal.valueOf(money.getAmount()));
        moneyResult.setCurrencyCode(money.getCurrency());

        return moneyResult;
    }

}
