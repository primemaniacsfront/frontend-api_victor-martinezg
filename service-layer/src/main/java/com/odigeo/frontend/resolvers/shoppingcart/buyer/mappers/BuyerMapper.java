package com.odigeo.frontend.resolvers.shoppingcart.buyer.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Buyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BuyerInformationDescription;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface BuyerMapper {

    @Mapping(target = "dateOfBirth", dateFormat = "yyyy-MM-dd")
    Buyer map(com.odigeo.dapi.client.Buyer buyer);

    BuyerInformationDescription mapBuyerInformationDescription(com.odigeo.dapi.client.BuyerInformationDescription buyerInformationDescription);

}
