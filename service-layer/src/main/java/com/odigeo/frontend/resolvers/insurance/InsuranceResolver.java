package com.odigeo.frontend.resolvers.insurance;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOffersRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOffersResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectInsuranceOfferRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectInsuranceOfferResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.insurance.handlers.InsuranceHandler;
import com.odigeo.frontend.resolvers.insurance.handlers.InsuranceProductHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

@Singleton
public class InsuranceResolver implements Resolver {

    @Inject
    private InsuranceHandler insuranceHandler;
    @Inject
    private InsuranceProductHandler insuranceProductHandler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher("getInsuranceOffers", this::insuranceOffersFetcher))
            .type("Mutation", typeWiring -> typeWiring
                .dataFetcher("selectInsuranceOffer", this::selectInsuranceOfferFetcher));
    }

    InsuranceOffersResponse insuranceOffersFetcher(DataFetchingEnvironment env) {
        InsuranceOffersResponse insuranceOffersResponse = new InsuranceOffersResponse();

        ResolverContext context = env.getContext();
        InsuranceOffersRequest insuranceOffersRequest = jsonUtils.fromDataFetching(env, InsuranceOffersRequest.class);
        insuranceOffersResponse.setInsurances(
            insuranceHandler.getInsuranceOffers(insuranceOffersRequest.getBookingId(),
            context.getVisitInformation().getVisitCode()
            )
        );

        return insuranceOffersResponse;
    }

    SelectInsuranceOfferResponse selectInsuranceOfferFetcher(DataFetchingEnvironment env) {
        SelectInsuranceOfferResponse selectInsuranceOfferResponse = new SelectInsuranceOfferResponse();

        SelectInsuranceOfferRequest selectInsuranceOfferRequest = jsonUtils.fromDataFetching(env, SelectInsuranceOfferRequest.class);
        selectInsuranceOfferResponse.setProductId(
            insuranceProductHandler.selectInsuranceOfferRequest(selectInsuranceOfferRequest)
        );

        return selectInsuranceOfferResponse;
    }


}
