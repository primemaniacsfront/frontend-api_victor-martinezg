package com.odigeo.frontend.resolvers.nonessentialproducts.configuration;

import com.edreams.configuration.ConfiguredInJsonFile;
import com.google.inject.Singleton;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@Singleton
@ConfiguredInJsonFile
public class NonEssentialProductsGroupsConfiguration {

    private Set<String> guarantees;
    private Set<String> rebookings;

    public Set<String> getGuarantees() {
        return Optional.ofNullable(guarantees)
            .orElse(Collections.emptySet());
    }

    public boolean isGuarantee(String policy) {
        return getGuarantees().contains(policy);
    }

    public void setGuarantees(Set<String> guarantees) {
        this.guarantees = Collections.unmodifiableSet(guarantees);
    }

    public Set<String> getRebookings() {
        return Optional.ofNullable(rebookings)
            .orElse(Collections.emptySet());
    }

    public void setRebookings(Set<String> rebookings) {
        this.rebookings = Collections.unmodifiableSet(rebookings);
    }

    public boolean isRebooking(String policy) {
        return getRebookings().contains(policy);
    }
}
