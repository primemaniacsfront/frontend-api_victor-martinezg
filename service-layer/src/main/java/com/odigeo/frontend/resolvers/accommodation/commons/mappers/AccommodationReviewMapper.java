package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.google.inject.Singleton;
import com.odigeo.accommodation.review.retrieve.bean.Review;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
public class AccommodationReviewMapper {
    private static final Float FIVE_STARS_RATING_CONVERSION = 0.05f;

    public AccommodationReview map(Review review) {
        AccommodationReview dto = new AccommodationReview();
        dto.setDedupId(review.getDedupId());
        dto.setProviderCode(review.getProviderCode());
        dto.setProviderReviewId(review.getProviderReviewId());
        if (review.getRating() != null) {
            dto.setRating(review.getRating() * FIVE_STARS_RATING_CONVERSION);
        }
        dto.setTotalReviews(review.getTotalReviews());
        return dto;
    }

    public Map<Integer, AccommodationReview> mapList(List<Review> reviews) {
        return reviews.stream().map(this::map).collect(Collectors.toMap(AccommodationReview::getDedupId, Function.identity()));
    }
}
