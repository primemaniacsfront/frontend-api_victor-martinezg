package com.odigeo.frontend.resolvers.search.handlers.vin;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.contract.ancillaries.InsuranceOfferDTO;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.frontend.resolvers.search.mappers.InsurancePolicyMapper;
import com.odigeo.searchengine.v2.responses.insurance.Insurance;
import com.odigeo.searchengine.v2.responses.insurance.InsuranceConditionsUrlType;
import com.odigeo.searchengine.v2.responses.insurance.InsuranceOffer;
import com.odigeo.searchengine.v2.responses.insurance.InsuranceUrl;
import com.odigeo.searchengine.v2.responses.itinerary.HubItinerary;
import org.apache.commons.collections4.IterableUtils;

@Singleton
class VinInsuranceCreator {

    @Inject
    private InsurancePolicyMapper policyMapper;

    public InsuranceOfferDTO createInsuranceOffer(Integer itineraryLink, SearchEngineContext seContext,
                                                  VinInsuranceIdCreator idCreator) {

        HubItinerary hub = seContext.getHubMap().get(itineraryLink);
        InsuranceOffer insuranceOffer = hub.getHubItineraryInsurances();

        Insurance insurance = IterableUtils.first(insuranceOffer.getInsurances());
        InsuranceOfferDTO insuranceOfferDTO = createInsurance(insurance);

        idCreator.createInsuranceId(insuranceOfferDTO);

        return insuranceOfferDTO;
    }

    private InsuranceOfferDTO createInsurance(Insurance insurance) {
        String url = getInsuranceUrl(insurance);

        InsuranceOfferDTO insuranceOfferDTO = new InsuranceOfferDTO();
        insuranceOfferDTO.setUrl(url);
        insuranceOfferDTO.setPolicy(policyMapper.map(insurance));

        return insuranceOfferDTO;
    }

    private String getInsuranceUrl(Insurance insurance) {
        String url = null;

        for (InsuranceUrl insuranceUrl : insurance.getConditionsUrls()) {
            if (insuranceUrl.getType() == InsuranceConditionsUrlType.EXTENDED) {
                return insuranceUrl.getUrl();
            }

            url = insuranceUrl.getUrl();
        }

        return url;
    }

}
