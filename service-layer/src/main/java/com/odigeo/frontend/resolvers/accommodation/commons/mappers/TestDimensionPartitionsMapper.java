package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.google.inject.Singleton;
import com.odigeo.hcsapi.v9.beans.TestDimensionPartition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class TestDimensionPartitionsMapper {

    public TestDimensionPartition map(String dimension, Integer partition) {
        TestDimensionPartition testDimensionPartition = new TestDimensionPartition();
        testDimensionPartition.setDimension(dimension);
        testDimensionPartition.setPartition(partition);
        return testDimensionPartition;
    }

    public List<TestDimensionPartition> map(Map<String, Integer> testDimensionPartitions) {
        return Optional.ofNullable(testDimensionPartitions)
                .map(test -> test.entrySet().stream().map(p -> map(p.getKey(), p.getValue())).collect(Collectors.toList()))
                .orElseGet(ArrayList::new);
    }

}
