package com.odigeo.frontend.resolvers.search.models;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CampaignConfig;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CarbonFootprint;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Perks;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayConfig;
import com.odigeo.frontend.contract.itinerary.ItineraryDTO;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class SearchItineraryDTO extends ItineraryDTO {

    private static final BigDecimal ME_RATING_DEFAULT = BigDecimal.ZERO;

    private String id;
    private String key;
    private Integer itinerariesLink;
    private Integer ticketsLeft;
    private BigDecimal meRating = ME_RATING_DEFAULT;
    private Integer index;
    private BigDecimal sortPrice;
    private List<Fee> fees;
    private Perks perks;
    private Boolean hotelXSellingEnabled;
    private Boolean hasFreeRebooking;
    private Boolean hasReliableCarriers;
    private Boolean hasRefundCarriers;
    private Boolean isFareUpgradeAvailable;
    private PrimeDayConfig primeDayConfig;
    private CarbonFootprint carbonFootprint;
    private Boolean isVin;
    private CampaignConfig campaignConfig;

    private Set<String> segmentsCarrierIds;
    private Set<String> sectionsCarrierIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getItinerariesLink() {
        return itinerariesLink;
    }

    public void setItinerariesLink(Integer itinerariesLink) {
        this.itinerariesLink = itinerariesLink;
    }

    public Integer getTicketsLeft() {
        return ticketsLeft;
    }

    public void setTicketsLeft(Integer ticketsLeft) {
        this.ticketsLeft = ticketsLeft;
    }

    public BigDecimal getMeRating() {
        return meRating;
    }

    public void setMeRating(BigDecimal meRating) {
        this.meRating = meRating;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public BigDecimal getSortPrice() {
        return sortPrice;
    }

    public void setSortPrice(BigDecimal sortPrice) {
        this.sortPrice = sortPrice;
    }

    public List<Fee> getFees() {
        return fees;
    }

    public void setFees(List<Fee> fees) {
        this.fees = fees;
    }

    public Boolean getHotelXSellingEnabled() {
        return hotelXSellingEnabled;
    }

    public void setHotelXSellingEnabled(Boolean hotelXSellingEnabled) {
        this.hotelXSellingEnabled = hotelXSellingEnabled;
    }

    public Boolean getHasFreeRebooking() {
        return hasFreeRebooking;
    }

    public void setHasFreeRebooking(Boolean hasFreeRebooking) {
        this.hasFreeRebooking = hasFreeRebooking;
    }

    public Boolean getHasReliableCarriers() {
        return hasReliableCarriers;
    }

    public void setHasReliableCarriers(Boolean hasReliableCarriers) {
        this.hasReliableCarriers = hasReliableCarriers;
    }

    public Boolean getHasRefundCarriers() {
        return hasRefundCarriers;
    }

    public void setHasRefundCarriers(Boolean hasRefundCarriers) {
        this.hasRefundCarriers = hasRefundCarriers;
    }

    public Boolean getIsFareUpgradeAvailable() {
        return isFareUpgradeAvailable;
    }

    public void setIsFareUpgradeAvailable(Boolean isFareUpgradeAvailable) {
        this.isFareUpgradeAvailable = isFareUpgradeAvailable;
    }

    public Perks getPerks() {
        return perks;
    }

    public void setPerks(Perks perks) {
        this.perks = perks;
    }

    public PrimeDayConfig getPrimeDayConfig() {
        return primeDayConfig;
    }

    public void setPrimeDayConfig(PrimeDayConfig primeDayConfig) {
        this.primeDayConfig = primeDayConfig;
    }

    public CarbonFootprint getCarbonFootprint() {
        return carbonFootprint;
    }

    public void setCarbonFootprint(CarbonFootprint carbonFootprint) {
        this.carbonFootprint = carbonFootprint;
    }

    public CampaignConfig getCampaignConfig() {
        return campaignConfig;
    }

    public void setCampaignConfig(CampaignConfig campaignConfig) {
        this.campaignConfig = campaignConfig;
    }

    public Boolean isVin() {
        return isVin;
    }

    public void setVin(Boolean vin) {
        isVin = vin;
    }

    public Set<String> getSegmentsCarrierIds() {
        return Optional.ofNullable(segmentsCarrierIds)
            .orElseGet(() -> {
                segmentsCarrierIds = getLegs().stream()
                    .map(LegDTO::getSegments)
                    .flatMap(List::stream)
                    .map(SegmentDTO::getCarrier)
                    .map(Carrier::getId)
                    .collect(Collectors.toSet());

                return segmentsCarrierIds;
            });
    }

    public Set<String> getSectionsCarrierIds() {
        return Optional.ofNullable(sectionsCarrierIds)
            .orElseGet(() -> {
                sectionsCarrierIds = getLegs().stream()
                    .map(LegDTO::getSegments)
                    .flatMap(List::stream)
                    .map(SegmentDTO::getSections)
                    .flatMap(List::stream)
                    .map(SectionDTO::getCarrier)
                    .map(Carrier::getId)
                    .collect(Collectors.toSet());

                return sectionsCarrierIds;
            });
    }
}
