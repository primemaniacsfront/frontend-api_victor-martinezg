package com.odigeo.frontend.resolvers.search.configuration;

import com.edreams.configuration.ConfiguredInJsonFile;

import javax.inject.Singleton;
import java.util.List;

@Singleton
@ConfiguredInJsonFile
public class AirlineCampaignConfiguration {

    private List<String> airlines;

    public List<String> getAirlines() {
        return airlines;
    }


    public void setAirlines(List<String> airlines) {
        this.airlines = airlines;
    }

}
