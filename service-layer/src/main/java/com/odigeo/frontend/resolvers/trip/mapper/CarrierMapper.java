package com.odigeo.frontend.resolvers.trip.mapper;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface CarrierMapper {

    @Mapping(target = "id", ignore = true)
    Carrier carrierIdContractToModel(Integer id, @Context ShoppingCartContext context);

    @Mapping(source = "code", target = "id")
    @Mapping(source = "name", target = "name")
    Carrier carrierContractToModel(com.odigeo.travelcompanion.v2.model.booking.Carrier carrier);

}
