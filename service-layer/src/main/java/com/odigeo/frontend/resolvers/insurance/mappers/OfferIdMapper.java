package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.insuranceproduct.api.last.request.OfferId;
import com.google.inject.Singleton;

@Singleton
public class OfferIdMapper {

    public OfferId map(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.OfferId apiOfferId) {
        OfferId offerId = new OfferId();

        offerId.setId(apiOfferId.getId());
        offerId.setVersion(apiOfferId.getVersion());

        return offerId;
    }
}
