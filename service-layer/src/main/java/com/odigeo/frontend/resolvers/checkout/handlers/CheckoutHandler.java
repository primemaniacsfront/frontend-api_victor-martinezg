package com.odigeo.frontend.resolvers.checkout.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeRequest;
import com.odigeo.frontend.resolvers.checkout.CheckoutFetcherResult;
import graphql.GraphQLContext;

public interface CheckoutHandler {

    CheckoutFetcherResult checkout(CheckoutRequest request, ResolverContext context, GraphQLContext graphQLContext);
    CheckoutResponse resume(CheckoutResumeRequest request, ResolverContext context);

}
