package com.odigeo.frontend.resolvers.prime.pages;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPageName;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.prime.utils.MembershipUtils;
import com.odigeo.frontend.services.UserDescriptionService;
import graphql.schema.DataFetchingEnvironment;

@Singleton
public class MembershipManagerPage implements MembershipPage {
    @Inject
    private SiteVariations siteVariations;
    @Inject
    private MetricsHandler metricsHandler;
    @Inject
    private UserDescriptionService userDescriptionService;

    @Override
    public MembershipPageName getPage(DataFetchingEnvironment env) {
        VisitInformation visitInformation = env.getGraphQlContext().get(VisitInformation.class);
        Membership membership = env.getGraphQlContext().get("Membership");
        if (isMembershipRecaptureM(visitInformation, membership, env.getContext())) {
            metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_PAGE_RECAPTURE);
            return MembershipPageName.RENEWAL_RECAPTURE;
        }
        return null;
    }

    private boolean isMembershipRecaptureM(VisitInformation visitInformation, Membership membership, ResolverContext context) {
        return siteVariations.isMembershipRecaptureM(visitInformation)
                && MembershipUtils.isAutoRenewalDeactivated(membership)
                && isUserLoggedOrAutoApply(membership, context);
    }

    private boolean isUserLoggedOrAutoApply(Membership membership, ResolverContext context) {
        boolean isUserLogged = userDescriptionService.isUserLogged(context);
        return isUserLogged || membership.getIsAutoApply();
    }

}
