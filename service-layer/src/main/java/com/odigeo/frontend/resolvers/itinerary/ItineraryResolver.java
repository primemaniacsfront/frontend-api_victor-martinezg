package com.odigeo.frontend.resolvers.itinerary;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.itinerary.handlers.ItineraryHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;

@Singleton
public class ItineraryResolver implements Resolver {

    @Inject
    private ItineraryHandler handler;

    static final String SHOPPING_CART_TYPE = "ShoppingCart";
    static final String ITINERARY_FIELD = "itinerary";

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type(TypeRuntimeWiring.newTypeWiring(SHOPPING_CART_TYPE)
                .dataFetcher(ITINERARY_FIELD, this::itineraryDataFetcher));
    }

    Itinerary itineraryDataFetcher(DataFetchingEnvironment env) {
        return handler.map(env);
    }
}
