package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.ProductType;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.section.AbstractSection;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.section.FlightSection;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.section.GeographicalInformation;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.section.TrainSection;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.google.inject.Singleton;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.location.LocationDTO;

@Singleton
class FootprintSectionMapper {

    public AbstractSection map(SectionDTO sectionDTO, Integer sectionIndex) {

        ProductType productType = getProductType(sectionDTO);

        AbstractSection section = ProductType.FLIGHT.equals(productType)
                ? initFlightSection(sectionDTO.getFlightCode())
                : initTrainSection(sectionDTO.getFlightCode());

        section.setProductType(productType);

        section.setArrivalDateUTC(sectionDTO.getArrivalDate());
        section.setDepartureDateUTC(sectionDTO.getDepartureDate());
        section.setGeoInformation(initGeoInformation(sectionDTO));
        section.setSectionIndex(sectionIndex);

        return section;
    }

    private GeographicalInformation initGeoInformation(SectionDTO sectionDTO) {

        GeographicalInformation geoInformation = new GeographicalInformation();

        geoInformation.setDepartureGeoNodeId(sectionDTO.getDeparture().getId());
        geoInformation.setArrivalIata(getIataByLocationType(sectionDTO.getDeparture()));

        geoInformation.setArrivalGeoNodeId(sectionDTO.getDestination().getId());
        geoInformation.setDepartureIata(getIataByLocationType(sectionDTO.getDestination()));

        return geoInformation;

    }

    private TrainSection initTrainSection(String trainCode) {
        TrainSection trainSection = new TrainSection();
        trainSection.setTrainCode(trainCode);
        return trainSection;
    }

    private FlightSection initFlightSection(String flightCode) {
        FlightSection flightSection = new FlightSection();
        flightSection.setFlightNumber(flightCode);
        return flightSection;
    }

    private ProductType getProductType(SectionDTO sectionDTO) {
        return TransportType.PLANE.equals(sectionDTO.getTransportType()) ? ProductType.FLIGHT : ProductType.TRAIN;
    }

    private String getIataByLocationType(LocationDTO location) {
        return location.getLocationType() == LocationType.TRAIN_STATION
                ? location.getCityIata()
                : location.getIata();
    }
}
