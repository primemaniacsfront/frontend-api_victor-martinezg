package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ThreeValuedLogic;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.mappers.GeoCoordinatesMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.hcsapi.v9.beans.Hotel;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDealInformation;
import org.apache.commons.lang3.EnumUtils;

@Singleton
public class AccommodationDealInformationMapper {
    @Inject
    private AccommodationImagesMapper accommodationImagesMapper;
    @Inject
    private GeoCoordinatesMapper geoCoordinatesMapper;
    @Inject
    private AccommodationCategoryMapper accommodationCategoryMapper;


    public AccommodationDealInformationDTO map(AccommodationDealInformation accommodationDealInformation) {
        AccommodationDealInformationDTO dto = new AccommodationDealInformationDTO();
        dto.setName(accommodationDealInformation.getName());
        dto.setAddress(accommodationDealInformation.getAddress());
        dto.setCityName(accommodationDealInformation.getCityName());
        dto.setDescription(accommodationDealInformation.getDescription());
        dto.setLocation(accommodationDealInformation.getLocation());
        dto.setChain(accommodationDealInformation.getChain());
        dto.setType(accommodationDealInformation.getType() != null ? EnumUtils.getEnum(AccommodationType.class, accommodationDealInformation.getType().value()) : null);
        dto.setMainAccommodationImage(accommodationImagesMapper.map(accommodationDealInformation.getMainAccommodationImage()));
        dto.setCancellationPolicyDescription(accommodationDealInformation.getCancellationPolicyDescription());
        dto.setCoordinates(geoCoordinatesMapper.map(accommodationDealInformation.getCoordinates()));
        dto.setCategory(accommodationDealInformation.getCategory() != null ? EnumUtils.getEnum(AccommodationCategory.class, accommodationDealInformation.getCategory().value()) : null);
        dto.setCancellationFree(accommodationDealInformation.getCancellationFree() != null ? EnumUtils.getEnum(ThreeValuedLogic.class, accommodationDealInformation.getCancellationFree().value()) : null);
        dto.setPaymentAtDestination(accommodationDealInformation.isPaymentAtDestination());
        return dto;
    }

    public AccommodationDealInformationDTO mapHcs(Hotel hotelRs) {
        AccommodationDealInformationDTO dto = new AccommodationDealInformationDTO();

        dto.setCancellationPolicyDescription(hotelRs.getCancellationPolicy());
        HotelSummary hotelSummary = hotelRs.getHotelSummary();
        if (hotelSummary != null) {
            dto.setName(hotelSummary.getName());
            dto.setAddress(hotelSummary.getAddress());
            dto.setCityName(hotelSummary.getCity());
            dto.setDescription(hotelSummary.getHotelLongDescription());
            dto.setLocation(hotelSummary.getLocation());
            dto.setChain(hotelSummary.getChain());
            dto.setType(hotelSummary.getType() != null ? EnumUtils.getEnum(AccommodationType.class, hotelSummary.getType().name()) : null);
            dto.setMainAccommodationImage(accommodationImagesMapper.mapHcsImage(hotelSummary.getMainHotelImage()));
            dto.setCoordinates(geoCoordinatesMapper.mapHcs(hotelSummary));
            dto.setCategory(accommodationCategoryMapper.mapStarToCategory(hotelSummary.getStarRating()));
        }
        return dto;
    }

}
