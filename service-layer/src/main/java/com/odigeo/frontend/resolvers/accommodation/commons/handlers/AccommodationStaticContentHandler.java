package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.HotelDetailResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.HotelDetailsRequestDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.model.AccommodationStaticContentDTO;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.model.AccommodationStaticContentResponseDTO;
import com.odigeo.hcsapi.v9.beans.HotelSummaryResponse;

import java.util.List;
import java.util.stream.Collectors;

public class AccommodationStaticContentHandler {

    @Inject
    private AccommodationDataHandler accommodationDataHandler;
    @Inject
    private HotelDetailResponseMapper hotelDetailResponseMapper;
    @Inject
    private ContentKeyMapper contentKeyMapper;

    public void populateSearchAccommodationStaticContent(VisitInformation visit, AccommodationSearchResponseDTO response) {
        HotelDetailsRequestDTO hotelDetailsRequestDTO = new HotelDetailsRequestDTO();
        List<SearchAccommodationDTO> accommodations = response.getAccommodationResponse().getAccommodations();
        List<AccommodationProviderKeyDTO> providerKeyDTOS = accommodations.stream().map(SearchAccommodationDTO::getContentKey).map(contentKey -> contentKeyMapper.extractProviderKeyFromContentKey(contentKey)).collect(Collectors.toList());
        hotelDetailsRequestDTO.setProviderKeys(providerKeyDTOS);

        HotelSummaryResponse accommodationSummary = accommodationDataHandler.getAccommodationSummary(hotelDetailsRequestDTO, visit);

        hotelDetailResponseMapper.remapSummaryToAccommodation(accommodationSummary, accommodations);
    }

    public AccommodationStaticContentResponseDTO retrieveStaticContent(VisitInformation visit, List<AccommodationProviderKeyDTO> providerKeyDTOS) {
        AccommodationStaticContentResponseDTO accommodationStaticContentResponseDTO = new AccommodationStaticContentResponseDTO();
        HotelDetailsRequestDTO hotelDetailsRequestDTO = new HotelDetailsRequestDTO();
        hotelDetailsRequestDTO.setProviderKeys(providerKeyDTOS);

        HotelSummaryResponse accommodationSummary = accommodationDataHandler.getAccommodationSummary(hotelDetailsRequestDTO, visit);
        List<AccommodationStaticContentDTO> accommodationStaticContentDTOList = hotelDetailResponseMapper.mapSummaries(accommodationSummary);
        accommodationStaticContentResponseDTO.setAccommodationsStaticContent(accommodationStaticContentDTOList);
        return accommodationStaticContentResponseDTO;
    }

}
