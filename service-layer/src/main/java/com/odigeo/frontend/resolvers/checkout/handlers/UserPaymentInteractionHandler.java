package com.odigeo.frontend.resolvers.checkout.handlers;

import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteractionId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteraction;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteractionRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.services.checkout.UserPaymentInteractionService;

@Singleton
public class UserPaymentInteractionHandler {

    @Inject
    private UserPaymentInteractionService paymentInteractionService;


    public UserPaymentInteraction retrieveUserPaymentInteraction(UserPaymentInteractionRequest request) {
        return paymentInteractionService.retrieveUserPaymentInteraction(new UserPaymentInteractionId(request.getUserPaymentInteractionId().getId()));
    }
}
