package com.odigeo.frontend.resolvers.search.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.google.inject.Singleton;
import org.apache.commons.lang3.EnumUtils;

@Singleton
public class PaymentMethodMapper {

    public PaymentMethod map(String code) {
        return EnumUtils.getEnum(PaymentMethod.class, code);
    }

}
