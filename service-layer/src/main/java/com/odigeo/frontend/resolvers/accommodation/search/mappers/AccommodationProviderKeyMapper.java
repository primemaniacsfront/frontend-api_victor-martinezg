package com.odigeo.frontend.resolvers.accommodation.search.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;

@Singleton
public class AccommodationProviderKeyMapper {

    public AccommodationProviderKeyDTO map(HotelSupplierKey hotelSupplierKey) {
        return new AccommodationProviderKeyDTO(hotelSupplierKey.getSupplierCode(), hotelSupplierKey.getHotelSupplierId());
    }
}
