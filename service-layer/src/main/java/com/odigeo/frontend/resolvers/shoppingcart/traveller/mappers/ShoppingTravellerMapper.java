package com.odigeo.frontend.resolvers.shoppingcart.traveller.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCartTraveller;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerInformationDescription;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.carrier.CarrierMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(uses = {CarrierMapper.class, PersonalInfoMapper.class})
public interface ShoppingTravellerMapper {

    List<TravellerInformationDescription> travellerListInfoToModel(List<com.odigeo.dapi.client.TravellerInformationDescription> travellerInformationDescription);

    List<ShoppingCartTraveller> travellerListContractToModel(List<com.odigeo.dapi.client.Traveller> traveller);

    TravellerInformationDescription travellerInfoToModel(com.odigeo.dapi.client.TravellerInformationDescription travellerInformationDescription);

    @Mapping(target = "identificationExpirationDate", dateFormat = "yyyy-MM-dd")
    @Mapping(source = ".", target = "personalInfo")
    ShoppingCartTraveller travellerContractToModel(com.odigeo.dapi.client.Traveller traveller);

}
