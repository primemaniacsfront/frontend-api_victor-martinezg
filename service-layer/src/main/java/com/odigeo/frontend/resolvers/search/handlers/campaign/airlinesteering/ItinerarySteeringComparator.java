package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Optional;


public class ItinerarySteeringComparator implements Comparator<SearchItineraryDTO>, Serializable {

    private final String feeTypeId;

    public ItinerarySteeringComparator(FeeType feeType) {
        this.feeTypeId = feeType.getId();
    }

    @Override
    public int compare(SearchItineraryDTO firstItinerary, SearchItineraryDTO secondItinerary) {

        int result = 0;

        if (secondItinerary.getMeRating() != null && firstItinerary.getMeRating() != null) {
            result = secondItinerary.getMeRating().compareTo(firstItinerary.getMeRating());
        } else if (secondItinerary.getMeRating() != null) {
            result = 1;
        } else if (firstItinerary.getMeRating() != null) {
            result = -1;
        }

        if (result == 0) {
            result = comparePrice(firstItinerary, secondItinerary);
        }

        return result;

    }

    private int comparePrice(SearchItineraryDTO firstItinerary, SearchItineraryDTO secondItinerary) {
        Optional<BigDecimal> firstItineraryPrice = ItinerarySteeringUtils.getPrice(firstItinerary, feeTypeId);
        Optional<BigDecimal> secondItineraryPrice = ItinerarySteeringUtils.getPrice(secondItinerary, feeTypeId);

        return firstItineraryPrice.isPresent() && secondItineraryPrice.isPresent()
                ? firstItineraryPrice.get().compareTo(secondItineraryPrice.get()) : 0;
    }

}
