package com.odigeo.frontend.resolvers.accommodation.commons.model;

public enum BoardTypeDTO {
    RO,
    HB,
    AI,
    BB,
    FB,
    SC,
    UN;

    public String value() {
        return this.name();
    }

}
