package com.odigeo.frontend.resolvers.accommodation.rooms.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDetailsDTO;
import com.odigeo.searchengine.v2.responses.accommodation.RoomDeal;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class RoomDetailsMapper {

    static final String JOIN = ", ";
    private static final String SEPARATOR = "\\s-\\s";

    public RoomDetailsDTO map(RoomDeal roomDeal) {
        return Optional.ofNullable(roomDeal)
                .map(RoomDeal::getDescription)
                .filter(StringUtils::isNotBlank)
                .map(this::buildRoomDetails)
                .orElse(null);
    }

    private RoomDetailsDTO buildRoomDetails(String description) {
        RoomDetailsDTO roomDetails = new RoomDetailsDTO();
        String[] descriptionSplit = description.split(SEPARATOR);
        roomDetails.setName(descriptionSplit[0]);
        if (descriptionSplit.length > 1) {
            roomDetails.setDescription(Arrays.stream(descriptionSplit).skip(1).collect(Collectors.joining(JOIN)));
        }
        return roomDetails;
    }
}
