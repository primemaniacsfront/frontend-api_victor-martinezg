package com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts;

import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsConfiguration;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;

public class NonEssentialProductContextBuilder {
    private ShoppingCartContext shoppingCartContext;
    private int segments;
    private int passengers;
    private NonEssentialProductsConfiguration nonEssentialProductsConfiguration;

    public NonEssentialProductContextBuilder() {
    }

    public NonEssentialProductContextBuilder withShoppingCartContext(ShoppingCartContext shoppingCartContext) {
        this.shoppingCartContext = shoppingCartContext;
        return this;
    }

    public NonEssentialProductContextBuilder withSegments(int segments) {
        this.segments = segments;
        return this;
    }

    public NonEssentialProductContextBuilder withPassengers(int passengers) {
        this.passengers = passengers;
        return this;
    }

    public NonEssentialProductContextBuilder withNonEssentialProductsConfiguration(NonEssentialProductsConfiguration nonEssentialProductsConfiguration) {
        this.nonEssentialProductsConfiguration = nonEssentialProductsConfiguration;
        return this;
    }

    public ShoppingCartContext getShoppingCartContext() {
        return shoppingCartContext;
    }

    public int getSegments() {
        return segments;
    }

    public int getPassengers() {
        return passengers;
    }

    public NonEssentialProductsConfiguration getNonEssentialProductsConfiguration() {
        return nonEssentialProductsConfiguration;
    }

    public NonEssentialProductContext build() {
        return new NonEssentialProductContext(this);
    }
}
