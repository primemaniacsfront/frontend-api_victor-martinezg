package com.odigeo.frontend.resolvers;

import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.List;

public interface ResolverLoader {

    String RESOLVER_ROOT_PACKAGE = "com.odigeo.frontend.resolvers";

    //Add here the complete list of resolvers for the schema
    static List<Class<? extends Resolver>> load() {
        return new ArrayList<>(new Reflections(RESOLVER_ROOT_PACKAGE).getSubTypesOf(Resolver.class));
    }
}
