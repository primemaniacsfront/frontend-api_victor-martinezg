package com.odigeo.frontend.resolvers.accommodation.product;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateAccommodationProductRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateAccommodationProductResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectRoomOfferRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectRoomOfferResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.accommodation.product.factories.CreateProductAccommodationFactory;
import com.odigeo.frontend.resolvers.accommodation.product.factories.SelectRoomOfferFactory;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;


@Singleton
public class AccommodationProductResolver implements Resolver {

    private static final String SELECT_ROOM_OFFER_QUERY = "selectRoomOffer";
    private static final String CREATE_ACCOMMODATION_PRODUCT_QUERY = "createAccommodationProduct";

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private SelectRoomOfferFactory selectRoomOfferFactory;
    @Inject
    private CreateProductAccommodationFactory createProductAccommodationFactory;


    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Mutation", typeWiring -> typeWiring
                .dataFetcher(SELECT_ROOM_OFFER_QUERY, this::selectRoomOfferFetcher)
                .dataFetcher(CREATE_ACCOMMODATION_PRODUCT_QUERY, this::createAccommodationProductFetcher));
    }

    SelectRoomOfferResponse selectRoomOfferFetcher(DataFetchingEnvironment environment) {
        SelectRoomOfferRequest requestParams = jsonUtils.fromDataFetching(environment, SelectRoomOfferRequest.class);
        ResolverContext resolverContext = environment.getContext();
        GraphQLContext graphQLContext = environment.getGraphQlContext();

        return selectRoomOfferFactory.getInstance(requestParams.getShoppingType()).selectRoomOffer(
            requestParams.getSearchId(),
            requestParams.getAccommodationDealKey(),
            requestParams.getRoomDealKey(),
            resolverContext,
            graphQLContext);
    }

    CreateAccommodationProductResponse createAccommodationProductFetcher(DataFetchingEnvironment environment) {
        CreateAccommodationProductRequest requestParams = jsonUtils.fromDataFetching(environment, CreateAccommodationProductRequest.class);
        ResolverContext resolverContext = environment.getContext();

        return createProductAccommodationFactory.getInstance(requestParams.getShoppingType()).createProductAccommodation(
                requestParams.getDealId(),
                requestParams.getGuests(),
                requestParams.getBuyer(),
                resolverContext);
    }
}
