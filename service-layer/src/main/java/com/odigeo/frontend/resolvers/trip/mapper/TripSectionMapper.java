package com.odigeo.frontend.resolvers.trip.mapper;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CabinClass;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripSection;
import com.odigeo.travelcompanion.v2.model.booking.SectionResult;
import com.odigeo.travelcompanion.v2.model.enrichedbooking.EnrichedSection;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mapper(uses = {CarrierMapper.class, LocationMapper.class, DateMapper.class})
public interface TripSectionMapper {

    @Mapping(source = "from", target = "departure")
    @Mapping(source = "to", target = "arrival")
    @Mapping(source = "aircraft", target = "vehicleModel")
    @Mapping(source = "productType", target = "transportType", qualifiedByName = "mapTransportType")
    @Mapping(source = "arrivalDate", target = "arrivalDate")
    @Mapping(source = "departureDate", target = "departureDate")
    @Mapping(source = "cabinClass", target = "cabinClass", qualifiedByName = "mapCabinClass")
    TripSection mapToSection(EnrichedSection result);

    default List<TripSection> mapToSections(Collection<SectionResult> sectionsDTO) {
        return Optional.ofNullable(sectionsDTO)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(sectionDTO -> {
                            EnrichedSection enrichedSection = sectionDTO.getSection();
                            TripSection section = this.mapToSection(enrichedSection);
                            section.setProviderBookingItemId(String.valueOf(sectionDTO.getItineraryBookingId()));
                            section.setFlightCode(enrichedSection.getCarrier().getCode() + sectionDTO.getSection().getId());
                            return section;
                        }
                )
                .collect(Collectors.toList());
    }

    @Named("mapTransportType")
    default TransportType mapTransportType(String type) {
        switch (type) {
        case "FLIGHT":
            return TransportType.PLANE;
        case "TRAIN":
            return TransportType.TRAIN;
        default:
            return null;
        }
    }

    @Named("mapCabinClass")
    default CabinClass mapCabinClass(String cabinClass) {
        return CabinClass.valueOf(cabinClass);
    }
}
