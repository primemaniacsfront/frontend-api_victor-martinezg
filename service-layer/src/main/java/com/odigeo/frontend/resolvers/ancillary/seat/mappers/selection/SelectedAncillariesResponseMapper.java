package com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedAncillariesResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryapi.v1.response.SelectAncillaryServiceResponse;

@Singleton
public class SelectedAncillariesResponseMapper {

    @Inject
    SelectedAncillaryProductMapper selectedAncillaryProductMapper;

    public SelectedAncillariesResponse map(SelectAncillaryServiceResponse selectAncillaryServiceResponse) {
        SelectedAncillariesResponse selectedAncillariesResponse = new SelectedAncillariesResponse();
        selectedAncillariesResponse.setCode(selectAncillaryServiceResponse.getCode());
        selectedAncillariesResponse.setMessage(selectAncillaryServiceResponse.getMessage());
        selectedAncillariesResponse.setSuccess(selectAncillaryServiceResponse.isSuccess());
        selectedAncillariesResponse.setProducts(
                selectedAncillaryProductMapper.map(selectAncillaryServiceResponse.getProductIds())
        );

        return selectedAncillariesResponse;
    }
}
