package com.odigeo.frontend.resolvers.deals.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Deal;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.util.BigDecimalUtils;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.marketing.search.price.v1.requests.PriceType;
import com.odigeo.marketing.search.price.v1.responses.RoutePrice;
import com.odigeo.marketing.search.price.v1.util.Money;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@Singleton
public class DealsRoutePriceMapper {

    @Inject
    ApparentPricesMapper apparentPricesMapper;
    @Inject
    CityMapper cityMapper;
    @Inject
    BigDecimalUtils bigDecimalUtils;

    public Deal map(RoutePrice routePrice, Map<String, City> destinations, Locale locale) {
        Deal deal = new Deal();
        deal.setDepartureDate(routePrice.getOutboundDate());
        deal.setArrivalDate(routePrice.getInboundDate());

        City destination = destinations.get(routePrice.getInboundCityIataCode());
        deal.setDestination(cityMapper.map(destination, locale));

        deal.setApparentPricesByPax(apparentPricesMapper.map(routePrice.getApparentPricesByPax()));
        deal.setDiscount(calculateDiscount(routePrice));

        return deal;
    }

    private double calculateDiscount(RoutePrice routePrice) {
        Optional<Map<String, Money>> source = Optional.ofNullable(routePrice)
                .map(RoutePrice::getApparentPricesByPax);
        BigDecimal first = source.map(apparentPrices -> apparentPrices.get(PriceType.MINIMUM_PURCHASABLE_PRICE.name())).map(Money::getAmount).orElse(null);
        BigDecimal second = source.map(apparentPrices -> apparentPrices.get(PriceType.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE.name())).map(Money::getAmount).orElse(null);

        return bigDecimalUtils.differenceInPercentage(first, second).doubleValue();
    }



}
