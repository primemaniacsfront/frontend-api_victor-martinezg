package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatCharacteristic;
import com.odigeo.frontend.commons.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class SeatCharacteristicsMapper {
    @Inject
    private Logger logger;

    public List<SeatCharacteristic> map(List<com.odigeo.itineraryapi.v1.response.SeatCharacteristic> seatCharacteristicList) {
        return Optional.ofNullable(seatCharacteristicList)
            .orElse(Collections.emptyList())
            .stream()
            .filter(this::filterValidValues)
            .map(sc -> SeatCharacteristic.valueOf(sc.getSeatCharacteristic()))
            .collect(Collectors.toList());
    }

    private boolean filterValidValues(com.odigeo.itineraryapi.v1.response.SeatCharacteristic characteristic) {
        SeatCharacteristic mappedCharacteristic = null;

        try {
            mappedCharacteristic = SeatCharacteristic.valueOf(characteristic.getSeatCharacteristic());
        } catch (IllegalArgumentException ex) {
            String message = String.format("Cannot map unknown characteristic %s", characteristic.getSeatCharacteristic());
            logger.warning(this.getClass(), message, ex);
        }

        return mappedCharacteristic != null;
    }
}
