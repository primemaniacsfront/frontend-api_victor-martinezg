package com.odigeo.frontend.resolvers.checkin.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinAddress;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinResidentPermit;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinTravelPermissions;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinTraveller;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.EmergencyContact;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.OnwardTicket;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.VisaDetails;
import com.google.inject.Singleton;
import com.odigeo.checkin.api.v1.model.ArrivalDetails;
import com.odigeo.checkin.api.v1.model.AutomaticCheckIn;
import com.odigeo.checkin.api.v1.model.Contact;
import com.odigeo.checkin.api.v1.model.Gender;
import com.odigeo.checkin.api.v1.model.NoVisaReason;
import com.odigeo.checkin.api.v1.model.OnwardFlight;
import com.odigeo.checkin.api.v1.model.ResidentPermit;
import com.odigeo.checkin.api.v1.model.TravelDocument;
import com.odigeo.checkin.api.v1.model.TravelDocumentType;
import com.odigeo.checkin.api.v1.model.TravelPermission;
import com.odigeo.checkin.api.v1.model.Traveller;
import com.odigeo.checkin.api.v1.model.Visa;
import com.odigeo.checkin.api.v1.model.VisaType;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Singleton
public class CreateCheckinRequestMapper {

    public AutomaticCheckIn map(CheckinRequest request, String locale) {
        AutomaticCheckIn checkinRequest = new AutomaticCheckIn();
        checkinRequest.setBookingId(request.getBookingId());
        checkinRequest.setTravellers(mapTravellers(request.getTravellers(), locale));

        return checkinRequest;
    }

    private List<Traveller> mapTravellers(List<CheckinTraveller> checkinTravellers, String locale) {
        List<Traveller> travellers = new ArrayList<>();
        for (CheckinTraveller traveller : checkinTravellers) {
            travellers.add(mapTraveller(traveller, locale));
        }

        return travellers;
    }

    private Traveller mapTraveller(CheckinTraveller traveller, String locale) {
        Traveller domainTraveller = new Traveller();
        domainTraveller.setNumPassenger(traveller.getNumPassenger());
        domainTraveller.setGender(Gender.valueOf(traveller.getGender().name()));
        domainTraveller.setDateOfBirth(LocalDate.parse(traveller.getBirthDate()));
        domainTraveller.setMobilePhone(traveller.getMobilePhone());
        domainTraveller.setEmail(traveller.getEmail());
        domainTraveller.setCitizenship(traveller.getCitizenship());
        domainTraveller.setCountryOfResidence(traveller.getCountryOfResidence());
        domainTraveller.setLocale(locale);
        domainTraveller.setContact(mapContact(traveller.getContact()));
        domainTraveller.setTravelDocument(mapTravelDocument(traveller.getTravelDocument()));
        domainTraveller.setArrivalDetails(mapArrivalDetails(traveller.getAddress()));
        domainTraveller.setTravelPermission(mapTravelPermission(traveller.getTravelPermissions()));

        return domainTraveller;
    }

    private Contact mapContact(EmergencyContact emergencyContact) {
        return Optional.ofNullable(emergencyContact)
                .map(externalContact -> {
                    Contact contact = new Contact();
                    contact.setFullName(externalContact.getFullName());
                    contact.setPhone(externalContact.getPhone());
                    contact.setRelation(externalContact.getRelation());
                    return contact;
                }).orElse(null);
    }

    private TravelDocument mapTravelDocument(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravelDocument document) {
        return Optional.ofNullable(document)
                .map(externalDocument -> {
                    TravelDocument travelDocument = new TravelDocument();
                    travelDocument.setTravelDocumentType(TravelDocumentType.valueOf(externalDocument.getType().name()));
                    travelDocument.setNumber(externalDocument.getNumber());
                    travelDocument.setExpiration(LocalDate.parse(externalDocument.getExpirationDate()));
                    travelDocument.setIssued(LocalDate.parse(externalDocument.getIssueDate()));
                    travelDocument.setIssuingCountry(externalDocument.getIssuingCountry());
                    return travelDocument;
                }).orElse(null);
    }

    private ArrivalDetails mapArrivalDetails(CheckinAddress address) {
        return Optional.ofNullable(address)
                .map(externalAddress -> {
                    ArrivalDetails arrivalDetails = new ArrivalDetails();
                    arrivalDetails.setAddress(externalAddress.getAddress());
                    arrivalDetails.setCity(externalAddress.getCity());
                    arrivalDetails.setState(externalAddress.getState());
                    arrivalDetails.setZip(externalAddress.getZip());
                    return arrivalDetails;
                }).orElse(null);
    }

    private TravelPermission mapTravelPermission(CheckinTravelPermissions permissions) {
        TravelPermission travelPermission = new TravelPermission();
        travelPermission.setNoVisaReason(NoVisaReason.valueOf(permissions.getNoVisaReason()));
        travelPermission.setOnwardFlight(mapOnwardFlight(permissions.getOnwardTicket()));
        travelPermission.setResidentPermit(mapResidentPermit(permissions.getResidentPermit()));
        travelPermission.setVisa(mapVisa(permissions.getVisaDetails()));

        return travelPermission;
    }

    private OnwardFlight mapOnwardFlight(OnwardTicket onwardTicket) {
        return Optional.ofNullable(onwardTicket)
                .map(externalOnwardTicket -> {
                    OnwardFlight flight = new OnwardFlight();
                    flight.setNumber(externalOnwardTicket.getId());
                    flight.setDate(Instant.parse(externalOnwardTicket.getDate()));
                    flight.setAirportDestination(externalOnwardTicket.getDestination());
                    return flight;
                }).orElse(null);
    }

    private ResidentPermit mapResidentPermit(CheckinResidentPermit permit) {
        return Optional.ofNullable(permit)
                .map(externalPermit -> {
                    ResidentPermit residentPermit = new ResidentPermit();
                    residentPermit.setNumber(externalPermit.getNumber());
                    residentPermit.setExpiration(LocalDate.parse(externalPermit.getExpirationDate()));
                    residentPermit.setExpires(externalPermit.getExpires());
                    residentPermit.setIssueCountry(externalPermit.getIssuingCountry());
                    return residentPermit;
                }).orElse(null);
    }

    private Visa mapVisa(VisaDetails visaDetails) {
        return Optional.ofNullable(visaDetails)
                .map(externalVisaDetails -> {
                    Visa visa = new Visa();
                    visa.setNumber(externalVisaDetails.getNumber());
                    visa.setVisaType(VisaType.valueOf(externalVisaDetails.getType().name()));
                    visa.setExpiration(LocalDate.parse(externalVisaDetails.getExpirationDate()));
                    visa.setIssueCountry(externalVisaDetails.getIssueCountry());
                    visa.setIssueDate(LocalDate.parse(externalVisaDetails.getIssueDate()));
                    return visa;
                }).orElse(null);
    }
}
