package com.odigeo.frontend.resolvers.search.handlers.helper;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.AsyncUtils;
import com.odigeo.frontend.resolvers.search.handlers.carbonfootprint.CarbonFootprintCallable;
import com.odigeo.frontend.resolvers.search.handlers.carbonfootprint.CarbonFootprintHandler;
import com.odigeo.frontend.resolvers.search.handlers.carbonfootprint.CarbonFootprintResponse;
import com.odigeo.frontend.resolvers.search.handlers.mir.MirCallable;
import com.odigeo.frontend.resolvers.search.handlers.mir.MirHandler;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.response.ItineraryRatingResponse;
import org.apache.commons.collections4.CollectionUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Singleton
public class MirFootprintItineraryHandler {

    @Inject
    private MirHandler mirHandler;

    @Inject
    private CarbonFootprintHandler carbonFootprintHandler;

    @Inject
    private AsyncUtils asyncUtils;

    private static final long DEFAULT_SHUTDOWN_EXECUTOR_TIMEOUT = 1;
    private static final TimeUnit DEFAULT_SHUTDOWN_TIMEOUT_UNIT = TimeUnit.SECONDS;

    public void addMirRatingAndFootprintInfoItinerary(SearchRequest searchRequest, SearchResponseDTO searchResponse,
                                                      ResolverContext context) {

        if (isEnabled(searchRequest, searchResponse)) {
            ExecutorService executor = Executors.newFixedThreadPool(2);

            Future<CarbonFootprintResponse> calculatorResponseFuture = executor.submit(new CarbonFootprintCallable(searchRequest, searchResponse, context));
            Future<ItineraryRatingResponse> mirResponseFuture = executor.submit(new MirCallable(searchRequest, searchResponse, context));

            asyncUtils.shutdownAndWait(executor, DEFAULT_SHUTDOWN_EXECUTOR_TIMEOUT, DEFAULT_SHUTDOWN_TIMEOUT_UNIT);

            ifIsDoneAddFootprint(calculatorResponseFuture, searchResponse, context);
            ifIsDoneAddRating(mirResponseFuture, searchResponse, context);
        }

    }

    private void ifIsDoneAddFootprint(Future<CarbonFootprintResponse> calculatorResponseFuture, SearchResponseDTO searchResponse,
                                      ResolverContext context) {
        CarbonFootprintResponse response = asyncUtils.mapFuture(calculatorResponseFuture, CarbonFootprintResponse.class);
        if (response != null) {
            carbonFootprintHandler.addCarbonFootprintInfo(response, searchResponse, context);
        }
    }

    private void ifIsDoneAddRating(Future<ItineraryRatingResponse> mirResponseFuture, SearchResponseDTO searchResponse,
                                   ResolverContext context) {
        ItineraryRatingResponse response = asyncUtils.mapFuture(mirResponseFuture, ItineraryRatingResponse.class);
        if (response != null) {
            mirHandler.addRatingAndReturningUserDeviceTracking(context, searchResponse, response);
        }
    }

    private boolean isEnabled(SearchRequest searchRequestDTO, SearchResponseDTO searchResponseDTO) {
        return searchRequestDTO.getTripType() != TripType.MULTIPLE_DESTINATIONS
                && CollectionUtils.isNotEmpty(searchResponseDTO.getItineraries());
    }

}
