package com.odigeo.frontend.resolvers.accommodation.rooms.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.PriceBreakdownHandler;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.RoomDetailsMapper;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.AccommodationRoomsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDealDTO;
import com.odigeo.searchengine.v2.responses.accommodation.RoomDeal;
import com.odigeo.searchengine.v2.responses.accommodation.RoomGroupDeal;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class AccommodationRoomsResponseHandler {
    @Inject
    private RoomDetailsMapper roomDetailsMapper;
    @Inject
    private PriceBreakdownHandler priceBreakdownHandler;

    public AccommodationRoomsResponseDTO buildAccommodationRoomResponse(SearchRoomResponse searchRoomResponse, VisitInformation visitInformation) {
        AccommodationRoomsResponseDTO accommodationRoomsResponseDTO = new AccommodationRoomsResponseDTO();
        accommodationRoomsResponseDTO.setSearchId(searchRoomResponse.getSearchId());
        accommodationRoomsResponseDTO.setRooms(buildRoomDealList(searchRoomResponse.getRoomDealLegend(), searchRoomResponse.getRoomGroupDeals(), visitInformation));
        accommodationRoomsResponseDTO.setCurrency(visitInformation.getCurrency());
        return accommodationRoomsResponseDTO;
    }

    private List<RoomDealDTO> buildRoomDealList(List<RoomDeal> roomDealLegend, List<RoomGroupDeal> roomGroupDeals, VisitInformation visitInformation) {
        return roomDealLegend.stream().map(roomDeal -> buildRoomDeal(roomDeal, roomGroupDeals, visitInformation)).collect(Collectors.toList());
    }

    private RoomDealDTO buildRoomDeal(RoomDeal roomDeal, List<RoomGroupDeal> roomGroupDeals, VisitInformation visitInformation) {
        RoomDealDTO roomDealDTO = new RoomDealDTO();
        roomDealDTO.setKey(roomDeal.getKey());
        roomDealDTO.setProviderRoomId(roomDeal.getRoomId());
        roomDealDTO.setProviderId(roomDeal.getProviderId());
        roomDealDTO.setProviderHotelId(roomDeal.getProviderHotelId());
        roomDealDTO.setDescription(roomDeal.getDescription());
        roomDealDTO.setType(roomDeal.getRoomType());
        roomDealDTO.setBoardType(roomDeal.getBoardType().value());
        roomDealDTO.setPayInAdvance(roomDeal.isDepositRequired());
        roomDealDTO.setCancellationFree(roomDeal.isIsCancellationFree());
        roomDealDTO.setRoomsLeft(roomDeal.getRoomsLeft());
        RoomGroupDeal groupDeal = roomGroupDeals.stream().filter(roomGroupDeal -> roomGroupDeal.getRoomsKeys().contains(roomDeal.getKey())).findFirst().orElse(null);
        if (groupDeal != null) {
            roomDealDTO.setPriceBreakdown(priceBreakdownHandler.mapPriceBreakdown(groupDeal, groupDeal.getRoomGroupDealInformation().getProviderCurrency(), visitInformation));
            roomDealDTO.setCancellationPolicy(groupDeal.getRoomGroupDealInformation().getCancellationPolicy());
        }
        roomDealDTO.setDetails(roomDetailsMapper.map(roomDeal));
        return roomDealDTO;
    }
}
