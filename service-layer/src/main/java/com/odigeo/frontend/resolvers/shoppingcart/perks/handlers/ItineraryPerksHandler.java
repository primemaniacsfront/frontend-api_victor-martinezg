package com.odigeo.frontend.resolvers.shoppingcart.perks.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Perks;
import com.google.inject.Singleton;
import graphql.GraphQLContext;

import java.math.BigDecimal;

@Singleton
public class ItineraryPerksHandler extends PerksHandler<Perks> {

    @Override
    public Perks map(GraphQLContext graphQlContext) {
        return getPerks(graphQlContext);
    }

    private Perks getPerks(GraphQLContext graphQlContext) {
        Perks perks = new Perks();
        String currency = getCurrency(graphQlContext);
        BigDecimal primeDiscountFee = getPrimeDiscountFee(graphQlContext);
        Money primeDiscount = new Money(primeDiscountFee, currency);
        perks.setPrimeDiscount(primeDiscount);
        return perks;
    }

}
