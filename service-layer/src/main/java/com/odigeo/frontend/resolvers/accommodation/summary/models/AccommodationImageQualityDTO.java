package com.odigeo.frontend.resolvers.accommodation.summary.models;

public enum AccommodationImageQualityDTO {
    LOW,
    MEDIUM,
    HIGH,
    UNKNOWN;

    public String value() {
        return this.name();
    }

}
