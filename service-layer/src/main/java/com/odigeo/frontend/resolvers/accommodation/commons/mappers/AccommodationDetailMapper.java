package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.summary.mappers.AccommodationFacilitiesMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.mappers.AccommodationRoomServicesMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetail;
import org.apache.commons.lang3.EnumUtils;

import java.util.stream.Collectors;

@Singleton
public class AccommodationDetailMapper {
    @Inject
    private AccommodationDealInformationMapper accommodationDealInformationMapper;
    @Inject
    private AccommodationFacilitiesMapper accommodationFacilitiesMapper;
    @Inject
    private AccommodationImagesMapper accommodationImagesMapper;
    @Inject
    private AccommodationRoomServicesMapper accommodationRoomServicesMapper;

    public AccommodationDetailDTO map(AccommodationDetail accommodationDetail) {
        AccommodationDetailDTO accommodationDetailDTO = new AccommodationDetailDTO();
        accommodationDetailDTO.setAccommodationDealInformation(accommodationDealInformationMapper.map(accommodationDetail.getAccommodationDealInformation()));
        accommodationDetailDTO.setStateProvince(accommodationDetail.getStateProvince());
        accommodationDetailDTO.setCountry(accommodationDetail.getCountry());
        accommodationDetailDTO.setPostalCode(accommodationDetail.getPostalCode());
        accommodationDetailDTO.setMapUrls(accommodationDetail.getMapUrls());
        accommodationDetailDTO.setLocationDescription(accommodationDetail.getLocationDescription());
        accommodationDetailDTO.setSurroundingAreaInfo(accommodationDetail.getSurroundingAreaInfo());
        accommodationDetailDTO.setCheckInPolicy(accommodationDetail.getCheckInPolicy());
        accommodationDetailDTO.setCheckOutPolicy(accommodationDetail.getCheckOutPolicy());
        accommodationDetailDTO.setHotelPolicy(accommodationDetail.getHotelPolicy());
        accommodationDetailDTO.setCreditCardTypes(accommodationDetail.getCreditCardTypes());
        accommodationDetailDTO.setPaymentMethod(accommodationDetail.getPaymentMethod());
        accommodationDetailDTO.setPhoneNumber(accommodationDetail.getPhoneNumber());
        accommodationDetailDTO.setMail(accommodationDetail.getMail());
        accommodationDetailDTO.setWeb(accommodationDetail.getWeb());
        accommodationDetailDTO.setFax(accommodationDetail.getFax());
        accommodationDetailDTO.setAccommodationImages(accommodationDetail.getAccommodationImages().stream().map(accommodationImagesMapper::map).collect(Collectors.toList()));
        accommodationDetailDTO.setFacilityGroups(accommodationFacilitiesMapper.mapFacilityGroups(accommodationDetail.getAccommodationFacilities()));
        accommodationDetailDTO.setAccommodationRoomServices(accommodationDetail.getAccommodationRoomServices().stream().map(accommodationRoomServicesMapper::map).collect(Collectors.toList()));
        accommodationDetailDTO.setNumberOfRooms(accommodationDetail.getNumberOfRooms());
        accommodationDetailDTO.setHotelType(accommodationDetail.getHotelType() != null ? EnumUtils.getEnum(AccommodationType.class, accommodationDetail.getHotelType().value()) : null);

        return accommodationDetailDTO;
    }

}
