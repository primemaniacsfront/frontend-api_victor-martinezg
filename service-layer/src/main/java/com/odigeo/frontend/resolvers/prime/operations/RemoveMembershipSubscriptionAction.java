package com.odigeo.frontend.resolvers.prime.operations;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.resolvers.user.mappers.ModifyMembershipSubscriptionProductMapper;

import java.util.Optional;

@Singleton
public class RemoveMembershipSubscriptionAction implements MembershipSubscriptionShoppingCartAction<ModifyMembershipSubscription, ResolverContext> {
    @Inject
    private ShoppingCartHandler shoppingCartHandler;
    @Inject
    private ModifyMembershipSubscriptionProductMapper modifyMembershipSubscriptionProductMapper;

    @Override
    public ShoppingCartSummaryResponse apply(ModifyMembershipSubscription modifySubscriptionProduct, ResolverContext context) {
        return Optional.ofNullable(modifySubscriptionProduct)
                .map(modifyMembershipSubscriptionProductMapper::toModifyShoppingCartRequest)
                .map(modifyRequest -> shoppingCartHandler.removeProductsToShoppingCart(modifyRequest, context))
                .orElse(null);
    }
}
