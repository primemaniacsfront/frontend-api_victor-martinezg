package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerGender;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserTraveller;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.carrier.CarrierMapper;
import com.odigeo.userprofiles.api.v2.model.Traveller;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Locale;

@Mapper(uses = {CarrierMapper.class, FrequentFlyerMapper.class, IdentificationMapper.class})
public interface TravellerMapper {

    @Mapping(source = "profile.prefixPhoneNumber", target = "prefixPhoneNumber")
    @Mapping(source = "profile.phoneNumber", target = "phoneNumber")
    @Mapping(source = "profile.prefixAlternatePhoneNumber", target = "prefixAlternatePhoneNumber")
    @Mapping(source = "profile.mobilePhoneNumber", target = "mobilePhoneNumber")
    @Mapping(source = "profile.alternatePhoneNumber", target = "alternatePhoneNumber")
    @Mapping(source = "profile.isDefault", target = "isDefault")
    @Mapping(source = "profile.cpf", target = "cpf")
    @Mapping(source = "profile.identificationList", target = "identificationList")
    @Mapping(source = "profile.gender", target = "personalInfo.travellerGender", qualifiedByName = "genderToPersonalInfo")
    @Mapping(source = "profile.address", target = "address")
    @Mapping(source = "typeOfTraveller", target = "personalInfo.ageType")
    @Mapping(source = "frequentFlyerList", target = "frequentFlyerNumbers")
    @Mapping(source = "mealType", target = "meal")
    @Mapping(target = "personalInfo.birthDate", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "isPrimeOwner", constant = "false")
    UserTraveller travellerContractToModel(Traveller traveller);

    @Named("genderToPersonalInfo")
    default TravellerGender genderToPersonalInfo(String gender) {
        TravellerGender travellerGender = TravellerGender.MALE;

        if (!StringUtils.isBlank(gender)) {
            String normalizeGender = gender.toUpperCase(Locale.ROOT);
            if (!"UNKNOWN".equals(normalizeGender)) {
                travellerGender = Enum.valueOf(TravellerGender.class, normalizeGender);
            }
        }
        return travellerGender;
    }
}
