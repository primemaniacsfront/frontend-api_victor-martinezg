package com.odigeo.frontend.resolvers.invoice.mappers;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InvoiceRequest;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;

@Mapper
public interface InvoiceMapper {

    @BeanMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    @Mapping(source = "invoiceZipCode", target = "invoiceZipcode")
    com.odigeo.dapi.client.InvoiceRequest buildShoppingCartInvoiceRequest(InvoiceRequest invoiceRequest);

}
