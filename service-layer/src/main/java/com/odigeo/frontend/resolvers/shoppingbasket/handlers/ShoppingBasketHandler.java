package com.odigeo.frontend.resolvers.shoppingbasket.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddContactDetailsToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddProductToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RemoveProductFromShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketResponse;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.AvailableCollectionMethodsResponseMapper;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ContactDetailsRequestMapper;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ProductRequestMapper;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ShoppingBasketResponseMapper;
import com.odigeo.frontend.services.currencyconvert.CurrencyExchangeHandler;
import com.odigeo.frontend.services.shoppingbasket.ShoppingBasketService;
import com.odigeo.shoppingbasket.v3.model.CustomerCollectionOptions;
import com.odigeo.shoppingbasket.v3.request.ContactDetailsRequest;
import com.odigeo.shoppingbasket.v3.request.CreateRequest;
import com.odigeo.shoppingbasket.v3.request.CustomerCollectionOptionRequest;
import com.odigeo.shoppingbasket.v3.request.ProductRequest;

import java.util.UUID;

public class ShoppingBasketHandler implements ShoppingBasketOperations {

    private static final String CUSTOMER_PRESENCE = "USER_IS_PRESENT";

    @Inject
    private ShoppingBasketService shoppingBasketService;
    @Inject
    private ShoppingBasketResponseMapper shoppingBasketMapper;
    @Inject
    private ProductRequestMapper productRequestMapper;
    @Inject
    private ContactDetailsRequestMapper contactDetailsRequestMapper;
    @Inject
    private CurrencyExchangeHandler currencyExchangeHandler;
    @Inject
    private AvailableCollectionMethodsResponseMapper availableCollectionMethodsResponseMapper;


    @Override
    public ShoppingBasketResponse createShoppingBasket(String bundleId, ResolverContext context) {
        String visitInformation = context.getVisitInformation().getVisitCode();
        CreateRequest createRequest = new CreateRequest();
        createRequest.setVisitInformation(visitInformation);
        com.odigeo.shoppingbasket.v3.response.ShoppingBasketResponse shoppingBasketResponse = shoppingBasketService.create(createRequest);

        return shoppingBasketMapper.map(shoppingBasketResponse);
    }

    @Override
    public ShoppingBasketResponse getShoppingBasket(String shoppingBasketId, ResolverContext context) {

        com.odigeo.shoppingbasket.v3.response.ShoppingBasketResponse shoppingBasketResponse = shoppingBasketService.getShoppingBasket(UUID.fromString(shoppingBasketId));

        return shoppingBasketMapper.map(shoppingBasketResponse);
    }

    @Override
    public AvailableCollectionMethodsResponse getAvailableCollectionMethods(String shoppingBasketId, ResolverContext context) {

        CustomerCollectionOptions availableCollectionMethods = shoppingBasketService.getAvailableCollectionMethods(UUID.fromString(shoppingBasketId), getCustomerCollectionOptionRequest());

        return availableCollectionMethodsResponseMapper.map(availableCollectionMethods);
    }

    private CustomerCollectionOptionRequest getCustomerCollectionOptionRequest() {
        CustomerCollectionOptionRequest request = new CustomerCollectionOptionRequest();
        request.setCurrencyConverterId(currencyExchangeHandler.getCurrentCurrencyConvertId().orElse(null));
        request.setCustomerPresence(CUSTOMER_PRESENCE);
        return request;
    }

    @Override
    public Boolean removeProduct(RemoveProductFromShoppingBasketRequest removeProductFromShoppingBasketRequest, ResolverContext context) {
        ProductRequest productRequest = productRequestMapper.map(
                removeProductFromShoppingBasketRequest.getProduct().getId(),
                removeProductFromShoppingBasketRequest.getProduct().getType().name()
        );

        shoppingBasketService.removeProduct(
                UUID.fromString(removeProductFromShoppingBasketRequest.getShoppingBasketId()),
                productRequest
        );

        return Boolean.TRUE;
    }

    @Override
    public Boolean addProduct(AddProductToShoppingBasketRequest addProductToShoppingBasketRequest, ResolverContext context) {
        ProductRequest productRequest = productRequestMapper.map(
                addProductToShoppingBasketRequest.getProduct().getId(),
                addProductToShoppingBasketRequest.getProduct().getType().name()
        );

        shoppingBasketService.addProduct(
                UUID.fromString(addProductToShoppingBasketRequest.getShoppingBasketId()),
                productRequest
        );

        return Boolean.TRUE;
    }

    @Override
    public Boolean addContactDetails(AddContactDetailsToShoppingBasketRequest addContactDetailsToShoppingBasketRequest) {
        ContactDetailsRequest contactDetailsRequest = contactDetailsRequestMapper.map(addContactDetailsToShoppingBasketRequest);
        shoppingBasketService.addContactDetails(UUID.fromString(addContactDetailsToShoppingBasketRequest.getShoppingId()), contactDetailsRequest);

        return Boolean.TRUE;
    }
}
