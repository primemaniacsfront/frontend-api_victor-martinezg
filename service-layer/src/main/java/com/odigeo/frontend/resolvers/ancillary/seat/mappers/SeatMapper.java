package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Seat;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryapi.v1.response.ProviderSeatMapPreferencesDescriptorItem;
import com.odigeo.itineraryapi.v1.response.Money;
import com.odigeo.itineraryapi.v1.response.SeatLocation;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatType;
import org.apache.commons.lang3.EnumUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class SeatMapper {

    @Inject
    private MoneyMapper moneyMapper;
    @Inject
    private SeatCharacteristicsMapper seatCharacteristicsMapper;

    public List<Seat> map(List<ProviderSeatMapPreferencesDescriptorItem> providerSeatList) {
        return Optional.ofNullable(providerSeatList)
            .orElse(Collections.emptyList())
            .stream()
            .map(this::mapSeat)
            .collect(Collectors.toList());
    }

    private Seat mapSeat(ProviderSeatMapPreferencesDescriptorItem providerSeat) {
        Seat seat = new Seat();
        SeatLocation seatLocation = providerSeat.getSeatLocation();

        seat.setFloor(seatLocation.getFloor());
        seat.setRow(seatLocation.getSeatRow());
        seat.setSeatMapRow(seatLocation.getSeatMapRow());
        seat.setColumn(seatLocation.getColumn());
        seat.setType(EnumUtils.getEnum(SeatType.class, providerSeat.getSeatType().getSeatType(), SeatType.NO_SEAT));
        seat.setCharacteristics(seatCharacteristicsMapper.map(providerSeat.getCharacteristics()));
        seat.setFee(moneyMapper.map(calculateFee(providerSeat.getTotalPrice(), providerSeat.getProviderPrice())));
        seat.setPrice(moneyMapper.map(providerSeat.getTotalPrice()));

        return seat;
    }

    private Money calculateFee(Money totalPrice, Money providerPrice) {
        Money providerFee = new Money();
        providerFee.setAmount(totalPrice.getAmount().subtract(providerPrice.getAmount()));
        providerFee.setCurrencyCode(totalPrice.getCurrencyCode());
        return providerFee;
    }
}
