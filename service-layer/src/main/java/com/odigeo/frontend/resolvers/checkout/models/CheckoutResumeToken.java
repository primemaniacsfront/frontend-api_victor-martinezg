package com.odigeo.frontend.resolvers.checkout.models;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.common.annotations.VisibleForTesting;

public class CheckoutResumeToken {

    @VisibleForTesting
    static final String SEPARATOR = "/";

    private long shoppingId;
    private ShoppingType shoppingType;
    private String paymentMethod;
    private int interactionStep;
    private String userPaymentInteractionId;

    public static CheckoutResumeToken valueOf(String value) {
        String[] tokenValues = value.split(SEPARATOR);
        return new CheckoutResumeToken(Long.parseLong(tokenValues[0]),
                ShoppingType.valueOf(tokenValues[1]), tokenValues[2],
                Integer.parseInt(tokenValues[3]), tokenValues[4]);
    }

    public CheckoutResumeToken() {
    }

    public CheckoutResumeToken(long shoppingId, ShoppingType shoppingType, String paymentMethod, int interactionStep, String userPaymentInteractionId) {
        this.shoppingId = shoppingId;
        this.shoppingType = shoppingType;
        this.paymentMethod = paymentMethod;
        this.interactionStep = interactionStep;
        this.userPaymentInteractionId = userPaymentInteractionId;
    }

    public long getShoppingId() {
        return shoppingId;
    }

    public void setShoppingId(long shoppingId) {
        this.shoppingId = shoppingId;
    }

    public ShoppingType getShoppingType() {
        return shoppingType;
    }

    public void setShoppingType(ShoppingType shoppingType) {
        this.shoppingType = shoppingType;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getInteractionStep() {
        return interactionStep;
    }

    public void setInteractionStep(int interactionStep) {
        this.interactionStep = interactionStep;
    }

    public String getUserPaymentInteractionId() {
        return userPaymentInteractionId;
    }

    public void setUserPaymentInteractionId(String userPaymentInteractionId) {
        this.userPaymentInteractionId = userPaymentInteractionId;
    }

    @Override
    public String toString() {
        return shoppingId + SEPARATOR + shoppingType.name() + SEPARATOR + paymentMethod  + SEPARATOR + interactionStep + SEPARATOR + userPaymentInteractionId;
    }

}
