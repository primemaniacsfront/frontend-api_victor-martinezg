package com.odigeo.frontend.resolvers.prime.models.retention;

public class RetentionFlowInfo {
    private RetentionFlowGroup flow;
    private RetentionFlowTravel travel;


    public RetentionFlowGroup getFlow() {
        return flow;
    }

    public void setFlow(RetentionFlowGroup flow) {
        this.flow = flow;
    }

    public RetentionFlowTravel getTravel() {
        return travel;
    }

    public void setTravel(RetentionFlowTravel travel) {
        this.travel = travel;
    }
}
