package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrl;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrlType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.Insurance;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class NonEssentialProductUrlsMapper {

    @Inject
    private NonEssentialProductUrlMapper nonEssentialProductUrlMapper;

    public List<NonEssentialProductUrl> map(Insurance insurance, AncillaryConfiguration ancillaryConfiguration) {
        List<NonEssentialProductUrl> nonEssentialProductUrls = Optional.ofNullable(insurance)
            .map(Insurance::getConditionsUrls)
            .orElse(Collections.emptyList())
            .stream()
            .map(nonEssentialProductUrlMapper::map)
            .collect(Collectors.toList());

        if (BooleanUtils.isTrue(ancillaryConfiguration.getIpid())) {
            NonEssentialProductUrl nonEssentialProductUrl = new NonEssentialProductUrl();
            // TODO: complete here ipid url
            nonEssentialProductUrl.setUrl("");
            nonEssentialProductUrl.setUrlType(NonEssentialProductUrlType.IPID);
            nonEssentialProductUrls.add(nonEssentialProductUrl);
        }
        return nonEssentialProductUrls;
    }
}
