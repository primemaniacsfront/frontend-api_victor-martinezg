package com.odigeo.frontend.resolvers.checkout.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutFailure;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCart;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.BookingSummary;
import com.odigeo.dapi.client.ErrorType;
import com.odigeo.frontend.resolvers.checkout.CheckoutFetcherResult;
import com.odigeo.frontend.resolvers.invoice.mappers.InvoiceMapper;
import com.odigeo.frontend.resolvers.shoppingcart.models.ConfirmRequest;
import graphql.GraphQLContext;
import graphql.GraphQLError;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.ValueMapping;
import org.mapstruct.ValueMappings;

import java.util.List;

@Mapper(uses = {CheckoutStatusMapper.class, InvoiceMapper.class})
@DecoratedWith(CheckoutMapperDecorator.class)
public interface CheckoutMapper {

    String UNKNOWN = "UNKNOWN";

    @Mapping(source = "request.shoppingId", target = "bookingId")
    @Mapping(source = "request.shoppingId", target = "clientBookingReferencesId")
    @Mapping(source = "request.expectedPrice", target = "paymentData.bookingRequest.expectedPrice.amount")
    @Mapping(source = "request.collectionMethod.collectionMethodType", target = "paymentData.bookingRequest.collectionMethodType")
    @Mapping(source = "currency", target = "paymentData.bookingRequest.expectedPrice.currency")
    @Mapping(target = "paymentData.bookingRequest.creditCardRequest", ignore = true)
    ConfirmRequest buildShoppingCartConfirmRequest(CheckoutRequest request, String currency, GraphQLContext graphQlContext) throws NumberFormatException;

    @Mapping(source = "response.status", target = "statusInfo.status")
    @Mapping(source = "userPaymentInteractionId", target = "userPaymentInteractionId.id")
    @Mapping(target = "shoppingCart", ignore = true)
    @Mapping(target = "status", ignore = true)
    CheckoutResponse mapShoppingCartToCheckoutResponse(BookingResponse response, String userPaymentInteractionId);

    @Mapping(source = "graphQLErrors", target = "errors")
    CheckoutFetcherResult mapShoppingCartToCheckoutFetcherResult(BookingResponse response, String userPaymentInteractionId, List<GraphQLError> graphQLErrors);

    @Mapping(target = "itinerary", ignore = true)
    @Mapping(source = "id", target = "bookingId")
    ShoppingCart mapBookingSummaryOnConfirmResponse(BookingSummary bookingSummary);

    @Mapping(target = "itinerary", ignore = true)
    @Mapping(target = "accommodationShoppingItem", ignore = true)
    @Mapping(target = "requiredBuyerInformation", ignore = true)
    @Mapping(target = "requiredTravellerInformation", ignore = true)
    ShoppingCart mapShoppingCartNotConfirmed(com.odigeo.dapi.client.ShoppingCart shoppingCart);

    @ValueMappings({
        @ValueMapping(source = "ERR-003", target = "INVALID_PARAMETERS"),
        @ValueMapping(source = "ERR-004", target = "COLLECTION_OPTION_NOT_OFFERED"),
        @ValueMapping(source = "ERR-005", target = "INVALID_EXPIRATION_DATE"),
        @ValueMapping(source = "ERR-006", target = "PROFESSIONAL_INVOICING_INFO_NOT_INFORMED"),
        @ValueMapping(source = "ERR-008", target = "INVALID_SESSION"),
        @ValueMapping(source = "ERR-009", target = "VALIDATION_ERROR"),
        @ValueMapping(source = "ERR-018", target = "ERROR_IN_PROVIDER"),
        @ValueMapping(source = "WNG-037", target = "PRODUCT_NOT_READY_FOR_CHECKOUT"),
        @ValueMapping(source = "INT-001", target = "INTERNAL_ERROR"),
        @ValueMapping(source = MappingConstants.ANY_REMAINING, target = "VALIDATION_ERROR")
        })
    CheckoutFailure mapDapiCodeErrorToCheckoutFailure(String code);

    @ValueMappings({
        @ValueMapping(source = UNKNOWN, target = "UNKNOWN"),
        @ValueMapping(source = "CARD_DENIED", target = "CARD_DENIED"),
        @ValueMapping(source = "CARD_TYPE_NOTMATCH", target = "CARD_TYPE_NOTMATCH"),
        @ValueMapping(source = "FARE_CHANGED", target = "FARE_CHANGED"),
        @ValueMapping(source = "NOT_MATCHING_PRICE", target = "NOT_MATCHING_PRICE"),
        @ValueMapping(source = "UNAVAILABLE_PROVIDER", target = "UNAVAILABLE_PROVIDER"),
        @ValueMapping(source = "CONFIRMATION_ERROR", target = "CONFIRMATION_ERROR"),
        @ValueMapping(source = "PROVIDER_INTERNAL_ERROR", target = "PROVIDER_INTERNAL_ERROR"),
        @ValueMapping(source = "INVALID_REQUEST_SENT_TO_PROVIDER", target = "INVALID_REQUEST_SENT_TO_PROVIDER"),
        @ValueMapping(source = "EXPIRED_CARD_AT_CHECK_IN", target = "EXPIRED_CARD_AT_CHECK_IN"),
        @ValueMapping(source = "BOOKING_NOT_CONFIRMED", target = "BOOKING_NOT_CONFIRMED"),
        @ValueMapping(source = "DUPLICITY", target = "DUPLICITY"),
        @ValueMapping(source = "ERROR_PAYMENT_TYPE", target = "ERROR_PAYMENT_TYPE"),
        @ValueMapping(source = "PROVIDER_PAYMENT_GATEWAY_DOWN", target = "PROVIDER_PAYMENT_GATEWAY_DOWN"),
        @ValueMapping(source = "CARD_TYPE_DEBIT_NOTMATCH", target = "CARD_TYPE_DEBIT_NOTMATCH"),
        @ValueMapping(source = "EMPTY_COLLECTION_OPTIONS", target = "EMPTY_COLLECTION_OPTIONS"),
        @ValueMapping(source = "WRONG_BOOKING_DATA", target = "WRONG_BOOKING_DATA"),
        @ValueMapping(source = "HIGH_FRAUD_RISK", target = "HIGH_FRAUD_RISK"),
        @ValueMapping(source = "COLLECTION_DECLINED", target = "COLLECTION_DECLINED"),
        @ValueMapping(source = "COLLECTION_WITH_3_D_DECLINED", target = "COLLECTION_WITH_3_D_DECLINED"),
        @ValueMapping(source = "COLLECTION_ERROR", target = "COLLECTION_ERROR"),
        @ValueMapping(source = "REPRICING_ABOVE_THRESHOLD", target = "REPRICING_ABOVE_THRESHOLD"),
        @ValueMapping(source = MappingConstants.NULL, target = UNKNOWN),
        @ValueMapping(source = MappingConstants.ANY_REMAINING, target = UNKNOWN)
        })
    CheckoutFailure mapDapiFlowErrorToCheckoutFailure(ErrorType errorType);
}
