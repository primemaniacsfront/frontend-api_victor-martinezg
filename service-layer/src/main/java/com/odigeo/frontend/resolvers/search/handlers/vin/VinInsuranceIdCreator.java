package com.odigeo.frontend.resolvers.search.handlers.vin;

import com.odigeo.frontend.contract.ancillaries.InsuranceOfferDTO;

import java.util.HashMap;
import java.util.Map;

class VinInsuranceIdCreator {

    private int currentId;
    private final Map<InsuranceOfferDTO, Integer> insuranceIds;

    VinInsuranceIdCreator() {
        insuranceIds = new HashMap<>();
    }

    public void createInsuranceId(InsuranceOfferDTO insurance) {
        Integer id = insuranceIds.get(insurance);

        if (id == null) {
            id = currentId++;
            insuranceIds.put(insurance, id);
        }

        insurance.setId(id);
    }

}
