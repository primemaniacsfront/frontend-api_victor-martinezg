package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Singleton;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.TripType;

@Singleton
class MirTripTypeMapper {

    public TripType map(SearchRequest searchRequestDTO) {
        return searchRequestDTO.getTripType() == com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType.ONE_WAY
            ? TripType.ONE_WAY
            : TripType.ROUND_TRIP;
    }

}
