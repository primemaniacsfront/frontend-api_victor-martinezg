package com.odigeo.frontend.resolvers.accommodation.summary.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationRoomServiceDTO;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationRoomService;

@Singleton
public class AccommodationRoomServicesMapper {

    public AccommodationRoomServiceDTO map(AccommodationRoomService accommodationRoomService) {
        AccommodationRoomServiceDTO dto = new AccommodationRoomServiceDTO();
        dto.setType(accommodationRoomService.getType());
        dto.setDescription(accommodationRoomService.getDescription());
        return dto;
    }

}
