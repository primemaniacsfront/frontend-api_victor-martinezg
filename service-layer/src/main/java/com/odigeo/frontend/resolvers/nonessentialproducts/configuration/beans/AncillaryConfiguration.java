package com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans;

import com.google.gson.Gson;

import java.io.Serializable;

public class AncillaryConfiguration implements Serializable {

    private String theme;
    private Boolean mandatory;
    private Boolean rejectNagEnabled;
    private Boolean prechecked;
    private Boolean continuanceNagEnabled;
    private Boolean isModalActive;
    private Boolean showModalExclusions;
    private Boolean showModalBenefits;
    private Boolean showModalDualContract;
    private Boolean isAttachableFromModal;
    private PricePerceptionType pricePerception;
    private Boolean ipid;
    private Boolean highlighted;

    private static final long serialVersionUID = 6340897846476813832L;

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Boolean isRejectNagEnabled() {
        return rejectNagEnabled;
    }

    public void setRejectNagEnabled(Boolean rejectNagEnabled) {
        this.rejectNagEnabled = rejectNagEnabled;
    }

    public Boolean getPrechecked() {
        return prechecked;
    }

    public void setPrechecked(Boolean prechecked) {
        this.prechecked = prechecked;
    }

    public Boolean isContinuanceNagEnabled() {
        return this.continuanceNagEnabled;
    }

    public void setContinuanceNagEnabled(Boolean nag) {
        this.continuanceNagEnabled = nag;
    }

    public Boolean getIsModalActive() {
        return isModalActive;
    }

    public void setIsModalActive(Boolean isModalActive) {
        this.isModalActive = isModalActive;
    }

    public Boolean getIsAttachableFromModal() {
        return isAttachableFromModal;
    }

    public void setIsAttachableFromModal(Boolean isAttachableFromModal) {
        this.isAttachableFromModal = isAttachableFromModal;
    }

    public Boolean getShowModalExclusions() {
        return showModalExclusions;
    }

    public void setShowModalExclusions(Boolean showModalExclusions) {
        this.showModalExclusions = showModalExclusions;
    }

    public Boolean getShowModalBenefits() {
        return showModalBenefits;
    }

    public void setShowModalBenefits(Boolean showModalBenefits) {
        this.showModalBenefits = showModalBenefits;
    }

    public Boolean getShowModalDualContract() {
        return showModalDualContract;
    }

    public void setShowModalDualContract(Boolean showModalDualContract) {
        this.showModalDualContract = showModalDualContract;
    }

    public PricePerceptionType getPricePerception() {
        return pricePerception;
    }

    public void setPricePerception(PricePerceptionType pricePerception) {
        this.pricePerception = pricePerception;
    }

    public Boolean getIpid() {
        return ipid;
    }

    public void setIpid(Boolean ipid) {
        this.ipid = ipid;
    }

    public Boolean getHighlighted() {
        return highlighted;
    }

    public void setHighlighted(Boolean highlighted) {
        this.highlighted = highlighted;
    }


    public AncillaryConfiguration cloneMe() {
        Gson gson = new Gson();
        String jsonString = gson.toJson(this);
        return gson.fromJson(jsonString, this.getClass());
    }
}
