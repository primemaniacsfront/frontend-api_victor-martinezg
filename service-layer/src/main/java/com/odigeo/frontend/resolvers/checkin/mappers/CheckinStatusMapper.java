package com.odigeo.frontend.resolvers.checkin.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Airport;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BoardingPass;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BoardingPassStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinSection;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinStatus;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.checkin.api.v1.model.response.BoardingPassMobile;
import com.odigeo.checkin.api.v1.model.response.CheckinInformation;
import com.odigeo.checkin.api.v1.model.response.Section;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.commons.util.GeoUtils;

import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class CheckinStatusMapper {

    @Inject
    private GeoUtils geoUtils;
    @Inject
    private DateUtils dateUtils;

    public CheckinStatus map(com.odigeo.checkin.api.v1.model.response.CheckinStatus checkinStatus) {
        CheckinStatus status = new CheckinStatus();
        status.setSections(
                Optional.ofNullable(checkinStatus.getCheckinInformation())
                        .map(CheckinInformation::getSections)
                        .orElse(Collections.emptyList())
                        .stream()
                        .map(this::mapSection)
                        .collect(Collectors.toList())
        );

        return status;
    }

    private CheckinSection mapSection(Section section) {
        CheckinSection checkinSection = new CheckinSection();
        checkinSection.setId(section.getSectionId());
        checkinSection.setPassengerName(section.getPassengerName());
        checkinSection.setStatus(BoardingPassStatus.valueOf(section.getStatus()));

        checkinSection.setBoardingPass(mapBoardingPass(section));

        return checkinSection;
    }

    private BoardingPass mapBoardingPass(Section section) {
        BoardingPass boardingPass = new BoardingPass();
        boardingPass.setId(section.getBoardingPassId());
        boardingPass.setUrl(section.getBoardingPassUrl());

        Optional.ofNullable(section.getBoardingPassMobile())
                .ifPresent(internalBoardingPass -> {
                    boardingPass.setFlightCode(internalBoardingPass.getFlightName());
                    boardingPass.setCarrierName(internalBoardingPass.getCarrierName());

                    boardingPass.setSeat(internalBoardingPass.getSeat());
                    mapAirports(boardingPass, internalBoardingPass);
                });

        return boardingPass;
    }

    private void mapAirports(BoardingPass boardingPass, BoardingPassMobile boardingPassMobile) {
        Airport departure = new Airport();
        departure.setName(boardingPassMobile.getDepartureAirportName());
        departure.setIata(boardingPassMobile.getDepartureAirport());

        departure.setDatetime(dateUtils.formatToOffsetDateTime(
                boardingPassMobile.getDepartureDate(),
                boardingPassMobile.getDepartureTime(),
                geoUtils.getZoneIdFromAirportIata(boardingPassMobile.getDepartureAirport())
        ));
        departure.setBoardingTime(boardingPassMobile.getBoardingTime());
        boardingPass.setDeparture(departure);

        Airport arrival = new Airport();
        arrival.setName(boardingPassMobile.getArrivalAirportName());
        arrival.setIata(boardingPassMobile.getArrivalAirport());

        arrival.setDatetime(dateUtils.formatToOffsetDateTime(
                boardingPassMobile.getArrivalDate(),
                boardingPassMobile.getArrivalTime(),
                geoUtils.getZoneIdFromAirportIata(boardingPassMobile.getArrivalAirport())
        ));
        boardingPass.setArrival(arrival);
    }
}
