package com.odigeo.frontend.resolvers.search.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.search.handlers.carbonfootprint.CarbonFootprintDebugHandler;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerarySearchResults;

import java.math.BigDecimal;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections4.MapUtils;

@Singleton
public class SearchDebugHandler {

    private static final String API_CAPTURE_LABEL = "apiCapture";
    private static final String API_CAPTURE_SEARCH_REQUEST_LABEL = "search-engine-request";
    private static final String API_CAPTURE_SEARCH_RESPONSE_LABEL = "search-engine-response";

    private static final String QAMODE_LABEL = "qamode";
    private static final String QAMODE_SEARCH_ENGINE_LABEL = "search-engine";
    private static final String QAMODE_ME_RATING_LABEL = "me-rating";
    private static final String QAMODE_FOOTPRINT_CALCULATOR = "footprint-calculator";

    @Inject
    private CarbonFootprintDebugHandler carbonFootprintDebugHandler;

    public void addApiCaptureInfo(ResolverContext context, SearchMethodRequest request, SearchResponse response) {
        DebugInfo debugInfo = context.getDebugInfo();

        if (debugInfo.isApiCaptureEnabled()) {
            debugInfo.getDebugInfoMap().put(API_CAPTURE_LABEL, Stream.of(
                new SimpleEntry<>(API_CAPTURE_SEARCH_REQUEST_LABEL, request),
                new SimpleEntry<>(API_CAPTURE_SEARCH_RESPONSE_LABEL, response)
            ).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue)));
        }
    }

    public void addQaModeSearchInfo(ResolverContext context, SearchResponse response) {
        DebugInfo debugInfo = context.getDebugInfo();

        if (debugInfo.isQaModeEnabled()) {
            Map<String, Object> qaModeMap = getQaModeMap(debugInfo);
            qaModeMap.put(QAMODE_SEARCH_ENGINE_LABEL, createQaModeSearchEngineInfo(response));
        }
    }

    private Map<String, Object> createQaModeSearchEngineInfo(SearchResponse searchResponse) {
        ItinerarySearchResults itinerarySearchResults = searchResponse.getItinerarySearchResults();

        if (Objects.nonNull(itinerarySearchResults)) {
            return itinerarySearchResults.getItineraryResults().stream()
                .map(itinerary -> new SimpleEntry<>(itinerary.getKey(), itinerary.getDebugInfo()))
                .collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));
        }

        return Collections.emptyMap();
    }

    public void addQaModeMeRatingInfo(ResolverContext context, List<SearchItineraryDTO> itineraries) {
        DebugInfo debugInfo = context.getDebugInfo();

        if (debugInfo.isQaModeEnabled()) {
            Map<String, Object> qaModeMap = getQaModeMap(debugInfo);
            qaModeMap.put(QAMODE_ME_RATING_LABEL, itineraries.stream()
                .collect(Collectors.toMap(SearchItineraryDTO::getId, SearchItineraryDTO::getMeRating)));
        }

    }

    public void addQaModeCarbonFootprintInfo(ResolverContext context, List<SearchItineraryDTO> itineraries, BigDecimal averageKilos) {
        DebugInfo debugInfo = context.getDebugInfo();

        if (debugInfo.isQaModeEnabled()) {
            Map<String, Object> qaModeMap = getQaModeMap(debugInfo);
            qaModeMap.put(QAMODE_FOOTPRINT_CALCULATOR, carbonFootprintDebugHandler.getDebugInformation(itineraries, averageKilos));
        }

    }

    private Map<String, Object> getQaModeMap(DebugInfo debugInfo) {
        Map<String, Object> debugInfoMap = debugInfo.getDebugInfoMap();
        Map<?, ?> qaModeMap = MapUtils.getMap(debugInfoMap, QAMODE_LABEL, new HashMap<>());

        debugInfoMap.put(QAMODE_LABEL, qaModeMap);
        return (Map<String, Object>) qaModeMap;
    }

}
