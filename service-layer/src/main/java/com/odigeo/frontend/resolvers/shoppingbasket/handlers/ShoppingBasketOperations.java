package com.odigeo.frontend.resolvers.shoppingbasket.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddContactDetailsToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddProductToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RemoveProductFromShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketResponse;
import com.odigeo.frontend.commons.context.ResolverContext;

public interface ShoppingBasketOperations {
    ShoppingBasketResponse createShoppingBasket(String bundleId, ResolverContext context);

    ShoppingBasketResponse getShoppingBasket(String shoppingBasketId, ResolverContext context);

    AvailableCollectionMethodsResponse getAvailableCollectionMethods(String shoppingBasketId, ResolverContext context);

    Boolean removeProduct(RemoveProductFromShoppingBasketRequest removeProductFromShoppingBasketRequest, ResolverContext context);

    Boolean addProduct(AddProductToShoppingBasketRequest addProductToShoppingBasketRequest, ResolverContext context);

    Boolean addContactDetails(AddContactDetailsToShoppingBasketRequest addContactDetailsToShoppingBasketRequest);
}
