package com.odigeo.frontend.resolvers.user;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPageName;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPagesResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipTokenResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.prime.pages.MembershipPageFactory;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import graphql.VisibleForTesting;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.Collections;
import java.util.Optional;

@Singleton
public class MembershipResolver implements Resolver {
    @VisibleForTesting
    static final String GET_USER_MEMBERSHIP_QUERY = "getUserMembership";
    @VisibleForTesting
    static final String GET_MEMBERSHIP_QUERY = "membership";
    @VisibleForTesting
    static final String UPDATE_USER_MEMBERSHIP_QUERY = "updateUserMembership";
    @VisibleForTesting
    static final String MEMBERSHIP_PAGES = "membershipPages";
    @VisibleForTesting
    static final String TOKEN_INFO = "tokenInfo";
    private static final String MEMBERSHIP = "membership";
    public static final String QUERY = "Query";
    public static final String MUTATION = "Mutation";
    public static final String USER = "User";
    @Inject
    private MembershipHandler membershipHandler;
    @Inject
    private MembershipPageFactory membershipPageFactory;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type(QUERY, typeWiring -> typeWiring
                        .dataFetcher(GET_USER_MEMBERSHIP_QUERY, this::membershipFromLoggedUserDataFetcher))
                .type(QUERY, typeWiring -> typeWiring
                        .dataFetcher(GET_MEMBERSHIP_QUERY, this::membershipDataFetcher))
                .type(QUERY, typeWiring -> typeWiring
                        .dataFetcher(MEMBERSHIP_PAGES, this::membershipPagesFetcher))
                .type(QUERY, typeWiring -> typeWiring
                        .dataFetcher(TOKEN_INFO, this::tokenInfo))
                .type(MUTATION, typeWiring -> typeWiring
                        .dataFetcher(UPDATE_USER_MEMBERSHIP_QUERY, this::updateMembershipDataFetcher))
                .type(USER, typeWiring -> typeWiring
                        .dataFetcher(MEMBERSHIP, this::membershipFromLoggedUserDataFetcher));
    }

    @VisibleForTesting
    Membership updateMembershipDataFetcher(DataFetchingEnvironment env) {
        return membershipHandler.updateMembershipData(env);
    }

    @VisibleForTesting
    Membership membershipFromLoggedUserDataFetcher(DataFetchingEnvironment env) {
        Membership membership = membershipHandler.getMembershipFromLoggedUser(env);
        persistMembershipInContext(env, membership);
        return membership;
    }

    @VisibleForTesting
    Membership membershipDataFetcher(DataFetchingEnvironment env) {
        MembershipRequest membershipRequest = jsonUtils.fromDataFetching(env, MembershipRequest.class);
        ShoppingInfo shoppingInfo = Optional.ofNullable(membershipRequest).map(MembershipRequest::getShoppingInfo).orElse(null);
        Membership membership = membershipHandler.getMembership(env, shoppingInfo);
        persistMembershipInContext(env, membership);
        return membership;
    }

    @VisibleForTesting
    MembershipPagesResponse membershipPagesFetcher(DataFetchingEnvironment env) {
        MembershipPageName page = membershipPageFactory.getPage(env);
        return new MembershipPagesResponse(page != null ? Collections.singletonList(page) : Collections.emptyList());
    }

    @VisibleForTesting
    MembershipTokenResponse tokenInfo(DataFetchingEnvironment env) {
        String token = jsonUtils.fromDataFetching(env);
        return membershipHandler.getTokenInfo(token);
    }

    private void persistMembershipInContext(DataFetchingEnvironment env, Membership membership) {
        if (membership != null) {
            env.getGraphQlContext().put("Membership", membership);
        }
    }
}
