package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.search.handlers.SearchDebugHandler;
import com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering.types.AirlineSteeringPosition;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.odigeo.frontend.monitoring.MeasureConstants.SEARCH_AIRLINE_STEERING_TOTAL_TIME;

@Singleton
public class AirlineSteeringHandler {

    @Inject
    private AirlineSteeringManager airlineSteeringManager;

    @Inject
    private MetricsHandler metricsHandler;

    @Inject
    private Logger logger;

    @Inject
    private SearchDebugHandler debugHandler;

    private final Predicate<String> isActivatedAirlinePredicate;

    private static final double RECALCULATE_RATING = 0.99999;
    private static final BigDecimal SUBSTRACT_VALUE = BigDecimal.valueOf(0.00001);

    public AirlineSteeringHandler() {
        isActivatedAirlinePredicate = (id) -> airlineSteeringManager.getAirlines().contains(id);
    }

    public void boostingAirline(SearchResponseDTO searchResponseDTO, ResolverContext context) {

        VisitInformation visit = context.getVisitInformation();

        AirlineSteeringPosition steeringPosition = airlineSteeringManager.getAirlineSteeringPosition(visit);

        if (isCorrectPartitionAndResponse(steeringPosition, searchResponseDTO.getItineraries())) {

            metricsHandler.startMetric(SEARCH_AIRLINE_STEERING_TOTAL_TIME);

            List<SearchItineraryDTO> itineraries = searchResponseDTO.getItineraries().stream()
                    .sorted(new ItinerarySteeringComparator(searchResponseDTO.getDefaultFeeType()))
                    .collect(Collectors.toList());

            positionHandler(itineraries, steeringPosition, searchResponseDTO.getDefaultFeeType());

            recalculateMeRating(itineraries);

            metricsHandler.stopMetric(SEARCH_AIRLINE_STEERING_TOTAL_TIME, visit);

            debugHandler.addQaModeMeRatingInfo(context, itineraries);
        }
    }

    private boolean isCorrectPartitionAndResponse(AirlineSteeringPosition steeringPosition, List<SearchItineraryDTO> itineraries) {
        return !steeringPosition.equals(AirlineSteeringPosition.NONE) && responseContainsAnyActivatedAirline(itineraries);
    }

    private boolean responseContainsAnyActivatedAirline(List<SearchItineraryDTO> itineraryDTOList) {
        Set<String> airlines = itineraryDTOList.stream().map(SearchItineraryDTO::getSectionsCarrierIds)
                .flatMap(Collection::stream).collect(Collectors.toSet());

        return airlines.stream().anyMatch(isActivatedAirlinePredicate);
    }

    private void positionHandler(List<SearchItineraryDTO> itineraries, AirlineSteeringPosition steeringPosition, FeeType feeType) {
        for (int i = 1; i < itineraries.size(); i++) {

            SearchItineraryDTO itineraryDTO = itineraries.get(i);
            if (itineraryDTO.getMeRating() == null) {
                break;
            }

            if (isItineraryWithCarrierSteering(itineraryDTO)) {
                int indexSteering = getIndexSteering(i, steeringPosition);
                changePosition(indexSteering, i, itineraries, feeType);
            }
        }
    }

    private int getIndexSteering(int index, AirlineSteeringPosition steeringPosition) {
        int changePosition = index - steeringPosition.getBoostingPosition();
        return changePosition < 0 ? 0 : changePosition;
    }

    private void changePosition(int indexSteering, int indexItinerary, List<SearchItineraryDTO> itineraries, FeeType feeType) {

        for (int j = indexSteering; j <= indexItinerary; j++) {

            SearchItineraryDTO itinerarySteeringPosition = itineraries.get(j);
            SearchItineraryDTO itineraryBoosting = itineraries.get(indexItinerary);

            if (!isItineraryWithCarrierSteering(itinerarySteeringPosition)
                    && isOnPercentageRange(itinerarySteeringPosition, itineraryBoosting, feeType)) {

                itineraries.set(j, itineraryBoosting);

                itineraries.set(indexItinerary, itinerarySteeringPosition);

                for (int i = j + 1; i <= indexItinerary && i < itineraries.size(); i++) {
                    SearchItineraryDTO itineraryDTOAux = itineraries.get(i);
                    itineraries.set(i, itinerarySteeringPosition);
                    itinerarySteeringPosition = itineraryDTOAux;
                }
                break;
            }
        }
    }

    private boolean isOnPercentageRange(SearchItineraryDTO itinerarySteeringPosition, SearchItineraryDTO itineraryBoosting, FeeType feeType) {
        Optional<BigDecimal> priceItinerary = ItinerarySteeringUtils.getPrice(itineraryBoosting, feeType.getId());
        Optional<BigDecimal> priceToCompare = ItinerarySteeringUtils.getPrice(itinerarySteeringPosition, feeType.getId());

        boolean isOnRange = false;
        if (priceItinerary.isPresent() && priceToCompare.isPresent()) {
            isOnRange = ItinerarySteeringUtils.isPriceLowerPercentage(priceItinerary.get(), priceToCompare.get(), airlineSteeringManager.getControlPercentagePrice());
        } else {
            logger.warning(this.getClass(), "WARN: Itineraries without prices. FeeType: " + feeType.getId());
        }
        return isOnRange;
    }

    private void recalculateMeRating(List<SearchItineraryDTO> itineraries) {
        BigDecimal meRating = BigDecimal.valueOf(RECALCULATE_RATING);

        for (SearchItineraryDTO itineraryDTO : itineraries.stream().filter(i -> i.getMeRating() != null).collect(Collectors.toList())) {
            itineraryDTO.setMeRating(meRating);
            meRating = meRating.subtract(SUBSTRACT_VALUE);
        }
    }

    private Boolean isItineraryWithCarrierSteering(SearchItineraryDTO itineraryDTO) {
        return Optional.ofNullable(itineraryDTO.getSectionsCarrierIds())
                .orElse(Collections.emptySet()).stream().anyMatch(isActivatedAirlinePredicate);
    }

}
