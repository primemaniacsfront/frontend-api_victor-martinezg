package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface UserDescriptionMapper {

    @Mapping(source = "creditCards", target = "creditCards.creditCards")
    User userContractToModel(com.odigeo.userprofiles.api.v2.model.User user);
}
