package com.odigeo.frontend.resolvers.user.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCards;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserCreditCardsResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.user.mappers.CreditCardsMapper;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.User;
import graphql.schema.DataFetchingEnvironment;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class UserCreditCardsHandler {

    @Inject
    private UserDescriptionService userDescriptionService;
    @Inject
    private CreditCardsMapper creditCardsMapper;

    public UserCreditCardsResponse getCreditCards(DataFetchingEnvironment env) {
        List<CreditCards> creditCardsList = Optional.ofNullable(Optional.ofNullable((User) env.getGraphQlContext().get("User"))
                        .orElse(userDescriptionService.getUserByToken(env.getContext())))
                .map(User::getCreditCards)
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .map(creditCardsMapper::creditCardsContractToModel)
                .collect(Collectors.toList());
        UserCreditCardsResponse userCreditCardsResponse = new UserCreditCardsResponse();
        userCreditCardsResponse.setCreditCards(creditCardsList);
        return userCreditCardsResponse;
    }
}
