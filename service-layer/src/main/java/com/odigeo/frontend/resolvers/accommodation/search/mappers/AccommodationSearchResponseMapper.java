package com.odigeo.frontend.resolvers.accommodation.search.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchResponse;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import org.mapstruct.Mapper;

@Mapper
public interface AccommodationSearchResponseMapper {

    AccommodationSearchResponse mapModelToContract(AccommodationSearchResponseDTO response);
}
