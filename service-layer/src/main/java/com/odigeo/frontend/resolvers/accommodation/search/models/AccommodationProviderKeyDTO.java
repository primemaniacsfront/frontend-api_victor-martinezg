package com.odigeo.frontend.resolvers.accommodation.search.models;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class AccommodationProviderKeyDTO {
    private String providerId;
    private String accommodationProviderId;

    public AccommodationProviderKeyDTO(String providerId, String accommodationProviderId) {
        this.providerId = providerId;
        this.accommodationProviderId = accommodationProviderId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getAccommodationProviderId() {
        return accommodationProviderId;
    }

    public void setAccommodationProviderId(String accommodationProviderId) {
        this.accommodationProviderId = accommodationProviderId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccommodationProviderKeyDTO that = (AccommodationProviderKeyDTO) o;
        return new EqualsBuilder()
                .append(getProviderId(), that.getProviderId())
                .append(getAccommodationProviderId(), that.getAccommodationProviderId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(13, 29)
                .append(getProviderId())
                .append(getAccommodationProviderId())
                .toHashCode();
    }
}
