package com.odigeo.frontend.resolvers.accommodation.summary.models;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;

import java.util.List;

public class AccommodationDetailDTO {

    private AccommodationDealInformationDTO accommodationDealInformation;
    private String stateProvince;
    private String country;
    private String postalCode;
    private List<String> mapUrls;
    private String locationDescription;
    private String surroundingAreaInfo;
    private String checkInPolicy;
    private String checkOutPolicy;
    private String hotelPolicy;
    private List<String> creditCardTypes;
    private String paymentMethod;
    private String phoneNumber;
    private String mail;
    private String web;
    private String fax;
    private List<AccommodationImageDTO> accommodationImages;
    private List<FacilityGroupsDTO> facilityGroups;
    private List<AccommodationRoomServiceDTO> accommodationRoomServices;
    private String numberOfRooms;
    private AccommodationType hotelType;
    private Double distanceFromCityCenter;

    public AccommodationDealInformationDTO getAccommodationDealInformation() {
        return accommodationDealInformation;
    }

    public void setAccommodationDealInformation(AccommodationDealInformationDTO accommodationDealInformation) {
        this.accommodationDealInformation = accommodationDealInformation;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<String> getMapUrls() {
        return mapUrls;
    }

    public void setMapUrls(List<String> mapUrls) {
        this.mapUrls = mapUrls;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getSurroundingAreaInfo() {
        return surroundingAreaInfo;
    }

    public void setSurroundingAreaInfo(String surroundingAreaInfo) {
        this.surroundingAreaInfo = surroundingAreaInfo;
    }

    public String getCheckInPolicy() {
        return checkInPolicy;
    }

    public void setCheckInPolicy(String checkInPolicy) {
        this.checkInPolicy = checkInPolicy;
    }

    public String getCheckOutPolicy() {
        return checkOutPolicy;
    }

    public void setCheckOutPolicy(String checkOutPolicy) {
        this.checkOutPolicy = checkOutPolicy;
    }

    public String getHotelPolicy() {
        return hotelPolicy;
    }

    public void setHotelPolicy(String hotelPolicy) {
        this.hotelPolicy = hotelPolicy;
    }

    public List<String> getCreditCardTypes() {
        return creditCardTypes;
    }

    public void setCreditCardTypes(List<String> creditCardTypes) {
        this.creditCardTypes = creditCardTypes;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public List<AccommodationImageDTO> getAccommodationImages() {
        return accommodationImages;
    }

    public void setAccommodationImages(List<AccommodationImageDTO> accommodationImages) {
        this.accommodationImages = accommodationImages;
    }

    public List<FacilityGroupsDTO> getFacilityGroups() {
        return facilityGroups;
    }

    public void setFacilityGroups(List<FacilityGroupsDTO> facilityGroups) {
        this.facilityGroups = facilityGroups;
    }

    public List<AccommodationRoomServiceDTO> getAccommodationRoomServices() {
        return accommodationRoomServices;
    }

    public void setAccommodationRoomServices(List<AccommodationRoomServiceDTO> accommodationRoomServices) {
        this.accommodationRoomServices = accommodationRoomServices;
    }

    public String getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(String numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public AccommodationType getHotelType() {
        return hotelType;
    }

    public void setHotelType(AccommodationType hotelType) {
        this.hotelType = hotelType;
    }

    public Double getDistanceFromCityCenter() {
        return distanceFromCityCenter;
    }

    public void setDistanceFromCityCenter(Double distanceFromCityCenter) {
        this.distanceFromCityCenter = distanceFromCityCenter;
    }
}
