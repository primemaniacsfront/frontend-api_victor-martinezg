package com.odigeo.frontend.resolvers.accommodation.search.models;

import java.util.List;

public class ItinerarySelectionRequestDTO {

    private String fareItineraryKey;
    private List<String> segmentKeys;
    private String fareFamilyCode;

    public String getFareItineraryKey() {
        return fareItineraryKey;
    }

    public void setFareItineraryKey(String fareItineraryKey) {
        this.fareItineraryKey = fareItineraryKey;
    }

    public List<String> getSegmentKeys() {
        return segmentKeys;
    }

    public void setSegmentKeys(List<String> segmentKeys) {
        this.segmentKeys = segmentKeys;
    }

    public String getFareFamilyCode() {
        return fareFamilyCode;
    }

    public void setFareFamilyCode(String fareFamilyCode) {
        this.fareFamilyCode = fareFamilyCode;
    }
}
