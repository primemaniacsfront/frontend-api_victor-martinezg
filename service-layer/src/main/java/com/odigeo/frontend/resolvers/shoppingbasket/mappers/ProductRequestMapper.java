package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.google.inject.Singleton;
import com.odigeo.shoppingbasket.v2.model.Product;
import com.odigeo.shoppingbasket.v2.requests.ProductRequest;

@Singleton
public class ProductRequestMapper {

    public ProductRequest map(String productId, String productType, String status) {
        ProductRequest productRequest = new ProductRequest();

        Product product = new Product();
        product.setId(productId);
        product.setType(productType);
        product.setStatus(status);
        productRequest.setProduct(product);

        return productRequest;
    }

    public com.odigeo.shoppingbasket.v3.request.ProductRequest map(String productId, String productType) {
        com.odigeo.shoppingbasket.v3.request.ProductRequest productRequest = new com.odigeo.shoppingbasket.v3.request.ProductRequest();

        com.odigeo.shoppingbasket.v3.model.Product product = new com.odigeo.shoppingbasket.v3.model.Product();
        product.setId(productId);
        product.setType(productType);
        productRequest.setProduct(product);

        return productRequest;
    }
}
