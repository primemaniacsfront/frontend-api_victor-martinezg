package com.odigeo.frontend.resolvers.accommodation.search.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;
import com.odigeo.hcsapi.v9.beans.SendPackage;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class HotelSummaryRequestMapper {

    public SendPackage map(List<AccommodationProviderKeyDTO> accommodationProviderKeys) {
        List<HotelSupplierKey> hotelSupplierKeys = createProviderHotelKeyListToHotelSupplierKeyList(accommodationProviderKeys);
        return new SendPackage(hotelSupplierKeys);
    }


    private List<HotelSupplierKey> createProviderHotelKeyListToHotelSupplierKeyList(List<AccommodationProviderKeyDTO> accommodationProviderKeys) {
        return accommodationProviderKeys.stream().map(providerKey -> new HotelSupplierKey(providerKey.getAccommodationProviderId(), providerKey.getProviderId())).collect(Collectors.toList());
    }
}
