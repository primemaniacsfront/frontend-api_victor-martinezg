package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerception;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.PricePerceptionType;

import java.util.Arrays;
import java.util.Objects;

@Singleton
public class PricePerceptionMapper {

    private static final com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerceptionType DEFAULT_PRICE_PERCEPTION_TYPE = com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerceptionType.PERPLAN;

    public PricePerception map(PricePerceptionType priceType) {
        PricePerception pricePerception = new PricePerception();
        pricePerception.setType(DEFAULT_PRICE_PERCEPTION_TYPE);

        if (Objects.nonNull(priceType)) {
            pricePerception.setType(getPricePerceptionType(priceType));
        }

        return pricePerception;
    }

    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerceptionType getPricePerceptionType(PricePerceptionType priceType) {
        return Arrays.stream(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerceptionType.values())
            .filter(pricePerceptionType -> pricePerceptionType.name().equals(priceType.name()))
            .findFirst()
            .orElse(DEFAULT_PRICE_PERCEPTION_TYPE);
    }
}
