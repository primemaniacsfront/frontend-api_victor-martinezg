package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImageType;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationImageQualityDTO;
import com.odigeo.hcsapi.v9.beans.HotelImage;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationImage;
import org.apache.commons.lang3.EnumUtils;

@Singleton
public class AccommodationImagesMapper {

    public AccommodationImageDTO map(AccommodationImage accommodationImage) {
        AccommodationImageDTO dto = new AccommodationImageDTO();
        dto.setUrl(accommodationImage.getUrl());
        dto.setThumbnailUrl(accommodationImage.getThumbnailUrl());
        dto.setType(accommodationImage.getType() != null ? EnumUtils.getEnum(AccommodationImageType.class, accommodationImage.getType().value()) : null);
        dto.setQuality(accommodationImage.getQuality() != null ? EnumUtils.getEnum(AccommodationImageQualityDTO.class, accommodationImage.getQuality().value()) : null);
        return dto;
    }

    public AccommodationImageDTO mapHcsImage(HotelImage accommodationImage) {
        AccommodationImageDTO dto = new AccommodationImageDTO();
        dto.setUrl(accommodationImage.getUrl());
        dto.setThumbnailUrl(accommodationImage.getThumbnail());
        dto.setType(accommodationImage.getType() != null ? EnumUtils.getEnum(AccommodationImageType.class, accommodationImage.getType().name()) : null);
        dto.setQuality(accommodationImage.getQuality() != null ? EnumUtils.getEnum(AccommodationImageQualityDTO.class, accommodationImage.getQuality().name()) : null);
        return dto;
    }

}
