package com.odigeo.frontend.resolvers.accommodation.search.models;

import java.math.BigDecimal;

public class FeeDetailsDTO {
    private BigDecimal tax;
    private BigDecimal discount;

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
}
