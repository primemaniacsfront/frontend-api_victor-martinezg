package com.odigeo.frontend.resolvers.prime.handlers;

import com.google.inject.Singleton;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.request.search.Pagination;
import com.odigeo.membership.request.search.SortCriteria;
import com.odigeo.membership.request.search.Sorting;
import com.odigeo.membership.request.search.SortingField;
import graphql.VisibleForTesting;

@Singleton
public class MembershipSearchRequestHandler {

    @VisibleForTesting
    static final Pagination PAGINATION_ONE = Pagination.builder(1).build();
    @VisibleForTesting
    static final Sorting NEWEST_FIRST = Sorting.builder(SortingField.TIMESTAMP, SortCriteria.DESC).build();

    public MembershipSearchRequest build(String email, String websiteCode) {
        return new MembershipSearchRequest.Builder()
                .website(websiteCode)
                .email(email)
                .sorting(NEWEST_FIRST)
                .pagination(PAGINATION_ONE)
                .withMemberAccount(Boolean.TRUE)
                .build();
    }

    public MembershipSearchRequest build(Long userId, String websiteCode) {
        return new MembershipSearchRequest.Builder()
                .website(websiteCode)
                .memberAccountSearchRequest(getMemberAccountSearchRequest(userId))
                .sorting(NEWEST_FIRST)
                .pagination(PAGINATION_ONE)
                .withMemberAccount(Boolean.TRUE)
                .build();
    }

    private MemberAccountSearchRequest getMemberAccountSearchRequest(long userId) {
        return new MemberAccountSearchRequest.Builder().userId(userId).build();
    }
}
