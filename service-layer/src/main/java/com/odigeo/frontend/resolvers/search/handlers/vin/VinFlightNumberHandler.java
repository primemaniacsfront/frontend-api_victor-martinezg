package com.odigeo.frontend.resolvers.search.handlers.vin;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.itinerary.HubItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.ItineraryProvider;
import com.odigeo.searchengine.v2.responses.itinerary.Section;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;
import org.apache.commons.collections4.ListUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
class VinFlightNumberHandler {

    public Set<String> findOriginHubFlightNumbers(SearchEngineContext seContext) {
        return calculateFlightNumbers(seContext, HubItinerary::getOth);

    }

    public Set<String> findDestinationHubFlightNumbers(SearchEngineContext seContext) {
        return calculateFlightNumbers(seContext, HubItinerary::getHtd);
    }

    private Set<String> calculateFlightNumbers(SearchEngineContext seContext, Function<HubItinerary, Integer> calculator) {
        List<HubItinerary> hubs = seContext.getHubs();
        Set<String> flightNumbers = new HashSet<>();

        hubs.forEach(hub -> {
            List<Integer> sectionIds = findSimpleSections(calculator.apply(hub), seContext);
            if (sectionIds.isEmpty()) {
                sectionIds = findCrossFaringSections(calculator.apply(hub), seContext);
            }

            flightNumbers.addAll(findFlightNumbers(sectionIds, seContext));
        });

        return flightNumbers;
    }

    private List<Integer> findSimpleSections(Integer itineraryId, SearchEngineContext seContext) {
        return Optional.ofNullable(seContext.getSimplesMap().get(itineraryId))
            .map(ItineraryProvider::getSections)
            .orElse(Collections.emptyList());
    }

    private List<Integer> findCrossFaringSections(Integer itineraryId, SearchEngineContext seContext) {
        return Optional.ofNullable(seContext.getCrossFaringMap().get(itineraryId))
            .map(crossFaring -> ListUtils.union(
                findSimpleSections(crossFaring.getIn(), seContext),
                findSimpleSections(crossFaring.getOut(), seContext)))
            .orElse(Collections.emptyList());
    }

    private Set<String> findFlightNumbers(List<Integer> sectionIds, SearchEngineContext seContext) {
        Map<Integer, SectionResult> sectionResultMap = seContext.getSectionResultMap();
        return sectionIds.stream()
            .map(sectionResultMap::get)
            .map(SectionResult::getSection)
            .map(Section::getId)
            .collect(Collectors.toSet());
    }

}
