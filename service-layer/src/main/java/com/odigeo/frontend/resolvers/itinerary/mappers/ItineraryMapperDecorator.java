package com.odigeo.frontend.resolvers.itinerary.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FreeCancellationLimit;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Leg;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType;
import com.google.inject.Inject;
import com.odigeo.frontend.resolvers.itinerary.configuration.FreeRebookingConfiguration;
import com.odigeo.frontend.resolvers.search.handlers.campaign.CampaignConfigHandler;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.apache.commons.lang3.StringUtils;

import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class ItineraryMapperDecorator implements ItineraryMapper {

    private static final int MINUTE_ROUND_UPPER = 55;
    private static final int MINUTES_PER_HOUR = 60;

    private final ItineraryMapper delegate;

    @Inject
    private FreeRebookingConfiguration freeRebookingConfiguration;
    @Inject
    private CampaignConfigHandler campaignConfigHandler;

    public ItineraryMapperDecorator(ItineraryMapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public Itinerary itineraryContractToModel(com.odigeo.dapi.client.Itinerary itinerary, ShoppingCartContext context) {
        Itinerary model = delegate.itineraryContractToModel(itinerary, context);
        Optional.ofNullable(model)
            .ifPresent(itineraryMapped -> {
                populateFreeCancellationLimit(itineraryMapped, itinerary);
                mapTripType(itineraryMapped);
                mapHasFreeRebooking(itineraryMapped, context);
                mapCampaignConfig(itineraryMapped, context);
            });

        return model;
    }

    private void mapTripType(Itinerary itineraryMapped) {
        if (itineraryMapped != null && itineraryMapped.getLegs() != null) {
            itineraryMapped.setTripType(calculateTripType(itineraryMapped));
        }
    }

    private void mapHasFreeRebooking(Itinerary itineraryMapped, ShoppingCartContext context) {
        if (context != null && StringUtils.isNotEmpty(context.getWebSiteCode())) {
            itineraryMapped.setHasFreeRebooking(hasFreeRebooking(itineraryMapped, context.getWebSiteCode()));
        }
    }

    private void mapCampaignConfig(Itinerary itineraryMapped, ShoppingCartContext context) {
        if (context != null) {
            itineraryMapped.setCampaignConfig(campaignConfigHandler.getCampaignConfig(itineraryMapped, context.getVisit()));
        }
    }

    private void populateFreeCancellationLimit(Itinerary model, com.odigeo.dapi.client.Itinerary itinerary) {
        if (model != null && itinerary != null && itinerary.getFreeCancellation() != null
            && itinerary.getFreeCancellation().getLimitTime() != null) {
            model.setFreeCancellationLimit(buildFreeCancellation(itinerary));
        }
    }

    private long calculateHoursApart(Instant freeCancellationLimitTime) {
        Duration duration = Duration.between(Calendar.getInstance().toInstant(), freeCancellationLimitTime);
        long diffHours = duration.toHours();
        return duration.toMinutes() % MINUTES_PER_HOUR < MINUTE_ROUND_UPPER ? diffHours : diffHours + 1;
    }

    private TripType calculateTripType(Itinerary itinerary) {
        if (itinerary.getLegs().size() == 1) {
            return TripType.ONE_WAY;
        } else if (itinerary.getLegs().size() == 2 && sameOriginsAndDestinations(itinerary)) {
            return TripType.ROUND_TRIP;
        } else {
            return TripType.MULTIPLE_DESTINATIONS;
        }
    }

    private boolean sameOriginsAndDestinations(Itinerary itinerary) {
        Leg leg1 = itinerary.getLegs().get(0);
        Leg leg2 = itinerary.getLegs().get(1);
        return calculateOrigin(leg1).getCityIata().equals(calculateDestination(leg2).getCityIata())
            && calculateDestination(leg1).getCityIata().equals(calculateOrigin(leg2).getCityIata());
    }

    private Location calculateOrigin(Leg leg) {
        return leg.getSegments().get(0).getSections().get(0).getDeparture();
    }

    private Location calculateDestination(Leg leg) {
        Segment lastSegment = leg.getSegments().get(leg.getSegments().size() - 1);
        return lastSegment.getSections().get(lastSegment.getSections().size() - 1).getDestination();
    }

    private FreeCancellationLimit buildFreeCancellation(com.odigeo.dapi.client.Itinerary itinerary) {
        FreeCancellationLimit freeCancellationLimit = new FreeCancellationLimit();
        freeCancellationLimit.setLimitTime(itinerary.getFreeCancellation().getLimitTime().getTimeInMillis());
        freeCancellationLimit.setHoursApart(calculateHoursApart(itinerary.getFreeCancellation().getLimitTime().toInstant()));
        return freeCancellationLimit;
    }

    private boolean hasFreeRebooking(Itinerary model, String webSiteCode) {
        return isSiteWithFreeRebooking(webSiteCode) && areDatesIncluded(model) && allCarriersHasFreeRebooking(model);
    }

    private boolean isSiteWithFreeRebooking(String webSiteCode) {
        return Optional.ofNullable(freeRebookingConfiguration)
            .map(FreeRebookingConfiguration::getSites)
            .map(sites -> sites.contains(webSiteCode))
            .orElse(false);
    }

    private boolean allCarriersHasFreeRebooking(Itinerary model) {
        Set<String> itineraryCarriers = getSegmentItineraryCarriers(model);
        return Optional.ofNullable(freeRebookingConfiguration)
            .map(FreeRebookingConfiguration::getCarriers)
            .map(carriers -> carriers.containsAll(itineraryCarriers))
            .orElse(false);
    }

    private Set<String> getSegmentItineraryCarriers(Itinerary itinerary) {
        return Optional.ofNullable(itinerary).map(Itinerary::getLegs)
            .orElse(Collections.emptyList()).stream()
            .map(Leg::getSegments).flatMap(List::stream)
            .map(Segment::getCarrier).map(Carrier::getId)
            .collect(Collectors.toSet());
    }

    private boolean areDatesIncluded(Itinerary itinerary) {
        return Optional.ofNullable(itinerary).map(Itinerary::getLegs).orElse(Collections.emptyList()).stream()
            .map(Leg::getSegments).flatMap(List::stream).map(Segment::getSections).flatMap(List::stream)
            .map(Section::getDepartureDate).map(ZonedDateTime::toLocalDate)
            .noneMatch(departureDate -> departureDate.isAfter(freeRebookingConfiguration.getMaxDateFormatted()));
    }
}
