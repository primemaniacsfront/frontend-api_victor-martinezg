package com.odigeo.frontend.resolvers.accommodation.rooms;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRoomAvailabilityRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRoomAvailabilityResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.accommodation.rooms.handlers.AccommodationRoomsResponseHandler;
import com.odigeo.frontend.resolvers.accommodation.rooms.handlers.RoomsHandler;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.SearchRoomAvailabilityResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.StaticRoomDetailsMapper;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.AccommodationRoomsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDealDTO;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

@Singleton
public class SearchRoomAvailabilityResolver implements Resolver {

    private static final String ROOM_AVAILABILITY_QUERY = "searchRoomAvailability";
    private static final String ROOMS_ROOM_DETAILS = "rooms/details";

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private SearchRoomAvailabilityResponseMapper accommodationRoomsResponseMapper;
    @Inject
    private RoomsHandler roomsHandler;
    @Inject
    private AccommodationRoomsResponseHandler accommodationRoomsResponseHandler;
    @Inject
    private StaticRoomDetailsMapper staticRoomDetailsMapper;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher(ROOM_AVAILABILITY_QUERY, this::accommodationRoomsFetcher));
    }


    public SearchRoomAvailabilityResponse accommodationRoomsFetcher(DataFetchingEnvironment env) {
        SearchRoomAvailabilityRequest requestParams = jsonUtils.fromDataFetching(env, SearchRoomAvailabilityRequest.class);
        ResolverContext context = env.getContext();
        VisitInformation visit = context.getVisitInformation();
        AccommodationRoomsResponseDTO accommodationRoomsResponseDTO = searchRoomAvailability(requestParams, visit);
        if (checkIfShouldPopulateStaticContent(env, accommodationRoomsResponseDTO)) {
            populateRoomStaticContent(visit, accommodationRoomsResponseDTO);
        }
        return accommodationRoomsResponseMapper.mapModelToContract(accommodationRoomsResponseDTO);
    }

    private boolean checkIfShouldPopulateStaticContent(DataFetchingEnvironment env, AccommodationRoomsResponseDTO accommodationRoomsResponseDTO) {
        return isAskingForRoomDetails(env) && CollectionUtils.isNotEmpty(accommodationRoomsResponseDTO.getRooms());
    }

    private AccommodationRoomsResponseDTO searchRoomAvailability(SearchRoomAvailabilityRequest requestParams, VisitInformation visit) {
        SearchRoomResponse roomAvailability = roomsHandler.getRoomAvailability(requestParams.getSearchId(), requestParams.getAccommodationDealKey(), visit);
        return accommodationRoomsResponseHandler.buildAccommodationRoomResponse(roomAvailability, visit);
    }

    private boolean isAskingForRoomDetails(DataFetchingEnvironment env) {
        return env.getSelectionSet().contains(ROOMS_ROOM_DETAILS);
    }

    private void populateRoomStaticContent(VisitInformation visit, AccommodationRoomsResponseDTO accommodationRoomsResponseDTO) {
        List<RoomDealDTO> source = accommodationRoomsResponseDTO.getRooms();
        String[] roomIds = source.stream().map(RoomDealDTO::getProviderRoomId).toArray(String[]::new);
        String providerId = source.get(0).getProviderId();
        String providerHotelId = source.get(0).getProviderHotelId();
        staticRoomDetailsMapper.remapRoomDetails(source, roomsHandler.getStaticRoomDetails(providerId, providerHotelId, visit, roomIds));
    }


}
