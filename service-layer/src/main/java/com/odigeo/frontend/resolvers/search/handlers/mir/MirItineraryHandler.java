package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.contract.location.LocationDTO;
import com.odigeo.frontend.resolvers.search.mappers.BaggageConditionMapper;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.services.CurexService;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.responses.Continent;
import com.odigeo.geoapi.v4.responses.ItineraryLocation;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.Coordinates;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.GeographicalInformation;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.Itinerary;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.Segment;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.SegmentResult;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
class MirItineraryHandler {

    private static final String CURRENCY_EUR = "EUR";
    private static final int ITINERARIES_TO_RATE = 150;

    @Inject
    private CurexService curexService;
    @Inject
    private BaggageConditionMapper baggageMapper;
    @Inject
    private GeoService geoService;
    @Inject
    private Logger logger;

    public List<Itinerary> buildRatingItineraries(SearchResponseDTO searchResponseDTO) {
        String currency = searchResponseDTO.getCurrencyCode();
        BigDecimal conversionRate = curexService.getCurrencyConvertRate(currency, CURRENCY_EUR);

        int index = 0;
        List<SearchItineraryDTO> itineraryDTOList = searchResponseDTO.getItineraries();
        List<Itinerary> ratingItineraries = new ArrayList<>(ITINERARIES_TO_RATE);
        Map<Integer, ItineraryLocation> geoInfoMap = new HashMap<>();

        while (index < ITINERARIES_TO_RATE && index < itineraryDTOList.size()) {
            SearchItineraryDTO itineraryDTO = itineraryDTOList.get(index);
            int currentIndex = itineraryDTO.getIndex();

            do {
                ratingItineraries.add(buildItinerary(itineraryDTO, conversionRate, geoInfoMap));
                index++;
            } while (index < itineraryDTOList.size()
                && (itineraryDTO = itineraryDTOList.get(index)).getIndex() == currentIndex);
        }

        return ratingItineraries;
    }

    private Itinerary buildItinerary(SearchItineraryDTO itineraryDTO, BigDecimal conversionRate,
                                     Map<Integer, ItineraryLocation> geoInfoMap) {

        Itinerary ratingItinerary = new Itinerary();
        ratingItinerary.setItineraryIndex(itineraryDTO.getIndex());
        ratingItinerary.setFreeCancellation(itineraryDTO.getFreeCancellation() != null);
        ratingItinerary.setSegmentResults(buildLegs(itineraryDTO.getLegs(), geoInfoMap));

        Optional.ofNullable(conversionRate).ifPresent(rate ->
            ratingItinerary.setApparentPriceInEur(itineraryDTO.getSortPrice().multiply(rate)));

        return ratingItinerary;
    }

    private List<SegmentResult> buildLegs(List<LegDTO> legDTOList, Map<Integer, ItineraryLocation> geoInfoMap) {
        return legDTOList.stream()
            .map(legDTO -> {
                SegmentResult ratingLeg = new SegmentResult();
                ratingLeg.setSegments(buildSegments(legDTO.getSegments(), geoInfoMap));

                return ratingLeg;

            }).collect(Collectors.toList());
    }

    private List<Segment> buildSegments(List<SegmentDTO> segmentDTOList, Map<Integer, ItineraryLocation> geoInfoMap) {
        return segmentDTOList.stream()
            .map(segmentDTO -> {
                Segment ratingSegment = new Segment();
                List<SectionDTO> sectionDTOList = segmentDTO.getSections();

                ratingSegment.setSegmentIndex(segmentDTO.getId());
                ratingSegment.setFlightDuration(segmentDTO.getDuration());
                ratingSegment.setSeatsAvailable(Optional.ofNullable(segmentDTO.getSeats()).orElse(-1));
                ratingSegment.setBaggageIncluded(baggageMapper.map(segmentDTO.getBaggageCondition()));
                ratingSegment.setMarketingCarrierCode(segmentDTO.getCarrier().getId());
                ratingSegment.setDepartureDateUTC(sectionDTOList.get(0).getDepartureDate());
                ratingSegment.setArrivalDateUTC(sectionDTOList.get(sectionDTOList.size() - 1).getArrivalDate());

                populateSegmentStops(ratingSegment, segmentDTO);
                populateSegmentGeoInfo(ratingSegment, segmentDTO, geoInfoMap);

                return ratingSegment;

            }).collect(Collectors.toList());
    }

    private void populateSegmentStops(Segment ratingSegment, SegmentDTO segmentDTO) {
        List<SectionDTO> sectionDTOList = segmentDTO.getSections();
        long stopOverDuration = 0;

        for (int i = 0; i < sectionDTOList.size() - 1; i++) {
            ZonedDateTime initStopOver = sectionDTOList.get(i).getArrivalDate();
            ZonedDateTime endStopOpver = sectionDTOList.get(i + 1).getDepartureDate();

            stopOverDuration += ChronoUnit.MINUTES.between(initStopOver, endStopOpver);
        }

        ratingSegment.setNumStopovers(sectionDTOList.size() - 1);
        ratingSegment.setDurStopovers(stopOverDuration);
    }

    private void populateSegmentGeoInfo(Segment ratingSegment, SegmentDTO segmentDTO,
                                        Map<Integer, ItineraryLocation> geoInfoMap) {

        GeographicalInformation geoInfo = new GeographicalInformation();
        List<SectionDTO> sectionDTOList = segmentDTO.getSections();

        LocationDTO departureDTO = sectionDTOList.get(0).getDeparture();
        geoInfo.setDepartureAirportIata(filterTrain(departureDTO));
        geoInfo.setDepartureCountryCode(departureDTO.getCountryCode());
        geoInfo.setDepartureCityIata(departureDTO.getCityIata());

        LocationDTO destinationDTO = sectionDTOList.get(sectionDTOList.size() - 1).getDestination();
        geoInfo.setArrivalAirportIata(filterTrain(destinationDTO));
        geoInfo.setArrivalCountryCode(destinationDTO.getCountryCode());
        geoInfo.setArrivalCityIata(destinationDTO.getCityIata());

        ItineraryLocation departureItineraryLoc = getGeoInfo(departureDTO, geoInfoMap);
        ItineraryLocation destinationItineraryLoc = getGeoInfo(destinationDTO, geoInfoMap);

        if (departureItineraryLoc != null && destinationItineraryLoc != null) {
            Coordinates departureCoordinates = new Coordinates();
            Coordinates arrivalCoordinates =  new Coordinates();

            departureCoordinates.setLatitude(departureItineraryLoc.getCoordinates().getLatitude());
            departureCoordinates.setLongitude(departureItineraryLoc.getCoordinates().getLongitude());
            arrivalCoordinates.setLatitude(destinationItineraryLoc.getCoordinates().getLatitude());
            arrivalCoordinates.setLongitude(destinationItineraryLoc.getCoordinates().getLongitude());

            geoInfo.setDepartureCoordinates(departureCoordinates);
            geoInfo.setArrivalCoordinates(arrivalCoordinates);

            Continent departureContinent = departureItineraryLoc.getCity().getCountry().getContinent();
            Continent destinationContinent = destinationItineraryLoc.getCity().getCountry().getContinent();

            geoInfo.setDepartureContinentId(Integer.toString(departureContinent.getContinentId()));
            geoInfo.setArrivalContinentId(Integer.toString(destinationContinent.getContinentId()));
        }

        ratingSegment.setGeoInformation(geoInfo);
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    private ItineraryLocation getGeoInfo(LocationDTO location, Map<Integer, ItineraryLocation> geoInfoMap) {
        Integer locationId = location.getId();
        ItineraryLocation itineraryLocation = geoInfoMap.get(locationId);

        if (itineraryLocation == null) {
            try {
                itineraryLocation = geoService.getItineraryLocation(locationId);
                geoInfoMap.put(locationId, itineraryLocation);
            } catch (RuntimeException ex) {
                logger.warning(this.getClass(), "Error obtaining the geographical information", ex);
            }
        }

        return itineraryLocation;
    }

    private String filterTrain(LocationDTO location) {
        return location.getLocationType() == LocationType.TRAIN_STATION
            ? location.getCityIata()
            : location.getIata();
    }

}
