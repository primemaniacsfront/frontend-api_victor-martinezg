package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddContactDetailsToShoppingBasketRequest;
import com.google.inject.Singleton;
import com.odigeo.shoppingbasket.v3.model.ContactDetails;
import com.odigeo.shoppingbasket.v3.request.ContactDetailsRequest;

@Singleton
public class ContactDetailsRequestMapper {
    public ContactDetailsRequest map(AddContactDetailsToShoppingBasketRequest addContactDetailsToShoppingBasketRequest) {
        ContactDetailsRequest request = new ContactDetailsRequest();
        ContactDetails contactDetails = new ContactDetails();
        contactDetails.setName(addContactDetailsToShoppingBasketRequest.getName());
        contactDetails.setLastName(addContactDetailsToShoppingBasketRequest.getLastName());
        contactDetails.setEmail(addContactDetailsToShoppingBasketRequest.getEmail());
        contactDetails.setAddress(addContactDetailsToShoppingBasketRequest.getAddress());
        contactDetails.setCityName(addContactDetailsToShoppingBasketRequest.getCityName());
        contactDetails.setCountry(addContactDetailsToShoppingBasketRequest.getCountry());
        contactDetails.setPostcode(addContactDetailsToShoppingBasketRequest.getPostcode());
        contactDetails.setPhoneNumber(addContactDetailsToShoppingBasketRequest.getPhoneNumber());
        contactDetails.setCountryPhoneNumber(addContactDetailsToShoppingBasketRequest.getCountryPhoneNumber());
        contactDetails.setTypePhoneNumber(addContactDetailsToShoppingBasketRequest.getTypePhoneNumber());
        contactDetails.setCountryCode(addContactDetailsToShoppingBasketRequest.getCountryCode());

        request.setContactDetails(contactDetails);
        return request;
    }
}
