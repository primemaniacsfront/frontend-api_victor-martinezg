package com.odigeo.frontend.resolvers.membershipoffer;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.prime.handlers.SubscriptionRequestHandler;
import com.odigeo.frontend.resolvers.prime.handlers.SubscriptionResponseHandler;
import com.odigeo.membership.offer.api.request.MembershipOfferRequest;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffers;
import graphql.VisibleForTesting;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.List;

@Singleton
public class MembershipOfferResolver implements Resolver {
    @VisibleForTesting
    static final String QUERY = "Query";
    @VisibleForTesting
    static final String SUBSCRIPTION_OFFER = "subscriptionOffer";
    @VisibleForTesting
    static final String SUBSCRIPTION_OFFERS = "subscriptionOffers";
    @VisibleForTesting
    static final String SUBSCRIPTION_OFFER_BY_ID = "subscriptionOfferById";
    @Inject
    private SubscriptionResponseHandler subscriptionResponseHandler;
    @Inject
    private MembershipOfferHandler membershipOfferHandler;
    @Inject
    private SubscriptionRequestHandler subscriptionRequestHandler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder.type(QUERY, typeWiring -> typeWiring
                .dataFetcher(SUBSCRIPTION_OFFER, this::subscriptionOfferFetcher)
                .dataFetcher(SUBSCRIPTION_OFFERS, this::subscriptionOffersFetcher)
                .dataFetcher(SUBSCRIPTION_OFFER_BY_ID, this::subscriptionOfferByIdFetcher)
        );
    }

    @VisibleForTesting
    SubscriptionResponse subscriptionOfferByIdFetcher(DataFetchingEnvironment env) {
        String offerId = jsonUtils.fromDataFetching(env);
        MembershipSubscriptionOffer subscriptionOffer = membershipOfferHandler.getSubscriptionOffer(offerId);
        return subscriptionResponseHandler.buildSubscriptionResponse(subscriptionOffer);
    }

    @VisibleForTesting
    SubscriptionResponse subscriptionOfferFetcher(DataFetchingEnvironment env) {
        VisitInformation visit = env.getGraphQlContext().get(VisitInformation.class);

        MembershipOfferRequest offerRequest = subscriptionRequestHandler.buildSubscriptionRequest(visit);
        MembershipSubscriptionOffer subscriptionOffer = membershipOfferHandler.getSubscriptionOffer(offerRequest);
        return subscriptionResponseHandler.buildSubscriptionResponse(subscriptionOffer);
    }

    @VisibleForTesting
    List<SubscriptionResponse> subscriptionOffersFetcher(DataFetchingEnvironment env) {
        VisitInformation visit = env.getGraphQlContext().get(VisitInformation.class);

        MembershipOfferRequest offerRequest = subscriptionRequestHandler.buildSubscriptionRequest(visit);
        MembershipSubscriptionOffers subscriptionOffers = membershipOfferHandler.getSubscriptionOffers(offerRequest);
        return subscriptionResponseHandler.buildSubscriptionOffersResponse(subscriptionOffers);
    }
}
