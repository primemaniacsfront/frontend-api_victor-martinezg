package com.odigeo.frontend.resolvers.accommodation.search.models;

import java.util.Objects;

public class AccommodationFilterKeyDTO {
    private final String filterType;
    private final String filterName;

    public AccommodationFilterKeyDTO(String filterType, String filterName) {
        this.filterType = filterType;
        this.filterName = filterName;
    }

    public String getFilterType() {
        return filterType;
    }

    public String getFilterName() {
        return filterName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccommodationFilterKeyDTO that = (AccommodationFilterKeyDTO) o;
        return filterType.equals(that.filterType) && filterName.equals(that.filterName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(filterType, filterName);
    }
}
