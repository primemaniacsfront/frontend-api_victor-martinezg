package com.odigeo.frontend.resolvers.accommodation.search.models;

public class AccommodationRoomSelectionRequestDTO {

    private String accommodationDealKey;
    private String roomGroupDealKey;

    public String getAccommodationDealKey() {
        return accommodationDealKey;
    }

    public void setAccommodationDealKey(String accommodationDealKey) {
        this.accommodationDealKey = accommodationDealKey;
    }

    public String getRoomGroupDealKey() {
        return roomGroupDealKey;
    }

    public void setRoomGroupDealKey(String roomGroupDealKey) {
        this.roomGroupDealKey = roomGroupDealKey;
    }
}
