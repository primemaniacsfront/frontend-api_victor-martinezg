package com.odigeo.frontend.resolvers.search.handlers.vin;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.contract.ancillaries.InsuranceOfferDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.searchengine.v2.responses.itinerary.CrossFaringItinerary;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class VinHandler {

    @Inject
    private VinFlightNumberHandler flightNumberHandler;
    @Inject
    private VinInsuranceCreator insuranceCreator;

    public void populateVinInfo(List<SearchItineraryDTO> itineraryList, SearchEngineContext seContext) {
        VinInsuranceIdCreator idCreator = new VinInsuranceIdCreator();

        Set<String> originFlights = flightNumberHandler.findOriginHubFlightNumbers(seContext);
        Set<String> destinationFlights = flightNumberHandler.findDestinationHubFlightNumbers(seContext);

        itineraryList.stream()
            .filter(itinerary -> isHubItinerary(itinerary, seContext))
            .forEach(itinerary -> {
                itinerary.setVin(Boolean.TRUE);
                List<SegmentDTO> segments = itinerary.getLegs().stream()
                    .flatMap(leg -> leg.getSegments().stream())
                    .collect(Collectors.toList());

                if (isCrossFaringItinerary(itinerary, seContext)) {
                    CrossFaringItinerary crossFaringItinerary = seContext.getCrossFaringMap().get(itinerary.getItinerariesLink());
                    if (isHubCrossfaringLegItinerary(crossFaringItinerary.getOut(), seContext)) {
                        populateSegmentVinInfo(seContext, idCreator, originFlights, destinationFlights, crossFaringItinerary.getOut(),
                                null, getLegSections(segments, 0));
                    }
                    if (isHubCrossfaringLegItinerary(crossFaringItinerary.getIn(), seContext)) {
                        populateSegmentVinInfo(seContext, idCreator, originFlights, destinationFlights, crossFaringItinerary.getIn(),
                                null, getLegSections(segments, 1));
                    }
                } else {
                    InsuranceOfferDTO insuranceOffer = null;

                    for (SegmentDTO segment : segments) {
                        List<SectionDTO> sections = segment.getSections();
                        insuranceOffer = populateSegmentVinInfo(seContext, idCreator, originFlights, destinationFlights, itinerary.getItinerariesLink(), insuranceOffer, sections);
                    }
                }
            });
    }

    private List<SectionDTO> getLegSections(List<SegmentDTO> segments, int index) {
        return Optional.ofNullable(segments.get(index))
                .map(SegmentDTO::getSections)
                .orElse(Collections.emptyList());
    }

    private InsuranceOfferDTO populateSegmentVinInfo(SearchEngineContext seContext, VinInsuranceIdCreator idCreator, Set<String> originFlights, Set<String> destinationFlights,
                                                               Integer itinerariesLink, InsuranceOfferDTO insuranceOffer, List<SectionDTO> sections) {
        InsuranceOfferDTO insuranceOfferForSegment = insuranceOffer;

        for (int i = 1; i < sections.size(); i++) {
            SectionDTO currentSection = sections.get(i);
            SectionDTO previousSection = sections.get(i - 1);
            insuranceOfferForSegment = populateSectionVinInfo(seContext, idCreator, originFlights, destinationFlights, itinerariesLink,
                    insuranceOfferForSegment, currentSection, previousSection);
        }
        return insuranceOfferForSegment;
    }

    private InsuranceOfferDTO populateSectionVinInfo(SearchEngineContext seContext, VinInsuranceIdCreator idCreator, Set<String> originFlights, Set<String> destinationFlights,
                                                     Integer itinerariesLink, InsuranceOfferDTO insuranceOffer, SectionDTO currentSection, SectionDTO previousSection) {
        InsuranceOfferDTO insuranceOfferForSection = insuranceOffer;

        String flight = currentSection.getKey();
        String previousFlight = previousSection.getKey();

        if (areHubSections(originFlights, destinationFlights, previousFlight, flight)) {
            insuranceOfferForSection = insuranceOfferForSection != null
                    ? insuranceOfferForSection
                    : insuranceCreator.createInsuranceOffer(itinerariesLink, seContext, idCreator);
            previousSection.setInsuranceOffer(insuranceOfferForSection);
        }
        return insuranceOfferForSection;
    }

    private boolean isHubCrossfaringLegItinerary(Integer itineraryId, SearchEngineContext seContext) {
        return seContext.getHubMap().containsKey(itineraryId);
    }

    private boolean isHubItinerary(SearchItineraryDTO itinerary, SearchEngineContext seContext) {
        if (isCrossFaringItinerary(itinerary, seContext)) {
            CrossFaringItinerary crossFaringItinerary = seContext.getCrossFaringMap().get(itinerary.getItinerariesLink());
            return seContext.getHubMap().get(crossFaringItinerary.getOut()) != null || seContext.getHubMap().get(crossFaringItinerary.getIn()) != null;
        } else {
            return seContext.getHubMap().get(itinerary.getItinerariesLink()) != null;
        }
    }

    private boolean isCrossFaringItinerary(SearchItineraryDTO itinerary, SearchEngineContext seContext) {
        return seContext.getCrossFaringMap().containsKey(itinerary.getItinerariesLink());
    }

    private boolean areHubSections(Set<String> originFlights, Set<String> destinationFlights,
                                   String previousFlightNumber, String flightNumber) {

        return containsHubSections(originFlights, destinationFlights, previousFlightNumber, flightNumber)
            || containsHubSections(destinationFlights, originFlights, previousFlightNumber, flightNumber);
    }

    private boolean containsHubSections(Set<String> originFlights, Set<String> destinationFlights,
                                        String previousFlight, String flight) {

        return originFlights.contains(previousFlight) && destinationFlights.contains(flight);
    }

}
