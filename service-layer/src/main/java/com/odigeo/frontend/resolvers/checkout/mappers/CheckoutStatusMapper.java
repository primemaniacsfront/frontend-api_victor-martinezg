package com.odigeo.frontend.resolvers.checkout.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutStatus;
import com.odigeo.dapi.client.BookingResponseStatus;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ValueMapping;
import org.mapstruct.ValueMappings;

@Mapper
@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public interface CheckoutStatusMapper {

    @ValueMappings({
        @ValueMapping(source = "BOOKING_ERROR", target = "FAILED"),
        @ValueMapping(source = "BOOKING_PAYMENT_RETRY", target = "FAILED"),
        @ValueMapping(source = "BOOKING_CONFIRMED", target = "SUCCESS"),
        @ValueMapping(source = "BOOKING_REPRICING", target = "REPRICING"),
        @ValueMapping(source = "BROKEN_FLOW", target = "STOP"),
        @ValueMapping(source = "ON_HOLD", target = "STOP"),
        @ValueMapping(source = "USER_INTERACTION_NEEDED", target = "USER_PAYMENT_INTERACTION"),
        @ValueMapping(source = "BOOKING_STOP", target = "STOP"),
        @ValueMapping(source = MappingConstants.ANY_REMAINING, target = "FAILED"),
        @ValueMapping(source = MappingConstants.NULL, target = "FAILED")
        })
    CheckoutStatus mapShoppingCartStatusToContract(BookingResponseStatus status);

}
