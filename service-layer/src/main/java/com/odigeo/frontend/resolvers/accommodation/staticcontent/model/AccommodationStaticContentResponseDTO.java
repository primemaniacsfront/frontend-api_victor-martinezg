package com.odigeo.frontend.resolvers.accommodation.staticcontent.model;

import java.util.List;

public class AccommodationStaticContentResponseDTO {

    private List<AccommodationStaticContentDTO> accommodationsStaticContent;

    public List<AccommodationStaticContentDTO> getAccommodationsStaticContent() {
        return accommodationsStaticContent;
    }

    public void setAccommodationsStaticContent(List<AccommodationStaticContentDTO> accommodationsStaticContent) {
        this.accommodationsStaticContent = accommodationsStaticContent;
    }
}
