package com.odigeo.frontend.resolvers.insurance.handlers;

import com.edreamsodigeo.insuranceproduct.api.last.request.SelectRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectInsuranceOfferRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.insurance.mappers.InsuranceProductMapper;
import com.odigeo.frontend.resolvers.insurance.mappers.SelectRequestMapper;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ProductIdMapper;
import com.odigeo.frontend.services.insuranceproduct.InsuranceProductService;

@Singleton
public class InsuranceProductHandler {

    @Inject
    private InsuranceProductService insuranceProductService;
    @Inject
    private SelectRequestMapper selectRequestMapper;
    @Inject
    private InsuranceProductMapper insuranceProductMapper;
    @Inject
    private ProductIdMapper productIdMapper;

    public ProductId selectInsuranceOfferRequest(SelectInsuranceOfferRequest selectInsuranceOfferRequest) {
        SelectRequest selectRequest = selectRequestMapper.map(selectInsuranceOfferRequest);
        com.edreamsodigeo.insuranceproduct.api.last.response.ProductId productId = insuranceProductService.selectOffer(selectRequest);
        return productIdMapper.map(productId, ProductType.INSURANCE);
    }

    public InsuranceProduct getInsuranceProduct(String productId) {
        com.edreamsodigeo.insuranceproduct.api.last.response.InsuranceProduct insuranceProduct = insuranceProductService.getInsuranceProduct(productId);
        return insuranceProductMapper.map(insuranceProduct);
    }

    public void completeInsuranceProduct(InsuranceProduct insuranceProduct) {
        insuranceProductMapper.completeInsuranceProduct(insuranceProduct, getInsuranceProduct(insuranceProduct.getProductId()));
    }

}
