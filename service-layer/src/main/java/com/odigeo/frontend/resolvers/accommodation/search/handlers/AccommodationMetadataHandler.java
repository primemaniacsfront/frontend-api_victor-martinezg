package com.odigeo.frontend.resolvers.accommodation.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationFilterMetadata;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationPriceMetadata;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchMetadata;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ThreeValuedLogic;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.BigDecimalSummaryStatistics;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationFacilityGroupsEnum;
import com.odigeo.frontend.resolvers.accommodation.commons.model.DiscountDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;
import com.odigeo.frontend.resolvers.accommodation.search.configuration.AccommodationHistogramRangeByCurrencyConfiguration;
import com.odigeo.frontend.resolvers.accommodation.search.configuration.AccommodationPriceRangeByCurrencyConfiguration;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationFilterKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.FilterType;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Singleton
public class AccommodationMetadataHandler {

    private static final String CANCELLATION_FREE = "CANCELLATION_FREE";
    private static final String PAY_AT_DESTINATION = "PAY_AT_DESTINATION";
    private static final String LEQ_ONE_DISTANCE = "1";
    private static final String LEQ_THRE_DISTANCE = "3";
    private static final String LEQ_FIVE_DISTANCE = "5";
    private static final String GEQ_FIVE_RATING = "5";
    private static final String GEQ_FOUR_RATING = "4";
    private static final String GEQ_THREE_RATING = "3";
    private static final BigDecimal DEFAULT_PERNIGHT_RANGE = BigDecimal.valueOf(25);
    private static final BigDecimal DEFAULT_HISTOGRAM_RANGE = BigDecimal.valueOf(10);

    @Inject
    private AccommodationPriceRangeByCurrencyConfiguration accommodationPriceRangeByCurrencyConfiguration;
    @Inject
    private AccommodationHistogramRangeByCurrencyConfiguration accommodationHistogramRangeByCurrencyConfiguration;


    public void populateMetadata(AccommodationSearchResponseDTO response, Integer numAdults, VisitInformation visitInformation) {
        AccommodationSearchMetadata metadata = new AccommodationSearchMetadata();
        metadata.setPriceMetadata(buildPriceMetadata(response.getAccommodationResponse().getAccommodations(), numAdults, visitInformation));
        metadata.setFiltersMetadata(buildFilterMetadata(response.getAccommodationResponse().getAccommodations()));
        response.getAccommodationResponse().setMetadata(metadata);
    }

    private AccommodationPriceMetadata buildPriceMetadata(List<SearchAccommodationDTO> accommodations, Integer numAdults, VisitInformation visitInformation) {
        AccommodationPriceMetadata priceMetadata = new AccommodationPriceMetadata();
        BigDecimalSummaryStatistics bigDecimalSummaryStatistics = accommodations.stream().map(accommodation -> accommodation.getPriceBreakdown().getPrice()).collect(BigDecimalSummaryStatistics.statistics());
        priceMetadata.setMaxPrice(bigDecimalSummaryStatistics.getMax());
        priceMetadata.setMinPrice(bigDecimalSummaryStatistics.getMin());
        priceMetadata.setAvgPrice(bigDecimalSummaryStatistics.getAverage());
        BigDecimalSummaryStatistics bigDecimalSummaryStatisticsDiscount = accommodations.stream()
                .map(SearchAccommodationDTO::getPriceBreakdown)
                .map(PriceBreakdownDTO::getDiscount)
                .map(DiscountDTO::getPercentage)
                .collect(BigDecimalSummaryStatistics.statistics());
        priceMetadata.setMaxDiscountPercentage(bigDecimalSummaryStatisticsDiscount.getMax());
        priceMetadata.setPricePerNightRange(calculatePricePerNightRange(numAdults, visitInformation.getCurrency()));
        priceMetadata.setHistogramRange(calculateHistogramRange(visitInformation.getCurrency()));
        return priceMetadata;
    }

    private BigDecimal calculateHistogramRange(String currency) {
        Optional<Long> aRange = Optional.ofNullable(accommodationHistogramRangeByCurrencyConfiguration.getCurrencyRange().get(currency));
        return aRange.map(BigDecimal::valueOf).orElse(DEFAULT_HISTOGRAM_RANGE);
    }

    private BigDecimal calculatePricePerNightRange(Integer numAdults, String currency) {
        Optional<Long> aRange = Optional.ofNullable(accommodationPriceRangeByCurrencyConfiguration.getCurrencyRange().get(currency));
        return aRange.map(range -> computePricePerNightRange(BigDecimal.valueOf(range), numAdults)).orElse(computePricePerNightRange(DEFAULT_PERNIGHT_RANGE, numAdults));
    }

    private BigDecimal computePricePerNightRange(BigDecimal price, Integer numAdults) {
        return numAdults > 0 && numAdults <= 2 ? price.multiply(BigDecimal.valueOf(2)) : price.multiply(BigDecimal.valueOf(numAdults));
    }

    private List<AccommodationFilterMetadata> buildFilterMetadata(List<SearchAccommodationDTO> accommodations) {
        Map<AccommodationFilterKeyDTO, Integer> filters = new HashMap<>();
        accommodations.forEach(accommodation -> {
            increaseHotelTypeFilterCount(filters, accommodation);
            increaseFacilityFilterCount(filters, accommodation);
            increaseNeighbourhoodFilterCount(filters, accommodation);
            increaseStarsCount(filters, accommodation);
            increaseReservationPolicyCount(filters, accommodation);
            increaseDistanceToCityCenterCount(filters, accommodation);
            increaseGuestRatingCount(filters, accommodation);
        });
        return buildFilterList(filters);
    }

    private void increaseGuestRatingCount(Map<AccommodationFilterKeyDTO, Integer> filters, SearchAccommodationDTO accommodation) {
        int guestRatingInterval = (int) (accommodation.getUserReview() != null ? accommodation.getUserReview().getRating() : 0);
        if (guestRatingInterval > 2) {
            if (guestRatingInterval >= 5) {
                increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.GUEST_RATING.name(), GEQ_FIVE_RATING));
            }
            if (guestRatingInterval >= 4) {
                increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.GUEST_RATING.name(), GEQ_FOUR_RATING));
            }
            increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.GUEST_RATING.name(), GEQ_THREE_RATING));
        }
    }

    private void increaseDistanceToCityCenterCount(Map<AccommodationFilterKeyDTO, Integer> filters, SearchAccommodationDTO accommodation) {
        Double distanceFromCityCenter = accommodation.getDistanceFromCityCenter();
        if (distanceFromCityCenter != null && distanceFromCityCenter <= 5) {
            if (distanceFromCityCenter <= 1) {
                increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.DISTANCE_TO_CENTER.name(), LEQ_ONE_DISTANCE));
            }
            if (distanceFromCityCenter <= 3) {
                increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.DISTANCE_TO_CENTER.name(), LEQ_THRE_DISTANCE));
            }
            increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.DISTANCE_TO_CENTER.name(), LEQ_FIVE_DISTANCE));
        }
    }

    private List<AccommodationFilterMetadata> buildFilterList(Map<AccommodationFilterKeyDTO, Integer> filters) {
        List<AccommodationFilterMetadata> filterList = new ArrayList<>();
        filters.forEach((accommodationFilterKey, count) -> {
            filterList.add(new AccommodationFilterMetadata(accommodationFilterKey.getFilterType(), accommodationFilterKey.getFilterName(), count));
        });
        return filterList;
    }

    private void increaseHotelTypeFilterCount(Map<AccommodationFilterKeyDTO, Integer> filters, SearchAccommodationDTO accommodation) {
        increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.PROPERTY_TYPE.name(), extractHotelType(accommodation)));
    }

    private String extractHotelType(SearchAccommodationDTO accommodation) {
        return Optional.ofNullable(accommodation.getAccommodationDeal())
                .map(AccommodationDealInformationDTO::getType)
                .map(AccommodationType::name)
                .orElse(FilterType.OTHER.name());
    }

    private void increaseFacilityFilterCount(Map<AccommodationFilterKeyDTO, Integer> filters, SearchAccommodationDTO accommodation) {
        accommodation.getAccommodationFacilities()
                .stream()
                .map(facility -> AccommodationFacilityGroupsEnum.COVID_MEASURES.name().equals(facility)
                        ? new AccommodationFilterKeyDTO(FilterType.HEALTH.name(), facility)
                        : new AccommodationFilterKeyDTO(FilterType.FACILITY.name(), facility))
                .forEach(facilityTypeKey -> increaseFilterCount(filters, facilityTypeKey));
    }

    private void increaseNeighbourhoodFilterCount(Map<AccommodationFilterKeyDTO, Integer> filters, SearchAccommodationDTO accommodation) {
        increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.NEIGHBOURHOOD.name(), extractNeighbourhood(accommodation)));
    }

    private String extractNeighbourhood(SearchAccommodationDTO accommodation) {
        return Optional.ofNullable(accommodation.getAccommodationDeal())
                .map(AccommodationDealInformationDTO::getNeighbourhood)
                .orElse(FilterType.OTHER.name());
    }

    private void increaseStarsCount(Map<AccommodationFilterKeyDTO, Integer> filters, SearchAccommodationDTO accommodation) {
        increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.STARS.name(), extractStars(accommodation)));
    }

    private String extractStars(SearchAccommodationDTO accommodation) {
        return Optional.ofNullable(accommodation.getAccommodationDeal())
                .map(AccommodationDealInformationDTO::getCategory)
                .map(AccommodationCategory::name)
                .orElse(FilterType.OTHER.name());
    }

    private void increaseReservationPolicyCount(Map<AccommodationFilterKeyDTO, Integer> filters, SearchAccommodationDTO accommodation) {
        if (extractCancellationFree(accommodation)) {
            increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.RESERVATION_POLICY.name(), CANCELLATION_FREE));
        }
        if (extractPayAtDestination(accommodation)) {
            increaseFilterCount(filters, new AccommodationFilterKeyDTO(FilterType.RESERVATION_POLICY.name(), PAY_AT_DESTINATION));
        }
    }

    private boolean extractCancellationFree(SearchAccommodationDTO accommodation) {
        return Optional.ofNullable(accommodation.getAccommodationDeal())
                .map(AccommodationDealInformationDTO::getCancellationFree)
                .map(ThreeValuedLogic.TRUE::equals)
                .orElse(false);
    }

    private boolean extractPayAtDestination(SearchAccommodationDTO accommodation) {
        return Optional.ofNullable(accommodation.getAccommodationDeal())
                .map(AccommodationDealInformationDTO::isPaymentAtDestination)
                .orElse(false);
    }

    private void increaseFilterCount(Map<AccommodationFilterKeyDTO, Integer> filters, AccommodationFilterKeyDTO filterTypeKey) {
        filters.putIfAbsent(filterTypeKey, 0);
        filters.put(filterTypeKey, filters.get(filterTypeKey) + 1);
    }
}
