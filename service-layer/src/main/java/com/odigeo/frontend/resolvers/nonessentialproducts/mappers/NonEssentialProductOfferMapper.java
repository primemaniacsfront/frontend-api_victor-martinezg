package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductOffer;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.Insurance;
import com.odigeo.dapi.client.InsuranceOffer;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryPolicyConfiguration;
import org.apache.commons.lang3.BooleanUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

@Singleton
public class NonEssentialProductOfferMapper {

    @Inject
    private NonEssentialProductsConfiguration nonEssentialProductsConfiguration;
    @Inject
    private NonEssentialProductsUIConfigMapper nonEssentialProductsUIConfigMapper;
    @Inject
    private  NonEssentialProductMapper nonEssentialProductMapper;
    @Inject
    private Logger logger;

    public NonEssentialProductOffer map(InsuranceOffer insuranceOffer, VisitInformation visitInformation) {
        return getNonEssentialProductOffer(insuranceOffer, visitInformation);
    }

    private NonEssentialProductOffer getNonEssentialProductOffer(InsuranceOffer insuranceOffer, VisitInformation visitInformation) {
        NonEssentialProductOffer nonEssentialProductOffer = null;
        Insurance insurance = Optional.ofNullable(insuranceOffer)
                .map(InsuranceOffer::getInsurances)
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
        if (Objects.nonNull(insurance)) {
            try {
                AncillaryPolicyConfiguration ancillaryPolicyConfiguration = nonEssentialProductsConfiguration.getConfiguration(visitInformation);
                AncillaryConfiguration ancillaryConfiguration = ancillaryPolicyConfiguration.get(insurance.getPolicy());
                if (Objects.nonNull(ancillaryConfiguration)) {
                    nonEssentialProductOffer = new NonEssentialProductOffer();
                    fillNonEssentialProductOffer(insuranceOffer, ancillaryConfiguration, visitInformation, nonEssentialProductOffer);
                }
            } catch (IOException e) {
                logger.error(NonEssentialProductOfferMapper.class, e);
            }
        }

        return nonEssentialProductOffer;
    }

    private void fillNonEssentialProductOffer(InsuranceOffer insuranceOffer, AncillaryConfiguration ancillaryConfiguration, VisitInformation visitInformation, NonEssentialProductOffer nonEssentialProduct) {
        nonEssentialProduct.setProduct(
                nonEssentialProductMapper.map(insuranceOffer, visitInformation)
        );
        nonEssentialProduct.setUiConfig(nonEssentialProductsUIConfigMapper.map(ancillaryConfiguration));
        nonEssentialProduct.setMandatory(BooleanUtils.isTrue(ancillaryConfiguration.getMandatory()));
    }

}
