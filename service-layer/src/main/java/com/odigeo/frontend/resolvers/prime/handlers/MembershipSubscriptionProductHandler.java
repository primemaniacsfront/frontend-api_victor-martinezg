package com.odigeo.frontend.resolvers.prime.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.ShoppingInfoUtils;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import com.odigeo.frontend.resolvers.prime.operations.MembershipShoppingOperations;
import com.odigeo.frontend.resolvers.prime.operations.MembershipShoppingOperationsFactory;

@Singleton
public class MembershipSubscriptionProductHandler {

    @Inject
    private MembershipShoppingOperationsFactory membershipShoppingOperationsFactory;
    @Inject
    private ShoppingInfoUtils shoppingInfoUtils;

    public MembershipSubscriptionResponse getSubscriptionFromShoppingCart(ShoppingInfo shoppingInfo, ResolverContext context) {
        return getMembershipShoppingOperations(shoppingInfo)
                .getMembershipSubscriptionResponse(shoppingInfoUtils.getShoppingId(shoppingInfo), context);
    }

    public MembershipSubscriptionResponse updateMembershipSubscription(ModifyMembershipSubscription modifyMembershipSubscription, ResolverContext context) {
        return getMembershipShoppingOperations(modifyMembershipSubscription.getShoppingInfo())
                .updateMembershipSubscriptionProduct(modifyMembershipSubscription, context);
    }

    private MembershipShoppingOperations getMembershipShoppingOperations(ShoppingInfo shoppingInfo) {
        return membershipShoppingOperationsFactory
                .getMembershipShoppingOperations(shoppingInfoUtils.getShoppingType(shoppingInfo));
    }
}
