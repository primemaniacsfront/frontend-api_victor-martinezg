package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.insuranceproduct.api.last.request.ShoppingBasketId;
import com.google.inject.Singleton;

@Singleton
public class ShoppingBasketIdMapper {

    public ShoppingBasketId map(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketId shoppingBasketId) {
        ShoppingBasketId shoppingBasketIdRequest = new ShoppingBasketId();
        shoppingBasketIdRequest.setId(shoppingBasketId.getId());
        return shoppingBasketIdRequest;
    }
}
