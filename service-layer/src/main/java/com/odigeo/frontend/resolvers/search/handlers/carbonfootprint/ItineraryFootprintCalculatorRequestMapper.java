package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.request.ItineraryFootprintCalculatorRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;

@Singleton
public class ItineraryFootprintCalculatorRequestMapper {

    @Inject
    private CarbonFootprintSearchCriteriaHandler searchCriteriaHandler;

    @Inject
    private FootprintItineraryMapper footprintItineraryMapper;

    public ItineraryFootprintCalculatorRequest map(SearchRequest searchRequest, SearchResponseDTO searchResponse,
                                                   ResolverContext context) {

        ItineraryFootprintCalculatorRequest calculatorRequest = new ItineraryFootprintCalculatorRequest();
        calculatorRequest.setSearchCriteria(searchCriteriaHandler.buildSearchCriteria(searchRequest, searchResponse, context.getVisitInformation()));
        calculatorRequest.setItineraries(footprintItineraryMapper.map(searchResponse.getItineraries()));
        return calculatorRequest;

    }
}
