package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;

import static com.odigeo.frontend.commons.context.visit.types.Site.*;

@Singleton
public class HotelXSellingHandler {

    private static final Set<Site> ENABLED_SITES = Sets.newHashSet(
        AR, AU, BR, CA, CH, CL, CO, DE, ES, FR, GB, IT, MX, NL, NZ, PE,
        PT, RU, UK, US, HK, PH, TH, SG, IN, GR, TR, ID, MA, AE, ZA,
        GOFR,
        OPAT, OPDE, OPDK2, OPFI2, OPFR, OPGB, OPIT, OPNO2, OPPL2, OPSE2, OPUK,
        TRDE, TRDK, TRFI, TRGB, TRIS, TRNO, TRSE
    );

    private static final Set<String> DISABLED_CARRIERS = Sets.newHashSet(
        "9W", "WW", "ZI", "SW", "DE", "4O", "P9", "AZ", "TG", "EG", "SA", "UT", "HX", "IG", "KK", "HU",
        "DY", "D8", "BE", "AY", "I9", "CX", "DI", "H5", "Z8", "AV", "JP", "MT", "DK", "SE", "O6"
    );

    private static final int DESKTOP_MAX_NIGHTS = 30;
    private static final int MOBILE_MAX_NIGHTS = 7;

    @Inject
    private DateUtils dateUtils;
    @Inject
    private SiteVariations siteVariations;

    public void populateIsEnabled(SearchItineraryDTO itinerary, SearchRequest searchRequest, VisitInformation visit) {
        boolean isEnabled = siteVariations.isDptIpTouchPointEnabled(visit)
            && isRoundTrip(searchRequest)
            && isEnabledSite(visit)
            && isLessThanMaxNights(visit, searchRequest)
            && isEnabledCarrier(itinerary);

        itinerary.setHotelXSellingEnabled(isEnabled);
    }

    private boolean isRoundTrip(SearchRequest searchRequestDTO) {
        return searchRequestDTO.getTripType() == TripType.ROUND_TRIP;
    }

    private boolean isEnabledSite(VisitInformation visit) {
        return ENABLED_SITES.contains(visit.getSite());
    }

    private boolean isLessThanMaxNights(VisitInformation visit, SearchRequest searchRequestDTO) {
        List<SegmentRequest> segments = searchRequestDTO.getItinerary().getSegments();
        SegmentRequest firstSegment = IterableUtils.first(segments);
        SegmentRequest lastSegment = Iterables.getLast(segments);
        LocalDate departureDate = dateUtils.convertFromIsoToLocalDate(firstSegment.getDate());
        LocalDate arrivalDate = dateUtils.convertFromIsoToLocalDate(lastSegment.getDate());

        int maxNights = visit.getDevice() == Device.MOBILE ? MOBILE_MAX_NIGHTS : DESKTOP_MAX_NIGHTS;
        long days = ChronoUnit.DAYS.between(departureDate, arrivalDate);

        return days < maxNights;
    }

    private boolean isEnabledCarrier(SearchItineraryDTO itinerary) {
        return !CollectionUtils.containsAny(itinerary.getSectionsCarrierIds(), DISABLED_CARRIERS);
    }

}
