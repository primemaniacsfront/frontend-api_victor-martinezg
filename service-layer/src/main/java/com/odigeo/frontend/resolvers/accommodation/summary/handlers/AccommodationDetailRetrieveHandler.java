package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSummaryRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.services.DapiService;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetailResponse;

@Singleton
public class AccommodationDetailRetrieveHandler {

    @Inject
    private SearchService searchService;
    @Inject
    private DapiService dapiService;

    public AccommodationDetailResponse getAccommodationDetails(AccommodationSummaryRequest requestDTO, VisitInformation visitInformation) {
        return searchService.getAccommodationDetails(requestDTO.getSearchId(), Long.parseLong(requestDTO.getAccommodationDealKey()), visitInformation.getVisitCode());
    }

    public ShoppingCartSummaryResponse getShoppingCart(long bookingId, ResolverContext context) {
        return dapiService.getShoppingCart(bookingId, context);
    }
}
