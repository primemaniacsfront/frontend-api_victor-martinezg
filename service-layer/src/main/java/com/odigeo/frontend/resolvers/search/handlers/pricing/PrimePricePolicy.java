package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.common.ItinerarySortCriteria;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.membership.MembershipPerks;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton
public class PrimePricePolicy extends AbstractPricePolicy {

    private static final String DISCOUNTED_FEE_TYPE_ID = "MEMBER_PRICE_POLICY_DISCOUNTED";
    private static final String UNDISCOUNTED_FEE_TYPE_ID = "MEMBER_PRICE_POLICY_UNDISCOUNTED";
    private static final String DISCOUNT_PLUS_COUPON_FEE_TYPE_ID = "MEMBER_PRICE_POLICY_DISCOUNTED_PLUS_COUPON";

    @Override
    protected List<Fee> calculateFees(SearchEngineContext seContext, FareItinerary fareItinerary) {
        ItinerarySortCriteria itinerarySortCriteria = getSortCriteria(seContext);
        boolean isSortPriceWithMembershipDiscountApplied = isSortPriceWithMembershipDiscountApplied(itinerarySortCriteria);
        List<Fee> fees = new ArrayList<>(Arrays.asList(createUndiscountedFee(fareItinerary, isSortPriceWithMembershipDiscountApplied),
                createDiscountedFee(fareItinerary, isSortPriceWithMembershipDiscountApplied)));
        createDiscountedPlusCouponFee(fareItinerary, isSortPriceWithMembershipDiscountApplied)
                .ifPresent(fees::add);
        return fees;
    }

    private boolean isSortPriceWithMembershipDiscountApplied(ItinerarySortCriteria itinerarySortCriteria) {
        return ItinerarySortCriteria.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE.equals(itinerarySortCriteria);
    }

    @Override
    protected String calculateDefaultFeeTypeId() {
        return DISCOUNTED_FEE_TYPE_ID;
    }

    private BigDecimal getPriceWithoutMembershipPerksFee(FareItinerary fareItinerary, boolean isSortPriceWithMembershipDiscountApplied) {
        BigDecimal sortPrice = fareItinerary.getPrice().getSortPrice();
        BigDecimal membershipPerksFee = getMembershipPerksFee(fareItinerary);
        BigDecimal couponFee = getCouponAmount(fareItinerary);
        return isSortPriceWithMembershipDiscountApplied
                ? sortPrice.subtract(membershipPerksFee).subtract(couponFee)
                : sortPrice;
    }

    private BigDecimal getMembershipPerksFee(FareItinerary fareItinerary) {
        return Optional.ofNullable(fareItinerary.getMembershipPerks())
                .map(MembershipPerks::getFee)
                .orElse(BigDecimal.ZERO);
    }

    private Fee createUndiscountedFee(FareItinerary fareItinerary, boolean isSortPriceWithMembershipDiscountApplied) {
        BigDecimal priceWithoutPerksFee = getPriceWithoutMembershipPerksFee(fareItinerary, isSortPriceWithMembershipDiscountApplied);
        return createFee(priceWithoutPerksFee, UNDISCOUNTED_FEE_TYPE_ID);
    }

    private Fee createDiscountedFee(FareItinerary fareItinerary, boolean isSortPriceWithMembershipDiscountApplied) {
        BigDecimal priceWithoutPerksFee = getPriceWithoutMembershipPerksFee(fareItinerary, isSortPriceWithMembershipDiscountApplied);
        BigDecimal priceWithPerksWithoutCoupon = priceWithoutPerksFee.add(getMembershipPerksFee(fareItinerary));
        return createFee(priceWithPerksWithoutCoupon, DISCOUNTED_FEE_TYPE_ID);
    }

    private Optional<Fee> createDiscountedPlusCouponFee(FareItinerary fareItinerary, boolean isSortPriceWithMembershipDiscountApplied) {
        BigDecimal priceWithoutPerksFee = getPriceWithoutMembershipPerksFee(fareItinerary, isSortPriceWithMembershipDiscountApplied);
        BigDecimal priceWithPerksFee = priceWithoutPerksFee.add(getMembershipPerksFee(fareItinerary)).add(getCouponAmount(fareItinerary));
        return BigDecimal.ZERO.compareTo(getCouponAmount(fareItinerary)) != 0
                ? Optional.of(createFee(priceWithPerksFee, DISCOUNT_PLUS_COUPON_FEE_TYPE_ID))
                : Optional.empty();
    }

    private BigDecimal getCouponAmount(FareItinerary fareItinerary) {
        return Optional.ofNullable(fareItinerary.getMembershipPerks())
                .map(MembershipPerks::getMembershipCoupon)
                .orElse(BigDecimal.ZERO);
    }

    private Fee createFee(BigDecimal price, String feeTypeId) {
        Fee fee = new Fee();
        FeeType feeType = new FeeType();
        Money money = new Money();
        money.setAmount(price);
        feeType.setId(feeTypeId);
        fee.setPrice(money);
        fee.setType(feeType);
        return fee;
    }
}
