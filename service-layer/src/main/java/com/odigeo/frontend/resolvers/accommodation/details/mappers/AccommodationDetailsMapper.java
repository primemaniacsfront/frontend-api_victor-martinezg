package com.odigeo.frontend.resolvers.accommodation.details.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationDealInformationMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationImagesMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.details.models.AccommodationDetailsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.mappers.AccommodationFacilitiesMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.hcsapi.v9.beans.Hotel;
import com.odigeo.hcsapi.v9.beans.HotelImage;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.hcsapi.v9.beans.Legend;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class AccommodationDetailsMapper {

    @Inject
    private AccommodationImagesMapper accommodationImagesMapper;
    @Inject
    private AccommodationFacilitiesMapper accommodationFacilitiesMapper;
    @Inject
    private AccommodationDealInformationMapper accommodationDealInformationMapper;

    public AccommodationDetailsResponseDTO map(Hotel hotelDetails, Legend legend) {
        AccommodationDetailsResponseDTO accommodationDetailResponse = new AccommodationDetailsResponseDTO();
        if (hotelDetails != null) {
            AccommodationDetailDTO accommodationDetail = new AccommodationDetailDTO();
            accommodationDetail.setAccommodationDealInformation(accommodationDealInformationMapper.mapHcs(hotelDetails));
            accommodationDetail.setPostalCode(hotelDetails.getPostalCode());
            accommodationDetail.setCountry(hotelDetails.getCountry());
            accommodationDetail.setCheckInPolicy(hotelDetails.getCheckIn());
            accommodationDetail.setCheckOutPolicy(hotelDetails.getCheckOut());
            accommodationDetail.setHotelPolicy(hotelDetails.getHotelPolicy());
            accommodationDetail.setPaymentMethod(hotelDetails.getPaymentMethod());
            accommodationDetail.setAccommodationImages(mapImages(hotelDetails.getHotelImages()));
            accommodationDetail.setNumberOfRooms(hotelDetails.getNumberOfRooms());
            accommodationDetail.setHotelType(mapAccommodationType(hotelDetails.getHotelSummary()));
            accommodationDetailResponse.setAccommodationDetail(accommodationDetail);
            mapFacilities(hotelDetails, legend, accommodationDetail);
        }
        return accommodationDetailResponse;
    }

    private AccommodationType mapAccommodationType(HotelSummary hotelSummary) {
        return Optional.ofNullable(hotelSummary)
            .map(HotelSummary::getType)
            .map(Enum::name)
            .map(hotelType -> EnumUtils.getEnum(AccommodationType.class, hotelType))
            .orElse(null);
    }

    private void mapFacilities(Hotel hotelRs, Legend legend, AccommodationDetailDTO accommodationDetail) {
        Optional.ofNullable(hotelRs)
            .map(Hotel::getHotelSummary)
            .map(HotelSummary::getHotelFacilities)
            .ifPresent(facilities -> accommodationDetail.setFacilityGroups(accommodationFacilitiesMapper.mapFacilityGroups(facilities, legend)));
    }

    private List<AccommodationImageDTO> mapImages(List<HotelImage> images) {
        return CollectionUtils.emptyIfNull(images)
            .stream()
            .map(accommodationImagesMapper::mapHcsImage)
            .collect(Collectors.toList());
    }
}
