package com.odigeo.frontend.resolvers.prime.models;

public enum MembershipStatus {
    PENDING_TO_ACTIVATE,
    PENDING_TO_COLLECT,
    EXPIRED,
    DEACTIVATED,
    ACTIVATED
}
