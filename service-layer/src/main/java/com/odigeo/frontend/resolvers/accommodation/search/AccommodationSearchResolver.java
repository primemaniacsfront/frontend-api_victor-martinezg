package com.odigeo.frontend.resolvers.accommodation.search;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.accommodation.search.handlers.AccommodationSearchHandler;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

@Singleton
public class AccommodationSearchResolver implements Resolver {

    static final AccommodationSearchType SEARCH_TYPE = AccommodationSearchType.DYNPACK;
    private static final String SEARCH_ACCOMMODATION_QUERY = "searchAccommodation";

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private AccommodationSearchHandler accommodationSearchHandler;


    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher(SEARCH_ACCOMMODATION_QUERY, this::searchAccommodationFetcher));
    }

    AccommodationSearchResponse searchAccommodationFetcher(DataFetchingEnvironment env) {
        AccommodationSearchRequest requestParams = jsonUtils.fromDataFetching(env, AccommodationSearchRequest.class);
        requestParams.setSearchType(SEARCH_TYPE);
        ResolverContext resolverContext = env.getContext();
        GraphQLContext context = env.getGraphQlContext();


        VisitInformation visit = resolverContext.getVisitInformation();
        MetricsHandler metricsHandler = context.get(MetricsHandler.class);

        metricsHandler.startMetric(MeasureConstants.SEARCH_ACCOMMODATION_BACKEND_TOTAL_TIME);

        AccommodationSearchResponse response = accommodationSearchHandler.searchAccommodation(requestParams, resolverContext, visit);

        metricsHandler.stopMetric(MeasureConstants.SEARCH_ACCOMMODATION_BACKEND_TOTAL_TIME, visit);
        return response;
    }

}
