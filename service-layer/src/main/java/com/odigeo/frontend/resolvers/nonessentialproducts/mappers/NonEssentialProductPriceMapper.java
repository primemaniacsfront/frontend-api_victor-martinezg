package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Price;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;

import java.math.BigDecimal;

@Singleton
public class NonEssentialProductPriceMapper {

    @Inject
    private PricePerceptionMapper pricePerceptionMapper;

    public Price map(BigDecimal amount, String currency, AncillaryConfiguration ancillaryConfiguration) {
        Price price = new Price();

        price.setPerception(pricePerceptionMapper.map(ancillaryConfiguration.getPricePerception()));
        Money money = new Money();
        money.setAmount(amount);
        money.setCurrency(currency);
        price.setValue(money);

        return price;
    }

}
