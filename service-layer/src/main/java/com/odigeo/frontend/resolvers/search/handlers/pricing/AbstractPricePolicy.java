package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.common.ItinerarySortCriteria;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

public abstract class AbstractPricePolicy {

    public final List<Fee> calculateItineraryPrices(SearchEngineContext seContext,
                                                    FareItinerary fareItinerary, BigDecimal passengers) {

        List<Fee> itineraryFees = calculateFees(seContext, fareItinerary);
        String currency = seContext.getCurrency();

        itineraryFees.forEach(fee -> {
            populatePricePerPassenger(fee, passengers);
            populateCurrency(fee, currency);
        });

        return itineraryFees;
    }

    public BigDecimal calculatePricePerPassenger(BigDecimal amount, BigDecimal passengers) {
        return amount.divide(passengers, 2, RoundingMode.HALF_UP);
    }

    public void populatePricePerPassenger(Fee fee, BigDecimal passengers) {
        Money price = fee.getPrice();
        price.setAmount(calculatePricePerPassenger(price.getAmount(), passengers));
    }

    public void populateCurrency(Fee fee, String currency) {
        fee.getPrice().setCurrency(currency);
    }

    public final FeeType calculateDefaultFeeType() {
        FeeType defaultFeeType = new FeeType();
        defaultFeeType.setId(calculateDefaultFeeTypeId());

        return defaultFeeType;
    }

    protected abstract List<Fee> calculateFees(SearchEngineContext seContext, FareItinerary fareItinerary);

    protected ItinerarySortCriteria getSortCriteria(SearchEngineContext searchEngineContext) {
        return Optional.ofNullable(searchEngineContext)
                .map(SearchEngineContext::getItinerarySortCriteria)
                .orElse(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    }

    protected abstract String calculateDefaultFeeTypeId();

}
