package com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AncillariesSelectionRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PassengerSeatsSelectionRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryapi.v1.request.AncillarySelectionRequest;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class AncillarySelectionRequestMapper {

    @Inject
    private SeatMapSelectionRequestMapper seatMapSelectionRequestMapper;

    public List<AncillarySelectionRequest> map(AncillariesSelectionRequest ancillariesSelectionRequest) {
        return CollectionUtils.emptyIfNull(ancillariesSelectionRequest.getSeats())
                .stream()
                .map(this::mapAncillarySelectionRequest)
                .collect(Collectors.toList());
    }

    private AncillarySelectionRequest mapAncillarySelectionRequest(PassengerSeatsSelectionRequest passengerSeatsSelectionRequest) {
        AncillarySelectionRequest ancillarySelectionRequest = new AncillarySelectionRequest();
        ancillarySelectionRequest.setTravellerNumber(passengerSeatsSelectionRequest.getPassenger());
        ancillarySelectionRequest.setSeatMapSelectionRequestList(seatMapSelectionRequestMapper.map(passengerSeatsSelectionRequest.getSelection()));
        return ancillarySelectionRequest;
    }
}
