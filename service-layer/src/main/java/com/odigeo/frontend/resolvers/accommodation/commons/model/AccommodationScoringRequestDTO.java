package com.odigeo.frontend.resolvers.accommodation.commons.model;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationResponseDTO;
import com.odigeo.searchengine.v2.requests.AccommodationRoomRequest;
import com.odigeo.searchengine.v2.requests.LocationRequest;
import com.odigeo.searchengine.v2.responses.accommodation.SearchType;

import java.util.Calendar;
import java.util.List;

public class AccommodationScoringRequestDTO {
    private final Long searchId;
    private final Calendar checkInDate;
    private final Calendar checkOutDate;
    private final List<AccommodationRoomRequest> roomRequests;
    private final SearchType searchType;
    private final LocationRequest destination;
    private final AccommodationResponseDTO response;
    private final VisitInformation visit;

    public AccommodationScoringRequestDTO(Long searchId, Calendar checkInDate, Calendar checkOutDate, List<AccommodationRoomRequest> roomRequests, SearchType searchType, LocationRequest destination, AccommodationResponseDTO response, VisitInformation visit) {
        this.searchId = searchId;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.roomRequests = roomRequests;
        this.searchType = searchType;
        this.destination = destination;
        this.response = response;
        this.visit = visit;
    }

    public Long getSearchId() {
        return searchId;
    }

    public Calendar getCheckInDate() {
        return checkInDate;
    }

    public Calendar getCheckOutDate() {
        return checkOutDate;
    }

    public List<AccommodationRoomRequest> getRoomRequests() {
        return roomRequests;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public LocationRequest getDestination() {
        return destination;
    }

    public AccommodationResponseDTO getResponse() {
        return response;
    }

    public VisitInformation getVisit() {
        return visit;
    }
}
