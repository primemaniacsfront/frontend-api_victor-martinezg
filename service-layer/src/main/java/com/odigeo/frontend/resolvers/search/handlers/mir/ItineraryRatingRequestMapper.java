package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.Itinerary;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.SearchCriteria;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.request.ItineraryRatingRequest;

import java.util.List;

@Singleton
public class ItineraryRatingRequestMapper {

    @Inject
    private MirSearchCriteriaHandler mirSearchCriteriaHandler;
    @Inject
    private MirItineraryHandler mirItineraryHandler;

    public ItineraryRatingRequest map(SearchRequest searchRequest, SearchResponseDTO searchResponse,
                                      ResolverContext context) {

        ItineraryRatingRequest ratingRequest = new ItineraryRatingRequest();
        VisitInformation visit = context.getVisitInformation();
        ratingRequest.setVisitInformation(visit.getVisitCode());

        List<Itinerary> ratingItineraries = mirItineraryHandler.buildRatingItineraries(searchResponse);
        SearchCriteria searchCriteria = mirSearchCriteriaHandler
                .buildRatingSearchCriteria(searchRequest, searchResponse, visit);

        ratingRequest.setSearchCriteria(searchCriteria);
        ratingRequest.setItineraries(ratingItineraries);
        return ratingRequest;

    }
}
