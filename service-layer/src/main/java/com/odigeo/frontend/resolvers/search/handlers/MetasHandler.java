package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelection;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.searchengine.v2.responses.ExternalSelectionResponse;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerarySearchResults;

@Singleton
public class MetasHandler {

    public void populateExternalSelection(SearchResponse searchResponse, SearchResponseDTO searchResponseDTO) {
        ItinerarySearchResults itinerarySearchResults = searchResponse.getItinerarySearchResults();

        if (itinerarySearchResults != null && itinerarySearchResults.getExternalSelectionResponse() != null) {
            ExternalSelectionResponse externalSelectionResponse = itinerarySearchResults.getExternalSelectionResponse();
            ExternalSelection externalSelection = new ExternalSelection();
            externalSelection.setId(externalSelectionResponse.getFareItineraryKey());
            externalSelection.setSegmentKeys(externalSelectionResponse.getSegmentKeys());

            searchResponseDTO.setExternalSelection(externalSelection);
        }
    }

}
