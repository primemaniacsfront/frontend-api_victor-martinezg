package com.odigeo.frontend.resolvers.prime.models;

public enum PrimeMarketStatus {
    ROLLOUT,
    IN_AB,
    UNKNOWN
}
