package com.odigeo.frontend.resolvers.deals.mappers;

import com.google.inject.Singleton;
import com.odigeo.geoapi.v4.responses.City;

import java.util.Locale;

@Singleton
public class CityMapper {

    public com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.City map(City city, Locale locale) {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.City cityContract =
                new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.City();
        cityContract.setIata(city.getIataCode());
        cityContract.setName(city.getName().getText(locale));
        return cityContract;
    }

}
