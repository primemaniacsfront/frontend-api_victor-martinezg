package com.odigeo.frontend.resolvers.checkout.mappers;

import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteraction;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Param;
import com.odigeo.dapi.client.UserInteractionNeededResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Mapper
public interface UserPaymentInteractionMapper {

    UserPaymentInteraction mapFromDapi(UserInteractionNeededResponse interactionData);

    default Map<String, String> mapParameters(UserInteractionNeededResponse.Parameters parameters) {
        return Optional.ofNullable(parameters).map(UserInteractionNeededResponse.Parameters::getEntry)
                .orElse(Collections.emptyList()).stream()
                .collect(Collectors.toMap(UserInteractionNeededResponse.Parameters.Entry::getKey, UserInteractionNeededResponse.Parameters.Entry::getValue));
    }

    @Mapping(source = "parameters", target = "parameters", qualifiedByName = "mapParametersToContract")
    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteraction mapUserPaymentInteractionToContract(UserPaymentInteraction userPaymentInteraction);


    @Named("mapParametersToContract")
    default List<Param> mapParametersToContract(Map<String, String> parameters) {
        return parameters.entrySet().stream()
                .map(entry -> new Param(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
