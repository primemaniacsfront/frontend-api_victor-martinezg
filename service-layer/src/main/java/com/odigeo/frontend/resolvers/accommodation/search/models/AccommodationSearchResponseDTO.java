package com.odigeo.frontend.resolvers.accommodation.search.models;


public class AccommodationSearchResponseDTO {

    private Long searchId;
    private AccommodationResponseDTO accommodationResponse;


    public Long getSearchId() {
        return searchId;
    }

    public void setSearchId(Long searchId) {
        this.searchId = searchId;
    }

    public AccommodationResponseDTO getAccommodationResponse() {
        return accommodationResponse;
    }

    public void setAccommodationResponse(AccommodationResponseDTO accommodationResponse) {
        this.accommodationResponse = accommodationResponse;
    }
}
