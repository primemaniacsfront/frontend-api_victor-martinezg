package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsModal;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsModalFeatureType;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import org.apache.commons.lang3.BooleanUtils;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class NonEssentialProductsModalMapper {

    public NonEssentialProductsModal map(AncillaryConfiguration ancillaryConfiguration) {
        NonEssentialProductsModal nonEssentialProductsModal = new NonEssentialProductsModal();

        nonEssentialProductsModal.setActive(BooleanUtils.isTrue(ancillaryConfiguration.getIsModalActive()));
        List<NonEssentialProductsModalFeatureType> nonEssentialProductsModalFeatureTypes = new ArrayList<>();
        if (BooleanUtils.isTrue(ancillaryConfiguration.getShowModalExclusions())) {
            nonEssentialProductsModalFeatureTypes.add(NonEssentialProductsModalFeatureType.EXCLUSIONS);
        }
        if (BooleanUtils.isTrue(ancillaryConfiguration.getShowModalBenefits())) {
            nonEssentialProductsModalFeatureTypes.add(NonEssentialProductsModalFeatureType.BENEFITS);
        }
        if (BooleanUtils.isTrue(ancillaryConfiguration.getIsAttachableFromModal())) {
            nonEssentialProductsModalFeatureTypes.add(NonEssentialProductsModalFeatureType.ATTACHABLE);
        }
        if (BooleanUtils.isTrue(ancillaryConfiguration.getShowModalDualContract())) {
            nonEssentialProductsModalFeatureTypes.add(NonEssentialProductsModalFeatureType.DUAL_CONTRACT);
        }
        nonEssentialProductsModal.setFeatures(nonEssentialProductsModalFeatureTypes);

        return nonEssentialProductsModal;
    }
}
