package com.odigeo.frontend.resolvers.user.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PersonalInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserTraveller;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.user.mappers.TravellerMapper;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.User;
import graphql.schema.DataFetchingEnvironment;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class UserTravellersHandler {

    @Inject
    private UserDescriptionService userDescriptionService;
    @Inject
    private MembershipHandler membershipHandler;
    @Inject
    private TravellerMapper travellerMapper;

    public List<UserTraveller> getTravellers(DataFetchingEnvironment env) {
        Membership membership = Optional.ofNullable((Membership) env.getGraphQlContext().get("Membership")).orElse(membershipHandler.getMembershipFromLoggedUser(env));
        User user = Optional.ofNullable((User) env.getGraphQlContext().get("User")).orElse(userDescriptionService.getUserByToken(env.getContext()));
        List<UserTraveller> travellers = Optional.ofNullable(user)
                .map(User::getTraveller)
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .map(travellerMapper::travellerContractToModel)
                .collect(Collectors.toList());
        if (membership != null) {
            checkPrimeTraveller(membership, travellers, user);
        }
        return travellers;
    }

    private void checkPrimeTraveller(Membership membership, List<UserTraveller> travellers, User user) {
        if (travellers.isEmpty() || travellers.stream().noneMatch(traveller -> traveller.getEmail().equals(user.getEmail()))) {
            UserTraveller traveller = new UserTraveller();
            editPrimeInfo(membership, traveller);
            traveller.setEmail(user.getEmail());
            travellers.add(traveller);
        } else {
            travellers.stream()
                    .filter(traveller -> membership != null && (traveller.getEmail().equals(user.getEmail())))
                    .findFirst().ifPresent(primeTraveller -> editPrimeInfo(membership, primeTraveller));
        }
    }

    private void editPrimeInfo(Membership membership, UserTraveller traveller) {
        traveller.setIsPrimeOwner(true);
        if (traveller.getPersonalInfo() == null) {
            traveller.setPersonalInfo(new PersonalInfo());
        }
        traveller.getPersonalInfo().setName(membership.getName());
        String[] lastNamesSplited = membership.getLastNames().split("\\s+");
        traveller.getPersonalInfo().setFirstLastName(lastNamesSplited[0]);
        traveller.getPersonalInfo().setSecondLastName(lastNamesSplited.length > 1 ? lastNamesSplited[1] : "");
    }

    @Deprecated
    public List<UserTraveller> getTravellers(User user) {
        return Optional.ofNullable(user)
                .map(User::getTraveller)
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .map(travellerMapper::travellerContractToModel)
                .collect(Collectors.toList());
    }
}
