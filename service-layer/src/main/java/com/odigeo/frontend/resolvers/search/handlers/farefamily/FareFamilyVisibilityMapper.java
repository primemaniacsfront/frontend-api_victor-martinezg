package com.odigeo.frontend.resolvers.search.handlers.farefamily;

import com.google.common.collect.Sets;
import com.google.inject.Singleton;
import org.apache.commons.lang3.StringUtils;

import java.util.Set;

@Singleton
class FareFamilyVisibilityMapper {

    private static final String ALWAYS = "Always";
    private static final String EXPANDED = "Expanded";
    private static final Set<String> PERK_TYPES_VISIBILITY_ALWAYS = Sets.newHashSet(
            "CABIN_BAG",
            "CHECKED_BAG",
            "STANDARD_SEAT_RESERVATION",
            "PREFERRED_SEAT_RESERVATION",
            "EXTRA_LEGROOM_SEAT_RESERVATION",
            "CHANGE_AFTER_DEPARTURE",
            "CHANGE_BEFORE_DEPARTURE",
            "EARLIER_FLIGHT_ON_TRAVEL_DAY",
            "REFUND_AFTER_DEPARTURE",
            "REFUND_BEFORE_DEPARTURE",
            "LOUNGE_ACCESS",
            "PRIORITY_BOARDING"
    );
    private static final Set<String> PERK_TYPES_VISIBILITY_EXPANDED = Sets.newHashSet(
            "MIDDLE_SEAT_KEPT_VACANT",
            "PRIORITY_SECURITY",
            "PRIORITY_CHECK_IN",
            "PRIORITY_BAGGAGE",
            "COMPLIMENTARY_FOOD_AND_BEV",
            "COMPLIMENTARY_SNACK_AND_DRINKS",
            "COMPLIMENTARY_PREMIUM_MEAL",
            "BUSINESS_CLASS_FOOD_AND_BEV",
            "PERCENT_MILES_100",
            "PERCENT_MILES_150",
            "MAGAZINES_AND_NEWSPAPERS",
            "UPGRADE_INTO_BUSINESS_CLASS",
            "PERSONAL_ITEM",
            "UPGRADE_ELIGIBILITY",
            "IN_FLIGHT_ENTERTAINMENT",
            "BEVERAGE",
            "WIFI"
    );

    public String map(String perkType) {
        return PERK_TYPES_VISIBILITY_ALWAYS.contains(perkType) ? ALWAYS
            : PERK_TYPES_VISIBILITY_EXPANDED.contains(perkType) ? EXPANDED : StringUtils.EMPTY;
    }

}
