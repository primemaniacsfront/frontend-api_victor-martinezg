package com.odigeo.frontend.resolvers.membershipoffer;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.membership.offer.api.MembershipOfferService;
import com.odigeo.membership.offer.api.exception.DataNotFoundException;
import com.odigeo.membership.offer.api.exception.InvalidParametersException;
import com.odigeo.membership.offer.api.exception.MembershipOfferServiceException;
import com.odigeo.membership.offer.api.request.MembershipOfferRequest;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffers;

@Singleton
public class MembershipOfferHandler {
    @Inject
    private MembershipOfferService membershipOfferService;
    @Inject
    private MetricsHandler metricsHandler;

    public MembershipSubscriptionOffer getSubscriptionOffer(MembershipOfferRequest membershipOfferRequest) {
        try {
            MembershipSubscriptionOffer membershipSubscriptionOffer = membershipOfferService.createMembershipSubscriptionOffer(membershipOfferRequest);
            metricsHandler.trackCounter(MeasureConstants.MOS_CREATE_OFFER);
            return membershipSubscriptionOffer;
        } catch (InvalidParametersException | MembershipOfferServiceException ex) {
            throw new ServiceException("Error in createMembershipSubscriptionOffer", ex, ServiceName.MEMBERSHIP_OFFER_API);
        }
    }

    public MembershipSubscriptionOffers getSubscriptionOffers(MembershipOfferRequest membershipOfferRequest) {
        try {
            MembershipSubscriptionOffers membershipSubscriptionOffers = membershipOfferService.createMembershipSubscriptionOffers(membershipOfferRequest);
            metricsHandler.trackCounter(MeasureConstants.MOS_CREATE_OFFERS);
            return membershipSubscriptionOffers;
        } catch (InvalidParametersException | MembershipOfferServiceException ex) {
            throw new ServiceException("Error in createMembershipSubscriptionOffers", ex, ServiceName.MEMBERSHIP_OFFER_API);
        }
    }

    public MembershipSubscriptionOffer getSubscriptionOffer(String offerId) {
        try {
            MembershipSubscriptionOffer membershipSubscriptionOffer = membershipOfferService.getMembershipSubscriptionOffer(offerId);
            metricsHandler.trackCounter(MeasureConstants.MOS_GET_OFFER);
            return membershipSubscriptionOffer;
        } catch (DataNotFoundException | MembershipOfferServiceException ex) {
            return null;
        }
    }
}
