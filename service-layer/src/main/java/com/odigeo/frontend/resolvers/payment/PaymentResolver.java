package com.odigeo.frontend.resolvers.payment;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCardBinDetails;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCardDateDetailsResponse;
import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.services.CollectionMethodService;
import com.odigeo.frontend.resolvers.payment.mappers.CreditCardBinDetailsMapper;
import com.odigeo.frontend.services.CustomerCreditCardBinSecurityService;
import com.odigeo.frontend.services.PrimeService;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.time.YearMonth;

@Singleton
public class PaymentResolver implements Resolver {

    static final String GET_CREDIT_CARD_BIN_DETAILS_QUERY = "getCreditCardBinDetails";
    static final String CREDIT_CARD_DATE_DETAILS_RESPONSE_QUERY = "creditCardDateDetailsResponse";

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private CollectionMethodService collectionMethodService;
    @Inject
    private CustomerCreditCardBinSecurityService customerCreditCardBinSecurityService;
    @Inject
    private CreditCardBinDetailsMapper mapper;
    @Inject
    private PrimeService primeService;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher(GET_CREDIT_CARD_BIN_DETAILS_QUERY, this::getCreditCardBinDetails))
            .type("Query", typeWiring -> typeWiring
                .dataFetcher(CREDIT_CARD_DATE_DETAILS_RESPONSE_QUERY, this::creditCardDateDetailsResponse));
    }

    CreditCardDateDetailsResponse creditCardDateDetailsResponse(DataFetchingEnvironment env) {
        String creditCardYearMonthAsString = jsonUtils.fromDataFetching(env);
        VisitInformation visitInformation = env.getGraphQlContext().get(VisitInformation.class);
        boolean isValidDateForPrime = isValidDateForPrime(creditCardYearMonthAsString, visitInformation.getSite());

        CreditCardDateDetailsResponse creditCardDateDetailsResponse = new CreditCardDateDetailsResponse();
        creditCardDateDetailsResponse.setIsValidDateForPrime(isValidDateForPrime);
        return creditCardDateDetailsResponse;
    }

    private boolean isValidDateForPrime(String creditCardYearMonthAsString, Site site) {
        YearMonth currentYearMonth = getCurrentYearMonth();
        YearMonth creditCardYearMonth = YearMonth.parse(creditCardYearMonthAsString);
        YearMonth nextYearMonth = currentYearMonth.plusMonths(primeService.getExpiringCreditCardPeriod(site));
        return creditCardYearMonth.isBefore(currentYearMonth) || creditCardYearMonth.isAfter(nextYearMonth);
    }

    @VisibleForTesting
    protected YearMonth getCurrentYearMonth() {
        return YearMonth.now();
    }

    CreditCardBinDetails getCreditCardBinDetails(DataFetchingEnvironment env) {
        String bin = jsonUtils.fromDataFetching(env);
        VisitInformation visit = env.getGraphQlContext().get(VisitInformation.class);
        com.odigeo.collectionmethod.v3.CreditCardBinDetails creditCardBinDetails = collectionMethodService.getCreditCardBinDetails(bin);
        CreditCardBinDetails creditCardDetails = mapper.creditCardBinDetailsContractToModel(creditCardBinDetails);
        creditCardDetails.setIsPrimeBlocking(customerCreditCardBinSecurityService.isCreditCardBlockedForSubscription(bin, visit));
        return creditCardDetails;
    }

}
