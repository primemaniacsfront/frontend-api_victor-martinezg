package com.odigeo.frontend.resolvers.accommodation.summary.models;

import java.util.List;

public class FacilityGroupsDTO extends FacilityDTO {
    protected List<FacilityDTO> facilities;

    public List<FacilityDTO> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<FacilityDTO> facilities) {
        this.facilities = facilities;
    }
}
