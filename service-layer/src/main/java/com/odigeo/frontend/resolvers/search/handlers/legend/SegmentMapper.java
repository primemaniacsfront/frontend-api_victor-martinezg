package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;
import com.odigeo.searchengine.v2.responses.itinerary.Segment;
import com.odigeo.searchengine.v2.responses.itinerary.SegmentResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Singleton
public class SegmentMapper {

    private static final String SECTION_INDEX_SEPARATOR = "_";

    @Inject
    private SectionMapper sectionMapper;
    @Inject
    private TransportHandler transportHandler;

    public Map<Integer, SegmentDTO> buildSegmentMap(SearchEngineContext seContext) {
        return seContext.getSegmentResults().stream()
            .collect(Collectors.toMap(SegmentResult::getId, segmentResult -> buildSegment(segmentResult, seContext)));
    }

    private SegmentDTO buildSegment(SegmentResult segmentResult, SearchEngineContext seContext) {
        Map<Integer, SectionResult> sectionMap = seContext.getSectionResultMap();
        Map<Integer, Carrier> carrierMap = seContext.getCarrierMap();

        Segment engineSegment = segmentResult.getSegment();

        SegmentDTO segmentDTO = new SegmentDTO();
        segmentDTO.setId(segmentResult.getId());
        segmentDTO.setCarrier(carrierMap.get(engineSegment.getCarrier()));
        segmentDTO.setDuration(engineSegment.getDuration());
        segmentDTO.setSeats(engineSegment.getSeats());

        List<SectionDTO> segmentSections = new ArrayList<>(engineSegment.getSections().size());
        int sectionIndex = 0;

        for (Integer sectionResultId : engineSegment.getSections()) {
            SectionResult sectionResult = sectionMap.get(sectionResultId);
            SectionDTO sectionDTO = sectionMapper.buildSection(sectionResult, seContext);

            sectionDTO.setId(sectionResultId + SECTION_INDEX_SEPARATOR + sectionIndex);
            segmentSections.add(sectionDTO);
            sectionIndex++;
        }

        segmentDTO.setSections(segmentSections);
        segmentDTO.setTransportTypes(transportHandler.calculateSegmentTransportTypes(segmentSections));

        return segmentDTO;
    }

}
