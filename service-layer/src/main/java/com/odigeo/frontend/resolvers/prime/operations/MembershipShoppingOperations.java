package com.odigeo.frontend.resolvers.prime.operations;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;

public interface MembershipShoppingOperations {
    FreeTrialCandidateRequest buildFreeTrialCandidateRequest(long shoppingId, ResolverContext context);

    String getBuyerEmail(long shoppingId, ResolverContext context);

    boolean isPrimeUserDiscountApplied(long shoppingId, ResolverContext context);

    MembershipSubscriptionResponse getMembershipSubscriptionResponse(long shoppingId, ResolverContext context);

    MembershipSubscriptionResponse updateMembershipSubscriptionProduct(ModifyMembershipSubscription modifyMembershipSubscription, ResolverContext context);
}
