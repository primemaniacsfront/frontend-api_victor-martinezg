package com.odigeo.frontend.resolvers.search.models;


import org.apache.commons.lang3.StringUtils;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TrackingInfo;

public class TrackingInfoDTO extends TrackingInfo {

    @Override
    public String getReturningUserDeviceId() {
        return super.getReturningUserDeviceId() == null ? StringUtils.EMPTY : super.getReturningUserDeviceId();
    }
}
