package com.odigeo.frontend.resolvers.nonessentialproducts.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryPolicyConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;

@Singleton
public class NonEssentialProductsConfiguration {

    private static final Map<Device, String> FILE_NAMES_BY_FLOW = new EnumMap<>(Device.class);

    private static final String SEPARATOR = FileSystems.getDefault().getSeparator();
    private static final String GENERATED_FILES_PATH = "generated";
    private static final String DESKTOP_CONFIG_JSON = "desktopConfig.json";
    private static final String MOBILE_CONFIG_JSON = "mobileConfig.json";

    static {
        FILE_NAMES_BY_FLOW.put(Device.DESKTOP, DESKTOP_CONFIG_JSON);
        FILE_NAMES_BY_FLOW.put(Device.MOBILE, MOBILE_CONFIG_JSON);
    }

    public AncillaryPolicyConfiguration getConfiguration(VisitInformation visitInformation) throws IOException {
        AncillaryPolicyConfiguration ancillariesGenericConfigurations = null;

        String fileName = getFileName(visitInformation.getDevice());
        InputStream jsonStream = getSiteFileOrDefault(visitInformation.getSite().name(), fileName);

        if (Objects.nonNull(jsonStream)) {
            ancillariesGenericConfigurations = new ObjectMapper().readValue(jsonStream, AncillaryPolicyConfiguration.class);
        }

        return ancillariesGenericConfigurations;
    }

    private InputStream getSiteFileOrDefault(String site, String fileName) {
        String pathToSite = getPathToSite(site, fileName);
        InputStream jsonStream = getResourceAsStream(pathToSite);
        if (Objects.isNull(jsonStream)) {
            String pathToDefault = getPathToDefault(fileName);
            jsonStream = getResourceAsStream(pathToDefault);
        }
        return jsonStream;
    }

    private InputStream getResourceAsStream(String pathToDefault) {
        return NonEssentialProductsConfiguration.class.getResourceAsStream(pathToDefault);
    }

    protected String getPathToDefault(String fileName) {
        return GENERATED_FILES_PATH + SEPARATOR + fileName;
    }

    protected String getPathToSite(String site, String fileName) {
        return GENERATED_FILES_PATH + SEPARATOR + site + SEPARATOR + fileName;
    }

    private String getFileName(Device device) {
        return FILE_NAMES_BY_FLOW.get(device);
    }
}
