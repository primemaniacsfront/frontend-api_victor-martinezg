package com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationShoppingItem;
import org.mapstruct.Mapper;

@Mapper
public interface AccommodationShoppingItemMapper {

    AccommodationShoppingItem map(com.odigeo.dapi.client.AccommodationShoppingItem accommodationShoppingItem);

}
