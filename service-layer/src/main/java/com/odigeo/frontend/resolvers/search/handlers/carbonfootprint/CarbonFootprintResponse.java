package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CarbonFootprint;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

public class CarbonFootprintResponse {

    private final Map<Integer, CarbonFootprint> itineraries;

    private BigDecimal searchKilosAverage;

    public CarbonFootprintResponse() {
        this.itineraries = Collections.EMPTY_MAP;
    }

    public CarbonFootprintResponse(Map<Integer, CarbonFootprint> itineraries, BigDecimal searchKilosAverage) {
        this.itineraries = itineraries;
        this.searchKilosAverage = searchKilosAverage;
    }

    public Map<Integer, CarbonFootprint> getItineraries() {
        return itineraries;
    }

    public BigDecimal getSearchKilosAverage() {
        return searchKilosAverage;
    }
}
