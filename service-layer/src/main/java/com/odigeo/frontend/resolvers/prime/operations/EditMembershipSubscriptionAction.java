package com.odigeo.frontend.resolvers.prime.operations;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;

@Singleton
public class EditMembershipSubscriptionAction implements MembershipSubscriptionShoppingCartAction<ModifyMembershipSubscription, ResolverContext> {
    @Inject
    private RemoveMembershipSubscriptionAction removeMembershipSubscriptionAction;
    @Inject
    private AddMembershipSubscriptionAction addMembershipSubscriptionAction;

    @Override
    public ShoppingCartSummaryResponse apply(ModifyMembershipSubscription modifySubscriptionProduct, ResolverContext context) {
        removeMembershipSubscriptionAction.apply(modifySubscriptionProduct, context);
        return addMembershipSubscriptionAction.apply(modifySubscriptionProduct, context);
    }
}
