package com.odigeo.frontend.resolvers.prime;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeInfo;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.prime.handlers.PrimeInfoHandler;
import com.odigeo.frontend.resolvers.prime.handlers.RetentionFlowInfoHandler;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowInfo;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

@Singleton
public class PrimeResolver implements Resolver {
    private static final String PRIME_INFO_QUERY = "primeInfo";
    private static final String RETENTION_INFO_FLOW_QUERY = "retentionInfo";

    @Inject
    private PrimeInfoHandler primeInfoHandler;
    @Inject
    RetentionFlowInfoHandler retentionFlowInfoHandler;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher(PRIME_INFO_QUERY, this::primeInfoFetcher)
                .dataFetcher(RETENTION_INFO_FLOW_QUERY, this::retentionFlowInfoFetcher));
    }

    PrimeInfo primeInfoFetcher(DataFetchingEnvironment env) {
        return primeInfoHandler.buildPrimeInfo(env.getContext());
    }

    RetentionFlowInfo retentionFlowInfoFetcher(DataFetchingEnvironment env) {
        return retentionFlowInfoHandler.buildRetentionFlowInfo(env);
    }
}
