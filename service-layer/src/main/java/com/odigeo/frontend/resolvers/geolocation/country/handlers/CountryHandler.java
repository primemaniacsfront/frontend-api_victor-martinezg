package com.odigeo.frontend.resolvers.geolocation.country.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetCountriesRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetCountriesResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.geolocation.country.mappers.CountryMapper;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.GeoServiceException;
import org.apache.commons.lang.LocaleUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class CountryHandler {

    @Inject
    private GeoService geoService;

    @Inject
    private CountryMapper countryMapper;


    public GetCountriesResponse getCountries(GetCountriesRequest getCountriesRequest) throws GeoServiceException {

        List<com.odigeo.geoapi.v4.responses.Country> countries = geoService.getCountries();
        List<Country> mappedCountries = countries.stream().map(country -> countryMapper.mapModelToContract(country,
                LocaleUtils.toLocale(getCountriesRequest.getLocale()))).sorted(
                Comparator.comparing(Country::getName)
        ).collect(Collectors.toList());

        GetCountriesResponse getCountriesResponse = new GetCountriesResponse();
        getCountriesResponse.setCountries(mappedCountries);
        return getCountriesResponse;
    }
}
