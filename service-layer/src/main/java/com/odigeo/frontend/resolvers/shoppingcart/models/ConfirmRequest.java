package com.odigeo.frontend.resolvers.shoppingcart.models;

import com.odigeo.dapi.client.FraudInfoRequest;
import com.odigeo.dapi.client.InvoiceRequest;
import com.odigeo.dapi.client.PaymentDataRequest;

public class ConfirmRequest {

    private PaymentDataRequest paymentData;
    private long bookingId;
    private String clientBookingReferencesId;
    private InvoiceRequest invoice;
    private FraudInfoRequest fraudInfo;

    public PaymentDataRequest getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(PaymentDataRequest paymentData) {
        this.paymentData = paymentData;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public String getClientBookingReferencesId() {
        return clientBookingReferencesId;
    }

    public void setClientBookingReferencesId(String clientBookingReferencesId) {
        this.clientBookingReferencesId = clientBookingReferencesId;
    }

    public InvoiceRequest getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceRequest invoice) {
        this.invoice = invoice;
    }

    public FraudInfoRequest getFraudInfo() {
        return fraudInfo;
    }

    public void setFraudInfo(FraudInfoRequest fraudInfo) {
        this.fraudInfo = fraudInfo;
    }
}
