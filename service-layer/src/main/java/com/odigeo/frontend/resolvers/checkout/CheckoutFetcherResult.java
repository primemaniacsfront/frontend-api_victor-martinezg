package com.odigeo.frontend.resolvers.checkout;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import graphql.GraphQLError;

import java.util.List;

public class CheckoutFetcherResult {

    private CheckoutResponse data;
    private List<GraphQLError> errors;

    public CheckoutResponse getData() {
        return data;
    }

    public void setData(CheckoutResponse data) {
        this.data = data;
    }

    public List<GraphQLError> getErrors() {
        return errors;
    }

    public void setErrors(List<GraphQLError> errors) {
        this.errors = errors;
    }
}
