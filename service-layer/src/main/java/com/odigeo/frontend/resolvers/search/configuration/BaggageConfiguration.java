package com.odigeo.frontend.resolvers.search.configuration;

import com.edreams.configuration.ConfiguredInJsonFile;

import javax.inject.Singleton;
import java.util.List;

@Singleton
@ConfiguredInJsonFile
public class BaggageConfiguration {

    private List<String> airlinesWithSmallBagInCabin;

    private String renfeCode;

    public List<String> getAirlinesWithSmallBagInCabin() {
        return airlinesWithSmallBagInCabin;
    }

    public void setAirlinesWithSmallBagInCabin(List<String> airlinesWithSmallBagInCabin) {
        this.airlinesWithSmallBagInCabin = airlinesWithSmallBagInCabin;
    }

    public String getRenfeCode() {
        return renfeCode;
    }

    public void setRenfeCode(String renfeCode) {
        this.renfeCode = renfeCode;
    }
}
