package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceUrl;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.insurance.api.last.insurance.ConditionsUrl;
import com.odigeo.insurance.api.last.insurance.InsuranceResponse;
import com.odigeo.insurance.api.last.insurance.offer.SimpleInsuranceOffer;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Singleton
public class InsuranceOfferMapper {

    @Inject
    private MoneyMapper moneyMapper;
    @Inject
    private InsuranceOfferIdMapper insuranceOfferIdMapper;
    @Inject
    private InsuranceUrlsMapper insuranceUrlsMapper;

    public InsuranceOfferType map(SimpleInsuranceOffer simpleInsuranceOffer) {
        InsuranceOfferType insuranceOfferType = new InsuranceOfferType();

        insuranceOfferType.setOfferId(insuranceOfferIdMapper.map(simpleInsuranceOffer.getOfferId()));
        insuranceOfferType.setOfferKey(simpleInsuranceOffer.getInsurance().getPolicy());
        insuranceOfferType.setPrice(moneyMapper.map(simpleInsuranceOffer.getTotalPrice()));
        insuranceOfferType.setConditionUrls(mapConditionUrls(simpleInsuranceOffer));

        return insuranceOfferType;
    }

    private List<InsuranceUrl> mapConditionUrls(SimpleInsuranceOffer insuranceOffer) {
        List<ConditionsUrl> conditionsUrls = Optional.ofNullable(insuranceOffer.getInsurance())
                .map(InsuranceResponse::getConditionsUrls)
                .orElse(Collections.emptyList());

        return insuranceUrlsMapper.map(conditionsUrls);
    }

}
