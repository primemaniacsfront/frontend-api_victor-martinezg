package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchCode;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.onefront.OFShoppingCartIntegration;
import com.odigeo.frontend.resolvers.search.handlers.campaign.CampaignConfigHandler;
import com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering.AirlineSteeringHandler;
import com.odigeo.frontend.resolvers.search.handlers.farefamily.FareFamilyHandler;
import com.odigeo.frontend.resolvers.search.handlers.helper.MirFootprintItineraryHandler;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.frontend.resolvers.search.handlers.legend.SegmentMapper;
import com.odigeo.frontend.resolvers.search.handlers.legend.TransportHandler;
import com.odigeo.frontend.resolvers.search.handlers.pricing.AbstractPricePolicy;
import com.odigeo.frontend.resolvers.search.handlers.pricing.PerksHandler;
import com.odigeo.frontend.resolvers.search.handlers.pricing.PriceHandler;
import com.odigeo.frontend.resolvers.search.handlers.pricing.PricePolicyFactory;
import com.odigeo.frontend.resolvers.search.handlers.vin.VinHandler;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerarySearchResults;
import org.apache.commons.collections4.CollectionUtils;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class SearchResponseHandler {

    private static final String ITINERARY_INDEX_SEPARATOR = ",";
    private static final String ITINERARY_UNIQUE_INDEX_SEPARATOR = "_";

    @Inject
    private BaggageHandler baggageHandler;
    @Inject
    private TicketHandler ticketHandler;
    @Inject
    private PriceHandler priceHandler;
    @Inject
    private PricePolicyFactory pricePolicyFactory;
    @Inject
    private OFShoppingCartIntegration ofShoppingCartIntegration;
    @Inject
    private HotelXSellingHandler hotelXSellingHandler;
    @Inject
    private SearchEngineContextBuilder seContextBuilder;
    @Inject
    private SegmentMapper segmentMapper;
    @Inject
    private RebookingHandler rebookingHandler;
    @Inject
    private VinHandler vinHandler;
    @Inject
    private FareFamilyHandler fareFamilyHandler;
    @Inject
    private SearchService searchService;
    @Inject
    private TransportHandler transportHandler;
    @Inject
    private PerksHandler perksHandler;
    @Inject
    private MetasHandler metasHandler;
    @Inject
    private MirFootprintItineraryHandler mirFootprintItineraryHandler;
    @Inject
    private AirlineSteeringHandler airlineSteeringHandler;
    @Inject
    private CampaignConfigHandler campaignConfigHandler;


    public SearchResponseDTO buildSearchResponse(SearchResponse searchResponse,
                                                 SearchRequest searchRequestDTO, ResolverContext context) {

        SearchResponseDTO searchResponseDTO = new SearchResponseDTO();
        ItinerarySearchResults itinerarySearchResults = searchResponse.getItinerarySearchResults();
        VisitInformation visit = context.getVisitInformation();

        List<SearchItineraryDTO> itineraryDTOList = Collections.emptyList();
        String currency = null;
        FeeType defaultFeeType = null;
        AbstractPricePolicy pricePolicy = pricePolicyFactory.getPricePolicy(context);

        if (itinerarySearchResults != null) {
            SearchEngineContext seContext = seContextBuilder.build(itinerarySearchResults);
            itineraryDTOList = buildItineraryResponse(seContext, searchRequestDTO, visit, pricePolicy);
            currency = seContext.getCurrency();
            defaultFeeType = pricePolicy.calculateDefaultFeeType();
        }

        searchResponseDTO.setSearchId(searchResponse.getSearchId());
        searchResponseDTO.setItineraries(itineraryDTOList);
        searchResponseDTO.setCurrencyCode(currency);
        searchResponseDTO.setDefaultFeeType(defaultFeeType);
        searchResponseDTO.setIsRebookingAvailable(rebookingHandler.isRebookingAvailable(searchRequestDTO, visit));
        searchResponseDTO.setCarriersWithRebooking(rebookingHandler.getCarriersWithRebooking(searchRequestDTO, visit));

        populateSearchCode(searchResponse, searchResponseDTO);

        mirFootprintItineraryHandler.addMirRatingAndFootprintInfoItinerary(searchRequestDTO, searchResponseDTO, context);
        airlineSteeringHandler.boostingAirline(searchResponseDTO, context);
        metasHandler.populateExternalSelection(searchResponse, searchResponseDTO);

        return searchResponseDTO;
    }

    private void populateSearchCode(SearchResponse searchResponse, SearchResponseDTO searchResponseDTO) {
        Optional.ofNullable(searchService.getSearchMessage(searchResponse))
                .ifPresent(searchMessage -> {
                    SearchCode searchCode = new SearchCode();
                    searchCode.setCode(searchMessage.getCode());
                    searchCode.setMessage(searchMessage.getDescription());

                    searchResponseDTO.setSearchCode(searchCode);
                });
    }

    private List<SearchItineraryDTO> buildItineraryResponse(SearchEngineContext seContext, SearchRequest searchRequest,
                                                            VisitInformation visit, AbstractPricePolicy pricePolicy) {

        List<FareItinerary> itineraries = seContext.getItineraryResults();
        List<SearchItineraryDTO> itineraryDTOList = new ArrayList<>(itineraries.size());

        Integer numPassengers = calculateNumPassengers(searchRequest);
        Map<Integer, SegmentDTO> segmentMap = segmentMapper.buildSegmentMap(seContext);

        itineraries.forEach(fareItinerary -> {
            Integer itineraryIndex = 0;
            Integer fareItineraryIndex = decodeFareItineraryIndex(fareItinerary);

            Map<Integer, Integer> segmentKeys = ofShoppingCartIntegration.buildSegmentKeysMap(fareItinerary);
            ZonedDateTime cancellationLimit = Optional.ofNullable(fareItinerary.getFreeCancellationLimit())
                    .map(seContext.getFreeCancellationMap()::get)
                    .orElse(null);

            Set<List<SegmentDTO>> cartesianProduct = splitOneSegmentPerLeg(fareItinerary, segmentMap);

            for (List<SegmentDTO> segmentDTOList : cartesianProduct) {
                SearchItineraryDTO itinerary = new SearchItineraryDTO();
                List<LegDTO> itineraryLegs = buildLegs(segmentDTOList, segmentKeys);

                itinerary.setLegs(itineraryLegs);

                itinerary.setId(encodeItineraryIndex(fareItinerary, itineraryIndex));
                itinerary.setKey(fareItinerary.getKey());
                itinerary.setItinerariesLink(fareItinerary.getItinerary());
                itinerary.setIndex(fareItineraryIndex);
                itinerary.setFreeCancellation(cancellationLimit);
                itinerary.setSortPrice(fareItinerary.getPrice().getSortPrice());
                itinerary.setTicketsLeft(ticketHandler.calculateSeatsLeft(itineraryLegs));
                itinerary.setFees(priceHandler.calculateItineraryPrices(seContext, fareItinerary, BigDecimal.valueOf(numPassengers), pricePolicy));
                itinerary.setTransportTypes(transportHandler.calculateItineraryTransportTypes(itineraryLegs));
                itinerary.setPerks(perksHandler.populatePerks(seContext, fareItinerary, numPassengers));
                itinerary.setCampaignConfig(campaignConfigHandler.populateCampaignConfig(itinerary, visit));
                itinerary.setVin(Boolean.FALSE);

                rebookingHandler.populateHasFreeRebooking(itinerary, searchRequest, visit);
                rebookingHandler.populateHasReliableCarriers(itinerary);
                rebookingHandler.populateHasRefundCarriers(itinerary);
                fareFamilyHandler.populateIsAvailable(itinerary, visit);
                hotelXSellingHandler.populateIsEnabled(itinerary, searchRequest, visit);
                baggageHandler.populateItineraryBaggageConditions(seContext, fareItinerary, itineraryLegs);

                itineraryDTOList.add(itinerary);
                itineraryIndex++;
            }
        });

        baggageHandler.populateSegmentBaggageConditions(segmentMap);
        vinHandler.populateVinInfo(itineraryDTOList, seContext);

        return itineraryDTOList;
    }

    private List<LegDTO> buildLegs(List<SegmentDTO> segmentDTOList, Map<Integer, Integer> segmentKeys) {
        return segmentDTOList.stream()
                .map(segmentDTO -> {
                    LegDTO legDTO = new LegDTO();
                    legDTO.setSegments(Collections.singletonList(segmentDTO));
                    legDTO.setSegmentKeys(Collections.singletonList(segmentKeys.get(segmentDTO.getId())));

                    return legDTO;
                }).collect(Collectors.toList());
    }

    private Set<List<SegmentDTO>> splitOneSegmentPerLeg(FareItinerary fareItinerary, Map<Integer, SegmentDTO> segmentMap) {
        List<Set<SegmentDTO>> interimList = Stream.of(
                fareItinerary.getFirstSegments(),
                fareItinerary.getSecondSegments(),
                fareItinerary.getThirdSegments(),
                fareItinerary.getFourthSegments(),
                fareItinerary.getFifthSegments(),
                fareItinerary.getSixthSegments())
                .filter(CollectionUtils::isNotEmpty)
                .map(segmentIds -> segmentIds.stream().map(segmentMap::get).collect(Collectors.toSet()))
                .collect(Collectors.toList());

        return Sets.cartesianProduct(interimList);
    }

    private Integer decodeFareItineraryIndex(FareItinerary fareItinerary) {
        return Integer.valueOf(fareItinerary.getKey().split(ITINERARY_INDEX_SEPARATOR)[0]);
    }

    private String encodeItineraryIndex(FareItinerary fareItinerary, Integer index) {
        return fareItinerary.getKey() + ITINERARY_UNIQUE_INDEX_SEPARATOR + index;
    }

    private Integer calculateNumPassengers(SearchRequest searchRequestDTO) {
        ItineraryRequest itineraryRequestDTO = searchRequestDTO.getItinerary();
        return itineraryRequestDTO.getNumAdults()
                + itineraryRequestDTO.getNumChildren()
                + itineraryRequestDTO.getNumInfants();
    }

}
