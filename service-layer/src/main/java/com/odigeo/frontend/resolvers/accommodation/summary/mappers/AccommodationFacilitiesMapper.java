package com.odigeo.frontend.resolvers.accommodation.summary.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationFacilityDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationFacilityGroupDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.FacilityDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.FacilityGroupsDTO;
import com.odigeo.hcsapi.v9.beans.FacilityInfo;
import com.odigeo.hcsapi.v9.beans.Legend;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationFacility;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationFacilityGroup;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class AccommodationFacilitiesMapper {

    public AccommodationFacilityDTO mapHcsFacility(String facilityCode, Legend legend) {
        AccommodationFacilityDTO facility = null;
        if (StringUtils.isNotEmpty(facilityCode)) {
            facility = Optional.ofNullable(legend)
                .map(Legend::getFacilityLegend)
                .map(facilityLegend -> facilityLegend.get(facilityCode))
                .map(facilityInfo -> mapAccommodationFacility(facilityCode, facilityInfo, legend))
                .orElse(null);
        }
        return facility;
    }

    private AccommodationFacilityDTO mapAccommodationFacility(String facilityCode, FacilityInfo facilityInfo, Legend legend) {
        AccommodationFacilityDTO facility = new AccommodationFacilityDTO();
        facility.setCode(facilityCode);
        facility.setDescription(facilityInfo.getText());
        facility.setFacilityGroup(new AccommodationFacilityGroupDTO());
        facility.getFacilityGroup().setCode(facilityInfo.getGroupCode());
        facility.getFacilityGroup().setDescription(
            legend.getFacilitiesGroupLegend() != null ? legend.getFacilitiesGroupLegend().get(facilityInfo.getGroupCode()) : null
        );
        return facility;
    }


    public List<FacilityGroupsDTO> mapFacilityGroups(List<AccommodationFacility> accommodationFacilities) {
        Map<String, FacilityGroupsDTO> facilityGroupsMap = new HashMap<>();
        Optional.ofNullable(accommodationFacilities)
            .map(Collection::stream)
            .orElseGet(Stream::empty)
            .filter(facility -> StringUtils.isNotBlank(facility.getDescription()))
            .forEach(facility -> mapFacilityDTO(facilityGroupsMap, facility));

        return Optional.of(facilityGroupsMap)
            .map(Map::values)
            .map(ArrayList::new)
            .orElseGet(ArrayList::new);
    }

    public List<FacilityGroupsDTO> mapFacilityGroups(List<String> facilities, Legend legend) {
        Map<String, FacilityGroupsDTO> facilityGroupsMap = buildFacilityMap(legend);

        Optional.ofNullable(facilities)
            .orElseGet(Collections::emptyList)
            .forEach(facility -> mapHcsFacility(facilityGroupsMap, facility, legend));

        return cleanFacilitiesGroup(facilityGroupsMap);
    }

    private void mapHcsFacility(Map<String, FacilityGroupsDTO> facilityGroupsMap, String facilityCode, Legend legend) {
        Optional.ofNullable(legend)
            .map(Legend::getFacilityLegend)
            .map(facilityLegend -> facilityLegend.get(facilityCode))
            .filter(facility -> StringUtils.isNotBlank(facility.getGroupCode()) && StringUtils.isNotBlank(facility.getText()))
            .ifPresent(facility -> {
                if (facilityGroupsMap.containsKey(facility.getGroupCode())) {
                    facilityGroupsMap.get(facility.getGroupCode()).getFacilities().add(mapFacility(facilityCode, facility));
                }
            });

    }

    private List<FacilityGroupsDTO> cleanFacilitiesGroup(Map<String, FacilityGroupsDTO> facilityGroupsMap) {
        return Optional.ofNullable(facilityGroupsMap)
            .map(Map::values)
            .orElseGet(Collections::emptyList)
            .stream()
            .filter(v -> v != null && CollectionUtils.isNotEmpty(v.getFacilities()))
            .collect(Collectors.toList());
    }

    private Map<String, FacilityGroupsDTO> buildFacilityMap(Legend legend) {
        Map<String, FacilityGroupsDTO> facilityGroupsMap = new HashMap<>();
        Optional.ofNullable(legend)
            .map(Legend::getFacilitiesGroupLegend)
            .orElseGet(Collections::emptyMap)
            .forEach((k, v) -> {
                FacilityGroupsDTO group = new FacilityGroupsDTO();
                group.setCode(k);
                group.setDescription(v);
                group.setFacilities(new ArrayList<>());
                facilityGroupsMap.put(k, group);
            });
        return facilityGroupsMap;
    }

    private FacilityDTO mapFacility(String facilityCode, FacilityInfo facilityInfo) {
        FacilityDTO facility = new FacilityDTO();
        facility.setCode(facilityCode);
        facility.setDescription(facilityInfo.getText());
        return facility;
    }

    private void mapFacilityDTO(Map<String, FacilityGroupsDTO> facilityGroupsMap, AccommodationFacility facility) {
        AccommodationFacilityGroup facilityGroup = facility.getFacilityGroup();
        FacilityDTO facilityDTO = mapFacility(facility);
        if (facilityDTO != null && facilityGroup != null && facilityGroupsMap.containsKey(facilityGroup.getCode())) {
            facilityGroupsMap.get(facilityGroup.getCode()).getFacilities().add(facilityDTO);
        } else if (facilityDTO != null && facilityGroup != null && StringUtils.isNotBlank(facilityGroup.getCode())) {
            FacilityGroupsDTO facilityGroupsDTO = mapFacilityGroupsDTO(facilityGroup);
            facilityGroupsDTO.getFacilities().add(facilityDTO);
            facilityGroupsMap.put(facilityGroup.getCode(), facilityGroupsDTO);
        }
    }

    private FacilityGroupsDTO mapFacilityGroupsDTO(AccommodationFacilityGroup facilityGroup) {
        FacilityGroupsDTO facilityGroups = new FacilityGroupsDTO();
        facilityGroups.setFacilities(new ArrayList<>());
        facilityGroups.setCode(facilityGroup.getCode());
        facilityGroups.setDescription(facilityGroup.getDescription());
        return facilityGroups;
    }

    private FacilityDTO mapFacility(AccommodationFacility facility) {
        FacilityDTO dto = null;
        if (StringUtils.isNotBlank(facility.getDescription())) {
            dto = new FacilityGroupsDTO();
            dto.setDescription(facility.getDescription());
            dto.setCode(facility.getCode());
        }
        return dto;
    }
}
