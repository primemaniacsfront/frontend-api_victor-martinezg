package com.odigeo.frontend.resolvers.accommodation.maps;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationStaticMapRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapStaticConfiguration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.StaticMapResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.GoogleMapsUtils;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationDataHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.HotelDetailsRequestDTO;
import com.odigeo.frontend.services.configurations.maps.AccommodationMapsConfiguration;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.hcsapi.v9.beans.HotelSummaryResponse;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.io.IOException;
import java.util.Base64;
import java.util.Collections;
import java.util.Optional;

@Singleton
public class MapsResolver implements Resolver {

    static final String STATIC_MAP = "accommodationStaticMap";
    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private GoogleMapsUtils googleMapsUtils;
    @Inject
    private AccommodationMapsConfiguration mapsConfiguration;
    @Inject
    private ContentKeyMapper contentKeyMapper;
    @Inject
    private AccommodationDataHandler accommodationDataHandler;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher(STATIC_MAP, this::staticMapFetcher));
    }

    StaticMapResponse staticMapFetcher(DataFetchingEnvironment env) {
        AccommodationStaticMapRequest request = jsonUtils.fromDataFetching(env, AccommodationStaticMapRequest.class);
        VisitInformation visit = env.getGraphQlContext().get(VisitInformation.class);
        HotelSummary hotelSummary = getHotelSummary(request.getContentKey(), visit);
        return Optional.ofNullable(hotelSummary)
            .filter(summary -> summary.getLongitude() != null && summary.getLatitude() != null)
            .map(summary -> getImage(request.getStaticMapConfiguration(), (double) summary.getLongitude(), (double) summary.getLatitude()))
            .map(this::getStaticMapResponse)
            .orElse(null);
    }

    private StaticMapResponse getStaticMapResponse(byte[] image) {
        StaticMapResponse mapStaticResponse = new StaticMapResponse();
        mapStaticResponse.setImage(Base64.getEncoder().encodeToString(image));
        return mapStaticResponse;
    }

    private HotelSummary getHotelSummary(String contentKey, VisitInformation visit) {
        AccommodationProviderKeyDTO providerKey = contentKeyMapper.extractProviderKeyFromContentKey(contentKey);
        HotelDetailsRequestDTO hotelDetailsRequestDTO = new HotelDetailsRequestDTO();
        hotelDetailsRequestDTO.setProviderKeys(Collections.singletonList(providerKey));
        HotelSummaryResponse summaryResponse = accommodationDataHandler.getAccommodationSummary(hotelDetailsRequestDTO, visit);
        HotelSupplierKey key = new HotelSupplierKey(providerKey.getAccommodationProviderId(), providerKey.getProviderId());
        return Optional.ofNullable(summaryResponse).map(HotelSummaryResponse::getHotelSummary).map(summary -> summary.get(key)).orElse(null);
    }

    private byte[] getImage(MapStaticConfiguration mapStaticConfiguration, double longitude, double latitude) {
        try {
            GeoApiContext context = googleMapsUtils
                .createContext(mapsConfiguration.getApiKey(), mapsConfiguration.getSecretKey(), mapsConfiguration.getClientId());
            return googleMapsUtils.getStaticMap(mapStaticConfiguration, longitude, latitude, context);
        } catch (ApiException | InterruptedException | IOException e) {
            throw new ServiceException("Could not retrieve image from google service for the request config", e, ServiceName.FRONTEND_API);
        }
    }

}
