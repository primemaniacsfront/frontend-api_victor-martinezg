package com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationBuyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateAccommodationProductResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Guest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.BuyerRequest;
import com.odigeo.dapi.client.PersonalInfoRequest;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.dapi.client.TravellerRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.CreateProductAccommodationHandler;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;

import java.util.List;

@Singleton
public class ShoppingCartCreateProductAccommodation implements CreateProductAccommodationHandler {

    @Inject
    private ShoppingCartHandler shoppingCartHandler;
    @Inject
    private AccommodationGuestsHandler accommodationGuestsHandler;

    @Override
    public CreateAccommodationProductResponse createProductAccommodation(String dealId, List<Guest> guestsRequest, AccommodationBuyer buyer, ResolverContext resolverContext) {
        List<Guest> guests = accommodationGuestsHandler.buildGuests(guestsRequest, parseLong(dealId), resolverContext, resolverContext.getVisitInformation().getVisitCode());
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = shoppingCartHandler.addPersonalInfo(createPersonalInfoRequest(dealId, guests, buyer), resolverContext);
        return buildResponse(shoppingCartSummaryResponse);
    }

    private PersonalInfoRequest createPersonalInfoRequest(String bookingId, List<Guest> guests, AccommodationBuyer buyerInfoDTO) {
        PersonalInfoRequest personalInfoRequest = new PersonalInfoRequest();
        personalInfoRequest.setBuyer(createBuyerRequest(buyerInfoDTO));
        personalInfoRequest.setBookingId(Long.parseLong(bookingId));
        guests.forEach(guest -> personalInfoRequest.getTravellerRequests().add(createTravellerRequest(guest)));
        return personalInfoRequest;

    }

    private BuyerRequest createBuyerRequest(AccommodationBuyer buyer) {
        BuyerRequest buyerRequest = new BuyerRequest();

        buyerRequest.setName(buyer.getName());
        buyerRequest.setLastNames(buyer.getLastNames());
        buyerRequest.setMail(buyer.getMail());
        buyerRequest.setBuyerIdentificationTypeName(buyer.getBuyerIdentificationTypeName());
        buyerRequest.setIdentification(buyer.getIdentification());
        buyerRequest.setDayOfBirth(buyer.getDayOfBirth());
        buyerRequest.setMonthOfBirth(buyer.getMonthOfBirth());
        buyerRequest.setYearOfBirth(buyer.getYearOfBirth());
        buyerRequest.setAddress(buyer.getAddress());
        buyerRequest.setCityName(buyer.getCityName());
        buyerRequest.setStateName(buyer.getStateName());
        buyerRequest.setZipCode(buyer.getZipCode());
        buyerRequest.setPhoneNumber1(buyer.getPhoneNumber());
        buyerRequest.setCountryPhoneNumber1(buyer.getCountryPhoneNumber());
        buyerRequest.setCountryCode(buyer.getCountryCode());

        return buyerRequest;
    }

    private TravellerRequest createTravellerRequest(Guest guest) {
        TravellerRequest travellerRequest = new TravellerRequest();
        travellerRequest.setTravellerTypeName(guest.getTravellerTypeName());
        travellerRequest.setTitleName(guest.getTitleName());
        travellerRequest.setName(guest.getName());
        travellerRequest.setFirstLastName(guest.getFirstLastName());
        travellerRequest.setSecondLastName(guest.getSecondLastName());
        travellerRequest.setMiddleName(guest.getMiddleName());
        travellerRequest.setGender(guest.getGender());
        travellerRequest.setNationalityCountryCode(guest.getNationalityCountryCode());
        travellerRequest.setCountryCodeOfResidence(guest.getCountryCodeOfResidence());
        travellerRequest.setIdentification(guest.getIdentification());
        travellerRequest.setIdentificationTypeName(guest.getIdentificationTypeName());
        travellerRequest.setIdentificationExpirationDay(guest.getIdentificationExpirationDay());
        travellerRequest.setIdentificationExpirationMonth(guest.getIdentificationExpirationMonth());
        travellerRequest.setIdentificationExpirationYear(guest.getIdentificationExpirationYear());
        travellerRequest.setIdentificationIssueCountryCode(guest.getIdentificationIssueCountryCode());
        travellerRequest.setDayOfBirth(guest.getDayOfBirth());
        travellerRequest.setMonthOfBirth(guest.getMonthOfBirth());
        travellerRequest.setYearOfBirth(guest.getYearOfBirth());
        travellerRequest.setMealName(guest.getMealName());
        travellerRequest.setLocalityCodeOfResidence(guest.getLocalityCodeOfResidence());
        return travellerRequest;
    }

    private CreateAccommodationProductResponse buildResponse(ShoppingCartSummaryResponse shoppingCartSummaryResponse) {
        CreateAccommodationProductResponse response = new CreateAccommodationProductResponse();
        AccommodationProduct accommodationProduct = new AccommodationProduct();
        accommodationProduct.setProductId(String.valueOf(shoppingCartSummaryResponse.getShoppingCart().getAccommodationShoppingItem().getId()));
        response.setAccommodationProduct(accommodationProduct);
        return response;
    }

    private long parseLong(String value) {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            throw new ServiceException("Invalid dealId", e, ServiceName.FRONTEND_API);
        }
    }
}
