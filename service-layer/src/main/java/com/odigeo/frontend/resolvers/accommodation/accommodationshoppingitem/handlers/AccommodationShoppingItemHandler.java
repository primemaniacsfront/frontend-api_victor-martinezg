package com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationShoppingItem;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.mappers.AccommodationShoppingItemMapper;
import graphql.GraphQLContext;

import java.util.Optional;

@Singleton
public class AccommodationShoppingItemHandler {

    @Inject
    private AccommodationShoppingItemMapper mapper;

    public AccommodationShoppingItem map(GraphQLContext graphQLContext) {
        com.odigeo.dapi.client.AccommodationShoppingItem shoppingItem = getShoppingItem(graphQLContext);
        return mapper.map(shoppingItem);
    }

    private com.odigeo.dapi.client.AccommodationShoppingItem getShoppingItem(GraphQLContext graphQLContext) {
        return Optional.ofNullable(graphQLContext)
                .map(context -> context.get(ShoppingCartSummaryResponse.class))
                .map(ShoppingCartSummaryResponse.class::cast)
                .map(ShoppingCartSummaryResponse::getShoppingCart)
                .map(ShoppingCart::getAccommodationShoppingItem)
                .orElse(null);
    }
}
