package com.odigeo.frontend.resolvers.search.handlers;

import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.prime.utils.MembershipUtils;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.frontend.services.PrimeService;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.User;
import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class PrimeHandler {

    @Inject
    private UserDescriptionService userService;
    @Inject
    private PrimeService primeService;
    @Inject
    private MembershipHandler membershipHandler;

    public boolean isPrimeMarket(ResolverContext context) {
        VisitInformation visit = context.getVisitInformation();
        return primeService.isPrimeSite(visit)
            || isPrimeUser(context);
    }

    private boolean isPrimeUser(ResolverContext context) {
        User user = userService.getUserByToken(context);
        return Optional.ofNullable(membershipHandler.getMembership(user, context.getVisitInformation()))
                .map(MembershipUtils::isActivated)
                .orElse(Boolean.FALSE);
    }

}
