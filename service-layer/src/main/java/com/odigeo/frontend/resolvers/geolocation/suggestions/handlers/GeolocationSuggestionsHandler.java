package com.odigeo.frontend.resolvers.geolocation.suggestions.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.geolocation.suggestions.mappers.GeolocationSuggestionsMapper;
import com.odigeo.frontend.resolvers.geolocation.suggestions.models.GeolocationSuggestionsRequestDTO;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.request.ProductType;
import com.odigeo.geoapi.v4.responses.LocationDescriptionSorted;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class GeolocationSuggestionsHandler {

    private static final boolean INCLUDE_MULTI_LANGUAGE = true;
    private static final boolean INCLUDE_SEARCH_IN_ALL_WORDS = true;
    private static final String GEO_NODE_TYPE_CITY = "CITY";

    @Inject
    private GeoService geoService;
    @Inject
    private GeolocationSuggestionsMapper geolocationSuggestionsMapper;

    public GeolocationSuggestionsRequestDTO buildGeolocationSuggestionsRequest(GeolocationSuggestionsRequest geolocationSuggestionsRequest, VisitInformation visit) {

        GeolocationSuggestionsRequestDTO request = new GeolocationSuggestionsRequestDTO();

        request.setSearchWord(geolocationSuggestionsRequest.getSearchWord());
        request.setLocale(visit.getLocale());
        request.setProductType(ProductType.valueOf(geolocationSuggestionsRequest.getProductType().name()));
        request.setWebsite(visit.getSite().name());
        request.setInputType(geolocationSuggestionsRequest.getInputType().name());
        request.setIncludeMultiLanguage(INCLUDE_MULTI_LANGUAGE);
        request.setIncludeRelatedLocations(geolocationSuggestionsRequest.getIncludeRelatedLocations());
        request.setIncludeSearchInAllWords(INCLUDE_SEARCH_IN_ALL_WORDS);
        request.setIncludeCountry(geolocationSuggestionsRequest.getIncludeCountry());
        request.setIncludeRegion(geolocationSuggestionsRequest.getIncludeRegion());
        request.setIncludeNearestLocations(geolocationSuggestionsRequest.getIncludeNearestLocations());
        request.setIncludeCitiesOnly(includeCitiesOnly(geolocationSuggestionsRequest));

        return request;
    }

    public GeolocationSuggestionsResponse getGeolocationSuggestions(GeolocationSuggestionsRequestDTO geolocationSuggestionsRequestDTO) {

        GeolocationSuggestionsResponse geolocationSuggestionsResponse = new GeolocationSuggestionsResponse();

        List<LocationDescriptionSorted> geolocationSuggestions = geoService.getGeolocationSuggestions(geolocationSuggestionsRequestDTO);

        if (geolocationSuggestionsRequestDTO.getIncludeCitiesOnly()) {
            geolocationSuggestions = getGeolocationSuggestionsCitiesOnly(geolocationSuggestions);
        }

        geolocationSuggestionsResponse.setGeolocationSuggestions(geolocationSuggestionsMapper.map(geolocationSuggestions));

        return geolocationSuggestionsResponse;
    }

    private boolean includeCitiesOnly(GeolocationSuggestionsRequest geolocationSuggestionsRequest) {
        return Optional.ofNullable(geolocationSuggestionsRequest.getIncludeCitiesOnly()).orElse(false);
    }

    private List<LocationDescriptionSorted> getGeolocationSuggestionsCitiesOnly(List<LocationDescriptionSorted> geolocationSuggestions) {
        return Optional.ofNullable(geolocationSuggestions).orElse(Collections.emptyList()).stream()
                .filter(geolocationSuggestion -> geolocationSuggestion.getGeoNodeType().equals(GEO_NODE_TYPE_CITY)
                        && geolocationSuggestion.getIataCode() != null && !geolocationSuggestion.getIataCode().isEmpty())
                .collect(Collectors.toList());
    }
}
