package com.odigeo.frontend.resolvers.shoppingbasket.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddContactDetailsToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddProductToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RemoveProductFromShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketResponse;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.insurance.handlers.InsuranceProductHandler;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.dapi.ShoppingCartAvailableCollectionMethods;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.CreateRequestMapper;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ProductRequestMapper;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ShoppingBasketResponseMapper;
import com.odigeo.frontend.services.shoppingbasket.ShoppingBasketV2Service;
import com.odigeo.shoppingbasket.v2.requests.CreateRequest;
import com.odigeo.shoppingbasket.v2.requests.ProductRequest;
import org.jboss.util.NotImplementedException;

import java.util.Collections;
import java.util.Optional;

public class ShoppingBasketV2Handler implements ShoppingBasketOperations {

    private static final String INITIAL_PRODUCT_STATUS = "INIT";

    @Inject
    private ShoppingBasketV2Service shoppingBasketV2Service;
    @Inject
    private ShoppingBasketResponseMapper shoppingBasketMapper;
    @Inject
    private ProductRequestMapper productRequestMapper;
    @Inject
    private CreateRequestMapper createRequestMapper;
    @Inject
    private InsuranceProductHandler insuranceProductHandler;
    @Inject
    private ShoppingCartAvailableCollectionMethods shoppingAvailableCollectionMethods;

    @Override
    public ShoppingBasketResponse createShoppingBasket(String bundleId, ResolverContext context) {

        String visitInformation = context.getVisitInformation().getVisitCode();
        CreateRequest createRequest = createRequestMapper.map(bundleId, visitInformation);
        com.odigeo.shoppingbasket.v2.responses.ShoppingBasketResponse shoppingBasketResponse = shoppingBasketV2Service.create(createRequest, context);

        return shoppingBasketMapper.map(shoppingBasketResponse);
    }

    @Override
    public ShoppingBasketResponse getShoppingBasket(String shoppingBasketId, ResolverContext context) {

        com.odigeo.shoppingbasket.v2.responses.ShoppingBasketResponse shoppingBasketResponse = shoppingBasketV2Service.getShoppingBasket(Long.parseLong(shoppingBasketId), context);

        ShoppingBasketResponse shoppingBasketResponseResult = shoppingBasketMapper.map(shoppingBasketResponse);

        completeInsuranceProducts(shoppingBasketResponseResult);

        return shoppingBasketResponseResult;
    }

    @Override
    public AvailableCollectionMethodsResponse getAvailableCollectionMethods(String shoppingBasketId, ResolverContext context) {
        return shoppingAvailableCollectionMethods.getAvailableCollectionMethods(shoppingBasketId, context);
    }

    private void completeInsuranceProducts(ShoppingBasketResponse shoppingBasketResponseResult) {
        Optional.ofNullable(shoppingBasketResponseResult.getInsurances())
            .orElse(Collections.emptyList())
            .parallelStream()
            .forEach(insuranceProductHandler::completeInsuranceProduct);
    }

    @Override
    public Boolean removeProduct(RemoveProductFromShoppingBasketRequest removeProductFromShoppingBasketRequest, ResolverContext context) {
        ProductRequest productRequest = productRequestMapper.map(
            removeProductFromShoppingBasketRequest.getProduct().getId(),
            removeProductFromShoppingBasketRequest.getProduct().getType().name(),
            null
        );

        shoppingBasketV2Service.removeProduct(
            Long.parseLong(removeProductFromShoppingBasketRequest.getShoppingBasketId()),
            productRequest,
            context
        );

        return Boolean.TRUE;
    }

    @Override
    public Boolean addProduct(AddProductToShoppingBasketRequest addProductToShoppingBasketRequest, ResolverContext context) {
        ProductRequest productRequest = productRequestMapper.map(
            addProductToShoppingBasketRequest.getProduct().getId(),
            addProductToShoppingBasketRequest.getProduct().getType().name(),
            INITIAL_PRODUCT_STATUS
        );

        shoppingBasketV2Service.addProduct(
            Long.parseLong(addProductToShoppingBasketRequest.getShoppingBasketId()),
            productRequest,
            context
        );

        return Boolean.TRUE;
    }

    @Override
    public Boolean addContactDetails(AddContactDetailsToShoppingBasketRequest addContactDetailsToShoppingBasketRequest) {
        throw new NotImplementedException();
    }
}
