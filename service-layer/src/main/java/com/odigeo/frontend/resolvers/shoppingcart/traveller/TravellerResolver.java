package com.odigeo.frontend.resolvers.shoppingcart.traveller;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCartTraveller;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerInformationDescription;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.shoppingcart.traveller.handlers.TravellerHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;

import java.util.List;

@Singleton
public class TravellerResolver implements Resolver {

    @Inject
    private TravellerHandler handler;
    static final String SHOPPING_CART_TYPE = "ShoppingCart";
    static final String REQUIRED_TRAVELLERS_INFORMATION_FIELD = "requiredTravellerInformation";
    static final String TRAVELLERS_FIELD = "travellers";

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type(TypeRuntimeWiring.newTypeWiring(SHOPPING_CART_TYPE)
                .dataFetcher(REQUIRED_TRAVELLERS_INFORMATION_FIELD, this::requiredTravellerInformationDataFetcher))
            .type(TypeRuntimeWiring.newTypeWiring(SHOPPING_CART_TYPE)
                .dataFetcher(TRAVELLERS_FIELD, this::travellersDataFetcher));
    }

    List<TravellerInformationDescription> requiredTravellerInformationDataFetcher(DataFetchingEnvironment env) {
        return handler.mapTravellerInformation(env.getGraphQlContext());
    }

    List<ShoppingCartTraveller> travellersDataFetcher(DataFetchingEnvironment env) {
        return handler.mapTravellers(env.getGraphQlContext());
    }
}
