package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.google.inject.Singleton;
import com.odigeo.insurance.api.last.util.SingleMoney;
import com.odigeo.product.v2.model.Price;

@Singleton
public class MoneyMapper {

    public Money map(Price price) {
        Money money = new Money();

        money.setAmount(price.getAmount());
        money.setCurrency(price.getCurrency().getCurrencyCode());

        return money;
    }

    public Money map(com.odigeo.insurance.api.last.util.Money money) {
        Money moneyResult = null;

        if (money instanceof SingleMoney) {
            SingleMoney singleMoney = (SingleMoney) money;
            moneyResult = new Money();
            moneyResult.setAmount(singleMoney.getAmount());
            moneyResult.setCurrency(singleMoney.getCurrency());
        }

        return moneyResult;
    }

}
