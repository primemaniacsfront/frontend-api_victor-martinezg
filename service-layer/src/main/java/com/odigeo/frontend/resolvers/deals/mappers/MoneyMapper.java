package com.odigeo.frontend.resolvers.deals.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import org.mapstruct.Mapper;

@Mapper
public interface MoneyMapper {

    Money moneyContractToModel(com.odigeo.marketing.search.price.v1.util.Money money);

}
