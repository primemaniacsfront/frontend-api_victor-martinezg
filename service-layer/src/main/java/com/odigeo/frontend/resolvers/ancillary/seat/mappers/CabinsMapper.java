package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Cabin;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryapi.v1.response.SeatMapCabin;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class CabinsMapper {

    @Inject
    private SeatMapper seatMapper;

    public List<Cabin> map(List<SeatMapCabin> seatMapCabinList) {
        return CollectionUtils.emptyIfNull(seatMapCabinList)
            .stream()
            .map(this::mapCabin)
            .collect(Collectors.toList());
    }

    private Cabin mapCabin(SeatMapCabin seatMapCabin) {
        Cabin cabin = new Cabin();

        cabin.setDistribution(seatMapCabin.getSeatMapRowsCharacteristics().getCabinColumnDistribution());
        cabin.setExtraLegSeats(seatMapCabin.getSeatMapRowsCharacteristics().getExtraLegSeats());
        cabin.setExitRows(seatMapCabin.getSeatMapRowsCharacteristics().getExitRows());
        cabin.setFloor(seatMapCabin.getCabinFloor());
        cabin.setFirstSeatsRow(seatMapCabin.getFirstSeatsRow());
        cabin.setLastSeatsRow(seatMapCabin.getLastSeatsRow());
        cabin.setSeatMapRowFrom(seatMapCabin.getSeatMapRowFrom());
        cabin.setSeatMapRowTo(seatMapCabin.getSeatMapRowTo());
        cabin.setSeats(seatMapper.map(seatMapCabin.getSeats()));

        return cabin;
    }

}
