package com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts;

import com.google.inject.Inject;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryPolicyConfiguration;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;

import java.io.IOException;
import java.util.Optional;

public class NonEssentialProductContext {
    private final ShoppingCartContext shoppingCartContext;
    private final int segments;
    private final int passengers;
    private final NonEssentialProductsConfiguration nonEssentialProductsConfiguration;

    @Inject
    private Logger logger;

    public NonEssentialProductContext(NonEssentialProductContextBuilder nonEssentialProductContextBuilder) {
        this.shoppingCartContext = nonEssentialProductContextBuilder.getShoppingCartContext();
        this.segments = nonEssentialProductContextBuilder.getSegments();
        this.passengers = nonEssentialProductContextBuilder.getPassengers();
        this.nonEssentialProductsConfiguration = nonEssentialProductContextBuilder.getNonEssentialProductsConfiguration();
    }

    public ShoppingCartContext getShoppingCartContext() {
        return shoppingCartContext;
    }

    public VisitInformation getVisitInformation() {
        return shoppingCartContext.getVisit();
    }

    public int getSegments() {
        return segments;
    }

    public int getPassengers() {
        return passengers;
    }

    public AncillaryConfiguration getAncillaryConfiguration(String policy) {
        VisitInformation visitInformation = shoppingCartContext.getVisit();
        AncillaryPolicyConfiguration ancillaryPolicyConfiguration = null;
        try {
            ancillaryPolicyConfiguration = nonEssentialProductsConfiguration.getConfiguration(visitInformation);
        } catch (IOException e) {
            logger.error(this.getClass(), e);
        }
        return Optional.ofNullable(ancillaryPolicyConfiguration).map(config -> config.get(policy)).orElse(null);
    }

    public String getCurrency() {
        return shoppingCartContext.getVisit().getCurrency();
    }
}
