package com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductProviderType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrl;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Price;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerception;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerceptionType;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class NonEssentialProductBuilder {
    private String policy;
    private BigDecimal totalPrice;
    private NonEssentialProductProviderType provider;
    private List<NonEssentialProductUrl> urls;
    private NonEssentialProductContext context;
    public NonEssentialProductBuilder() {
        policy = "";
        totalPrice = BigDecimal.ZERO;
        provider = NonEssentialProductProviderType.NOSUPP;
    }

    public NonEssentialProductBuilder withPolicy(String policy) {
        this.policy = policy;
        return this;
    }

    public NonEssentialProductBuilder withTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public NonEssentialProductBuilder withProvider(NonEssentialProductProviderType provider) {
        this.provider = provider;
        return this;
    }

    public NonEssentialProductBuilder withUrls(Collection<NonEssentialProductUrl> urls) {
        this.urls = new ArrayList<>(urls);
        return this;
    }

    public NonEssentialProductBuilder withContext(NonEssentialProductContext context) {
        this.context = context;
        return this;
    }

    public String getPolicy() {
        return policy;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public NonEssentialProductProviderType getProvider() {
        return provider;
    }

    public List<NonEssentialProductUrl> getUrls() {
        return urls;
    }

    public NonEssentialProductContext getContext() {
        return context;
    }

    private static BigDecimal perceivedPrice(BigDecimal totalPrice, PricePerceptionType pricePerceptionType, int passengers, int segments) {
        BigDecimal result = totalPrice;
        switch (pricePerceptionType) {
        case INCLUDED:
            result = BigDecimal.ZERO;
            break;
        case PERPAX:
            result = totalPrice.divide(new BigDecimal(passengers), RoundingMode.HALF_UP);
            break;
        case PERSEGMENT:
            result = totalPrice.divide(new BigDecimal(passengers * segments), RoundingMode.HALF_UP);
            break;
        case PERPLAN:
        default:
            result = totalPrice;
            break;
        }
        return result;
    }

    private static Price computePrice(String policy, BigDecimal totalPrice, NonEssentialProductContext context) {
        AncillaryConfiguration ancillaryConfiguration = context.getAncillaryConfiguration(policy);
        if (ancillaryConfiguration == null) {
            return null;
        }
        PricePerception pricePerception = new PricePerception(PricePerceptionType.valueOf(ancillaryConfiguration.getPricePerception().name()));
        Money money = new Money();
        money.setAmount(perceivedPrice(totalPrice, pricePerception.getType(), context.getPassengers(), context.getSegments()));
        money.setCurrency(context.getCurrency());
        Price price = new Price();
        price.setValue(money);
        price.setPerception(pricePerception);
        return price;
    }

    public NonEssentialProduct build() {
        NonEssentialProduct product = new NonEssentialProduct();
        product.setOfferId(this.policy);
        product.setPrice(computePrice(this.policy, this.totalPrice, this.context));
        product.setProvider(this.provider);
        product.setUrls(this.urls == null ? Collections.emptyList() : this.urls);
        return product;
    }
}
