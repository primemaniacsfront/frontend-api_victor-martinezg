package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.google.inject.Inject;
import com.odigeo.dapi.client.AccommodationDealInformation;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

public abstract class AccommodationSummaryDecorator implements AccommodationSummaryMapper {

    @Inject
    private GeoLocationHandler geolocationHandler;

    private final AccommodationSummaryMapper delegate;

    public AccommodationSummaryDecorator(AccommodationSummaryMapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public AccommodationSummaryResponseDTO map(AccommodationShoppingItem accommodationShoppingItem, AccommodationReview accommodationReview,
                                               AccommodationSummaryContext context) {
        AccommodationSummaryResponseDTO response = delegate.map(accommodationShoppingItem, accommodationReview, context);
        Optional.ofNullable(response)
            .map(AccommodationSummaryResponseDTO::getAccommodationDetail)
            .ifPresent(accommodationDetails -> mapDistanceFromCityCenter(accommodationDetails, context));
        Optional.ofNullable(response)
            .map(AccommodationSummaryResponseDTO::getAccommodationDetail)
            .map(AccommodationDetailDTO::getAccommodationDealInformation)
            .ifPresent(accommodationDetails -> mapMainImage(accommodationDetails, accommodationShoppingItem, context));
        return response;
    }

    private void mapDistanceFromCityCenter(AccommodationDetailDTO response, AccommodationSummaryContext context) {
        Optional.ofNullable(response)
            .map(AccommodationDetailDTO::getAccommodationDealInformation)
            .map(AccommodationDealInformationDTO::getCoordinates)
            .ifPresent(coordinates -> response.setDistanceFromCityCenter(
                    geolocationHandler.computeDistanceFromGeonode(context.getCity(), coordinates)
                )
            );
    }

    private void mapMainImage(AccommodationDealInformationDTO response, AccommodationShoppingItem accommodationShoppingItem, AccommodationSummaryContext context) {
        Optional.ofNullable(accommodationShoppingItem)
            .map(AccommodationShoppingItem::getAccommodationDealInformation)
            .map(AccommodationDealInformation::getAccommodationImages)
            .map(Collection::stream)
            .orElseGet(Stream::empty)
            .findFirst()
            .ifPresent(image -> response.setMainAccommodationImage(mapAccommodationImage(image, context)));

    }
}
