package com.odigeo.frontend.resolvers.accommodation.rooms.mappers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDealDTO;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDetailsDTO;
import com.odigeo.hcsapi.v9.beans.RoomDetails;
import com.odigeo.hcsapi.v9.beans.RoomsDetailResponse;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class StaticRoomDetailsMapper {

    @Inject
    private RoomImageMapper roomImageMapper;

    public void remapRoomDetails(List<RoomDealDTO> source, RoomsDetailResponse roomsDetailResponse) {
        for (RoomDealDTO roomDeal : source) {
            String providerRoomId = roomsDetailResponse.getProviderRoomIdByRoomId().get(roomDeal.getProviderRoomId());
            RoomDetails roomDetails = roomsDetailResponse.getRoomDetailByProviderRoomId().get(providerRoomId);
            if (roomDetails != null) {
                RoomDetailsDTO roomDetailsDTO = new RoomDetailsDTO();
                roomDetailsDTO.setName(getName(roomDetails.getRoomName(), roomDeal));
                roomDetailsDTO.setDescription(getDescription(roomDetails.getRoomDescription(), roomDeal));
                roomDetailsDTO.setMainImage(roomImageMapper.map(roomDetails.getMainRoomImage()));
                roomDetailsDTO.setImages(roomDetails.getRoomImages().stream().map(roomImageMapper::map).collect(Collectors.toList()));
                roomDeal.setDetails(roomDetailsDTO);
            }
        }
    }

    private String getName(String roomName, RoomDealDTO roomDeal) {
        return StringUtils.isNotBlank(roomName)
                ? roomName
                : Optional.of(roomDeal)
                .map(RoomDealDTO::getDetails)
                .map(RoomDetailsDTO::getName)
                .filter(StringUtils::isNotBlank)
                .orElse(null);
    }

    private String getDescription(String description, RoomDealDTO roomDeal) {
        return StringUtils.isNotBlank(description)
                ? description
                : Optional.of(roomDeal)
                .map(RoomDealDTO::getDetails)
                .map(RoomDetailsDTO::getDescription)
                .filter(StringUtils::isNotBlank)
                .orElseGet(roomDeal::getDescription);
    }
}
