package com.odigeo.frontend.resolvers.accommodation.summary.models;

public class AccommodationFacilityDTO {
    protected AccommodationFacilityGroupDTO facilityGroup;
    protected String code;
    protected String description;

    public AccommodationFacilityDTO() {
    }

    public AccommodationFacilityGroupDTO getFacilityGroup() {
        return facilityGroup;
    }

    public void setFacilityGroup(AccommodationFacilityGroupDTO facilityGroup) {
        this.facilityGroup = facilityGroup;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
