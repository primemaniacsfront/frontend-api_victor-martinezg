package com.odigeo.frontend.resolvers.trip.bookingimportantmessage;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Cancellation;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CancellationMessage;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Message;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MessageFilter;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class BookingImportantMessageFilter {

    public Optional<Message> filter(Message message, MessageFilter messageFilter) {
        List<BiFunction<Message, MessageFilter, Optional<Message>>> filters = Collections.singletonList(this::filterByProviderBookingItemId);
        Optional<Message> filteredMessage = Optional.of(message);
        for (BiFunction<Message, MessageFilter, Optional<Message>> filter : filters) {
            filteredMessage = filteredMessage.flatMap(fm -> filter.apply(fm, messageFilter));
        }
        return filteredMessage;
    }

    private Optional<Message> filterByProviderBookingItemId(Message message, MessageFilter messageFilter) {
        boolean filterPresent = Optional.ofNullable(messageFilter)
                .map(MessageFilter::getProviderBookingItemId)
                .filter(providerBookingItemId -> providerBookingItemId != 0)
                .isPresent();
        return filterPresent ? getFilteredMessage(message, messageFilter) : Optional.of(message);
    }

    private Optional<Message> getFilteredMessage(Message message, MessageFilter messageFilter) {
        List<Cancellation> cancellations = getCancellationsMatchingProviderBookingItemId((CancellationMessage) message, messageFilter.getProviderBookingItemId());
        if (cancellations.isEmpty()) {
            return Optional.empty();
        } else {
            ((CancellationMessage) message).setCancellations(cancellations);
            return Optional.of(message);
        }
    }

    private List<Cancellation> getCancellationsMatchingProviderBookingItemId(CancellationMessage message, Long providerBookingItemIdFilter) {
        return message.getCancellations()
                .stream()
                .filter(cancellation -> cancellation.getProviderBookingItemId() == providerBookingItemIdFilter)
                .collect(Collectors.toList());
    }

}
