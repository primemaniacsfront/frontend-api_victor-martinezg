package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.Segment;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.section.AbstractSection;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;

import java.util.ArrayList;
import java.util.List;

@Singleton
class FootprintSegmentMapper {

    @Inject
    private FootprintSectionMapper sectionMapper;

    public Segment map(SegmentDTO segmentDTO) {
        Segment segment = new Segment();
        segment.setSegmentIndex(segmentDTO.getId());
        segment.setSections(getSections(segmentDTO.getSections()));
        return segment;

    }

    private List<AbstractSection> getSections(List<SectionDTO> sectionDTOList) {

        List<AbstractSection> sections = new ArrayList<>();
        for (int i = 0; i < sectionDTOList.size(); i++) {
            sections.add(sectionMapper.map(sectionDTOList.get(i), i));
        }
        return sections;
    }
}
