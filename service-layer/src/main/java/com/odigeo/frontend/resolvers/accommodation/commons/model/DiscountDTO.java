package com.odigeo.frontend.resolvers.accommodation.commons.model;

import java.math.BigDecimal;

public class DiscountDTO {

    private BigDecimal percentage;
    private BigDecimal packageAmount;
    private BigDecimal primeAmount;

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getPackageAmount() {
        return packageAmount;
    }

    public void setPackageAmount(BigDecimal packageAmount) {
        this.packageAmount = packageAmount;
    }

    public BigDecimal getPrimeAmount() {
        return primeAmount;
    }

    public void setPrimeAmount(BigDecimal primeAmount) {
        this.primeAmount = primeAmount;
    }
}
