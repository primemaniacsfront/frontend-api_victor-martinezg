package com.odigeo.frontend.resolvers.accommodation.search.configuration;

import com.edreams.configuration.ConfiguredInJsonFile;
import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.Map;

@Singleton
@ConfiguredInJsonFile
public class AccommodationHistogramRangeByCurrencyConfiguration {
    private Map<String, Long> currencyRange = new HashMap<>();

    public void setCurrencyRange(Map<String, Long> currencyRange) {
        this.currencyRange = currencyRange;
    }

    public Map<String, Long> getCurrencyRange() {
        return currencyRange;
    }
}
