package com.odigeo.frontend.resolvers.itinerary.mappers.segment;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Mapper
public abstract class StopMapper {

    @AfterMapping
    public void calculateStops(@MappingTarget Segment segment) {
        List<Section> sections = Optional.ofNullable(segment).map(Segment::getSections).orElse(Collections.emptyList());
        int stops = CollectionUtils.isNotEmpty(sections) ? sections.size() - 1 : 0;
        int techStops = sections.stream().map(section -> section.getTechnicalStops().size()).reduce(0, Integer::sum);
        segment.setTechnicalStops(techStops);
        segment.setStops(stops);
        segment.setAllStops(stops + techStops);

        for (int i = 1; i < sections.size(); ++i) {
            Duration duration = Duration.between(sections.get(i - 1).getArrivalDate(), sections.get(i).getDepartureDate());
            sections.get(i).setStopDuration(Math.abs(duration.toMillis()) / (1000 * 60));
        }
    }

}
