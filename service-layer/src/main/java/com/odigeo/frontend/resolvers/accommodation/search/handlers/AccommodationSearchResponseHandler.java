package com.odigeo.frontend.resolvers.accommodation.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ThreeValuedLogic;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.PriceBreakdownHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.BoardTypeDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationPriceCalculatorDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.FeeDetailsDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.FeeInfoDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDeal;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDealPriceCalculatorAdapter;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationSearchReview;
import com.odigeo.searchengine.v2.responses.fee.FeeDetails;
import com.odigeo.searchengine.v2.responses.fee.FeeInfo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Singleton
public class AccommodationSearchResponseHandler {
    @Inject
    private ContentKeyMapper contentKeyMapper;
    @Inject
    private PriceBreakdownHandler priceBreakdownHandler;

    public AccommodationSearchResponseDTO buildSearchResponse(SearchResponse searchResponse, VisitInformation visitInformation, Integer numAdults) {

        checkSearchResponseMessages(searchResponse);
        AccommodationSearchResponseDTO searchResponseDTO = new AccommodationSearchResponseDTO();

        searchResponseDTO.setSearchId(searchResponse.getSearchId());
        AccommodationResponseDTO accommodationResponseDTO = new AccommodationResponseDTO();
        accommodationResponseDTO.setAccommodations(buildAccommodationList(searchResponse, visitInformation, numAdults));
        accommodationResponseDTO.setCurrency(searchResponse.getAccommodationSearchResults().getPriceCurrency().getCurrencyCode());
        searchResponseDTO.setAccommodationResponse(accommodationResponseDTO);

        return searchResponseDTO;
    }

    private void checkSearchResponseMessages(SearchResponse response) {
        Optional.ofNullable(response)
                .flatMap(searchResponse -> Optional.ofNullable(searchResponse.getMessages())
                        .filter(CollectionUtils::isNotEmpty)
                        .map(IterableUtils::first))
                .ifPresent(message -> {
                    String code = message.getCode();
                    String desc = message.getDescription();
                    throw new ServiceException(code + ":" + desc, ServiceName.SEARCH_ENGINE);
                });
    }

    private List<SearchAccommodationDTO> buildAccommodationList(SearchResponse searchResponse, VisitInformation visitInformation, Integer numNights) {
        List<SearchAccommodationDTO> accommodations = new ArrayList<>();
        searchResponse.getAccommodationSearchResults().getAccommodationResults().forEach(a -> accommodations.add(buildAccommodation(a, visitInformation, numNights)));
        return accommodations;
    }

    private SearchAccommodationDTO buildAccommodation(AccommodationDeal accommodationDeal, VisitInformation visitInformation, Integer numNights) {
        SearchAccommodationDTO searchAccommodationDTO = new SearchAccommodationDTO();
        searchAccommodationDTO.setDedupId(accommodationDeal.getDedupId());
        searchAccommodationDTO.setKey(accommodationDeal.getKey());
        searchAccommodationDTO.setPriceCalculator(buildAccommodationPriceCalculator(accommodationDeal.getPrice()));
        searchAccommodationDTO.setPriceBreakdown(priceBreakdownHandler.mapPriceBreakdown(accommodationDeal, visitInformation, numNights));
        searchAccommodationDTO.setContentKey(buildContentKey(accommodationDeal));
        searchAccommodationDTO.setProviderPrice(getProviderPrice(accommodationDeal));
        searchAccommodationDTO.setUsersRates(buildUsersRates(accommodationDeal));
        searchAccommodationDTO.setRoomsLeft(accommodationDeal.getRoomsLeft());
        searchAccommodationDTO.setBoardType(accommodationDeal.getBoardType() != null ? BoardTypeDTO.valueOf(accommodationDeal.getBoardType().name()) : null);
        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();
        accommodationDealInformationDTO.setCancellationFree(ThreeValuedLogic.valueOf(accommodationDeal.getCancellationFree().name()));
        accommodationDealInformationDTO.setPaymentAtDestination(accommodationDeal.isPaymentAtDestination());
        searchAccommodationDTO.setAccommodationDeal(accommodationDealInformationDTO);
        return searchAccommodationDTO;
    }

    private List<AccommodationReview> buildUsersRates(AccommodationDeal accommodationDeal) {
        List<AccommodationReview> accommodationReviewDTOS = new ArrayList<>();
        List<AccommodationSearchReview> usersRates = accommodationDeal.getUsersRates();
        usersRates.forEach(rate -> accommodationReviewDTOS.add(buildUserRate(rate)));
        return accommodationReviewDTOS;
    }

    private AccommodationReview buildUserRate(AccommodationSearchReview rate) {
        AccommodationReview accommodationReviewDTO = new AccommodationReview();
        accommodationReviewDTO.setTotalReviews(rate.getNumberOfUsersRates());
        accommodationReviewDTO.setProviderReviewId(rate.getProviderReviewId());
        accommodationReviewDTO.setProviderCode(rate.getSource());
        accommodationReviewDTO.setRating(rate.getAverageUsersRate().floatValue());
        return accommodationReviewDTO;
    }

    private Money getProviderPrice(AccommodationDeal accommodationDeal) {
        Money money = new Money();
        money.setAmount(accommodationDeal.getProviderPrice());
        money.setCurrency(accommodationDeal.getProviderCurrency());
        return money;
    }

    private String buildContentKey(AccommodationDeal accommodationDeal) {
        return contentKeyMapper.buildContentKey(accommodationDeal.getProviderId(), accommodationDeal.getProviderHotelId());
    }

    private AccommodationPriceCalculatorDTO buildAccommodationPriceCalculator(AccommodationDealPriceCalculatorAdapter price) {
        AccommodationPriceCalculatorDTO accommodationPriceCalculatorDTO = new AccommodationPriceCalculatorDTO();
        accommodationPriceCalculatorDTO.setMarkup(price.getMarkup());
        accommodationPriceCalculatorDTO.setSortPrice(price.getTotalPrice());
        accommodationPriceCalculatorDTO.setFeeInfo(buildFeeInfo(price.getFeeInfo()));
        return accommodationPriceCalculatorDTO;
    }

    private FeeInfoDTO buildFeeInfo(FeeInfo feeInfo) {
        FeeInfoDTO feeInfoDTO = new FeeInfoDTO();
        feeInfoDTO.setPaymentFee(buildFeeDetails(feeInfo.getPaymentFee()));
        feeInfoDTO.setSearchFee(buildFeeDetails(feeInfo.getSearchFee()));
        return feeInfoDTO;
    }

    private FeeDetailsDTO buildFeeDetails(FeeDetails feeDetails) {
        FeeDetailsDTO feeDetailsDTO = new FeeDetailsDTO();
        feeDetailsDTO.setDiscount(feeDetails.getDiscount());
        feeDetailsDTO.setTax(feeDetails.getTax());
        return feeDetailsDTO;
    }

}
