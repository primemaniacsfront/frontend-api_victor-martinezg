package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.criteria.SearchCriteria;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import org.apache.commons.lang3.StringUtils;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Singleton
public class CarbonFootprintSearchCriteriaHandler {

    private static final String GLOBAL_CODE = "GB";
    private static final String GLOBAL_WORD = "GLOBAL";

    @Inject
    private CarbonFootprintTripTypeMapper tripMapper;

    public SearchCriteria buildSearchCriteria(SearchRequest searchRequestDTO,
                                              SearchResponseDTO searchResponseDTO,
                                              VisitInformation visit) {

        SearchCriteria searchCriteria = new SearchCriteria();
        ItineraryRequest itineraryRequestDTO = searchRequestDTO.getItinerary();

        searchCriteria.setSearchId(searchResponseDTO.getSearchId());
        searchCriteria.setNumberOfAdults(itineraryRequestDTO.getNumAdults());
        searchCriteria.setNumberOfChildren(itineraryRequestDTO.getNumChildren());
        searchCriteria.setTripType(tripMapper.map(searchRequestDTO));
        searchCriteria.setBrand(visit.getBrand().name());
        searchCriteria.setMarket(getMarket(visit));
        searchCriteria.setSearchDateUTC(ZonedDateTime.now(ZoneOffset.UTC));

        return searchCriteria;
    }

    private String getMarket(VisitInformation visit) {
        String websiteCode = visit.getSite().name();
        return StringUtils.contains(websiteCode, GLOBAL_CODE) ? GLOBAL_WORD : visit.getWebsiteDefaultCountry();
    }
}
