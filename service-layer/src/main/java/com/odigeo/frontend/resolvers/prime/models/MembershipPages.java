package com.odigeo.frontend.resolvers.prime.models;

import com.odigeo.frontend.resolvers.prime.pages.MembershipManagerPage;
import com.odigeo.frontend.resolvers.prime.pages.MembershipPage;
import com.odigeo.frontend.resolvers.prime.pages.MembershipSubscriptionPage;

public enum MembershipPages {
    ACTIVE_MEMBERSHIP(MembershipManagerPage.class),
    SUBSCRIPTION_FUNNEL(MembershipSubscriptionPage.class);

    private final Class<? extends MembershipPage> pageClazz;

    MembershipPages(final Class<? extends MembershipPage> enumClazz) {
        this.pageClazz = enumClazz;
    }

    public  Class<? extends MembershipPage> value() {
        return pageClazz;
    }

}
