package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrl;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrlType;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.InsuranceConditionsUrlType;
import com.odigeo.dapi.client.InsuranceUrl;

@Singleton
public class NonEssentialProductUrlMapper {

    public NonEssentialProductUrl map(InsuranceUrl insuranceUrl) {
        NonEssentialProductUrl nonEssentialProductUrl = new NonEssentialProductUrl();

        nonEssentialProductUrl.setUrl(insuranceUrl.getUrl());
        nonEssentialProductUrl.setUrlType(getUrlType(insuranceUrl.getType()));

        return nonEssentialProductUrl;
    }

    private NonEssentialProductUrlType getUrlType(InsuranceConditionsUrlType type) {
        NonEssentialProductUrlType nonEssentialProductUrlType = NonEssentialProductUrlType.SIMPLE;

        if (InsuranceConditionsUrlType.EXTENDED.equals(type)) {
            nonEssentialProductUrlType = NonEssentialProductUrlType.EXTENDED;
        }

        return nonEssentialProductUrlType;
    }
}
