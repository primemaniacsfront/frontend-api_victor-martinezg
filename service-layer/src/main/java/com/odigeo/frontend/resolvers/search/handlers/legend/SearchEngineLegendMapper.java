package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.contract.location.LocationDTO;
import com.odigeo.frontend.resolvers.search.mappers.LocationMapper;
import com.odigeo.searchengine.v2.responses.baggage.BaggageConditions;
import com.odigeo.searchengine.v2.responses.baggage.BaggageEstimationFees;
import com.odigeo.searchengine.v2.responses.collection.CollectionEstimationFees;
import com.odigeo.searchengine.v2.responses.collection.CollectionMethod;
import com.odigeo.searchengine.v2.responses.gis.Location;
import com.odigeo.searchengine.v2.responses.itinerary.Carrier;
import com.odigeo.searchengine.v2.responses.itinerary.CrossFaringItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.FreeCancellationLimit;
import com.odigeo.searchengine.v2.responses.itinerary.HubItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerariesLegend;
import com.odigeo.searchengine.v2.responses.itinerary.ItineraryProvider;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Singleton
class SearchEngineLegendMapper {

    @Inject
    private DateUtils dateUtils;
    @Inject
    private LocationMapper locationMapper;

    public Map<Integer, com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier> buildCarrierMap(ItinerariesLegend legend) {
        return legend.getCarriers().stream()
            .collect(Collectors.toMap(Carrier::getId, carrier -> {
                com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier carrierDTO = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier();
                carrierDTO.setId(carrier.getCode());
                carrierDTO.setName(carrier.getName());

                return carrierDTO;
            }));
    }

    public Map<Integer, LocationDTO> buildLocationMap(ItinerariesLegend legend) {
        return legend.getLocations().stream()
            .collect(Collectors.toMap(Location::getGeoNodeId, engineLocation -> {
                LocationDTO locationDTO = new LocationDTO();
                locationDTO.setId(engineLocation.getGeoNodeId());
                locationDTO.setCityName(engineLocation.getCityName());
                locationDTO.setName(engineLocation.getName());
                locationDTO.setIata(engineLocation.getIataCode());
                locationDTO.setCityIata(engineLocation.getCityIataCode());
                locationDTO.setLocationType(locationMapper.map(engineLocation));
                locationDTO.setCountryCode(engineLocation.getCountryCode());
                locationDTO.setCountryName(engineLocation.getCountryName());

                return locationDTO;
            }));
    }

    public Map<Integer, ZonedDateTime> buildFreeCancellationMap(ItinerariesLegend legend) {
        return legend.getFreeCancellationLimits().stream()
            .collect(Collectors.toMap(FreeCancellationLimit::getId, cancellationLimit ->
                dateUtils.convertFromCalendar(cancellationLimit.getLimitTime())));
    }

    public Map<Integer, BaggageConditions> buildBaggageMap(ItinerariesLegend legend) {
        return legend.getBaggageEstimationFees().stream()
            .collect(Collectors.toMap(BaggageEstimationFees::getId, BaggageEstimationFees::getBaggageConditions));
    }

    public Map<Integer, List<TechnicalStop>> buildTechStopsMap(ItinerariesLegend legend,
                                                               Map<Integer, LocationDTO> locationMap) {
        return legend.getSectionResults().stream()
            .collect(Collectors.toMap(SectionResult::getId, sectionResult ->
                sectionResult.getSection().getTechnicalStops().stream().map(technicalStop -> {
                    TechnicalStop technicalStopDTO = new TechnicalStop();
                    technicalStopDTO.setLocation(locationMap.get(technicalStop.getLocation()));
                    technicalStopDTO.setDepartureDate(dateUtils.convertFromCalendar(technicalStop.getDepartureDate()));
                    technicalStopDTO.setArrivalDate(dateUtils.convertFromCalendar(technicalStop.getArrivalDate()));

                    return technicalStopDTO;
                }).collect(Collectors.toList())));
    }

    public Map<Integer, CollectionMethod> buildCollectionMethodMap(ItinerariesLegend legend) {
        return legend.getCollectionMethods().stream()
            .collect(Collectors.toMap(CollectionMethod::getId, collectionMethod -> collectionMethod));
    }

    public Map<Integer, CollectionEstimationFees> buildCollectionFeesMap(ItinerariesLegend legend) {
        return legend.getCollectionEstimationFees().stream()
            .collect(Collectors.toMap(CollectionEstimationFees::getId, collectionEstimation -> collectionEstimation));
    }

    public Map<Integer, HubItinerary> buildHubMap(ItinerariesLegend legend) {
        return legend.getItineraries().getHubs().stream()
                .collect(Collectors.toMap(HubItinerary::getId, hub -> hub));
    }

    public Map<Integer, SectionResult> buildSectionResultMap(ItinerariesLegend legend) {
        return legend.getSectionResults().stream()
            .collect(Collectors.toMap(SectionResult::getId, sectionResult -> sectionResult));
    }

    public Map<Integer, ItineraryProvider> buildSimplesMap(ItinerariesLegend legend) {
        return legend.getItineraries().getSimples().stream()
            .collect(Collectors.toMap(ItineraryProvider::getId, simple -> simple));
    }

    public Map<Integer, CrossFaringItinerary> buildCrossFaringMap(ItinerariesLegend legend) {
        return legend.getItineraries().getCrossFarings().stream()
            .collect(Collectors.toMap(CrossFaringItinerary::getId, crossFaring -> crossFaring));
    }

}
