package com.odigeo.frontend.resolvers.geolocation.country.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country;
import org.mapstruct.Context;

import java.util.Locale;
import java.util.Optional;

public abstract class CountryMapperDecorator implements CountryMapper {

    private final CountryMapper countryMapper;

    public CountryMapperDecorator(CountryMapper countryMapper) {
        this.countryMapper = countryMapper;
    }

    @Override
    public Country mapModelToContract(com.odigeo.geoapi.v4.responses.Country country, @Context Locale locale) {
        Country countryDto = countryMapper.mapModelToContract(country, locale);
        countryDto.setName(getName(country, locale));
        return countryDto;
    }

    private String getName(com.odigeo.geoapi.v4.responses.Country country, Locale locale) {
        if (country.getName() != null) {
            return Optional.of(country.getName().getText(locale)).orElse(
                    Optional.of(country.getName().getText(Locale.getDefault())).orElse(
                            country.getCountryCode()
                    ));
        }
        return null;
    }
}
