package com.odigeo.frontend.resolvers.accommodation.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.google.inject.Inject;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationReviewHandler;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AccommodationSearchReviewsHandler {
    private static final String TRIPADVISOR = "TA";
    @Inject
    private AccommodationReviewHandler accommodationReviewHandler;

    public void populateReviews(AccommodationSearchResponseDTO response) {
        List<Integer> dedupIds = response.getAccommodationResponse().getAccommodations()
                .stream()
                .map(SearchAccommodationDTO::getDedupId)
                .collect(Collectors.toList());
        Map<Integer, AccommodationReview> reviewsByProvider = accommodationReviewHandler.getReviewsByProvider(dedupIds, TRIPADVISOR);
        response.getAccommodationResponse().getAccommodations().forEach(accommodationDTO -> setUserReview(accommodationDTO, reviewsByProvider));
    }

    private void setUserReview(SearchAccommodationDTO searchAccommodationDTO, Map<Integer, AccommodationReview> reviewsByProvider) {
        searchAccommodationDTO.setUserReview(reviewsByProvider.get(searchAccommodationDTO.getDedupId()));
    }

}
