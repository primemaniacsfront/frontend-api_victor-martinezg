package com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi;

import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.DebugModeConfiguration;
import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.dapi.DapiSelectionDebugModeConfiguration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectRoomOfferResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.AccommodationRoomSelectionRequest;
import com.odigeo.dapi.client.BookingStatus;
import com.odigeo.dapi.client.SelectionRequest;
import com.odigeo.dapi.client.SelectionRequests;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.dapi.client.TestConfigurationSelectionRequest;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.SelectRoomOfferHandler;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.dapi.ShoppingCartAddUser;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.User;
import graphql.GraphQLContext;

@Singleton
public class ShoppingCartSelectRoomOffer implements SelectRoomOfferHandler {

    @Inject
    private ShoppingCartHandler shoppingCartHandler;
    @Inject
    private UserDescriptionService userService;
    @Inject
    private ShoppingCartAddUser shoppingCartAddUser;

    @Override
    public SelectRoomOfferResponse selectRoomOffer(String searchId, String accommodationDealKey, String roomDealKey, ResolverContext context, GraphQLContext graphQLContext) {
        SelectionRequests selectionRequests = createSelectionRequest(Long.parseLong(searchId), accommodationDealKey, roomDealKey);
        ShoppingCartSummaryResponse shoppingCart = shoppingCartHandler.createShoppingCart(
                selectionRequests,
                context,
                getTestConfigurationSelectionRequest(graphQLContext));
        SelectRoomOfferResponse response = new SelectRoomOfferResponse();
        response.setDealId(Long.toString(shoppingCart.getShoppingCart().getBookingId()));
        addUserLoginIfExists(context);
        return response;
    }

    private void addUserLoginIfExists(ResolverContext context) {
        User user = userService.getUserByToken(context);
        if (user != null) {
            setupDapiCookiesForSequentialCalls(context);
            shoppingCartAddUser.addUser(user, context);
        }
    }

    private void setupDapiCookiesForSequentialCalls(ResolverContext context) {
        context.getResponseInfo().getCookies()
                .stream()
                .filter(cookie -> cookie.getName().equals(SiteCookies.DAPI_JSESSIONID.value()))
                .findFirst()
                .ifPresent(dapiCookie -> context.getRequestInfo().addCookie(dapiCookie));
    }

    private TestConfigurationSelectionRequest getTestConfigurationSelectionRequest(GraphQLContext graphQLContext) {
        DebugModeConfiguration debugModeConfiguration = graphQLContext.get(DebugModeConfiguration.class);
        TestConfigurationSelectionRequest testConfigurationSelectionRequest = new TestConfigurationSelectionRequest();
        if (debugModeConfiguration != null && debugModeConfiguration.getDapiSelectionDebug() != null) {
            DapiSelectionDebugModeConfiguration dapiSelectionDebug = debugModeConfiguration.getDapiSelectionDebug();
            testConfigurationSelectionRequest.setFakeProviderBooking(dapiSelectionDebug.isFakeProvider());
            testConfigurationSelectionRequest.setBookingStatus(dapiSelectionDebug.getBookingStatus() != null ? BookingStatus.fromValue(dapiSelectionDebug.getBookingStatus().name()) : null);
        }
        return testConfigurationSelectionRequest;
    }

    private SelectionRequests createSelectionRequest(Long searchId, String accommodationDealKey, String roomGroupDealKey) {
        AccommodationRoomSelectionRequest accommodationRoomSelectionRequest = new AccommodationRoomSelectionRequest();
        accommodationRoomSelectionRequest.setAccommodationDealKey(accommodationDealKey);
        accommodationRoomSelectionRequest.setRoomGroupDealKey(roomGroupDealKey);
        SelectionRequest selectionRequest = new SelectionRequest();
        selectionRequest.setSearchId(searchId);
        selectionRequest.setAccommodationSelection(accommodationRoomSelectionRequest);
        SelectionRequests selectionRequests = new SelectionRequests();
        selectionRequests.getSelectionRequest().add(selectionRequest);
        return selectionRequests;
    }
}
