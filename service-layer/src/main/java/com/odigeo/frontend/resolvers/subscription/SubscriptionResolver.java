package com.odigeo.frontend.resolvers.subscription;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionFdoResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.subscription.handlers.SubscriptionHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

@Singleton
public class SubscriptionResolver implements Resolver {

    private static final String SUBSCRIBE_TO_FDO_MUTATION = "subscribeToFdo";
    @Inject
    private SubscriptionHandler subscriptionHandler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Mutation", typeWiring -> typeWiring
                        .dataFetcher(SUBSCRIBE_TO_FDO_MUTATION, this::subscribeToFdoDataFetcher));
    }

    SubscriptionFdoResponse subscribeToFdoDataFetcher(DataFetchingEnvironment env) {
        String email = jsonUtils.fromDataFetching(env);
        return subscriptionHandler.subscribeToFdo(email, env.getContext());
    }
}
