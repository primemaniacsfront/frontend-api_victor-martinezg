package com.odigeo.frontend.resolvers.accommodation.search.models;

public class SelectionRequestDTO {

    private AccommodationRoomSelectionRequestDTO accommodationSelection;
    private ItinerarySelectionRequestDTO itinerarySelection;
    private long searchId;

    public AccommodationRoomSelectionRequestDTO getAccommodationSelection() {
        return accommodationSelection;
    }

    public void setAccommodationSelection(AccommodationRoomSelectionRequestDTO accommodationSelection) {
        this.accommodationSelection = accommodationSelection;
    }

    public ItinerarySelectionRequestDTO getItinerarySelection() {
        return itinerarySelection;
    }

    public void setItinerarySelection(ItinerarySelectionRequestDTO itinerarySelection) {
        this.itinerarySelection = itinerarySelection;
    }

    public long getSearchId() {
        return searchId;
    }

    public void setSearchId(long searchId) {
        this.searchId = searchId;
    }
}
