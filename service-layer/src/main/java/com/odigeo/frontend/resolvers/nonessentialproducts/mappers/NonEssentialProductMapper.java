package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.Insurance;
import com.odigeo.dapi.client.InsuranceOffer;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryPolicyConfiguration;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

@Singleton
public class NonEssentialProductMapper {

    @Inject
    private NonEssentialProductsConfiguration nonEssentialProductsConfiguration;
    @Inject
    private NonEssentialProductUrlsMapper nonEssentialProductUrlsMapper;
    @Inject
    private NonEssentialProductPriceMapper nonEssentialProductPriceMapper;
    @Inject
    private NonEssentialProductProviderMapper nonEssentialProductProviderMapper;
    @Inject
    private Logger logger;

    public NonEssentialProduct map(InsuranceOffer insuranceOffer, VisitInformation visitInformation) {
        NonEssentialProduct nonEssentialProduct = null;

        Insurance insurance = Optional.ofNullable(insuranceOffer)
            .map(InsuranceOffer::getInsurances)
            .orElse(Collections.emptyList())
            .stream()
            .filter(Objects::nonNull)
            .findFirst()
            .orElse(null);

        if (Objects.nonNull(insurance)) {
            nonEssentialProduct = getNonEssentialProduct(insurance, insuranceOffer.getTotalPrice(), visitInformation);
        }

        return nonEssentialProduct;
    }

    private NonEssentialProduct getNonEssentialProduct(Insurance insurance, BigDecimal totalPrice, VisitInformation visitInformation) {
        NonEssentialProduct nonEssentialProduct = null;
        try {
            AncillaryPolicyConfiguration ancillaryPolicyConfiguration = nonEssentialProductsConfiguration.getConfiguration(visitInformation);
            AncillaryConfiguration ancillaryConfiguration = ancillaryPolicyConfiguration.get(insurance.getPolicy());
            if (Objects.nonNull(ancillaryConfiguration)) {
                nonEssentialProduct = new NonEssentialProduct();
                nonEssentialProduct.setOfferId(insurance.getPolicy());
                fillNonEssentialProduct(insurance, totalPrice, ancillaryConfiguration, visitInformation, nonEssentialProduct);
            }
        } catch (IOException e) {
            logger.error(NonEssentialProductMapper.class, e);
        }

        return nonEssentialProduct;
    }

    private void fillNonEssentialProduct(Insurance insurance, BigDecimal totalPrice, AncillaryConfiguration ancillaryConfiguration, VisitInformation visitInformation, NonEssentialProduct nonEssentialProduct) {
        nonEssentialProduct.setProvider(nonEssentialProductProviderMapper.map(insurance));
        nonEssentialProduct.setUrls(nonEssentialProductUrlsMapper.map(insurance, ancillaryConfiguration));
        nonEssentialProduct.setPrice(nonEssentialProductPriceMapper.map(totalPrice, visitInformation.getCurrency(), ancillaryConfiguration));
    }

}
