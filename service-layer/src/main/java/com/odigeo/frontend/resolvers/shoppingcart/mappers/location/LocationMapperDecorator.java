package com.odigeo.frontend.resolvers.shoppingcart.mappers.location;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import org.apache.commons.lang3.StringUtils;

public abstract class LocationMapperDecorator implements LocationMapper {

    static final String AIRPORT = "Airport";
    static final String TRAIN_STATION = "Train Station";
    static final String BUS_STATION = "Bus Station";

    private final LocationMapper delegate;

    public LocationMapperDecorator(LocationMapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public Location locationContractToModel(com.odigeo.dapi.client.Location location) {
        if (location == null || location.getType() == null) {
            return delegate.locationContractToModel(location);
        }

        Location model = delegate.locationContractToModel(location);
        model.setLocationType(mapLocationType(location.getType()));
        return model;
    }

    private LocationType mapLocationType(String location) {
        String locationType = StringUtils.defaultString(location);

        // TODO prepare funnel for bus (current bus content is provided by airlines)
        switch(locationType) {
        case AIRPORT:
        case BUS_STATION:
            return LocationType.AIRPORT;
        case TRAIN_STATION:
            return LocationType.TRAIN_STATION;
        default:
            return LocationType.OTHER;
        }
    }

}
