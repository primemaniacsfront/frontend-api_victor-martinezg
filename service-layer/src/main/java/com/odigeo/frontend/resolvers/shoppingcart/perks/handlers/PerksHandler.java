package com.odigeo.frontend.resolvers.shoppingcart.perks.handlers;

import com.odigeo.dapi.client.MembershipPerks;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import graphql.GraphQLContext;
import graphql.VisibleForTesting;

import java.math.BigDecimal;
import java.util.Optional;

public abstract class PerksHandler<T> {

    public abstract T map(GraphQLContext graphQlContext);

    String getCurrency(GraphQLContext graphQlContext) {
        return Optional.ofNullable(getVisitInformation(graphQlContext)).map(VisitInformation::getCurrency).orElse("");
    }

    BigDecimal getPrimeDiscountFee(GraphQLContext graphQlContext) {
        BigDecimal fee = getPerksIfDiscountApplied(graphQlContext);
        return fee != null ? fee : getPerksIfDiscountNotApplied(graphQlContext);
    }

    private VisitInformation getVisitInformation(GraphQLContext graphQlContext) {
        return Optional.ofNullable(graphQlContext)
                .map(context -> context.get(VisitInformation.class))
                .map(VisitInformation.class::cast)
                .orElse(null);
    }

    @VisibleForTesting
    BigDecimal getPerksIfDiscountNotApplied(GraphQLContext graphQlContext) {
        return Optional.ofNullable(graphQlContext)
                .map(context -> context.get(ShoppingCartSummaryResponse.class))
                .map(ShoppingCartSummaryResponse.class::cast)
                .map(ShoppingCartSummaryResponse::getMembershipPerks)
                .map(MembershipPerks::getFee)
                .orElse(null);
    }

    @VisibleForTesting
    BigDecimal getPerksIfDiscountApplied(GraphQLContext graphQlContext) {
        return Optional.ofNullable(graphQlContext)
                .map(context -> context.get(ShoppingCartSummaryResponse.class))
                .map(ShoppingCartSummaryResponse.class::cast)
                .map(ShoppingCartSummaryResponse::getShoppingCart)
                .map(ShoppingCart::getMembershipPerks)
                .map(MembershipPerks::getFee)
                .orElse(null);
    }

}
