package com.odigeo.frontend.resolvers.accommodation.search.models;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchMetadata;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeoLocation;

import java.util.List;

public class AccommodationResponseDTO {

    private List<SearchAccommodationDTO> accommodations;
    private String currency;
    private AccommodationSearchMetadata metadata;
    private GeoLocation location;

    public List<SearchAccommodationDTO> getAccommodations() {
        return accommodations;
    }

    public void setAccommodations(List<SearchAccommodationDTO> accommodations) {
        this.accommodations = accommodations;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public AccommodationSearchMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(AccommodationSearchMetadata metadata) {
        this.metadata = metadata;
    }

    public GeoLocation getLocation() {
        return location;
    }

    public void setLocation(GeoLocation location) {
        this.location = location;
    }
}
