package com.odigeo.frontend.resolvers.accommodation.rooms.models;

import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;

public class RoomDealDTO {
    private String key;
    private String type;
    private String boardType;
    private boolean payInAdvance;
    private boolean cancellationFree;
    private PriceBreakdownDTO priceBreakdown;
    private RoomDetailsDTO details;
    private String cancellationPolicy;
    private Integer roomsLeft;
    /*fields needed for further search/details but not for showing*/
    private String description;
    private String providerRoomId;
    private String providerId;
    private String providerHotelId;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProviderHotelId() {
        return providerHotelId;
    }

    public void setProviderHotelId(String providerHotelId) {
        this.providerHotelId = providerHotelId;
    }

    public RoomDetailsDTO getDetails() {
        return details;
    }

    public void setDetails(RoomDetailsDTO details) {
        this.details = details;
    }

    public String getProviderRoomId() {
        return providerRoomId;
    }

    public void setProviderRoomId(String providerRoomId) {
        this.providerRoomId = providerRoomId;
    }

    public String getBoardType() {
        return boardType;
    }

    public void setBoardType(String boardType) {
        this.boardType = boardType;
    }

    public PriceBreakdownDTO getPriceBreakdown() {
        return priceBreakdown;
    }

    public void setPriceBreakdown(PriceBreakdownDTO priceBreakdown) {
        this.priceBreakdown = priceBreakdown;
    }

    public boolean isPayInAdvance() {
        return payInAdvance;
    }

    public void setPayInAdvance(boolean payInAdvance) {
        this.payInAdvance = payInAdvance;
    }

    public boolean isCancellationFree() {
        return cancellationFree;
    }

    public void setCancellationFree(boolean cancellationFree) {
        this.cancellationFree = cancellationFree;
    }

    public Integer getRoomsLeft() {
        return roomsLeft;
    }

    public void setRoomsLeft(Integer roomsLeft) {
        this.roomsLeft = roomsLeft;
    }

    public String getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }
}
