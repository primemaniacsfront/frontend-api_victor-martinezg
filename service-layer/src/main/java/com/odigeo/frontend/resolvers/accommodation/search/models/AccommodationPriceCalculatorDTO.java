package com.odigeo.frontend.resolvers.accommodation.search.models;


import java.math.BigDecimal;

public class AccommodationPriceCalculatorDTO {

    private FeeInfoDTO feeInfo;
    private BigDecimal sortPrice;
    private BigDecimal markup;

    public FeeInfoDTO getFeeInfo() {
        return feeInfo;
    }

    public void setFeeInfo(FeeInfoDTO feeInfo) {
        this.feeInfo = feeInfo;
    }

    public BigDecimal getSortPrice() {
        return sortPrice;
    }

    public void setSortPrice(BigDecimal sortPrice) {
        this.sortPrice = sortPrice;
    }

    public BigDecimal getMarkup() {
        return markup;
    }

    public void setMarkup(BigDecimal markup) {
        this.markup = markup;
    }
}
