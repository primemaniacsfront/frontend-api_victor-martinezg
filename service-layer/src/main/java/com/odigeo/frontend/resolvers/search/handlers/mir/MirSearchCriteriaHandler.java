package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.SearchCriteria;
import org.apache.commons.lang3.StringUtils;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

@Singleton
class MirSearchCriteriaHandler {

    private static final String GLOBAL_CODE = "GB";
    private static final String GLOBAL_WORD = "GLOBAL";

    @Inject
    private MirDeviceTypeMapper deviceMapper;
    @Inject
    private MirTripTypeMapper tripMapper;

    public SearchCriteria buildRatingSearchCriteria(SearchRequest searchRequestDTO,
                                                    SearchResponseDTO searchResponseDTO,
                                                    VisitInformation visit) {

        SearchCriteria searchCriteria = new SearchCriteria();
        ItineraryRequest itineraryRequestDTO = searchRequestDTO.getItinerary();
        List<SearchItineraryDTO> itineraryDTOList = searchResponseDTO.getItineraries();

        searchCriteria.setVisitId(visit.getVisitId());
        searchCriteria.setSearchId(searchResponseDTO.getSearchId());
        searchCriteria.setNumberOfAdults(itineraryRequestDTO.getNumAdults());
        searchCriteria.setNumberOfChildren(itineraryRequestDTO.getNumChildren());
        searchCriteria.setDynPack(Boolean.FALSE);
        searchCriteria.setTripType(tripMapper.map(searchRequestDTO));
        searchCriteria.setDeviceType(deviceMapper.map(visit));
        searchCriteria.setBrand(visit.getBrand().name());
        searchCriteria.setMarket(getMarket(visit));
        searchCriteria.setSearchDateUTC(ZonedDateTime.now(ZoneOffset.UTC));
        searchCriteria.setDepartureDateUTC(getDepartureDate(itineraryDTOList));

        return searchCriteria;
    }

    private String getMarket(VisitInformation visit) {
        String websiteCode = visit.getSite().name();
        return StringUtils.contains(websiteCode, GLOBAL_CODE) ? GLOBAL_WORD : visit.getWebsiteDefaultCountry();
    }

    private ZonedDateTime getDepartureDate(List<SearchItineraryDTO> itineraryDTOList) {
        return itineraryDTOList.get(0)
            .getLegs().get(0)
            .getSegments().get(0)
            .getSections().get(0)
            .getDepartureDate();
    }

}
