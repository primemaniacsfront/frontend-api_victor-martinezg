package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.request.ItineraryFootprintCalculatorRequest;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.response.ItineraryFootprintCalculatorResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.services.CarbonFootprintService;

import java.util.concurrent.Callable;

public class CarbonFootprintCallable implements Callable<CarbonFootprintResponse> {

    private final SearchRequest searchRequest;
    private final SearchResponseDTO searchResponse;
    private final ResolverContext context;

    public CarbonFootprintCallable(SearchRequest searchRequestDTO, SearchResponseDTO searchResponseDTO, ResolverContext context) {
        this.context = context;
        this.searchRequest = searchRequestDTO;
        this.searchResponse = searchResponseDTO;
    }

    @Override
    public CarbonFootprintResponse call() {
        ItineraryFootprintCalculatorRequestMapper requestMapper = ConfigurationEngine.getInstance(ItineraryFootprintCalculatorRequestMapper.class);
        ItineraryFootprintCalculatorRequest itineraryFootprintCalculatorRequest = requestMapper.map(searchRequest, searchResponse, context);
        CarbonFootprintService service = ConfigurationEngine.getInstance(CarbonFootprintService.class);
        ItineraryFootprintCalculatorResponse response = service.getCarbonFootprint(itineraryFootprintCalculatorRequest);
        CarbonFootprintResponseCalculator calculator = ConfigurationEngine.getInstance(CarbonFootprintResponseCalculator.class);
        return calculator.calculateFootprintMap(response);
    }

}



