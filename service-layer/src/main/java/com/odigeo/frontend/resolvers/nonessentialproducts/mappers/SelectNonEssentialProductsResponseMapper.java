package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectNonEssentialProductsResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ProductIdMapper;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class SelectNonEssentialProductsResponseMapper {

    @Inject
    private ProductIdMapper productIdMapper;

    public SelectNonEssentialProductsResponse map(List<String> requestedProductsPolicies) {
        SelectNonEssentialProductsResponse selectNonEssentialProductsResponse = new SelectNonEssentialProductsResponse();
        selectNonEssentialProductsResponse.setProductId(getInsuranceProductIdList(requestedProductsPolicies));
        return selectNonEssentialProductsResponse;
    }

    private List<ProductId> getInsuranceProductIdList(List<String> requestedProductsPolicies) {
        return Optional.ofNullable(requestedProductsPolicies)
                .orElse(Collections.emptyList())
                .stream()
                .map(policy -> productIdMapper.map(new com.edreamsodigeo.insuranceproduct.api.last.response.ProductId(policy), ProductType.INSURANCE))
                .collect(Collectors.toList());
    }
}
