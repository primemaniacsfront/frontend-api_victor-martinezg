package com.odigeo.frontend.resolvers.checkout.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.checkout.handlers.dapi.ShoppingCartCheckoutHandler;

public class CheckoutFactory {

    // Add other CheckoutHandler implementations (ShoppingBasket v2 and v3)
    @Inject
    private ShoppingCartCheckoutHandler shoppingCartCheckoutHandler;

    public CheckoutHandler getInstance(ShoppingType type) {
        if (ShoppingType.CART.equals(type)) {
            return shoppingCartCheckoutHandler;
        } else {
            throw new ServiceException("No checkout implementation for " + type.name(), ServiceName.FRONTEND_API);
        }
    }

}
