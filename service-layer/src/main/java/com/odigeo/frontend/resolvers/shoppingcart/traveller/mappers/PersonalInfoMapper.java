package com.odigeo.frontend.resolvers.shoppingcart.traveller.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PersonalInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface PersonalInfoMapper {

    @Mapping(source = "traveller.name", target = "name")
    @Mapping(source = "traveller.firstLastName", target = "firstLastName")
    @Mapping(source = "traveller.secondLastName", target = "secondLastName")
    @Mapping(source = "traveller.travellerType", target = "ageType")
    @Mapping(source = "traveller.title", target = "title")
    @Mapping(source = "traveller.travellerGender", target = "travellerGender")
    @Mapping(source = "traveller.nationalityCountryCode", target = "nationalityCountryCode")
    @Mapping(source = "traveller.dateOfBirth", target = "birthDate", dateFormat = "yyyy-MM-dd")
    PersonalInfo personalInfoContractToModel(com.odigeo.dapi.client.Traveller traveller);
}
