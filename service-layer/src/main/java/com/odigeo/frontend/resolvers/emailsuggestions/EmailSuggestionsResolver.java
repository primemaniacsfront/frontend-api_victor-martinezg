package com.odigeo.frontend.resolvers.emailsuggestions;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.emailsuggestions.handlers.EmailSuggestionsHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.List;

@Singleton
public class EmailSuggestionsResolver implements Resolver {

    private static final String EMAIL_SUGGESTIONS_QUERY = "getEmailSuggestions";
    @Inject
    private EmailSuggestionsHandler emailSuggestionsHandler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher(EMAIL_SUGGESTIONS_QUERY, this::emailSuggestionsDataFetcher));
    }

    List<String> emailSuggestionsDataFetcher(DataFetchingEnvironment env) {
        String email = jsonUtils.fromDataFetching(env);
        return emailSuggestionsHandler.getEmailSuggestions(email, env.getContext());
    }
}
