package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceProduct;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class InsuranceProductMapper {

    @Inject
    private MoneyMapper moneyMapper;

    public InsuranceProduct map(com.edreamsodigeo.insuranceproduct.api.last.response.InsuranceProduct insuranceProduct) {
        InsuranceProduct insuranceProductResult = new InsuranceProduct();

        insuranceProductResult.setOfferKey(insuranceProduct.getPolicy());
        insuranceProductResult.setPrice(moneyMapper.map(insuranceProduct.getSellingPrice()));
        insuranceProductResult.setProductId(insuranceProduct.getId());

        return insuranceProductResult;
    }

    public void completeInsuranceProduct(InsuranceProduct insuranceProductToComplete, InsuranceProduct insuranceProductWithExtraInfo) {
        insuranceProductToComplete.setOfferKey(insuranceProductWithExtraInfo.getOfferKey());
        insuranceProductToComplete.setPrice(insuranceProductWithExtraInfo.getPrice());
    }
}
