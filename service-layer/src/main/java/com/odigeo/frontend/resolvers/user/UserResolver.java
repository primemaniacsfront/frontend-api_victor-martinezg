package com.odigeo.frontend.resolvers.user;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ChangePasswordWithCodeRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.User;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserCreditCardsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserTraveller;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.prime.utils.MembershipUtils;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.frontend.resolvers.user.handlers.UserCreditCardsHandler;
import com.odigeo.frontend.resolvers.user.handlers.UserPasswordHandler;
import com.odigeo.frontend.resolvers.user.handlers.UserTravellersHandler;
import com.odigeo.frontend.resolvers.user.mappers.UserDescriptionMapper;
import com.odigeo.frontend.services.UserDescriptionService;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.List;

@Singleton
public class UserResolver implements Resolver {

    private static final String QUERY = "Query";
    private static final String MUTATION = "Mutation";
    private static final String USER = "User";
    private static final String USER_INFO_QUERY = "userInfo";
    private static final String GET_USER_DESCRIPTION_QUERY = "getUser";
    private static final String GET_USER_TRAVELLERS_QUERY = "getTravellers";
    private static final String TRAVELLERS = "travellers";
    private static final String GET_USER_CREDIT_CARDS_QUERY = "getUserCreditCards";
    private static final String CREDIT_CARDS = "creditCards";
    private static final String CHANGE_PASSWORD = "changePassword";

    @Inject
    private UserTravellersHandler userTravellersHandler;
    @Inject
    private UserCreditCardsHandler userCreditCardsHandler;
    @Inject
    private MembershipHandler membershipHandler;
    @Inject
    private UserDescriptionService userDescriptionService;
    @Inject
    private UserDescriptionMapper mapper;
    @Inject
    private UserPasswordHandler userPasswordHandler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type(QUERY, typeWiring -> typeWiring
                        .dataFetcher(USER_INFO_QUERY, this::userInfoDataFetcher))
                .type(QUERY, typeWiring -> typeWiring
                        .dataFetcher(GET_USER_DESCRIPTION_QUERY, this::userDescriptionDataFetcher))
                .type(QUERY, typeWiring -> typeWiring
                        .dataFetcher(GET_USER_TRAVELLERS_QUERY, this::userTravellersDataFetcher))
                .type(USER, typeWiring -> typeWiring
                        .dataFetcher(TRAVELLERS, this::userTravellersDataFetcher))
                .type(QUERY, typeWiring -> typeWiring
                        .dataFetcher(GET_USER_CREDIT_CARDS_QUERY, this::userCreditCardsDataFetcher))
                .type(USER, typeWiring -> typeWiring
                        .dataFetcher(CREDIT_CARDS, this::userCreditCardsDataFetcher))
                .type(MUTATION, typeWiring -> typeWiring
                        .dataFetcher(CHANGE_PASSWORD, this::changePasswordDataFetcher));
    }

    @Deprecated
    UserInfo userInfoDataFetcher(DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        com.odigeo.userprofiles.api.v2.model.User user = userDescriptionService.getUserByToken(context);
        UserInfo response = null;

        if (user != null) {
            List<UserTraveller> travellers = userTravellersHandler.getTravellers(user);
            Membership membership = membershipHandler.getMembership(user, context.getVisitInformation());

            response = new UserInfo();
            response.setIsPrime(MembershipUtils.isActivated(membership));

            if (user.getId() != null) {
                response.setId(String.valueOf(user.getId()));
            }

            if (!travellers.isEmpty()) {
                response.setName(travellers.get(0).getPersonalInfo().getName());
            }
        }

        return response;
    }

    User userDescriptionDataFetcher(DataFetchingEnvironment env) {
        com.odigeo.userprofiles.api.v2.model.User user = userDescriptionService.getUserByToken(env.getContext());
        if (user != null) {
            env.getGraphQlContext().put("User", user);
        }
        return mapper.userContractToModel(user);
    }

    List<UserTraveller> userTravellersDataFetcher(DataFetchingEnvironment env) {
        return userTravellersHandler.getTravellers(env);
    }

    UserCreditCardsResponse userCreditCardsDataFetcher(DataFetchingEnvironment env) {
        return userCreditCardsHandler.getCreditCards(env);
    }

    User changePasswordDataFetcher(DataFetchingEnvironment env) {
        ChangePasswordWithCodeRequest changePasswordWithCodeRequest = jsonUtils.fromDataFetching(env, ChangePasswordWithCodeRequest.class);
        com.odigeo.userprofiles.api.v2.model.User user = userPasswordHandler.changePasswordWithCode(changePasswordWithCodeRequest, env);
        return mapper.userContractToModel(user);
    }
}
