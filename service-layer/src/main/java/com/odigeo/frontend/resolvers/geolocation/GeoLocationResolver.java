package com.odigeo.frontend.resolvers.geolocation;

import com.google.inject.Inject;
import com.odigeo.frontend.resolvers.Resolver;
import graphql.schema.idl.RuntimeWiring;

public class GeoLocationResolver implements Resolver {

    @Inject
    private GeoLocationFetcher geoLocationFetcher;


    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher("getGeoLocation", geoLocationFetcher::getGeoLocation));
    }

}
