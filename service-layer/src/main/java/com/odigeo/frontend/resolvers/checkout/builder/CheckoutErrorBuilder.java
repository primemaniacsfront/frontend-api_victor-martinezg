package com.odigeo.frontend.resolvers.checkout.builder;

import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceName;
import graphql.ErrorClassification;
import graphql.GraphQLError;
import graphql.GraphqlErrorBuilder;
import graphql.execution.ResultPath;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CheckoutErrorBuilder {

    private static final String CODE = "code";
    private static final String SERVICE = "service";
    private static final String DESCRIPTION = "description";

    private Map<String, Object> extensions;
    private ErrorClassification errorType;
    private String message;
    private List<Object> path;

    public static CheckoutErrorBuilder newError() {
        return new CheckoutErrorBuilder();
    }

    public CheckoutErrorBuilder message(String message, Object... formatArgs) {
        this.message = formatArgs != null && formatArgs.length != 0 ? String.format(message, formatArgs) : message;
        return this;
    }

    public CheckoutErrorBuilder path(ResultPath path) {
        this.path = path.toList();
        return this;
    }

    public CheckoutErrorBuilder path(List<Object> path) {
        this.path = path;
        return this;
    }

    public CheckoutErrorBuilder errorType(ErrorClassification errorType) {
        this.errorType = errorType;
        return this;
    }

    public CheckoutErrorBuilder extensions(Map<String, Object> extensions) {
        this.extensions = extensions;
        return this;
    }

    public CheckoutErrorBuilder extensions(ErrorCodes errorCode, ServiceName serviceName) {
        this.extensions = Stream.of(
                new AbstractMap.SimpleEntry<>(CODE, errorCode.getCode()),
                new AbstractMap.SimpleEntry<>(SERVICE, serviceName.getLabel()),
                new AbstractMap.SimpleEntry<>(DESCRIPTION, errorCode.getDescription())
        ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
        return this;
    }

    public GraphQLError build() {
        return GraphqlErrorBuilder.newError().errorType(errorType)
                .path(path).message(message).extensions(extensions).build();
    }
}
