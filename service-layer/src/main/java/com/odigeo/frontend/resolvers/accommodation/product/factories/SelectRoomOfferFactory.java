package com.odigeo.frontend.resolvers.accommodation.product.factories;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Inject;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.SelectRoomOfferHandler;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi.ShoppingCartSelectRoomOffer;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.productapi.AccommodationProductSelectRoomOffer;

public class SelectRoomOfferFactory {

    @Inject
    private ShoppingCartSelectRoomOffer shoppingCartSelectRoomOffer;
    @Inject
    private AccommodationProductSelectRoomOffer accommodationProductSelectRoomOffer;

    public SelectRoomOfferHandler getInstance(ShoppingType shoppingType) {
        return ShoppingType.BASKET_V3.equals(shoppingType) ? accommodationProductSelectRoomOffer : shoppingCartSelectRoomOffer;
    }
}
