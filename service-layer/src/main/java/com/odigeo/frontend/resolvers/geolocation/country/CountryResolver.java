package com.odigeo.frontend.resolvers.geolocation.country;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetCountriesRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetCountriesResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.geolocation.country.handlers.CountryHandler;
import com.odigeo.geoapi.v4.GeoServiceException;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

@Singleton
public class CountryResolver implements Resolver {
    public static final String GET_COUNTRIES_QUERY = "getCountries";

    @Inject
    private CountryHandler countryHandler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher(GET_COUNTRIES_QUERY, this::getCountries));
    }

    GetCountriesResponse getCountries(DataFetchingEnvironment env) throws GeoServiceException {
        GetCountriesRequest requestParams = jsonUtils.fromDataFetching(env, GetCountriesRequest.class);
        return countryHandler.getCountries(requestParams);
    }
}
