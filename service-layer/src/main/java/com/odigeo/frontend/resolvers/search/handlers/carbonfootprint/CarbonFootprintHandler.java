package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CarbonFootprint;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.search.handlers.SearchDebugHandler;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;

import java.util.Map;

@Singleton
public class CarbonFootprintHandler {

    @Inject
    private SearchDebugHandler debugHandler;

    @Inject
    private MetricsHandler metricsHandler;

    public void addCarbonFootprintInfo(CarbonFootprintResponse response, SearchResponseDTO searchResponse,
                                       ResolverContext context) {
        response.getItineraries().forEach((k, v) -> {
            searchResponse.getItineraries().get(k).setCarbonFootprint(v);
        });

        debugHandler.addQaModeCarbonFootprintInfo(context, searchResponse.getItineraries(), response.getSearchKilosAverage());
        evaluateCounterMetric(response.getItineraries());
    }

    private void evaluateCounterMetric(Map<Integer, CarbonFootprint> response) {
        if (response.values().stream().anyMatch(CarbonFootprint::getIsEco)) {
            metricsHandler.trackCounter(MeasureConstants.SEARCH_STATIC_FOOTPRINT);
        }
    }
}
