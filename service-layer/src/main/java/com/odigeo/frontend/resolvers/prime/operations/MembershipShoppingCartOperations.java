package com.odigeo.frontend.resolvers.prime.operations;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionActions;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionResponse;
import com.google.inject.Inject;
import com.odigeo.dapi.client.Buyer;
import com.odigeo.dapi.client.MembershipSubscriptionShoppingItem;
import com.odigeo.dapi.client.OtherProductsShoppingItems;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.membershipoffer.MembershipOfferHandler;
import com.odigeo.frontend.resolvers.prime.handlers.SubscriptionResponseHandler;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.resolvers.user.mappers.MembershipSubscriptionProductMapper;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.Optional;

public class MembershipShoppingCartOperations implements MembershipShoppingOperations {
    @Inject
    private ShoppingCartHandler shoppingCartHandler;
    @Inject
    private MembershipSubscriptionProductMapper membershipSubscriptionProductMapper;
    @Inject
    private SubscriptionResponseHandler subscriptionResponseHandler;
    @Inject
    private MembershipOfferHandler membershipOfferHandler;
    @Inject
    private Map<MembershipSubscriptionActions, MembershipSubscriptionShoppingCartAction<ModifyMembershipSubscription, ResolverContext>> membershipSubscriptionProductActions;

    @Override
    public FreeTrialCandidateRequest buildFreeTrialCandidateRequest(long shoppingId, ResolverContext context) {
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = shoppingCartHandler.getShoppingCart(shoppingId, context);
        return new FreeTrialCandidateRequest.Builder()
                .withEmail(getMailFromShoppingCart(shoppingCartSummaryResponse))
                .withName(getFuturePrimeMemberNameFromShoppingCart(shoppingCartSummaryResponse))
                .withLastNames(getFuturePrimeMemberLastNameFromShoppingCart(shoppingCartSummaryResponse))
                .withWebsite(getWebsiteFromContext(context))
                .build();
    }

    @Override
    public String getBuyerEmail(long shoppingId, ResolverContext context) {
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = shoppingCartHandler.getShoppingCart(shoppingId, context);
        return getMailFromShoppingCart(shoppingCartSummaryResponse);
    }

    @Override
    public boolean isPrimeUserDiscountApplied(long shoppingId, ResolverContext context) {
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = shoppingCartHandler.getShoppingCart(shoppingId, context);
        return isPrimeUserDiscountApplied(shoppingCartSummaryResponse);
    }

    @Override
    public MembershipSubscriptionResponse getMembershipSubscriptionResponse(long shoppingId, ResolverContext context) {
        ShoppingCartSummaryResponse shoppingCart = shoppingCartHandler.getShoppingCart(shoppingId, context);
        return Optional.ofNullable(getMembershipSubscriptionShoppingItem(shoppingCart))
                .map(shoppingItem -> membershipSubscriptionProductMapper
                        .fromMembershipShoppingItem(shoppingItem))
                .orElse(null);
    }

    @Override
    public MembershipSubscriptionResponse updateMembershipSubscriptionProduct(ModifyMembershipSubscription modifyMembershipSubscription, ResolverContext context) {
        return Optional.ofNullable(membershipSubscriptionProductActions
                .get(modifyMembershipSubscription.getAction())
                .andThen(this::getMembershipSubscriptionShoppingItem)
                .apply(modifyMembershipSubscription, context))
                .map(shoppingItem -> membershipSubscriptionProductMapper.fromMembershipShoppingItemAndSubscriptionResponse(shoppingItem, getSubscriptionResponse(modifyMembershipSubscription)))
                .orElse(null);
    }

    private SubscriptionResponse getSubscriptionResponse(ModifyMembershipSubscription modifyMembershipSubscription) {
        return Optional.of(modifyMembershipSubscription.getOfferId())
                .map(membershipOfferHandler::getSubscriptionOffer)
                .map(subscriptionResponseHandler::buildSubscriptionResponse)
                .orElse(null);
    }

    private MembershipSubscriptionShoppingItem getMembershipSubscriptionShoppingItem(ShoppingCartSummaryResponse shoppingCart) {
        return Optional.ofNullable(shoppingCart.getShoppingCart())
                .map(ShoppingCart::getOtherProductsShoppingItemContainer)
                .map(OtherProductsShoppingItems::getMembershipSubscriptionShoppingItems)
                .flatMap(optList -> optList.stream().findFirst())
                .orElse(null);
    }

    private String getMailFromShoppingCart(ShoppingCartSummaryResponse shoppingCartSummaryResponse) {
        return getShoppingCart(shoppingCartSummaryResponse)
                .map(ShoppingCart::getBuyer)
                .map(Buyer::getMail)
                .orElse(StringUtils.EMPTY);
    }

    private Optional<ShoppingCart> getShoppingCart(ShoppingCartSummaryResponse shoppingCartSummaryResponse) {
        return Optional.ofNullable(shoppingCartSummaryResponse)
                .map(ShoppingCartSummaryResponse::getShoppingCart);
    }

    private String getFuturePrimeMemberNameFromShoppingCart(ShoppingCartSummaryResponse shoppingCartSummaryResponse) {
        return getShoppingCart(shoppingCartSummaryResponse)
                .map(ShoppingCart::getBuyer)
                .map(Buyer::getName)
                .orElse(StringUtils.EMPTY);
    }

    private String getFuturePrimeMemberLastNameFromShoppingCart(ShoppingCartSummaryResponse shoppingCartSummaryResponse) {
        return getShoppingCart(shoppingCartSummaryResponse)
                .map(ShoppingCart::getBuyer)
                .map(Buyer::getLastNames)
                .orElse(StringUtils.EMPTY);
    }

    private String getWebsiteFromContext(ResolverContext context) {
        return Optional.ofNullable(context.getVisitInformation())
                .map(VisitInformation::getWebsiteDefaultCountry)
                .orElse(StringUtils.EMPTY);
    }

    private boolean isPrimeUserDiscountApplied(ShoppingCartSummaryResponse shoppingCartSummaryResponse) {
        return getShoppingCart(shoppingCartSummaryResponse)
                .map(ShoppingCart::getMembershipPerks)
                .isPresent();
    }
}
