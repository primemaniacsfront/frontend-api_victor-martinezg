package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.google.common.collect.Sets;
import com.google.inject.Singleton;

import java.util.Set;
import org.apache.commons.lang3.EnumUtils;

import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod.*;

@Singleton
class PaymentMethodManager {

    private static final Set<PaymentMethod> SUPPORTED_PAYMENT_METHOD = Sets.newHashSet(
        BANKTRANSFER,
        CREDITCARD,
        DOTPAY,
        GIROPAY,
        GOOGLEPAY,
        IDEAL,
        KLARNA,
        KLARNA_ACCOUNT,
        MULTIBANCO,
        PAYPAL,
        POLI,
        SAMSUNGPAY,
        SECURE3D,
        SEPADD,
        SOFORT,
        TRUSTLY
    );

    public boolean isSupported(String paymentMethod) {
        return SUPPORTED_PAYMENT_METHOD.contains(EnumUtils.getEnum(PaymentMethod.class, paymentMethod));
    }

}
