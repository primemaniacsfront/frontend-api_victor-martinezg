package com.odigeo.frontend.resolvers.shoppingbasket.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.odigeo.frontend.commons.context.ResolverContext;

public interface AvailableCollectionMethodsHandler {
    AvailableCollectionMethodsResponse getAvailableCollectionMethods(String shoppingBasketId, ResolverContext context);
}
