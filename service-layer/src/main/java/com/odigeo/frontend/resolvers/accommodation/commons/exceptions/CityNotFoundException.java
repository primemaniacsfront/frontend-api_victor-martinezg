package com.odigeo.frontend.resolvers.accommodation.commons.exceptions;

public class CityNotFoundException extends Exception {
    public CityNotFoundException(Throwable throwable) {
        super(throwable);
    }
}
