package com.odigeo.frontend.resolvers.accommodation.details;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationDetailsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationDetailsResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.details.handlers.AccommodationDetailsHandler;
import com.odigeo.frontend.resolvers.accommodation.details.mappers.AccommodationDetailResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.details.models.AccommodationDetailsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationSearchCriteriaRetrieveHandler;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.criteria.AbstractProductCategorySearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.provider.AccommodationProviderSearchCriteria;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.Optional;

@Singleton
public class AccommodationDetailsResolver implements Resolver {

    static final String ACCOMMODATION_DETAILS = "accommodationDetails";
    static final String ACCOMMODATION = "ACCOMMODATION";

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private AccommodationSearchCriteriaRetrieveHandler searchCriteriaRetrieveHandler;
    @Inject
    private AccommodationDetailResponseMapper accommodationDetailResponseMapper;
    @Inject
    private AccommodationDetailsHandler accommodationDetailsHandler;
    @Inject
    private ContentKeyMapper contentKeyMapper;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher(ACCOMMODATION_DETAILS, this::accommodationDetailsFetcher));
    }

    AccommodationDetailsResponse accommodationDetailsFetcher(DataFetchingEnvironment env) {
        AccommodationDetailsRequest request = jsonUtils.fromDataFetching(env, AccommodationDetailsRequest.class);
        ResolverContext context = env.getContext();
        VisitInformation visit = context.getVisitInformation();
        return accommodationDetailResponseMapper.map(getHotelDetails(request, visit));
    }

    private AccommodationDetailsResponseDTO getHotelDetails(AccommodationDetailsRequest request, VisitInformation visit) {
        Integer geoDestination = Optional.ofNullable(searchCriteriaRetrieveHandler.getSearchResultsBySearchId(request.getSearchId(), visit.getVisitCode()))
                .map(SearchResults::getSearchCriteria)
                .filter(searchCriteria -> searchCriteria instanceof AccommodationSearchCriteria)
                .map(AccommodationSearchCriteria.class::cast)
                .map(AbstractProductCategorySearchCriteria::getProviderSearchCriteria)
                .map(AccommodationProviderSearchCriteria.class::cast)
                .map(AccommodationProviderSearchCriteria::getDestinationGeoNodeId)
                .orElse(null);
        return accommodationDetailsHandler.getAccommodationDetails(contentKeyMapper.extractProviderKeyFromContentKey(request.getContentKey()), visit, geoDestination);
    }
}
