package com.odigeo.frontend.resolvers.itinerary.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.odigeo.frontend.resolvers.itinerary.mappers.leg.LegMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mapstruct.Context;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {LegMapper.class, TransportTypeMapper.class, TicketsLeftMapper.class})
@DecoratedWith(ItineraryMapperDecorator.class)
public interface ItineraryMapper {

    @Mapping(source = "itinerary.freeCancellation.limitTime", target = "freeCancellation")
    @Mapping(target = "freeCancellationLimit", ignore = true)
    @Mapping(source = "itinerary.legend", target = "legs")
    Itinerary itineraryContractToModel(com.odigeo.dapi.client.Itinerary itinerary, @Context ShoppingCartContext context);

}
