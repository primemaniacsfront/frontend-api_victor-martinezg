package com.odigeo.frontend.resolvers.checkout.models;

import java.util.Map;

public class CheckoutResumeRequest {

    private long shoppingId;
    private int interactionStep;
    private Map<String, String> params;
    private String userPaymentInteractionId;

    public CheckoutResumeRequest() {
    }

    public CheckoutResumeRequest(long shoppingId, int interactionStep, Map<String, String> params, String userPaymentInteractionId) {
        this.shoppingId = shoppingId;
        this.interactionStep = interactionStep;
        this.params = params;
        this.userPaymentInteractionId = userPaymentInteractionId;
    }

    public long getShoppingId() {
        return shoppingId;
    }

    public void setShoppingId(long shoppingId) {
        this.shoppingId = shoppingId;
    }

    public int getInteractionStep() {
        return interactionStep;
    }

    public void setInteractionStep(int interactionStep) {
        this.interactionStep = interactionStep;
    }

    public Map<String, String> getParams() {
        return this.params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getUserPaymentInteractionId() {
        return userPaymentInteractionId;
    }

    public void setUserPaymentInteractionId(String userPaymentInteractionId) {
        this.userPaymentInteractionId = userPaymentInteractionId;
    }
}
