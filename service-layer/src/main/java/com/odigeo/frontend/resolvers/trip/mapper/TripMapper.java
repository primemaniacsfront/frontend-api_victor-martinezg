package com.odigeo.frontend.resolvers.trip.mapper;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Trip;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import com.odigeo.travelcompanion.v2.model.booking.postsellserviceoption.PostsellServiceOptionBooking;
import com.odigeo.travelcompanion.v2.model.booking.postsellserviceoption.PostsellServiceOptionType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Mapper(uses = {DateMapper.class, ItineraryMapper.class})
public interface TripMapper {

    @Mapping(source = "bookingBasicInfo.website", target = "website")
    @Mapping(source = "bookingBasicInfo.id", target = "id")
    @Mapping(source = "bookingBasicInfo.creationDate", target = "creationDate")
    @Mapping(source = "itineraryBookings", target = "itinerary")
    @Mapping(source = "postsellServiceOptionBookings", target = "serviceOptions", qualifiedByName = "mapServiceOptions")
    @Mapping(source = "postsellServiceOptionBookings", target = "additions", qualifiedByName = "mapAdditions")
    Trip mapToTripContract(BookingDetail response);

    @Named("mapServiceOptions")
    default List<NonEssentialProduct> mapServiceOptions(List<PostsellServiceOptionBooking> postsellServiceOptionBookings) {
        return Optional.ofNullable(postsellServiceOptionBookings)
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(postsellServiceOptionBooking -> postsellServiceOptionBooking.getPostsellServiceOptionType() == PostsellServiceOptionType.STANDARD)
                .map(PostsellServiceOptionBooking::getPostsellServiceOptionProducts)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(postsellServiceOptionProduct -> {
                    NonEssentialProduct product = new NonEssentialProduct();
                    product.setOfferId(postsellServiceOptionProduct.getName());
                    return product;
                })
                .collect(Collectors.toList());
    }

    @Named("mapAdditions")
    default List<NonEssentialProduct> mapAdditions(List<PostsellServiceOptionBooking> postsellServiceOptionBookings) {
        return Optional.ofNullable(postsellServiceOptionBookings)
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(postsellServiceOptionBooking -> postsellServiceOptionBooking.getPostsellServiceOptionType() == PostsellServiceOptionType.ADDITION)
                .map(PostsellServiceOptionBooking::getPostsellServiceOptionProducts)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(postsellServiceOptionProduct -> {
                    NonEssentialProduct product = new NonEssentialProduct();
                    product.setOfferId(postsellServiceOptionProduct.getName());
                    return product;
                })
                .collect(Collectors.toList());
    }
}
