package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.google.inject.Singleton;

@Singleton
public class ProductIdMapper {

    public ProductId map(com.edreamsodigeo.insuranceproduct.api.last.response.ProductId productId, ProductType productType) {
        ProductId productIdResult = new ProductId();

        productIdResult.setId(productId.getId());
        productIdResult.setType(productType);

        return productIdResult;
    }
}
