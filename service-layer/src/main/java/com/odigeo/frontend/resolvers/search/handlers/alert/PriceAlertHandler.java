package com.odigeo.frontend.resolvers.search.handlers.alert;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PriceAlertRequest;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.services.CrmSubscriptionService;
import com.odigeo.marketing.subscriptions.rest.v1.parameters.IataPair;
import com.odigeo.marketing.subscriptions.rest.v1.parameters.PriceAlertSubscription;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Currency;

public class PriceAlertHandler {

    private static final String SUBSCRIPTION_KEY_FORMAT = "%s|%s";

    @Inject
    private CrmSubscriptionService crmSubscriptionService;
    @Inject
    private PointOfSaleMapper posWebsiteMapper;

    public void subscribePriceAlerts(PriceAlertRequest priceAlertRequestDTO, VisitInformation visit) {
        crmSubscriptionService.subscribeToPriceAlert(buildPriceAlertSubscription(priceAlertRequestDTO, visit));
    }

    private PriceAlertSubscription buildPriceAlertSubscription(PriceAlertRequest request, VisitInformation visit) {
        return new PriceAlertSubscription.Builder()
                .withIataPair(buildIataPair(request))
                .withPrice(request.getPrice())
                .withCurrency(Currency.getInstance(visit.getCurrency()))
                .withDepartureDate(LocalDate.parse(request.getDepartureDate()))
                .withArrivalDate(LocalDate.parse(request.getArrivalDate()))
                .withEmail(request.getEmail())
                .withMarket(visit.getSite().name())
                .withBrand(visit.getBrand().name())
                .withCreationDate(LocalDateTime.now())
                .withSubscribed(request.getNewsletterAccepted())
                .withSubscriptionKey(buildSubscriptionKey(request.getEmail(), visit))
                .withIp(visit.getUserIp())
                .build();
    }

    private IataPair buildIataPair(PriceAlertRequest priceAlertRequestDTO) {
        return new IataPair.Builder()
                .departureIata(priceAlertRequestDTO.getDepartureIata())
                .arrivalIata(priceAlertRequestDTO.getArrivalIata())
                .build();
    }

    private String buildSubscriptionKey(String email, VisitInformation visit) {
        return String.format(SUBSCRIPTION_KEY_FORMAT, email, posWebsiteMapper.map(visit));
    }

}
