package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FrequentFlyerCard;
import com.odigeo.userprofiles.api.v2.model.FrequentFlyer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface FrequentFlyerMapper {
    @Mapping(source = "airlineCode", target = "carrier")
    @Mapping(source = "frequentFlyerNumber", target = "number")
    FrequentFlyerCard frequentFlyerContractToModel(FrequentFlyer frequentFlyer);
}
