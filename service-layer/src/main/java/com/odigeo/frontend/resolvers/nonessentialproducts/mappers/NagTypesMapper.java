package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NagType;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import org.apache.commons.lang3.BooleanUtils;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class NagTypesMapper {

    public List<NagType> map(AncillaryConfiguration ancillaryConfiguration) {
        List<NagType> nagTypeList = new ArrayList<>();

        if (BooleanUtils.isTrue(ancillaryConfiguration.isContinuanceNagEnabled())) {
            nagTypeList.add(NagType.CONTINUANCE);
        }
        if (BooleanUtils.isTrue(ancillaryConfiguration.isRejectNagEnabled())) {
            nagTypeList.add(NagType.REJECT);
        }

        return nagTypeList;
    }
}
