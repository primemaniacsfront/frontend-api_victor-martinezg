package com.odigeo.frontend.resolvers.search.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.searchengine.v2.searchresults.criteria.marketing.tracking.MarketingTrackingInfo;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.Cookie;
import org.apache.commons.lang3.StringUtils;

@Singleton
public class MarketingTrackingHandler {

    private static final String UTM_SOURCE = "utmcsr";
    private static final String UTM_MEDIUM = "utmcmd";
    private static final String UTM_TERM = "utmctr";
    private static final String UTM_CONTENT = "utmcct";
    private static final String UTM_CAMPAIGN = "utmccn";
    private static final String UTM_GCLID = "utmgclid";

    private static final int SIX_MONTHS_IN_DAYS = 180;
    private static final int COOKIE_MAX_AGE_IN_SECONDS = (int) TimeUnit.DAYS.toSeconds(SIX_MONTHS_IN_DAYS - 30);

    private static final String DEFAULT_UTM_NO_GA = "no_GA";
    private static final String DEFAULT_GOOGLE_UTM_SOURCE_VALUE = "google";
    private static final String DEFAULT_GOOGLE_UTM_MEDIUM_VALUE = "cpc";

    private static final Map<String, Pattern> PATTERNS = new ConcurrentHashMap<>();

    @Inject
    private Logger logger;

    public MarketingTrackingInfo createMktTrackingInfo(ResolverContext context) {
        Cookie cookie = context.getRequestInfo().getCookie(SiteCookies.MKT_TRACKING);
        String cookieValue = getMarketingTrackingCookieValue(cookie);

        String decodedCookieValue = decode(cookieValue);
        String utmMedium = StringUtils.EMPTY;
        String utmContent = StringUtils.EMPTY;
        String utmTerm = StringUtils.EMPTY;
        String utmGclId = StringUtils.EMPTY;
        String utmCampaign = StringUtils.EMPTY;
        String utmSource = StringUtils.EMPTY;

        if (StringUtils.isNotEmpty(decodedCookieValue)) {
            utmCampaign = findInMarketingTrackingCookie(UTM_CAMPAIGN, decodedCookieValue);
            utmContent = findInMarketingTrackingCookie(UTM_CONTENT, decodedCookieValue);
            utmTerm = findInMarketingTrackingCookie(UTM_TERM, decodedCookieValue);
            utmGclId = findInMarketingTrackingCookie(UTM_GCLID, decodedCookieValue);
            if (StringUtils.isEmpty(utmGclId)) {
                utmSource = findInMarketingTrackingCookie(UTM_SOURCE, decodedCookieValue);
                utmMedium = findInMarketingTrackingCookie(UTM_MEDIUM, decodedCookieValue);
            } else {
                utmSource = DEFAULT_GOOGLE_UTM_SOURCE_VALUE;
                utmMedium = DEFAULT_GOOGLE_UTM_MEDIUM_VALUE;
            }
        }

        if (areAllEmptyValues(utmCampaign, utmContent, utmGclId, utmMedium, utmSource, utmTerm)) {
            utmSource = DEFAULT_UTM_NO_GA;
            utmMedium = DEFAULT_UTM_NO_GA;
        }

        String customCookieValue = Optional.ofNullable(cookie).map(Cookie::getValue).orElse(StringUtils.EMPTY);

        MarketingTrackingInfo trackingInfoEngine = new MarketingTrackingInfo();
        trackingInfoEngine.setUtmCampaign(utmCampaign);
        trackingInfoEngine.setUtmClickId(utmGclId);
        trackingInfoEngine.setUtmContent(utmContent);
        trackingInfoEngine.setUtmMedium(utmMedium);
        trackingInfoEngine.setUtmTerm(utmTerm);
        trackingInfoEngine.setUtmSource(utmSource);
        trackingInfoEngine.setMarketingTrackingCookie(cookieValue);
        trackingInfoEngine.setCustomMarketingTrackingCookie(customCookieValue);
        trackingInfoEngine.setMarketingPortal(context.getVisitInformation().getMktPortal());

        return trackingInfoEngine;
    }

    private boolean areAllEmptyValues(String... utmCodes) {
        return Arrays.stream(utmCodes).allMatch(StringUtils::isEmpty);
    }

    private String getMarketingTrackingCookieValue(Cookie cookie) {
        return marketingTrackingCookieIsYoungerThanThirtyDays(cookie)
            ? cookie.getValue()
            : StringUtils.EMPTY;
    }

    private boolean marketingTrackingCookieIsYoungerThanThirtyDays(Cookie cookie) {
        return Optional.ofNullable(cookie)
            .map(Cookie::getMaxAge)
            .map(maxAge -> maxAge == -1 || maxAge > COOKIE_MAX_AGE_IN_SECONDS)
            .orElse(false);
    }

    private String decode(String cookieValue) {
        String decodedCookie = StringUtils.EMPTY;

        try {
            decodedCookie = URLDecoder.decode(StringUtils.defaultString(cookieValue), StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            logger.warning(this.getClass(), "Can't decode cookie mktTrack value");
        }

        return decodedCookie;
    }

    private String findInMarketingTrackingCookie(String key, CharSequence cookieValue) {
        String result = null;
        Pattern pattern = PATTERNS.get(key);

        if (pattern == null) {
            pattern = Pattern.compile(key + "=([^|]*)");
            PATTERNS.put(key, pattern);
        }

        Matcher matcher = pattern.matcher(cookieValue);
        if (matcher.find()) {
            result = matcher.group(1);
        }

        return result;
    }

}
