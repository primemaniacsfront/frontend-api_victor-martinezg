package com.odigeo.frontend.resolvers.shoppingcart;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCart;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.AvailableProductsResponse;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.services.DapiService;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.ArrayList;
import java.util.Optional;

@Singleton
public class ShoppingCartResolver implements Resolver {
    private static final String GET_SHOPPING_CART_QUERY = "getShoppingCart";
    private static final String UPDATE_PRODUCTS_SHOPPING_BASKET_MUTATION = "updateProductsInShoppingBasket";

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private DapiService dapiService;
    @Inject
    private ShoppingCartHandler shoppingCartHandler;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher(GET_SHOPPING_CART_QUERY, this::getShoppingCart))
            .type("Mutation", typeWiring -> typeWiring
                .dataFetcher(UPDATE_PRODUCTS_SHOPPING_BASKET_MUTATION, this::updateProductsInShoppingBasket));
    }

    ShoppingCart getShoppingCart(DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        Long bookingId = jsonUtils.fromDataFetching(env);
        ShoppingCartSummaryResponse shoppingCart = dapiService.getShoppingCart(bookingId, context);
        saveInContext(shoppingCart, env);
        return shoppingCartHandler.buildShoppingCart(getShoppingCartDapi(shoppingCart), getVisitInformation(env));
    }

    UpdateProductsInShoppingBasketResponse updateProductsInShoppingBasket(DataFetchingEnvironment env) {
        UpdateProductsInShoppingBasketResponse response = new UpdateProductsInShoppingBasketResponse(new ArrayList<>());

        UpdateProductsInShoppingBasketRequest request = jsonUtils.fromDataFetching(env, UpdateProductsInShoppingBasketRequest.class);
        Long bookingId = Long.parseLong(request.getShoppingId());

        AvailableProductsResponse availableProductsResponse = shoppingCartHandler.getAvailableProductsFromDapi(bookingId, env.getContext());

        response.getStatus().addAll(shoppingCartHandler.removeProducts(request, availableProductsResponse, env.getContext()));
        response.getStatus().addAll(shoppingCartHandler.addProducts(request, availableProductsResponse, env.getContext()));

        return response;
    }

    private com.odigeo.dapi.client.ShoppingCart getShoppingCartDapi(ShoppingCartSummaryResponse shoppingCart) {
        return Optional.ofNullable(shoppingCart)
                .map(ShoppingCartSummaryResponse::getShoppingCart)
                .orElse(null);
    }

    private VisitInformation getVisitInformation(DataFetchingEnvironment env) {
        return Optional.ofNullable(env)
                .map(DataFetchingEnvironment::getGraphQlContext)
                .map(context -> context.get(VisitInformation.class))
                .map(VisitInformation.class::cast)
                .orElse(null);
    }

    private void saveInContext(ShoppingCartSummaryResponse shoppingCartSummaryResponse, DataFetchingEnvironment env) {
        if (shoppingCartSummaryResponse != null) {
            GraphQLContext graphQLContext = env.getGraphQlContext();
            graphQLContext.put(ShoppingCartSummaryResponse.class, shoppingCartSummaryResponse);
        }
    }
}
