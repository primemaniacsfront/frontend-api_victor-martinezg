package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.insurance.api.last.insurance.InsuranceOffers;
import com.odigeo.insurance.api.last.insurance.offer.SimpleInsuranceOffer;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class InsuranceOffersMapper {

    @Inject
    private InsuranceOfferMapper insuranceOfferMapper;

    public List<InsuranceOfferType> map(InsuranceOffers insuranceOffers) {
        return Optional.ofNullable(insuranceOffers)
            .map(InsuranceOffers::getOffers)
            .orElse(Collections.emptyList())
            .stream()
            .filter(SimpleInsuranceOffer.class::isInstance)
            .map(SimpleInsuranceOffer.class::cast)
            .map(insuranceOffer -> insuranceOfferMapper.map(insuranceOffer))
            .collect(Collectors.toList());
    }

}
