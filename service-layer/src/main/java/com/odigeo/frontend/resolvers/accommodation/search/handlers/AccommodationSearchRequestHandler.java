package com.odigeo.frontend.resolvers.accommodation.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RoomRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.ModuleReleaseInfo;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.search.handlers.MarketingTrackingHandler;
import com.odigeo.searchengine.v2.requests.AccommodationRoomRequest;
import com.odigeo.searchengine.v2.requests.AccommodationSearchRequest;
import com.odigeo.searchengine.v2.requests.ItinerarySelectionRequest;
import com.odigeo.searchengine.v2.requests.LocationRequest;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.requests.SelectionRequest;
import com.odigeo.searchengine.v2.responses.accommodation.SearchType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Singleton
public class AccommodationSearchRequestHandler {

    @Inject
    private ModuleReleaseInfo moduleReleaseInfo;
    @Inject
    private DateUtils dateUtils;
    @Inject
    private MarketingTrackingHandler marketingTrackingHandler;
    @Inject
    private AccommodationSearchRequestValidator validator;

    public SearchMethodRequest buildSearchRequest(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest request, ResolverContext context) {
        validator.validateAccommodationSearchRequest(request);
        SearchMethodRequest searchRequest = new SearchMethodRequest();
        AccommodationSearchRequest accommodationSearchRequest = new AccommodationSearchRequest();

        VisitInformation visit = context.getVisitInformation();
        DebugInfo debugInfo = context.getDebugInfo();

        searchRequest.setSearchRequest(accommodationSearchRequest);
        searchRequest.setVisitInformation(visit.getVisitCode());

        searchRequest.setQaMode(debugInfo.isEnabled());
        searchRequest.setEstimationFees(Boolean.FALSE);
        searchRequest.setClientVersion(moduleReleaseInfo.formatModuleInfo());

        searchRequest.setMarketingTrackingInfo(marketingTrackingHandler.createMktTrackingInfo(context));
        searchRequest.setBuypath(request.getBuyPath());
        buildAccommodationRequest(request, accommodationSearchRequest);
        return searchRequest;
    }

    private void buildAccommodationRequest(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest accommodationRequestDTO, AccommodationSearchRequest searchRequest) {
        searchRequest.setCheckInDate(dateUtils.convertFromIsoToCalendar(accommodationRequestDTO.getCheckInDate()));
        searchRequest.setCheckOutDate(dateUtils.convertFromIsoToCalendar(accommodationRequestDTO.getCheckOutDate()));

        searchRequest.setDestination(buildDestination(accommodationRequestDTO));

        searchRequest.getRoomRequests().addAll(buildRoomRequests(accommodationRequestDTO));

        if (accommodationRequestDTO.getMainProduct() != null) {
            searchRequest.setMainProduct(buildMainProduct(accommodationRequestDTO));
        }

        searchRequest.setSearchType(Optional.of(accommodationRequestDTO.getSearchType()).map(Enum::name).map(SearchType::fromValue).orElse(null));
        searchRequest.setUpsell(Optional.ofNullable(accommodationRequestDTO.getUpsell()).orElse(false));
        searchRequest.setMembershipFee(accommodationRequestDTO.getMembershipFee());
    }

    private LocationRequest buildDestination(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest accommodationRequestDTO) {
        LocationRequest destination = new LocationRequest();
        destination.setGeoNodeId(accommodationRequestDTO.getDestination().getGeoNodeId());
        destination.setIataCode(accommodationRequestDTO.getDestination().getIata());
        return destination;
    }

    private List<AccommodationRoomRequest> buildRoomRequests(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest accommodationRequestDTO) {
        List<AccommodationRoomRequest> roomRequests = new ArrayList<>();
        accommodationRequestDTO.getRoomRequests().forEach(roomRequest -> roomRequests.add(buildRoomRequest(roomRequest)));
        return roomRequests;
    }

    private AccommodationRoomRequest buildRoomRequest(RoomRequest roomRequest) {
        AccommodationRoomRequest accommodationRoomRequest = new AccommodationRoomRequest();
        accommodationRoomRequest.setNumAdults(roomRequest.getNumAdults());
        if (roomRequest.getChildrenAges() != null) {
            accommodationRoomRequest.getChildrenAges().addAll(roomRequest.getChildrenAges());
        }
        return accommodationRoomRequest;
    }

    private SelectionRequest buildMainProduct(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest accommodationRequestDTO) {
        SelectionRequest selectionRequest = new SelectionRequest();
        selectionRequest.setItinerarySelection(buildItinerarySelection(accommodationRequestDTO));
        selectionRequest.setSearchId(accommodationRequestDTO.getMainProduct().getSearchId());
        return selectionRequest;
    }

    private ItinerarySelectionRequest buildItinerarySelection(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest accommodationRequestDTO) {
        ItinerarySelectionRequest itinerarySelectionRequest = new ItinerarySelectionRequest();
        itinerarySelectionRequest.setFareItineraryKey(accommodationRequestDTO.getMainProduct().getItinerarySelection().getFareItineraryKey());
        itinerarySelectionRequest.getSegmentKeys().addAll(accommodationRequestDTO.getMainProduct().getItinerarySelection().getSegmentKeys());
        return itinerarySelectionRequest;
    }


}
