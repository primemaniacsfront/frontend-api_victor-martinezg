package com.odigeo.frontend.resolvers.insurance.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.insurance.mappers.InsuranceOffersMapper;
import com.odigeo.frontend.resolvers.insurance.mappers.PostBookingOfferRequestMapper;
import com.odigeo.frontend.services.insurance.InsuranceService;
import com.odigeo.insurance.api.last.insurance.InsuranceOffers;
import com.odigeo.insurance.api.last.request.PostBookingOfferRequest;

import java.util.List;

@Singleton
public class InsuranceHandler {

    @Inject
    private InsuranceService insuranceService;
    @Inject
    private InsuranceOffersMapper insuranceOffersMapper;
    @Inject
    private PostBookingOfferRequestMapper postBookingOfferRequestMapper;

    public List<InsuranceOfferType> getInsuranceOffers(long bookingId, String visitInformation) {

        PostBookingOfferRequest postBookingOfferRequest =
                postBookingOfferRequestMapper.map(bookingId, visitInformation);

        InsuranceOffers insuranceOffers =
                insuranceService.getOffers(postBookingOfferRequest);

        return insuranceOffersMapper.map(insuranceOffers);
    }
}
