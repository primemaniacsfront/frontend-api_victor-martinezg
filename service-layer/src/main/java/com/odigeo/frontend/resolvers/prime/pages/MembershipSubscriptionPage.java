package com.odigeo.frontend.resolvers.prime.pages;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPageName;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPagesRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import graphql.VisibleForTesting;
import graphql.schema.DataFetchingEnvironment;

@Singleton
public class MembershipSubscriptionPage implements MembershipPage {
    @VisibleForTesting
    static final int MULTI_OFFER_PARTITION = 3;
    @VisibleForTesting
    static final int SINGLE_OFFER_PARTITION = 2;
    @Inject
    private MembershipHandler membershipHandler;
    @Inject
    private SiteVariations siteVariations;
    @Inject
    private MetricsHandler metricsHandler;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public MembershipPageName getPage(DataFetchingEnvironment env) {
        boolean isFT = isEligibleForFreeTrial(env);
        if (!isFT) {
            metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_PAGE_SUBSCRIPTION_FTL);
            return MembershipPageName.SUBSCRIPTION_FTL;
        }
        VisitInformation visitInformation = env.getGraphQlContext().get(VisitInformation.class);
        return getSubscriptionTypeFunnelPage(visitInformation);
    }

    private boolean isEligibleForFreeTrial(DataFetchingEnvironment env) {
        MembershipPagesRequest membershipPagesRequest = jsonUtils.fromDataFetching(env, MembershipPagesRequest.class);
        try {
            return membershipHandler.isEligibleForFreeTrial(membershipPagesRequest.getShoppingInfo(), env.getContext());
        } catch (ServiceException e) {
            metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_FREE_TRIAL_ABUSER_FAILED);
            return Boolean.TRUE;
        }
    }

    @VisibleForTesting
    MembershipPageName getSubscriptionTypeFunnelPage(VisitInformation visitInformation) {
        MembershipPageName page = null;
        if (siteVariations.getMembershipSubscriptionTiers(visitInformation) == MULTI_OFFER_PARTITION) {
            metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_PAGE_SUBSCRIPTION_TIERS);
            page = MembershipPageName.SUBSCRIPTION_MULTIPLE_OFFERS;
        } else if (siteVariations.getMembershipSubscriptionTiers(visitInformation) == SINGLE_OFFER_PARTITION) {
            metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_PAGE_SUBSCRIPTION);
            page = MembershipPageName.SUBSCRIPTION_SINGLE_OFFER;
        }
        return page;
    }
}
