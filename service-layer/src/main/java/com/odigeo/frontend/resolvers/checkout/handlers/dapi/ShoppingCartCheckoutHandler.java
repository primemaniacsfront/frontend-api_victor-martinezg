package com.odigeo.frontend.resolvers.checkout.handlers.dapi;

import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteractionId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.google.inject.Inject;
import com.odigeo.dapi.client.BookingAdditionalParameter;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.ResumeDataRequest;
import com.odigeo.dapi.client.UserInteractionNeededResponse;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.DocumentValidatorUtils;
import com.odigeo.frontend.resolvers.checkout.CheckoutFetcherResult;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutHandler;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutResponseHandler;
import com.odigeo.frontend.resolvers.checkout.mappers.CheckoutMapper;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeRequest;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.resolvers.shoppingcart.models.ConfirmRequest;
import com.odigeo.frontend.services.checkout.UserPaymentInteractionService;
import graphql.GraphQLContext;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ShoppingCartCheckoutHandler implements CheckoutHandler {

    @Inject
    private ShoppingCartHandler shoppingCartHandler;
    @Inject
    private CheckoutMapper checkoutMapper;
    @Inject
    private DocumentValidatorUtils documentValidatorUtils;
    @Inject
    private CheckoutResponseHandler checkoutResponseHandler;
    @Inject
    private UserPaymentInteractionService paymentInteractionService;
    @Inject
    private UserInteractionHelper userInteractionHelper;

    @Override
    public CheckoutFetcherResult checkout(CheckoutRequest request, ResolverContext context, GraphQLContext graphQLContext) {
        cifValidator(request);
        String currency = getCurrency(context);
        ConfirmRequest confirmRequest = checkoutMapper.buildShoppingCartConfirmRequest(request, currency, graphQLContext);
        BookingResponse response = shoppingCartHandler.confirm(confirmRequest, context);
        RequestInfo requestInfo = Optional.ofNullable(graphQLContext).map(ctx -> ctx.get(RequestInfo.class))
                .map(RequestInfo.class::cast).orElse(null);
        String userPaymentInteractionId = createUserPaymentInteractionId(response.getUserInteractionNeededResponse(), request, requestInfo);
        return checkoutResponseHandler.buildCheckoutResponse(response, graphQLContext, userPaymentInteractionId);
    }

    @Override
    public CheckoutResponse resume(CheckoutResumeRequest request, ResolverContext context) {
        BookingResponse response = shoppingCartHandler.resume(request.getShoppingId(),
                buildResumeDataRequest(request.getInteractionStep(), request.getParams()), context);
        paymentInteractionService.updateOutcomeUserPaymentInteraction(request.getUserPaymentInteractionId(),
                response.getUserInteractionNeededResponse().getParameters());
        return checkoutMapper.mapShoppingCartToCheckoutResponse(response, request.getUserPaymentInteractionId());
    }

    private ResumeDataRequest buildResumeDataRequest(int step, Map<String, String> params) {
        ResumeDataRequest resumeDataRequest = new ResumeDataRequest();
        resumeDataRequest.setInteractionStep(step);
        resumeDataRequest.getAdditionalParameters().addAll(toDapiParams(params));
        return resumeDataRequest;
    }

    private List<BookingAdditionalParameter> toDapiParams(Map<String, String> params) {
        return params.entrySet().stream().map(param -> {
            BookingAdditionalParameter bookingParam = new BookingAdditionalParameter();
            bookingParam.setName(param.getKey());
            bookingParam.setValue(param.getValue());
            return bookingParam;
        }).collect(Collectors.toList());
    }

    private String getCurrency(ResolverContext context) {
        return Optional.ofNullable(context).map(ResolverContext::getVisitInformation).map(VisitInformation::getCurrency).orElse(StringUtils.EMPTY);
    }

    private void cifValidator(CheckoutRequest request) {
        Optional.ofNullable(request)
                .map(CheckoutRequest::getInvoice).ifPresent(invoice -> {
                    if (!documentValidatorUtils.isValidCIF(invoice.getInvoiceId(), invoice.getInvoiceCountryCode())) {
                        throw new ServiceException("invoice with wrong invoiceId", Response.Status.BAD_REQUEST, ServiceName.ONEFRONT);
                    }
                });
    }

    private String createUserPaymentInteractionId(UserInteractionNeededResponse interactionData, CheckoutRequest request, RequestInfo info) {
        String interactionId = null;
        if (interactionData != null) {
            UserPaymentInteractionId paymentInteractionId = paymentInteractionService.createUserPaymentInteraction(interactionData);
            replaceUrlInSnippet(interactionData, request, info, paymentInteractionId);
            interactionId = paymentInteractionId.getId().toString();
        }
        return interactionId;
    }

    private void replaceUrlInSnippet(UserInteractionNeededResponse interactionData, CheckoutRequest request, RequestInfo info, UserPaymentInteractionId userPaymentInteractionId) {
        if (StringUtils.isNotEmpty(interactionData.getJavascriptSnippet())) {
            String serverName = Optional.ofNullable(info).map(RequestInfo::getServerName).orElse(StringUtils.EMPTY);
            String ctxPath = Optional.ofNullable(info).map(RequestInfo::getContextPath).orElse(StringUtils.EMPTY);
            userInteractionHelper.buildReturnUrl(request, serverName, ctxPath, userPaymentInteractionId.getId().toString());
            // TODO Update snippet with url get from buildReturnUrl in user payment interaction service COUNTER-632
        }
    }

}
