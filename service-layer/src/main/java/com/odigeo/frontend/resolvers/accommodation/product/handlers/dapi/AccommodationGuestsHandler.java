package com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Guest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.dapi.client.RoomGroupDeal;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationDetailRetrieveHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationSearchCriteriaRetrieveHandler;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.criteria.AbstractProductCategorySearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.DynPackSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.accommodation.RoomCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.provider.AccommodationProviderSearchCriteria;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Singleton
public class AccommodationGuestsHandler {

    private static final int NO_GUEST = 0;

    @Inject
    private AccommodationDetailRetrieveHandler accommodationDetailRetrieveHandler;
    @Inject
    private AccommodationSearchCriteriaRetrieveHandler accommodationSearchCriteriaRetrieveHandler;

    public List<Guest> buildGuests(List<Guest> mainGuests, long bookingId, ResolverContext resolverContext, String visitCode) {
        List<Integer> numberOfGuest = calculateNumberOfGuestByRoom(bookingId, resolverContext, visitCode);
        if (mainGuests.size() < numberOfGuest.size()) {
            throw new ServiceException(
                String.format("Number of guest is not correct, expect:%d but received:%d", numberOfGuest.size(), mainGuests.size()),
                ServiceName.FRONTEND_API);
        }
        return IntStream.range(0, numberOfGuest.size())
                .mapToObj(i -> Collections.nCopies(numberOfGuest.get(i), mainGuests.get(i)))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<Integer> calculateNumberOfGuestByRoom(long bookingId, ResolverContext resolverContext, String visitCode) {
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = accommodationDetailRetrieveHandler.getShoppingCart(Long.valueOf(bookingId), resolverContext);
        return Optional.ofNullable(shoppingCartSummaryResponse)
            .map(ShoppingCartSummaryResponse::getShoppingCart)
            .map(ShoppingCart::getAccommodationShoppingItem)
            .map(AccommodationShoppingItem::getRoomGroupDeal)
            .map(RoomGroupDeal::getSearchId)
            .map(searchId -> accommodationSearchCriteriaRetrieveHandler.getSearchResultsBySearchId(searchId, visitCode))
            .map(SearchResults::getSearchCriteria)
            .map(searchCriteria -> searchCriteria instanceof DynPackSearchCriteria
                ? ((DynPackSearchCriteria) searchCriteria).getAccommodationSearchCriteria()
                : (AccommodationSearchCriteria) searchCriteria
            ).map(AbstractProductCategorySearchCriteria::getProviderSearchCriteria)
            .map(AccommodationProviderSearchCriteria.class::cast)
            .map(AccommodationProviderSearchCriteria::getRoomCriterias)
            .filter(CollectionUtils::isNotEmpty)
            .map(this::calculateGuestByRoom)
            .orElseThrow(() -> new ServiceException("Error calculating number of guest", ServiceName.FRONTEND_API));
    }

    private List<Integer> calculateGuestByRoom(List<RoomCriteria> roomCriteria) {
        return roomCriteria.stream().map(this::calculateGuestRoom).collect(Collectors.toList());
    }

    private int calculateGuestRoom(RoomCriteria roomCriteria) {
        return defaultIfNull(roomCriteria.getNumberOfAdults(), NO_GUEST)
            + defaultIfNull(roomCriteria.getNumberOfChilds(), NO_GUEST)
            + defaultIfNull(roomCriteria.getNumberOfInfants(), NO_GUEST);
    }
}
