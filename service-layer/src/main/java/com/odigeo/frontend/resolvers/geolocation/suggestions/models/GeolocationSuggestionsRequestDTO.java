package com.odigeo.frontend.resolvers.geolocation.suggestions.models;

import com.odigeo.geoapi.v4.request.ProductType;

public class GeolocationSuggestionsRequestDTO {

    private String searchWord;
    private String locale;
    private ProductType productType;
    private String website;
    private String inputType;
    private boolean includeMultiLanguage;
    private boolean includeRelatedLocations;
    private boolean includeSearchInAllWords;
    private boolean includeCountry;
    private boolean includeRegion;
    private boolean includeNearestLocations;
    private boolean includeCitiesOnly;

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public boolean getIncludeMultiLanguage() {
        return includeMultiLanguage;
    }

    public void setIncludeMultiLanguage(boolean includeMultiLanguage) {
        this.includeMultiLanguage = includeMultiLanguage;
    }

    public boolean getIncludeRelatedLocations() {
        return includeRelatedLocations;
    }

    public void setIncludeRelatedLocations(boolean includeRelatedLocations) {
        this.includeRelatedLocations = includeRelatedLocations;
    }

    public boolean getIncludeSearchInAllWords() {
        return includeSearchInAllWords;
    }

    public void setIncludeSearchInAllWords(boolean includeSearchInAllWords) {
        this.includeSearchInAllWords = includeSearchInAllWords;
    }

    public boolean getIncludeCountry() {
        return includeCountry;
    }

    public void setIncludeCountry(boolean includeCountry) {
        this.includeCountry = includeCountry;
    }

    public boolean getIncludeRegion() {
        return includeRegion;
    }

    public void setIncludeRegion(boolean includeRegion) {
        this.includeRegion = includeRegion;
    }

    public boolean getIncludeNearestLocations() {
        return includeNearestLocations;
    }

    public void setIncludeNearestLocations(boolean includeNearestLocations) {
        this.includeNearestLocations = includeNearestLocations;
    }

    public boolean getIncludeCitiesOnly() {
        return includeCitiesOnly;
    }

    public void setIncludeCitiesOnly(boolean includeCitiesOnly) {
        this.includeCitiesOnly = includeCitiesOnly;
    }
}
