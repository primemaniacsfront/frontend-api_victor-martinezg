package com.odigeo.frontend.resolvers.geolocation;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeoLocation;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.mappers.GeoCoordinatesMapper;
import com.odigeo.frontend.commons.util.GeoUtils;
import com.odigeo.frontend.resolvers.accommodation.commons.exceptions.CityNotFoundException;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoServiceException;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Country;
import com.odigeo.geoapi.v4.responses.GeoNode;
import com.odigeo.geoapi.v4.utils.LocalizedText;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;

@Singleton
public class GeoLocationHandler {

    private static final double NOT_COMPUTABLE = -1.0;

    @Inject
    private GeoService geoService;
    @Inject
    private GeoCoordinatesMapper geoCoordinatesMapper;
    @Inject
    private GeoUtils geoUtils;


    public GeoLocation retrieveGeoLocation(Integer geonodeId, String locale) throws GeoNodeNotFoundException, GeoServiceException {
        return mapGeoNode(geoService.getGeonode(geonodeId), locale);
    }

    public City getCityGeonode(Integer cityGeoNodeId, String cityIata) throws CityNotFoundException {
        try {
            return cityGeoNodeId != null ? geoService.getCityByGeonodeId(cityGeoNodeId) : geoService.getCityByIataCode(cityIata);
        } catch (ServiceException | GeoNodeNotFoundException ex) {
            throw new CityNotFoundException(ex);
        }
    }

    public Double computeDistanceFromGeonode(GeoNode geoNode, GeoCoordinatesDTO coordinates) {
        if (geoNode != null && coordinates != null) {
            return BigDecimal.valueOf(geoUtils.calculateDistance(geoCoordinatesMapper.map(geoNode.getCoordinates()), coordinates))
                    .setScale(2, RoundingMode.HALF_UP)
                    .doubleValue();
        }
        return NOT_COMPUTABLE;
    }

    private GeoLocation mapGeoNode(GeoNode geonode, String locale) {
        GeoLocation geoLocation = new GeoLocation();
        GeoNodeType geoNodeType = GeoNodeType.getValueFromString(geonode.getClass().getSimpleName());
        switch (geoNodeType) {
        case CITY:
            mapGeoLocation((City) geonode, geoLocation, locale);
            break;
        case COUNTRY:
            mapGeoLocation((Country) geonode, geoLocation, locale);
            break;
        case UNKNOWN:
        default:
            return geoLocation;
        }
        return geoLocation;
    }

    private String getTextFromLocalizedText(LocalizedText localizedText, String locale) {
        return localizedText.getText(new Locale(locale), new Locale(locale.split("_")[0]));
    }

    private void mapGeoLocation(City city, GeoLocation geoLocation, String locale) {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.City responseCity = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.City();
        responseCity.setId(city.getGeoNodeId());
        responseCity.setIata(city.getIataCode());
        responseCity.setName(getTextFromLocalizedText(city.getName(), locale));
        responseCity.setCoordinates(geoCoordinatesMapper.mapToContractGeoCoordinates(city.getCoordinates()));
        geoLocation.setCity(responseCity);
        mapGeoLocation(city.getCountry(), geoLocation, locale);
    }

    private void mapGeoLocation(Country country, GeoLocation geoLocation, String locale) {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country responseCountry = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country();
        responseCountry.setCode(country.getCountryCode());
        responseCountry.setName(getTextFromLocalizedText(country.getName(), locale));
        responseCountry.setPhonePrefix(country.getPhonePrefix());
        responseCountry.setId(country.getGeoNodeId());
        geoLocation.setCountry(responseCountry);
    }


}
