package com.odigeo.frontend.resolvers.itinerary.mappers.section;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.time.Duration;

@Mapper
public abstract class TechnicalStopMapper {

    @AfterMapping
    public void setStopDuration(@MappingTarget TechnicalStop techStop) {
        Duration duration = Duration.between(techStop.getArrivalDate(), techStop.getDepartureDate());
        techStop.setStopDuration(Math.abs(duration.toMillis()) / (1000 * 60));
    }

}
