package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.google.inject.Inject;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.resolvers.accommodation.commons.exceptions.CityNotFoundException;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.City;

public class AccommodationLocationHandler {

    @Inject
    private GeoLocationHandler geoLocationHandler;
    @Inject
    private Logger logger;


    public void populateSearchedLocation(AccommodationSearchResponseDTO response, Integer searchedCityGeoNodeId, String searchedIataCode) {
        try {
            City city = geoLocationHandler.getCityGeonode(searchedCityGeoNodeId, searchedIataCode);
            populateDistanceFromCityCenter(response, city);
        } catch (CityNotFoundException e) {
            logger.warning(this.getClass(), "Error retrieving city from geoservice", e);
        }
    }

    private void populateDistanceFromCityCenter(AccommodationSearchResponseDTO response, City city) {
        response.getAccommodationResponse()
                .getAccommodations()
                .forEach(accommodationDTO ->
                        accommodationDTO.setDistanceFromCityCenter(geoLocationHandler.computeDistanceFromGeonode(city, accommodationDTO.getAccommodationDeal().getCoordinates())));
    }


}
