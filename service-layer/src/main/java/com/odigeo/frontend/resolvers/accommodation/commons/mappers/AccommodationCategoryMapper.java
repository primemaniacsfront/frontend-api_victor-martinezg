package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory;
import com.google.inject.Singleton;
import com.odigeo.hcsapi.v9.beans.HotelStarRating;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Singleton
public class AccommodationCategoryMapper {

    private static final Map<HotelStarRating, AccommodationCategory> STAR_RATING_TO_CATEGORY_MAP = new HashMap<>();

    static {
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H0, AccommodationCategory.STARS_0);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H0_5, AccommodationCategory.STARS_0_5);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H1, AccommodationCategory.STARS_1);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H1_5, AccommodationCategory.STARS_1_5);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H2, AccommodationCategory.STARS_2);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H2_5, AccommodationCategory.STARS_2_5);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H3, AccommodationCategory.STARS_3);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H3_5, AccommodationCategory.STARS_3_5);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H4, AccommodationCategory.STARS_4);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H4_5, AccommodationCategory.STARS_4_5);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H5, AccommodationCategory.STARS_5);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H5_5, AccommodationCategory.STARS_6);
        STAR_RATING_TO_CATEGORY_MAP.put(HotelStarRating.H7, AccommodationCategory.STARS_7);
    }

    public AccommodationCategory mapStarToCategory(HotelStarRating hotelStarRating) {
        return Optional.ofNullable(STAR_RATING_TO_CATEGORY_MAP.get(hotelStarRating)).orElse(AccommodationCategory.UNKNOWN);
    }

}
