package com.odigeo.frontend.resolvers.trip.bookingimportantmessage;

import com.edreamsodigeo.bookingimportantmessage.contract.v1.model.cancellation.CancellationMessage;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Message;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MessageFilter;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.trip.bookingimportantmessage.mapper.BookingImportantMessageMapper;
import com.odigeo.frontend.services.managemybooking.BookingImportantMessageService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class BookingImportantMessageHandler {

    private final BookingImportantMessageService bookingImportantMessageService;
    private final BookingImportantMessageMapper bookingImportantMessageMapper;
    private final BookingImportantMessageFilter bookingImportantMessageFilter;

    @Inject
    public BookingImportantMessageHandler(BookingImportantMessageService bookingImportantMessageService, BookingImportantMessageMapper bookingImportantMessageMapper, BookingImportantMessageFilter bookingImportantMessageFilter) {
        this.bookingImportantMessageService = bookingImportantMessageService;
        this.bookingImportantMessageMapper = bookingImportantMessageMapper;
        this.bookingImportantMessageFilter = bookingImportantMessageFilter;
    }

    public List<Message> getAllMessagesForToken(String token, MessageFilter messageFilter) {
        return bookingImportantMessageService.getImportantMessagesByToken(token)
                .stream()
                .map(this::mapToDomain)
                .map(message -> bookingImportantMessageFilter.filter(message, messageFilter))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    public List<Message> getAllMessagesForBookingIdAndEmail(Long bookingId, String email, MessageFilter messageFilter) {
        return bookingImportantMessageService.getImportantMessagesByBookingIdAndEmail(bookingId, email)
                .stream()
                .map(this::mapToDomain)
                .map(message -> bookingImportantMessageFilter.filter(message, messageFilter))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    public Message mapToDomain(com.edreamsodigeo.bookingimportantmessage.contract.v1.model.Message message) {
        if (message instanceof CancellationMessage) {
            return bookingImportantMessageMapper.toContract((CancellationMessage) message);
        } else {
            return null;
        }
    }

}
