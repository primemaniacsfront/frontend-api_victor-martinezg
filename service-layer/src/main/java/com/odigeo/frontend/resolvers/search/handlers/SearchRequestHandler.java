package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.ModuleReleaseInfo;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.search.handlers.pricing.PriceHandler;
import com.odigeo.searchengine.v2.requests.ExternalSelectionRequest;
import com.odigeo.searchengine.v2.requests.ItinerarySearchRequest;
import com.odigeo.searchengine.v2.requests.ItinerarySegmentRequest;
import com.odigeo.searchengine.v2.requests.LocationRequest;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.requests.enums.SearchProductType;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Singleton
public class SearchRequestHandler {

    private static final int MAX_SEARCH_RESULTS = 750;

    @Inject
    private ModuleReleaseInfo moduleReleaseInfo;
    @Inject
    private DateUtils dateUtils;
    @Inject
    private MarketingTrackingHandler marketingTrackingHandler;
    @Inject
    private CabinMapper cabinMapper;
    @Inject
    private SiteVariations siteVariations;
    @Inject
    private PriceHandler priceHandler;
    @Inject
    private PrimeHandler primeHandler;

    public SearchMethodRequest buildSearchRequest(SearchRequest searchRequestDTO, ResolverContext context) {
        SearchMethodRequest searchRequest = new SearchMethodRequest();
        ItinerarySearchRequest itineraryRequest = new ItinerarySearchRequest();
        ItineraryRequest itineraryRequestDTO = searchRequestDTO.getItinerary();

        VisitInformation visit = context.getVisitInformation();
        DebugInfo debugInfo = context.getDebugInfo();

        searchRequest.setSearchRequest(itineraryRequest);
        searchRequest.setBuypath(searchRequestDTO.getBuyPath());
        searchRequest.setVisitInformation(visit.getVisitCode());

        searchRequest.setQaMode(debugInfo.isEnabled());
        searchRequest.setEstimationFees(Boolean.TRUE);
        searchRequest.setClientVersion(moduleReleaseInfo.formatModuleInfo());

        searchRequest.setMarketingTrackingInfo(marketingTrackingHandler.createMktTrackingInfo(context));

        buildItineraryRequest(itineraryRequestDTO, itineraryRequest, context);
        buildExternalSelectionRequest(itineraryRequestDTO, itineraryRequest);
        buildSegmentRequest(itineraryRequestDTO, itineraryRequest);

        return searchRequest;
    }

    private void buildItineraryRequest(ItineraryRequest itineraryRequestDTO, ItinerarySearchRequest itineraryRequest,
                                       ResolverContext context) {

        boolean isResidentDiscountInPax = siteVariations.isResidentDiscountInPax(context.getVisitInformation());

        itineraryRequest.setNumAdults(itineraryRequestDTO.getNumAdults());
        itineraryRequest.setNumChildren(itineraryRequestDTO.getNumChildren());
        itineraryRequest.setNumInfants(itineraryRequestDTO.getNumInfants());
        itineraryRequest.setCabinClass(cabinMapper.map(itineraryRequestDTO.getCabinClass()));
        itineraryRequest.setResident(!isResidentDiscountInPax && itineraryRequestDTO.getResident());
        itineraryRequest.setMainAirportsOnly(itineraryRequestDTO.getMainAirportsOnly());
        itineraryRequest.setSearchMainProductType(SearchProductType.FLIGHT);
        itineraryRequest.setDirectFlightsOnly(Boolean.FALSE);
        itineraryRequest.setDynpackSearch(Boolean.FALSE);
        itineraryRequest.setMaxSize(MAX_SEARCH_RESULTS);

        priceHandler.populatePricePolicy(itineraryRequest, itineraryRequestDTO, primeHandler.isPrimeMarket(context));
    }

    private void buildExternalSelectionRequest(ItineraryRequest itineraryRequestDTO,
                                               ItinerarySearchRequest itineraryRequest) {

        itineraryRequest.setExternalSelectionRequest(Optional
            .ofNullable(itineraryRequestDTO.getExternalSelection())
            .map(externalSelectionDTO -> {
                ExternalSelectionRequest externalSelection = new ExternalSelectionRequest();
                externalSelection.setSearchId(externalSelectionDTO.getSearchId());
                externalSelection.setCollectionMethodId(externalSelectionDTO.getCollectionMethodId());
                externalSelection.setFareItineraryKey(externalSelectionDTO.getFareItineraryKey());
                externalSelection.getSegmentKeys().addAll(externalSelectionDTO.getSegmentKeys());

                externalSelection.setDisplayedCurrency(externalSelectionDTO.getDisplayedCurrency());
                Optional.ofNullable(externalSelectionDTO.getDisplayedPrice())
                    .ifPresent(displayedPrice -> externalSelection.setDisplayedPrice(new BigDecimal(displayedPrice.toString())));

                return externalSelection;
            })
            .orElse(null));
    }

    private void buildSegmentRequest(ItineraryRequest itineraryRequestDTO, ItinerarySearchRequest itineraryRequest) {
        List<ItinerarySegmentRequest> segmentRequests = itineraryRequest.getSegmentRequests();
        List<SegmentRequest> segmentRequestsDTO = itineraryRequestDTO.getSegments();

        for (SegmentRequest segmentRequestDTO : segmentRequestsDTO) {
            ItinerarySegmentRequest itinerarySegmentRequest = new ItinerarySegmentRequest();
            com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationRequest departureDTO = segmentRequestDTO.getDeparture();
            com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationRequest destinationDTO = segmentRequestDTO.getDestination();
            LocationRequest departure = new LocationRequest();
            LocationRequest destination = new LocationRequest();

            departure.setIataCode(departureDTO.getIata());
            departure.setGeoNodeId(departureDTO.getGeoNodeId());
            destination.setIataCode(destinationDTO.getIata());
            destination.setGeoNodeId(destinationDTO.getGeoNodeId());

            itinerarySegmentRequest.setDeparture(departure);
            itinerarySegmentRequest.setDestination(destination);
            itinerarySegmentRequest.setDate(dateUtils.convertFromIsoToCalendar(segmentRequestDTO.getDate()));

            segmentRequests.add(itinerarySegmentRequest);
        }
    }

}
