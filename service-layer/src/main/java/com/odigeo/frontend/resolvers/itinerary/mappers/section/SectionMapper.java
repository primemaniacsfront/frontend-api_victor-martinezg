package com.odigeo.frontend.resolvers.itinerary.mappers.section;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.odigeo.dapi.client.SectionResult;
import com.odigeo.frontend.resolvers.itinerary.mappers.TransportTypeMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.carrier.CarrierMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.location.LocationMapper;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(uses = {CarrierMapper.class, LocationMapper.class, TransportTypeMapper.class, CabinClassMapper.class})
public interface SectionMapper {

    List<Section> sectionIdsContractToModel(List<Integer> ids, @Context ShoppingCartContext context);

    @Mapping(target = "id", ignore = true)
    Section sectionIdContractToModel(Integer id, @Context ShoppingCartContext context);

    @Mapping(source = "result.section.from", target = "departure")
    @Mapping(source = "result.section.to", target = "destination")
    @Mapping(source = "result.section.baggageAllowanceQuantity", target = "baggageAllowance")
    @Mapping(source = "result.section.departureDate", target = "departureDate")
    @Mapping(source = "result.section.arrivalDate", target = "arrivalDate")
    @Mapping(source = "result.section.carrier", target = "carrier")
    @Mapping(source = "result.section.operatingCarrier", target = "operatingCarrier")
    @Mapping(source = "result.section.cabinClass", target = "cabinClass")
    @Mapping(source = "result.section.arrivalTerminal", target = "arrivalTerminal")
    @Mapping(source = "result.section.departureTerminal", target = "departureTerminal")
    @Mapping(source = "result.section.technicalStops", target = "technicalStops")
    @Mapping(source = "result.section.vehicleModel", target = "vehicleModel")
    @Mapping(source = "result.section.id", target = "flightNumber")
    @Mapping(source = "result.section.duration", target = "duration")
    @Mapping(source = "provider", target = "providerCode")
    Section sectionContractToModel(SectionResult result, String provider, @Context ShoppingCartContext context);

}
