package com.odigeo.frontend.resolvers.shoppingcart.buyer;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Buyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BuyerInformationDescription;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.shoppingcart.buyer.handlers.BuyerHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;

@Singleton
public class BuyerResolver implements Resolver {

    static final String SHOPPING_CART_TYPE = "ShoppingCart";
    static final String BUYER_FIELD = "buyer";
    static final String BUYER_INFO_FIELD = "requiredBuyerInformation";

    @Inject
    private BuyerHandler buyerHandler;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type(TypeRuntimeWiring.newTypeWiring(SHOPPING_CART_TYPE)
                .dataFetcher(BUYER_FIELD, this::buyerDataFetcher))
            .type(TypeRuntimeWiring.newTypeWiring(SHOPPING_CART_TYPE)
                .dataFetcher(BUYER_INFO_FIELD, this::buyerInfoDataFetcher));
    }

    Buyer buyerDataFetcher(DataFetchingEnvironment env) {
        return buyerHandler.map(env.getGraphQlContext());
    }

    BuyerInformationDescription buyerInfoDataFetcher(DataFetchingEnvironment env) {
        return buyerHandler.mapBuyerInfo(env.getGraphQlContext());
    }


}
