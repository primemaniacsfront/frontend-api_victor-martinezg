package com.odigeo.frontend.resolvers.trip.mapper;

import org.mapstruct.Mapper;

import java.util.Calendar;
import java.util.Date;

@Mapper
public interface DateMapper {

    default long mapDateToTimeStamp(Calendar date) {
        return date.getTimeInMillis();
    }

    default long mapDateToTimeStamp(Date date) {
        return date.getTime();
    }
}
