package com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans;

import java.util.HashMap;

public class AncillaryPolicyConfiguration extends HashMap<String, AncillaryConfiguration> {

    private static final long serialVersionUID = -358224902668018909L;

    // HashMap<policy, ancillaryConfiguration>
    public AncillaryPolicyConfiguration() {
        // default constructor
    }

}
