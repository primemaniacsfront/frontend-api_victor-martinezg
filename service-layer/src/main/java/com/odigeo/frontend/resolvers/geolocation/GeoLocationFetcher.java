package com.odigeo.frontend.resolvers.geolocation;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeoLocation;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoServiceException;
import graphql.schema.DataFetchingEnvironment;

import java.util.Map;

@Singleton
public class GeoLocationFetcher {

    public static final String GEO_NODE_ID = "GeoNodeId";
    public static final String LOCALE = "Locale";
    @Inject
    private GeoLocationHandler geoLocationHandler;
    @Inject
    private JsonUtils jsonUtils;

    public GeoLocation getGeoLocation(DataFetchingEnvironment env) {
        Integer geonodeId;
        String locale;
        if (env.getArguments() != null) {
            geonodeId = jsonUtils.fromDataFetching(env);
            locale = ((VisitInformation) env.getGraphQlContext().get(VisitInformation.class)).getLocale();
        } else {
            Map<String, Object> localContext = env.getLocalContext();
            geonodeId = (Integer) localContext.get(GEO_NODE_ID);
            locale = (String) localContext.get(LOCALE);
        }
        try {
            return geoLocationHandler.retrieveGeoLocation(geonodeId, locale);
        } catch (GeoNodeNotFoundException | GeoServiceException e) {
            throw new ServiceException("Exception at geoservice", e, ServiceName.GEO_SERVICE);
        }
    }
}
