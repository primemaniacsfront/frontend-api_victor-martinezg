package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;

@Singleton
public class ContentKeyMapper {

    public AccommodationProviderKeyDTO extractProviderKeyFromContentKey(String contentKey) {
        String[] key = contentKey.split("#");
        return new AccommodationProviderKeyDTO(key[0], key[1]);
    }

    public String buildContentKey(String providerId, String providerHotelId) {
        return providerId + "#" + providerHotelId;
    }
}
