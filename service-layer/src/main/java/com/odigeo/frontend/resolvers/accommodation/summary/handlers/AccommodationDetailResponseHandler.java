package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationDetailMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetailResponse;

@Singleton
public class AccommodationDetailResponseHandler {

    @Inject
    private AccommodationDetailMapper accommodationDetailMapper;
    @Inject
    private AccommodationImageHandler accommodationImageHandler;

    public AccommodationSummaryResponseDTO getAccommodationDetails(AccommodationDetailResponse response) {
        AccommodationSummaryResponseDTO responseDTO = new AccommodationSummaryResponseDTO();
        
        if (response.getAccommodationDetail() != null) {
            responseDTO.setAccommodationDetail(accommodationDetailMapper.map(response.getAccommodationDetail()));
            accommodationImageHandler.removeDuplicatedImages(responseDTO.getAccommodationDetail());
        }

        return responseDTO;
    }

}
