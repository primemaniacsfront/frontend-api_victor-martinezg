package com.odigeo.frontend.resolvers.accommodation.product.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationBuyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateAccommodationProductResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Guest;
import com.odigeo.frontend.commons.context.ResolverContext;

import java.util.List;

public interface CreateProductAccommodationHandler {
    CreateAccommodationProductResponse createProductAccommodation(String dealId, List<Guest> guests, AccommodationBuyer buyer, ResolverContext resolverContext);
}
