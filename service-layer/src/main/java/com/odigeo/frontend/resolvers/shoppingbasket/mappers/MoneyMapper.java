package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.google.inject.Singleton;
import com.odigeo.shoppingbasket.v2.model.Price;

@Singleton
public class MoneyMapper {

    public Money map(Price price) {
        Money money = new Money();

        money.setAmount(price.getAmount());
        money.setCurrency(price.getCurrency().getCurrencyCode());

        return money;
    }
}
