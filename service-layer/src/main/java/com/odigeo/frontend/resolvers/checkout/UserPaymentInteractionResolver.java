package com.odigeo.frontend.resolvers.checkout;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteraction;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteractionRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.checkout.handlers.UserPaymentInteractionHandler;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;


@Singleton
public class UserPaymentInteractionResolver implements Resolver {

    private static final String USER_PAYMENT_INTERACTION = "retrieveUserPaymentInteraction";
    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private UserPaymentInteractionHandler handler;


    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher(USER_PAYMENT_INTERACTION, this::retrieveUserPaymentInteractionFetcher));
    }

    UserPaymentInteraction retrieveUserPaymentInteractionFetcher(DataFetchingEnvironment env) {
        UserPaymentInteractionRequest request = jsonUtils.fromDataFetching(env, UserPaymentInteractionRequest.class);
        return handler.retrieveUserPaymentInteraction(request);
    }

}
