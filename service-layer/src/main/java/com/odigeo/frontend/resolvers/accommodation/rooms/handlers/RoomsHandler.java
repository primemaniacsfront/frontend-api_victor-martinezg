package com.odigeo.frontend.resolvers.accommodation.rooms.handlers;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.hcsapi.v9.beans.RoomsDetailResponse;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;

public interface RoomsHandler {

    SearchRoomResponse getRoomAvailability(Long searchId, Long accommodationDealKey, VisitInformation visit);

    RoomsDetailResponse getStaticRoomDetails(String providerId, String providerHotelId, VisitInformation visit, String... roomId);

}
