package com.odigeo.frontend.resolvers.accommodation.summary.models;

import com.odigeo.frontend.resolvers.accommodation.commons.model.BoardTypeDTO;

public class RoomDealSummaryDTO {
    protected String roomId;
    protected String roomKey;
    protected String description;
    protected String bedsDescription;
    protected String cancelPolicy;
    protected RoomSmokingPreferenceDTO smokingPreference;
    protected String providerId;
    protected Boolean isCancellationFree;
    protected Boolean depositRequired;
    protected BoardTypeDTO boardType;
    protected Boolean paymentAtDestination;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomKey() {
        return roomKey;
    }

    public void setRoomKey(String roomKey) {
        this.roomKey = roomKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBedsDescription() {
        return bedsDescription;
    }

    public void setBedsDescription(String bedsDescription) {
        this.bedsDescription = bedsDescription;
    }

    public String getCancelPolicy() {
        return cancelPolicy;
    }

    public void setCancelPolicy(String cancelPolicy) {
        this.cancelPolicy = cancelPolicy;
    }

    public RoomSmokingPreferenceDTO getSmokingPreference() {
        return smokingPreference;
    }

    public void setSmokingPreference(RoomSmokingPreferenceDTO smokingPreference) {
        this.smokingPreference = smokingPreference;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public Boolean getCancellationFree() {
        return isCancellationFree;
    }

    public void setCancellationFree(Boolean cancellationFree) {
        isCancellationFree = cancellationFree;
    }

    public Boolean getDepositRequired() {
        return depositRequired;
    }

    public void setDepositRequired(Boolean depositRequired) {
        this.depositRequired = depositRequired;
    }

    public BoardTypeDTO getBoardType() {
        return boardType;
    }

    public void setBoardType(BoardTypeDTO boardType) {
        this.boardType = boardType;
    }

    public Boolean getPaymentAtDestination() {
        return paymentAtDestination;
    }

    public void setPaymentAtDestination(Boolean paymentAtDestination) {
        this.paymentAtDestination = paymentAtDestination;
    }
}
