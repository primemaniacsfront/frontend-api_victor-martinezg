package com.odigeo.frontend.resolvers.ancillary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AncillaryResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectAncillariesRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedAncillariesResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.ancillary.seat.mappers.SeatMapSegmentMapper;
import com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection.AncillarySelectionRequestMapper;
import com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection.SelectedAncillariesResponseMapper;
import com.odigeo.frontend.services.ancillary.AncillaryService;
import com.odigeo.itineraryapi.v1.request.AncillariesServicesType;
import com.odigeo.itineraryapi.v1.response.AncillaryOptionsContainer;
import com.odigeo.itineraryapi.v1.request.SelectAncillaryRequest;
import com.odigeo.itineraryapi.v1.response.AvailableAncillariesOptionsResponse;
import com.odigeo.itineraryapi.v1.response.SelectAncillaryServiceResponse;

import java.util.Optional;
import java.util.Set;

@Singleton
public class AncillaryHandler {
    @Inject
    private AncillaryService ancillaryService;
    @Inject
    private SeatMapSegmentMapper seatMapSegmentMapper;
    @Inject
    private AncillarySelectionRequestMapper ancillarySelectionRequestMapper;
    @Inject
    private SelectedAncillariesResponseMapper selectedAncillariesResponseMapper;

    public AncillaryResponse getAncillaries(long bookingId,
                                            Set<AncillariesServicesType> ancillariesServices,
                                            ResolverContext context) {

        AncillaryResponse response = new AncillaryResponse();

        AvailableAncillariesOptionsResponse availableAncillariesResponse =
            ancillaryService.ancillariesOptions(bookingId, ancillariesServices, context);

        AncillaryOptionsContainer ancillariesContainer = Optional.ofNullable(availableAncillariesResponse)
                .map(AvailableAncillariesOptionsResponse::getAncillaryOptionsContainer)
                .orElseThrow(() -> new ServiceException(String.format("provided %d bookingID is not valid", bookingId), ServiceName.ITINERARY_API));

        response.setSeats(seatMapSegmentMapper.map(ancillariesContainer));

        return response;
    }

    public SelectedAncillariesResponse selectAncillaries(SelectAncillariesRequest request,
                                                         ResolverContext context) {

        long bookingID = Long.parseLong(request.getBookingID());
        long shoppingBasketID = Long.parseLong(request.getShoppingBasketID());

        SelectAncillaryRequest itineraryApiRequest = new SelectAncillaryRequest();
        itineraryApiRequest.setAncillarySelectionRequestList(ancillarySelectionRequestMapper.map(request.getSelection()));
        itineraryApiRequest.setShoppingBasketId(shoppingBasketID);
        itineraryApiRequest.setVisitId(context.getVisitInformation().getVisitId().toString());
        itineraryApiRequest.setVisitInformation(context.getVisitInformation().getVisitCode());
        itineraryApiRequest.setQaMode(context.getDebugInfo().isEnabled());

        SelectAncillaryServiceResponse itineraryApiResponse =
                ancillaryService.selectAncillaries(bookingID, itineraryApiRequest);

        return selectedAncillariesResponseMapper.map(itineraryApiResponse);
    }
}
