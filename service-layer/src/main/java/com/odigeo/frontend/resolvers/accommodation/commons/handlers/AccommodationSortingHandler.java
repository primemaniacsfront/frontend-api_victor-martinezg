package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.accommodation.scoring.service.AccommodationScores;
import com.odigeo.accommodation.scoring.service.AccommodationScoringService;
import com.odigeo.accommodation.scoring.service.GetScoresRequest;
import com.odigeo.accommodation.scoring.service.ScoringServiceException;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationScoringRequestDTO;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.AccommodationScoringRequestMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.searchengine.v2.requests.AccommodationSearchRequest;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;

import java.math.BigDecimal;
import java.util.List;

@Singleton
public class AccommodationSortingHandler {

    @Inject
    private Logger logger;

    private static final String OFFLINE = "OFFLINE";

    @Inject
    private AccommodationScoringRequestMapper accommodationScoringRequestMapper;
    @Inject
    private AccommodationScoringService accommodationScoringService;


    public void populateScoring(VisitInformation visit, AccommodationSearchResponseDTO response, SearchMethodRequest searchRequest) {
        AccommodationSearchRequest accommodationSearchRequest = (AccommodationSearchRequest) searchRequest.getSearchRequest();
        try {
            AccommodationScores accommodationScores = getAccommodationScores(
                    getAccommodationScoringRequestDTO(visit, response, accommodationSearchRequest));
            remapScores(accommodationScores, response);
        } catch (ScoringServiceException e) {
            logger.error(this.getClass(), "Error calculating scoring", e);
        }

    }

    private AccommodationScoringRequestDTO getAccommodationScoringRequestDTO(VisitInformation visit, AccommodationSearchResponseDTO response, AccommodationSearchRequest accommodationSearchRequest) {
        return new AccommodationScoringRequestDTO(response.getSearchId(), accommodationSearchRequest.getCheckInDate(), accommodationSearchRequest.getCheckOutDate(), accommodationSearchRequest.getRoomRequests(), accommodationSearchRequest.getSearchType(), accommodationSearchRequest.getDestination(), response.getAccommodationResponse(), visit);
    }

    private void remapScores(AccommodationScores accommodationScores, AccommodationSearchResponseDTO response) {
        List<SearchAccommodationDTO> accommodations = response.getAccommodationResponse().getAccommodations();
        accommodations.forEach(a -> a.setScore(findScoreByDedupId(accommodationScores, a.getDedupId())));
    }

    private BigDecimal findScoreByDedupId(AccommodationScores accommodationScores, Integer dedupId) {
        return accommodationScores.getAccommodationScores().stream().filter(s -> s.getInternalAccommodationId().equals(dedupId)).findAny().map(score -> score.getScores().get(OFFLINE)).orElse(BigDecimal.ZERO);
    }

    private AccommodationScores getAccommodationScores(AccommodationScoringRequestDTO accommodationScoringRequestDTO) throws ScoringServiceException {
        GetScoresRequest request = accommodationScoringRequestMapper.map(accommodationScoringRequestDTO);
        return accommodationScoringService.getScores(request);
    }

    public void sortByScore(AccommodationSearchResponseDTO response) {
        response.getAccommodationResponse().getAccommodations().sort(AccommodationSortingHandler::scoreSorting);
    }

    private static int scoreSorting(SearchAccommodationDTO t1, SearchAccommodationDTO t2) {
        return t2.getScore().compareTo(t1.getScore());
    }
}
