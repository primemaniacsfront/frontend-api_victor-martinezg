package com.odigeo.frontend.resolvers.itinerary.mappers.section;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CabinClass;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ValueMapping;

@Mapper
public interface CabinClassMapper {

    @ValueMapping(source = MappingConstants.NULL, target = "TOURIST")
    @ValueMapping(source = MappingConstants.ANY_REMAINING, target = "TOURIST")
    CabinClass cabinClassToCabinType(com.odigeo.dapi.client.CabinClass cabinClass);

}
