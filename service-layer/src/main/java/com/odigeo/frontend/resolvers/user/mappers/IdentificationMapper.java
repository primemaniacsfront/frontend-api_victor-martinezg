package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Identification;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface IdentificationMapper {


    @Mapping(target = "identificationExpirationDate", dateFormat = "yyyy-MM-dd")
    Identification identificationContractToModel(com.odigeo.userprofiles.api.v2.model.Identification identification);
}
