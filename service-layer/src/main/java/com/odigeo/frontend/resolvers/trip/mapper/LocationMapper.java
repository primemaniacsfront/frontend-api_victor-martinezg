package com.odigeo.frontend.resolvers.trip.mapper;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface LocationMapper {

    @Mapping(source = "geoNodeId", target = "id")
    @Mapping(source = "cityIataCode", target = "cityIata")
    @Mapping(source = "iataCode", target = "iata")
    @Mapping(target = "locationType", ignore = true)
    Location locationContractToModel(com.odigeo.travelcompanion.v2.model.booking.Location location);
}
