package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateMembershipProductRequest;
import com.odigeo.dapi.client.ModifyShoppingCartRequest;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface ModifyMembershipSubscriptionProductMapper {
    ModifyMembershipSubscription fromUpdateMembershipRequest(UpdateMembershipProductRequest updateMembershipProductRequest);

    @Mapping(target = "upsellProduct", ignore = true)
    @Mapping(target = "membershipSubscriptionByEmail.offerId", source = "offerId")
    @Mapping(target = "membershipSubscriptionByEmail.name", source = "names")
    @Mapping(target = "membershipSubscriptionByEmail.lastNames", source = "lastNames")
    @Mapping(target = "membershipSubscriptionByEmail.email", source = "email")
    @Mapping(target = "bookingId", source = "shoppingInfo.shoppingId")
    ModifyShoppingCartRequest toModifyShoppingCartRequest(ModifyMembershipSubscription modifyMembershipSubscription);


}
