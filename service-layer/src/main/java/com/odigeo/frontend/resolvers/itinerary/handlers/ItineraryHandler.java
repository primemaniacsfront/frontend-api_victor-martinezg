package com.odigeo.frontend.resolvers.itinerary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Perks;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.odigeo.dapi.client.ItineraryShoppingItem;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.itinerary.mappers.ItineraryMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import com.odigeo.frontend.resolvers.shoppingcart.perks.handlers.PerksHandler;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;

import java.util.Optional;

@Singleton
public class ItineraryHandler {

    @Inject
    private ItineraryMapper mapper;
    @Inject
    @Named("ItineraryPerksHandler")
    private PerksHandler perksHandler;

    public Itinerary map(DataFetchingEnvironment env) {
        GraphQLContext context = env.getGraphQlContext();
        com.odigeo.dapi.client.Itinerary itinerary = getItinerary(context);
        VisitInformation visit = getVisitInformation(context);
        ShoppingCartContext shoppingCartContext = new ShoppingCartContext(visit);
        Itinerary itineraryMapped = mapper.itineraryContractToModel(itinerary, shoppingCartContext);
        Optional.ofNullable(itineraryMapped).ifPresent(iti -> {
            iti.setPerks((Perks) perksHandler.map(context));
        });
        return itineraryMapped;
    }

    private VisitInformation getVisitInformation(GraphQLContext context) {
        return Optional.ofNullable(context.get(VisitInformation.class))
                .map(VisitInformation.class::cast)
                .orElse(null);
    }

    private com.odigeo.dapi.client.Itinerary getItinerary(GraphQLContext context) {
        return Optional.ofNullable(context.get(ShoppingCartSummaryResponse.class))
                .map(ShoppingCartSummaryResponse.class::cast)
                .map(ShoppingCartSummaryResponse::getShoppingCart)
                .map(ShoppingCart::getItineraryShoppingItem)
                .map(ItineraryShoppingItem::getItinerary)
                .orElse(null);
    }
}
