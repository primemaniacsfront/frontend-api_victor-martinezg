package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.google.inject.Singleton;
import com.odigeo.shoppingbasket.v2.requests.CreateRequest;

@Singleton
public class CreateRequestMapper {

    public CreateRequest map(String bundleId, String visitInformation) {
        CreateRequest createRequest = new CreateRequest();

        createRequest.setBundleId(bundleId);
        createRequest.setVisitInformation(visitInformation);

        return createRequest;
    }
}
