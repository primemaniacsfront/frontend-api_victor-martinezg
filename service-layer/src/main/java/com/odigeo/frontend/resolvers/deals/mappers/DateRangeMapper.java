package com.odigeo.frontend.resolvers.deals.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.DateRange;
import org.mapstruct.Mapper;

@Mapper
public interface DateRangeMapper {

    com.odigeo.marketing.search.price.v1.requests.DateRange dateRangeContractToModel(DateRange dateRange);

}
