package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceUrl;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.insurance.api.last.insurance.ConditionsUrl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class InsuranceUrlsMapper {

    @Inject
    private InsuranceUrlMapper insuranceUrlMapper;

    public List<InsuranceUrl> map(List<ConditionsUrl> conditionsUrls) {
        return Optional.ofNullable(conditionsUrls).orElse(Collections.emptyList()).stream()
            .map(insuranceUrlMapper::map)
            .collect(Collectors.toList());
    }

}
