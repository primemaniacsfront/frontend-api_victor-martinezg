package com.odigeo.frontend.resolvers.prime.utils;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipDuration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipDurationTimeUnit;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.odigeo.frontend.resolvers.prime.models.MembershipRenewalStatus;
import com.odigeo.frontend.resolvers.prime.models.MembershipStatus;
import com.odigeo.frontend.resolvers.prime.models.MembershipType;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Predicate;

public class MembershipUtilsPredicateProvider {
    private static final String RENEWAL_DISABLED = MembershipRenewalStatus.DISABLED.name();
    private static final String ACTIVE_STATUS = MembershipStatus.ACTIVATED.name();
    private static final String EMPLOYEE_TYPE = MembershipType.EMPLOYEE.name();
    private static final String BASIC_FREE_TYPE = MembershipType.BASIC_FREE.name();

    protected static final Predicate<Membership> IS_MEMBERSHIP_RENEWAL_DISABLED = membership ->
            Optional.ofNullable(membership)
                    .map(Membership::getAutoRenewalStatus)
                    .map(RENEWAL_DISABLED::equals)
                    .orElse(Boolean.FALSE);

    protected static final Predicate<Membership> IS_MEMBERSHIP_ACTIVATED = membership ->
            Optional.ofNullable(membership)
                    .map(Membership::getStatus)
                    .map(ACTIVE_STATUS::equals)
                    .orElse(Boolean.FALSE);

    protected static final Predicate<Membership> IS_MEMBERSHIP_EMPLOYEE = membership ->
            Optional.ofNullable(membership)
                    .map(Membership::getType)
                    .map(EMPLOYEE_TYPE::equals)
                    .orElse(Boolean.FALSE);

    protected static final Predicate<Membership> IS_MEMBERSHIP_BASIC_FREE = membership ->
            Optional.ofNullable(membership)
                    .map(Membership::getType)
                    .map(BASIC_FREE_TYPE::equals)
                    .orElse(Boolean.FALSE);

    protected static final Predicate<Membership> IS_MEMBERSHIP_PRICE_ZERO = membership ->
            Optional.ofNullable(membership)
                    .map(Membership::getTotalPrice)
                    .map(Money::getAmount)
                    .map(BigDecimal.ZERO::equals)
                    .orElse(Boolean.TRUE);

    private static final Predicate<Membership> MEMBERSHIP_MONTH_AMOUNT = membership ->
            Optional.ofNullable(membership.getDuration()).map(MembershipDuration::getAmount).map(duration -> duration == 1).orElse(Boolean.FALSE);

    private static final Predicate<Membership> MEMBERSHIP_MONTH_UNIT = membership ->
            Optional.ofNullable(membership.getDuration()).map(MembershipDuration::getTimeUnit).map(MembershipDurationTimeUnit.MONTHS::equals).orElse(Boolean.FALSE);


    protected static final Predicate<Membership> IS_MEMBERSHIP_ONE_MONTH_DURATION = MEMBERSHIP_MONTH_AMOUNT.or(MEMBERSHIP_MONTH_UNIT.negate());

    protected static final Predicate<Membership> IS_FREE_TRIAL = IS_MEMBERSHIP_PRICE_ZERO.and(IS_MEMBERSHIP_ONE_MONTH_DURATION).and(IS_MEMBERSHIP_BASIC_FREE.negate());
    protected static final Predicate<Membership> IS_AUTO_RENEWAL_DEACTIVATED = IS_MEMBERSHIP_RENEWAL_DISABLED.and(IS_MEMBERSHIP_EMPLOYEE.negate());


}
