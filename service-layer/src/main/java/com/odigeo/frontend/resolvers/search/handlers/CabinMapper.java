package com.odigeo.frontend.resolvers.search.handlers;

import com.google.inject.Singleton;
import com.odigeo.searchengine.v2.common.CabinClass;
import java.util.Optional;
import org.apache.commons.lang3.EnumUtils;

@Singleton
public class CabinMapper {

    CabinClass map(String cabinClass) {
        return Optional.ofNullable(cabinClass)
            .map(cabin -> EnumUtils.getEnum(CabinClass.class, cabin))
            .orElse(CabinClass.TOURIST);
    }

}
