package com.odigeo.frontend.resolvers.deals.configuration;

import com.edreams.configuration.ConfiguredInJsonFile;
import com.google.inject.Singleton;

import java.util.List;

@Singleton
@ConfiguredInJsonFile
public class DealsConfiguration {

    private Integer maxPopularDestinations;
    private String aggregateBy;
    private Double maxDistanceInKm;
    private Integer size;
    private Integer maxSearchAgeDays;
    private String tripType;
    private String priceType;
    private List<Integer> baseDestinations;

    public Integer getMaxPopularDestinations() {
        return maxPopularDestinations;
    }

    public void setMaxPopularDestinations(Integer maxPopularDestinations) {
        this.maxPopularDestinations = maxPopularDestinations;
    }

    public String getAggregateBy() {
        return aggregateBy;
    }

    public void setAggregateBy(String aggregateBy) {
        this.aggregateBy = aggregateBy;
    }

    public Double getMaxDistanceInKm() {
        return maxDistanceInKm;
    }

    public void setMaxDistanceInKm(Double maxDistanceInKm) {
        this.maxDistanceInKm = maxDistanceInKm;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getMaxSearchAgeDays() {
        return maxSearchAgeDays;
    }

    public void setMaxSearchAgeDays(Integer maxSearchAgeDays) {
        this.maxSearchAgeDays = maxSearchAgeDays;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public List<Integer> getBaseDestinations() {
        return baseDestinations;
    }

    public void setBaseDestinations(List<Integer> baseDestinations) {
        this.baseDestinations = baseDestinations;
    }
}
