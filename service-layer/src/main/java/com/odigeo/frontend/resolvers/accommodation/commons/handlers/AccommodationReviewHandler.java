package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.accommodation.review.retrieve.bean.RequestDedups;
import com.odigeo.accommodation.review.retrieve.bean.Review;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationReviewMapper;
import com.odigeo.frontend.services.AccommodationReviewService;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Singleton
public class AccommodationReviewHandler {

    @Inject
    private AccommodationReviewService accommodationReviewService;
    @Inject
    private AccommodationReviewMapper accommodationReviewMapper;

    public AccommodationReview getSingleReviewByProvider(Integer dedupId, String reviewProviderCode) {
        RequestDedups requestDedups = new RequestDedups();
        requestDedups.setDedupIds(Collections.singletonList(dedupId));
        Review result = accommodationReviewService.getReviews(requestDedups).stream()
                .filter(Objects::nonNull)
                .findFirst().orElse(null);
        return (result != null && result.getProviderCode().equals(reviewProviderCode)) ? accommodationReviewMapper.map(result) : new AccommodationReview();
    }

    public Map<Integer, AccommodationReview> getReviewsByProvider(List<Integer> dedupIds, String reviewProviderCode) {
        RequestDedups requestDedups = new RequestDedups();
        requestDedups.setDedupIds(dedupIds);
        List<Review> reviews = accommodationReviewService.getReviews(requestDedups)
                .stream()
                .filter(review -> review.getProviderCode().equals(reviewProviderCode))
                .collect(Collectors.toList());
        return accommodationReviewMapper.mapList(reviews);
    }

}
