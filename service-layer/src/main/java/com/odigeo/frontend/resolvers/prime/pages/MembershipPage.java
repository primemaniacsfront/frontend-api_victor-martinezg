package com.odigeo.frontend.resolvers.prime.pages;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPageName;
import graphql.schema.DataFetchingEnvironment;

public interface MembershipPage {

    MembershipPageName getPage(DataFetchingEnvironment env);

}
