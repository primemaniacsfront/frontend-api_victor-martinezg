package com.odigeo.frontend.resolvers.prime.pages;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPageName;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPagesRequest;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.prime.models.MembershipPages;
import com.odigeo.frontend.resolvers.prime.utils.MembershipUtils;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.frontend.services.UserDescriptionService;
import graphql.VisibleForTesting;
import graphql.schema.DataFetchingEnvironment;

import java.util.Map;

public class MembershipPageFactory {
    @Inject
    MembershipHandler membershipHandler;
    @Inject
    private UserDescriptionService userDescriptionService;
    @Inject
    private JsonUtils jsonUtils;
    @Inject
    Map<MembershipPages, MembershipPage> membershipPages;

    public MembershipPageName getPage(DataFetchingEnvironment env) {
        Membership membership = getMembership(env);
        MembershipPages page = MembershipPages.SUBSCRIPTION_FUNNEL;
        if (MembershipUtils.isActivated(membership)) {
            page = MembershipPages.ACTIVE_MEMBERSHIP;
            env.getGraphQlContext().put("Membership", membership);
        }
        return membershipPages.get(page).getPage(env);
    }

    private Membership getMembership(DataFetchingEnvironment env) {
        Membership membership = null;
        if (userDescriptionService.isUserLogged(env.getContext())) {
            membership = membershipHandler.getMembershipFromLoggedUser(env);
        } else {
            membership = getMembershipFromShoppingPersonalInfo(env);
        }
        return membership;
    }

    @VisibleForTesting
    Membership getMembershipFromShoppingPersonalInfo(DataFetchingEnvironment env) {
        MembershipPagesRequest membershipPagesRequest = jsonUtils.fromDataFetching(env, MembershipPagesRequest.class);
        try {
            return membershipHandler.getMembershipFromShoppingPersonalInfo(membershipPagesRequest.getShoppingInfo(), env.getContext());
        } catch (ServiceException e) {
            return null;
        }
    }
}
