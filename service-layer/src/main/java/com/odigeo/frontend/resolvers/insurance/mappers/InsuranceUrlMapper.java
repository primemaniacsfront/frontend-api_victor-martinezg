package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceUrl;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.insurance.api.last.insurance.ConditionsUrl;

@Singleton
public class InsuranceUrlMapper {

    @Inject
    private InsuranceUrlTypeMapper insuranceUrlTypeMapper;

    public InsuranceUrl map(ConditionsUrl conditionsUrl) {
        InsuranceUrl insuranceUrl = new InsuranceUrl();

        insuranceUrl.setUrl(conditionsUrl.getUrl());
        insuranceUrl.setType(insuranceUrlTypeMapper.map(conditionsUrl.getUrlType()));

        return insuranceUrl;
    }

}
