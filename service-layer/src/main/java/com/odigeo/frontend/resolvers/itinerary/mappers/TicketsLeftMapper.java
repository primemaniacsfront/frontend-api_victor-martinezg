package com.odigeo.frontend.resolvers.itinerary.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.Collections;
import java.util.Optional;

@Mapper
public abstract class TicketsLeftMapper {

    @AfterMapping
    public void setTicketsLeft(@MappingTarget Itinerary itinerary) {
        int ticketsLeft = Optional.ofNullable(itinerary).map(Itinerary::getLegs).orElse(Collections.emptyList()).stream()
            .map(leg -> leg.getSegments().stream()
                .mapToInt(segment -> Optional.ofNullable(segment.getSeats()).orElse(0))
                .sum())
            .filter(value -> value > 0)
            .min(Integer::compare)
            .orElse(0);
        itinerary.setTicketsLeft(ticketsLeft);
    }

}
