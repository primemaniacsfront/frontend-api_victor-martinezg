package com.odigeo.frontend.resolvers.prime.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.google.inject.Inject;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.prime.models.MembershipType;
import com.odigeo.frontend.resolvers.prime.models.SourceType;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowGroup;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowInfo;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowTravel;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.membership.MemberUserArea;
import com.odigeo.membership.response.FutureFlight;
import graphql.VisibleForTesting;
import graphql.schema.DataFetchingEnvironment;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RetentionFlowInfoHandler {

    @Inject
    private MembershipHandler membershipHandler;
    @Inject
    private MemberUserArea memberUserAreaService;
    @Inject
    private MetricsHandler metricsHandler;

    public RetentionFlowInfo buildRetentionFlowInfo(DataFetchingEnvironment env) {
        Membership membership = membershipHandler.getMembershipFromLoggedUser(env);
        List<FutureFlight> futureFlights = getFutureFlights(membership.getId());

        RetentionFlowGroup group = RetentionFlowGroup.builder()
                .freeTrial(MembershipType.fromValue(membership.getType()), SourceType.fromValue(membership.getSourceType()), membership.getMonthsDuration())
                .daysFromRenew(LocalDate.parse(membership.getExpirationDate()))
                .upcomingTrips(futureFlights).build();

        RetentionFlowInfo retentionFlowInfo = new RetentionFlowInfo();
        retentionFlowInfo.setFlow(group);

        futureFlights.stream().findFirst()
                .map(futureFlight -> new RetentionFlowTravel(futureFlight.getDepartureDate(), futureFlight.getDestination()))
                .ifPresent(retentionFlowInfo::setTravel);


        return retentionFlowInfo;
    }

    @VisibleForTesting
    List<FutureFlight> getFutureFlights(long membershipId) {
        metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_USER_AREA_GET_FUTURE_FLIGHTS);
        return Optional.ofNullable(memberUserAreaService.getFutureFlights(membershipId)).orElseGet(ArrayList::new);
    }

}
