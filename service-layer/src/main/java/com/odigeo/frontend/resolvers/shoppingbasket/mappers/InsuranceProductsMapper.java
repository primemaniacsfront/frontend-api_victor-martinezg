package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceProduct;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.shoppingbasket.v2.model.ShoppingBasket;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class InsuranceProductsMapper {

    @Inject
    private InsuranceProductMapper insuranceProductMapper;

    public List<InsuranceProduct> map(ShoppingBasket shoppingBasket) {
        return Optional.ofNullable(shoppingBasket.getProducts()).orElse(Collections.emptyList()).stream()
            .map(product -> insuranceProductMapper.map(product))
            .collect(Collectors.toList());
    }

}
