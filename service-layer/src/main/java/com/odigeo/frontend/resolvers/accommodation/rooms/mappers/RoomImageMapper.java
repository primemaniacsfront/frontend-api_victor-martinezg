package com.odigeo.frontend.resolvers.accommodation.rooms.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.commons.model.ImageDTO;
import com.odigeo.hcsapi.v9.beans.RoomImage;

@Singleton
public class RoomImageMapper {

    public ImageDTO map(RoomImage roomImage) {
        ImageDTO dto = new ImageDTO();
        dto.setUrl(roomImage.getUrl());
        dto.setThumbnailUrl(roomImage.getThumbnailUrl());
        return dto;
    }

}
