package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsThemeType;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;

import java.util.Arrays;

@Singleton
public class NonEssentialProductsThemeTypeMapper {

    public NonEssentialProductsThemeType map(AncillaryConfiguration ancillaryConfiguration) {
        return Arrays.stream(NonEssentialProductsThemeType.values())
            .filter(nonEssentialProductsThemeType -> nonEssentialProductsThemeType.name().equals(ancillaryConfiguration.getTheme()))
            .findFirst()
            .orElse(null);
    }
}
