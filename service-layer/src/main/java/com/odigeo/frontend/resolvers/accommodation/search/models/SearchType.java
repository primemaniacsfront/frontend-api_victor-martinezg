package com.odigeo.frontend.resolvers.accommodation.search.models;

import java.util.Optional;

public enum SearchType {
    DYNPACK,
    STANDALONE,
    UPSELL,
    POST_BOOKING;

    SearchType() {
    }

    public String value() {
        return this.name();
    }

    public static SearchType fromValue(String value) {
        return Optional.ofNullable(value).map(SearchType::valueOf).orElse(null);
    }
}
