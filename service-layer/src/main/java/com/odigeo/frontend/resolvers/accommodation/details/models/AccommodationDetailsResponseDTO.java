package com.odigeo.frontend.resolvers.accommodation.details.models;

import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;

public class AccommodationDetailsResponseDTO {
    private AccommodationDetailDTO accommodationDetail;

    public AccommodationDetailDTO getAccommodationDetail() {
        return accommodationDetail;
    }

    public void setAccommodationDetail(AccommodationDetailDTO accommodationDetail) {
        this.accommodationDetail = accommodationDetail;
    }
}
