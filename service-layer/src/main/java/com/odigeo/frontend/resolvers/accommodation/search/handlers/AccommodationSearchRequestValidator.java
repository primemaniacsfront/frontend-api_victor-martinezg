package com.odigeo.frontend.resolvers.accommodation.search.handlers;

import com.edreams.util.Checks;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RoomRequest;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.DateUtils;
import org.apache.commons.collections.CollectionUtils;

import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

public class AccommodationSearchRequestValidator {

    @Inject
    private DateUtils dateUtils;

    private static final int MIN_DAYS_INTERVAL_ACCOMMODATION = 1;
    private static final int MAX_DAYS_INTERVAL_ACCOMMODATION = 30;
    private static final int MAX_ROOMS_ACCOMMODATION = 30;
    private static final int MAX_ADULTS_PER_ROOM = 10;
    private static final int MAX_CHILDREN_PER_ROOM = 10;
    private static final int MAX_CHILD_AGE = 12;

    public void validateAccommodationSearchRequest(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest request) {
        try {
            checkDates(dateUtils.convertFromIsoToLocalDate(request.getCheckInDate()), dateUtils.convertFromIsoToLocalDate(request.getCheckOutDate()));
            checkRoomRequests(request.getRoomRequests());
        } catch (IllegalArgumentException e) {
            throw new ServiceException(e.getMessage(), e, Response.Status.BAD_REQUEST, ErrorCodes.VALIDATION);
        }

    }

    private void checkDates(LocalDate checkInDate, LocalDate checkOutDate) {
        if (checkInDate.isAfter(checkOutDate)) {
            throw new IllegalArgumentException("checkInDate is after checkOutDate");
        }
        Checks.checkValidRange(DAYS.between(checkInDate, checkOutDate), MIN_DAYS_INTERVAL_ACCOMMODATION, MAX_DAYS_INTERVAL_ACCOMMODATION, "accommodationSearchRequest days between checkIn and checkOut");
    }

    private void checkRoomRequests(List<RoomRequest> roomRequests) {
        Checks.checkValidRange(roomRequests.size(), 1, MAX_ROOMS_ACCOMMODATION, "numRooms");
        roomRequests.forEach(roomRequest -> {
            Checks.checkValidRange(roomRequest.getNumAdults(), 1, MAX_ADULTS_PER_ROOM, "numAdults");
            if (CollectionUtils.isNotEmpty(roomRequest.getChildrenAges())) {
                Checks.checkValidRange(roomRequest.getChildrenAges().size(), 0, MAX_CHILDREN_PER_ROOM, "numChildren");
                roomRequest.getChildrenAges().forEach(age -> Checks.checkValidRange(age, 0, MAX_CHILD_AGE, "childrenAges"));
            }
        });
    }
}
