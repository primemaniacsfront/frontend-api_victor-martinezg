package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceUrlType;
import com.google.inject.Singleton;

@Singleton
public class InsuranceUrlTypeMapper {

    public InsuranceUrlType map(String urlType) {
        for (InsuranceUrlType insuranceUrlType : InsuranceUrlType.values()) {
            if (insuranceUrlType.name().equalsIgnoreCase(urlType)) {
                return insuranceUrlType;
            }
        }
        return InsuranceUrlType.SIMPLE;
    }

}
