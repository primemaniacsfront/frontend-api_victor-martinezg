package com.odigeo.frontend.resolvers.itinerary.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Leg;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Mapper
public abstract class TransportTypeMapper {

    @AfterMapping
    public void setTransportTypeSegment(@MappingTarget Segment segment) {
        segment.setTransportTypes(Optional.ofNullable(segment)
            .map(Segment::getSections)
            .orElse(Collections.emptyList())
            .stream()
            .map(Section::getTransportType)
            .distinct()
            .collect(Collectors.toList()));
    }

    @AfterMapping
    public void setTransportTypeSection(@MappingTarget Section section) {
        boolean departureFlight = Optional.ofNullable(section).map(Section::getDeparture)
            .map(Location::getLocationType).filter(type -> LocationType.AIRPORT == type).isPresent();
        boolean destinationFlight = Optional.ofNullable(section).map(Section::getDestination)
            .map(Location::getLocationType).filter(type -> LocationType.AIRPORT == type).isPresent();
        section.setTransportType((departureFlight && destinationFlight) ? TransportType.PLANE : TransportType.TRAIN);
    }

    @AfterMapping
    public void setTransportTypeItinerary(@MappingTarget Itinerary itinerary) {
        List<TransportType> transportTypes = Optional.ofNullable(itinerary)
            .map(Itinerary::getLegs)
            .orElse(Collections.emptyList())
            .stream()
            .map(Leg::getSegments)
            .flatMap(List::stream)
            .map(Segment::getTransportTypes)
            .flatMap(List::stream)
            .distinct()
            .collect(Collectors.toList());
        itinerary.setTransportTypes(transportTypes);
    }

}
