package com.odigeo.frontend.resolvers.shoppingcart.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCart;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts.NonEssentialProductsMapper;
import com.odigeo.frontend.resolvers.itinerary.mappers.ItineraryMapper;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {ItineraryMapper.class, NonEssentialProductsMapper.class})
public interface ShoppingCartMapper {

    @Mapping(target = "accommodationShoppingItem", ignore = true)
    @Mapping(target = "itinerary", ignore = true)
    @Mapping(target = "buyer", ignore = true)
    @Mapping(target = "requiredBuyerInformation", ignore = true)
    @Mapping(target = "travellers", ignore = true)
    @Mapping(target = "requiredTravellerInformation", ignore = true)
    @Mapping(target = "collectionMethods", ignore = true)
    @Mapping(source = "shoppingCart", target = "nonEssentialProducts")
    ShoppingCart shoppingCartContractToModel(com.odigeo.dapi.client.ShoppingCart shoppingCart, @Context ShoppingCartContext context);
}
