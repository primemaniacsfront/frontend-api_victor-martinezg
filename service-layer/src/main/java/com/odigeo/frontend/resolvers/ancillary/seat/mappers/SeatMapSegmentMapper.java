package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatMapSegment;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryapi.v1.response.AncillaryOptionsContainer;
import com.odigeo.itineraryapi.v1.response.SeatMapResultsResponse;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class SeatMapSegmentMapper {

    @Inject
    private SeatMapMapper seatMapMapper;

    public List<SeatMapSegment> map(AncillaryOptionsContainer ancillaryOptionsContainer) {
        return Optional.ofNullable(ancillaryOptionsContainer)
            .map(AncillaryOptionsContainer::getSeatMapResultsResponse)
            .orElse(Collections.emptySet())
            .stream()
            .map(this::mapSeatMapSegment)
            .collect(Collectors.toList());
    }

    private SeatMapSegment mapSeatMapSegment(SeatMapResultsResponse seatMapResultsResponse) {
        SeatMapSegment seatMapSegment = new SeatMapSegment();

        seatMapSegment.setSegment(seatMapResultsResponse.getSegment());
        seatMapSegment.setSeatMaps(seatMapMapper.map(seatMapResultsResponse.getSeatMapPreferencesDescriptorItemList()));

        return seatMapSegment;
    }
}
