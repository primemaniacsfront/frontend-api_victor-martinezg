package com.odigeo.frontend.resolvers.checkout.mappers;

import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScope;
import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.DebugModeConfiguration;
import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.dapi.DapiConfirmDebugModeConfiguration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CollectionMethodRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MethodTypeCode;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.google.inject.Inject;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.CollectionStatusTestConfiguration;
import com.odigeo.dapi.client.CreditCardCollectionDetailsParametersRequest;
import com.odigeo.dapi.client.FraudRiskLevelTestConfiguration;
import com.odigeo.dapi.client.PaymentDataRequest;
import com.odigeo.dapi.client.ShoppingCartBookingRequest;
import com.odigeo.dapi.client.TestConfigurationConfirmRequest;
import com.odigeo.dapi.client.UserInteractionNeededRequest;
import com.odigeo.frontend.commons.util.QueryParamUtils;
import com.odigeo.frontend.resolvers.checkout.CheckoutFetcherResult;
import com.odigeo.frontend.resolvers.checkout.handlers.dapi.UserInteractionHelper;
import com.odigeo.frontend.resolvers.shoppingcart.models.ConfirmRequest;
import com.odigeo.frontend.services.PaymentInstrumentService;
import graphql.GraphQLContext;
import graphql.GraphQLError;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Objects;

public abstract class CheckoutMapperDecorator implements CheckoutMapper {

    @Inject
    private PaymentInstrumentService paymentInstrumentService;
    @Inject
    private QueryParamUtils queryParamUtils;
    @Inject
    private UserInteractionHelper userInteractionHelper;

    private final CheckoutMapper delegate;

    public CheckoutMapperDecorator(CheckoutMapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public ConfirmRequest buildShoppingCartConfirmRequest(CheckoutRequest request, String currency, GraphQLContext graphQlContext) {
        ConfirmRequest confirmRequest = delegate.buildShoppingCartConfirmRequest(request, currency, graphQlContext);
        if (request != null) {
            buildPaymentDataRequest(confirmRequest.getPaymentData(), request, graphQlContext);
        }
        return confirmRequest;
    }

    @Override
    public CheckoutFetcherResult mapShoppingCartToCheckoutFetcherResult(BookingResponse response, String userPaymentInteractionId, List<GraphQLError> graphQLErrors) {
        CheckoutFetcherResult checkoutFetcherResult = delegate.mapShoppingCartToCheckoutFetcherResult(response, userPaymentInteractionId, graphQLErrors);
        CheckoutResponse checkoutResponse = mapShoppingCartToCheckoutResponse(response, userPaymentInteractionId);
        if (response != null) {
            checkoutFetcherResult.setData(checkoutResponse);
            mapStatusInfoInCheckoutResponse(response, checkoutFetcherResult);
        }
        return checkoutFetcherResult;
    }

    @Override
    public CheckoutResponse mapShoppingCartToCheckoutResponse(BookingResponse response, String paymentInteractionId) {
        CheckoutResponse checkoutResponse = delegate.mapShoppingCartToCheckoutResponse(response, paymentInteractionId);
        if (response != null && response.getShoppingCart() != null && response.getShoppingCart().getShoppingCart() != null) {
            checkoutResponse.setShoppingCart(delegate.mapShoppingCartNotConfirmed(response.getShoppingCart().getShoppingCart()));
        } else if (response != null && response.getBookingProcessingSummaryResponse() != null && response.getBookingProcessingSummaryResponse().getBookingSummary() != null) {
            checkoutResponse.setShoppingCart(delegate.mapBookingSummaryOnConfirmResponse(response.getBookingProcessingSummaryResponse().getBookingSummary()));
        }
        setMyTripsRedirectionToken(checkoutResponse);
        return checkoutResponse;
    }

    private void setMyTripsRedirectionToken(CheckoutResponse checkoutResponse) {
        if (checkoutResponse != null && checkoutResponse.getShoppingCart() != null && checkoutResponse.getShoppingCart().getBuyer() != null) {
            checkoutResponse.setMyTripsRedirectionToken(queryParamUtils.generateMyTripsRedirectionToken(checkoutResponse.getShoppingCart().getBookingId(),
                    checkoutResponse.getShoppingCart().getBuyer().getMail()));
        }
    }

    private void buildPaymentDataRequest(PaymentDataRequest request, CheckoutRequest checkoutRequest, GraphQLContext graphQlContext) {
        buildBookingRequest(request.getBookingRequest(), checkoutRequest);
        request.setTestConfigurationConfirmRequest(buildTestConfigConfirmRequest(graphQlContext));
    }

    private void buildBookingRequest(ShoppingCartBookingRequest request, CheckoutRequest checkoutRequest) {
        CollectionMethodRequest methodRequest = checkoutRequest.getCollectionMethod();
        if (PaymentMethod.CREDITCARD.equals(methodRequest.getCollectionMethodType())) {
            CreditCardPciScope creditCardInfo = paymentInstrumentService.getCreditCardFromPaymentInstrumentToken(
                    methodRequest.getPaymentInstrumentToken());
            request.setCreditCardRequest(buildCreditCardRequest(creditCardInfo, methodRequest.getMethodTypeCode()));
        }
        request.setUserInteractionNeededRequest(buildUserInteractionNeededRequest());
    }

    private CreditCardCollectionDetailsParametersRequest buildCreditCardRequest(CreditCardPciScope creditCardInfo, MethodTypeCode cardTypeCode) {
        CreditCardCollectionDetailsParametersRequest request = new CreditCardCollectionDetailsParametersRequest();
        request.setCardNumber(creditCardInfo.getNumber());
        request.setCardExpirationMonth(creditCardInfo.getExpirationMonth());
        request.setCardExpirationYear(creditCardInfo.getExpirationYear());
        request.setCardOwner(creditCardInfo.getHolder());
        request.setCardSecurityNumber(creditCardInfo.getCvv());
        request.setCardTypeCode(cardTypeCode.toString());
        return request;
    }


    private TestConfigurationConfirmRequest buildTestConfigConfirmRequest(GraphQLContext graphQlContext) {
        DebugModeConfiguration debugModeConfiguration = graphQlContext.get(DebugModeConfiguration.class);
        TestConfigurationConfirmRequest testConfigurationConfirmRequest = new TestConfigurationConfirmRequest();
        testConfigurationConfirmRequest.setFraudRiskLevel(FraudRiskLevelTestConfiguration.NO_RISK_LEVEL);
        if (debugModeConfiguration != null && debugModeConfiguration.getDapiConfirmDebug() != null) {
            DapiConfirmDebugModeConfiguration dapiConfirmDebug = debugModeConfiguration.getDapiConfirmDebug();
            testConfigurationConfirmRequest.setCollectionStatus(dapiConfirmDebug.getCollectionStatus() != null ? CollectionStatusTestConfiguration.fromValue(dapiConfirmDebug.getCollectionStatus().name()) : null);
        }
        return testConfigurationConfirmRequest;
    }

    private void mapStatusInfoInCheckoutResponse(BookingResponse bookingResponse, CheckoutFetcherResult checkoutFetcherResult) {
        if (bookingResponse.getShoppingCart() != null && CollectionUtils.isNotEmpty(bookingResponse.getShoppingCart().getMessages())) {
            checkoutFetcherResult.getData().getStatusInfo().setCode(mapDapiCodeErrorToCheckoutFailure(Objects.requireNonNull(bookingResponse.getShoppingCart().getMessages().stream()
                    .findFirst().orElse(null)).getCode()));
            checkoutFetcherResult.getData().getStatusInfo().setMessage(Objects.requireNonNull(bookingResponse.getShoppingCart().getMessages().stream()
                    .findFirst().orElse(null)).getDescription());
        } else if (CollectionUtils.isNotEmpty(bookingResponse.getMessages())) {
            checkoutFetcherResult.getData().getStatusInfo().setCode(mapDapiCodeErrorToCheckoutFailure(Objects.requireNonNull(bookingResponse.getMessages().stream()
                    .findFirst().orElse(null)).getCode()));
            checkoutFetcherResult.getData().getStatusInfo().setMessage(Objects.requireNonNull(bookingResponse.getMessages().stream()
                    .findFirst().orElse(null)).getDescription());
        } else if (bookingResponse.getFlowErrors() != null) {
            checkoutFetcherResult.getData().getStatusInfo().setCode(mapDapiFlowErrorToCheckoutFailure(Objects.requireNonNull(bookingResponse.getFlowErrors().getFlowError().stream()
                    .findFirst().orElse(null)).getErrorType()));
            checkoutFetcherResult.getData().getStatusInfo().setMessage(Objects.requireNonNull(bookingResponse.getFlowErrors().getFlowError().stream()
                    .findFirst().orElse(null)).getErrorMessageKey());
        }
    }

    private UserInteractionNeededRequest buildUserInteractionNeededRequest() {
        UserInteractionNeededRequest request = new UserInteractionNeededRequest();
        request.setReturnUrl(userInteractionHelper.getReturnUrlTag());
        return request;
    }

}
