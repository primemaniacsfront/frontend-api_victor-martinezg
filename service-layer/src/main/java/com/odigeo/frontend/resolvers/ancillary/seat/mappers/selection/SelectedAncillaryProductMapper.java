package com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AncillaryProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedAncillaryProduct;
import com.odigeo.frontend.commons.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.collections4.MapUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Singleton
public class SelectedAncillaryProductMapper {

    @Inject
    private Logger logger;

    public List<SelectedAncillaryProduct> map(Map<String, String> products) {
        return MapUtils.emptyIfNull(products)
                .entrySet()
                .stream()
                .filter(this::filterValidValues)
                .map(this::mapSelectedAncillaryProduct)
                .collect(Collectors.toList());
    }

    private SelectedAncillaryProduct mapSelectedAncillaryProduct(Map.Entry<String, String> product) {
        SelectedAncillaryProduct selectedAncillaryProduct = new SelectedAncillaryProduct();

        selectedAncillaryProduct.setType(AncillaryProductType.valueOf(product.getKey()));
        selectedAncillaryProduct.setId(product.getValue());

        return selectedAncillaryProduct;
    }

    private boolean filterValidValues(Map.Entry<String, String> product) {
        AncillaryProductType ancillaryProductType = null;

        try {
            ancillaryProductType = AncillaryProductType.valueOf(product.getKey());
        } catch (IllegalArgumentException ex) {
            String message = String.format("Cannot map unknown product type %s", product.getKey());
            logger.warning(this.getClass(), message, ex);
        }

        return ancillaryProductType != null;
    }
}
