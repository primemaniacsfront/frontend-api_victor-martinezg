package com.odigeo.frontend.resolvers.search.handlers;

import com.google.inject.Singleton;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.math.NumberUtils;

@Singleton
public class TicketHandler {

    private static final Integer NO_TICKETS_LEFT_INFORMATION = NumberUtils.INTEGER_MINUS_ONE;

    public Integer calculateSeatsLeft(List<LegDTO> itineraryLegs) {
        return itineraryLegs.stream()
            .map(leg -> leg.getSegments().stream()
                .mapToInt(segment -> Optional.ofNullable(segment.getSeats()).orElse(NumberUtils.INTEGER_ZERO))
                .sum())
            .filter(value -> value > NumberUtils.INTEGER_ZERO)
            .min(Integer::compare)
            .orElse(NO_TICKETS_LEFT_INFORMATION);
    }

}
