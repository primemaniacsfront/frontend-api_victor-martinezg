package com.odigeo.frontend.resolvers.geolocation.suggestions.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestion;
import com.odigeo.geoapi.v4.responses.LocationDescriptionSorted;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface GeolocationSuggestionsMapper {

    List<GeolocationSuggestion> map(List<LocationDescriptionSorted> locationDescriptionSortedList);
}
