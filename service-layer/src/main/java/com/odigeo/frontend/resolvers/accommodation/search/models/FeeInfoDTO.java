package com.odigeo.frontend.resolvers.accommodation.search.models;

public class FeeInfoDTO {
    private FeeDetailsDTO searchFee;
    private FeeDetailsDTO paymentFee;

    public FeeDetailsDTO getSearchFee() {
        return searchFee;
    }

    public void setSearchFee(FeeDetailsDTO searchFee) {
        this.searchFee = searchFee;
    }

    public FeeDetailsDTO getPaymentFee() {
        return paymentFee;
    }

    public void setPaymentFee(FeeDetailsDTO paymentFee) {
        this.paymentFee = paymentFee;
    }
}
