package com.odigeo.frontend.resolvers.trip.bookingimportantmessage.mapper;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CancellationRefundStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Cancellation;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CancellationRefundStatusDateMapEntry;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Mapper
public interface CancellationMapper {

    @Mapping(source = "refund", target = "cancellationRefund")
    Cancellation toContract(com.edreamsodigeo.bookingimportantmessage.contract.v1.model.cancellation.Cancellation cancellation);

    default List<CancellationRefundStatusDateMapEntry> toContract(Map<com.edreamsodigeo.bookingimportantmessage.contract.v1.model.cancellation.CancellationRefundStatus, LocalDateTime> cancellationRefundStatusDateMap) {
        if (Objects.isNull(cancellationRefundStatusDateMap)) {
            return null;
        }
        return cancellationRefundStatusDateMap
                .entrySet()
                .stream()
                .map(entry -> new CancellationRefundStatusDateMapEntry(toContract(entry.getKey()), entry.getValue().toString()))
                .collect(Collectors.toList());
    }

    CancellationRefundStatus toContract(com.edreamsodigeo.bookingimportantmessage.contract.v1.model.cancellation.CancellationRefundStatus cancellationRefundStatus);

}
