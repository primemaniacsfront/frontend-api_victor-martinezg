package com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans;

public enum PricePerceptionType {
    PERPLAN,
    PERPAX,
    PERSEGMENT,
    INCLUDED
}
