package com.odigeo.frontend.resolvers.deals.handlers;

import com.edreamsodigeo.seo.sslppopularitystats.popularity.v1.model.responses.CityBookingAggregation;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.mappers.GeoCoordinatesMapper;
import com.odigeo.frontend.commons.util.GeoUtils;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.resolvers.deals.configuration.DealsConfiguration;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.frontend.services.PopularityStatsService;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Coordinates;
import com.odigeo.marketing.search.price.v1.requests.IataPair;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class DealsHelper {

    @Inject
    private DealsConfiguration dealsConfiguration;
    @Inject
    private GeoService geoService;
    @Inject
    private PopularityStatsService popularityStatsService;
    @Inject
    private GeoUtils geoUtils;
    @Inject
    private GeoCoordinatesMapper geoCoordinatesMapper;

    Map<String, City> getPopularDestinationCities(City departureCity, Site site) {
        return Stream.concat(
                dealsConfiguration.getBaseDestinations().stream(),
                getPopularDestinations(site, dealsConfiguration.getAggregateBy(), dealsConfiguration.getMaxPopularDestinations())
            )
            .filter(id -> !id.equals(departureCity.getGeoNodeId()))
            .distinct()
            .map(this::getCity)
            .filter(city -> hasValidIata(city) && withinMaxDistance(city.getCoordinates(), departureCity.getCoordinates(), dealsConfiguration.getMaxDistanceInKm()))
            .collect(Collectors.toMap(City::getIataCode, City::getCity));
    }

    Set<IataPair> getRoutesIataPairs(City departure, Collection<City> destinations) {
        return destinations
                .stream()
                .map(city -> new IataPair.Builder().departureIata(departure.getIataCode()).arrivalIata(city.getIataCode()).build())
                .collect(Collectors.toSet());
    }

    City getCity(Integer geoNodeId) {
        try {
            return geoService.getCityByGeonodeId(geoNodeId);
        } catch (GeoNodeNotFoundException e) {
            throw new ServiceException(String.format("GeoNodeId not found: %d", geoNodeId), e, ServiceName.GEO_SERVICE);
        }
    }

    boolean hasValidIata(City city) {
        return Optional.ofNullable(city.getIataCode())
                .map(StringUtils::isNotEmpty)
                .orElse(false);
    }

    private Stream<Integer> getPopularDestinations(Site site, String aggregateBy, Integer max) {
        return popularityStatsService.getPopularDestinationCities(site, aggregateBy, max)
                .stream()
                .map(CityBookingAggregation::getCityGeonodeId);
    }

    private boolean withinMaxDistance(Coordinates dep, Coordinates dest, Double maxDistanceInKm) {
        GeoCoordinatesDTO depCoordinates = geoCoordinatesMapper.map(dep);
        GeoCoordinatesDTO destCoordinates = geoCoordinatesMapper.map(dest);

        return geoUtils.calculateDistance(depCoordinates, destCoordinates) <= maxDistanceInKm;
    }

}
