package com.odigeo.frontend.resolvers.trip.mapper;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripItinerary;
import com.odigeo.travelcompanion.v2.model.booking.ItineraryProviderBookings;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {DateMapper.class, LocationMapper.class, TripSectionMapper.class})
public interface ItineraryMapper {

    @Mapping(source = "bookingItinerary.type", target = "itineraryType")
    @Mapping(source = "bookingItinerary.tripType", target = "tripType")
    @Mapping(source = "bookingItinerary.arrival", target = "destination")
    @Mapping(source = "bookingItinerary.arrivalDate", target = "arrivalDate")
    @Mapping(source = "bookingItinerary.departure", target = "origin")
    @Mapping(source = "bookingItinerary.departureDate", target = "departureDate")
    @Mapping(source = "itinerarySegments", target = "legs")
    TripItinerary mapToTripItineraryContract(ItineraryProviderBookings itineraryProviderBookings);
}
