package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSummaryRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.dapi.client.AccommodationStaticInformation;
import com.odigeo.dapi.client.RoomGroupDeal;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationReviewHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationSummaryContext;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationSummaryMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.City;
import graphql.schema.DataFetchingEnvironment;

import java.util.Optional;

@Singleton
public class AccommodationSummaryShoppingCartHandler implements AccommodationSummaryHandler {

    @Inject
    private AccommodationSearchCriteriaRetrieveHandler accommodationSearchCriteriaRetrieveHandler;
    @Inject
    private GeoLocationHandler geolocationHandler;
    @Inject
    private AccommodationDetailRetrieveHandler accommodationDetailRetrieveHandler;
    @Inject
    private AccommodationReviewHandler accommodationReviewHandler;
    @Inject
    private AccommodationSummaryMapper accommodationSummaryMapper;
    @Inject
    private Logger logger;

    @Override
    public GeoLocationHandler getGeolocationHandler() {
        return geolocationHandler;
    }

    @Override
    public AccommodationSearchCriteriaRetrieveHandler getAccommodationSearchCriteriaRetrieveHandler() {
        return accommodationSearchCriteriaRetrieveHandler;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public AccommodationSummaryResponseDTO accommodationSummaryFromShopping(AccommodationSummaryRequest request, DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        VisitInformation visit = context.getVisitInformation();
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = accommodationDetailRetrieveHandler.getShoppingCart(request.getBookingId(), context);
        AccommodationReview review = Optional.ofNullable(shoppingCartSummaryResponse)
            .map(ShoppingCartSummaryResponse::getShoppingCart)
            .map(ShoppingCart::getAccommodationShoppingItem)
            .map(AccommodationShoppingItem::getAccommodationStaticInformation)
            .map(AccommodationStaticInformation::getDedupId)
            .map(dedupId -> accommodationReviewHandler.getSingleReviewByProvider(dedupId, REVIEW_PROVIDER_ID))
            .orElse(null);
        AccommodationSummaryContext summaryContext = new AccommodationSummaryContext(getDestinationCity(visit.getVisitCode(), shoppingCartSummaryResponse));
        return accommodationSummaryMapper.map(shoppingCartSummaryResponse.getShoppingCart().getAccommodationShoppingItem(), review, summaryContext);
    }

    City getDestinationCity(String visitCode, ShoppingCartSummaryResponse shoppingCartSummaryRS) {
        return Optional.ofNullable(shoppingCartSummaryRS)
            .map(ShoppingCartSummaryResponse::getShoppingCart)
            .map(ShoppingCart::getAccommodationShoppingItem)
            .map(AccommodationShoppingItem::getRoomGroupDeal)
            .map(RoomGroupDeal::getSearchId)
            .map(searchId -> retrieveDestinationGeoNodeId(searchId, visitCode))
            .map(this::getCityByGeoNode)
            .orElse(null);
    }
}
