package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ShoppingBasketResponseMapper {

    @Inject
    private InsuranceProductsMapper insuranceProductsMapper;

    public ShoppingBasketResponse map(com.odigeo.shoppingbasket.v2.responses.ShoppingBasketResponse shoppingBasketResponse) {
        ShoppingBasketResponse shoppingBasket = new ShoppingBasketResponse();

        shoppingBasket.setShoppingBasketId(String.valueOf(shoppingBasketResponse.getShoppingBasket().getId()));
        shoppingBasket.setInsurances(insuranceProductsMapper.map(shoppingBasketResponse.getShoppingBasket()));

        return shoppingBasket;
    }

    public ShoppingBasketResponse map(com.odigeo.shoppingbasket.v3.response.ShoppingBasketResponse shoppingBasketResponse) {
        ShoppingBasketResponse shoppingBasket = new ShoppingBasketResponse();

        shoppingBasket.setShoppingBasketId(String.valueOf(shoppingBasketResponse.getShoppingBasket().getId()));

        return shoppingBasket;
    }
}
