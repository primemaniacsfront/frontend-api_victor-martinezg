package com.odigeo.frontend.resolvers.prime.operations;

import com.odigeo.dapi.client.ShoppingCartSummaryResponse;

import java.util.function.BiFunction;

public interface MembershipSubscriptionShoppingCartAction<T, U> extends BiFunction<T, U, ShoppingCartSummaryResponse> {

}
