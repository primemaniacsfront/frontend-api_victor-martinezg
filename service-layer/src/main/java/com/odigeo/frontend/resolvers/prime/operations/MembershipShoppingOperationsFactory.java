package com.odigeo.frontend.resolvers.prime.operations;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;

import javax.ws.rs.core.Response;

public class MembershipShoppingOperationsFactory {
    @Inject
    private MembershipShoppingCartOperations shoppingCartOperations;

    public MembershipShoppingOperations getMembershipShoppingOperations(ShoppingType shoppingType) {
        if (ShoppingType.CART.equals(shoppingType)) {
            return shoppingCartOperations;
        }
        throw new ServiceException(String.format("shoppingType %s is not implemented", shoppingType), Response.Status.BAD_REQUEST, ServiceName.FRONTEND_API);
    }
}
