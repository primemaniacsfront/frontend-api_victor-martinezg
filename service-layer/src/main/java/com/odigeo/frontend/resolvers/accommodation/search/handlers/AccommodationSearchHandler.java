package com.odigeo.frontend.resolvers.accommodation.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RoomRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationLocationHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationSortingHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationStaticContentHandler;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.AccommodationSearchResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.responses.SearchResponse;

@Singleton
public class AccommodationSearchHandler {

    @Inject
    private AccommodationSortingHandler accommodationSortingHandler;
    @Inject
    private AccommodationStaticContentHandler accommodationStaticContentHandler;
    @Inject
    private AccommodationMetadataHandler accommodationMetadataHandler;
    @Inject
    private AccommodationSearchResponseMapper accommodationSearchResponseMapper;
    @Inject
    private AccommodationSearchReviewsHandler accommodationSearchReviewsHandler;
    @Inject
    private AccommodationSearchRequestHandler accommodationSearchRequestHandler;
    @Inject
    private AccommodationSearchResponseHandler accommodationSearchResponseHandler;
    @Inject
    private AccommodationLocationHandler accommodationLocationHandler;
    @Inject
    private SearchService searchService;
    @Inject
    private DateUtils dateUtils;

    public AccommodationSearchResponse searchAccommodation(AccommodationSearchRequest requestParams, ResolverContext resolverContext, VisitInformation visit) {
        SearchMethodRequest searchRequest = accommodationSearchRequestHandler.buildSearchRequest(requestParams, resolverContext);

        SearchResponse searchResponse = searchService.executeSearch(searchRequest);

        AccommodationSearchResponseDTO response = accommodationSearchResponseHandler.buildSearchResponse(searchResponse, visit, getNumNightsFromRequestParams(requestParams));

        accommodationSearchReviewsHandler.populateReviews(response);
        accommodationStaticContentHandler.populateSearchAccommodationStaticContent(visit, response);
        LocationRequest destination = requestParams.getDestination();
        accommodationLocationHandler.populateSearchedLocation(response, destination.getGeoNodeId(), destination.getIata());

        accommodationSortingHandler.populateScoring(visit, response, searchRequest);

        accommodationSortingHandler.sortByScore(response);

        accommodationMetadataHandler.populateMetadata(response, getNumAdultsFromRequestParams(requestParams), visit);
        return accommodationSearchResponseMapper.mapModelToContract(response);
    }

    private Integer getNumAdultsFromRequestParams(AccommodationSearchRequest requestParams) {
        return requestParams.getRoomRequests().stream().map(RoomRequest::getNumAdults).reduce(0, Integer::sum);
    }

    private Integer getNumNightsFromRequestParams(AccommodationSearchRequest requestParams) {
        return dateUtils.daysBetweenIsoDates(requestParams.getCheckInDate(), requestParams.getCheckOutDate());
    }
}
