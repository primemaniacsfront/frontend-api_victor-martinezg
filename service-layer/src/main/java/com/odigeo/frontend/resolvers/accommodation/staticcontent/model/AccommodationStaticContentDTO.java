package com.odigeo.frontend.resolvers.accommodation.staticcontent.model;

import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;

public class AccommodationStaticContentDTO {

    private String contentKey;
    private AccommodationDealInformationDTO accommodationDeal;

    public String getContentKey() {
        return contentKey;
    }

    public void setContentKey(String contentKey) {
        this.contentKey = contentKey;
    }

    public AccommodationDealInformationDTO getAccommodationDeal() {
        return accommodationDeal;
    }

    public void setAccommodationDeal(AccommodationDealInformationDTO accommodationDeal) {
        this.accommodationDeal = accommodationDeal;
    }
}
