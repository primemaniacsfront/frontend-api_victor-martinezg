package com.odigeo.frontend.resolvers.shoppingcart.mappers.baggage;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageCondition;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.Collections;
import java.util.Optional;

@Mapper
public abstract class BaggageConditionMapper {

    @AfterMapping
    public void setBaggageConditionSegment(@MappingTarget Segment segment) {
        segment.setBaggageCondition(BaggageCondition.CABIN_INCLUDED);
        boolean allBaggagesIncluded = Optional.ofNullable(segment).map(Segment::getSections).orElse(Collections.emptyList())
            .stream().allMatch(section -> calculateByQuantityTransportType(section));
        if (allBaggagesIncluded) {
            segment.setBaggageCondition(BaggageCondition.CHECKIN_INCLUDED);
        }
    }

    private boolean calculateByQuantityTransportType(Section section) {
        Integer baggageQuantity = Optional.ofNullable(section).map(Section::getBaggageAllowance).orElse(0);
        boolean departureFlight = Optional.ofNullable(section).map(Section::getDeparture)
            .map(Location::getLocationType).filter(type -> LocationType.AIRPORT == type).isPresent();
        boolean destinationFlight = Optional.ofNullable(section).map(Section::getDestination)
            .map(Location::getLocationType).filter(type -> LocationType.AIRPORT == type).isPresent();
        return Optional.ofNullable(baggageQuantity).map(quantity -> quantity > 0).orElse(false)
            || !(departureFlight && destinationFlight);
    }

}
