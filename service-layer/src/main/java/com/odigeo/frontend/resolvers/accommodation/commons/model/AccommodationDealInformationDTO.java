package com.odigeo.frontend.resolvers.accommodation.commons.model;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ThreeValuedLogic;

public class AccommodationDealInformationDTO {
    private String name;
    private String address;
    private String cityName;
    private String description;
    private String location;
    private String chain;
    private String neighbourhood;
    private AccommodationType type;
    private AccommodationImageDTO mainAccommodationImage;
    private String cancellationPolicyDescription;
    private GeoCoordinatesDTO coordinates;
    private AccommodationCategory category;
    private ThreeValuedLogic cancellationFree;
    private boolean paymentAtDestination;

    private String contentKey;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getChain() {
        return chain;
    }

    public void setChain(String chain) {
        this.chain = chain;
    }

    public AccommodationType getType() {
        return type;
    }

    public void setType(AccommodationType type) {
        this.type = type;
    }

    public AccommodationImageDTO getMainAccommodationImage() {
        return mainAccommodationImage;
    }

    public void setMainAccommodationImage(AccommodationImageDTO mainAccommodationImage) {
        this.mainAccommodationImage = mainAccommodationImage;
    }

    public String getCancellationPolicyDescription() {
        return cancellationPolicyDescription;
    }

    public void setCancellationPolicyDescription(String cancellationPolicyDescription) {
        this.cancellationPolicyDescription = cancellationPolicyDescription;
    }

    public GeoCoordinatesDTO getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(GeoCoordinatesDTO coordinates) {
        this.coordinates = coordinates;
    }

    public AccommodationCategory getCategory() {
        return category;
    }

    public void setCategory(AccommodationCategory category) {
        this.category = category;
    }

    public ThreeValuedLogic getCancellationFree() {
        return cancellationFree;
    }

    public void setCancellationFree(ThreeValuedLogic cancellationFree) {
        this.cancellationFree = cancellationFree;
    }

    public boolean isPaymentAtDestination() {
        return paymentAtDestination;
    }

    public void setPaymentAtDestination(boolean paymentAtDestination) {
        this.paymentAtDestination = paymentAtDestination;
    }

    public String getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getContentKey() {
        return contentKey;
    }

    public void setContentKey(String contentKey) {
        this.contentKey = contentKey;
    }
}
