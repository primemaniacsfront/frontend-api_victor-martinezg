package com.odigeo.frontend.resolvers.prime.utils;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;

import java.util.Optional;

import static com.odigeo.frontend.resolvers.prime.utils.MembershipUtilsPredicateProvider.IS_AUTO_RENEWAL_DEACTIVATED;
import static com.odigeo.frontend.resolvers.prime.utils.MembershipUtilsPredicateProvider.IS_FREE_TRIAL;
import static com.odigeo.frontend.resolvers.prime.utils.MembershipUtilsPredicateProvider.IS_MEMBERSHIP_ACTIVATED;

public final class MembershipUtils {
    private MembershipUtils() {
    }

    public static boolean isActivated(Membership membership) {
        return Optional.ofNullable(membership)
                .filter(IS_MEMBERSHIP_ACTIVATED)
                .isPresent();
    }

    public static boolean isFreeTrial(Membership membership) {
        return Optional.ofNullable(membership)
                .filter(IS_FREE_TRIAL)
                .isPresent();
    }

    public static boolean isAutoRenewalDeactivated(Membership membership) {
        return Optional.ofNullable(membership)
                .filter(IS_AUTO_RENEWAL_DEACTIVATED)
                .isPresent();
    }
}
