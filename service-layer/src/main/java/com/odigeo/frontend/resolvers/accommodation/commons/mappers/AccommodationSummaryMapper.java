package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.odigeo.dapi.client.AccommodationImage;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.dapi.client.RoomDeal;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.RoomDealSummaryDTO;
import org.apache.commons.collections.CollectionUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper
@DecoratedWith(AccommodationSummaryDecorator.class)
public interface AccommodationSummaryMapper {

    @Mapping(source = "accommodationShoppingItem.accommodationDealInformation", target = "accommodationDetail.accommodationDealInformation")
    @Mapping(source = "accommodationShoppingItem.accommodationDealInformation.type", target = "accommodationDetail.hotelType")
    @Mapping(source = "accommodationShoppingItem", target = "roomDeals", qualifiedByName = "mapRoomsDeal")
    @Mapping(source = "accommodationReview", target = "accommodationReview")
    AccommodationSummaryResponseDTO map(AccommodationShoppingItem accommodationShoppingItem, AccommodationReview accommodationReview,
                                        @Context AccommodationSummaryContext accommodationSummaryContext);

    @Mapping(source = "roomDeal.isCancellationFree", target = "cancellationFree")
    @Mapping(source = "roomDeal.description", target = "description")
    @Mapping(source = "roomDeal.key", target = "roomKey")
    @Named("mapRoomDeal")
    RoomDealSummaryDTO mapRoomDeal(RoomDeal roomDeal, @Context AccommodationSummaryContext accommodationSummaryContext);


    AccommodationImageDTO mapAccommodationImage(AccommodationImage accommodationImage, @Context AccommodationSummaryContext accommodationSummaryContext);

    @AfterMapping
    default void mapBedDescription(RoomDeal roomDeal, @MappingTarget RoomDealSummaryDTO roomDealSummaryDTO) {
        if (CollectionUtils.isNotEmpty(roomDeal.getBedsDescriptions())) {
            roomDealSummaryDTO.setBedsDescription(roomDeal.getBedsDescriptions().get(0));
        } else {
            roomDealSummaryDTO.setBedsDescription(roomDeal.getRoomType());
        }
    }

    @AfterMapping
    default void paymentAtDestination(@Context AccommodationSummaryContext context, @MappingTarget RoomDealSummaryDTO roomDeal) {
        roomDeal.setPaymentAtDestination(context.isPaymentAtDestination() && !roomDeal.getDepositRequired());
    }

    @Named("mapRoomsDeal")
    default List<RoomDealSummaryDTO> mapRoomsDeal(AccommodationShoppingItem accommodationShoppingItem,
                                                  @Context AccommodationSummaryContext accommodationSummaryContext) {
        List<RoomDeal> roomsDeal = accommodationShoppingItem.getRoomDealLegend();
        return accommodationShoppingItem.getRoomGroupDeal()
            .getRoomsKeys()
            .stream()
            .map(key -> mapRoomDeal(
                    roomsDeal.stream().filter(room -> room.getKey().equals(key)).findFirst().orElse(null), accommodationSummaryContext
                )
            ).collect(Collectors.toList());
    }

}
