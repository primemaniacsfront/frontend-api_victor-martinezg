package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSummaryRequest;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.resolvers.accommodation.commons.exceptions.CityNotFoundException;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.criteria.AbstractProductCategorySearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.DynPackSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.provider.AccommodationProviderSearchCriteria;
import graphql.schema.DataFetchingEnvironment;

import java.util.Optional;

public interface AccommodationSummaryHandler {

    String REVIEW_PROVIDER_ID = "TA";

    GeoLocationHandler getGeolocationHandler();

    AccommodationSearchCriteriaRetrieveHandler getAccommodationSearchCriteriaRetrieveHandler();

    Logger getLogger();

    AccommodationSummaryResponseDTO accommodationSummaryFromShopping(AccommodationSummaryRequest request, DataFetchingEnvironment env);

    default City getCityByGeoNode(Integer geoNode) {
        City city = null;
        try {
            city = getGeolocationHandler().getCityGeonode(geoNode, null);
        } catch (CityNotFoundException e) {
            getLogger().warning(this.getClass(), "Error retrieving city by geoNode", e);
        }
        return city;
    }

    default Integer retrieveDestinationGeoNodeId(long searchId, String visitCode) {
        SearchResults searchResults = getAccommodationSearchCriteriaRetrieveHandler().getSearchResultsBySearchId(searchId, visitCode);
        return Optional.ofNullable(searchResults)
            .map(SearchResults::getSearchCriteria)
            .map(searchCriteria -> searchCriteria instanceof DynPackSearchCriteria
                ? ((DynPackSearchCriteria) searchCriteria).getAccommodationSearchCriteria()
                : (AccommodationSearchCriteria) searchCriteria
            ).map(AbstractProductCategorySearchCriteria::getProviderSearchCriteria)
            .filter(searchCriteria -> searchCriteria instanceof AccommodationProviderSearchCriteria)
            .map(AccommodationProviderSearchCriteria.class::cast)
            .map(AccommodationProviderSearchCriteria::getDestinationGeoNodeId)
            .orElse(null);
    }
}
