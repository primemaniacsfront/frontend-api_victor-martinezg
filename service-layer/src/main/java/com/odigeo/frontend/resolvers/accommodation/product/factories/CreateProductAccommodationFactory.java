package com.odigeo.frontend.resolvers.accommodation.product.factories;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Inject;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.CreateProductAccommodationHandler;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi.ShoppingCartCreateProductAccommodation;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.productapi.CreateAccommodationProduct;

public class CreateProductAccommodationFactory {

    @Inject
    private ShoppingCartCreateProductAccommodation shoppingCartCreateProductAccommodation;
    @Inject
    private CreateAccommodationProduct createAccommodationProduct;

    public CreateProductAccommodationHandler getInstance(ShoppingType shoppingType) {
        return ShoppingType.BASKET_V3.equals(shoppingType) ? createAccommodationProduct : shoppingCartCreateProductAccommodation;
    }
}
