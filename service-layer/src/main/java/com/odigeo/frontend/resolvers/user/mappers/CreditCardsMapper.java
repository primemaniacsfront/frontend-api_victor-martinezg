package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCards;
import com.odigeo.userprofiles.api.v2.model.CreditCard;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper
@DecoratedWith(CreditCardsMapperDecorator.class)
public interface CreditCardsMapper {

    @Mapping(source = "creditCardNumber", target = "creditCardNumber", qualifiedByName = "hideCreditCards")
    @Mapping(source = "creationDate", target = "creationDate", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "modifiedDate", target = "modifiedDate", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "lastUsageDate", target = "lastUsageDate", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "creditCardTypeId.creditCardCode", target = "creditCardCode")
    CreditCards creditCardsContractToModel(CreditCard creditCard);

    @Named("hideCreditCards")
    default String hideCreditCards(String creditCard) {
        return "****" + creditCard.substring(creditCard.length() - 4);
    }
}
