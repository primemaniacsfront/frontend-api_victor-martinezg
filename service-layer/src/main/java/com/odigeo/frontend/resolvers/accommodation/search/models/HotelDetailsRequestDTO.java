package com.odigeo.frontend.resolvers.accommodation.search.models;

import java.util.List;

public class HotelDetailsRequestDTO {

    private List<AccommodationProviderKeyDTO> providerKeys;


    public List<AccommodationProviderKeyDTO> getProviderKeys() {
        return providerKeys;
    }

    public void setProviderKeys(List<AccommodationProviderKeyDTO> providerKeys) {
        this.providerKeys = providerKeys;
    }
}
