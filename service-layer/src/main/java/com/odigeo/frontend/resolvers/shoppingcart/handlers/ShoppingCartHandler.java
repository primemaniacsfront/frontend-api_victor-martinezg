package com.odigeo.frontend.resolvers.shoppingcart.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCart;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketActionStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.AvailableProductsResponse;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.ModifyShoppingCartRequest;
import com.odigeo.dapi.client.PersonalInfoRequest;
import com.odigeo.dapi.client.ResumeDataRequest;
import com.odigeo.dapi.client.SelectionRequests;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.dapi.client.TestConfigurationSelectionRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.nonessentialproducts.handlers.NonEssentialProductsHandler;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartMapper;
import com.odigeo.frontend.resolvers.shoppingcart.models.ConfirmRequest;
import com.odigeo.frontend.services.DapiService;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class ShoppingCartHandler {

    @Inject
    private DapiService dapiService;
    @Inject
    private ShoppingCartMapper mapper;
    @Inject
    private NonEssentialProductsHandler nonEssentialProductsHandler;

    public ShoppingCart buildShoppingCart(com.odigeo.dapi.client.ShoppingCart shoppingCart, VisitInformation visitInformation) {
        return mapper.shoppingCartContractToModel(shoppingCart, new ShoppingCartContext(visitInformation));
    }

    public ShoppingCartSummaryResponse createShoppingCart(SelectionRequests selectionRequest, ResolverContext context, TestConfigurationSelectionRequest testConfigurationSelectionRequest) {
        return dapiService.createShoppingCart(selectionRequest, testConfigurationSelectionRequest, context);
    }

    public ShoppingCartSummaryResponse addPersonalInfo(PersonalInfoRequest personalInfoRequest, ResolverContext context) {
        return dapiService.addPersonalInfoToShoppingCart(personalInfoRequest, context);
    }

    public BookingResponse confirm(ConfirmRequest request, ResolverContext context) {
        return dapiService.confirmBooking(request.getPaymentData(), request.getBookingId(),
                request.getClientBookingReferencesId(), request.getInvoice(), request.getFraudInfo(), context);
    }

    public BookingResponse resume(long bookingId, ResumeDataRequest request, ResolverContext context) {
        return dapiService.resumeBooking(bookingId, request, context);
    }

    public ShoppingCartSummaryResponse getShoppingCart(long bookingId, ResolverContext context) {
        return dapiService.getShoppingCart(bookingId, context);
    }

    public ShoppingCartSummaryResponse removeProductsToShoppingCart(ModifyShoppingCartRequest modifyShoppingCartRequest, ResolverContext context) {
        return dapiService.removeFromShoppingCart(modifyShoppingCartRequest, context);
    }

    public ShoppingCartSummaryResponse addProductsToShoppingCart(ModifyShoppingCartRequest modifyShoppingCartRequest, ResolverContext context) {
        return dapiService.addToShoppingCart(modifyShoppingCartRequest, context);
    }

    public AvailableProductsResponse getAvailableProductsFromDapi(Long bookingId, ResolverContext context) {
        return dapiService.getAvailableProducts(bookingId, context);
    }

    public List<UpdateProductsInShoppingBasketActionStatus> removeProducts(UpdateProductsInShoppingBasketRequest updateShoppingBasketRequest,
                                                                           AvailableProductsResponse availableProductsResponse, ResolverContext context) {
        ModifyShoppingCartRequest removeProductsRequest = new ModifyShoppingCartRequest();
        removeProductsRequest.setBookingId(Long.parseLong(updateShoppingBasketRequest.getShoppingId()));
        List<UpdateProductsInShoppingBasketActionStatus> updateProductsInShoppingBasketActionStatuses = new ArrayList<>();

        List<String> nonEssentialProductsToRemove = nonEssentialProductsHandler.getNonEssentialProductsIdsToRemove(updateShoppingBasketRequest, availableProductsResponse);
        removeProductsRequest.getInsuranceOffers().addAll(nonEssentialProductsToRemove);

        ShoppingCartSummaryResponse shoppingCartResponse = removeProductsToShoppingCart(removeProductsRequest, context);

        updateProductsInShoppingBasketActionStatuses.addAll(nonEssentialProductsHandler.getNonEssentialProductsActionStatus(nonEssentialProductsToRemove, shoppingCartResponse, availableProductsResponse));

        return updateProductsInShoppingBasketActionStatuses;
    }

    public List<UpdateProductsInShoppingBasketActionStatus> addProducts(UpdateProductsInShoppingBasketRequest updateShoppingBasketRequest,
                                                                        AvailableProductsResponse availableProductsResponse, ResolverContext context) {
        ModifyShoppingCartRequest addProductsRequest = new ModifyShoppingCartRequest();
        addProductsRequest.setBookingId(Long.parseLong(updateShoppingBasketRequest.getShoppingId()));
        List<UpdateProductsInShoppingBasketActionStatus> updateProductsInShoppingBasketActionStatuses = new ArrayList<>();

        List<String> nonEssentialProductsToAdd = nonEssentialProductsHandler.getNonEssentialProductsIdsToAdd(updateShoppingBasketRequest, availableProductsResponse);
        addProductsRequest.getInsuranceOffers().addAll(nonEssentialProductsToAdd);

        ShoppingCartSummaryResponse shoppingCartResponse = addProductsToShoppingCart(addProductsRequest, context);

        updateProductsInShoppingBasketActionStatuses.addAll(nonEssentialProductsHandler.getNonEssentialProductsActionStatus(nonEssentialProductsToAdd, shoppingCartResponse, availableProductsResponse));

        return updateProductsInShoppingBasketActionStatuses;
    }

}
