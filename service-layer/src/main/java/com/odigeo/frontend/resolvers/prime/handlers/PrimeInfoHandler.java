package com.odigeo.frontend.resolvers.prime.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeInfo;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.prime.utils.MembershipUtils;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.frontend.services.PrimeService;
import com.odigeo.frontend.services.UserDescriptionService;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

@Singleton
public class PrimeInfoHandler {

    @Inject
    private PrimeService primeService;
    @Inject
    private MembershipHandler membershipHandler;
    @Inject
    private UserDescriptionService userService;
    @Inject
    private SiteVariations siteVariations;

    public PrimeInfo buildPrimeInfo(ResolverContext context) {
        VisitInformation visit = context.getVisitInformation();
        PrimeInfo primeInfo = new PrimeInfo();

        primeInfo.setIsPrimeSite(isPrimeSite(visit));
        primeInfo.setIsPrimeUser(isPrimeUser(context));
        primeInfo.setIsLoggedPrimeUser(isPrimeUser(context));
        primeInfo.setIsPrimeUserFromCookie(isPrimeUserFromCookies(context));
        return primeInfo;
    }

    private boolean isPrimeSite(VisitInformation visit) {
        return primeService.isPrimeSite(visit);
    }

    private boolean isPrimeUser(ResolverContext context) {
        VisitInformation visit = context.getVisitInformation();
        return Optional.ofNullable(userService.getUserByToken(context))
                .map(user -> membershipHandler.getMembership(user, visit))
                .map(MembershipUtils::isActivated)
                .orElse(Boolean.FALSE);
    }

    private boolean isPrimeUserFromCookies(ResolverContext context) {
        VisitInformation visit = context.getVisitInformation();
        if (siteVariations.isPrimeUserFromCookieEnabled(visit)) {
            RequestInfo request = context.getRequestInfo();
            return Optional.ofNullable(request.getCookieValue(SiteCookies.SSO_REMEMBER_ME))
                    .map(this::extractPrimeInfoFromCookie)
                    .orElse(false);
        }
        return false;
    }

    private boolean extractPrimeInfoFromCookie(String cookie) {
        String[] cookieValues = ArrayUtils.nullToEmpty(StringUtils.splitByWholeSeparator(cookie, "%3B"));
        return cookieValues.length >= 3 && Boolean.parseBoolean(cookieValues[2]);
    }

}
