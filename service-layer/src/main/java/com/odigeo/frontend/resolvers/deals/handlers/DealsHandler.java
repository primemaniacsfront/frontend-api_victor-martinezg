package com.odigeo.frontend.resolvers.deals.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Deal;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.DealsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NumberRange;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.deals.configuration.DealsConfiguration;
import com.odigeo.frontend.resolvers.deals.mappers.DateRangeMapper;
import com.odigeo.frontend.resolvers.deals.mappers.DealsRoutePriceMapper;
import com.odigeo.frontend.services.SearchPriceService;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.marketing.search.price.v1.requests.SearchPricesRequest;
import com.odigeo.marketing.search.price.v1.responses.RoutePrice;
import com.odigeo.marketing.search.price.v1.responses.SearchPriceResponse;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.Range;

import javax.ws.rs.core.Response;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class DealsHandler {

    @Inject
    private DealsConfiguration dealsConfiguration;
    @Inject
    private DealsHelper dealsHelper;
    @Inject
    private SearchPriceService searchPriceService;
    @Inject
    private DateRangeMapper dateRangeMapper;
    @Inject
    private DealsRoutePriceMapper dealsRoutePriceMapper;

    public List<Deal> getDeals(DealsRequest request, VisitInformation visitInformation) {
        Site site = visitInformation.getSite();

        City departure = dealsHelper.getCity(request.getDeparture());
        validateIata(departure);
        Map<String, City> destinations = dealsHelper.getPopularDestinationCities(departure, site);

        SearchPricesRequest pricesRequest = buildSearchPriceRequest(request, departure, destinations.values(), site);
        SearchPriceResponse pricesResponse = searchPriceService.searchRoutePrices(pricesRequest);

        Locale locale = LocaleUtils.toLocale(visitInformation.getLocale());
        return Optional.ofNullable(pricesResponse)
            .map(SearchPriceResponse::getRoutePriceList)
            .map(routePrices -> processResponse(routePrices, request.getDurationDays(), destinations, locale))
            .orElse(Collections.emptyList());
    }

    private SearchPricesRequest buildSearchPriceRequest(DealsRequest request, City departure, Collection<City> destinationCities, Site site) {
        SearchPricesRequest.Builder builder = new SearchPricesRequest.Builder();

        builder.priceType(dealsConfiguration.getPriceType());
        builder.tripType(dealsConfiguration.getTripType());
        builder.maxSearchAgeDays(dealsConfiguration.getMaxSearchAgeDays());
        builder.website(site.name());
        builder.size(dealsConfiguration.getSize());

        builder.routesFilter(dealsHelper.getRoutesIataPairs(departure, destinationCities));

        builder.departureDateRange(dateRangeMapper.dateRangeContractToModel(request.getDepartureDateRange()));
        builder.returnDateRange(dateRangeMapper.dateRangeContractToModel(request.getArrivalDateRange()));

        return builder.build();
    }

    private List<Deal> processResponse(List<RoutePrice> routePrices, NumberRange daysRange, Map<String, City> destinations, Locale locale) {
        return routePrices
            .stream()
            .filter(routePrice -> filterByDurationDays(routePrice, daysRange))
            .sorted(Comparator.comparingDouble(routePrice -> routePrice.getApparentPricesByPax().get(dealsConfiguration.getPriceType()).getAmount().doubleValue()))
            .map(routePrice -> dealsRoutePriceMapper.map(routePrice, destinations, locale))
            .collect(Collectors.toList());
    }

    private boolean filterByDurationDays(RoutePrice routePrice, NumberRange daysRange) {
        return Optional.ofNullable(daysRange)
            .map(range -> {
                int durationDays = (int) routePrice.getOutboundDate().until(routePrice.getInboundDate(), ChronoUnit.DAYS);
                return Range.between(range.getStart(), range.getEnd()).contains(durationDays);
            })
            .orElse(true);
    }

    private void validateIata(City city) {
        if (!dealsHelper.hasValidIata(city)) {
            throw new ServiceException(
                "Origin with null IATA code",
                Response.Status.BAD_REQUEST,
                ErrorCodes.VALIDATION,
                ServiceName.FRONTEND_API
            );
        }
    }

}
