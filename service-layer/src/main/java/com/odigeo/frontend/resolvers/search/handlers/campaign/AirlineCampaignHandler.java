package com.odigeo.frontend.resolvers.search.handlers.campaign;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AirlineCampaignConfig;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.search.configuration.AirlineCampaignConfiguration;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Singleton
public class AirlineCampaignHandler {

    private final List<String> airlines;

    @Inject
    public AirlineCampaignHandler(AirlineCampaignConfiguration airlineCampaignConfiguration) {
        this.airlines = airlineCampaignConfiguration.getAirlines();
    }

    public AirlineCampaignConfig populateCampaignConfig(Set<String> segmentsCarrierIds) {
        AirlineCampaignConfig airlineCampaignConfig = new AirlineCampaignConfig();
        airlineCampaignConfig.setHasAirlineCampaign(hasAirlineCampaign(segmentsCarrierIds));
        return airlineCampaignConfig;
    }

    private Boolean hasAirlineCampaign(Set<String> segmentsCarrierIds) {
        return Optional.ofNullable(airlines).orElse(Collections.emptyList())
                .stream().anyMatch(airline -> segmentsCarrierIds.contains(airline));
    }

}
