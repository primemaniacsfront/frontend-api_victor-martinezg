package com.odigeo.frontend.resolvers.user;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetMembershipProductRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateMembershipProductRequest;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.prime.handlers.MembershipSubscriptionProductHandler;
import com.odigeo.frontend.resolvers.user.mappers.ModifyMembershipSubscriptionProductMapper;
import graphql.VisibleForTesting;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeRuntimeWiring;

import java.util.function.UnaryOperator;


public class MembershipSubscriptionProductResolver implements Resolver {
    private static final String QUERY = "Query";
    private static final String MUTATION = "Mutation";
    @VisibleForTesting
    static final String MEMBERSHIP_PRODUCT = "membershipProduct";
    @VisibleForTesting
    static final String UPDATE_MEMBERSHIP_PRODUCT = "updateMembershipProduct";
    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private MembershipSubscriptionProductHandler membershipSubscriptionProductHandler;
    @Inject
    private ModifyMembershipSubscriptionProductMapper modifyMembershipSubscriptionProductMapper;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type(QUERY, runtimeWiringBuilder(MEMBERSHIP_PRODUCT, this::membershipSubscriptionProductDataFetcher))
                .type(MUTATION, runtimeWiringBuilder(UPDATE_MEMBERSHIP_PRODUCT, this::updateMembershipProductResolver));
    }

    private <T> UnaryOperator<TypeRuntimeWiring.Builder> runtimeWiringBuilder(String key, DataFetcher<T> dataFetcher) {
        return typeWiring -> typeWiring
                .dataFetcher(key, dataFetcher);
    }

    MembershipSubscriptionResponse membershipSubscriptionProductDataFetcher(DataFetchingEnvironment env) {
        GetMembershipProductRequest getMembershipProductRequest = jsonUtils.fromDataFetching(env, GetMembershipProductRequest.class);
        return membershipSubscriptionProductHandler.getSubscriptionFromShoppingCart(getMembershipProductRequest.getShoppingInfo(), env.getContext());
    }

    MembershipSubscriptionResponse updateMembershipProductResolver(DataFetchingEnvironment env) {
        UpdateMembershipProductRequest updateMembershipProductRequest = jsonUtils.fromDataFetching(env, UpdateMembershipProductRequest.class);
        return membershipSubscriptionProductHandler.updateMembershipSubscription(modifyMembershipSubscriptionProductMapper.fromUpdateMembershipRequest(updateMembershipProductRequest), env.getContext());
    }
}
