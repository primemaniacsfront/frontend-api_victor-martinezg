package com.odigeo.frontend.resolvers.itinerary.configuration;

import com.edreams.configuration.ConfiguredInJsonFile;

import javax.inject.Singleton;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Singleton
@ConfiguredInJsonFile
public class FreeRebookingConfiguration {

    private List<String> sites;
    private List<String> carriers;
    private String maxDateFormat;
    private String maxDate;

    public List<String> getSites() {
        return sites;
    }

    public void setSites(List<String> sites) {
        this.sites = sites;
    }

    public List<String> getCarriers() {
        return carriers;
    }

    public void setCarriers(List<String> carriers) {
        this.carriers = carriers;
    }

    public String getMaxDateFormat() {
        return maxDateFormat;
    }

    public void setMaxDateFormat(String maxDateFormat) {
        this.maxDateFormat = maxDateFormat;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public LocalDate getMaxDateFormatted() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(maxDateFormat);
        return LocalDate.parse(maxDate, formatter);
    }
}
