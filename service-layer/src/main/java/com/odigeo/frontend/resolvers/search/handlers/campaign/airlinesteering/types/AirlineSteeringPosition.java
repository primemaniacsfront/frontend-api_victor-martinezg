package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering.types;

public enum AirlineSteeringPosition {

    HIGH(4, 5),
    MEDIUM(3, 3),
    LOW(2, 1),
    NONE(1, 0);

    private final int partition;

    private final int boostingPosition;

    AirlineSteeringPosition(int partition, int boostingPosition) {
        this.boostingPosition = boostingPosition;
        this.partition = partition;
    }

    public int getPartition() {
        return partition;
    }

    public int getBoostingPosition() {
        return boostingPosition;
    }
}
