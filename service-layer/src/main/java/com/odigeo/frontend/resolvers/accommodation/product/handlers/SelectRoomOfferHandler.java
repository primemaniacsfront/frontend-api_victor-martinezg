package com.odigeo.frontend.resolvers.accommodation.product.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectRoomOfferResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import graphql.GraphQLContext;

public interface SelectRoomOfferHandler {
    SelectRoomOfferResponse selectRoomOffer(String searchId, String accommodationDealKey, String roomDealKey, ResolverContext context, GraphQLContext graphQLContext);
}
