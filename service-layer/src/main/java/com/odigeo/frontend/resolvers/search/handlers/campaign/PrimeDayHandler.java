package com.odigeo.frontend.resolvers.search.handlers.campaign;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayConfig;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayFareType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.search.configuration.PrimeDayMarketsConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class PrimeDayHandler {

    private final Map<Site, Map<String, PrimeDayFareType>> marketsCarriers = new HashMap<>();

    @Inject
    public PrimeDayHandler(PrimeDayMarketsConfiguration primeDayMarketsConfiguration) {
        this.marketsCarriers.putAll(primeDayMarketsConfiguration.getMarketsCarriers());
    }

    public PrimeDayConfig populatePrimeDayConfig(Set<String> sectionsCarriersIds, VisitInformation visit) {

        PrimeDayConfig primeDayConfig = new PrimeDayConfig();
        primeDayConfig.setIsPrimeDayFare(isPrimeDayFare(sectionsCarriersIds, visit));

        if (primeDayConfig.getIsPrimeDayFare()) {
            primeDayConfig.setPrimeDayFareType(calculatePrimeDayFareType(sectionsCarriersIds, visit));
        }
        return primeDayConfig;
    }

    private Boolean isPrimeDayFare(Set<String> sectionsCarriersIds, VisitInformation visit) {
        return Optional.ofNullable(marketsCarriers.get(visit.getSite()))
            .map(carriers -> carriers.keySet().containsAll(sectionsCarriersIds))
            .orElse(false);
    }

    private PrimeDayFareType calculatePrimeDayFareType(Set<String> sectionsCarriersIds, VisitInformation visit) {
        Optional<Map<String, PrimeDayFareType>> siteFare = Optional.ofNullable(marketsCarriers.get(visit.getSite()));
        return getFareTypeForItinerary(siteFare.get(), sectionsCarriersIds).stream().sorted().findFirst().orElse(null);
    }

    private Set<PrimeDayFareType> getFareTypeForItinerary(Map<String, PrimeDayFareType> carriersPrimeFare, Set<String> sectionsCarrierId) {
        return sectionsCarrierId.stream().map(carrier -> carriersPrimeFare.get(carrier)).collect(Collectors.toSet());
    }

}
