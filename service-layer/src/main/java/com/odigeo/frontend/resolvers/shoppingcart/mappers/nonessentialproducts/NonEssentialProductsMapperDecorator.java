package com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductProviderType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrl;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrlType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsResponse;
import com.odigeo.dapi.client.AdditionalProductShoppingItem;
import com.odigeo.dapi.client.Insurance;
import com.odigeo.dapi.client.InsuranceShoppingItem;
import com.odigeo.dapi.client.InsuranceUrl;
import com.odigeo.dapi.client.Itinerary;
import com.odigeo.dapi.client.ItineraryShoppingItem;
import com.odigeo.dapi.client.OtherProductsShoppingItems;
import com.odigeo.dapi.client.PostsellServiceOptionShoppingItem;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsGroupsConfiguration;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mapstruct.Context;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class NonEssentialProductsMapperDecorator implements NonEssentialProductsMapper {

    private NonEssentialProductUrl mapUrl(InsuranceUrl url) {
        NonEssentialProductUrlType type;
        switch (url.getType()) {
        case EXTENDED:
            type = NonEssentialProductUrlType.EXTENDED;
            break;
        case BASIC:
        default:
            type = NonEssentialProductUrlType.SIMPLE;
            break;
        }
        return new NonEssentialProductUrl(url.getUrl(), type);
    }

    private List<NonEssentialProductUrl> mapUrls(List<InsuranceUrl> urls) {
        return urls.stream().map(this::mapUrl).collect(Collectors.toList());
    }

    private NonEssentialProduct mapInsurance(InsuranceShoppingItem item, NonEssentialProductContext context) {
        Insurance insurance = item.getInsurance();
        String policy = insurance.getPolicy();
        NonEssentialProductProviderType provider = NonEssentialProductProviderType.valueOf(insurance.getProvider());
        return new NonEssentialProductBuilder()
                .withPolicy(policy)
                .withTotalPrice(item.getTotalPrice())
                .withProvider(provider)
                .withUrls(mapUrls(insurance.getConditionsUrls()))
                .withContext(context)
                .build();
    }

    private NonEssentialProduct mapServiceOption(PostsellServiceOptionShoppingItem item, NonEssentialProductContext context) {
        String policy = item.getName();
        return new NonEssentialProductBuilder()
                .withPolicy(policy)
                .withTotalPrice(item.getTotalPrice())
                .withContext(context)
                .build();
    }

    private NonEssentialProduct mapAddition(AdditionalProductShoppingItem item, NonEssentialProductContext context) {
        String policy = item.getAdditionalProduct().getReference();
        return new NonEssentialProductBuilder()
                .withPolicy(policy)
                .withTotalPrice(item.getTotalPrice())
                .withContext(context)
                .build();
    }

    private int getSegments(ShoppingCart shoppingCart) {
        return Optional.ofNullable(shoppingCart)
                .map(ShoppingCart::getItineraryShoppingItem)
                .map(ItineraryShoppingItem::getItinerary)
                .map(Itinerary::getSegments)
                .map(Collection::size)
                .orElse(0);
    }

    private int getPax(ShoppingCart shoppingCart) {
        return Optional.ofNullable(shoppingCart)
                .map(ShoppingCart::getRequiredTravellerInformation)
                .map(Collection::size)
                .orElse(0);
    }

    @Override
    public NonEssentialProductsResponse otherProductsToResponse(ShoppingCart shoppingCart, @Context ShoppingCartContext context) {
        NonEssentialProductsResponse response = null;
        int segments = getSegments(shoppingCart);
        int pax = getPax(shoppingCart);

        if (hasOtherProductShoppingItemContainer(shoppingCart)
                && segments > 0
                && pax > 0) {

            OtherProductsShoppingItems otherProductsShoppingItems = shoppingCart.getOtherProductsShoppingItemContainer();

            NonEssentialProductContext nonEssentialProductContext = new NonEssentialProductContextBuilder()
                    .withShoppingCartContext(context)
                    .withSegments(segments)
                    .withPassengers(pax)
                    .withNonEssentialProductsConfiguration(getNonEssentialProductsConfiguration())
                    .build();

            Stream<NonEssentialProduct> insurances = otherProductsShoppingItems
                    .getInsuranceShoppingItems()
                    .stream()
                    .map(item -> mapInsurance(item, nonEssentialProductContext));
            Stream<NonEssentialProduct> serviceOptions = otherProductsShoppingItems
                    .getPostsellServiceOptionsItems()
                    .stream()
                    .map(item -> mapServiceOption(item, nonEssentialProductContext));
            Stream<NonEssentialProduct> additions = otherProductsShoppingItems
                    .getAdditionalProductShoppingItems()
                    .stream()
                    .map(item -> mapAddition(item, nonEssentialProductContext));

            List<NonEssentialProduct> products = Stream.of(insurances, serviceOptions, additions)
                    .flatMap(Function.identity())
                    .collect(Collectors.toList());

            NonEssentialProductsGroupsConfiguration essentialProductsGroupsConfiguration = getNonEssentialProductsGroupsConfiguration();

            response = new NonEssentialProductsResponse();
            response.setGuarantees(
                    products.stream()
                            .filter(product -> essentialProductsGroupsConfiguration.isGuarantee(product.getOfferId()))
                            .collect(Collectors.toList())
            );

            response.setRebooking(
                    products.stream()
                            .filter(product -> essentialProductsGroupsConfiguration.isRebooking(product.getOfferId()))
                            .collect(Collectors.toList())
            );

            response.setInsurances(Collections.emptyList());
            response.setAdditions(Collections.emptyList());
            response.setFareRules(Collections.emptyList());
            response.setServiceOptions(Collections.emptyList());
        }

        return response;
    }

    private boolean hasOtherProductShoppingItemContainer(ShoppingCart shoppingCart) {
        return Optional.ofNullable(shoppingCart).map(ShoppingCart::getOtherProductsShoppingItemContainer).isPresent();
    }

    private NonEssentialProductsGroupsConfiguration getNonEssentialProductsGroupsConfiguration() {
        return ConfigurationEngine.getInstance(NonEssentialProductsGroupsConfiguration.class);
    }

    private NonEssentialProductsConfiguration getNonEssentialProductsConfiguration() {
        return ConfigurationEngine.getInstance(NonEssentialProductsConfiguration.class);
    }
}
