package com.odigeo.frontend.resolvers.search.configuration;

import com.edreams.configuration.ConfiguredInJsonFile;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Singleton
@ConfiguredInJsonFile
public class AirlineSteeringConfiguration {

    private List<String> airlines = new ArrayList<>();

    private BigDecimal percentage;

    public void setAirlines(List<String> airlines) {
        this.airlines = airlines;
    }

    public List<String> getAirlines() {
        return airlines;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
}
