package com.odigeo.frontend.resolvers.accommodation.summary;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSummaryRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.accommodation.commons.exceptions.CityNotFoundException;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationReviewHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationDetailResponseHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationDetailRetrieveHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationSearchCriteriaRetrieveHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationSummaryShoppingCartHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.RoomDealSummaryHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetailResponse;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.provider.AccommodationProviderSearchCriteria;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.Optional;

@Singleton
public class AccommodationSummaryResolver implements Resolver {

    private static final String SEARCH_QUERY = "accommodationSummary";
    static final String REVIEW_PROVIDER_ID = "TA";

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private AccommodationDetailRetrieveHandler accommodationDetailRetrieveHandler;
    @Inject
    private AccommodationDetailResponseHandler accommodationDetailResponseHandler;
    @Inject
    private AccommodationSearchCriteriaRetrieveHandler accommodationSearchCriteriaRetrieveHandler;
    @Inject
    private RoomDealSummaryHandler roomDealSummaryHandler;
    @Inject
    private AccommodationReviewHandler accommodationReviewHandler;
    @Inject
    private GeoLocationHandler geoLocationHandler;
    @Inject
    private Logger logger;
    @Inject
    private AccommodationSummaryShoppingCartHandler accommodationSummaryShoppingCartHandler;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher(SEARCH_QUERY, this::accommodationSummaryFetcher));
    }

    AccommodationSummaryResponseDTO accommodationSummaryFetcher(DataFetchingEnvironment env) {
        AccommodationSummaryRequest request = jsonUtils.fromDataFetching(env, AccommodationSummaryRequest.class);
        AccommodationSummaryResponseDTO response;
        if (request.getBookingId() > 0) {
            response = accommodationSummaryShoppingCartHandler.accommodationSummaryFromShopping(request, env);
        } else {
            response = accommodationSummaryFromSearch(request, env);
        }
        return response;
    }

    private AccommodationSummaryResponseDTO accommodationSummaryFromSearch(AccommodationSummaryRequest request, DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        VisitInformation visit = context.getVisitInformation();
        //TODO: Decide what to do with debug info from request/response

        AccommodationDetailResponse response = accommodationDetailRetrieveHandler.getAccommodationDetails(request, visit);
        //TODO: Decide what to do with error handling from the response. It is treated by erroHandler in frontend-html5. The response DTO is already fine
        AccommodationSummaryResponseDTO responseDTO = accommodationDetailResponseHandler.getAccommodationDetails(response);
        responseDTO.setRoomDeals(roomDealSummaryHandler.getRoomsDealsByRoomKey(request, responseDTO.getAccommodationDetail(), visit));
        responseDTO.setAccommodationReview(accommodationReviewHandler.getSingleReviewByProvider(request.getDedupId(), REVIEW_PROVIDER_ID));
        mapDistanceFromCityCenter(request, responseDTO);
        return responseDTO;
    }

    private void mapDistanceFromCityCenter(AccommodationSummaryRequest requestDTO, AccommodationSummaryResponseDTO responseDTO) {
        Optional.ofNullable(responseDTO)
            .map(AccommodationSummaryResponseDTO::getAccommodationDetail)
            .map(AccommodationDetailDTO::getAccommodationDealInformation)
            .map(AccommodationDealInformationDTO::getCoordinates)
            .ifPresent(coordinates -> {
                try {
                    City city = geoLocationHandler.getCityGeonode(retrieveDestinationGeoNodeId(requestDTO), null);
                    responseDTO.getAccommodationDetail().setDistanceFromCityCenter(geoLocationHandler.computeDistanceFromGeonode(city, coordinates));
                } catch (CityNotFoundException e) {
                    logger.warning(this.getClass(), "Error calculating distance from city center", e);
                }
            });
    }

    private Integer retrieveDestinationGeoNodeId(AccommodationSummaryRequest requestDTO) {
        AccommodationSearchCriteria searchCriteria = accommodationSearchCriteriaRetrieveHandler.getSearchCriteria(requestDTO.getSearchId(), Integer.parseInt(requestDTO.getAccommodationDealKey()));
        return ((AccommodationProviderSearchCriteria) searchCriteria.getProviderSearchCriteria()).getDestinationGeoNodeId();
    }

}
