package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryapi.v1.response.SeatMapPreferencesDescriptorItem;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class SeatMapMapper {

    @Inject
    private CabinsMapper cabinsMapper;

    public List<SeatMap> map(List<SeatMapPreferencesDescriptorItem> seatMapList) {
        return Optional.ofNullable(seatMapList)
            .orElse(Collections.emptyList())
            .stream()
            .map(this::mapSeatMap)
            .collect(Collectors.toList());
    }

    private SeatMap mapSeatMap(SeatMapPreferencesDescriptorItem seatMapPreferencesDescriptorItem) {
        SeatMap seatMap = new SeatMap();

        seatMap.setSection(seatMapPreferencesDescriptorItem.getSection());
        seatMap.setCabins(cabinsMapper.map(seatMapPreferencesDescriptorItem.getCabin()));

        return seatMap;
    }

}
