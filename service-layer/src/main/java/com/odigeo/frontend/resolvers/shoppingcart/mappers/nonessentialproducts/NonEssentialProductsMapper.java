package com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsResponse;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mapstruct.Context;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

@Mapper
@DecoratedWith(NonEssentialProductsMapperDecorator.class)
public interface NonEssentialProductsMapper {
    NonEssentialProductsResponse otherProductsToResponse(ShoppingCart shoppingCart, @Context ShoppingCartContext context);
}
