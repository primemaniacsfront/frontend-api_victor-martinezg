package com.odigeo.frontend.resolvers.accommodation.search.mappers;

import com.edreams.util.xml.XmlUtils;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationCategoryMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationImagesMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationFacilityGroupsEnum;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.model.AccommodationStaticContentDTO;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.hcsapi.v9.beans.HotelSummaryResponse;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.jboss.resteasy.client.exception.ResteasyClientException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class HotelDetailResponseMapper {

    @Inject
    private AccommodationProviderKeyMapper accommodationProviderKeyMapper;
    @Inject
    private AccommodationImagesMapper accommodationImagesMapper;
    @Inject
    private AccommodationCategoryMapper accommodationCategoryMapper;
    @Inject
    private ContentKeyMapper contentKeyMapper;

    public void remapSummaryToAccommodation(HotelSummaryResponse accommodationSummary, List<SearchAccommodationDTO> accommodations) {
        cleanAccommodationWithoutHotelSummary(accommodations, accommodationSummary);
        remapSummariesAndFacilities(accommodations, accommodationSummary);
    }

    public List<AccommodationStaticContentDTO> mapSummaries(HotelSummaryResponse accommodationSummary) {
        List<AccommodationStaticContentDTO> results = new ArrayList<>();
        for (Map.Entry<HotelSupplierKey, HotelSummary> entry : accommodationSummary.getHotelSummary().entrySet()) {
            AccommodationStaticContentDTO accommodationStaticContentDTO = new AccommodationStaticContentDTO();
            HotelSummary hotelSummary = entry.getValue();
            accommodationStaticContentDTO.setAccommodationDeal(mapSummary(hotelSummary));
            accommodationStaticContentDTO.setContentKey(contentKeyMapper.buildContentKey(hotelSummary.getHotelKey().getSupplierCode(), hotelSummary.getHotelKey().getHotelSupplierId()));
            results.add(accommodationStaticContentDTO);
        }
        return results;
    }

    private void remapSummariesAndFacilities(List<SearchAccommodationDTO> accommodationDealResults, HotelSummaryResponse hotelSummaryResponse) throws ResteasyClientException {
        Map<AccommodationProviderKeyDTO, SearchAccommodationDTO> accommodationMap = accommodationDealResults.stream().collect(
                Collectors.toMap(p -> contentKeyMapper.extractProviderKeyFromContentKey(p.getContentKey()), p -> p, (actualValue, newValue) -> actualValue));

        for (Map.Entry<HotelSupplierKey, HotelSummary> entry : hotelSummaryResponse.getHotelSummary().entrySet()) {
            SearchAccommodationDTO accommodationDeal = accommodationMap.get(accommodationProviderKeyMapper.map(entry.getKey()));
            accommodationDeal.setAccommodationDeal(remapSummary(accommodationDeal.getAccommodationDeal(), entry.getValue()));
            accommodationDeal.setAccommodationFacilities(mapCustomFacilityGroups(entry.getValue().getHotelFacilities()));
        }
    }

    private void cleanAccommodationWithoutHotelSummary(List<SearchAccommodationDTO> accommodationDealResults, HotelSummaryResponse hotelSummaryResponse) {
        Set<HotelSupplierKey> accommodationContentKeys = Optional.ofNullable(hotelSummaryResponse)
                .map(HotelSummaryResponse::getHotelSummary)
                .map(Map::keySet).orElse(new HashSet<>());
        accommodationDealResults.removeIf(accommodationDealDTO -> accommodationWithoutHotelSummary(accommodationDealDTO, accommodationContentKeys));
    }

    private boolean accommodationWithoutHotelSummary(SearchAccommodationDTO accommodationDealDTO, Set<HotelSupplierKey> accommodationContentKeys) {
        AccommodationProviderKeyDTO providerKey = contentKeyMapper.extractProviderKeyFromContentKey(accommodationDealDTO.getContentKey());
        return !accommodationContentKeys.contains(new HotelSupplierKey(providerKey.getAccommodationProviderId(), providerKey.getProviderId()));
    }

    private AccommodationDealInformationDTO mapSummary(HotelSummary hotelSummary) {
        return remapSummary(null, hotelSummary);
    }

    private AccommodationDealInformationDTO remapSummary(AccommodationDealInformationDTO accommodationDealDTO, HotelSummary hotelSummary) {
        AccommodationDealInformationDTO result = accommodationDealDTO;
        if (hotelSummary == null) {
            return result;
        }
        if (result == null) {
            result = new AccommodationDealInformationDTO();
        }
        result.setName(mapName(hotelSummary));
        result.setAddress(hotelSummary.getAddress());
        result.setCityName(hotelSummary.getCity());
        result.setNeighbourhood(hotelSummary.getNeighbourhood());
        result.setDescription(mapDescription(hotelSummary));
        result.setMainAccommodationImage(mapMainImage(hotelSummary));
        result.setCoordinates(mapCoordinates(hotelSummary));
        result.setLocation(hotelSummary.getLocation());
        result.setChain(hotelSummary.getChain());
        result.setType(mapType(hotelSummary));
        result.setCategory(mapCategory(hotelSummary));

        return result;
    }

    private String mapName(HotelSummary hotelSummary) {
        return removeInvalidXmlChars(hotelSummary.getName());
    }

    private String mapShortDescription(HotelSummary hotelSummary) {
        return StringUtils.isEmpty(hotelSummary.getHotelShortDescription()) ? StringUtils.EMPTY : removeInvalidXmlChars(hotelSummary.getHotelShortDescription());
    }

    private String mapDescription(HotelSummary hotelSummary) {
        return StringUtils.isEmpty(hotelSummary.getHotelLongDescription()) ? mapShortDescription(hotelSummary) : removeInvalidXmlChars(hotelSummary.getHotelLongDescription());
    }

    private AccommodationImageDTO mapMainImage(HotelSummary hotelSummary) {
        return accommodationImagesMapper.mapHcsImage(hotelSummary.getMainHotelImage());
    }

    private GeoCoordinatesDTO mapCoordinates(HotelSummary hotelSummary) {
        GeoCoordinatesDTO coordinates = new GeoCoordinatesDTO(BigDecimal.ZERO, BigDecimal.ZERO);
        if (hotelSummary.getLatitude() != null && hotelSummary.getLongitude() != null) {
            coordinates = new GeoCoordinatesDTO(new BigDecimal(String.valueOf(hotelSummary.getLatitude())), new BigDecimal(String.valueOf(hotelSummary.getLongitude())));
        }
        return coordinates;
    }

    private AccommodationType mapType(HotelSummary hotelSummary) {
        return EnumUtils.getEnum(AccommodationType.class, hotelSummary.getType().name());
    }

    private AccommodationCategory mapCategory(HotelSummary hotelSummary) {
        return accommodationCategoryMapper.mapStarToCategory(hotelSummary.getStarRating());
    }

    private List<String> mapCustomFacilityGroups(List<String> facilities) {
        return Arrays.stream(AccommodationFacilityGroupsEnum.values()).filter(group -> hasValidCodeInFacilityGroup(facilities, group)).map(AccommodationFacilityGroupsEnum::name).collect(Collectors.toList());
    }

    private boolean hasValidCodeInFacilityGroup(List<String> facilities, AccommodationFacilityGroupsEnum group) {
        return group.getFacilities().stream().anyMatch(code -> facilities.contains(String.valueOf(code)));
    }

    private String removeInvalidXmlChars(String value) {
        return XmlUtils.replaceInvalidXmlChars(XmlUtils.XML_1_0_PATTERN, value, StringUtils.EMPTY);
    }


}
