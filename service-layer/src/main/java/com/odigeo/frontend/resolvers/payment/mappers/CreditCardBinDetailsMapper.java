package com.odigeo.frontend.resolvers.payment.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCardBinDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface CreditCardBinDetailsMapper {

    @Mapping(target = "creditCardTypeCode", source = "creditCardType.code")
    CreditCardBinDetails creditCardBinDetailsContractToModel(com.odigeo.collectionmethod.v3.CreditCardBinDetails creditCardBinDetails);

}
