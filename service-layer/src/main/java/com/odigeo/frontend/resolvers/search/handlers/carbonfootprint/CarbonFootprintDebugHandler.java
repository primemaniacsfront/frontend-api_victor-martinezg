package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CarbonFootprint;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Singleton
public class CarbonFootprintDebugHandler {

    private static final String NON_INFO = "Non info";
    private static final String ITINERARY_INFO = "isEco: %b, totalCo2Kilos: %s, totalCo2eKilos: %s, percentage than average: %s";
    private static final String TEXT = "%s. Kilos average search: %s";

    public Map<String, Object> getDebugInformation(List<SearchItineraryDTO> itineraries, final BigDecimal averageKilos) {
        return itineraries.stream()
                .map(itinerary -> new AbstractMap.SimpleEntry<>(itinerary.getId(), getItineraryFootprintInfo(itinerary.getCarbonFootprint(), averageKilos)))
                .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
    }

    private String getItineraryFootprintInfo(CarbonFootprint carbonFootprint, BigDecimal averageKilos) {
        String debugInfo = carbonFootprint == null ? NON_INFO
                : String.format(ITINERARY_INFO, carbonFootprint.getIsEco(), carbonFootprint.getTotalCo2Kilos(), carbonFootprint.getTotalCo2eKilos(), carbonFootprint.getEcoPercentageThanAverage());
        return String.format(TEXT, debugInfo, averageKilos);

    }
}
