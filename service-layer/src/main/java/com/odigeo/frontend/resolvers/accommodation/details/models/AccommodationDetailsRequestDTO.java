package com.odigeo.frontend.resolvers.accommodation.details.models;

import java.io.Serializable;

public class AccommodationDetailsRequestDTO implements Serializable {

    private long searchId;
    private String accommodationDealKey;

    public long getSearchId() {
        return searchId;
    }

    public void setSearchId(long searchId) {
        this.searchId = searchId;
    }

    public String getAccommodationDealKey() {
        return accommodationDealKey;
    }

    public void setAccommodationDealKey(String accommodationDealKey) {
        this.accommodationDealKey = accommodationDealKey;
    }
}
