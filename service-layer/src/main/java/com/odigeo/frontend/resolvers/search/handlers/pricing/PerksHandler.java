package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Perks;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.membership.MembershipPerks;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Singleton
public class PerksHandler {

    public Perks populatePerks(SearchEngineContext searchEngineContext, FareItinerary fareItinerary, int numPassengers) {
        return Optional.ofNullable(fareItinerary.getMembershipPerks())
                .map(membershipPerks -> createPerks(searchEngineContext, membershipPerks, numPassengers))
                .orElse(null);
    }

    private Perks createPerks(SearchEngineContext searchEngineContext, MembershipPerks membershipPerks, int numPassengers) {
        Perks perksDTO = new Perks();
        BigDecimal passengers = BigDecimal.valueOf(numPassengers);
        String currency = searchEngineContext.getCurrency();
        perksDTO.setPrimeDiscount(createMoney(membershipPerks.getFee(), currency));
        perksDTO.setExtraPrimeCoupon(createMoney(membershipPerks.getMembershipCoupon(), currency));
        perksDTO.setPrimeDiscountPerPassenger(createMoneyPerPassenger(membershipPerks.getFee(), currency, passengers));
        perksDTO.setExtraPrimeCouponPerPassenger(createMoneyPerPassenger(membershipPerks.getMembershipCoupon(), currency, passengers));
        return perksDTO;
    }

    private Money createMoneyPerPassenger(BigDecimal amount, String currency, BigDecimal passengers) {
        return Optional.ofNullable(amount)
                .map(nonNullAmount -> nonNullAmount.divide(passengers, 2, RoundingMode.HALF_UP))
                .map(perPassengerAmount -> createMoney(perPassengerAmount, currency))
                .orElse(null);
    }

    private Money createMoney(BigDecimal amount, String currency) {
        return Optional.ofNullable(amount)
                .map(nonNullAmount -> {
                    Money money = new Money();
                    money.setCurrency(currency);
                    money.setAmount(nonNullAmount);
                    return money;
                })
                .orElse(null);
    }
}
