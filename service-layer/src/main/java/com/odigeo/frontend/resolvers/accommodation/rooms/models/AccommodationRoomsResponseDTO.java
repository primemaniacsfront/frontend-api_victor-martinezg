package com.odigeo.frontend.resolvers.accommodation.rooms.models;


import java.util.List;

public class AccommodationRoomsResponseDTO {
    private long searchId;
    private String currency;
    private List<RoomDealDTO> rooms;

    public long getSearchId() {
        return searchId;
    }

    public void setSearchId(long searchId) {
        this.searchId = searchId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<RoomDealDTO> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomDealDTO> rooms) {
        this.rooms = rooms;
    }
}
