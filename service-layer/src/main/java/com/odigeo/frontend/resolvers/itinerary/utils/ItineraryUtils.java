package com.odigeo.frontend.resolvers.itinerary.utils;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Leg;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import com.google.inject.Singleton;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class ItineraryUtils {

    public Set<String> getSectionsCarrierIds(Itinerary itinerary) {
        return Optional.ofNullable(itinerary)
            .map(Itinerary::getLegs)
            .orElseGet(Collections::emptyList)
            .stream()
            .map(Leg::getSegments)
            .flatMap(List::stream)
            .map(Segment::getSections)
            .flatMap(List::stream)
            .map(Section::getCarrier)
            .map(Carrier::getId)
            .collect(Collectors.toSet());
    }

    public Set<String> getSegmentsCarrierIds(Itinerary itinerary) {
        return Optional.ofNullable(itinerary)
            .map(Itinerary::getLegs)
            .orElseGet(Collections::emptyList)
            .stream()
            .map(Leg::getSegments)
            .flatMap(List::stream)
            .map(Segment::getCarrier)
            .map(Carrier::getId)
            .collect(Collectors.toSet());
    }
}
