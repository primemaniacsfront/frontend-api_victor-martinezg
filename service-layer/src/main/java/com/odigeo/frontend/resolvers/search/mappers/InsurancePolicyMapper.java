package com.odigeo.frontend.resolvers.search.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsurancePolicy;
import com.google.inject.Singleton;
import com.odigeo.searchengine.v2.responses.insurance.Insurance;
import org.apache.commons.lang3.StringUtils;

@Singleton
public class InsurancePolicyMapper {

    private static final String EXTERNAL_INSURANCE = "NEWHUB162";
    private static final String ODIGEO_GUARANTEE = "VINGSELF";

    public InsurancePolicy map(Insurance insurance) {
        String policy = StringUtils.defaultString(insurance.getPolicy());

        switch (policy) {
        case EXTERNAL_INSURANCE : return InsurancePolicy.EXTERNAL_INSURANCE;
        case ODIGEO_GUARANTEE : return InsurancePolicy.ODIGEO_GUARANTEE;
        default : return InsurancePolicy.OTHER;
        }
    }

}
