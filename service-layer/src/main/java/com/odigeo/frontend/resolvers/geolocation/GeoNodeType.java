package com.odigeo.frontend.resolvers.geolocation;

import java.util.Arrays;

public enum GeoNodeType {
    CITY("City"),
    COUNTRY("Country"),
    UNKNOWN("");

    private final String className;

    GeoNodeType(String className) {
        this.className = className;
    }

    public String value() {
        return className;
    }

    public static GeoNodeType getValueFromString(String name) {
        return Arrays.stream(GeoNodeType.values()).filter(geoNodeType -> geoNodeType.value().equals(name)).findFirst().orElse(UNKNOWN);
    }
}
