package com.odigeo.frontend.resolvers;

import graphql.schema.idl.RuntimeWiring;

public interface Resolver {
    void addToBuilder(RuntimeWiring.Builder resolversBuilder);
}
