package com.odigeo.frontend.resolvers.search.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageCondition;
import com.google.inject.Singleton;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.BaggageIncluded;

@Singleton
public class BaggageConditionMapper {

    public  BaggageIncluded map(BaggageCondition baggageCondition) {
        return baggageCondition == BaggageCondition.CHECKIN_INCLUDED
            ? BaggageIncluded.CHECK_IN
            : BaggageIncluded.CABIN;
    }

}
