package com.odigeo.frontend.resolvers.prime.handlers;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.membership.offer.api.request.MembershipOfferRequest;
import com.odigeo.membership.offer.api.request.TestTokenSet;

@Singleton
public class SubscriptionRequestHandler {

    public MembershipOfferRequest buildSubscriptionRequest(VisitInformation visit) {

        MembershipOfferRequest offerRequest = new MembershipOfferRequest();

        offerRequest.setWebsiteCode(visit.getSite().name());
        offerRequest.setAnInterface(visit.getWebInterface().name());
        offerRequest.setMarketingPortal(visit.getMktPortal());
        offerRequest.setTestTokenSet(new TestTokenSet(visit.getDimensionPartitionMap()));
        offerRequest.setExcludeFree(false);

        return offerRequest;
    }

}
