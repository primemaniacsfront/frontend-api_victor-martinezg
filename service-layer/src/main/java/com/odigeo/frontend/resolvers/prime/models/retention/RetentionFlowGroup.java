package com.odigeo.frontend.resolvers.prime.models.retention;

import com.odigeo.frontend.resolvers.prime.models.MembershipType;
import com.odigeo.frontend.resolvers.prime.models.SourceType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.Range;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

public enum RetentionFlowGroup {

    BASIC_LONG_WITH_TRIP(false, true, true),
    BASIC_LONG_NO_TRIP(false, true, false),
    BASIC_SHORT_WITH_TRIP(false, false, true),
    BASIC_SHORT_NO_TRIP(false, false, false),
    FREE_TRIAL_WITH_TRIP(true, null, true),
    FREE_TRIAL_NO_TRIP(true, null, false),
    DEFAULT(null, null, null);

    private final Boolean freeTrial;
    private final Boolean longRenew;
    private final Boolean upcomingTrips;

    RetentionFlowGroup(Boolean freeTrial, Boolean longRenew, Boolean trips) {
        this.freeTrial = freeTrial;
        this.longRenew = longRenew;
        this.upcomingTrips = trips;
    }

    public Boolean isFreeTrial() {
        return freeTrial;
    }

    public Boolean isLongRenew() {
        return longRenew;
    }

    public Boolean isUpcomingTrips() {
        return upcomingTrips;
    }

    public static FreeTrial builder() {
        return new Builder();
    }

    public interface FreeTrial {
        DaysFromRenew freeTrial(MembershipType membershipType, SourceType sourceType, Integer monthsDuration);

        /**
         * skip freeTrial it is Optional
         */
        UpcomingTrips daysFromRenew(LocalDate membershipExpirationDate);
    }

    public interface DaysFromRenew {
        UpcomingTrips daysFromRenew(LocalDate membershipExpirationDate);
    }

    public interface UpcomingTrips {
        Build upcomingTrips(Collection<?> futureFlights);
    }

    public interface Build {
        RetentionFlowGroup build();
    }

    private static class Builder implements FreeTrial, DaysFromRenew, UpcomingTrips, Build {

        private static final Range<Integer> LONG_RENEW_RANGE = Range.between(276, 365);

        private Integer daysFromRenew;
        private Boolean upcomingTrips;
        private Boolean freeTrial;

        @Override
        public DaysFromRenew freeTrial(MembershipType membershipType, SourceType sourceType, Integer monthsDuration) {
            freeTrial = Objects.equals(monthsDuration, 1) && MembershipType.BASIC.equals(membershipType) && SourceType.FUNNEL_BOOKING.equals(sourceType);
            return this;
        }

        @Override
        public UpcomingTrips daysFromRenew(LocalDate membershipExpirationDate) {
            this.daysFromRenew = Optional.ofNullable(membershipExpirationDate)
                    .map(date -> ChronoUnit.DAYS.between(LocalDate.now(), date))
                    .map(Math::toIntExact).orElse(null);
            return this;
        }

        @Override
        public Build upcomingTrips(Collection<?> futureFlights) {
            this.upcomingTrips = CollectionUtils.isNotEmpty(futureFlights);
            return this;
        }

        @Override
        public RetentionFlowGroup build() {

            return Arrays.stream(RetentionFlowGroup.values())
                    .filter(filterByFreeTrial())
                    .filter(filterByDaysFromRenew())
                    .filter(filterByUpcomingTrips())
                    .findFirst()
                    .orElse(DEFAULT);

        }

        private Predicate<RetentionFlowGroup> filterByFreeTrial() {
            return group -> Optional.of(group).map(RetentionFlowGroup::isFreeTrial)
                    .map(freeTrialCandidate -> (freeTrial == null && !freeTrialCandidate) || freeTrialCandidate.equals(freeTrial))
                    .orElse(true);
        }

        private Predicate<RetentionFlowGroup> filterByDaysFromRenew() {
            boolean longRenew = LONG_RENEW_RANGE.contains(daysFromRenew);
            return group -> Optional.of(group).map(RetentionFlowGroup::isLongRenew)
                    .map(longRenewCandidate -> longRenewCandidate.equals(longRenew))
                    .orElse(true);
        }

        private Predicate<RetentionFlowGroup> filterByUpcomingTrips() {
            return group -> Optional.of(group).map(RetentionFlowGroup::isUpcomingTrips)
                    .map(upcomingTripsCandidate -> upcomingTripsCandidate.equals(upcomingTrips))
                    .orElse(true);
        }


    }
}
