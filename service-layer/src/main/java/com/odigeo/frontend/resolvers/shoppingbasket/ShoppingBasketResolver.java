package com.odigeo.frontend.resolvers.shoppingbasket;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddContactDetailsToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddProductToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RemoveProductFromShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.shoppingbasket.factories.ShoppingBasketFactory;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.ShoppingBasketOperations;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

@Singleton
public class ShoppingBasketResolver implements Resolver {

    @Inject
    private ShoppingBasketFactory shoppingBasketFactory;
    @Inject
    private JsonUtils jsonUtils;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
            .type("Query", typeWiring -> typeWiring
                .dataFetcher("getShoppingBasket", this::shoppingBasketFetcher)
                .dataFetcher("getAvailableCollectionMethods", this::availableCollectionMethod))
            .type("Mutation", typeWiring -> typeWiring
                .dataFetcher("createShoppingBasket", this::createShoppingBasketFetcher)
                .dataFetcher("addProductToShoppingBasket", this::addProductsInShoppingBasketFetcher)
                .dataFetcher("removeProductFromShoppingBasket", this::removeProductFromShoppingBasketFetcher)
                .dataFetcher("addContactDetailsToShoppingBasket", this::addContactDetailsToShoppingBasket)
            );
    }

    ShoppingBasketResponse shoppingBasketFetcher(DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        ShoppingBasketRequest request = jsonUtils.fromDataFetching(env, ShoppingBasketRequest.class);

        return getShoppingBasketFactoryInstance(request.getShoppingType()).getShoppingBasket(request.getShoppingBasketId(), context);
    }

    AvailableCollectionMethodsResponse availableCollectionMethod(DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        AvailableCollectionMethodsRequest request = jsonUtils.fromDataFetching(env, AvailableCollectionMethodsRequest.class);

        return getShoppingBasketFactoryInstance(request.getShoppingType()).getAvailableCollectionMethods(request.getShoppingId(), context);
    }

    ShoppingBasketResponse createShoppingBasketFetcher(DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        CreateShoppingBasketRequest request = jsonUtils.fromDataFetching(env, CreateShoppingBasketRequest.class);

        return getShoppingBasketFactoryInstance(request.getShoppingType()).createShoppingBasket(request.getBundleId().getId(), context);
    }

    Boolean removeProductFromShoppingBasketFetcher(DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        RemoveProductFromShoppingBasketRequest request = jsonUtils.fromDataFetching(env, RemoveProductFromShoppingBasketRequest.class);

        return getShoppingBasketFactoryInstance(request.getShoppingType()).removeProduct(request, context);
    }

    Boolean addProductsInShoppingBasketFetcher(DataFetchingEnvironment env) {
        ResolverContext context = env.getContext();
        AddProductToShoppingBasketRequest request = jsonUtils.fromDataFetching(env, AddProductToShoppingBasketRequest.class);

        return getShoppingBasketFactoryInstance(request.getShoppingType()).addProduct(request, context);
    }


    Boolean addContactDetailsToShoppingBasket(DataFetchingEnvironment env) {
        AddContactDetailsToShoppingBasketRequest request = jsonUtils.fromDataFetching(env, AddContactDetailsToShoppingBasketRequest.class);

        return getShoppingBasketFactoryInstance(ShoppingType.BASKET_V3).addContactDetails(request);
    }

    private ShoppingBasketOperations getShoppingBasketFactoryInstance(ShoppingType shoppingType) {
        return shoppingBasketFactory.getInstance(shoppingType);
    }
}
