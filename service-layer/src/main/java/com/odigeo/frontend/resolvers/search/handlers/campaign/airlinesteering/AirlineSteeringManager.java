package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.configuration.AirlineSteeringConfiguration;
import com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering.types.AirlineSteeringPosition;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Singleton
class AirlineSteeringManager {

    @Inject
    private SiteVariations siteVariations;

    @Inject
    private AirlineSteeringConfiguration airlineSteeringConfiguration;

    public AirlineSteeringPosition getAirlineSteeringPosition(VisitInformation visitInformation) {
        Integer partition = siteVariations.getAirlineSteeringPartition(visitInformation);
        return Arrays.stream(AirlineSteeringPosition.values()).filter(p -> Objects.equals(p.getPartition(), partition)).findFirst()
                .orElse(AirlineSteeringPosition.NONE);
    }

    public List<String> getAirlines() {
        return airlineSteeringConfiguration.getAirlines();
    }

    public BigDecimal getControlPercentagePrice() {
        return airlineSteeringConfiguration.getPercentage();
    }


}
