package com.odigeo.frontend.resolvers.accommodation.rooms.models;


import com.odigeo.frontend.resolvers.accommodation.commons.model.ImageDTO;

import java.util.List;

public class RoomDetailsDTO {
    private ImageDTO mainImage;
    private List<ImageDTO> images;
    private String name;
    private String description;

    public ImageDTO getMainImage() {
        return mainImage;
    }

    public void setMainImage(ImageDTO mainImage) {
        this.mainImage = mainImage;
    }

    public List<ImageDTO> getImages() {
        return images;
    }

    public void setImages(List<ImageDTO> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
