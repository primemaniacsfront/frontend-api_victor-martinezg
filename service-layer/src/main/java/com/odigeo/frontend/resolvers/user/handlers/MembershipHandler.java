package com.odigeo.frontend.resolvers.user.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateUserMembershipAction;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateUserMembershipRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipTokenResponse;
import com.google.common.base.Splitter;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.commons.util.ShoppingInfoUtils;
import com.odigeo.frontend.resolvers.prime.operations.MembershipShoppingOperations;
import com.odigeo.frontend.resolvers.prime.operations.MembershipShoppingOperationsFactory;
import com.odigeo.frontend.resolvers.user.mappers.MembershipMapper;
import com.odigeo.frontend.services.membership.MembershipServiceHandler;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.frontend.services.membership.MembershipSearchHandler;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.userprofiles.api.v2.model.User;
import graphql.schema.DataFetchingEnvironment;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Map;

@Singleton
public class MembershipHandler {
    private static final List<UpdateUserMembershipAction> ALLOWED_NOT_LOGGED_MEMBERSHIP_UPDATES = Collections.singletonList(UpdateUserMembershipAction.ACTIVATE_RENEWAL_STATUS);

    @Inject
    private UserDescriptionService userDescriptionService;
    @Inject
    private MembershipMapper membershipMapper;
    @Inject
    private MembershipServiceHandler membershipServiceHandler;
    @Inject
    private MembershipSearchHandler membershipSearchHandler;
    @Inject
    private MembershipShoppingOperationsFactory membershipShoppingOperationsFactory;
    @Inject
    private Logger logger;
    @Inject
    private ShoppingInfoUtils shoppingInfoUtils;
    @Inject
    private JsonUtils jsonUtils;

    public Membership getMembershipFromLoggedUser(DataFetchingEnvironment env) {
        User user = getUser(env);
        VisitInformation visitInformation = env.getGraphQlContext().get(VisitInformation.class);
        return getMembership(user, visitInformation);
    }

    public Membership getMembershipFromShoppingPersonalInfo(ShoppingInfo shoppingInfo, ResolverContext context) {
        MembershipShoppingOperations membershipShoppingOperations = membershipShoppingOperationsFactory.getMembershipShoppingOperations(shoppingInfoUtils.getShoppingType(shoppingInfo));
        String email = membershipShoppingOperations.getBuyerEmail(shoppingInfoUtils.getShoppingId(shoppingInfo), context);
        Membership membership = null;
        if (isAutoApply(shoppingInfo, context)) {
            String website = getWebsiteDefaultCountry(context);
            membership = getMembershipAndSetAutoApply(email, website);
        }
        return membership;
    }

    public Membership getMembership(DataFetchingEnvironment env, ShoppingInfo shoppingInfo) {
        Membership membership = null;
        if (userDescriptionService.isUserLogged(env.getContext())) {
            membership = getMembershipFromLoggedUser(env);
        } else if (Objects.nonNull(shoppingInfo)) {
            membership = getMembershipFromShoppingPersonalInfo(shoppingInfo, env.getContext());
        }
        return membership;
    }

    private Membership getMembershipAndSetAutoApply(String email, String website) {
        return Optional.ofNullable(email)
        .map(mail -> membershipSearchHandler.getCurrentMembership(mail, website))
        .map(membershipMapper::membershipContractToModel)
        .map(this::setAutoApply)
        .orElse(null);
    }

    private String getWebsiteDefaultCountry(ResolverContext context) {
        return Optional.ofNullable(context.getVisitInformation())
                .map(VisitInformation::getWebsiteDefaultCountry)
                .orElse(StringUtils.EMPTY);
    }

    private Membership setAutoApply(Membership membership) {
        membership.setIsAutoApply(Boolean.TRUE);
        return membership;
    }

    public boolean isAutoApply(ShoppingInfo shoppingInfo, ResolverContext context) {
        MembershipShoppingOperations membershipShoppingOperations = membershipShoppingOperationsFactory.getMembershipShoppingOperations(shoppingInfoUtils.getShoppingType(shoppingInfo));
        boolean isDiscountApplied = membershipShoppingOperations.isPrimeUserDiscountApplied(shoppingInfoUtils.getShoppingId(shoppingInfo), context);
        boolean isUserLogged = userDescriptionService.isUserLogged(context);
        return isDiscountApplied && !isUserLogged;
    }

    public Membership getMembership(User user, VisitInformation visit) {
        return Optional.ofNullable(getCurrentMembership(user, visit)).map(membershipMapper::membershipContractToModel).orElse(null);
    }

    public boolean isEligibleForFreeTrial(ShoppingInfo shoppingInfo, ResolverContext context) {
        MembershipShoppingOperations membershipShoppingOperations = membershipShoppingOperationsFactory.getMembershipShoppingOperations(shoppingInfoUtils.getShoppingType(shoppingInfo));
        FreeTrialCandidateRequest freeTrialCandidateRequest = membershipShoppingOperations.buildFreeTrialCandidateRequest(shoppingInfoUtils.getShoppingId(shoppingInfo), context);
        return membershipServiceHandler.isEligibleForFreeTrial(freeTrialCandidateRequest);
    }

    private User getUser(DataFetchingEnvironment env) {
        return Optional.ofNullable((User) env.getGraphQlContext().get("User")).orElse(userDescriptionService.getUserByToken(env.getContext()));
    }

    private MembershipResponse getCurrentMembership(User user, VisitInformation visit) {
        String websiteCode = Optional.ofNullable(visit).map(VisitInformation::getSite).map(Enum::name).orElse(StringUtils.EMPTY);
        return Optional.ofNullable(user)
                .map(User::getId)
                .map(userId -> getCurrentMembership(websiteCode, userId))
                .orElse(null);
    }

    private MembershipResponse getCurrentMembership(String websiteCode, Long userId) {
        return membershipSearchHandler.getCurrentMembership(userId, websiteCode);
    }

    public Membership updateMembershipData(DataFetchingEnvironment env) {
        UpdateUserMembershipRequest updateUserMembershipRequest = jsonUtils.fromDataFetching(env, UpdateUserMembershipRequest.class);
        UpdateUserMembershipAction updateUserMembershipAction = getUpdateUserMembershipAction(updateUserMembershipRequest);
        ShoppingInfo shoppingInfo = Optional.ofNullable(updateUserMembershipRequest).map(UpdateUserMembershipRequest::getShoppingInfo).orElse(null);
        Membership currentMembership = getMembership(env, shoppingInfo);

        if (Objects.nonNull(currentMembership) && isAllowedToUpdateMembership(env.getContext(), updateUserMembershipAction)) {
            updateUserMembership(updateUserMembershipAction, currentMembership.getId());
        }
        return getMembership(env, shoppingInfo);
    }

    private boolean isAllowedToUpdateMembership(ResolverContext context, UpdateUserMembershipAction updateUserMembershipAction) {
        boolean isUserLogged = userDescriptionService.isUserLogged(context);
        return isUserLogged || ALLOWED_NOT_LOGGED_MEMBERSHIP_UPDATES.contains(updateUserMembershipAction);
    }

    public MembershipTokenResponse getTokenInfo(String token) {
        String decryptedToken = membershipServiceHandler.decryptToken(token);
        try {
            Map<String, String> mappedToken = Splitter.on("&").withKeyValueSeparator("=").split(decryptedToken);
            return new MembershipTokenResponse(mappedToken.get("name"), mappedToken.get("email"));
        } catch (IllegalArgumentException ex) {
            logger.warning(this.getClass(), "Error parsing the decrypted token");
            return new MembershipTokenResponse();
        }

    }

    private void updateUserMembership(UpdateUserMembershipAction updateUserMembershipAction, long id) {
        switch (updateUserMembershipAction) {
        case ACTIVATE_RENEWAL_STATUS:
            membershipServiceHandler.enableAutoRenewal(id);
            break;
        case DEACTIVATE_RENEWAL_STATUS:
            membershipServiceHandler.disableAutoRenewal(id);
            break;
        case SET_REMIND_ME_LATER:
            membershipServiceHandler.setRemindMeLater(id);
            break;
        default:
            throw new ServiceException(String.format("Invalid membership operation %s", updateUserMembershipAction), ServiceName.FRONTEND_API);
        }
    }

    private UpdateUserMembershipAction getUpdateUserMembershipAction(UpdateUserMembershipRequest updateUserMembershipRequest) {
        return Optional.ofNullable(updateUserMembershipRequest)
                .map(UpdateUserMembershipRequest::getAction)
                .orElse(UpdateUserMembershipAction.UNKNOWN);
    }

}
