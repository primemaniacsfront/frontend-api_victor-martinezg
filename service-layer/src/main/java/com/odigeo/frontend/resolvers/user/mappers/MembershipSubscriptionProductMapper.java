package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionResponse;
import com.odigeo.dapi.client.MembershipSubscriptionShoppingItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface MembershipSubscriptionProductMapper {

    @Mapping(target = "primeMember.name", source = "membershipSubscriptionShoppingItem.name")
    @Mapping(target = "primeMember.lastNames", source = "membershipSubscriptionShoppingItem.lastName")
    MembershipSubscriptionResponse fromMembershipShoppingItem(MembershipSubscriptionShoppingItem membershipSubscriptionShoppingItem);

    @Mapping(target = "primeMember.name", source = "membershipSubscriptionShoppingItem.name")
    @Mapping(target = "primeMember.lastNames", source = "membershipSubscriptionShoppingItem.lastName")
    @Mapping(target = "offerAdded", source = "subscriptionResponse")
    MembershipSubscriptionResponse fromMembershipShoppingItemAndSubscriptionResponse(MembershipSubscriptionShoppingItem membershipSubscriptionShoppingItem, SubscriptionResponse subscriptionResponse);
}
