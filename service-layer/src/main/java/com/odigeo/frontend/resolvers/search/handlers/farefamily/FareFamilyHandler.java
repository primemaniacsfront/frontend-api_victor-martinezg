package com.odigeo.frontend.resolvers.search.handlers.farefamily;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamilyRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.AsyncUtils;
import com.odigeo.frontend.resolvers.search.handlers.pricing.MinPricePolicy;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.services.ItineraryService;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.itineraryapi.v1.response.FareFamily;
import com.odigeo.itineraryapi.v1.response.FareFamilyPerk;
import com.odigeo.itineraryapi.v1.response.RetrieveFareFamiliesResponse;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Singleton
public class FareFamilyHandler {

    @Inject
    private SiteVariations siteVariations;
    @Inject
    private FareFamilyVisibilityMapper fareFamilyVisibilityMapper;
    @Inject
    private SearchResultPriceMapper searchResultPriceMapper;
    @Inject
    private ItineraryService itineraryService;
    @Inject
    private SearchService searchService;
    @Inject
    private MinPricePolicy pricePolicy;
    @Inject
    private AsyncUtils asyncUtils;

    private static final String ITINERARY_INDEX_SEPARATOR = ",";
    private static final String SPACE = " ";

    private static final Set<String> CARRIERS_SCOPE_DISPLAY = Sets.newHashSet(
        "UA", "AA", "AF", "TP", "IB", "KL", "AZ", "BA", "A5", "AY", "SK", "SU", "A3", "OU", "VS", "LO", "SS",
        "UX", "TS", "LH", "LX", "OS", "SN"
    );

    public List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily> retrieveFareFamilies(FareFamilyRequest fareFamilyRequest, VisitInformation visit) {
        List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily> fareFamilies = Collections.emptyList();

        if (checkScopeDisplay(fareFamilyRequest.getCarrierIds(), visit)) {
            int fareItineraryIndex = parseFareItineraryIndex(fareFamilyRequest);
            long searchId = fareFamilyRequest.getSearchId();
            String segmentKeys = parseSegmentKeys(fareFamilyRequest);
            int passengers = Objects.nonNull(fareFamilyRequest.getPassengers()) ? fareFamilyRequest.getPassengers() : 1;

            ExecutorService executor = Executors.newFixedThreadPool(2);

            Future<RetrieveFareFamiliesResponse> familiesResponseFuture = executor.submit(fareFamiliesTask(searchId, fareItineraryIndex, segmentKeys));
            Future<BigDecimal> bigDecimalFuture = executor.submit(itineraryPriceTask(searchId, fareItineraryIndex, passengers));

            asyncUtils.shutdownAndWait(executor);

            RetrieveFareFamiliesResponse fareFamiliesResponse = asyncUtils.mapFuture(familiesResponseFuture, RetrieveFareFamiliesResponse.class);
            BigDecimal itineraryPrice = asyncUtils.mapFuture(bigDecimalFuture, BigDecimal.class);

            if (itineraryPrice != null) {
                fareFamilies = buildFareFamilies(fareFamiliesResponse, itineraryPrice);
            }

        }

        return fareFamilies;
    }

    private Callable<BigDecimal> itineraryPriceTask(long searchId, int fareItineraryIndex, int passengers) {
        return () -> fetchItineraryPrice(searchId, fareItineraryIndex, passengers);
    }

    private Callable<RetrieveFareFamiliesResponse> fareFamiliesTask(long searchId, int fareItineraryIndex, String segmentKeys) {
        return () -> itineraryService.retrieveFareFamilies(searchId, fareItineraryIndex, segmentKeys);
    }

    public void populateIsAvailable(SearchItineraryDTO itinerary, VisitInformation visit) {
        boolean isAvailable = checkScopeDisplay(itinerary.getSegmentsCarrierIds(), visit);

        itinerary.setIsFareUpgradeAvailable(isAvailable);
    }

    private boolean checkScopeDisplay(Collection<String> carrierIds, VisitInformation visit) {
        return siteVariations.isFareFamilyTestEnabled(visit)
            && carrierIds.stream().anyMatch(CARRIERS_SCOPE_DISPLAY::contains);
    }

    private int parseFareItineraryIndex(FareFamilyRequest fareFamilyRequest) {
        return Integer.parseInt(fareFamilyRequest.getFareItineraryKey().split(ITINERARY_INDEX_SEPARATOR)[0]);
    }

    private String parseSegmentKeys(FareFamilyRequest fareFamilyRequest) {
        return String.join(SPACE, fareFamilyRequest.getSegmentKeys());
    }

    private List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily> buildFareFamilies(RetrieveFareFamiliesResponse response, BigDecimal itineraryPrice) {
        return Optional.ofNullable(response)
            .map(RetrieveFareFamiliesResponse::getFareFamilies)
            .map(fareFamilies -> mapFareFamilies(fareFamilies, itineraryPrice))
            .orElse(Collections.emptyList());
    }

    private List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily> mapFareFamilies(List<FareFamily> fareFamilies, BigDecimal itineraryPrice) {
        return fareFamilies.stream()
            .map(fareFamily -> mapFareFamily(fareFamily, itineraryPrice))
            .collect(Collectors.toList());
    }

    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily mapFareFamily(FareFamily fareFamily, BigDecimal itineraryPrice) {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily fareFamilyDTO = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily();

        fareFamilyDTO.setProviderItineraryIndex(fareFamily.getProviderItineraryIndex());
        fareFamilyDTO.setName(fareFamily.getName());
        fareFamilyDTO.setCabinClass(fareFamily.getCabinClass());
        fareFamilyDTO.setPrice(getFareFamilyPrice(fareFamily));
        fareFamilyDTO.setTotalPrice(getFareFamilyTotalPrice(fareFamily, itineraryPrice));
        mapFareFamilyPerks(fareFamilyDTO, fareFamily);

        return fareFamilyDTO;
    }

    private void mapFareFamilyPerks(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily fareFamilyDTO, FareFamily fareFamily) {
        List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamilyPerk> fareFamilyPerkDTOS = fareFamily.getFareFamilyPerks().stream()
            .map(this::mapFareFamilyPerk)
            .collect(Collectors.toList());
        fareFamilyDTO.setFareFamilyPerks(fareFamilyPerkDTOS);
    }

    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamilyPerk mapFareFamilyPerk(FareFamilyPerk fareFamilyPerk) {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamilyPerk fareFamilyPerkDTO = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamilyPerk();
        String perkType = fareFamilyPerk.getFareUpgradePerkType();

        fareFamilyPerkDTO.setDescription(fareFamilyPerk.getDescription());
        fareFamilyPerkDTO.setAvailability(fareFamilyPerk.getAvailability());
        fareFamilyPerkDTO.setFareUpgradePerkType(perkType);
        fareFamilyPerkDTO.setFareUpgradePerkTypeGroup(fareFamilyPerk.getFareUpgradePerkTypeGroup());
        fareFamilyPerkDTO.setUnits(fareFamilyPerk.getUnits());
        fareFamilyPerkDTO.setWeight(fareFamilyPerk.getWeight());

        String visibility = fareFamilyVisibilityMapper.map(perkType);
        if (StringUtils.isNotEmpty(visibility)) {
            fareFamilyPerkDTO.setVisibility(visibility);
        }

        return fareFamilyPerkDTO;
    }

    private Money getFareFamilyPrice(FareFamily fareFamily) {
        return buildFareFamilyPrice(fareFamily, null);
    }

    private Money getFareFamilyTotalPrice(FareFamily fareFamily, BigDecimal itineraryPrice) {
        return buildFareFamilyPrice(fareFamily, itineraryPrice);
    }

    private Money buildFareFamilyPrice(FareFamily fareFamily, BigDecimal itineraryPrice) {
        Money price = new Money();
        BigDecimal amount = Objects.nonNull(itineraryPrice) ? itineraryPrice.add(fareFamily.getPrice()) : fareFamily.getPrice();

        price.setAmount(amount);
        price.setCurrency(fareFamily.getCurrency());

        return price;
    }

    private BigDecimal fetchItineraryPrice(long searchId, int fareItineraryIndex, int passengers) {
        SearchResults searchResult = searchService.getSearchResult(searchId, fareItineraryIndex);

        return Optional.ofNullable(searchResult)
            .map(searchResultPriceMapper::map)
            .map(amount -> pricePolicy.calculatePricePerPassenger(amount, BigDecimal.valueOf(passengers)))
            .orElse(null);
    }
}
