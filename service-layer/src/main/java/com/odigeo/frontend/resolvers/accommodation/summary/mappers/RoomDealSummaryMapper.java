package com.odigeo.frontend.resolvers.accommodation.summary.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.commons.model.BoardTypeDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.RoomDealSummaryDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.RoomSmokingPreferenceDTO;
import com.odigeo.searchengine.v2.responses.accommodation.RoomDeal;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;

@Singleton
public class RoomDealSummaryMapper {

    public RoomDealSummaryDTO map(RoomDeal roomDeal) {
        RoomDealSummaryDTO dto = new RoomDealSummaryDTO();
        dto.setRoomId(roomDeal.getRoomId());
        dto.setRoomKey(roomDeal.getKey());
        dto.setDescription(roomDeal.getDescription());
        if (CollectionUtils.isNotEmpty(roomDeal.getBedsDescriptions())) {
            dto.setBedsDescription(roomDeal.getBedsDescriptions().get(0));
        } else {
            dto.setBedsDescription(roomDeal.getRoomType());
        }
        dto.setCancelPolicy(roomDeal.getCancelPolicy());
        dto.setSmokingPreference(roomDeal.getSmokingPreference() != null ? EnumUtils.getEnum(RoomSmokingPreferenceDTO.class, roomDeal.getSmokingPreference().value()) : null);
        dto.setProviderId(roomDeal.getProviderId());
        dto.setCancellationFree(roomDeal.isIsCancellationFree());
        dto.setDepositRequired(roomDeal.isDepositRequired());
        dto.setBoardType(roomDeal.getBoardType() != null ? EnumUtils.getEnum(BoardTypeDTO.class, roomDeal.getBoardType().value()) : null);
        return dto;
    }

    public RoomDealSummaryDTO map(RoomDeal roomDeal, boolean payAtDestination) {
        RoomDealSummaryDTO dto = map(roomDeal);
        dto.setPaymentAtDestination(payAtDestination && !dto.getDepositRequired());
        return dto;
    }

}
