package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.TestDimensionPartitionsMapper;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.HotelSummaryRequestMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.HotelDetailsRequestDTO;
import com.odigeo.frontend.services.AccommodationStaticContentService;
import com.odigeo.hcsapi.v9.beans.HotelDetailsResponse;
import com.odigeo.hcsapi.v9.beans.HotelSummaryResponse;
import com.odigeo.hcsapi.v9.beans.RoomsDetailRequest;
import com.odigeo.hcsapi.v9.beans.RoomsDetailResponse;
import com.odigeo.hcsapi.v9.beans.SendPackage;
import com.odigeo.searchengine.v2.SearchEngineServiceRest;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;

import java.util.Arrays;

@Singleton
public class AccommodationDataHandler {

    @Inject
    private SearchEngineServiceRest searchEngineService;
    @Inject
    private AccommodationStaticContentService accommodationStaticContentService;
    @Inject
    private TestDimensionPartitionsMapper testDimensionPartitionsMapper;
    @Inject
    private HotelSummaryRequestMapper hotelSummaryRequestMapper;


    public SearchRoomResponse getRoomAvailability(Long searchId, Long accommodationDealKey, VisitInformation visit) {
        return searchEngineService.searchRoomAvailability(searchId, accommodationDealKey, visit.getVisitCode());
    }

    public RoomsDetailResponse getStaticRoomDetails(String providerId, String providerHotelId, VisitInformation visit, String... roomId) {
        RoomsDetailRequest roomsDetailRequest = new RoomsDetailRequest();
        roomsDetailRequest.setLocale(visit.getLocale());
        roomsDetailRequest.setTestDimensionPartitions(testDimensionPartitionsMapper.map(visit.getDimensionPartitionMap()));
        roomsDetailRequest.setRoomIds(Arrays.asList(roomId));
        roomsDetailRequest.setSupplierCode(providerId);
        roomsDetailRequest.setSupplierHotelId(providerHotelId);
        return accommodationStaticContentService.getRoomsDetail(roomsDetailRequest);
    }

    public HotelSummaryResponse getAccommodationSummary(HotelDetailsRequestDTO requestDTO, VisitInformation visit) {
        SendPackage sendPackage = hotelSummaryRequestMapper.map(requestDTO.getProviderKeys());
        sendPackage.setLocale(visit.getLocale());
        sendPackage.setTestDimensionPartitions(testDimensionPartitionsMapper.map(visit.getDimensionPartitionMap()));
        return accommodationStaticContentService.getHotelsSummary(sendPackage);
    }

    public HotelDetailsResponse getAccommodationDetails(HotelDetailsRequestDTO request, VisitInformation visit) {
        SendPackage sendPackage = hotelSummaryRequestMapper.map(request.getProviderKeys());
        sendPackage.setLocale(visit.getLocale());
        sendPackage.setTestDimensionPartitions(testDimensionPartitionsMapper.map(visit.getDimensionPartitionMap()));
        return accommodationStaticContentService.getHotelsDetail(sendPackage);
    }
}
