package com.odigeo.frontend.resolvers.trip.bookingimportantmessage.mapper;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CancellationMessage;
import org.mapstruct.Mapper;

@Mapper(uses = CancellationMapper.class)
public interface BookingImportantMessageMapper {

    CancellationMessage toContract(com.edreamsodigeo.bookingimportantmessage.contract.v1.model.cancellation.CancellationMessage message);

}
