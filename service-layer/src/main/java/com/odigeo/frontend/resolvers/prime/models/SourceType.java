package com.odigeo.frontend.resolvers.prime.models;

import java.util.Optional;

public enum SourceType {
    POST_BOOKING,
    FUNNEL_BOOKING;

    public static SourceType fromValue(String value) {
        return Optional.ofNullable(value).map(SourceType::valueOf).orElse(null);
    }
}
