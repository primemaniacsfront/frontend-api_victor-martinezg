package com.odigeo.frontend.resolvers.emailsuggestions.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.emailsuggestions.configuration.EmailSuggestionsConfiguration;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class EmailSuggestionsHandler {

    private final List<String> suggestEmailGenericDomains;
    private final Map<String, List<String>> emailProvidersAutocomplete;

    @Inject
    public EmailSuggestionsHandler(EmailSuggestionsConfiguration emailSuggestionsConfiguration) {
        this.suggestEmailGenericDomains = emailSuggestionsConfiguration.getSuggestEmailGenericDomains();
        this.emailProvidersAutocomplete = emailSuggestionsConfiguration.getEmailProvidersAutocomplete();
    }

    public List<String> getEmailSuggestions(String email, ResolverContext context) {
        List<String> result = new ArrayList<>();
        if (null != email) {
            result = getMatches(email.toLowerCase(new Locale(context.getVisitInformation().getLocale())), context.getVisitInformation().getSite().name());
        }
        return result;
    }

    private List<String> getMatches(String domain, String websiteCode) {
        List<String> results = new ArrayList<>();
        for (String anEmailDomainsList : getSuggestionEmailDomainsList(websiteCode)) {
            if (isValidSuggestion(domain, anEmailDomainsList)) {
                results.add(anEmailDomainsList);
            }
        }
        return results;
    }

    private List<String> getSuggestionEmailDomainsList(String websiteCode) {
        List<String> emailProviders = Optional.ofNullable(emailProvidersAutocomplete.get(websiteCode)).orElse(Collections.emptyList());
        return Stream.concat(emailProviders.stream(), suggestEmailGenericDomains.stream()).collect(Collectors.toList());
    }

    private boolean isValidSuggestion(String domain, String suggestion) {
        return StringUtils.isBlank(domain) || (suggestion.startsWith(domain) && !domain.equals(suggestion));
    }
}
