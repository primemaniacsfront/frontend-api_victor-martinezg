package com.odigeo.frontend.resolvers.accommodation.product.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CollectionMethod;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartCollectionOption;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;

@Mapper
public interface AvailableCollectionMethodsMapper {
//TODO this is not a product mapper, we should move it to the right package
    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    AvailableCollectionMethodsResponse paymentInformationContractToModel(ShoppingCart shoppingCart);

    @Mapping(target = ".", source = "method")
    CollectionMethod collectionMethodContractToModel(ShoppingCartCollectionOption collectionOption);
}
