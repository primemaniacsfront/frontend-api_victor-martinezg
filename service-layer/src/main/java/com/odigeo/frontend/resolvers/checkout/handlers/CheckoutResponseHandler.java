package com.odigeo.frontend.resolvers.checkout.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.BookingResponseStatus;
import com.odigeo.dapi.client.FlowError;
import com.odigeo.dapi.client.MessageResponse;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.checkout.CheckoutFetcherResult;
import com.odigeo.frontend.resolvers.checkout.CheckoutResolver;
import com.odigeo.frontend.resolvers.checkout.builder.CheckoutErrorBuilder;
import com.odigeo.frontend.resolvers.checkout.mappers.CheckoutMapper;
import graphql.ErrorType;
import graphql.GraphQLContext;
import graphql.GraphQLError;
import graphql.execution.ResultPath;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Singleton
public class CheckoutResponseHandler {

    @Inject
    private CheckoutMapper checkoutMapper;

    public CheckoutFetcherResult buildCheckoutResponse(BookingResponse response, GraphQLContext graphQLContext, String userPaymentInteractionId) {
        return checkoutMapper.mapShoppingCartToCheckoutFetcherResult(response, userPaymentInteractionId, handleDapiErrors(response, graphQLContext));
    }

    private List<GraphQLError> handleDapiErrors(BookingResponse response, GraphQLContext graphQLContext) {
        if (response.getShoppingCart() != null) {
            return handleResponseErrors(response, graphQLContext);
        } else {
            return handleErrors(response.getMessages(), graphQLContext);
        }
    }

    private List<GraphQLError> handleResponseErrors(BookingResponse response, GraphQLContext graphQLContext) {
        if (CollectionUtils.isNotEmpty(response.getShoppingCart().getMessages())) {
            return handleErrors(response.getShoppingCart().getMessages(), graphQLContext);
        } else if (response.getStatus().equals(BookingResponseStatus.BOOKING_PAYMENT_RETRY)) {
            return handleFlowErrors(response.getFlowErrors().getFlowError(), graphQLContext);
        } else if (response.getFlowErrors() != null) {
            throw new ServiceException(graphQLContext.get(CheckoutResolver.PATH).toString() + " " + response.getStatus().toString(), ServiceName.DAPI);
        }
        return Collections.EMPTY_LIST;
    }

    private List<GraphQLError> handleFlowErrors(List<FlowError> flowErrors, GraphQLContext graphQLContext) {
        if (CollectionUtils.isNotEmpty(flowErrors)) {
            return Collections.singletonList(CheckoutErrorBuilder.newError().errorType(ErrorType.ValidationError)
                    .path((ResultPath) graphQLContext.get(CheckoutResolver.PATH)).message(Objects.requireNonNull(flowErrors.stream()
                            .findFirst().orElse(null)).getErrorMessageKey()).extensions(ErrorCodes.VALIDATION, ServiceName.DAPI).build());
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    private List<GraphQLError> handleErrors(List<MessageResponse> messages, GraphQLContext graphQLContext) {
        if (CollectionUtils.isNotEmpty(messages)) {
            return Collections.singletonList(CheckoutErrorBuilder.newError().errorType(ErrorType.ValidationError)
                    .path((ResultPath) graphQLContext.get(CheckoutResolver.PATH)).message(Objects.requireNonNull(messages.stream()
                            .findFirst().orElse(null)).getDescription()).extensions(ErrorCodes.VALIDATION, ServiceName.DAPI).build());
        } else {
            return Collections.EMPTY_LIST;
        }
    }
}
