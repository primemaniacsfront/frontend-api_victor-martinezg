package com.odigeo.frontend.resolvers.search.handlers.campaign;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CampaignConfig;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayConfig;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.itinerary.utils.ItineraryUtils;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;

import java.util.Optional;
import java.util.Set;

@Singleton
public class CampaignConfigHandler {

    @Inject
    private AirlineCampaignHandler airlineCampaignHandler;
    @Inject
    private SiteVariations siteVariations;
    @Inject
    private ItineraryUtils itineraryUtils;
    @Inject
    private PrimeDayHandler primeDayHandler;

    public CampaignConfig populateCampaignConfig(SearchItineraryDTO itinerary, VisitInformation visit) {
        CampaignConfig campaignConfig = new CampaignConfig();

        populatePrimeDayIfEnabled(itinerary.getSectionsCarrierIds(), itinerary, null, visit, campaignConfig);
        populateAirlineIfEnabled(itinerary.getSegmentsCarrierIds(), campaignConfig, visit);

        return campaignConfig;
    }

    public CampaignConfig getCampaignConfig(Itinerary itinerary, VisitInformation visit) {
        CampaignConfig campaignConfig = new CampaignConfig();

        populatePrimeDayIfEnabled(itineraryUtils.getSectionsCarrierIds(itinerary), null, itinerary, visit, campaignConfig);
        populateAirlineIfEnabled(itineraryUtils.getSegmentsCarrierIds(itinerary), campaignConfig, visit);

        return campaignConfig;
    }


    private void populatePrimeDayIfEnabled(Set<String> sectionsCarriersIds, SearchItineraryDTO searchItinerary,
                                           Itinerary itinerary, VisitInformation visit, CampaignConfig campaignConfig) {
        boolean isPrimeDayEnabled = siteVariations.isPrimeDayCampaignEnabled(visit);
        PrimeDayConfig primeDayConfig = primeDayHandler.populatePrimeDayConfig(sectionsCarriersIds, visit);

        // TODO We are delete in a few weeks
        Optional.ofNullable(searchItinerary)
            .ifPresent(iti -> iti.setPrimeDayConfig(primeDayConfig));
        Optional.ofNullable(itinerary)
            .ifPresent(iti -> iti.setPrimeDayConfig(primeDayConfig));
        if (isPrimeDayEnabled) {
            campaignConfig.setPrimeDayConfig(primeDayConfig);
        }
    }

    private void populateAirlineIfEnabled(Set<String> segmentsCarrierIds, CampaignConfig campaignConfig, VisitInformation visit) {
        if (siteVariations.isAirlineCampaignEnabled(visit)) {
            campaignConfig.setAirlineCampaignConfig(airlineCampaignHandler.populateCampaignConfig(segmentsCarrierIds));
        }
    }

}
