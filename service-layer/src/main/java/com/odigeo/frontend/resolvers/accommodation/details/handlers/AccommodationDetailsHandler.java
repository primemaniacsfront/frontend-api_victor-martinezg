package com.odigeo.frontend.resolvers.accommodation.details.handlers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.exceptions.CityNotFoundException;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationDataHandler;
import com.odigeo.frontend.resolvers.accommodation.details.mappers.AccommodationDetailsMapper;
import com.odigeo.frontend.resolvers.accommodation.details.models.AccommodationDetailsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.HotelDetailsRequestDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.hcsapi.v9.beans.HotelDetailsResponse;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Singleton
public class AccommodationDetailsHandler {
    @Inject
    private AccommodationDetailsMapper accommodationDetailsMapper;
    @Inject
    private AccommodationDataHandler accommodationDataHandler;
    @Inject
    private GeoLocationHandler geoLocationHandler;
    @Inject
    private Logger logger;

    public AccommodationDetailsResponseDTO getAccommodationDetails(AccommodationProviderKeyDTO providerKey, VisitInformation visit, Integer destinationGeoCode) {
        return getAccommodationDetails(visit, Collections.singletonList(providerKey), destinationGeoCode);
    }

    private AccommodationDetailsResponseDTO getAccommodationDetails(VisitInformation visit,
                                                                    List<AccommodationProviderKeyDTO> hotelSupplierKey,
                                                                    Integer destinationGeoCode) {
        AccommodationDetailsResponseDTO response = new AccommodationDetailsResponseDTO();
        if (!hotelSupplierKey.isEmpty()) {
            HotelDetailsRequestDTO hotelDetailsRequestDTO = new HotelDetailsRequestDTO();
            hotelDetailsRequestDTO.setProviderKeys(hotelSupplierKey);
            HotelDetailsResponse hotelDetailsResponse = accommodationDataHandler.getAccommodationDetails(hotelDetailsRequestDTO, visit);
            response = Optional.ofNullable(hotelDetailsResponse)
                    .map(HotelDetailsResponse::getHotel)
                    .orElseGet(Collections::emptyMap)
                    .entrySet().stream().findFirst()
                    .map(Map.Entry::getValue)
                    .map(value -> accommodationDetailsMapper.map(value, hotelDetailsResponse.getLegend()))
                    .orElseGet(AccommodationDetailsResponseDTO::new);
            calculateDistance(response, destinationGeoCode);
        }
        return response;
    }

    private void calculateDistance(AccommodationDetailsResponseDTO response, Integer geoCode) {
        try {
            City cityGeonode = geoLocationHandler.getCityGeonode(geoCode, null);
            Optional.ofNullable(response)
                    .map(AccommodationDetailsResponseDTO::getAccommodationDetail)
                    .ifPresent(details -> details.setDistanceFromCityCenter(geoLocationHandler.computeDistanceFromGeonode(cityGeonode, details.getAccommodationDealInformation().getCoordinates())));
        } catch (CityNotFoundException e) {
            logger.warning(this.getClass(), "Error calculating distance from city center", e);
        }
    }
}
