package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.common.ItinerarySortCriteria;
import com.odigeo.searchengine.v2.requests.ItinerarySearchRequest;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;

import java.math.BigDecimal;
import java.util.List;

@Singleton
public class PriceHandler {

    public final List<Fee> calculateItineraryPrices(SearchEngineContext seContext,
                                                    FareItinerary fareItinerary, BigDecimal passengers, AbstractPricePolicy policy) {

        List<Fee> itineraryFees = policy.calculateFees(seContext, fareItinerary);
        itineraryFees.forEach(fee -> {
            policy.populatePricePerPassenger(fee, passengers);
            policy.populateCurrency(fee, seContext.getCurrency());
        });

        return itineraryFees;
    }

    public void populatePricePolicy(ItinerarySearchRequest itineraryRequest,
                                    ItineraryRequest itineraryRequestDTO, boolean isPrimeMarket) {

        boolean isMeta = itineraryRequestDTO.getExternalSelection() != null;

        if (isMeta) {
            itineraryRequest.setSortCriteria(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
            itineraryRequest.setMembershipFee(isPrimeMarket);
        } else {
            itineraryRequest.setSortCriteria(isPrimeMarket
                    ? ItinerarySortCriteria.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE
                    : ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
        }
    }

}
