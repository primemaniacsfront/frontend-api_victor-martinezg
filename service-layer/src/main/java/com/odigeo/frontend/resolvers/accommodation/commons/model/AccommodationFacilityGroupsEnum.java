package com.odigeo.frontend.resolvers.accommodation.commons.model;

import java.util.Arrays;
import java.util.List;

public enum AccommodationFacilityGroupsEnum {
    AC {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(109, 517, 823);
        }
    },
    BAR_RESTAURANT {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(824, 467, 3, 7, 115, 116, 117, 301, 305);
        }
    },
    LIFT {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(48, 155);
        }
    },
    FREE_AIRPORT_SHUTTLE {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(617, 624, 626, 639, 640, 139);
        }
    },
    FREE_PARKING {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(652, 46, 636, 656);
        }
    },
    FREE_WIFI {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(107);
        }
    },
    GYM {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(417, 11, 210, 230, 11, 660, 667);
        }
    },
    HANDICAPPED {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(157, 25);
        }
    },
    HEATING {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(80);
        }
    },
    PETS_ALLOWED {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(4, 429, 448);
        }
    },
    ROOM_SERVICE {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(5, 913);
        }
    },
    SPA_SAUNA {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(10, 54);
        }
    },
    SWIMMING_POOL {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(202, 103, 104, 200, 672, 805);
        }
    },
    COVID_MEASURES {
        @Override
        public List<Integer> getFacilities() {
            return Arrays.asList(882, 904, 913, 884, 876, 878, 890, 877, 891, 892, 895, 896, 887, 879, 897, 910, 880, 872,
                873, 913, 886, 889, 888, 901, 902, 903, 904, 874, 894, 906, 875, 912, 909, 914, 868, 893, 911, 908, 900,
                869, 907, 898, 899, 870, 905, 871, 885, 881, 883);
        }
    };

    public abstract List<Integer> getFacilities();
}

