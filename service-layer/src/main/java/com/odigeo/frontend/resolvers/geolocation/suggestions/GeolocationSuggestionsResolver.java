package com.odigeo.frontend.resolvers.geolocation.suggestions;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.geolocation.suggestions.handlers.GeolocationSuggestionsHandler;
import com.odigeo.frontend.resolvers.geolocation.suggestions.models.GeolocationSuggestionsRequestDTO;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

@Singleton
public class GeolocationSuggestionsResolver implements Resolver {

    private static final String GEOLOCATION_SUGGESTIONS_QUERY = "geolocationSuggestions";

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private GeolocationSuggestionsHandler geolocationSuggestionsHandler;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher(GEOLOCATION_SUGGESTIONS_QUERY, this::getGeolocationSuggestions));
    }

    GeolocationSuggestionsResponse getGeolocationSuggestions(DataFetchingEnvironment env) {
        GeolocationSuggestionsRequest geolocationSuggestionsRequest = jsonUtils.fromDataFetching(env, GeolocationSuggestionsRequest.class);
        ResolverContext context = env.getContext();
        VisitInformation visit = context.getVisitInformation();

        GeolocationSuggestionsRequestDTO geolocationSuggestionsRequestDTO = geolocationSuggestionsHandler
                .buildGeolocationSuggestionsRequest(geolocationSuggestionsRequest, visit);

        return geolocationSuggestionsHandler.getGeolocationSuggestions(geolocationSuggestionsRequestDTO);
    }
}
