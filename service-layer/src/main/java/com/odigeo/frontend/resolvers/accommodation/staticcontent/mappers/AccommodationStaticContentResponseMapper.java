package com.odigeo.frontend.resolvers.accommodation.staticcontent.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationStaticContentResponse;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.model.AccommodationStaticContentResponseDTO;
import org.mapstruct.Mapper;

@Mapper
public interface AccommodationStaticContentResponseMapper {

    AccommodationStaticContentResponse mapModelToContract(AccommodationStaticContentResponseDTO response);

}
