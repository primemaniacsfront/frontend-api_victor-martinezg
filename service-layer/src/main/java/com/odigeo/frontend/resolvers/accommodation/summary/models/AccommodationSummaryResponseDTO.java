package com.odigeo.frontend.resolvers.accommodation.summary.models;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;

import java.util.List;

public class AccommodationSummaryResponseDTO {
    private AccommodationDetailDTO accommodationDetail;
    private List<RoomDealSummaryDTO> roomDeals;
    private AccommodationReview accommodationReview;

    public AccommodationSummaryResponseDTO() {
    }

    public AccommodationDetailDTO getAccommodationDetail() {
        return accommodationDetail;
    }

    public void setAccommodationDetail(AccommodationDetailDTO accommodationDetail) {
        this.accommodationDetail = accommodationDetail;
    }

    public List<RoomDealSummaryDTO> getRoomDeals() {
        return roomDeals;
    }

    public void setRoomDeals(List<RoomDealSummaryDTO> roomDeals) {
        this.roomDeals = roomDeals;
    }

    public AccommodationReview getAccommodationReview() {
        return accommodationReview;
    }

    public void setAccommodationReview(AccommodationReview accommodationReview) {
        this.accommodationReview = accommodationReview;
    }
}
