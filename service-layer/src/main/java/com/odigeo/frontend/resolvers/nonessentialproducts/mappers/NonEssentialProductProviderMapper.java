package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductProviderType;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.Insurance;

import java.util.Arrays;

@Singleton
public class NonEssentialProductProviderMapper {

    public NonEssentialProductProviderType map(Insurance insurance) {
        return Arrays.stream(NonEssentialProductProviderType.values())
                .filter(nonEssentialProductsProviderType -> nonEssentialProductsProviderType.name().equals(insurance.getProvider()))
                .findFirst()
                .orElse(null);
    }
}
