package com.odigeo.frontend.resolvers.prime.models;

import java.util.Set;

public class PrimeMarket {
    private PrimeMarketStatus status;
    private Set<String> excludedMktPortals;
    private boolean hotelsEnabled;
    private boolean carsEnabled;
    private boolean dynpackEnabled;
    private int expiringCreditCardPeriod;

    public PrimeMarket() {
    }

    public PrimeMarketStatus getStatus() {
        return status;
    }

    public void setStatus(PrimeMarketStatus status) {
        this.status = status;
    }

    public Set<String> getExcludedMktPortals() {
        return excludedMktPortals;
    }

    public void setExcludedMktPortals(Set<String> excludedMktPortals) {
        this.excludedMktPortals = excludedMktPortals;
    }

    public boolean isHotelsEnabled() {
        return hotelsEnabled;
    }

    public void setHotelsEnabled(boolean hotelsEnabled) {
        this.hotelsEnabled = hotelsEnabled;
    }

    public boolean isDynpackEnabled() {
        return dynpackEnabled;
    }

    public void setDynpackEnabled(boolean dynpackEnabled) {
        this.dynpackEnabled = dynpackEnabled;
    }

    public boolean isCarsEnabled() {
        return carsEnabled;
    }

    public void setCarsEnabled(boolean carsEnabled) {
        this.carsEnabled = carsEnabled;
    }

    public int getExpiringCreditCardPeriod() {
        return expiringCreditCardPeriod;
    }

    public void setExpiringCreditCardPeriod(int expiringCreditCardPeriod) {
        this.expiringCreditCardPeriod = expiringCreditCardPeriod;
    }
}
