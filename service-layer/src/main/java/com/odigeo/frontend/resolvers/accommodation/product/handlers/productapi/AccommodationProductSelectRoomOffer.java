package com.odigeo.frontend.resolvers.accommodation.product.handlers.productapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectRoomOfferResponse;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.SelectRoomOfferHandler;
import com.odigeo.frontend.services.accommodation.AccommodationOfferService;
import graphql.GraphQLContext;

public class AccommodationProductSelectRoomOffer implements SelectRoomOfferHandler {

    @Inject
    private AccommodationOfferService accommodationOfferService;

    @Override
    public SelectRoomOfferResponse selectRoomOffer(String searchId, String accommodationDealKey, String roomDealKey, ResolverContext context, GraphQLContext graphQLContext) {
        SelectRoomOfferResponse response = new SelectRoomOfferResponse();
        String dealId = accommodationOfferService.checkAvailability(searchId, accommodationDealKey, roomDealKey);
        response.setDealId(dealId);
        return response;
    }
}
