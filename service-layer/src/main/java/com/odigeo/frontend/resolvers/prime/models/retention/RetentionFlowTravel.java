package com.odigeo.frontend.resolvers.prime.models.retention;

public class RetentionFlowTravel {

    private String date;
    private String arrivalCity;

    public RetentionFlowTravel() {
    }

    public RetentionFlowTravel(String date, String arrivalCity) {
        this.date = date;
        this.arrivalCity = arrivalCity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }
}
