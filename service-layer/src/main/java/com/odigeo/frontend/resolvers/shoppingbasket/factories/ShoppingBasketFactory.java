package com.odigeo.frontend.resolvers.shoppingbasket.factories;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Inject;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.ShoppingBasketHandler;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.ShoppingBasketOperations;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.ShoppingBasketV2Handler;

public class ShoppingBasketFactory {

    @Inject
    private ShoppingBasketV2Handler shoppingBasketV2Handler;
    @Inject
    private ShoppingBasketHandler shoppingBasketHandler;

    public ShoppingBasketOperations getInstance(ShoppingType shoppingType) {
        return ShoppingType.BASKET_V3.equals(shoppingType) ? shoppingBasketHandler : shoppingBasketV2Handler;
    }

}
