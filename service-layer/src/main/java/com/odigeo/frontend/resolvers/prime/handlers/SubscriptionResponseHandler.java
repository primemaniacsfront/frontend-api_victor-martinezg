package com.odigeo.frontend.resolvers.prime.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionOfferDuration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionOfferPeriod;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionOfferTimeUnit;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionOfferType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionResponse;
import com.google.inject.Singleton;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffers;
import com.odigeo.membership.offer.api.response.TimeUnit;
import graphql.VisibleForTesting;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class SubscriptionResponseHandler {

    public static final int DEFAULT_DURATION = 1;
    private static final Predicate<MembershipSubscriptionOffer> EXISTING_OFFER_TYPE = subOffer -> Optional.ofNullable(subOffer)
            .map(MembershipSubscriptionOffer::getMembershipType)
            .map(type -> Stream.of(SubscriptionOfferType.values())
                            .map(Enum::name)
                            .anyMatch(existentType -> existentType.equals(type))
            ).orElse(Boolean.FALSE);

    public List<SubscriptionResponse> buildSubscriptionOffersResponse(MembershipSubscriptionOffers subscriptionOffers) {
        return Optional.ofNullable(subscriptionOffers)
                .map(MembershipSubscriptionOffers::getMembershipSubscriptionOffers)
                .orElse(Collections.emptyList())
                .stream()
                .filter(EXISTING_OFFER_TYPE)
                .map(this::mapSubscriptionOfferToSubscriptionResponse)
                .collect(Collectors.toList());
    }

    public SubscriptionResponse buildSubscriptionResponse(MembershipSubscriptionOffer subscriptionOffer) {
        return Optional.ofNullable(subscriptionOffer)
                .map(this::mapSubscriptionOfferToSubscriptionResponse)
                .orElse(null);
    }

    private SubscriptionResponse mapSubscriptionOfferToSubscriptionResponse(MembershipSubscriptionOffer primeOffer) {
        SubscriptionResponse response = new SubscriptionResponse();
        Money price = new Money(primeOffer.getPrice(), primeOffer.getCurrencyCode());
        Money renewalPrice = new Money(primeOffer.getRenewalPrice(), primeOffer.getCurrencyCode());
        SubscriptionOfferDuration duration = buildSubscriptionDuration(getDurationOrDefault(primeOffer.getDuration()), primeOffer.getDurationTimeUnit());
        SubscriptionOfferDuration renewalDuration = buildSubscriptionDuration(getDurationOrDefault(primeOffer.getRenewalDuration()), TimeUnit.MONTHS);
        response.setOfferId(primeOffer.getOfferId());
        response.setProductId(primeOffer.getProductId());
        response.setFee(price);
        response.setPrice(price);
        response.setRenewalPrice(renewalPrice);
        response.setDuration(duration);
        response.setRenewalDuration(renewalDuration);
        response.setSubscriptionPeriod(getOfferPeriod(primeOffer.getPrice()));
        response.setType(getMembershipType(primeOffer));
        return response;
    }

    @VisibleForTesting
    SubscriptionOfferType getMembershipType(MembershipSubscriptionOffer primeOffer) {
        try {
            return SubscriptionOfferType.valueOf(primeOffer.getMembershipType());
        } catch (IllegalArgumentException e) {
            return SubscriptionOfferType.BASIC;
        }
    }

    private SubscriptionOfferDuration buildSubscriptionDuration(Integer amount, TimeUnit timeUnit) {
        SubscriptionOfferDuration subscriptionDuration = new SubscriptionOfferDuration();
        subscriptionDuration.setAmount(amount);
        subscriptionDuration.setTimeUnit(mapTimeUnit(timeUnit));
        return subscriptionDuration;
    }

    private SubscriptionOfferTimeUnit mapTimeUnit(TimeUnit timeUnit) {
        return TimeUnit.DAYS.equals(timeUnit) ? SubscriptionOfferTimeUnit.DAYS : SubscriptionOfferTimeUnit.MONTHS;
    }

    private SubscriptionOfferPeriod getOfferPeriod(BigDecimal price) {
        return price.equals(BigDecimal.ZERO) ? SubscriptionOfferPeriod.FREE_TRIAL : SubscriptionOfferPeriod.PAID;
    }

    @VisibleForTesting
    int getDurationOrDefault(Integer duration) {
        return Optional.ofNullable(duration).orElse(DEFAULT_DURATION);
    }
}
