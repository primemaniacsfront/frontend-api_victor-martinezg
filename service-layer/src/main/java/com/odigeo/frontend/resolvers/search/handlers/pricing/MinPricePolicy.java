package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.frontend.resolvers.search.mappers.PaymentMethodMapper;
import com.odigeo.searchengine.v2.responses.collection.CollectionEstimationFees;
import com.odigeo.searchengine.v2.responses.collection.CollectionMethod;
import com.odigeo.searchengine.v2.responses.collection.CollectionMethodKeyPrice;
import com.odigeo.searchengine.v2.responses.collection.CreditCardType;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import org.apache.commons.collections4.CollectionUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class MinPricePolicy extends AbstractPricePolicy {

    private static final String DEFAULT_FEE_TYPE_ID = "CHEAPEST";

    @Inject
    private PaymentMethodMapper pmMapper;
    @Inject
    private PaymentMethodManager pmManager;
    @Inject
    private Logger logger;

    @Override
    protected List<Fee> calculateFees(SearchEngineContext seContext, FareItinerary fareItinerary) {
        Map<Integer, CollectionEstimationFees> collectionFeesMap = seContext.getCollectionFeesMap();
        Map<Integer, CollectionMethod> collectionMethodMap = seContext.getCollectionMethodMap();

        Integer collectionId = fareItinerary.getCollectionMethodFees();
        CollectionEstimationFees collectionFees = collectionFeesMap.get(collectionId);

        List<CollectionMethodKeyPrice> keyPrices = Optional.ofNullable(collectionFees)
            .map(CollectionEstimationFees::getCollectionMethodFees)
            .orElse(Collections.emptyList());

        List<CollectionMethodKeyPrice> filteredKeyPrices = filterKeyPrices(keyPrices, collectionMethodMap);
        List<Fee> fees = Collections.emptyList();

        if (CollectionUtils.isNotEmpty(filteredKeyPrices)) {
            CollectionMethodKeyPrice cheapestKeyPrice = calculateCheapest(filteredKeyPrices);
            BigDecimal sortPrice = fareItinerary.getPrice().getSortPrice();

            fees = filteredKeyPrices.stream()
                .map(keyPrice -> createCollectionFee(keyPrice, cheapestKeyPrice, sortPrice, collectionMethodMap))
                .collect(Collectors.toList());

            fees.add(createDefaultCollectionFee(cheapestKeyPrice, sortPrice, collectionMethodMap));
        }

        return fees;
    }

    @Override
    public String calculateDefaultFeeTypeId() {
        return DEFAULT_FEE_TYPE_ID;
    }

    private List<CollectionMethodKeyPrice> filterKeyPrices(List<CollectionMethodKeyPrice> keyPrices,
                                                           Map<Integer, CollectionMethod> collectionMethodMap) {

        return CollectionUtils.emptyIfNull(keyPrices).stream()
            .filter(keyPrice -> {
                CollectionMethod collectionMethod = collectionMethodMap.get(keyPrice.getCollectionMethodKey());
                String collectionType = collectionMethod.getType();

                boolean isSupported = pmManager.isSupported(collectionType);

                if (!isSupported) {
                    logger.warning(this.getClass(), "payment method not supported: " + collectionType);
                }

                return isSupported;

            }).collect(Collectors.toList());
    }

    private Fee createDefaultCollectionFee(CollectionMethodKeyPrice cheapestKeyPrice, BigDecimal sortPrice,
                                              Map<Integer, CollectionMethod> collectionMethodMap) {

        Fee defaultFee = createCollectionFee(cheapestKeyPrice, cheapestKeyPrice, sortPrice, collectionMethodMap);
        defaultFee.getType().setId(calculateDefaultFeeTypeId());

        return defaultFee;
    }

    private Fee createCollectionFee(CollectionMethodKeyPrice keyPrice, CollectionMethodKeyPrice cheapestKeyPrice,
                                       BigDecimal sortPrice, Map<Integer, CollectionMethod> collectionMethodMap) {

        BigDecimal collectionFee = keyPrice.getPrice();
        BigDecimal cheapestCollectionFee = cheapestKeyPrice.getPrice();
        CollectionMethod collectionMethod = collectionMethodMap.get(keyPrice.getCollectionMethodKey());

        Fee fee = new Fee();
        fee.setPrice(calculatePrice(sortPrice, cheapestCollectionFee, collectionFee));
        fee.setType(calculateFeeType(collectionMethod));

        return fee;
    }

    private FeeType calculateFeeType(CollectionMethod collectionMethod) {
        String feeTypeName = Optional.ofNullable(collectionMethod.getCreditCardType())
            .map(CreditCardType::getName).orElse(null);

        FeeType feeType = new FeeType();
        feeType.setId(Integer.toString(collectionMethod.getId()));
        feeType.setName(feeTypeName);
        feeType.setPaymentMethod(pmMapper.map(collectionMethod.getType()));

        return feeType;
    }

    private CollectionMethodKeyPrice calculateCheapest(List<CollectionMethodKeyPrice> keyPrices) {
        return keyPrices.stream()
            .min(Comparator.comparing(CollectionMethodKeyPrice::getPrice))
            .orElse(null);
    }

    private Money calculatePrice(BigDecimal sortPrice, BigDecimal cheapestCollectionFee, BigDecimal collectionFee) {
        Money price = new Money();
        BigDecimal amount = sortPrice.subtract(cheapestCollectionFee).add(collectionFee);

        price.setAmount(amount);
        return price;
    }

}
