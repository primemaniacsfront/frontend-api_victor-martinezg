package com.odigeo.frontend.resolvers.shoppingbasket.handlers.dapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.AvailableCollectionMethodsHandler;
import com.odigeo.frontend.resolvers.accommodation.product.mappers.AvailableCollectionMethodsMapper;
import com.odigeo.frontend.services.DapiService;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

@Singleton
public class ShoppingCartAvailableCollectionMethods implements AvailableCollectionMethodsHandler {

    @Inject
    private DapiService dapiService;
    @Inject
    private AvailableCollectionMethodsMapper availableCollectionMethodsMapper;

    @Override
    public AvailableCollectionMethodsResponse getAvailableCollectionMethods(String shoppingBasketId, ResolverContext context) {
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = dapiService.getShoppingCart(parseShoppingCartId(shoppingBasketId), context);
        AvailableCollectionMethodsResponse response = availableCollectionMethodsMapper.paymentInformationContractToModel(shoppingCartSummaryResponse.getShoppingCart());
        Optional.ofNullable(response)
            .map(AvailableCollectionMethodsResponse::getCollectionOptions)
            .map(Collection::stream)
            .orElseGet(Stream::empty)
            .findFirst()
            .ifPresent(option -> option.setIsChosenMethod(true));
        return response;
    }

    private long parseShoppingCartId(String shoppingBasketId) {
        try {
            return Long.parseLong(shoppingBasketId);
        } catch (NumberFormatException e) {
            throw new ServiceException(String.format("shoppingId is not a valid number: %s", shoppingBasketId), e, ServiceName.DAPI);
        }
    }
}
