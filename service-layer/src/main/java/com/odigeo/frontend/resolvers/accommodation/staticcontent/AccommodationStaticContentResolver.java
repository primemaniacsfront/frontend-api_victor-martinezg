package com.odigeo.frontend.resolvers.accommodation.staticcontent;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationStaticContentRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationStaticContentResponse;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.Resolver;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationStaticContentHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.mappers.AccommodationStaticContentResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.model.AccommodationStaticContentResponseDTO;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;

import java.util.List;
import java.util.stream.Collectors;

public class AccommodationStaticContentResolver implements Resolver {

    private static final String ACCOMMODATION_STATIC_CONTENT_QUERY = "accommodationStaticContent";

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private AccommodationStaticContentResponseMapper accommodationStaticContentResponseMapper;
    @Inject
    private AccommodationStaticContentHandler accommodationStaticContentHandler;
    @Inject
    private ContentKeyMapper contentKeyMapper;

    @Override
    public void addToBuilder(RuntimeWiring.Builder resolversBuilder) {
        resolversBuilder
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher(ACCOMMODATION_STATIC_CONTENT_QUERY, this::accommodationStaticContentFetcher));
    }


    AccommodationStaticContentResponse accommodationStaticContentFetcher(DataFetchingEnvironment env) {
        AccommodationStaticContentRequest requestParams = jsonUtils.fromDataFetching(env, AccommodationStaticContentRequest.class);
        ResolverContext resolverContext = env.getContext();
        GraphQLContext context = env.getGraphQlContext();

        VisitInformation visit = resolverContext.getVisitInformation();
        MetricsHandler metricsHandler = context.get(MetricsHandler.class);
        metricsHandler.startMetric(MeasureConstants.SEARCH_STATIC_CONTENT_BACKEND_TOTAL_TIME);

        AccommodationStaticContentResponseDTO response = accommodationStaticContentHandler.retrieveStaticContent(visit, getAccommodationProviderKeyDTOS(requestParams));

        metricsHandler.stopMetric(MeasureConstants.SEARCH_STATIC_CONTENT_BACKEND_TOTAL_TIME, visit);
        return accommodationStaticContentResponseMapper.mapModelToContract(response);
    }

    private List<AccommodationProviderKeyDTO> getAccommodationProviderKeyDTOS(AccommodationStaticContentRequest requestParams) {
        return requestParams.getContentKeys().stream().map(contentKey -> contentKeyMapper.extractProviderKeyFromContentKey(contentKey)).collect(Collectors.toList());
    }
}
