package com.odigeo.frontend.resolvers.shoppingcart.mappers.location;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mapstruct.Context;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
@DecoratedWith(LocationMapperDecorator.class)
public interface LocationMapper {

    @Mapping(source = "geoNodeId", target = "id")
    @Mapping(source = "cityIataCode", target = "cityIata")
    @Mapping(source = "iataCode", target = "iata")
    @Mapping(target = "locationType", ignore = true)
    Location locationContractToModel(com.odigeo.dapi.client.Location location);

    @Mapping(target = "id", ignore = true)
    Location locationIdContractToModel(Integer id, @Context ShoppingCartContext context);

}
