package com.odigeo.frontend.resolvers.shoppingbasket.handlers.dapi;

import com.google.inject.Inject;
import com.odigeo.dapi.client.RegisterUserLoginRequest;
import com.odigeo.dapi.client.RegisterUserLoginResult;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.AddUserHandler;
import com.odigeo.frontend.services.DapiService;
import com.odigeo.userprofiles.api.v2.model.User;

public class ShoppingCartAddUser implements AddUserHandler {

    @Inject
    private DapiService dapiService;

    public Boolean addUser(User user, ResolverContext context) {
        RegisterUserLoginResult registerUserLoginResult = dapiService.registerUserLogin(getRegisterUserLoginRequest(user), context);
        return registerUserLoginResult != null;
    }

    private RegisterUserLoginRequest getRegisterUserLoginRequest(User user) {
        RegisterUserLoginRequest request = new RegisterUserLoginRequest();
        request.setLogin(true);
        request.setUserId(user.getId());
        return request;
    }
}
