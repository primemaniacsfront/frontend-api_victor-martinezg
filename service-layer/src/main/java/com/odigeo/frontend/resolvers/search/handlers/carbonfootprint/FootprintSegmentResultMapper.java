package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.SegmentResult;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;

import java.util.stream.Collectors;

@Singleton
class FootprintSegmentResultMapper {

    @Inject
    private FootprintSegmentMapper segmentMapper;

    public SegmentResult map(LegDTO legDTO) {

        SegmentResult segmentResult = new SegmentResult();
        segmentResult.setSegments(legDTO.getSegments().stream().map(segmentMapper::map).collect(Collectors.toList()));
        return segmentResult;

    }

}
