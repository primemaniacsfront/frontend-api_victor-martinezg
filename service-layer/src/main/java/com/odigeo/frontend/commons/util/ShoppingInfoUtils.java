package com.odigeo.frontend.commons.util;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Singleton;
import graphql.VisibleForTesting;

import java.util.Optional;

@Singleton
public class ShoppingInfoUtils {

    @VisibleForTesting
    static final long INVALID_SHOPPING_ID = 0L;

    public ShoppingType getShoppingType(ShoppingInfo shoppingInfo) {
        return Optional.ofNullable(shoppingInfo).map(ShoppingInfo::getShoppingType).orElse(null);
    }

    public long getShoppingId(ShoppingInfo shoppingInfo) {
        return Optional.ofNullable(shoppingInfo).map(ShoppingInfo::getShoppingId).map(this::parseLong).orElse(INVALID_SHOPPING_ID);
    }

    private long parseLong(String value) {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            return INVALID_SHOPPING_ID;
        }
    }
}
