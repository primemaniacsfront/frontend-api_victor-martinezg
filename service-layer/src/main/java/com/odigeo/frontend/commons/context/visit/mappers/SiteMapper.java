package com.odigeo.frontend.commons.context.visit.mappers;

import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.website.Website;
import java.util.Optional;
import javax.inject.Singleton;
import org.apache.commons.lang3.EnumUtils;

@Singleton
public class SiteMapper {

    public Site map(VisitResponse visitResponse) {
        return Optional.ofNullable(visitResponse.getWebsite())
            .map(Website::getCode)
            .map(site -> EnumUtils.getEnum(Site.class, site))
            .orElse(Site.UNKNOWN);
    }

}
