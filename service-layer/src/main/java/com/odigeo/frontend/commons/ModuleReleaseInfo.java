package com.odigeo.frontend.commons;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.rest.monitoring.ModuleInfo;
import com.odigeo.commons.rest.monitoring.ModuleInfoLoader;
import com.odigeo.commons.rest.monitoring.MonitoringConfigurationException;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class ModuleReleaseInfo {

    private static final Logger logger = LoggerFactory.getLogger(ModuleReleaseInfo.class);
    private static final String DEFAULT_ID = "frontend-api";
    private static final String DEFAULT_VERSION = "UNKNOWN";

    @Inject
    private ModuleInfoLoader moduleInfoLoader;

    private ModuleInfo moduleInfo;

    public String formatModuleInfo() {
        moduleInfo = Optional.ofNullable(moduleInfo)
            .orElseGet(() -> {
                try {
                    return moduleInfoLoader.load();
                } catch (MonitoringConfigurationException ex) {
                    logger.error("Error loading artifact and version", ex);
                    return new ModuleInfo(DEFAULT_ID, DEFAULT_VERSION);
                }
            });

        return moduleInfo.toString();
    }

}
