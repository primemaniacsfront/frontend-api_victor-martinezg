package com.odigeo.frontend.commons.context;

import java.util.HashMap;
import java.util.Map;

public class DebugInfo {

    private boolean enabled;
    private boolean qaModeEnabled;
    private boolean apiCaptureEnabled;

    private final Map<String, Object> debugInfoMap = new HashMap<>(0);

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isQaModeEnabled() {
        return qaModeEnabled;
    }

    public void setQaModeEnabled(boolean qaModeEnabled) {
        this.qaModeEnabled = qaModeEnabled;
    }

    public boolean isApiCaptureEnabled() {
        return apiCaptureEnabled;
    }

    public void setApiCaptureEnabled(boolean apiCaptureEnabled) {
        this.apiCaptureEnabled = apiCaptureEnabled;
    }

    public Map<String, Object> getDebugInfoMap() {
        return debugInfoMap;
    }

}
