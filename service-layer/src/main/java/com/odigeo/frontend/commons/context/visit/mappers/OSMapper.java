package com.odigeo.frontend.commons.context.visit.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.types.OS;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.useragent.UserAgentInfo;
import com.odigeo.visitengineapi.v1.useragent.UserAgentInfoKey;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

@Singleton
public class OSMapper {

    private static final String WINDOWS = "WINDOWS";
    private static final String MAC = "MAC";
    private static final String POSIX = "POSIX";
    private static final String ANDROID = "ANDROID";
    private static final String IOS = "IOS";

    public OS map(VisitResponse visitResponse) {
        String os = Optional.ofNullable(visitResponse.getUserAgentInfo())
            .map(UserAgentInfo::getUserAgentInfoKey)
            .map(UserAgentInfoKey::getOsType)
            .orElse(StringUtils.EMPTY);

        switch (os) {
        case WINDOWS: return OS.WINDOWS;
        case MAC: return OS.MAC;
        case POSIX: return OS.POSIX;
        case ANDROID: return OS.ANDROID;
        case IOS: return OS.IOS;
        default: return OS.OTHER;
        }
    }

}
