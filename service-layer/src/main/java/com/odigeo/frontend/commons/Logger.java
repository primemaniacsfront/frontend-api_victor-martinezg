package com.odigeo.frontend.commons;

import com.google.inject.Singleton;

@Singleton
public class Logger {

    public void warning(Class<?> loggerClass, Object message, Throwable exception) {
        getLogger(loggerClass).warn(message, exception);
    }

    public void warning(Class<?> loggerClass, Object message) {
        getLogger(loggerClass).warn(message);
    }

    public void warning(Class<?> loggerClass, Throwable exception) {
        getLogger(loggerClass).warn(null, exception);
    }

    public void error(Class<?> loggerClass, Object message, Throwable exception) {
        getLogger(loggerClass).error(message, exception);
    }

    public void error(Class<?> loggerClass, Object message) {
        getLogger(loggerClass).error(message);
    }

    public void error(Class<?> loggerClass, Throwable exception) {
        getLogger(loggerClass).error(null, exception);
    }

    org.apache.log4j.Logger getLogger(Class<?> loggerClass) {
        return org.apache.log4j.Logger.getLogger(loggerClass);
    }
}
