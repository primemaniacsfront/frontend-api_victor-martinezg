package com.odigeo.frontend.commons;

import java.util.Arrays;

public enum SiteCookies {

    MKT_TRACKING("mktTrack"),
    SSO_TOKEN("sso_token"),
    TEST_TOKEN_SPACE("TestTokenSpace"),
    VISIT_INFORMATION("viI"),
    SSO_REMEMBER_ME("sso_remember_me"),

    //shoppinbasket v2 integration
    SB_JSESSIONID("SBJSESSIONID"),

    //ShoppingCart  integration
    DAPI_JSESSIONID("DAPI.JSESSIONID"),

    @Deprecated //onefront integration
    OF_JSESSIONID("OF1JSESSIONID"),

    DEBUG_CONFIGURATION("DEBUGCONFIGURATION");


    private final String cookieName;

    SiteCookies(String cookieName) {
        this.cookieName = cookieName;
    }

    public String value() {
        return cookieName;
    }

    public static SiteCookies getValueFromString(String name) {
        return Arrays.stream(SiteCookies.values()).filter(siteCookie -> siteCookie.value().equals(name)).findFirst().orElse(null);
    }

}
