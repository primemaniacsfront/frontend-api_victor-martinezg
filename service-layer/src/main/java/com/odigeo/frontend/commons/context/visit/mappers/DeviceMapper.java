package com.odigeo.frontend.commons.context.visit.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.visitengineapi.generic.interfaces.DeviceType;
import com.odigeo.visitengineapi.v1.interfaces.Interface;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import java.util.Optional;
import org.apache.commons.lang3.EnumUtils;

@Singleton
public class DeviceMapper {

    public Device map(VisitResponse visitResponse) {
        DeviceType deviceType = Optional.ofNullable(visitResponse.getClientInterface())
            .map(Interface::getDeviceType)
            .map(device -> EnumUtils.getEnum(DeviceType.class, device))
            .orElse(DeviceType.SERVER_TO_SERVER);

        switch (deviceType) {
        case DESKTOP: return Device.DESKTOP;
        case SMARTPHONE: return Device.MOBILE;
        case TABLET: return Device.TABLET;
        default: return Device.OTHER;
        }
    }

}
