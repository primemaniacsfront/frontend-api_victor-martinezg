package com.odigeo.frontend.commons.exception;

import org.apache.commons.lang3.StringUtils;

public enum ServiceName {

    FRONTEND_API,
    SEARCH_ENGINE,
    VISIT_ENGINE,
    ONEFRONT,
    DAPI,
    ITINERARY_API,
    GEO_SERVICE,
    MEMBERSHIP_OFFER_API,
    SSLP_POPULARITY_STATS,
    SEARCH_PRICE_API,
    TEST_ASSIGMENT,
    USER_API,
    INSURANCE_API,
    INSURANCE_PRODUCT_API,
    SHOPPING_BASKET_API,
    CRM_SUBSCRIPTION_API,
    COLLECTION_METHOD_API,
    PAYMENT_INSTRUMENT,
    TRIP_API,
    MEMBERSHIP_API,
    USER_PAYMENT_INTERACTION;

    public String getLabel() {
        return StringUtils.lowerCase(name());
    }

}
