package com.odigeo.frontend.commons.context.visit.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import com.odigeo.visitengineapi.generic.interfaces.Interface;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import java.util.Optional;
import org.apache.commons.lang3.EnumUtils;

@Singleton
public class WebInterfaceMapper {

    public WebInterface map(VisitResponse visitResponse) {
        Interface webInterface = Optional.ofNullable(visitResponse.getClientInterface())
            .map(com.odigeo.visitengineapi.v1.interfaces.Interface::getDescription)
            .map(description -> EnumUtils.getEnum(Interface.class, description))
            .orElse(Interface.UNKNOWN);

        switch (webInterface) {
        case ONE_FRONT_DESKTOP: return WebInterface.ONE_FRONT_DESKTOP;
        case ONE_FRONT_SMARTPHONE: return WebInterface.ONE_FRONT_SMARTPHONE;
        case ONE_FRONT_TABLET: return WebInterface.ONE_FRONT_TABLET;
        default: return WebInterface.OTHER;
        }
    }

}
