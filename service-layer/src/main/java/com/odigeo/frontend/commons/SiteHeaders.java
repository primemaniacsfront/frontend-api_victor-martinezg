package com.odigeo.frontend.commons;

import com.odigeo.commons.logs.HeaderFields;
import org.apache.http.HttpHeaders;

public enum SiteHeaders {

    TRACE_ID(new HeaderFields().getTraceHeader()),

    REFERER(HttpHeaders.REFERER),
    USER_AGENT(HttpHeaders.USER_AGENT),

    VISIT_INFORMATION("X-visit"),

    //shoppingbasket integration
    SB_JSESSIONID("X-SBJSESSIONID"),

    @Deprecated //onefront integration
    OF_JSESSIONID("X-OF1JSESSIONID");

    private final String headerName;

    SiteHeaders(String headerName) {
        this.headerName = headerName;
    }

    public String value() {
        return headerName;
    }

}
