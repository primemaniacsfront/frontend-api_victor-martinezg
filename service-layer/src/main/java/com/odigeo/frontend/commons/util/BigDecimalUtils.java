package com.odigeo.frontend.commons.util;

import com.google.inject.Singleton;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Singleton
public class BigDecimalUtils {

    public BigDecimal differenceInPercentage(BigDecimal first, BigDecimal second) {
        return first != null && second != null && !(first.equals(BigDecimal.ZERO) && second.equals(BigDecimal.ZERO))
            ? first.subtract(second).divide(first.max(second), 4, RoundingMode.HALF_DOWN).scaleByPowerOfTen(2).abs()
            : BigDecimal.ZERO;
    }

}
