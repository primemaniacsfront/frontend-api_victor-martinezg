package com.odigeo.frontend.commons.exception;

public enum ErrorCodes {

    INTERNAL_GENERIC("INT-01", "Internal error"),

    VALIDATION("ERR-01", "Validation error on request parameters"),
    COOKIE_HEADER_INVALID("ERR-02", "Validation error on request cookies/header"),

    RSA_ENCRYPTION("RSA-01", "RSA Encryption error");

    private final String code;
    private final String description;

    ErrorCodes(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
