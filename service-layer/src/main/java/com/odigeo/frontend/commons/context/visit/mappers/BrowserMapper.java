package com.odigeo.frontend.commons.context.visit.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.types.Browser;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.useragent.UserAgentInfo;
import com.odigeo.visitengineapi.v1.useragent.UserAgentInfoKey;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

@Singleton
public class BrowserMapper {

    private static final String CHROME = "CHROME";
    private static final String MOZILLA = "MOZILLA";
    private static final String INTERNET_EXPLORER = "INTERNET_EXPLORER";
    private static final String EDGE = "EDGE";
    private static final String SAFARI = "SAFARI";

    public Browser map(VisitResponse visitResponse) {
        String browser = Optional.ofNullable(visitResponse.getUserAgentInfo())
            .map(UserAgentInfo::getUserAgentInfoKey)
            .map(UserAgentInfoKey::getUserAgentFamily)
            .orElse(StringUtils.EMPTY);

        switch (browser) {
        case CHROME: return Browser.CHROME;
        case MOZILLA: return Browser.FIREFOX;
        case INTERNET_EXPLORER: return Browser.IE;
        case EDGE: return Browser.EDGE;
        case SAFARI: return Browser.SAFARI;
        default: return Browser.OTHER;
        }
    }

}
