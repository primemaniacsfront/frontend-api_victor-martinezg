package com.odigeo.frontend.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapImageFormat;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapStaticConfiguration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapType;
import com.google.inject.Singleton;
import com.google.maps.GeoApiContext;
import com.google.maps.StaticMapsRequest;
import com.google.maps.model.LatLng;
import com.google.maps.model.Size;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapImageFormat.JPG_BASELINE;
import static com.google.maps.StaticMapsRequest.ImageFormat.jpgBaseline;

@Singleton
public class GoogleMapsRequestMapper {

    static final List<Integer> SCALE_VALIDATION = Arrays.asList(1, 2, 4);
    static final String CENTER_SEPARATOR = ",";

    public StaticMapsRequest mapGoogleMapsStaticRequest(MapStaticConfiguration mapStaticConfiguration, double longitude,
                                                        double latitude, GeoApiContext context) {
        Optional<MapStaticConfiguration> configuration = Optional.ofNullable(mapStaticConfiguration);
        StaticMapsRequest request = new StaticMapsRequest(context);
        request.center(latitude + CENTER_SEPARATOR + longitude);
        configuration.map(MapStaticConfiguration::getSize).ifPresent(size -> request.size(new Size(size.getWidth(), size.getHeight())));
        configuration.map(MapStaticConfiguration::getZoom).ifPresent(zoom -> request.zoom(zoom));
        configuration.map(MapStaticConfiguration::getScale).ifPresent(scale -> request.scale(getScale(scale)));
        configuration.map(MapStaticConfiguration::getFormat).ifPresent(format -> request.format(mapImageFormat(format)));
        configuration.map(MapStaticConfiguration::getMapType).ifPresent(mapType -> mapMapType(mapType, request));
        configuration.map(MapStaticConfiguration::getAdditionalParams)
            .map(Collection::stream)
            .orElseGet(Stream::empty)
            .forEach(mapStyle -> request.custom(mapStyle.getProperty(), mapStyle.getValue()));
        request.markers(mapMarker(latitude, longitude, mapStaticConfiguration.getMarkerUrl()));
        return request;
    }

    private void mapMapType(MapType mapType, StaticMapsRequest request) {
        request.maptype(EnumUtils.getEnum(StaticMapsRequest.StaticMapType.class, mapType.toString().toLowerCase(Locale.ROOT)));
    }

    private StaticMapsRequest.ImageFormat mapImageFormat(MapImageFormat googleMapsFormat) {
        StaticMapsRequest.ImageFormat imageFormat;
        if (googleMapsFormat.equals(JPG_BASELINE)) {
            imageFormat = jpgBaseline;
        } else {
            imageFormat = EnumUtils.getEnum(StaticMapsRequest.ImageFormat.class, googleMapsFormat.toString().toLowerCase(Locale.ROOT));
        }
        return imageFormat;
    }

    private int getScale(int scale) {
        if (SCALE_VALIDATION.contains(scale)) {
            return scale;
        } else {
            String errorMessage = "Wrong staticMapConfiguration.scale. Allowed values :" + SCALE_VALIDATION.stream()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
            throw new ServiceException(errorMessage, Response.Status.BAD_REQUEST, ServiceName.FRONTEND_API);
        }
    }

    private StaticMapsRequest.Markers mapMarker(double latitude, double longitude, String markerImageUIR) {
        StaticMapsRequest.Markers marker = new StaticMapsRequest.Markers();
        LatLng markerLocation = new LatLng(latitude, longitude);
        if (StringUtils.isNotEmpty(markerImageUIR)) {
            marker.customIcon(markerImageUIR, StaticMapsRequest.Markers.CustomIconAnchor.center);
        }
        marker.addLocation(markerLocation);
        return marker;
    }
}
