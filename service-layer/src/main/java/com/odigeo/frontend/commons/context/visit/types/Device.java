package com.odigeo.frontend.commons.context.visit.types;

public enum Device {
    DESKTOP,
    MOBILE,
    TABLET,
    OTHER
}
