package com.odigeo.frontend.commons.util;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.RsaException;
import com.odigeo.frontend.services.configurations.rsa.RsaKeyConfig;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.security.GeneralSecurityException;

@Singleton
public class EncryptUtils {
    @Inject
    private RsaKeyConfig rsaKeyConfig;

    public byte[] encrypt(byte[] message) throws RsaException {
        try {
            Cipher cipher = Cipher.getInstance(RsaKeyConfig.RSA);
            cipher.init(Cipher.ENCRYPT_MODE, rsaKeyConfig.getPublicKey());
            return cipher.doFinal(message);
        } catch (GeneralSecurityException exception) {
            throw new RsaException(ErrorCodes.RSA_ENCRYPTION.getDescription(), exception);
        }
    }

    public String encryptBase64URLSafe(byte[] message) throws RsaException {
        return Base64.encodeBase64URLSafeString(encrypt(message));
    }
}
