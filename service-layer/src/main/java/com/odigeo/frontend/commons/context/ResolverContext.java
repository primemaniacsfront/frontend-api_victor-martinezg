package com.odigeo.frontend.commons.context;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;

import javax.ws.rs.core.Response.Status;

@Deprecated //use DataFetchingEnvironment::getGraphQLContext instead of getContext
public class ResolverContext {

    private VisitInformation visitInformation;
    private DebugInfo debugInfo;

    private RequestInfo requestInfo;
    private ResponseInfo responseInfo;

    public VisitInformation getVisitInformation() {
        if (visitInformation == null) {
            throw new ServiceException("invalid visit", Status.BAD_REQUEST, ServiceName.VISIT_ENGINE);
        }

        return visitInformation;
    }

    void setVisitInformation(VisitInformation visitInformation) {
        this.visitInformation = visitInformation;
    }

    public DebugInfo getDebugInfo() {
        return debugInfo;
    }

    void setDebugInfo(DebugInfo debugInfo) {
        this.debugInfo = debugInfo;
    }

    public RequestInfo getRequestInfo() {
        return requestInfo;
    }

    void setRequestInfo(RequestInfo requestInfo) {
        this.requestInfo = requestInfo;
    }

    public ResponseInfo getResponseInfo() {
        return responseInfo;
    }

    void setResponseInfo(ResponseInfo responseInfo) {
        this.responseInfo = responseInfo;
    }

}
