package com.odigeo.frontend.commons.util;


import com.google.inject.Singleton;

import java.util.Locale;
import java.util.Optional;
import java.util.regex.Pattern;

@Singleton
public class DocumentValidatorUtils {
    protected static final String SPAIN = "ES";
    private static final Pattern CIF_PATTERN = Pattern.compile("^[a-zA-Z]{1}\\d{7}[a-jA-J0-9]{1}$");
    private static final String CIF_LETTERS = "KPQRSNW";
    private static final String[] CIF_CONTROL_DIGIT = {"J", "A", "B", "C", "D", "E", "F", "G", "H", "I"};

    public boolean isValidCIF(String cifId, String countryCode) {
        return isNecessaryValidateId(countryCode) ? isValidCif(cifId) : Boolean.TRUE;
    }

    private boolean isNecessaryValidateId(String countryCode) {
        return Optional.ofNullable(countryCode)
            .map(country -> country.toUpperCase(Locale.ROOT))
            .map(value -> value.equals(SPAIN))
            .orElse(false);
    }

    private boolean isValidCif(String cifId) {
        return Optional.ofNullable(cifId)
            .filter(value -> CIF_PATTERN.matcher(value).matches())
            .map(cif -> cif.toUpperCase(Locale.ROOT))
            .map(this::cifValidator)
            .orElse(false);
    }

    private boolean cifValidator(String cifId) {
        String cifWithoutControlDigit = cifId.substring(0, 8);
        cifWithoutControlDigit = calculateCif(cifWithoutControlDigit);
        return cifId.equals(cifWithoutControlDigit);
    }

    private String calculateCif(String cifWithoutControlDigit) {
        return cifWithoutControlDigit + calculateCIFControlDigit(cifWithoutControlDigit);
    }

    private String calculateCIFControlDigit(String cifWithoutControlDigit) {
        String cifNumber = cifWithoutControlDigit.substring(1, 8);
        String cifFirstLetter = cifWithoutControlDigit.substring(0, 1);
        int cifControlDigitPos = getSumEvenPositions(cifNumber) + getSumOddPositions(cifNumber);
        cifControlDigitPos = 10 - (cifControlDigitPos % 10);
        cifControlDigitPos = switchValueToGetControlDigit(cifControlDigitPos);
        return getCIFControlDigit(cifFirstLetter, cifControlDigitPos);
    }

    private String getCIFControlDigit(String cifFirstLetter, int cifControlDigitPosition) {
        return CIF_LETTERS.contains(cifFirstLetter) ? CIF_CONTROL_DIGIT[cifControlDigitPosition] : String.valueOf(cifControlDigitPosition);
    }

    private int switchValueToGetControlDigit(int result) {
        return result == 10 ? 0 : result;
    }

    private int getSumEvenPositions(String cifNumber) {
        int totalEven = 0;
        for (int i = 1; i < cifNumber.length(); i += 2) {
            int aux = Integer.parseInt(String.valueOf(cifNumber.charAt(i)));
            totalEven += aux;
        }
        return totalEven;
    }

    private int getSumOddPositions(String cifNumber) {
        int totalOdd = 0;
        for (int i = 0; i < cifNumber.length(); i += 2) {
            totalOdd += oddPosition(String.valueOf(cifNumber.charAt(i)));
        }
        return totalOdd;
    }

    private int oddPosition(String oddNumberStr) {
        int oddNumber = Integer.parseInt(oddNumberStr);
        oddNumber = oddNumber * 2;
        oddNumber = (oddNumber / 10) + (oddNumber % 10);

        return oddNumber;
    }
}
