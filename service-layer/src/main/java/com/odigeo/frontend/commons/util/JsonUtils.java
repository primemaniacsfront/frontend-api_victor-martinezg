package com.odigeo.frontend.commons.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.inject.Singleton;
import graphql.schema.DataFetchingEnvironment;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.MapUtils;

@Singleton
public class JsonUtils {

    private static final Gson GSON = new Gson();

    public String toJson(Object source) {
        return GSON.toJson(source);
    }

    public <T> T fromJson(String json, Class<T> dtoClass) {
        return GSON.fromJson(json, dtoClass);
    }

    public <T> T fromDataFetching(DataFetchingEnvironment env) {
        return firstParam(env);
    }

    public <T> T fromDataFetching(DataFetchingEnvironment env, String paramName) {
        return env.getArgument(paramName);
    }

    public <T> T fromDataFetching(DataFetchingEnvironment env, Class<T> clazz) {
        JsonElement jsonElement = GSON.toJsonTree(firstParam(env));
        return GSON.fromJson(jsonElement, clazz);
    }

    public <T> T fromDataFetching(DataFetchingEnvironment env, Class<T> clazz, String paramName) {
        JsonElement jsonElement = GSON.toJsonTree(env.getArgument(paramName));
        return GSON.fromJson(jsonElement, clazz);
    }

    public <T> Optional<T> getParameterFromLocalContext(DataFetchingEnvironment env, Class<T> clazz, String paramName) {
        Map<String, Object> localContext = env.getLocalContext();
        return Optional.ofNullable(localContext)
                .map(lc -> lc.get(paramName))
                .map(GSON::toJsonTree)
                .map(jsonElement ->  GSON.fromJson(jsonElement, clazz));
    }

    private <T> T firstParam(DataFetchingEnvironment env) {
        return Optional.ofNullable(env.getArguments())
            .filter(MapUtils::isNotEmpty)
            .map(Map::values)
            .map(values -> (T) IterableUtils.first(values))
            .orElse(null);
    }

}
