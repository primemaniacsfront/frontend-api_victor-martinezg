package com.odigeo.frontend.commons.context.visit.types;

public enum Brand {
    ED,
    GV,
    OP,
    TL,
    UNKNOWN
}
