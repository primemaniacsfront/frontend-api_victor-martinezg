package com.odigeo.frontend.commons.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collector;

public class BigDecimalSummaryStatistics implements Consumer<BigDecimal> {

    private long count;
    private BigDecimal sum = BigDecimal.ZERO;
    private BigDecimal min = BigDecimal.valueOf(Double.MAX_VALUE);
    private BigDecimal max = BigDecimal.valueOf(Double.MAX_VALUE).negate();


    public static Collector<BigDecimal, ?, BigDecimalSummaryStatistics> statistics() {
        return Collector.of(BigDecimalSummaryStatistics::new,
                BigDecimalSummaryStatistics::accept, BigDecimalSummaryStatistics::combine);
    }

    @Override
    public void accept(BigDecimal value) {
        Objects.requireNonNull(value);
        ++count;
        sum = sum.add(value);
        min = min.min(value);
        max = max.max(value);
    }

    public BigDecimalSummaryStatistics combine(BigDecimalSummaryStatistics other) {
        count += other.getCount();
        sum = sum.add(other.getSum());
        min = min.min(other.getMin());
        max = max.max(other.getMax());
        return this;
    }

    public final long getCount() {
        return count;
    }

    public final BigDecimal getSum() {
        return sum;
    }

    public final BigDecimal getMin() {
        return min;
    }

    public final BigDecimal getMax() {
        return max;
    }

    public final BigDecimal getAverage() {
        return getCount() > 0 ? getSum().divide(BigDecimal.valueOf(getCount()), 2, RoundingMode.FLOOR) : BigDecimal.ZERO;
    }
}
