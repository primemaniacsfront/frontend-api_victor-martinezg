package com.odigeo.frontend.commons.util;


import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoServiceException;
import com.odigeo.geoapi.v4.InvalidParametersException;
import com.odigeo.geoapi.v4.responses.AbstractGeoNode;

import java.time.ZoneId;
import java.util.Optional;
import java.util.TimeZone;

@Singleton
public class GeoUtils {

    @Inject
    private GeoService geoService;

    @Inject
    private Logger logger;

    public Double calculateDistance(GeoCoordinatesDTO c1, GeoCoordinatesDTO c2) {
        if (c1 == null || c2 == null) {
            return null;
        }
        double earthRadius = 6371; // in kilometers

        double destinationLatitude = c2.getLatitude().doubleValue();
        double sourceLatitude = c1.getLatitude().doubleValue();
        double destinationLongitude = c2.getLongitude().doubleValue();
        double sourceLongitude = c1.getLongitude().doubleValue();

        double deltaLatitude = Math.toRadians(destinationLatitude - sourceLatitude);
        double deltaLongitude = Math.toRadians(destinationLongitude - sourceLongitude);

        double latitudeRad1 = Math.toRadians(sourceLatitude);
        double latitudeRad2 = Math.toRadians(destinationLatitude);

        double a = Math.sin(deltaLatitude / 2) * Math.sin(deltaLatitude / 2) + Math.sin(deltaLongitude / 2)
                * Math.sin(deltaLongitude / 2) * Math.cos(latitudeRad1) * Math.cos(latitudeRad2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return earthRadius * c;
    }

    public ZoneId getZoneIdFromAirportIata(String airportIata) {
        try {
            return Optional.ofNullable(geoService.getAirportByIataCode(airportIata))
                    .map(com.odigeo.geoapi.v4.responses.Airport::getCity)
                    .map(AbstractGeoNode::getTimeZone)
                    .map(TimeZone::toZoneId)
                    .orElse(ZoneId.of("UTC"));
        } catch (GeoNodeNotFoundException | GeoServiceException | InvalidParametersException e) {
            logger.warning(this.getClass(), String.format("Airport for iata: %s not found", airportIata));
        }

        return ZoneId.of("UTC");
    }
}

