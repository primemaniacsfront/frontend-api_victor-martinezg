package com.odigeo.frontend.commons.context.visit.types;

public enum OS {
    WINDOWS,
    MAC,
    POSIX,
    ANDROID,
    IOS,
    OTHER
}
