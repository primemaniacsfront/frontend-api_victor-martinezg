package com.odigeo.frontend.commons.exception;

public class RsaException extends Exception {

    public RsaException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
