package com.odigeo.frontend.commons.context.visit.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.website.Website;
import java.util.Optional;
import org.apache.commons.lang3.EnumUtils;

@Singleton
public class BrandMapper {

    public Brand map(VisitResponse visitResponse) {
        return Optional.ofNullable(visitResponse.getWebsite())
            .map(Website::getBrand)
            .map(brand -> EnumUtils.getEnum(Brand.class, brand))
            .orElse(Brand.UNKNOWN);
    }

}
