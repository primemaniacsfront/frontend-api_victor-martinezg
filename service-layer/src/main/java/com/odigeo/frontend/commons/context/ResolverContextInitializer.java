package com.odigeo.frontend.commons.context;

import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.DebugModeConfiguration;
import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.DebugModeConfigurationSerializer;
import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.exception.SerializeException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.visit.VisitDeserializer;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.monitoring.MetricsHandler;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class ResolverContextInitializer {

    private static final Logger logger = LoggerFactory.getLogger(ResolverContextInitializer.class);

    @Inject
    private VisitDeserializer visitDeserializer;
    @Inject
    private DebugModeConfigurationSerializer debugModeConfigurationSerializer;

    public ResolverContext createContext(HttpServletRequest servletRequest, DebugInfo debugInfo) {
        ResolverContext context = new ResolverContext();

        RequestInfo requestInfo = createRequestInfo(servletRequest);
        context.setRequestInfo(requestInfo);

        VisitInformation visitInformation = getVisitInformation(requestInfo);
        context.setVisitInformation(visitInformation);

        context.setResponseInfo(new ResponseInfo());
        context.setDebugInfo(debugInfo);

        return context;
    }

    private VisitInformation getVisitInformation(RequestInfo requestInfo) {
        String visit = requestInfo.getHeaderOrCookie(SiteHeaders.VISIT_INFORMATION, SiteCookies.VISIT_INFORMATION);
        return visitDeserializer.deserialize(visit);
    }

    private DebugModeConfiguration getDebugModeConfiguration(RequestInfo requestInfo) {
        DebugModeConfiguration debugModeConfiguration = null;
        try {
            String debugInfo = requestInfo.getCookieValue(SiteCookies.DEBUG_CONFIGURATION);
            debugModeConfiguration = debugModeConfigurationSerializer.deserialize(debugInfo);
        } catch (SerializeException e) {
            logger.error("Couldn't retrieve debug configuration", e);
        }

        return debugModeConfiguration;
    }

    public Map<Class<?>, Object> createContextMap(HttpServletRequest servletRequest) {
        RequestInfo requestInfo = createRequestInfo(servletRequest);
        VisitInformation visitInformation = getVisitInformation(requestInfo);
        Map<Class<?>, Object> contextMap = Stream.of(
                new SimpleEntry<>(MetricsHandler.class, new MetricsHandler()),
                new SimpleEntry<>(VisitInformation.class, visitInformation),
                new SimpleEntry<>(RequestInfo.class, requestInfo)
        ).filter(se -> se.getValue() != null).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));
        setDebugConfigurationToContextMap(requestInfo, visitInformation, contextMap);
        return contextMap;
    }

    private void setDebugConfigurationToContextMap(RequestInfo requestInfo, VisitInformation visitInformation, Map<Class<?>, Object> contextMap) {
        DebugModeConfiguration debugModeConfiguration = getDebugModeConfiguration(requestInfo);
        if (debugModeConfiguration != null && Objects.equals(visitInformation.getVisitId(), debugModeConfiguration.getVisitId())) {
            contextMap.put(DebugModeConfiguration.class, debugModeConfiguration);
        }
    }

    private RequestInfo createRequestInfo(HttpServletRequest servletRequest) {
        RequestInfo requestInfo = new RequestInfo();

        requestInfo.setRequestUrl(servletRequest.getRequestURL().toString());
        requestInfo.setServerName(servletRequest.getServerName());
        requestInfo.setContextPath(servletRequest.getContextPath());

        initializeCookies(servletRequest, requestInfo);
        initializeHeaders(servletRequest, requestInfo);

        return requestInfo;
    }

    private void initializeCookies(HttpServletRequest servletRequest, RequestInfo requestInfo) {
        Cookie[] cookies = ArrayUtils.nullToEmpty(servletRequest.getCookies(), Cookie[].class);
        Stream.of(cookies).forEach(requestInfo::addCookie);
    }

    private void initializeHeaders(HttpServletRequest servletRequest, RequestInfo requestInfo) {
        SiteHeaders[] siteHeaders = ArrayUtils.nullToEmpty(SiteHeaders.values(), SiteHeaders[].class);

        Stream.of(siteHeaders).forEach(siteHeader -> {
            String headerName = siteHeader.value();
            String headerValue = servletRequest.getHeader(headerName);

            requestInfo.addHeader(siteHeader, headerValue);
        });
    }

}
