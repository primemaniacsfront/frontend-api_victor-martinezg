package com.odigeo.frontend.commons.util;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapStaticConfiguration;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.maps.GeoApiContext;
import com.google.maps.StaticMapsRequest;
import com.google.maps.errors.ApiException;
import com.google.maps.errors.NotFoundException;
import com.odigeo.frontend.commons.mappers.GoogleMapsRequestMapper;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Optional;

@Singleton
public class GoogleMapsUtils {

    @Inject
    private GoogleMapsRequestMapper googleMapsRequestMapper;

    public GeoApiContext createContext(String apiKey, String secretKey, String clientId) throws IllegalArgumentException {
        GeoApiContext context = null;
        if (StringUtils.isNotEmpty(apiKey) && StringUtils.isNotEmpty(secretKey)) {
            context = new GeoApiContext.Builder()
                .apiKey(apiKey)
                .enterpriseCredentials(Optional.ofNullable(clientId).filter(StringUtils::isNotEmpty).orElse(null), secretKey)
                .build();
        }
        return context;
    }

    public byte[] getStaticMap(MapStaticConfiguration request, double longitude, double latitude, GeoApiContext context) throws IOException, InterruptedException, ApiException {
        StaticMapsRequest mapRequest = googleMapsRequestMapper.mapGoogleMapsStaticRequest(request, longitude, latitude, context);
        return requestImage(mapRequest);
    }

    private byte[] requestImage(StaticMapsRequest mapRequest) throws IOException, InterruptedException, ApiException {
        return Optional.ofNullable(mapRequest.await())
            .filter(imageResult -> ArrayUtils.isNotEmpty(imageResult.imageData))
            .map(imageResult -> imageResult.imageData)
            .orElseThrow(() -> new NotFoundException("ImageData not found"));
    }
}
