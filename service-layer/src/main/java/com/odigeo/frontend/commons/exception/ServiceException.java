package com.odigeo.frontend.commons.exception;

import javax.ws.rs.core.Response.Status;

public class ServiceException extends RuntimeException {

    private static final Status DEFAULT_STATUS = Status.INTERNAL_SERVER_ERROR;
    private static final ErrorCodes DEFAULT_ERROR_CODE = ErrorCodes.INTERNAL_GENERIC;
    private static final ServiceName DEFAULT_SERVICE_NAME = ServiceName.FRONTEND_API;

    private final Status status;
    private final ErrorCodes errorCode;
    private final ServiceName serviceName;

    public ServiceException(String message, Throwable cause, Status status, ErrorCodes errorCode) {
        super(message, cause);
        this.status = status;
        this.errorCode = errorCode;
        this.serviceName = DEFAULT_SERVICE_NAME;
    }

    public ServiceException(String message, Throwable cause, Status status, ErrorCodes errorCode, ServiceName serviceName) {
        super(message, cause);
        this.status = status;
        this.errorCode = errorCode;
        this.serviceName = serviceName;
    }

    public ServiceException(String message, ServiceName serviceName) {
        super(message);
        this.status = DEFAULT_STATUS;
        this.errorCode = DEFAULT_ERROR_CODE;
        this.serviceName = serviceName;
    }

    public ServiceException(Throwable cause, ServiceName serviceName) {
        super(null, cause);
        this.status = DEFAULT_STATUS;
        this.errorCode = DEFAULT_ERROR_CODE;
        this.serviceName = serviceName;
    }

    public ServiceException(String message, Throwable cause, ServiceName serviceName) {
        super(message, cause);
        this.status = DEFAULT_STATUS;
        this.errorCode = DEFAULT_ERROR_CODE;
        this.serviceName = serviceName;
    }

    public ServiceException(String message, Status status, ServiceName serviceName) {
        super(message);
        this.status = status;
        this.errorCode = DEFAULT_ERROR_CODE;
        this.serviceName = serviceName;
    }

    public ServiceException(String message, Throwable cause, Status status, ServiceName serviceName) {
        super(message, cause);
        this.status = status;
        this.errorCode = DEFAULT_ERROR_CODE;
        this.serviceName = serviceName;
    }

    public ServiceException(String message, Status status, ErrorCodes errorCode, ServiceName serviceName) {
        super(message);
        this.status = status;
        this.errorCode = errorCode;
        this.serviceName = serviceName;
    }

    public Status getStatus() {
        return status;
    }

    public ErrorCodes getErrorCode() {
        return errorCode;
    }

    public ServiceName getServiceName() {
        return serviceName;
    }
}
