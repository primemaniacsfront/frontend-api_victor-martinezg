package com.odigeo.frontend.commons.util;

import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

@Singleton
public class DateUtils {

    public ZonedDateTime convertFromCalendar(Calendar calendar) {
        return ZonedDateTime.ofInstant(
                calendar.getTime().toInstant(),
                calendar.getTimeZone().toZoneId());
    }

    public Calendar convertFromIsoToCalendar(String date) {
        Calendar calendar = Calendar.getInstance();
        LocalDate localDate = convertFromIsoToLocalDate(date);
        calendar.setTime(Date.from(localDate.atStartOfDay().toInstant(ZoneOffset.UTC)));

        return calendar;
    }

    public LocalDate convertFromIsoToLocalDate(String date) {
        return DateTimeFormatter.ISO_LOCAL_DATE.parse(date, LocalDate::from);
    }

    public boolean isBeforeOrEquals(LocalDate date, LocalDate comparedDate) {
        return date.isBefore(comparedDate) || date.isEqual(comparedDate);
    }

    public Integer daysBetweenIsoDates(String startDate, String endDate) {
        return (int) ChronoUnit.DAYS.between(convertFromIsoToLocalDate(startDate), convertFromIsoToLocalDate(endDate));

    }

    public String formatToOffsetDateTime(String date, String time, ZoneId zoneId) {
        if (StringUtils.isEmpty(date) || StringUtils.isEmpty(time)) {
            return null;
        }
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-ddHH:mm");
        ZonedDateTime datetime = ZonedDateTime.of(
                LocalDateTime.parse(date + time, inputFormatter),
                zoneId
        );

        return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(datetime);
    }
}
