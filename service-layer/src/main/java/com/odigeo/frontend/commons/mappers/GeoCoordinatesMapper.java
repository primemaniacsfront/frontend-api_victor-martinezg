package com.odigeo.frontend.commons.mappers;

import com.google.inject.Singleton;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.geoapi.v4.responses.Coordinates;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.searchengine.v2.responses.gis.GeoCoordinates;

import java.math.BigDecimal;

@Singleton
public class GeoCoordinatesMapper {

    public GeoCoordinatesDTO map(GeoCoordinates geoCoordinates) {
        GeoCoordinatesDTO dto = new GeoCoordinatesDTO();
        dto.setLatitude(geoCoordinates.getLatitude());
        dto.setLongitude(geoCoordinates.getLongitude());
        return dto;
    }

    public GeoCoordinatesDTO map(Coordinates coordinates) {
        GeoCoordinatesDTO dto = new GeoCoordinatesDTO();
        dto.setLatitude(coordinates.getLatitude());
        dto.setLongitude(coordinates.getLongitude());
        return dto;
    }

    public com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeoCoordinates mapToContractGeoCoordinates(Coordinates coordinates) {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeoCoordinates geoCoordinates = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeoCoordinates();
        geoCoordinates.setLatitude(coordinates.getLatitude());
        geoCoordinates.setLongitude(coordinates.getLongitude());
        return geoCoordinates;
    }

    public GeoCoordinatesDTO mapHcs(HotelSummary hotelSummary) {
        GeoCoordinatesDTO dto = null;
        if (hotelSummary != null && hotelSummary.getLatitude() != null) {
            dto = new GeoCoordinatesDTO(new BigDecimal(hotelSummary.getLatitude()), new BigDecimal(hotelSummary.getLongitude()));
        }
        return dto;
    }

}
