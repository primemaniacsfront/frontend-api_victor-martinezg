package com.odigeo.frontend.commons.context.visit.types;

public enum Browser {
    CHROME,
    FIREFOX,
    IE,
    EDGE,
    SAFARI,
    OTHER
}
