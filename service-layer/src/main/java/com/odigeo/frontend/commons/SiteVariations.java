package com.odigeo.frontend.commons;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.services.TestTokenService;

import java.util.Arrays;
import java.util.Optional;

import static com.odigeo.frontend.services.TestTokenService.PARTITION_B;
import static com.odigeo.frontend.services.TestTokenService.PARTITION_C;

@Singleton
public class SiteVariations {

    // FF
    @VisibleForTesting
    static final String FARE_FAMILY_DESKTOP_ENABLED = "FF_FF178";
    @VisibleForTesting
    static final String FARE_FAMILY_MOBILE_ENABLED = "FF_FF27";

    // UNLOCKED
    @VisibleForTesting
    static final String PRIME_ENABLED = "ULC_UNL293";

    // OPRIME
    @VisibleForTesting
    static final String MEMBERSHIP_DISPLAYED_META = "OPR_OPRIME1345";
    @VisibleForTesting
    static final String MEMBERSHIP_SUBSCRIPTION_TIERS = "PRIME_OPRIME1979";
    @VisibleForTesting
    public static final String MEMBERSHIP_RECAPTURE_M = "PRIME_OPRIME1755_M";

    // DPTOUCHPOINTS
    @VisibleForTesting
    static final String DPT_IP_TOUCHPOINT_ENABLED = "DPT_DPT643";

    // BOOKINGS
    @VisibleForTesting
    static final String RESIDENT_DISCOUNT_IN_PAX = "FBO_BOOK3259";

    // COUNTER TOPS
    @VisibleForTesting
    static final String IS_PRIME_USER_FROM_COOKIE = "CON_COUNTER_409";

    // SMART SELECTION
    @VisibleForTesting
    static final String IS_PRIME_DAY_CAMPAIGN_ENABLED = "SSE_FC3610";
    @VisibleForTesting
    static final String IS_AIRLINE_CAMPAIGN_ENABLED = "SSE_FC3563";
    @VisibleForTesting
    static final String AIRLINE_STEERING_ALIAS = "SSE_FC3683";
    @VisibleForTesting
    static final String OF_COMMONS_REST_SERVICES_ALIAS = "SSE_FC3695";


    private static final Integer[] PARTITIONS_B_C = {PARTITION_B, PARTITION_C};
    public static final int PARTITION_A = 1;

    @Inject
    private TestTokenService testTokenService;

    public boolean isFareFamilyTestEnabled(VisitInformation visit) {
        Device device = visit.getDevice();

        return device == Device.DESKTOP && isEnabled(FARE_FAMILY_DESKTOP_ENABLED, visit, PARTITIONS_B_C)
                || device == Device.MOBILE && isEnabled(FARE_FAMILY_MOBILE_ENABLED, visit, PARTITIONS_B_C);
    }

    public boolean isPrimeTestEnabled(VisitInformation visit) {
        return isEnabled(PRIME_ENABLED, visit);
    }

    public boolean isMembershipDisplayedMeta(VisitInformation visit) {
        return isEnabled(MEMBERSHIP_DISPLAYED_META, visit);
    }

    public int getMembershipSubscriptionTiers(VisitInformation visit) {
        return getActivePartition(MEMBERSHIP_SUBSCRIPTION_TIERS, visit);
    }

    public boolean isMembershipRecaptureM(VisitInformation visit) {
        return isEnabled(MEMBERSHIP_RECAPTURE_M, visit);
    }

    public boolean isDptIpTouchPointEnabled(VisitInformation visit) {
        return isEnabled(DPT_IP_TOUCHPOINT_ENABLED, visit);
    }

    public boolean isResidentDiscountInPax(VisitInformation visit) {
        return isEnabled(RESIDENT_DISCOUNT_IN_PAX, visit);
    }

    public boolean isPrimeUserFromCookieEnabled(VisitInformation visit) {
        return isEnabled(IS_PRIME_USER_FROM_COOKIE, visit);
    }

    public boolean isPrimeDayCampaignEnabled(VisitInformation visit) {
        return isEnabled(IS_PRIME_DAY_CAMPAIGN_ENABLED, visit, PARTITIONS_B_C);
    }

    public boolean isAirlineCampaignEnabled(VisitInformation visit) {
        return isEnabled(IS_AIRLINE_CAMPAIGN_ENABLED, visit);
    }

    public Integer getAirlineSteeringPartition(VisitInformation visitInformation) {
        return testTokenService.find(AIRLINE_STEERING_ALIAS, visitInformation);
    }

    public boolean isOFCommonsRestServiceEnabled(VisitInformation visit) {
        return isEnabled(OF_COMMONS_REST_SERVICES_ALIAS, visit);
    }

    private boolean isEnabled(String alias, VisitInformation visit) {
        return isEnabled(alias, visit, PARTITION_B);
    }

    private int getActivePartition(String alias, VisitInformation visit) {
        return Optional.ofNullable(testTokenService.find(alias, visit)).orElse(PARTITION_A);
    }

    private boolean isEnabled(String dimension, VisitInformation visit, Integer... partitions) {
        Integer partition = getActivePartition(dimension, visit);
        return Arrays.asList(partitions).contains(partition);
    }
}
