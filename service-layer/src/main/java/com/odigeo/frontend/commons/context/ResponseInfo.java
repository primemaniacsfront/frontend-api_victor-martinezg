package com.odigeo.frontend.commons.context;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;

import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;
import javax.servlet.http.Cookie;

public class ResponseInfo {

    protected static final String DEFAULT_COOKIE_PATH = "/";
    protected static final int DEFAULT_COOKIE_EXPIRATION = 60 * 60 * 24 * 365 * 10;

    private final Map<SiteCookies, Cookie> cookies = new EnumMap<>(SiteCookies.class);
    private final Map<SiteHeaders, String> headers = new EnumMap<>(SiteHeaders.class);

    public Collection<Cookie> getCookies() {
        return cookies.values();
    }

    public Cookie getCookie(SiteCookies siteCookies) {
        return cookies.get(siteCookies);
    }

    public void addCookie(SiteCookies siteCookie, String value) {
        this.addCookie(siteCookie, value, DEFAULT_COOKIE_EXPIRATION);
    }

    public void addCookie(SiteCookies siteCookie, String value, int maxExpirationTime) {
        Cookie cookie = new Cookie(siteCookie.value(), value);
        cookie.setPath(DEFAULT_COOKIE_PATH);
        cookie.setMaxAge(maxExpirationTime);
        cookie.setHttpOnly(true);
        cookie.setSecure(true);

        cookies.put(siteCookie, cookie);
    }

    public Map<SiteHeaders, String> getHeaders() {
        return headers;
    }

    public void putHeader(SiteHeaders siteHeader, String value) {
        headers.put(siteHeader, value);
    }

}
