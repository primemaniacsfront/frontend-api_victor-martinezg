package com.odigeo.frontend.commons.util;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.exception.ServiceException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Singleton
public class AsyncUtils {

    private static final long DEFAULT_SHUTDOWN_EXECUTOR_TIMEOUT = 30;

    @Inject
    private Logger logger;

    public void shutdownAndWait(ExecutorService executor) {
        shutdownAndWait(executor, DEFAULT_SHUTDOWN_EXECUTOR_TIMEOUT, TimeUnit.SECONDS);
    }

    public void shutdownAndWait(ExecutorService executor, long timeout, TimeUnit unit) {
        executor.shutdown();
        try {
            executor.awaitTermination(timeout, unit);
        } catch (InterruptedException e) {
            logger.error(this.getClass(), "Interruption while executor shutdown waiting", e);
        }
    }

    public <T> T mapFuture(Future future, Class<T> clazz) {
        try {
            return clazz.cast(future.get());
        } catch (ClassCastException | InterruptedException e) {
            return null;
        } catch (ExecutionException e) {
            if (e.getCause() instanceof ServiceException) {
                throw (ServiceException) e.getCause();
            } else {
                logger.error(this.getClass(), "Exception thrown during thread execution", e.getCause());
                return null;
            }
        }
    }

}
