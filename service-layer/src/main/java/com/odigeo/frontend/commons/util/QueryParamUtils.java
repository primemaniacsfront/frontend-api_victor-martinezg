package com.odigeo.frontend.commons.util;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.RsaException;
import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class QueryParamUtils {

    private static final String BOOKING_ID = "bookingid";
    private static final String EMAIL = "email";
    private static final String AMPERSAND = "&";
    private static final String EQUAL = "=";

    @Inject
    private EncryptUtils encryptUtils;

    public String getQueryParamsString(Map<String, Object> params, String prefix, String suffix) {
        return params.keySet().stream()
                .map(key -> key + EQUAL + params.get(key))
                .collect(Collectors.joining(AMPERSAND, prefix, suffix));
    }

    public String getQueryParamsString(Map<String, Object> params) {
        return getQueryParamsString(params, StringUtils.EMPTY, StringUtils.EMPTY);
    }

    public String getQueryParamsEncrypted(Map<String, Object> params, String prefix, String suffix) {
        String queryParams = getQueryParamsString(params, prefix, suffix);
        try {
            return encryptUtils.encryptBase64URLSafe(queryParams.getBytes(StandardCharsets.UTF_8.name()));
        } catch (RsaException | UnsupportedEncodingException e) {
            return StringUtils.EMPTY;
        }
    }

    public String getQueryParamsEncrypted(Map<String, Object> params) {
        return getQueryParamsEncrypted(params, StringUtils.EMPTY, StringUtils.EMPTY);
    }

    public String generateMyTripsRedirectionToken(long bookingId, String email) {
        Map<String, Object> queryParams = Stream.of(
                new AbstractMap.SimpleEntry<>(BOOKING_ID, bookingId),
                new AbstractMap.SimpleEntry<>(EMAIL, email)
        ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
        return getQueryParamsEncrypted(queryParams);
    }

}
