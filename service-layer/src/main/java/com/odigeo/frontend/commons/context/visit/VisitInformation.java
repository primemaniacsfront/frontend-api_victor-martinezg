package com.odigeo.frontend.commons.context.visit;

import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Browser;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.OS;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import java.util.Map;

public class VisitInformation {

    private Long visitId;
    private String visitCode;

    private Site site;
    private Brand brand;
    private Device device;
    private Browser browser;
    private OS os;
    private WebInterface webInterface;

    private String locale;
    private String currency;
    private String mktPortal;
    private String websiteDefaultCountry;
    private String userIp;
    private String userDevice;

    private Map<String, Integer> dimensionPartitionMap;

    public Long getVisitId() {
        return visitId;
    }

    void setVisitId(Long visitId) {
        this.visitId = visitId;
    }

    public String getVisitCode() {
        return visitCode;
    }

    void setVisitCode(String visitCode) {
        this.visitCode = visitCode;
    }

    public Device getDevice() {
        return device;
    }

    public Site getSite() {
        return site;
    }

    void setSite(Site site) {
        this.site = site;
    }

    public Brand getBrand() {
        return brand;
    }

    void setBrand(Brand brand) {
        this.brand = brand;
    }

    void setDevice(Device device) {
        this.device = device;
    }

    public Browser getBrowser() {
        return browser;
    }

    void setBrowser(Browser browser) {
        this.browser = browser;
    }

    public OS getOs() {
        return os;
    }

    void setOs(OS os) {
        this.os = os;
    }

    public WebInterface getWebInterface() {
        return webInterface;
    }

    void setWebInterface(WebInterface webInterface) {
        this.webInterface = webInterface;
    }

    public String getLocale() {
        return locale;
    }

    void setLocale(String locale) {
        this.locale = locale;
    }

    public String getCurrency() {
        return currency;
    }

    void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMktPortal() {
        return mktPortal;
    }

    void setMktPortal(String mktPortal) {
        this.mktPortal = mktPortal;
    }

    public String getWebsiteDefaultCountry() {
        return websiteDefaultCountry;
    }

    void setWebsiteDefaultCountry(String websiteDefaultCountry) {
        this.websiteDefaultCountry = websiteDefaultCountry;
    }

    public String getUserIp() {
        return userIp;
    }

    void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getUserDevice() {
        return userDevice;
    }

    void setUserDevice(String userDevice) {
        this.userDevice = userDevice;
    }

    public Map<String, Integer> getDimensionPartitionMap() {
        return dimensionPartitionMap;
    }

    void setDimensionPartitionMap(Map<String, Integer> dimensionPartitionMap) {
        this.dimensionPartitionMap = dimensionPartitionMap;
    }

}
