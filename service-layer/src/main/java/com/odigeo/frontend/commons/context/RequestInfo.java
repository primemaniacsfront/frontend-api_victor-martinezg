package com.odigeo.frontend.commons.context;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.Cookie;
import org.apache.commons.lang3.StringUtils;

public class RequestInfo {

    private String requestUrl;
    private String serverName;
    private String contextPath;
    private final Map<SiteHeaders, String> headers = new HashMap<>();
    private final Map<String, Cookie> cookies = new HashMap<>();

    public String getRequestUrl() {
        return requestUrl;
    }

    void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getServerName() {
        return serverName;
    }

    void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public Collection<Cookie> getCookies() {
        return cookies.values();
    }

    public Cookie getCookie(SiteCookies siteCookie) {
        return cookies.get(siteCookie.value());
    }

    public String getCookieValue(SiteCookies siteCookie) {
        return Optional.ofNullable(getCookie(siteCookie)).map(Cookie::getValue).orElse(null);
    }

    public void addCookie(Cookie cookie) {
        cookies.put(cookie.getName(), cookie);
    }

    public String getHeader(SiteHeaders siteHeader) {
        return headers.get(siteHeader);
    }

    void addHeader(SiteHeaders siteHeader, String value) {
        headers.put(siteHeader, value);
    }

    public String getHeaderOrCookie(SiteHeaders siteHeader, SiteCookies siteCookie) {
        String headerValue = getHeader(siteHeader);
        String cookieValue = getCookieValue(siteCookie);

        return StringUtils.isEmpty(headerValue) || StringUtils.equals(headerValue, "null") ? cookieValue : headerValue;
    }

}
