package com.odigeo.frontend.commons.context.visit;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.mappers.BrandMapper;
import com.odigeo.frontend.commons.context.visit.mappers.BrowserMapper;
import com.odigeo.frontend.commons.context.visit.mappers.DeviceMapper;
import com.odigeo.frontend.commons.context.visit.mappers.OSMapper;
import com.odigeo.frontend.commons.context.visit.mappers.SiteMapper;
import com.odigeo.frontend.commons.context.visit.mappers.WebInterfaceMapper;
import com.odigeo.frontend.services.VisitEngineService;
import com.odigeo.visitengineapi.v1.marketing.MarketingPortal;
import com.odigeo.visitengineapi.v1.multitest.TestTokenSet;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.website.Website;
import java.util.Collections;
import java.util.Currency;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

@Singleton
public class VisitDeserializer {

    @Inject
    private VisitEngineService visitEngineService;
    @Inject
    private BrandMapper brandMapper;
    @Inject
    private SiteMapper siteMapper;
    @Inject
    private WebInterfaceMapper webInterfaceMapper;
    @Inject
    private DeviceMapper deviceMapper;
    @Inject
    private BrowserMapper browserMapper;
    @Inject
    private OSMapper osMapper;

    public VisitInformation deserialize(String visit) {
        VisitResponse visitResponse = visitEngineService.deserialize(visit);
        VisitInformation visitInformation = null;

        if (visitResponse != null) {
            visitInformation = new VisitInformation();

            visitInformation.setVisitId(visitResponse.getVisitId());
            visitInformation.setVisitCode(visitResponse.getVisitInformation());

            visitInformation.setSite(siteMapper.map(visitResponse));
            visitInformation.setBrand(brandMapper.map(visitResponse));
            visitInformation.setDevice(deviceMapper.map(visitResponse));
            visitInformation.setBrowser(browserMapper.map(visitResponse));
            visitInformation.setOs(osMapper.map(visitResponse));
            visitInformation.setWebInterface(webInterfaceMapper.map(visitResponse));

            visitInformation.setLocale(initLocale(visitResponse));
            visitInformation.setCurrency(initCurrency(visitResponse));
            visitInformation.setMktPortal(initMktPortal(visitResponse));
            visitInformation.setWebsiteDefaultCountry(initWebsiteDefaultCountry(visitResponse));
            visitInformation.setUserIp(visitResponse.getUserIp());
            visitInformation.setUserDevice(visitResponse.getUserDevice());
            visitInformation.setDimensionPartitionMap(initDimensionPartitionMap(visitResponse));
        }

        return visitInformation;
    }

    private String initLocale(VisitResponse visitResponse) {
        return Optional.ofNullable(visitResponse.getLocale())
            .map(Locale::toString)
            .orElse(StringUtils.EMPTY);
    }

    private String initCurrency(VisitResponse visitResponse) {
        return Optional.ofNullable(visitResponse.getWebsite())
            .map(Website::getCurrency)
            .map(Currency::getCurrencyCode)
            .orElse(StringUtils.EMPTY);
    }

    private String initMktPortal(VisitResponse visitResponse) {
        return Optional.ofNullable(visitResponse.getMarketingPortal())
            .map(MarketingPortal::getCode)
            .orElse(StringUtils.EMPTY);
    }

    private String initWebsiteDefaultCountry(VisitResponse visitResponse) {
        return Optional.ofNullable(visitResponse.getWebsite())
            .map(Website::getDefaultCountry)
            .orElse(StringUtils.EMPTY);
    }

    private Map<String, Integer> initDimensionPartitionMap(VisitResponse visitResponse) {
        return Optional.ofNullable(visitResponse.getTestTokenSet())
            .map(TestTokenSet::getDimensionPartitionMapRepresentation)
            .orElse(Collections.emptyMap());
    }

}
