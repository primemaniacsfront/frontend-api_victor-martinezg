package com.odigeo.frontend.commons.context.visit.types;

public enum WebInterface {
    ONE_FRONT_DESKTOP,
    ONE_FRONT_SMARTPHONE,
    ONE_FRONT_TABLET,
    OTHER
}
