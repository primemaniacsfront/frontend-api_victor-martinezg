package com.odigeo.frontend.services;

import com.edreamsodigeo.seo.sslppopularitystats.exceptions.InvalidParametersException;
import com.edreamsodigeo.seo.sslppopularitystats.exceptions.PopularityStatsException;
import com.edreamsodigeo.seo.sslppopularitystats.popularity.v1.BookingPopularityService;
import com.edreamsodigeo.seo.sslppopularitystats.popularity.v1.model.responses.CityBookingAggregation;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;

import java.util.List;

@Singleton
public class PopularityStatsService {

    @Inject
    private BookingPopularityService bookingPopularityService;

    public List<CityBookingAggregation> getPopularDestinationCities(Site site, String aggregateBy, Integer max) {
        try {
            return bookingPopularityService.citiesAggregateByWebsite(site.name(), aggregateBy, max);
        } catch (InvalidParametersException | PopularityStatsException e) {
            throw new ServiceException("Exception in PopularityStatsService", e, ServiceName.SSLP_POPULARITY_STATS);
        }
    }
}
