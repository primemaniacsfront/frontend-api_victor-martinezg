package com.odigeo.frontend.services.onefront;

import java.io.Serializable;
import java.util.Objects;

public class SearchSessionLoaderServiceCacheKey implements Serializable {

    private String value;

    public SearchSessionLoaderServiceCacheKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !getClass().equals(obj.getClass())) {
            return false;
        }

        SearchSessionLoaderServiceCacheKey shoppingBasketCacheKey = (SearchSessionLoaderServiceCacheKey) obj;
        return Objects.equals(value, shoppingBasketCacheKey.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

}
