package com.odigeo.frontend.services.onefront;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.client.CookieStore;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class SearchSessionLoaderService {

    private static final String SEARCH_SESSION_LOADER_SESSION_ERROR = "We couldn't store search results in onefront session";

    @Inject
    private SearchSessionLoaderServiceFactory searchSessionLoaderServiceFactory;

    public void loadSearchInfoInSession(String searchResponse, ResolverContext context) {

        try {
            CookieStore cookieStore = SearchSessionLoaderServiceBrowserStorageHelper.createSearchSessionLoaderCookieStore(context);

            Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pair = getSearchSessionLoaderService(cookieStore, context.getRequestInfo());
            com.odigeo.frontend.contract.onefront.SearchSessionLoaderService searchSessionLoaderService = pair.getLeft();
            searchSessionLoaderService.loadSearchInfoInSession(searchResponse);

            searchSessionLoaderServiceFactory.updateServiceCache(pair);

        } catch (RuntimeException e) {
            throw new ServiceException(SEARCH_SESSION_LOADER_SESSION_ERROR, e, ServiceName.ONEFRONT);
        }
    }


    private Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> getSearchSessionLoaderService(CookieStore cookieStore, RequestInfo requestInfo) {
        return searchSessionLoaderServiceFactory.getSearchSessionLoaderService(cookieStore, requestInfo);
    }


}
