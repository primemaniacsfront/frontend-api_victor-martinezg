package com.odigeo.frontend.services.onefront;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.logs.interceptor.ServerLoggingFactory;
import com.odigeo.commons.logs.interceptor.TracerFactory;
import com.odigeo.commons.rest.RestErrorsHandler;
import com.odigeo.commons.rest.ServiceBuilder;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.configuration.URLConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.commons.rest.monitoring.interceptor.MonitorFactory;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.ssl.SSLContexts;
import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class SearchSessionLoaderServiceFactory {

    private static final Logger logger = LoggerFactory.getLogger(SearchSessionLoaderServiceFactory.class.getName());
    private static final String SEARCH_SESSION_LOADER_CONNECTION_ERROR = "We couldn't create SearchSessionLoader connection";
    private static final String SEARCH_SESSION_LOADER_CACHE_ERROR = "We had a problem with connections cache";
    private static final String PATH = "/";
    private static final String PATH_PARAM_NAME = "path";
    private static final String CACHE_NAME = "sessionloaderservice";
    private static final String SECURE_PROTOCOL = "https";
    private static final String LOCAL_ENVIRONMENT_HOST = "local";

    private JCS cache;

    @Inject
    private OFURLConfigurationBuilder ofUrlConfigurationBuilder;

    com.odigeo.frontend.contract.onefront.SearchSessionLoaderService createSearchSessionLoaderService(
            ServiceConfiguration<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService> searchSessionLoaderServiceConfiguration,
            CookieStore cookieStore,
            RequestInfo requestInfo) throws IllegalAccessException, IOException, InvocationTargetException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException, URISyntaxException {

        ServiceConfiguration<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService> configuration = addMandatoryConfiguration(searchSessionLoaderServiceConfiguration, requestInfo);
        ServiceBuilder<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService> searchSessionLoaderServiceBuilder = newInstance(com.odigeo.frontend.contract.onefront.SearchSessionLoaderService.class, configuration, cookieStore, requestInfo);

        return searchSessionLoaderServiceBuilder.build();
    }

    private <T> ServiceBuilder<T> newInstance(Class<T> clazz, ServiceConfiguration<T> configuration, CookieStore cookieStore, RequestInfo requestInfo) throws IllegalAccessException, IOException, InvocationTargetException {
        ServiceBuilder<T> serviceBuilder = createServiceBuilder(clazz, configuration, cookieStore, requestInfo);

        serviceBuilder
            .withInterceptorConfiguration(configuration.getInterceptorConfiguration())
            .withConnectionConfiguration(configuration.getConnectionConfiguration())
            .withLoadBalancer(configuration.getLoadBalancerHolder())
            .withResteasyProviderFactory(configuration.getFactory());

        return serviceBuilder;
    }

    private <T> ServiceBuilder<T> createServiceBuilder(Class<T> clazz, ServiceConfiguration<T> configuration, CookieStore cookieStore, RequestInfo requestInfo) throws IllegalAccessException, IOException, InvocationTargetException {
        RestErrorsHandler restErrorsHandler = configuration.getErrorsHandler();
        URLConfiguration urlConfiguration = ofUrlConfigurationBuilder.build(clazz, requestInfo);

        ServiceBuilder<T> serviceBuilder = new ServiceBuilder<>(clazz, urlConfiguration, restErrorsHandler);
        getSearchSessionLoaderCookieList(cookieStore).forEach(cookie -> setBasicParameters(cookie, urlConfiguration));

        serviceBuilder.withCookieStore(cookieStore);

        return serviceBuilder;
    }

    private List<Cookie> getSearchSessionLoaderCookieList(CookieStore cookieStore) {
        return Optional.ofNullable(cookieStore)
            .map(CookieStore::getCookies)
            .orElse(Collections.emptyList())
            .stream()
            .collect(Collectors.toList());
    }

    private void setBasicParameters(Cookie genericCookie, URLConfiguration urlConfiguration) {
        BasicClientCookie cookie = (BasicClientCookie) genericCookie;
        cookie.setDomain(urlConfiguration.getHost());
        cookie.setSecure(StringUtils.equalsIgnoreCase(urlConfiguration.getProtocol(), SECURE_PROTOCOL));
        String path = Objects.isNull(cookie.getPath()) ? PATH : cookie.getPath();
        cookie.setPath(path);
        cookie.setAttribute(PATH_PARAM_NAME, path);
    }

    private ServiceConfiguration<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService> addMandatoryConfiguration(ServiceConfiguration<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService> serviceConfiguration,
                                                                                                                             RequestInfo requestInfo) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, URISyntaxException {

        addTraceIdInterceptor(serviceConfiguration.getInterceptorConfiguration());
        addServerNameLoggingInterceptor(serviceConfiguration.getInterceptorConfiguration());
        addMonitoringInterceptor(serviceConfiguration.getInterceptorConfiguration());

        if (isDevelopmentEnvironment(requestInfo)) {
            ConnectionConfiguration connectionConfiguration = serviceConfiguration.getConnectionConfiguration();
            SSLSocketFactory sslSocketFactory = new SSLSocketFactory(SSLContexts.custom().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build());
            sslSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            connectionConfiguration.setSSLSocketFactory(sslSocketFactory);
        }
        return serviceConfiguration;
    }

    private boolean isDevelopmentEnvironment(RequestInfo requestInfo) throws URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder(requestInfo.getRequestUrl());
        return (uriBuilder.getHost().contains(LOCAL_ENVIRONMENT_HOST));
    }

    private void addTraceIdInterceptor(InterceptorConfiguration interceptorConfiguration) {
        interceptorConfiguration.addInterceptor((new TracerFactory()).getInstanceHttpRequestInterceptor());
    }

    private void addServerNameLoggingInterceptor(InterceptorConfiguration interceptorConfiguration) {
        interceptorConfiguration.addInterceptor((new ServerLoggingFactory()).getInstanceHttpResponseInterceptor());
    }

    void addMonitoringInterceptor(InterceptorConfiguration interceptorConfiguration) {
        MonitorFactory factory = new MonitorFactory();
        interceptorConfiguration
            .addInterceptor(factory.newHttpRequestInterceptor())
            .addInterceptor(factory.newHttpResponseInterceptor())
            .addInterceptor(factory.newRestUtilsInterceptor());
    }

    public Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> getSearchSessionLoaderService(CookieStore cookieStore, RequestInfo requestInfo) {
        if (!isCacheInitialized()) {
            initializeCache();
        }
        Cookie onefrontJsessionidCookie = SearchSessionLoaderServiceBrowserStorageHelper.getOnefrontJsessionidCookie(cookieStore);
        SearchSessionLoaderServiceCacheKey key = new SearchSessionLoaderServiceCacheKey(onefrontJsessionidCookie.getValue());
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pair = getPairServiceCookieStoreFromCache(key);
        if (Objects.isNull(pair)) {
            try {
                ServiceConfiguration.Builder<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService> searchSessionLoaderServiceBuilder = new ServiceConfiguration.Builder<>(com.odigeo.frontend.contract.onefront.SearchSessionLoaderService.class);
                ServiceConfiguration<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService> searchSessionLoaderServiceConfiguration = searchSessionLoaderServiceBuilder.build();
                com.odigeo.frontend.contract.onefront.SearchSessionLoaderService service = createSearchSessionLoaderService(searchSessionLoaderServiceConfiguration, cookieStore, requestInfo);
                pair = new ImmutablePair<>(service, cookieStore);
            } catch (IllegalAccessException | IOException | InvocationTargetException | NoSuchAlgorithmException | KeyStoreException | UnrecoverableKeyException | KeyManagementException | URISyntaxException e) {
                throw new ServiceException(SEARCH_SESSION_LOADER_CONNECTION_ERROR, e, ServiceName.ONEFRONT);
            }
        }

        return pair;
    }

    Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> getPairServiceCookieStoreFromCache(SearchSessionLoaderServiceCacheKey key) {
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> serviceCookieStorePair = null;
        if (isCacheInitialized()) {
            serviceCookieStorePair = (Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore>) cache.get(key);
        }
        return serviceCookieStorePair;
    }

    private void initializeCache() {
        synchronized (this) {
            try {
                if (!isCacheInitialized()) {
                    buildCache();
                }
            } catch (CacheException e) {
                throw new ServiceException("Error in initializeCache", e, ServiceName.ONEFRONT);
            }
        }
    }

    public void updateServiceCache(Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pair) {
        Cookie onefrontJsessionidCookie = SearchSessionLoaderServiceBrowserStorageHelper.getOnefrontJsessionidCookie(pair.getRight());
        SearchSessionLoaderServiceCacheKey key = new SearchSessionLoaderServiceCacheKey(onefrontJsessionidCookie.getValue());
        try {
            if (!isCacheInitialized()) {
                initializeCache();
            }
            if (isCacheInitialized()) {
                cache.put(key, pair);
            }
        } catch (CacheException | ServiceException e) {
            logger.warn(SEARCH_SESSION_LOADER_CACHE_ERROR, e);
        }
    }

    boolean isCacheInitialized() {
        return Objects.nonNull(cache);
    }

    protected void buildCache() throws CacheException {
        cache = JCS.getInstance(CACHE_NAME);
    }

}
