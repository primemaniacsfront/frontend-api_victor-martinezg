package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.marketing.search.price.v1.requests.SearchPricesRequest;
import com.odigeo.marketing.search.price.v1.responses.SearchPriceResponse;
import com.odigeo.marketing.search.price.v1.services.SearchPriceApiService;

import java.lang.reflect.UndeclaredThrowableException;

@Singleton
public class SearchPriceService {

    @Inject
    private SearchPriceApiService searchPriceApiService;

    public SearchPriceResponse searchRoutePrices(SearchPricesRequest searchPricesRequest) {
        try {
            return searchPriceApiService.searchRoutePrices(searchPricesRequest);
        } catch (UndeclaredThrowableException e) {
            throw new ServiceException("Exception in SearchPriceService", e, ServiceName.SEARCH_PRICE_API);
        }
    }
}
