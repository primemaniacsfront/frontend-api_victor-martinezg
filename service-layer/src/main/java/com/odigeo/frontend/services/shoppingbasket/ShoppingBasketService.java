package com.odigeo.frontend.services.shoppingbasket;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.shoppingbasket.v3.model.CustomerCollectionOptions;
import com.odigeo.shoppingbasket.v3.model.Product;
import com.odigeo.shoppingbasket.v3.request.ContactDetailsRequest;
import com.odigeo.shoppingbasket.v3.request.CreateRequest;
import com.odigeo.shoppingbasket.v3.request.CustomerCollectionOptionRequest;
import com.odigeo.shoppingbasket.v3.request.ProductRequest;
import com.odigeo.shoppingbasket.v3.response.ShoppingBasketResponse;

import java.util.Optional;
import java.util.UUID;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class ShoppingBasketService {

    private static final String CREATE_SHOPPINGBASKET_ERROR = "We couldn't create shoppingbasket";
    private static final String GET_SHOPPINGBASKET_ERROR = "We couldn't get shoppingbasket with id: ";
    private static final String GET_AVAILABLE_COLLECTION_ERROR = "We couldn't get available collection options with shopping basket id: ";
    private static final String ADD_CONTACT_DETAILS_ERROR = "We couldn't add contact details to shopping basket with shopping basket id: ";
    private static final String ADD_PRODUCT_ERROR = "We couldn't add productId: ";
    private static final String REMOVE_PRODUCT_ERROR = "We couldn't remove productId: ";

    @Inject
    private com.odigeo.shoppingbasket.v3.ShoppingBasketService shoppingBasketService;

    public ShoppingBasketResponse create(CreateRequest createRequest) {
        ShoppingBasketResponse shoppingBasketResponse;
        try {
            shoppingBasketResponse = shoppingBasketService.createShoppingBasket(createRequest);
        } catch (Exception e) {
            throw new ServiceException(CREATE_SHOPPINGBASKET_ERROR, e, ServiceName.SHOPPING_BASKET_API);
        }
        return shoppingBasketResponse;
    }

    public ShoppingBasketResponse getShoppingBasket(UUID shoppingBasketId) {
        ShoppingBasketResponse shoppingBasketResponse;

        try {
            shoppingBasketResponse = shoppingBasketService.getShoppingBasket(shoppingBasketId, false);
        } catch (Exception e) {
            throw new ServiceException(GET_SHOPPINGBASKET_ERROR + shoppingBasketId, e, ServiceName.SHOPPING_BASKET_API);
        }

        return shoppingBasketResponse;
    }

    public void addProduct(UUID shoppingBasketId, ProductRequest productRequest) {
        try {
            shoppingBasketService.addProduct(shoppingBasketId, productRequest);
        } catch (Exception e) {
            throw new ServiceException(ADD_PRODUCT_ERROR + getProductId(productRequest), e, ServiceName.SHOPPING_BASKET_API);
        }
    }

    public void removeProduct(UUID shoppingBasketId, ProductRequest productRequest) {
        try {
            Product product = productRequest.getProduct();
            shoppingBasketService.removeProduct(shoppingBasketId, product.getType(), product.getId());
        } catch (Exception e) {
            throw new ServiceException(REMOVE_PRODUCT_ERROR + getProductId(productRequest), e, ServiceName.SHOPPING_BASKET_API);
        }
    }

    public CustomerCollectionOptions getAvailableCollectionMethods(UUID shoppingBasketId, CustomerCollectionOptionRequest customerCollectionOptionRequest) {
        try {
            return shoppingBasketService.getAvailableCustomerCollectionOptions(shoppingBasketId, customerCollectionOptionRequest);
        } catch (Exception e) {
            throw new ServiceException(GET_AVAILABLE_COLLECTION_ERROR + shoppingBasketId, e, ServiceName.SHOPPING_BASKET_API);
        }
    }

    public void addContactDetails(UUID shoppingBasketId, ContactDetailsRequest contactDetailsRequest) {
        try {
            shoppingBasketService.addContactDetails(shoppingBasketId, contactDetailsRequest);
        } catch (Exception e) {
            throw new ServiceException(ADD_CONTACT_DETAILS_ERROR + shoppingBasketId, e, ServiceName.SHOPPING_BASKET_API);
        }
    }

    private String getProductId(ProductRequest productRequest) {
        return Optional.ofNullable(productRequest.getProduct())
                .map(Product::getId)
                .orElse(null);
    }

}
