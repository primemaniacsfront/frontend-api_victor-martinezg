package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.AvailableProductsResponse;
import com.odigeo.dapi.client.BaseResponse;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.BookingService;
import com.odigeo.dapi.client.DistributionApiConnection;
import com.odigeo.dapi.client.DistributionApiConnectionOptions;
import com.odigeo.dapi.client.FraudInfoRequest;
import com.odigeo.dapi.client.InvoiceRequest;
import com.odigeo.dapi.client.MessageResponse;
import com.odigeo.dapi.client.ModifyShoppingCartRequest;
import com.odigeo.dapi.client.PaymentDataRequest;
import com.odigeo.dapi.client.PersonalInfoRequest;
import com.odigeo.dapi.client.PromotionalCodesMaxUsageExecutionException;
import com.odigeo.dapi.client.RegisterUserLoginRequest;
import com.odigeo.dapi.client.RegisterUserLoginResult;
import com.odigeo.dapi.client.ResumeDataRequest;
import com.odigeo.dapi.client.SearchExpiredException;
import com.odigeo.dapi.client.SelectionRequests;
import com.odigeo.dapi.client.SessionExpiredException;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.dapi.client.TestConfigurationSelectionRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResponseInfo;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.services.configurations.dapi.DapiCallback;
import com.odigeo.frontend.services.configurations.dapi.DapiConnectionConfig;
import com.odigeo.frontend.services.configurations.dapi.DapiConnectionFactory;
import org.apache.commons.collections.CollectionUtils;

import javax.servlet.http.Cookie;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Singleton
public class DapiService {

    private static final String ERR = "ERR-";
    private static final String INT = "INT-";
    private static final String GET_SHOPPING_CART = "getShoppingCart";
    private static final String CREATE_SHOPPING_CART = "createShoppingCart";
    private static final String ADD_PERSONAL_INFO_TO_SHOPPING_CART = "addPersonalInfoToShoppingCart";
    private static final String REGISTER_USER_LOGIN = "registerUserLogin";
    private static final String GET_AVAILABLE_PRODUCTS = "getAvailableProducts";
    private static final String RESUME_BOOKING = "resumeBooking";
    private static final String ADD_TO_SHOPPING_CART = "addToShoppingCart";
    private static final String REMOVE_FROM_SHOPPING_CART = "removeFromShoppingCart";

    @Inject
    private DapiConnectionFactory dapiConnectionFactory;
    @Inject
    private DapiConnectionConfig dapiConnectionConfig;

    public AvailableProductsResponse getAvailableProducts(Long bookingId, ResolverContext context) {
        try {
            BookingService service = getBookingService(context);
            AvailableProductsResponse response = service.getAvailableProducts(bookingId, null);
            handleResponseErrors(response, GET_AVAILABLE_PRODUCTS);
            return response;
        } catch (SearchExpiredException | SessionExpiredException e) {
            throw new ServiceException("Exception in getAvailableProducts from DapiService", e, ServiceName.DAPI);
        }
    }

    public ShoppingCartSummaryResponse addToShoppingCart(ModifyShoppingCartRequest modifyShoppingCartRequest, ResolverContext context) {
        try {
            BookingService service = getBookingService(context);
            ShoppingCartSummaryResponse response = service.addToShoppingCart(modifyShoppingCartRequest);
            handleResponseErrors(response, ADD_TO_SHOPPING_CART);
            return response;
        } catch (SearchExpiredException | SessionExpiredException e) {
            throw new ServiceException("Exception in addToShoppingCart from DapiService", e, ServiceName.DAPI);
        }
    }

    public ShoppingCartSummaryResponse removeFromShoppingCart(ModifyShoppingCartRequest modifyShoppingCartRequest, ResolverContext context) {
        try {
            BookingService service = getBookingService(context);
            ShoppingCartSummaryResponse response = service.removeFromShoppingCart(modifyShoppingCartRequest);
            handleResponseErrors(response, REMOVE_FROM_SHOPPING_CART);
            return response;
        } catch (SearchExpiredException | SessionExpiredException e) {
            throw new ServiceException("Exception in removeFromShoppingCart from DapiService", e, ServiceName.DAPI);
        }
    }

    public ShoppingCartSummaryResponse getShoppingCart(Long bookingId, ResolverContext context) {
        try {
            BookingService service = getBookingService(context);
            ShoppingCartSummaryResponse response = service.getShoppingCart(bookingId);
            handleResponseErrors(response, GET_SHOPPING_CART);
            return response;
        } catch (SearchExpiredException | SessionExpiredException e) {
            throw new ServiceException("Exception in getShoppingCart from DapiService", e, ServiceName.DAPI);
        }
    }

    public ShoppingCartSummaryResponse createShoppingCart(SelectionRequests selectionRequests,
                                                          TestConfigurationSelectionRequest testConfigurationSelectionRequest,
                                                          ResolverContext context) {
        try {
            BookingService service = getBookingService(context);
            ShoppingCartSummaryResponse response = service.createShoppingCart(selectionRequests,
                    testConfigurationSelectionRequest,
                    dapiConnectionConfig.getCredentialId(),
                    dapiConnectionConfig.getPassword());
            handleResponseErrors(response, CREATE_SHOPPING_CART);
            return response;
        } catch (SearchExpiredException | SessionExpiredException e) {
            throw new ServiceException("Exception in createShoppingCart from DapiService", e, ServiceName.DAPI);
        }
    }

    public RegisterUserLoginResult registerUserLogin(RegisterUserLoginRequest registerUserLoginRequest, ResolverContext context) {
        try {
            BookingService service = getBookingService(context);
            RegisterUserLoginResult registerUserLoginResult = service.registerUserLogin(registerUserLoginRequest);
            handleResponseErrors(registerUserLoginResult, REGISTER_USER_LOGIN);
            return registerUserLoginResult;
        } catch (SessionExpiredException e) {
            throw new ServiceException("Exception in registerUserLogin from DapiService", e, ServiceName.DAPI);
        }
    }

    public ShoppingCartSummaryResponse addPersonalInfoToShoppingCart(PersonalInfoRequest personalInfoRequest, ResolverContext context) {
        try {
            BookingService service = getBookingService(context);
            ShoppingCartSummaryResponse response = service.addPersonalInfoToShoppingCart(personalInfoRequest);
            handleResponseErrors(response, ADD_PERSONAL_INFO_TO_SHOPPING_CART);
            return response;
        } catch (SearchExpiredException | SessionExpiredException e) {
            throw new ServiceException("Exception in addPersonalInfoToShoppingCart from DapiService", e, ServiceName.DAPI);
        }
    }

    public BookingResponse confirmBooking(PaymentDataRequest paymentDataRequest, long bookingId, String clientBookingReferencesId,
                                          InvoiceRequest invoiceRequest, FraudInfoRequest fraudInfoRequest, ResolverContext context) {
        try {
            BookingService service = getBookingService(context);
            return service.confirmBooking(paymentDataRequest, bookingId, clientBookingReferencesId, invoiceRequest, fraudInfoRequest);
        } catch (PromotionalCodesMaxUsageExecutionException | SessionExpiredException e) {
            throw new ServiceException("Exception in confirmBooking from DapiService", e, ServiceName.DAPI);
        }
    }


    private void handleResponseErrors(BaseResponse response, String getShoppingCart) {
        if (CollectionUtils.isNotEmpty(response.getMessages())) {
            handleErrors(response.getMessages(), getShoppingCart);
        }
    }

    private void handleErrors(List<MessageResponse> messages, String operationName) {
        messages.stream()
                .filter(Objects::nonNull)
                .filter(msg -> msg.getCode().contains(ERR) || msg.getCode().contains(INT))
                .findFirst()
                .ifPresent(err -> {
                    throw new ServiceException(operationName + " " + err.getCode() + ":" + err.getDescription(), ServiceName.DAPI);
                });
    }

    public BookingResponse resumeBooking(long bookingId, ResumeDataRequest resumeRequest, ResolverContext context) {
        try {
            BookingService service = getBookingService(context);
            BookingResponse response = service.resumeBooking(bookingId, resumeRequest);
            handleResponseErrors(response, RESUME_BOOKING);
            return response;
        } catch (SessionExpiredException e) {
            throw new ServiceException("Exception in resumeBooking from DapiService", e, ServiceName.DAPI);
        }
    }

    private BookingService getBookingService(ResolverContext context) {
        String serverName = context.getRequestInfo().getServerName();
        Collection<Cookie> requestCookies = context.getRequestInfo().getCookies();
        Collection<Cookie> responseCookies = context.getResponseInfo().getCookies();
        ResponseInfo responseInfo = context.getResponseInfo();
        DistributionApiConnection distributionApiConnection = dapiConnectionFactory.getDapiConnectionFactory().getConnection();
        DapiCallback dapiCallback = new DapiCallback(distributionApiConnection, serverName, requestCookies, responseCookies, responseInfo);
        DistributionApiConnectionOptions options = DistributionApiConnection.getDefaultBookingOptions();
        options.setSoapCallback(dapiCallback);
        return distributionApiConnection.getBookingService(options);
    }
}
