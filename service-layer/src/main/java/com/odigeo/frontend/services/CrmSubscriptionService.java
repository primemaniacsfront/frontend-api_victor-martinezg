package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.marketing.subscriptions.rest.v1.parameters.FdoSubscription;
import com.odigeo.marketing.subscriptions.rest.v1.parameters.PriceAlertSubscription;
import com.odigeo.marketing.subscriptions.rest.v1.services.SubscriptionApiService;

import javax.ws.rs.core.Response;
import java.lang.reflect.UndeclaredThrowableException;

@Singleton
public class CrmSubscriptionService {

    @Inject
    private SubscriptionApiService subscriptionService;

    public void subscribeToPriceAlert(PriceAlertSubscription subscription) {
        subscriptionService.subscribeToPriceAlert(subscription);
    }

    public void subscribeToFdo(FdoSubscription subscription) {
        try {
            subscriptionService.subscribeToFdo(subscription);
        } catch (UndeclaredThrowableException e) {
            throw new ServiceException("Error Subscribing to FDO", e, Response.Status.INTERNAL_SERVER_ERROR, ServiceName.CRM_SUBSCRIPTION_API);
        }
    }
}
