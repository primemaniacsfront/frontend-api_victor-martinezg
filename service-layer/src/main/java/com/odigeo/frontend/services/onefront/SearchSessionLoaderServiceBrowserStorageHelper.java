package com.odigeo.frontend.services.onefront;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.jboss.util.Strings;

import javax.servlet.http.Cookie;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public final class SearchSessionLoaderServiceBrowserStorageHelper {

    private SearchSessionLoaderServiceBrowserStorageHelper() {
    }

    public static CookieStore createSearchSessionLoaderCookieStore(ResolverContext context) {
        CookieStore cookieStore = new BasicCookieStore();
        Collection<Cookie> cookies = getRequestInfo(context).getCookies();

        cookies.forEach(cookie -> addCookieToStore(cookieStore, cookie.getName(), cookie.getValue()));

        addCookieToStore(cookieStore, getRequestInfo(context), SiteHeaders.VISIT_INFORMATION, SiteCookies.VISIT_INFORMATION);
        addCookieToStore(cookieStore, getRequestInfo(context), SiteHeaders.OF_JSESSIONID, SiteCookies.OF_JSESSIONID);
        addCookieToStore(cookieStore, getRequestInfo(context), SiteHeaders.SB_JSESSIONID, SiteCookies.SB_JSESSIONID);

        return cookieStore;
    }

    private static RequestInfo getRequestInfo(ResolverContext context) {
        return Optional.ofNullable(context)
                .map(ResolverContext::getRequestInfo)
                .orElse(null);
    }

    private static void addCookieToStore(CookieStore cookieStore, RequestInfo requestInfo,
                                  SiteHeaders siteHeader, SiteCookies siteCookie) {
        String cookieName = siteCookie.value();
        String cookieValue = requestInfo.getHeaderOrCookie(siteHeader, siteCookie);
        addCookieToStore(cookieStore, cookieName, cookieValue);
    }

    private static void addCookieToStore(CookieStore cookieStore, String cookieName, String cookieValue) {
        BasicClientCookie clientCookie = new BasicClientCookie(cookieName, cookieValue);
        cookieStore.addCookie(clientCookie);
    }



    public static org.apache.http.cookie.Cookie getOnefrontJsessionidCookie(CookieStore cookieStore) {
        return Optional.ofNullable(cookieStore)
            .map(CookieStore::getCookies)
            .orElse(Collections.emptyList())
            .stream()
            .filter(cookie -> SiteCookies.OF_JSESSIONID.value().equalsIgnoreCase(cookie.getName()))
            .findFirst()
            .orElse(new BasicClientCookie(SiteCookies.OF_JSESSIONID.value(), Strings.EMPTY));
    }

}
