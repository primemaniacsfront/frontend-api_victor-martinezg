package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.userprofiles.api.v2.SSOApiService;
import com.odigeo.userprofiles.api.v2.UserApiService;
import com.odigeo.userprofiles.api.v2.model.Brand;
import com.odigeo.userprofiles.api.v2.model.SSOToken;
import com.odigeo.userprofiles.api.v2.model.TrafficInterface;
import com.odigeo.userprofiles.api.v2.model.User;
import com.odigeo.userprofiles.api.v2.model.requests.PasswordChangeRequest;
import com.odigeo.userprofiles.api.v2.model.requests.credentials.SSOTokenUserCredentials;
import com.odigeo.userprofiles.api.v2.model.requests.credentials.UserCredentials;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.GenericUserProfilesServiceException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidFindException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidValidationException;

import java.util.Optional;

@Singleton
public class UserDescriptionService {

    @Inject
    private UserApiService userService;
    @Inject
    private SSOApiService ssoApiService;

    public User getUserByToken(ResolverContext context) {
        return Optional.ofNullable(context.getRequestInfo().getCookieValue(SiteCookies.SSO_TOKEN))
                .map(token -> {
                    VisitInformation visit = context.getVisitInformation();
                    SSOTokenUserCredentials credentials = new SSOTokenUserCredentials();

                    credentials.setToken(token);
                    credentials.setBrand(Brand.fromString(visit.getBrand().name()));
                    credentials.setTrafficInterface(getTrafficInterface(visit));
                    credentials.setWebsite(visit.getSite().name());
                    credentials.setLocale(visit.getLocale());

                    return credentials;
                }).map(this::getUser).orElse(null);
    }

    public boolean isUserLogged(ResolverContext context) {
        return Optional.ofNullable(context.getRequestInfo().getCookieValue(SiteCookies.SSO_TOKEN)).isPresent();
    }

    public User setUserPassword(PasswordChangeRequest passwordChangeRequest) {
        try {
            return userService.setUserPassword(passwordChangeRequest);
        } catch (GenericUserProfilesServiceException ex) {
            throw new ServiceException("Error in setUserPassword", ex, ServiceName.USER_API);
        }
    }

    public SSOToken generateToken(UserCredentials userCredentials) {
        try {
            return ssoApiService.generateToken(userCredentials);
        } catch (GenericUserProfilesServiceException ex) {
            throw new ServiceException("Error in generateToken", ex, ServiceName.USER_API);
        }
    }

    User getUser(UserCredentials credentials) {
        try {
            return userService.getUser(credentials);
        } catch (InvalidCredentialsException | InvalidValidationException | InvalidFindException ex) {
            return null;
        } catch (GenericUserProfilesServiceException ex) {
            throw new ServiceException("Error in getUser", ex, ServiceName.USER_API);
        }
    }

    TrafficInterface getTrafficInterface(VisitInformation visit) {
        switch (visit.getWebInterface()) {
        case ONE_FRONT_DESKTOP:
            return TrafficInterface.ONE_FRONT_DESKTOP;
        case ONE_FRONT_SMARTPHONE:
            return TrafficInterface.ONE_FRONT_SMARTPHONE;
        default:
            return TrafficInterface.OTHERS;
        }
    }
}
