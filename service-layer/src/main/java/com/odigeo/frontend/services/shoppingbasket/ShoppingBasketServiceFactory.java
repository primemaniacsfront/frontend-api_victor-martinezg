package com.odigeo.frontend.services.shoppingbasket;

import com.google.inject.Singleton;
import com.odigeo.commons.logs.interceptor.ServerLoggingFactory;
import com.odigeo.commons.logs.interceptor.TracerFactory;
import com.odigeo.commons.rest.RestErrorsHandler;
import com.odigeo.commons.rest.ServiceBuilder;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.configuration.URLConfiguration;
import com.odigeo.commons.rest.configuration.URLConfigurationLoader;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.commons.rest.monitoring.interceptor.MonitorFactory;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.shoppingbasket.v2.ShoppingBasketService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class ShoppingBasketServiceFactory {

    private static final Logger logger = LoggerFactory.getLogger(ShoppingBasketServiceFactory.class.getName());
    private static final String SHOPPING_BASKET_CONNECTION_ERROR = "We couldn't create shoppingbasket connection";
    private static final String SHOPPING_BASKET_CACHE_ERROR = "We had a problem with connections cache";
    private static final String PATH = "/";
    private static final String PATH_PARAM_NAME = "path";
    private static final String CACHE_NAME = "sbservice";
    private static final String SECURE_PROTOCOL = "https";

    private JCS cache;

    ShoppingBasketService createShoppingBasketService(
            ServiceConfiguration<ShoppingBasketApiV2> shoppingBasketServiceServiceConfiguration,
            CookieStore cookieStore) throws IllegalAccessException, IOException, InvocationTargetException {

        ServiceConfiguration<ShoppingBasketApiV2> configuration = addMandatoryConfiguration(shoppingBasketServiceServiceConfiguration);
        ServiceBuilder<ShoppingBasketApiV2> shoppingBasketServiceBuilder = newInstance(ShoppingBasketApiV2.class, configuration, cookieStore);

        return shoppingBasketServiceBuilder.build();
    }

    private <T> ServiceBuilder<T> newInstance(Class<T> clazz, ServiceConfiguration<T> configuration, CookieStore cookieStore) throws IllegalAccessException, IOException, InvocationTargetException {
        ServiceBuilder<T> serviceBuilder = createServiceBuilder(clazz, configuration, cookieStore);

        serviceBuilder
            .withInterceptorConfiguration(configuration.getInterceptorConfiguration())
            .withConnectionConfiguration(configuration.getConnectionConfiguration())
            .withLoadBalancer(configuration.getLoadBalancerHolder())
            .withResteasyProviderFactory(configuration.getFactory());

        return serviceBuilder;
    }

    private <T> ServiceBuilder<T> createServiceBuilder(Class<T> clazz, ServiceConfiguration<T> configuration, CookieStore cookieStore) throws IllegalAccessException, IOException, InvocationTargetException {
        RestErrorsHandler restErrorsHandler = configuration.getErrorsHandler();
        URLConfiguration urlConfiguration = (new URLConfigurationLoader()).getURLConfiguration(clazz);
        ServiceBuilder<T> serviceBuilder = new ServiceBuilder<>(clazz, urlConfiguration, restErrorsHandler);

        getSbCookieList(cookieStore).forEach(cookie -> setBasicParameters(cookie, urlConfiguration));

        serviceBuilder.withCookieStore(cookieStore);

        return serviceBuilder;
    }

    private List<Cookie> getSbCookieList(CookieStore cookieStore) {
        return Optional.ofNullable(cookieStore)
            .map(CookieStore::getCookies)
            .orElse(Collections.emptyList())
            .stream()
            .filter(cookie -> ShoppingBasketBrowserStorageHelper.JSESSIONID.equals(cookie.getName()))
            .collect(Collectors.toList());
    }

    private void setBasicParameters(Cookie genericCookie, URLConfiguration urlConfiguration) {
        BasicClientCookie cookie = (BasicClientCookie) genericCookie;
        cookie.setDomain(urlConfiguration.getHost());
        cookie.setSecure(StringUtils.equalsIgnoreCase(urlConfiguration.getProtocol(), SECURE_PROTOCOL));
        String path = Objects.isNull(cookie.getPath()) ? PATH : cookie.getPath();
        cookie.setPath(path);
        cookie.setAttribute(PATH_PARAM_NAME, path);
    }

    private ServiceConfiguration<ShoppingBasketApiV2> addMandatoryConfiguration(ServiceConfiguration<ShoppingBasketApiV2> serviceConfiguration) {

        addTraceIdInterceptor(serviceConfiguration.getInterceptorConfiguration());
        addServerNameLoggingInterceptor(serviceConfiguration.getInterceptorConfiguration());
        addMonitoringInterceptor(serviceConfiguration.getInterceptorConfiguration());

        return serviceConfiguration;
    }

    private void addTraceIdInterceptor(InterceptorConfiguration interceptorConfiguration) {
        interceptorConfiguration.addInterceptor((new TracerFactory()).getInstanceHttpRequestInterceptor());
    }

    private void addServerNameLoggingInterceptor(InterceptorConfiguration interceptorConfiguration) {
        interceptorConfiguration.addInterceptor((new ServerLoggingFactory()).getInstanceHttpResponseInterceptor());
    }

    void addMonitoringInterceptor(InterceptorConfiguration interceptorConfiguration) {
        MonitorFactory factory = new MonitorFactory();
        interceptorConfiguration
            .addInterceptor(factory.newHttpRequestInterceptor())
            .addInterceptor(factory.newHttpResponseInterceptor())
            .addInterceptor(factory.newRestUtilsInterceptor());
    }

    public Pair<ShoppingBasketService, CookieStore> getShoppingBasketService(CookieStore cookieStore) {
        if (!isCacheInitialized()) {
            initializeCache();
        }
        Cookie sbJsessionidCookie = ShoppingBasketBrowserStorageHelper.getSbJsessionidCookie(cookieStore);
        ShoppingBasketCacheKey key = new ShoppingBasketCacheKey(sbJsessionidCookie.getValue());
        Pair<ShoppingBasketService, CookieStore> pair = getPairServiceCookieStoreFromCache(key);
        if (Objects.isNull(pair)) {
            try {
                ServiceConfiguration.Builder<ShoppingBasketApiV2> shoppingBasketServiceBuilder = new ServiceConfiguration.Builder<>(ShoppingBasketApiV2.class);
                ServiceConfiguration<ShoppingBasketApiV2> basketServiceServiceConfiguration = shoppingBasketServiceBuilder.build();
                ShoppingBasketService service = createShoppingBasketService(basketServiceServiceConfiguration, cookieStore);
                pair = new ImmutablePair<>(service, cookieStore);
            } catch (IllegalAccessException | IOException | InvocationTargetException e) {
                throw new ServiceException(SHOPPING_BASKET_CONNECTION_ERROR, e, ServiceName.SHOPPING_BASKET_API);
            }
        }

        return pair;
    }

    Pair<ShoppingBasketService, CookieStore> getPairServiceCookieStoreFromCache(ShoppingBasketCacheKey key) {
        Pair<ShoppingBasketService, CookieStore> serviceCookieStorePair = null;
        if (isCacheInitialized()) {
            serviceCookieStorePair = (Pair<ShoppingBasketService, CookieStore>) cache.get(key);
        }
        return serviceCookieStorePair;
    }

    private void initializeCache() {
        synchronized (this) {
            try {
                if (!isCacheInitialized()) {
                    buildCache();
                }
            } catch (CacheException e) {
                throw new ServiceException("Error in initializeCache", e, ServiceName.SHOPPING_BASKET_API);
            }
        }
    }

    public void updateServiceCache(Pair<ShoppingBasketService, CookieStore> pair) {
        Cookie sbJsessionidCookie = ShoppingBasketBrowserStorageHelper.getSbJsessionidCookie(pair.getRight());
        ShoppingBasketCacheKey key = new ShoppingBasketCacheKey(sbJsessionidCookie.getValue());
        try {
            if (!isCacheInitialized()) {
                initializeCache();
            }
            if (isCacheInitialized()) {
                cache.put(key, pair);
            }
        } catch (CacheException | ServiceException e) {
            logger.warn(SHOPPING_BASKET_CACHE_ERROR, e);
        }
    }

    boolean isCacheInitialized() {
        return Objects.nonNull(cache);
    }

    protected void buildCache() throws CacheException {
        cache = JCS.getInstance(CACHE_NAME);
    }
}
