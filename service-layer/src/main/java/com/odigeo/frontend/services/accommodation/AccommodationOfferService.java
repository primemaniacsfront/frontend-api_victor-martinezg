package com.odigeo.frontend.services.accommodation;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.accommodation.offer.model.AccommodationDealKey;
import com.odigeo.accommodation.offer.model.Availability;
import com.odigeo.accommodation.offer.model.RoomGroupDealKey;
import com.odigeo.accommodation.offer.model.SearchId;

@Singleton
public class AccommodationOfferService {

    @Inject
    private com.odigeo.accommodation.offer.AccommodationOfferService accommodationOfferService;

    public String checkAvailability(String searchId, String accommodationDealKey, String roomGroupDealKey) {
        SearchId searchIdRequest = SearchId.builder().id(searchId).build();
        AccommodationDealKey accommodationDealKeyRequest = AccommodationDealKey.builder().id(accommodationDealKey).build();
        RoomGroupDealKey roomGroupDealKeyRequest = RoomGroupDealKey.builder().id(roomGroupDealKey).build();
        Availability availability = accommodationOfferService.checkAvailability(searchIdRequest, accommodationDealKeyRequest, roomGroupDealKeyRequest);
        return availability.getAvailabilityId().getId();
    }
}
