package com.odigeo.frontend.services.currencyconvert;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.curex.client.currencyconverter.CurrencyConverterFactory;
import com.odigeo.curex.client.currencyconverter.DefaultCurrencyConverter;
import com.odigeo.curex.client.currencyconverter.exception.CurrencyConverterFactoryException;
import com.odigeo.frontend.commons.Logger;

import java.math.BigDecimal;
import java.util.Optional;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class CurrencyExchangeHandler {

    @Inject
    private Logger logger;
    private static final String ERROR_CURRENCY_CONVERT_RATE = "Error getting the conversion rate";
    private static final String ERROR_CURRENCY_ID = "Error getting the current conversion rate id";

    @Inject
    private CurrencyConverterFactory currencyConverterFactory;

    public BigDecimal getCurrencyConvertRate(String sourceCurrency, String targetCurrency) {
        BigDecimal conversionRate = BigDecimal.ONE;
        try {
            DefaultCurrencyConverter currentCurrencyConverter = currencyConverterFactory.getCurrentCurrencyConverter();
            conversionRate = currentCurrencyConverter.getConversionRate(sourceCurrency, targetCurrency);
        } catch (CurrencyConverterFactoryException | RuntimeException ex) {
            logger.error(this.getClass(), ERROR_CURRENCY_CONVERT_RATE
                    + " source: " + sourceCurrency + " destination: " + targetCurrency);
        }
        return conversionRate;
    }

    public Optional<Long> getCurrentCurrencyConvertId() {
        Long id = null;
        try {
            id = currencyConverterFactory.getCurrentCurrencyConverter().getId();
        } catch (CurrencyConverterFactoryException e) {
            logger.error(this.getClass(), ERROR_CURRENCY_ID);
        }
        return Optional.ofNullable(id);
    }
}
