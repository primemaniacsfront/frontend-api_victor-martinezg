package com.odigeo.frontend.services;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MeasureTags;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.searchengine.v2.SearchEngineServiceRest;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetailResponse;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;
import com.odigeo.searchengine.v2.responses.error.MessageResponse;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.Response.Status;
import java.util.Map;
import java.util.Optional;

@Singleton
public class SearchService {

    private static final String INTERNAL_ERROR_PREFIX = "INT";
    private static final String VALIDATION_ERROR_PREFIX = "ERR";
    private static final String WARNING_PREFIX = "WNG";

    @VisibleForTesting
    static final String WNG_025 = "WNG-025";

    @Inject
    private SearchEngineServiceRest searchEngineService;

    @Inject
    private MetricsHandler metricsHandler;

    @Inject
    private Logger logger;

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public SearchResponse executeSearch(SearchMethodRequest searchMethodRequest) {
        SearchResponse response;

        try {
            response = searchEngineService.search(searchMethodRequest);
        } catch (RuntimeException ex) {
            throw new ServiceException("exception at search-engine request", ex, ServiceName.SEARCH_ENGINE);
        }

        handleResponseSearchMessage(response);

        return response;
    }

    private void handleResponseSearchMessage(SearchResponse response) {
        Optional.ofNullable(response).map(this::getSearchMessage).ifPresent(message -> {
            String code = message.getCode();
            String desc = message.getDescription();

            if (isValidationError(code)) {
                trackErrorMetric(code);
                throw new ServiceException(code + ":" + desc, Status.BAD_REQUEST, ErrorCodes.VALIDATION, ServiceName.SEARCH_ENGINE);
            } else if (isInternalError(code)) {
                trackErrorMetric(code);
                throw new ServiceException(code + ":" + desc, ServiceName.SEARCH_ENGINE);
            } else if (isWarning(code)) {
                trackWarningMetric(code);
                logWarning(code);
            }
        });
    }

    private void trackErrorMetric(String code) {
        Map<String, String> tags = new HashedMap<>();
        tags.put(MeasureTags.CODE.getLabel(), code);
        metricsHandler.trackCounter(MeasureConstants.SEARCH_ERROR_COUNTER, tags);
    }

    private void trackWarningMetric(String code) {
        Map<String, String> tags = new HashedMap<>();
        tags.put(MeasureTags.CODE.getLabel(), code);
        metricsHandler.trackCounter(MeasureConstants.SEARCH_WARNING_COUNTER, tags);
    }

    private void logWarning(String code) {
        if (StringUtils.equals(WNG_025, code)) {
            logger.warning(this.getClass(), "SE response with warning: " + code);
        }
    }

    //TODO will be improved at TFC-1598
    private boolean isInternalError(String code) {
        return StringUtils.startsWith(code, INTERNAL_ERROR_PREFIX);
    }

    //TODO will be improved at TFC-1598
    private boolean isValidationError(String code) {
        return StringUtils.startsWith(code, VALIDATION_ERROR_PREFIX);
    }

    private boolean isWarning(String code) {
        return StringUtils.startsWith(code, WARNING_PREFIX);
    }

    public MessageResponse getSearchMessage(SearchResponse searchResponse) {
        return Optional.ofNullable(searchResponse.getMessages())
                .filter(CollectionUtils::isNotEmpty)
                .map(IterableUtils::first)
                .orElse(null);
    }

    public SearchResults getSearchResult(long searchId, Integer fareItineraryIndex) {
        return getSearchResult(searchId, fareItineraryIndex, null);
    }

    public SearchResults getSearchResults(long searchId, String visitInformation) {
        return searchEngineService.getSearchResults(searchId, visitInformation);
    }

    public SearchResults getSearchResult(long searchId, Integer fareItineraryIndex, Integer accommodationDealIndex) {
        return searchEngineService.getSearchResult(searchId, fareItineraryIndex, accommodationDealIndex);
    }

    public AccommodationDetailResponse getAccommodationDetails(long searchId, long accommodationDealKey, String visitInformation) {
        return searchEngineService.getAccommodationDetails(searchId, accommodationDealKey, visitInformation);
    }

    public SearchRoomResponse getRoomsAvailability(long searchId, long accommodationDealKey, String visitInformation) {
        return searchEngineService.searchRoomAvailability(searchId, accommodationDealKey, visitInformation);
    }
}
