package com.odigeo.frontend.services.currencyconvert;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.visit.VisitInformation;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Singleton
public class CurrencyConvertHandler {

    @Inject
    private CurrencyExchangeHandler currencyExchangeHandler;


    public BigDecimal convertToSiteCurrency(VisitInformation visitInformation, BigDecimal ammountToConvert, String sourceCurrencyCode) {
        BigDecimal convertRate = getConvertRate(visitInformation.getCurrency(), sourceCurrencyCode);
        return ammountToConvert.multiply(convertRate).setScale(2, RoundingMode.HALF_UP);
    }

    private BigDecimal getConvertRate(String targetCurrencyCode, String providerCurrencyCode) {
        return currencyExchangeHandler.getCurrencyConvertRate(providerCurrencyCode, targetCurrencyCode);
    }
}
