package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.geolocation.suggestions.models.GeolocationSuggestionsRequestDTO;
import com.odigeo.geoapi.v4.AirportService;
import com.odigeo.geoapi.v4.AutoCompleteService;
import com.odigeo.geoapi.v4.CityService;
import com.odigeo.geoapi.v4.CountryService;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoNodeService;
import com.odigeo.geoapi.v4.GeoServiceException;
import com.odigeo.geoapi.v4.InvalidParametersException;
import com.odigeo.geoapi.v4.ItineraryLocationService;
import com.odigeo.geoapi.v4.responses.Airport;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Country;
import com.odigeo.geoapi.v4.responses.GeoNode;
import com.odigeo.geoapi.v4.responses.ItineraryLocation;
import com.odigeo.geoapi.v4.responses.LocationDescriptionSorted;

import java.util.List;

@Singleton
public class GeoService {

    @Inject
    private ItineraryLocationService itineraryService;
    @Inject
    private CityService cityService;
    @Inject
    private AutoCompleteService autoCompleteService;
    @Inject
    private AirportService airportService;
    @Inject
    private CountryService countryService;
    @Inject
    private GeoNodeService geoNodeService;
    @Inject
    private Logger logger;


    public ItineraryLocation getItineraryLocation(Integer geoId) {
        try {
            return itineraryService.getItineraryLocationByGeoNodeId(geoId);
        } catch (GeoServiceException ex) {
            throw new ServiceException("Exception in getItineraryLocation", ex, ServiceName.GEO_SERVICE);
        } catch (GeoNodeNotFoundException ex) {
            logger.warning(GeoService.class, "Warn in getItineraryLocation: " + ex.getMessage());
            return null;
        }
    }

    public City getCityByGeonodeId(Integer geoId) throws GeoNodeNotFoundException {
        try {
            return cityService.getEntityByGeoNodeId(geoId);
        } catch (GeoServiceException ex) {
            throw new ServiceException("Error in getCityByGeonodeId", ex, ServiceName.GEO_SERVICE);
        }
    }

    public City getCityByIataCode(String iataCode) throws GeoNodeNotFoundException {
        try {
            return cityService.getCityByIataCode(iataCode);
        } catch (GeoServiceException | InvalidParametersException ex) {
            throw new ServiceException("Error in getCityByIataCode", ex, ServiceName.GEO_SERVICE);
        }
    }

    public List<LocationDescriptionSorted> getGeolocationSuggestions(GeolocationSuggestionsRequestDTO request) {
        try {
            return autoCompleteService.autoCompleteSorted(
                    request.getSearchWord(),
                    request.getLocale(),
                    request.getProductType(),
                    request.getWebsite(),
                    request.getInputType(),
                    request.getIncludeMultiLanguage(),
                    request.getIncludeRelatedLocations(),
                    request.getIncludeSearchInAllWords(),
                    request.getIncludeCountry(),
                    request.getIncludeRegion(),
                    request.getIncludeNearestLocations()
            );
        } catch (InvalidParametersException ex) {
            throw new ServiceException("Error in getGeolocationSuggestions", ex, ServiceName.GEO_SERVICE);
        }
    }

    public Airport getAirportByIataCode(String airportIata) throws GeoNodeNotFoundException, InvalidParametersException, GeoServiceException {
        return airportService.getAirportByIataCode(airportIata);
    }

    public List<Country> getCountries() throws GeoServiceException {
        return countryService.retrieveAll();
    }

    public GeoNode getGeonode(Integer geonodeId) throws GeoNodeNotFoundException, GeoServiceException {
        return geoNodeService.getGeoNodeById(geonodeId);
    }
}
