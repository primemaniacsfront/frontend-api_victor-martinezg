package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.hcsapi.v9.HcsApiService;
import com.odigeo.hcsapi.v9.beans.HotelDetailsResponse;
import com.odigeo.hcsapi.v9.beans.HotelSummaryResponse;
import com.odigeo.hcsapi.v9.beans.RoomsDetailRequest;
import com.odigeo.hcsapi.v9.beans.RoomsDetailResponse;
import com.odigeo.hcsapi.v9.beans.SendPackage;

@Singleton
public class AccommodationStaticContentService {
    @Inject
    private HcsApiService hcsApiService;

    public RoomsDetailResponse getRoomsDetail(RoomsDetailRequest roomsDetailRequest) {
        return hcsApiService.getRoomsDetail(roomsDetailRequest);
    }

    public HotelSummaryResponse getHotelsSummary(SendPackage hotelsSummaryRequest) {
        return hcsApiService.getHotelsSummary(hotelsSummaryRequest);
    }

    public HotelDetailsResponse getHotelsDetail(SendPackage hotelsSummaryRequest) {
        return hcsApiService.getHotelsDetail(hotelsSummaryRequest);
    }
}
