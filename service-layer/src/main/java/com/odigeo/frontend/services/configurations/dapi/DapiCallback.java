package com.odigeo.frontend.services.configurations.dapi;

import com.odigeo.dapi.client.DistributionApiConnection;
import com.odigeo.dapi.client.SerializableCookie;
import com.odigeo.dapi.client.SoapCallback;
import com.odigeo.dapi.client.cookies.CookieTransferOptions;
import com.odigeo.dapi.client.cookies.CookieTransferOptionsBuilder;
import com.odigeo.dapi.client.cookies.CookiesStorage;
import com.odigeo.dapi.client.cookies.DapiCookies;
import com.odigeo.dapi.client.cookies.HttpRequestCookies;
import com.odigeo.dapi.client.cookies.HttpResponseCookies;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.context.ResponseInfo;

import javax.servlet.http.Cookie;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.UseVarargs")
public class DapiCallback implements SoapCallback {
    private final DistributionApiConnection connection;
    private final CookieTransferOptions cookieTransferOptions;
    private final CookiesStorage storage;
    private final HttpRequestCookies requestCookies;
    private final HttpResponseCookies responseCookies;
    private final ResponseInfo responseInfo;
    private static final List<String> STORAGE_COOKIE_TO_INJECT = Arrays.asList("DAPI.JSESSIONID");

    public DapiCallback(DistributionApiConnection connection, String serverName, Collection<Cookie> requestCookies, Collection<Cookie> responseCookies, ResponseInfo responseInfo) {
        this.connection = connection;
        this.cookieTransferOptions = getTransferOptions(serverName);
        this.storage = new CookiesStorage();
        this.requestCookies = getRequestCookies(requestCookies);
        this.responseCookies = getResponseCookies(responseCookies);
        this.responseInfo = responseInfo;
    }

    @Override
    public void beforeCall(Method method, Object[] args) {
        // Inject cookies needed for dapi received in request and created in createShoppingCart
        requestCookies.getCookies().stream().filter(cookie -> STORAGE_COOKIE_TO_INJECT.contains(cookie.getName())).forEach(storage::addCookie);
        DapiCookies.transfer(requestCookies, storage).withOptions(cookieTransferOptions).to(connection).now();
    }

    @Override
    public void onError(Method method, Object[] args, Exception e) {
        afterCall(method, args); // Ignore the error regarding cookies
    }

    @Override
    public void afterCall(Method method, Object[] args) {
        DapiCookies.transfer(connection).withOptions(cookieTransferOptions).to(storage, responseCookies).now();
        getContextResponseCookies(storage.getCookies()).forEach(cookie -> responseInfo.addCookie(SiteCookies.getValueFromString(cookie.getName()), cookie.getValue()));
    }

    private CookieTransferOptions getTransferOptions(String domain) {
        CookieTransferOptionsBuilder builder = new CookieTransferOptionsBuilder(domain);
        builder.addCookieToTransferLiterally(SiteCookies.TEST_TOKEN_SPACE.value());
        return builder.build();
    }

    private HttpRequestCookies getRequestCookies(Collection<Cookie> cookies) {
        Collection<SerializableCookie> cookieList = getCookies(cookies);
        return new CollectionBasedHttpRequestCookies(cookieList);
    }

    private HttpResponseCookies getResponseCookies(Collection<Cookie> cookies) {
        Collection<SerializableCookie> cookieList = getCookies(cookies);
        return new CollectionBasedHttpResponseCookies(cookieList);
    }

    private Collection<SerializableCookie> getCookies(Collection<Cookie> cookies) {
        return cookies.stream()
                .map(DapiCookieHelper::new)
                .map(dapiCookie -> new SerializableCookie(
                        dapiCookie.getExpirationDate(),
                        dapiCookie.getNameAndValue(),
                        dapiCookie.getPath(),
                        dapiCookie.getDomain(),
                        dapiCookie.isSecure()))
                .collect(Collectors.toList());
    }

    private Collection<Cookie> getContextResponseCookies(Collection<SerializableCookie> cookies) {
        return cookies.stream()
                .map(dapiCookie -> new Cookie(
                        dapiCookie.getName(),
                        dapiCookie.getValue()
                ))
                .collect(Collectors.toList());
    }
}
