package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.prime.PrimeMarketsConfiguration;
import com.odigeo.frontend.resolvers.prime.models.PrimeMarket;
import com.odigeo.frontend.resolvers.prime.models.PrimeMarketStatus;
import com.odigeo.membership.MemberUserArea;
import com.odigeo.membership.response.FutureFlight;
import com.odigeo.membership.response.Membership;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;

@Singleton
public class PrimeService {

    @Inject
    private MemberUserArea memberUserAreaService;
    @Inject
    private PrimeMarketsConfiguration primeMarketsConfiguration;
    @Inject
    private SiteVariations siteVariations;

    private static final int DEFAULT_EXPIRING_PERIOD = 1;

    public List<FutureFlight> getFutureFlights(Membership member) {
        return Optional.ofNullable(member)
                .map(Membership::getMembershipId)
                .map(membershipId -> memberUserAreaService.getFutureFlights(membershipId))
                .orElseGet(ArrayList::new);
    }

    public boolean isPrimeSite(VisitInformation visit) {
        PrimeMarket primeMarket = primeMarketsConfiguration.getPrimeMarket(visit.getSite());
        PrimeMarketStatus primeMarketStatus = Optional.ofNullable(primeMarket).map(PrimeMarket::getStatus).orElse(PrimeMarketStatus.UNKNOWN);
        return isActivePrimeMarket(primeMarketStatus, visit) && !isExcludedMktPortal(primeMarket, visit);
    }

    public int getExpiringCreditCardPeriod(Site site) {
        return Optional.ofNullable(primeMarketsConfiguration.getPrimeMarket(site))
                .map(PrimeMarket::getExpiringCreditCardPeriod)
                .orElse(DEFAULT_EXPIRING_PERIOD);
    }

    private boolean isExcludedMktPortal(PrimeMarket primeMarket, VisitInformation visit) {
        Set<String> excludedMktPortals = Optional.ofNullable(primeMarket).map(PrimeMarket::getExcludedMktPortals).orElse(new HashSet<>());
        return excludedMktPortals.contains(visit.getMktPortal()) && !siteVariations.isMembershipDisplayedMeta(visit);
    }

    private boolean isActivePrimeMarket(PrimeMarketStatus primeMarketStatus, VisitInformation visit) {
        return PrimeMarketStatus.ROLLOUT.equals(primeMarketStatus)
                || PrimeMarketStatus.IN_AB.equals(primeMarketStatus) && siteVariations.isPrimeTestEnabled(visit);
    }
}
