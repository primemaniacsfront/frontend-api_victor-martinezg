package com.odigeo.frontend.services.membership;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import graphql.VisibleForTesting;


@Singleton
public class MembershipServiceHandler {
    @VisibleForTesting
    static final String ENABLE_AUTO_RENEWAL = "ENABLE_AUTO_RENEWAL";
    @VisibleForTesting
    static final String DISABLE_AUTO_RENEWAL = "DISABLE_AUTO_RENEWAL";
    @VisibleForTesting
    static final String SET_REMIND_ME_LATER = "SET_REMIND_ME_LATER";
    @Inject
    private MembershipService membershipService;
    @Inject
    private MetricsHandler metricsHandler;

    public void enableAutoRenewal(long membershipId) {
        UpdateMembershipRequest updateMembershipRequest = buildUpdateMembershipRequest(membershipId, ENABLE_AUTO_RENEWAL);
        metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_UPDATE_ENABLE_RENEWAL_STATUS);
        updateMembership(updateMembershipRequest);
    }

    public void disableAutoRenewal(long membershipId) {
        UpdateMembershipRequest updateMembershipRequest = buildUpdateMembershipRequest(membershipId, DISABLE_AUTO_RENEWAL);
        metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_UPDATE_DISABLE_RENEWAL_STATUS);
        updateMembership(updateMembershipRequest);
    }

    public void setRemindMeLater(long membershipId) {
        UpdateMembershipRequest updateMembershipRequest = buildUpdateMembershipRequest(membershipId, SET_REMIND_ME_LATER);
        updateMembershipRequest.setRemindMeLater(true);
        metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_UPDATE_SET_REMINDER_LATER);
        updateMembership(updateMembershipRequest);
    }


    public String decryptToken(String token) {
        metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_DECRYPT_TOKEN);
        return membershipService.decrypt(token);
    }

    private UpdateMembershipRequest buildUpdateMembershipRequest(long membershipId, String operation) {
        UpdateMembershipRequest updateMembershipRequest = new UpdateMembershipRequest();
        updateMembershipRequest.setMembershipId(String.valueOf(membershipId));
        updateMembershipRequest.setOperation(operation);
        return updateMembershipRequest;
    }

    private void updateMembership(UpdateMembershipRequest updateMembershipRequest) {
        if (!membershipService.updateMembership(updateMembershipRequest)) {
            metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_UPDATE_FAILED);
            throw new ServiceException(String.format("Failed to execute operation %s", updateMembershipRequest.getOperation()), ServiceName.MEMBERSHIP_API);
        }
    }

    public boolean isEligibleForFreeTrial(FreeTrialCandidateRequest freeTrialCandidateRequest) {
        boolean isFreeTrialCandidate = membershipService.isEligibleForFreeTrial(freeTrialCandidateRequest);
        if (!isFreeTrialCandidate) {
            metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_FREE_TRIAL_ABUSER);
        }
        return isFreeTrialCandidate;
    }

}
