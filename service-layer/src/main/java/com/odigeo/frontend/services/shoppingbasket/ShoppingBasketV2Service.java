package com.odigeo.frontend.services.shoppingbasket;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.shoppingbasket.v2.model.Product;
import com.odigeo.shoppingbasket.v2.requests.CreateRequest;
import com.odigeo.shoppingbasket.v2.requests.ProductRequest;
import com.odigeo.shoppingbasket.v2.responses.BaseResponse;
import com.odigeo.shoppingbasket.v2.responses.ShoppingBasketResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;

import java.util.Objects;
import java.util.Optional;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class ShoppingBasketV2Service {

    private static final String CREATE_SHOPPINGBASKET_ERROR = "We couldn't create shoppingbasket";
    private static final String GET_SHOPPINGBASKET_ERROR = "We couldn't get shoppingbasket with id: ";
    private static final String ADD_PRODUCT_ERROR = "We couldn't add productId: ";
    private static final String REMOVE_PRODUCT_ERROR = "We couldn't remove productId: ";
    private static final String ERROR = "; Error: ";

    @Inject
    private ShoppingBasketServiceFactory shoppingBasketServiceFactory;

    public ShoppingBasketResponse create(CreateRequest createRequest, ResolverContext context) {
        ShoppingBasketResponse shoppingBasketResponse;

        try {
            CookieStore cookieStore = new BasicCookieStore();

            Pair<com.odigeo.shoppingbasket.v2.ShoppingBasketService, CookieStore> pair = getShoppingBasketApiService(cookieStore);
            com.odigeo.shoppingbasket.v2.ShoppingBasketService shoppingBasketApiService = pair.getLeft();
            shoppingBasketResponse = shoppingBasketApiService.create(createRequest);
            shoppingBasketServiceFactory.updateServiceCache(pair);

            Cookie sbJsessionidCookie = ShoppingBasketBrowserStorageHelper.getSbJsessionidCookie(pair.getRight());
            ShoppingBasketBrowserStorageHelper.putSbHeader(context, sbJsessionidCookie.getValue());
            ShoppingBasketBrowserStorageHelper.setSbCookie(context, sbJsessionidCookie.getValue());
        } catch (RuntimeException e) {
            throw new ServiceException(CREATE_SHOPPINGBASKET_ERROR, e, ServiceName.SHOPPING_BASKET_API);
        }
        validate(shoppingBasketResponse, CREATE_SHOPPINGBASKET_ERROR);

        return shoppingBasketResponse;
    }

    public ShoppingBasketResponse getShoppingBasket(long shoppingBasketId, ResolverContext context) {
        ShoppingBasketResponse shoppingBasketResponse;

        try {
            CookieStore cookieStore = ShoppingBasketBrowserStorageHelper.createShoppingBasketCookieStore(context);

            Pair<com.odigeo.shoppingbasket.v2.ShoppingBasketService, CookieStore> pair = getShoppingBasketApiService(cookieStore);
            com.odigeo.shoppingbasket.v2.ShoppingBasketService shoppingBasketApiService = pair.getLeft();
            shoppingBasketResponse = shoppingBasketApiService.getShoppingBasket(shoppingBasketId);
        } catch (RuntimeException e) {
            throw new ServiceException(GET_SHOPPINGBASKET_ERROR + shoppingBasketId, e, ServiceName.SHOPPING_BASKET_API);
        }
        validate(shoppingBasketResponse, GET_SHOPPINGBASKET_ERROR + shoppingBasketId);

        return shoppingBasketResponse;
    }

    public void addProduct(long shoppingBasketId, ProductRequest productRequest, ResolverContext context) {
        ShoppingBasketResponse shoppingBasketResponse;

        try {
            CookieStore cookieStore = ShoppingBasketBrowserStorageHelper.createShoppingBasketCookieStore(context);

            Pair<com.odigeo.shoppingbasket.v2.ShoppingBasketService, CookieStore> pair = getShoppingBasketApiService(cookieStore);
            com.odigeo.shoppingbasket.v2.ShoppingBasketService shoppingBasketApiService = pair.getLeft();
            shoppingBasketResponse = shoppingBasketApiService.addProduct(shoppingBasketId, productRequest);
        } catch (RuntimeException e) {
            throw new ServiceException(ADD_PRODUCT_ERROR + getProductId(productRequest), e, ServiceName.SHOPPING_BASKET_API);
        }
        validate(shoppingBasketResponse, ADD_PRODUCT_ERROR + getProductId(productRequest));
    }

    public void removeProduct(long shoppingBasketId, ProductRequest productRequest, ResolverContext context) {
        ShoppingBasketResponse shoppingBasketResponse;

        try {
            CookieStore cookieStore = ShoppingBasketBrowserStorageHelper.createShoppingBasketCookieStore(context);

            Pair<com.odigeo.shoppingbasket.v2.ShoppingBasketService, CookieStore> pair = getShoppingBasketApiService(cookieStore);
            com.odigeo.shoppingbasket.v2.ShoppingBasketService shoppingBasketApiService = pair.getLeft();
            shoppingBasketResponse = shoppingBasketApiService.removeProduct(shoppingBasketId, productRequest);
        } catch (RuntimeException e) {
            throw new ServiceException(REMOVE_PRODUCT_ERROR + getProductId(productRequest), e, ServiceName.SHOPPING_BASKET_API);
        }
        validate(shoppingBasketResponse, REMOVE_PRODUCT_ERROR + getProductId(productRequest));
    }

    private String getProductId(ProductRequest productRequest) {
        return Optional.ofNullable(productRequest.getProduct())
            .map(Product::getId)
            .orElse(null);
    }

    private void validate(ShoppingBasketResponse shoppingBasketResponse, String errorMessage) {
        String errorType = Optional.ofNullable(shoppingBasketResponse).map(BaseResponse::getErrorType).orElse(null);
        if (Objects.isNull(shoppingBasketResponse) || Objects.nonNull(errorType)) {
            throw new ServiceException(errorMessage + ERROR + errorType, ServiceName.SHOPPING_BASKET_API);
        }
    }

    private Pair<com.odigeo.shoppingbasket.v2.ShoppingBasketService, CookieStore> getShoppingBasketApiService(CookieStore cookieStore) {
        return shoppingBasketServiceFactory.getShoppingBasketService(cookieStore);
    }

}
