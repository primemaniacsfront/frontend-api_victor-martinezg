package com.odigeo.frontend.services.configurations.dapi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.DistributionApiConnectionFactory;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;

import java.net.MalformedURLException;

@Singleton
public class DapiConnectionFactory {

    @Inject
    private DapiConnectionConfig config;

    public DistributionApiConnectionFactory getDapiConnectionFactory() {
        try {
            return new DistributionApiConnectionFactory(config.getDapiHost(), config.isUseSecureForBooking());
        } catch (MalformedURLException e) {
            throw new ServiceException("Invalid configuration. Cannot create a connection for dapi", e, ServiceName.DAPI);
        }
    }
}
