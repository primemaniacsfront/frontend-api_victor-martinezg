package com.odigeo.frontend.services.checkout;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutStatus;
import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.QueryParamUtils;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutFactory;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutHandler;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeRequest;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeToken;
import org.apache.commons.lang3.StringUtils;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserInteractionNeededService {

    @VisibleForTesting
    static final String URL_REDIRECT = "/secure/apmReturn/";
    @VisibleForTesting
    static final String URL_REDIRECT_PROTOCOL = "https://";
    @VisibleForTesting
    static final String URL_REDIRECT_PSD2_OK = "/secure/3ds/";
    @VisibleForTesting
    static final String URL_REDIRECT_PSD2_KO = "/secure/3ds/ko/";
    @VisibleForTesting
    static final String URL_REDIRECT_PSD2_LOADING = "/secure/3ds/loading/";
    @VisibleForTesting
    static final String PAYMENT_INTERACTION_ID = "userPaymentInteractionId";
    @VisibleForTesting
    static final String CC_PAYMENT_METHOD = "creditcard";
    @VisibleForTesting
    static final String SHOPPING_ID = "shoppingid";
    @VisibleForTesting
    static final String CHECKOUT_STATUS = "status";
    @VisibleForTesting
    static final String TRIPS_REDIRECT_TOKEN = "myTripsRedirectionToken";
    @VisibleForTesting
    static final String CTX_PATH = "/travel";

    @Inject
    private CheckoutFactory checkoutFactory;
    @Inject
    private QueryParamUtils queryParamUtils;

    public String resume(Map<String, String> params, ResolverContext context, CheckoutResumeToken token, String serverName) {
        CheckoutHandler handler = checkoutFactory.getInstance(token.getShoppingType());
        CheckoutResumeRequest request = new CheckoutResumeRequest(token.getShoppingId(), token.getInteractionStep(), params, token.getUserPaymentInteractionId());
        CheckoutResponse response = handler.resume(request, context);
        String resumeRedirectUrl = buildResumeRedirectUrl(response, token);
        return URL_REDIRECT_PROTOCOL + serverName + CTX_PATH + resumeRedirectUrl;
    }

    private String buildResumeRedirectUrl(CheckoutResponse response, CheckoutResumeToken token) {
        String redirectPage = getRedirectPage(response.getStatusInfo().getStatus(), token.getPaymentMethod(), token.getInteractionStep()) + "?";
        Map<String, Object> queryParamsMap = getMapParams(response, token, redirectPage);
        return queryParamUtils.getQueryParamsString(queryParamsMap, redirectPage, StringUtils.EMPTY);
    }

    private Map<String, Object> getMapParams(CheckoutResponse response, CheckoutResumeToken token, String redirectPage) {
        if (redirectPage.contains(URL_REDIRECT_PSD2_KO)) {
            return buildQueryParamsMap(response.getShoppingCart().getBookingId(), StringUtils.EMPTY);
        } else {
            return token.getInteractionStep() == 1
                    ? buildQueryParamsMap(response.getShoppingCart().getBookingId(), token.getUserPaymentInteractionId())
                    : buildQueryParamsMap(response.getShoppingCart().getBookingId(), response.getStatusInfo().getStatus(), response.getShoppingCart().getBuyer().getMail());
        }
    }

    private Map<String, Object> buildQueryParamsMap(long shoppingId, String userPaymentInteractionId) {
        Map<String, Object> paramsMap = Stream.of(
                new AbstractMap.SimpleEntry<>(SHOPPING_ID, shoppingId)
        ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
        if (StringUtils.isNotEmpty(userPaymentInteractionId)) {
            paramsMap.put(PAYMENT_INTERACTION_ID, userPaymentInteractionId);
        }
        return paramsMap;
    }

    private Map<String, Object> buildQueryParamsMap(long shoppingId, CheckoutStatus status, String email) {
        return Stream.of(
                new AbstractMap.SimpleEntry<>(SHOPPING_ID, shoppingId),
                new AbstractMap.SimpleEntry<>(CHECKOUT_STATUS, status),
                new AbstractMap.SimpleEntry<>(TRIPS_REDIRECT_TOKEN, queryParamUtils.generateMyTripsRedirectionToken(shoppingId, email))
        ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
    }

    private String getRedirectPage(CheckoutStatus checkoutStatus, String paymentMethod, int step) {
        String psd2RedirectPage = CheckoutStatus.SUCCESS.equals(checkoutStatus)
                ? URL_REDIRECT_PSD2_OK
                : URL_REDIRECT_PSD2_KO;
        psd2RedirectPage = CheckoutStatus.USER_PAYMENT_INTERACTION.equals(checkoutStatus) && step == 2
                ? URL_REDIRECT_PSD2_LOADING
                : psd2RedirectPage;
        return paymentMethod.equalsIgnoreCase(CC_PAYMENT_METHOD) ? psd2RedirectPage : URL_REDIRECT;
    }

}
