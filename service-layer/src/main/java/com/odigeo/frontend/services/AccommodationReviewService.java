package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.accommodation.review.retrieve.AccommodationReviewRetrieverService;
import com.odigeo.accommodation.review.retrieve.bean.RequestDedups;
import com.odigeo.accommodation.review.retrieve.bean.Review;
import com.odigeo.frontend.commons.Logger;

import java.util.Collections;
import java.util.List;

@Singleton
public class AccommodationReviewService {
    @Inject
    private AccommodationReviewRetrieverService accommodationReviewRetrieverService;
    @Inject
    private Logger logger;

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public List<Review> getReviews(RequestDedups reviewsRequest) {
        try {
            return accommodationReviewRetrieverService.getReviews(reviewsRequest);
        } catch (RuntimeException e) {
            logger.error(this.getClass(), "Error retrieving reviews", e);
        }
        return Collections.emptyList();
    }
}
