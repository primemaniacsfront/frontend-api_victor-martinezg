package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.visitengineapi.v1.VisitEngineException;
import com.odigeo.visitengineapi.v1.multitest.MultitestException;
import com.odigeo.visitengineapi.v1.multitest.TestAssignment;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentNotFoundException;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentService;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Singleton
public class TestTokenService {

    public static final Integer PARTITION_A = 1;
    public static final Integer PARTITION_B = 2;
    public static final Integer PARTITION_C = 3;

    private static final Integer DEFAULT_PARTITION_NUMBER = 1;
    private static final Integer UNDEFINED_PARTITION_NUMBER = 0;

    @Inject
    private TestAssignmentService testAssignmentService;
    @Inject
    private Logger logger;

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public Integer find(String alias, VisitInformation visit) {
        try {
            TestAssignment token = testAssignmentService.getTestAssignment(alias);
            Map<String, Integer> dimensions = visit.getDimensionPartitionMap();

            return calculatePartition(token, dimensions);

        } catch (TestAssignmentNotFoundException | VisitEngineException | RuntimeException ex) {
            logger.warning(this.getClass(), "exception while requesting the alias: " + alias, ex);
            return DEFAULT_PARTITION_NUMBER;
        }
    }

    public List<Integer> findMultiple(List<String> aliases, VisitInformation visit) {
        try {
            Map<String, Integer> dimensions = visit.getDimensionPartitionMap();
            Map<String, TestAssignment> tokens = testAssignmentService.findAllTestAssignment().stream()
                .collect(Collectors.toMap(TestAssignment::getTestName, token -> token));

            return aliases.stream()
                .map(alias -> calculatePartition(tokens.get(alias), dimensions))
                .collect(Collectors.toList());

        } catch (MultitestException ex) {
            throw new ServiceException("error while requesting the list of aliases", ex, ServiceName.TEST_ASSIGMENT);
        }
    }

    private Integer calculatePartition(TestAssignment token, Map<String, Integer> dimensions) {
        Integer numberPartition = DEFAULT_PARTITION_NUMBER;

        if (token != null && token.isEnabled()) {
            numberPartition = token.getWinnerPartition() > UNDEFINED_PARTITION_NUMBER
                ? token.getWinnerPartition()
                : dimensions.get(token.getAssignedTestLabel());
        }

        return numberPartition;
    }

}
