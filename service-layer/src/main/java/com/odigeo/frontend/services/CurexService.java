package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.curex.client.currencyconverter.CurrencyConverterFactory;
import com.odigeo.curex.client.currencyconverter.DefaultCurrencyConverter;
import com.odigeo.curex.client.currencyconverter.exception.CurrencyConverterFactoryException;
import com.odigeo.frontend.commons.Logger;
import java.math.BigDecimal;

@Singleton
public class CurexService {

    @Inject
    private CurrencyConverterFactory converterFactory;
    @Inject
    private Logger logger;

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public BigDecimal getCurrencyConvertRate(String sourceCurrency, String destinationCurrency) {
        BigDecimal rate = null;

        try {
            DefaultCurrencyConverter defaultConverter = converterFactory.getCurrentCurrencyConverter();
            rate = defaultConverter.getConversionRate(sourceCurrency, destinationCurrency);

        } catch (CurrencyConverterFactoryException | RuntimeException ex) {
            logger.warning(this.getClass(), "error getting the conversion rate."
                + " source: " + sourceCurrency
                + " destination: " + destinationCurrency);
        }

        return rate;
    }

}
