package com.odigeo.frontend.services;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.ItineraryFootprintCalculatorService;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.exception.ItineraryFootprintCalculatorApiException;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.exception.RequestValidationException;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.request.ItineraryFootprintCalculatorRequest;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.response.ItineraryFootprintCalculatorResponse;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

@Singleton
public class CarbonFootprintService {

    @Inject
    private ItineraryFootprintCalculatorService footprintCalculatorService;
    @Inject
    private Logger logger;

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public ItineraryFootprintCalculatorResponse getCarbonFootprint(ItineraryFootprintCalculatorRequest itineraryFootprintCalculatorRequest) {
        ItineraryFootprintCalculatorResponse response = null;

        try {
            response = footprintCalculatorService.getFootprint(itineraryFootprintCalculatorRequest);
        } catch (ItineraryFootprintCalculatorApiException ex) {
            logger.warning(this.getClass(), "Error obtaining itinerary footprint calculator", ex);
        } catch (RequestValidationException ex) {
            logger.warning(this.getClass(), "Error validating request of itinerary-footprint-calculator module", ex);
        } catch (ConnectException ex) {
            logger.warning(this.getClass(), "ConnectException trying to connect to itinerary-footprint-calculator module", ex);
        } catch (SocketTimeoutException ex) {
            logger.warning(this.getClass(), "SocketTimeoutException waiting for itinerary-footprint-calculator results", ex);
        } catch (RuntimeException ex) {
            logger.warning(this.getClass(), "Other error obtaining itinerary footprint calculator", ex);
        }

        return response;
    }
}
