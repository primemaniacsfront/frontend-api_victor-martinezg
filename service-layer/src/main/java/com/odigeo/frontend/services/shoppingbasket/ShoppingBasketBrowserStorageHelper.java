package com.odigeo.frontend.services.shoppingbasket;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.ResolverContext;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.jboss.util.Strings;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

public final class ShoppingBasketBrowserStorageHelper {

    public static final String JSESSIONID = "JSESSIONID";

    private ShoppingBasketBrowserStorageHelper() {
    }

    public static void setSbCookie(ResolverContext context, String cookieValue) {
        Optional.ofNullable(context)
            .map(ResolverContext::getResponseInfo)
            .ifPresent(responseInfo -> responseInfo.addCookie(SiteCookies.SB_JSESSIONID, cookieValue));
    }

    public static void putSbHeader(ResolverContext context, String headerValue) {
        Optional.ofNullable(context)
            .map(ResolverContext::getResponseInfo)
            .ifPresent(responseInfo -> responseInfo.putHeader(SiteHeaders.SB_JSESSIONID, headerValue));
    }

    public static CookieStore createShoppingBasketCookieStore(ResolverContext context) {
        CookieStore cookieStore = new BasicCookieStore();

        String sessionValue = getSbJsessionIdValue(context);
        if (Objects.nonNull(sessionValue)) {
            cookieStore.addCookie(createShoppingBasketCookie(sessionValue));
        }

        return cookieStore;
    }

    private static String getSbJsessionIdValue(ResolverContext context) {
        return Optional.ofNullable(context)
            .map(ResolverContext::getRequestInfo)
            .map(requestInfo -> requestInfo.getHeaderOrCookie(SiteHeaders.SB_JSESSIONID, SiteCookies.SB_JSESSIONID))
            .orElse(null);
    }

    private static BasicClientCookie createShoppingBasketCookie(String cookieValue) {
        return new BasicClientCookie(JSESSIONID, cookieValue);
    }

    public static org.apache.http.cookie.Cookie getSbJsessionidCookie(CookieStore cookieStore) {
        return Optional.ofNullable(cookieStore)
            .map(CookieStore::getCookies)
            .orElse(Collections.emptyList())
            .stream()
            .filter(cookie -> JSESSIONID.equalsIgnoreCase(cookie.getName()))
            .findFirst()
            .orElse(new BasicClientCookie(JSESSIONID, Strings.EMPTY));
    }
}
