package com.odigeo.frontend.services.shoppingbasket;

import java.io.Serializable;
import java.util.Objects;

public class ShoppingBasketCacheKey implements Serializable {

    private String value;

    public ShoppingBasketCacheKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !getClass().equals(obj.getClass())) {
            return false;
        }

        ShoppingBasketCacheKey shoppingBasketCacheKey = (ShoppingBasketCacheKey) obj;
        return Objects.equals(value, shoppingBasketCacheKey.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
