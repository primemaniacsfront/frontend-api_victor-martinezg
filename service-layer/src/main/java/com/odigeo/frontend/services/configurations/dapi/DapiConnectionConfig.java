package com.odigeo.frontend.services.configurations.dapi;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class DapiConnectionConfig {
    private String dapiHost;
    private boolean useSecureForBooking;
    private String credentialId;
    private String password;

    public String getDapiHost() {
        return dapiHost;
    }

    public void setDapiHost(String dapiHost) {
        this.dapiHost = dapiHost;
    }

    public boolean isUseSecureForBooking() {
        return useSecureForBooking;
    }

    public void setUseSecureForBooking(boolean useSecureForBooking) {
        this.useSecureForBooking = useSecureForBooking;
    }

    public String getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(String credentialId) {
        this.credentialId = credentialId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
