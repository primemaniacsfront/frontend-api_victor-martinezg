package com.odigeo.frontend.services.insurance;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.insurance.api.last.InsuranceApiServiceException;
import com.odigeo.insurance.api.last.UnexpectedInsuranceApiServiceException;
import com.odigeo.insurance.api.last.insurance.InsuranceOffers;
import com.odigeo.insurance.api.last.request.InvalidParametersException;
import com.odigeo.insurance.api.last.request.PostBookingOfferRequest;
import java.util.Objects;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class InsuranceService {

    private static final String GET_OFFERS_ERROR = "We couldn't get offers ";
    private static final String FOR_BOOKING_ID = "for bookingId: ";
    private static final String FOR_SHOPPING_BASKET_ID = "for shoppingBasketId: ";
    private static final String WITHOUT_IDENTIFIER = "without identifier";

    @Inject
    private com.odigeo.insurance.api.last.InsuranceApiService insuranceApiService;

    public InsuranceOffers getOffers(PostBookingOfferRequest postBookingOfferRequest) {
        try {
            return insuranceApiService.getOffers(postBookingOfferRequest);
        } catch (InsuranceApiServiceException | InvalidParametersException | UnexpectedInsuranceApiServiceException | RuntimeException e) {
            throw new ServiceException(GET_OFFERS_ERROR + getErrorMessage(postBookingOfferRequest), e, ServiceName.INSURANCE_API);
        }
    }

    private String getErrorMessage(PostBookingOfferRequest postBookingOfferRequest) {
        return Objects.nonNull(postBookingOfferRequest.getBookingId())
            ? getMessageForBookingId(postBookingOfferRequest)
            : getMessageForNoBookingId(postBookingOfferRequest);
    }

    private String getMessageForNoBookingId(PostBookingOfferRequest postBookingOfferRequest) {
        return Objects.nonNull(postBookingOfferRequest.getShoppingBasketId())
            ? getMessageForShoppingBasketId(postBookingOfferRequest)
            : WITHOUT_IDENTIFIER;
    }

    private String getMessageForShoppingBasketId(PostBookingOfferRequest postBookingOfferRequest) {
        return FOR_SHOPPING_BASKET_ID + postBookingOfferRequest.getShoppingBasketId();
    }

    private String getMessageForBookingId(PostBookingOfferRequest postBookingOfferRequest) {
        return FOR_BOOKING_ID + postBookingOfferRequest.getBookingId();
    }
}
