package com.odigeo.frontend.services.configurations.rsa;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

@Singleton
@ConfiguredInPropertiesFile
public class RsaKeyConfig {

    public static final String RSA = "RSA";
    private String exponentD;
    private String modulusD;
    private String exponentE;
    private String modulusE;
    private PrivateKey rsaPrivateKey;
    private PublicKey rsaPublicKey;

    public void setExponentD(String exponentD) {
        this.exponentD = exponentD;
    }

    public void setModulusD(String modulusD) {
        this.modulusD = modulusD;
    }

    public void setExponentE(String exponentE) {
        this.exponentE = exponentE;
    }
    public void setModulusE(String modulusE) {
        this.modulusE = modulusE;
    }

    public PrivateKey getPrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        if (rsaPrivateKey == null) {
            rsaPrivateKey = KeyFactory.getInstance(RSA).generatePrivate(new RSAPrivateKeySpec(
                    new BigInteger(modulusD), new BigInteger(exponentD)));
        }
        return rsaPrivateKey;
    }

    public PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        if (rsaPublicKey == null) {
            rsaPublicKey = KeyFactory.getInstance(RSA).generatePublic(new RSAPublicKeySpec(
                    new BigInteger(modulusE),
                    new BigInteger(exponentE)));
        }
        return rsaPublicKey;
    }
}
