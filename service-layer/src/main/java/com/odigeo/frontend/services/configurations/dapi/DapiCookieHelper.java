package com.odigeo.frontend.services.configurations.dapi;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import java.io.Serializable;
import java.util.Date;

public class DapiCookieHelper implements Serializable {
    private Date expirationDate;
    private String nameAndValue;
    private String name;
    private String value;
    private String path;
    private String domain;
    private boolean isSecure;

    public DapiCookieHelper(Cookie cookie) {
        int cookieAge = cookie.getMaxAge();
        // If cookie age is -1, it means it is a session cookie.
        if (cookieAge >= 0) {
            this.expirationDate = new Date(System.currentTimeMillis() + 1000L * cookie.getMaxAge());
        }
        this.name = cookie.getName();
        this.value = cookie.getValue();
        this.nameAndValue = name + "=" + value;
        this.path = StringUtils.defaultString(cookie.getPath(), "/");
        this.domain = cookie.getDomain();
        this.isSecure = cookie.getSecure();
    }

    public Date getExpirationDate() {
        return this.expirationDate != null ? new Date(this.expirationDate.getTime()) : null;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = new Date(expirationDate.getTime());
    }

    public String getNameAndValue() {
        return nameAndValue;
    }

    public void setNameAndValue(String nameAndValue) {
        this.nameAndValue = nameAndValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean isSecure() {
        return isSecure;
    }

    public void setSecure(boolean secure) {
        isSecure = secure;
    }
}
