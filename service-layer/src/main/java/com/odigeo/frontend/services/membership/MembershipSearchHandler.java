package com.odigeo.frontend.services.membership;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.prime.handlers.MembershipSearchRequestHandler;
import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MembershipResponse;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Singleton
public class MembershipSearchHandler {
    @Inject
    private MetricsHandler metricsHandler;
    @Inject
    private MembershipSearchApi membershipSearchApi;
    @Inject
    private MembershipSearchRequestHandler membershipSearchRequestHandler;

    public MembershipResponse getCurrentMembership(Long userId, String websiteCode) {
        metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_GET_CURRENT_MEMBERSHIP);
        return getCurrentMembership(membershipSearchRequestHandler.build(userId, websiteCode));
    }

    public MembershipResponse getCurrentMembership(String email, String websiteCode) {
        metricsHandler.trackCounter(MeasureConstants.MEMBERSHIP_GET_CURRENT_MEMBERSHIP);
        return getCurrentMembership(membershipSearchRequestHandler.build(email, websiteCode));
    }

    private MembershipResponse getCurrentMembership(MembershipSearchRequest membershipSearchRequest) {
        List<MembershipResponse> membershipResponseList = searchMemberships(membershipSearchRequest);
        return getFirstMembershipFromList(membershipResponseList);
    }

    private List<MembershipResponse> searchMemberships(MembershipSearchRequest membershipSearchRequest) {
        try {
            return membershipSearchApi.searchMemberships(membershipSearchRequest);
        } catch (MembershipServiceException e) {
            throw new ServiceException("Failed to searchMembership", e, ServiceName.MEMBERSHIP_API);
        }
    }

    private MembershipResponse getFirstMembershipFromList(List<MembershipResponse> membershipResponseList) {
        return Optional.ofNullable(membershipResponseList)
                .map(Collection::stream)
                .flatMap(Stream::findFirst)
                .orElse(null);
    }
}
