package com.odigeo.frontend.services.ancillary;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;

import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.itineraryapi.v1.request.SelectAncillaryRequest;
import com.odigeo.itineraryapi.v1.response.SelectAncillaryServiceResponse;

import com.odigeo.itineraryapi.v1.InvalidParametersException;
import com.odigeo.itineraryapi.v1.ItineraryApiService;
import com.odigeo.itineraryapi.v1.request.GetAncillaryRequest;
import com.odigeo.itineraryapi.v1.response.AvailableAncillariesOptionsResponse;
import com.odigeo.itineraryapi.v1.request.AncillariesServicesType;

import java.util.Set;

@Singleton
public class AncillaryService {

    @Inject
    private ItineraryApiService itineraryApiService;

    public AvailableAncillariesOptionsResponse ancillariesOptions(long bookingID,
                                                                  Set<AncillariesServicesType> services,
                                                                  ResolverContext context) {
        try {
            GetAncillaryRequest request = new GetAncillaryRequest();
            request.setVisitId(context.getVisitInformation().getVisitId().toString());
            request.setVisitInformation(context.getVisitInformation().getVisitCode());
            request.setQaMode(context.getDebugInfo().isEnabled());
            request.setServices(services);
            return itineraryApiService.getAncillaryServiceOptions(bookingID, request).getResponse();
        } catch (InvalidParametersException ex) {
            throw new ServiceException("Error in ancillariesOptions", ex, ServiceName.ITINERARY_API);
        }
    }

    public SelectAncillaryServiceResponse selectAncillaries(long bookingID,
                                                            SelectAncillaryRequest request) {
        try {
            return itineraryApiService.selectAncillaryService(bookingID, request).getResponse();
        } catch (InvalidParametersException ex) {
            throw new ServiceException("Error in selectAncillaries", ex, ServiceName.ITINERARY_API);
        }
    }

}
