package com.odigeo.frontend.services.insuranceproduct;

import com.edreamsodigeo.insuranceproduct.api.last.InsuranceProductServiceException;
import com.edreamsodigeo.insuranceproduct.api.last.request.OfferId;
import com.edreamsodigeo.insuranceproduct.api.last.request.SelectRequest;
import com.edreamsodigeo.insuranceproduct.api.last.response.InsuranceProduct;
import com.edreamsodigeo.insuranceproduct.api.last.response.ProductId;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.product.v2.exception.ProductException;
import java.util.Optional;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class InsuranceProductService {

    private static final String SELECT_OFFER_ERROR = "We couldn't select offer: ";
    private static final String GET_INSURANCE_PRODUCT_ERROR = "We couldn't get the insurance product with id: ";

    @Inject
    private com.edreamsodigeo.insuranceproduct.api.last.InsuranceProductService insuranceProductApiService;

    public ProductId selectOffer(SelectRequest selectRequest) {
        try {
            return insuranceProductApiService.select(selectRequest);
        } catch (InsuranceProductServiceException | RuntimeException e) {
            throw new ServiceException(SELECT_OFFER_ERROR + getOfferId(selectRequest), e, ServiceName.INSURANCE_PRODUCT_API);
        }
    }

    public InsuranceProduct getInsuranceProduct(String productId) {
        try {
            return insuranceProductApiService.getProduct(productId);
        } catch (ProductException | RuntimeException e) {
            throw new ServiceException(GET_INSURANCE_PRODUCT_ERROR + productId, e, ServiceName.INSURANCE_PRODUCT_API);
        }
    }

    private String getOfferId(SelectRequest selectRequest) {
        return Optional.ofNullable(selectRequest.getOfferId())
                .map(OfferId::getId)
                .orElse(null);
    }
}
