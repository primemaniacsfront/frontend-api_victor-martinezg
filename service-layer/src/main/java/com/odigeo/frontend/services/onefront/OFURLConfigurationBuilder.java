package com.odigeo.frontend.services.onefront;

import com.google.inject.Singleton;
import com.odigeo.commons.rest.configuration.URLConfiguration;
import com.odigeo.commons.rest.configuration.URLConfigurationLoader;
import com.odigeo.frontend.commons.context.RequestInfo;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;

@Singleton
public class OFURLConfigurationBuilder {

    private static final String MY_LOCAL_ENVIRONMENT_HOST = "mylocalenvironment.com";
    private static final int MY_LOCAL_ENVIRONMENT_PORT = 8080;
    private static final String OF2_LOCAL_ENVIRONMENT_HOST = ".local";
    private static final String OF2_LOCAL_ENVIRONMENT_PROTOCOL = "https";
    private static final int OF2_LOCAL_ENVIRONMENT_PORT = 80;

    public <Service> URLConfiguration build(Class<Service> clazz, RequestInfo requestInfo)  {
        try {
            URLConfiguration urlConfiguration = (new URLConfigurationLoader()).getURLConfiguration(clazz);

            URIBuilder uriBuilder = new URIBuilder(requestInfo.getRequestUrl());
            if (MY_LOCAL_ENVIRONMENT_HOST.equals(uriBuilder.getHost())) {
                urlConfiguration.setHost(uriBuilder.getHost());
                uriBuilder.setPort(MY_LOCAL_ENVIRONMENT_PORT);
            } else if (uriBuilder.getHost().contains(OF2_LOCAL_ENVIRONMENT_HOST)) {
                urlConfiguration.setHost(uriBuilder.getHost());
                urlConfiguration.setProtocol(OF2_LOCAL_ENVIRONMENT_PROTOCOL);
                urlConfiguration.setPort(OF2_LOCAL_ENVIRONMENT_PORT);
            }

            return urlConfiguration;

        } catch (IOException | InvocationTargetException | IllegalAccessException | URISyntaxException ex) {
            return null;
        }
    }

}
