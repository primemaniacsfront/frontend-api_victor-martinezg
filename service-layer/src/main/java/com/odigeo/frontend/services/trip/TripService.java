package com.odigeo.frontend.services.trip;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.itineraryapi.v1.InvalidParametersException;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import com.odigeo.travelcompanion.v2.model.booking.TripDetailsMode;
import com.odigeo.travelcompanion.v2.model.user.Brand;

import java.util.Locale;

@Singleton
public class TripService {

    @Inject
    private com.odigeo.travelcompanion.v2.TripService tripService;

    public BookingDetail getTrip(Long bookingId, Locale locale, String email, Brand brand) {
        try {
            return tripService.getTrip(bookingId, locale, email, brand, TripDetailsMode.ENRICHED);
        } catch (InvalidParametersException ex) {
            throw new ServiceException("Error in getBookingDetail", ex, ServiceName.TRIP_API);
        }
    }
}
