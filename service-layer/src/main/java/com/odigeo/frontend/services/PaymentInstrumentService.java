package com.odigeo.frontend.services;

import com.edreamsodigeo.payments.paymentinstrument.contract.PaymentInstrumentResourceException;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScope;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScopeResource;
import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentToken;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;

@Singleton
public class PaymentInstrumentService {

    private static final String GET_CC_PCI_SCOPE_ERROR = "Error get credit card PCI info.";
    private static final String TOKEN_INFO = " Token value ";
    private static final String TOKENIZE_ERROR = "Error tokenizing";

    @Inject
    private CreditCardPciScopeResource creditCardPciScopeResource;

    public CreditCardPciScope getCreditCardFromPaymentInstrumentToken(String paymentInstrumentToken) {
        try {
            PaymentInstrumentToken token = PaymentInstrumentToken.valueOf(paymentInstrumentToken);
            return creditCardPciScopeResource.getPciScope(token);
        } catch (PaymentInstrumentResourceException e) {
            throw new ServiceException(GET_CC_PCI_SCOPE_ERROR + TOKEN_INFO + paymentInstrumentToken, e, ServiceName.PAYMENT_INSTRUMENT);
        }
    }

    public PaymentInstrumentToken tokenize(String number, String holder, String expirationMonth, String expirationYear) {
        try {
            CreditCardPciScope creditCardPciScope = buildCreditCardPciScope(number, holder, expirationMonth, expirationYear);
            return creditCardPciScopeResource.tokenize(creditCardPciScope);
        } catch (PaymentInstrumentResourceException e) {
            throw new ServiceException(TOKENIZE_ERROR, e, ServiceName.PAYMENT_INSTRUMENT);
        }
    }

    private CreditCardPciScope buildCreditCardPciScope(String number, String holder, String expirationMonth, String expirationYear) {
        CreditCardPciScope creditCardPciScope = new CreditCardPciScope();
        creditCardPciScope.setNumber(number);
        creditCardPciScope.setHolder(holder);
        creditCardPciScope.setExpirationMonth(expirationMonth);
        creditCardPciScope.setExpirationYear(expirationYear);
        return creditCardPciScope;
    }

}
