package com.odigeo.frontend.services.configurations.dapi;

import com.odigeo.dapi.client.SerializableCookie;
import com.odigeo.dapi.client.cookies.HttpRequestCookies;

import java.util.Collection;

public class CollectionBasedHttpRequestCookies implements HttpRequestCookies {
    final Collection<SerializableCookie> cookies;

    public CollectionBasedHttpRequestCookies(Collection<SerializableCookie> cookies) {
        this.cookies = cookies;
    }

    @Override
    public Collection<SerializableCookie> getCookies() {
        return cookies;
    }
}
