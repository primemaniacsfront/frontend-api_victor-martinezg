package com.odigeo.frontend.services.configurations.dapi;

import com.odigeo.dapi.client.SerializableCookie;
import com.odigeo.dapi.client.cookies.HttpResponseCookies;

import java.util.Collection;

public class CollectionBasedHttpResponseCookies implements HttpResponseCookies {
    final Collection<SerializableCookie> cookies;

    public CollectionBasedHttpResponseCookies(Collection<SerializableCookie> cookies) {
        this.cookies = cookies;
    }

    @Override
    public void addCookie(SerializableCookie serializableCookie) {
        cookies.add(serializableCookie);
    }
}
