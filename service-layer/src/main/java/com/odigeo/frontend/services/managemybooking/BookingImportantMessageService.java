package com.odigeo.frontend.services.managemybooking;

import com.edreamsodigeo.bookingimportantmessage.contract.v1.BookingImportantMessageResource;
import com.edreamsodigeo.bookingimportantmessage.contract.v1.exception.BookingImportantMessageResourceException;
import com.edreamsodigeo.bookingimportantmessage.contract.v1.exception.BookingImportantMessageValidationException;
import com.edreamsodigeo.bookingimportantmessage.contract.v1.model.Message;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

@Singleton
public class BookingImportantMessageService {

    private static final Logger logger = Logger.getLogger(BookingImportantMessageService.class);

    @Inject
    private BookingImportantMessageResource bookingImportantMessageResource;

    public List<Message> getImportantMessagesByBookingIdAndEmail(Long bookingId, String email) {
        try {
            return bookingImportantMessageResource.getImportantMessagesByBookingIdAndEmail(bookingId, email);
        } catch (BookingImportantMessageResourceException | BookingImportantMessageValidationException e) {
            logger.error(e.getMessage());
            return Collections.emptyList();
        }
    }

    public List<Message> getImportantMessagesByToken(String token) {
        try {
            return bookingImportantMessageResource.getImportantMessagesByToken(token);
        } catch (BookingImportantMessageResourceException | BookingImportantMessageValidationException e) {
            logger.error(e.getMessage());
            return Collections.emptyList();
        }
    }

}
