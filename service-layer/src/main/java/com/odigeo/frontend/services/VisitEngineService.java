package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.visitengineapi.v1.VisitEngineException;
import com.odigeo.visitengineapi.v1.client.autonomous.VisitEngineClient;
import com.odigeo.visitengineapi.v1.request.InvalidParametersException;
import com.odigeo.visitengineapi.v1.response.VisitResponse;

@Singleton
public class VisitEngineService {

    @Inject
    private VisitEngineClient visitEngineClient;

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public VisitResponse deserialize(String visit) {
        try {
            return visitEngineClient.buildVisit(visit);
        } catch (VisitEngineException | InvalidParametersException | RuntimeException ex) {
            return null;
        }
    }

}
