package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.itineraryapi.v1.InvalidParametersException;
import com.odigeo.itineraryapi.v1.ItineraryApiService;
import com.odigeo.itineraryapi.v1.response.RetrieveFareFamiliesResponse;

@Singleton
public class ItineraryService {

    @Inject
    private ItineraryApiService itineraryApiService;

    public RetrieveFareFamiliesResponse retrieveFareFamilies(long searchId, int fareItineraryIndex, String segmentKeys) {
        try {
            return itineraryApiService.retrieveFareFamilies(searchId, fareItineraryIndex, segmentKeys).getResponse();

        } catch (InvalidParametersException ex) {
            throw new ServiceException("Error in retrieveFareFamilies", ex, ServiceName.ITINERARY_API);
        }
    }

}
