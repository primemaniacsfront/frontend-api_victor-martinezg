package com.odigeo.frontend.services;

import com.edreamsodigeo.payments.customercreditcardbinconfig.contract.CreditCardBin;
import com.edreamsodigeo.payments.customercreditcardbinconfig.contract.CustomerCreditCardBinConfigResource;
import com.edreamsodigeo.payments.customercreditcardbinconfig.contract.CustomerCreditCardBinConfigResourceException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;

@Singleton
public class CustomerCreditCardBinSecurityService {

    @Inject
    private Logger logger;
    @Inject
    private CustomerCreditCardBinConfigResource customerCreditCardBinConfigResource;
    @Inject
    private PrimeService primeService;

    public boolean isCreditCardBlockedForSubscription(String binNumber, VisitInformation visitInformation) {
        return isPrimeMarket(visitInformation) ? isPrimeBlockedCard(binNumber) : Boolean.FALSE;
    }

    private boolean isPrimeBlockedCard(String binNumber) {
        try {
            return customerCreditCardBinConfigResource.isBlocked(CreditCardBin.valueOf(binNumber.substring(0, customerCreditCardBinConfigResource.getLength())));
        } catch (CustomerCreditCardBinConfigResourceException e) {
            logger.error(this.getClass(), "Error calling BIN blocked service", e);
            return Boolean.FALSE;
        }
    }

    private boolean isPrimeMarket(VisitInformation visitInformation) {
        return primeService.isPrimeSite(visitInformation);
    }

}
