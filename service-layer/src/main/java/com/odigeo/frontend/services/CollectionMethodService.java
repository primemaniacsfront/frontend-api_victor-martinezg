package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.collectionmethod.v3.CollectionMethodApiService;
import com.odigeo.collectionmethod.v3.CreditCardBinDetails;
import com.odigeo.collectionmethod.v3.InvalidParametersException;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;

import java.lang.reflect.UndeclaredThrowableException;

@Singleton
public class CollectionMethodService {

    @Inject
    private CollectionMethodApiService collectionMethodApiService;

    public CreditCardBinDetails getCreditCardBinDetails(String bin) {
        CreditCardBinDetails binDetails;

        try {
            binDetails = collectionMethodApiService.getCreditCardBinDetails(bin);
        } catch (InvalidParametersException | UndeclaredThrowableException e) {
            throw new ServiceException("Error Collection Method Api Service with bin " + bin + ", " + e.getMessage(), e, ServiceName.COLLECTION_METHOD_API);
        }

        return binDetails;
    }

}
