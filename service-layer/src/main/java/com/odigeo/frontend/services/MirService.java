package com.odigeo.frontend.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.MeItineraryRatingService;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.exception.MeItineraryRatingApiException;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.exception.RequestValidationException;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.request.ItineraryRatingRequest;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.response.ItineraryRatingResponse;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

@Singleton
public class MirService {

    @Inject
    private MeItineraryRatingService ratingService;
    @Inject
    private Logger logger;

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public ItineraryRatingResponse getRatings(ItineraryRatingRequest itineraryRatingRequest) {
        ItineraryRatingResponse response = null;

        try {
            response = ratingService.getRatings(itineraryRatingRequest);
        } catch (MeItineraryRatingApiException ex) {
            logger.warning(this.getClass(), "Error obtaining itinerary ratings", ex);
        } catch (RequestValidationException ex) {
            logger.warning(this.getClass(), "Error validating request of me-itinerary-rating module", ex);
        } catch (ConnectException ex) {
            logger.warning(this.getClass(), "ConnectException trying to connect to me-itinerary-rating module", ex);
        } catch (SocketTimeoutException ex) {
            logger.warning(this.getClass(), "SocketTimeoutException waiting for me-itinerary-rating results", ex);
        } catch (UndeclaredThrowableException ex) {
            logger.warning(this.getClass(), "Undeclared exception from me-itinerary-rating", ex);
        } catch (RuntimeException ex) {
            logger.warning(this.getClass(), "Other error obtaining itinerary ratings", ex);
        }

        return response;
    }

}
