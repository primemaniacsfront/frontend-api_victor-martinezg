package com.odigeo.frontend.services.shoppingbasket;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/shoppingbasket/v2")
@Consumes({"application/json;charset=UTF-8", "application/json"})
@Produces({"application/json;charset=UTF-8", "application/json"})
public interface ShoppingBasketApiV2 extends com.odigeo.shoppingbasket.v2.ShoppingBasketService {
}
