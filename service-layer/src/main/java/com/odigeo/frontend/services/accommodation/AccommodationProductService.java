package com.odigeo.frontend.services.accommodation;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationBuyer;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.accommodation.product.model.AvailabilityId;
import com.odigeo.accommodation.product.model.CreateProduct;
import com.odigeo.accommodation.product.model.Customer;
import com.odigeo.accommodation.product.model.ProductId;

import java.time.LocalDate;

@Singleton
public class AccommodationProductService {

    @Inject
    private com.odigeo.accommodation.product.AccommodationProductService accommodationProductService;


    public String createProduct(String offerId, AccommodationBuyer buyer) {
        AvailabilityId availabilityId = AvailabilityId.builder().id(offerId).build();
        Customer customer = Customer.builder()
                .firstName(buyer.getName())
                .lastName(buyer.getLastNames())
                .birthDate(LocalDate.of(buyer.getYearOfBirth(), buyer.getMonthOfBirth(), buyer.getDayOfBirth()))
                .email(buyer.getMail())
                .phoneNumber(buyer.getPhoneNumber())
                .prefixPhoneNumber(buyer.getCountryPhoneNumber())
                .invoiceDate(LocalDate.now())
                .build();

        CreateProduct createProductRequest = CreateProduct.builder().availabilityId(availabilityId).buyer(customer).build();
        ProductId product = accommodationProductService.createProduct(createProductRequest);
        return product.getId().toString();
    }
}
