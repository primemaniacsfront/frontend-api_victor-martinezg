package com.odigeo.frontend.services.checkout;

import com.edreamsodigeo.collections.userpaymentinteraction.contract.UserPaymentInteractionResource;
import com.edreamsodigeo.collections.userpaymentinteraction.contract.exceptions.ResourceNotFoundException;
import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteraction;
import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteractionId;
import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteractionOutcome;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.dapi.client.UserInteractionNeededResponse;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.resolvers.checkout.mappers.UserPaymentInteractionMapper;

import java.util.Map;

@Singleton
public class UserPaymentInteractionService {

    @Inject
    private UserPaymentInteractionResource userPaymentInteractionResource;
    @Inject
    private UserPaymentInteractionMapper userPaymentInteractionMapper;

    public UserPaymentInteractionId createUserPaymentInteraction(UserInteractionNeededResponse userInteractionData) {
        UserPaymentInteraction userPaymentInteraction = userPaymentInteractionMapper.mapFromDapi(userInteractionData);
        return userPaymentInteractionResource.creation(userPaymentInteraction);
    }

    public void updateOutcomeUserPaymentInteraction(String userPaymentInteractionId, UserInteractionNeededResponse.Parameters parameters) {
        updateOutcomeUserPaymentInteraction(userPaymentInteractionId, userPaymentInteractionMapper.mapParameters(parameters));
    }

    public void updateOutcomeUserPaymentInteraction(String userPaymentInteractionId, Map<String, String> params) {
        UserPaymentInteractionId id = new UserPaymentInteractionId(userPaymentInteractionId);
        UserPaymentInteractionOutcome outcome = new UserPaymentInteractionOutcome();
        outcome.setValues(params);
        userPaymentInteractionResource.creationOutcome(id, outcome);
    }

    public com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteraction retrieveUserPaymentInteraction(UserPaymentInteractionId userPaymentInteractionId) {
        try {
            return userPaymentInteractionMapper.mapUserPaymentInteractionToContract(userPaymentInteractionResource.retrieve(userPaymentInteractionId));
        } catch (ResourceNotFoundException e) {
            throw new ServiceException("Error retrieving UserPaymentInteraction", e, ServiceName.USER_PAYMENT_INTERACTION);
        }
    }

}
