package com.odigeo.frontend.contract.itinerary.segment;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageCondition;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;

import java.util.List;
import java.util.Set;

public class SegmentDTO {

    private Integer id;
    private Carrier carrier;
    private List<SectionDTO> sections;
    private BaggageCondition baggageCondition;
    private Long duration;
    private Integer seats;
    private Set<TransportType> transportTypes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<SectionDTO> getSections() {
        return sections;
    }

    public void setSections(List<SectionDTO> sections) {
        this.sections = sections;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public BaggageCondition getBaggageCondition() {
        return baggageCondition;
    }

    public void setBaggageCondition(BaggageCondition baggageCondition) {
        this.baggageCondition = baggageCondition;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Set<TransportType> getTransportTypes() {
        return transportTypes;
    }

    public void setTransportTypes(Set<TransportType> transportTypes) {
        this.transportTypes = transportTypes;
    }

}
