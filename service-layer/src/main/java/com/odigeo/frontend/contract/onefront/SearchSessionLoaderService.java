package com.odigeo.frontend.contract.onefront;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/service/frontendapi")
public interface SearchSessionLoaderService {
    String JSON_MIME_TYPE = "application/json; charset=UTF-8";

    @POST
    @Path("/loadSearchInfoInSession")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    void loadSearchInfoInSession(String searchResponseJson);

    @POST
    @Path("/loadSearchMetaInfoInSession")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    void loadSearchMetaInfoInSession(String searchResponseJson,
                                            @HeaderParam("referer") String paramReferer);


}
