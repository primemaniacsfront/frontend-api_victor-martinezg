package com.odigeo.frontend.contract.itinerary.leg;

import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;

import java.util.List;

public class LegDTO {

    private List<SegmentDTO> segments;

    @Deprecated //onefront integration
    private List<Integer> segmentKeys;

    public List<SegmentDTO> getSegments() {
        return segments;
    }

    public void setSegments(List<SegmentDTO> segments) {
        this.segments = segments;
    }

    public List<Integer> getSegmentKeys() {
        return segmentKeys;
    }

    public void setSegmentKeys(List<Integer> segmentKeys) {
        this.segmentKeys = segmentKeys;
    }

}
