package com.odigeo.frontend.contract.location;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;

public class LocationDTO extends Location {

    private String countryCode;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

}
