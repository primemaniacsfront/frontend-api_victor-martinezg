package com.odigeo.frontend.contract.itinerary.section;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.odigeo.frontend.contract.ancillaries.InsuranceOfferDTO;
import com.odigeo.frontend.contract.location.LocationDTO;

import java.time.ZonedDateTime;
import java.util.List;

public class SectionDTO {

    private String id;
    private String key;
    private ZonedDateTime departureDate;
    private ZonedDateTime arrivalDate;
    private LocationDTO departure;
    private LocationDTO destination;
    private Carrier carrier;
    private Carrier operatingCarrier;
    private Enum cabinClass;
    private String flightCode;
    private String arrivalTerminal;
    private String departureTerminal;
    private List<TechnicalStop> technicalStops;
    private Integer baggageAllowance;
    private String vehicleModel;
    private TransportType transportType;
    private InsuranceOfferDTO insuranceOffer;
    private Long duration;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ZonedDateTime getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(ZonedDateTime departureDate) {
        this.departureDate = departureDate;
    }

    public ZonedDateTime getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(ZonedDateTime arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public LocationDTO getDeparture() {
        return departure;
    }

    public void setDeparture(LocationDTO departure) {
        this.departure = departure;
    }

    public LocationDTO getDestination() {
        return destination;
    }

    public void setDestination(LocationDTO destination) {
        this.destination = destination;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public Carrier getOperatingCarrier() {
        return operatingCarrier;
    }

    public void setOperatingCarrier(Carrier operatingCarrier) {
        this.operatingCarrier = operatingCarrier;
    }

    public Enum getCabinClass() {
        return cabinClass;
    }

    public void setCabinClass(Enum cabinClass) {
        this.cabinClass = cabinClass;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public String getArrivalTerminal() {
        return arrivalTerminal;
    }

    public void setArrivalTerminal(String arrivalTerminal) {
        this.arrivalTerminal = arrivalTerminal;
    }

    public String getDepartureTerminal() {
        return this.departureTerminal;
    }

    public void setDepartureTerminal(String departureTerminal) {
        this.departureTerminal = departureTerminal;
    }

    public List<TechnicalStop> getTechnicalStops() {
        return technicalStops;
    }

    public void setTechnicalStops(List<TechnicalStop> technicalStops) {
        this.technicalStops = technicalStops;
    }

    public Integer getBaggageAllowance() {
        return baggageAllowance;
    }

    public void setBaggageAllowance(Integer baggageAllowance) {
        this.baggageAllowance = baggageAllowance;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public TransportType getTransportType() {
        return transportType;
    }

    public void setTransportType(TransportType transportType) {
        this.transportType = transportType;
    }

    public InsuranceOfferDTO getInsuranceOffer() {
        return insuranceOffer;
    }

    public void setInsuranceOffer(InsuranceOfferDTO insuranceOffer) {
        this.insuranceOffer = insuranceOffer;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }
}
