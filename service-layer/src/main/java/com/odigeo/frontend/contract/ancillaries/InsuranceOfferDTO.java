package com.odigeo.frontend.contract.ancillaries;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOffer;

import java.util.Objects;

public class InsuranceOfferDTO extends InsuranceOffer {

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !getClass().equals(obj.getClass())) {
            return false;
        }

        InsuranceOfferDTO insuranceOffer = (InsuranceOfferDTO) obj;
        return Objects.equals(super.getUrl(), insuranceOffer.getUrl())
            && Objects.equals(super.getPolicy(), insuranceOffer.getPolicy());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getUrl(), super.getPolicy());
    }

}
