package com.odigeo.frontend.contract.itinerary;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;

public class ItineraryDTO {
    private ZonedDateTime freeCancellation;
    private List<LegDTO> legs;
    private Set<TransportType> transportTypes;

    public ZonedDateTime getFreeCancellation() {
        return freeCancellation;
    }

    public void setFreeCancellation(ZonedDateTime freeCancellation) {
        this.freeCancellation = freeCancellation;
    }

    public List<LegDTO> getLegs() {
        return legs;
    }

    public void setLegs(List<LegDTO> legs) {
        this.legs = legs;
    }

    public Set<TransportType> getTransportTypes() {
        return transportTypes;
    }

    public void setTransportTypes(Set<TransportType> transportTypes) {
        this.transportTypes = transportTypes;
    }
}
