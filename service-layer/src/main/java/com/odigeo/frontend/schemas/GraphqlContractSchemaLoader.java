package com.odigeo.frontend.schemas;

import com.edreamsodigeo.retail.frontendgraphql.contract.SchemaList;

import java.util.List;

public class GraphqlContractSchemaLoader extends AbstractInputStreamSchemaLoader {

    @Override
    protected List<String> getSchemaList() {
        return SchemaList.getSchemaList();
    }

}
