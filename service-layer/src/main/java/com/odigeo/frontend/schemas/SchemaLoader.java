package com.odigeo.frontend.schemas;

import java.io.Reader;
import java.util.List;

public interface SchemaLoader {

    List<Reader> getReaderList();

    //Add here the schema loader
    static List<Reader> loadReaders() {
        return new GraphqlContractSchemaLoader().getReaderList();
    }
}
