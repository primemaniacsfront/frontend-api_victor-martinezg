package com.odigeo.frontend.schemas;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractInputStreamSchemaLoader implements SchemaLoader {
    @Override
    public List<Reader> getReaderList() {
        return getSchemaList().stream()
            .map(schema -> new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(schema), StandardCharsets.UTF_8))
            .collect(Collectors.toList());
    }

    protected abstract List<String> getSchemaList();
}
