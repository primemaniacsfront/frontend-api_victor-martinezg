package com.odigeo.frontend.onefront;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.onefront.utils.OFRestClient;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.frontend.services.onefront.SearchSessionLoaderService;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.error.MessageResponse;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerarySearchResults;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.IOException;
import java.util.Optional;

@Singleton
@Deprecated //onefront integration
public class OFSearchIntegration {

    private static final String SEND_SEARCH_RESULTS_SERVICE = "loadSearchInfoInSession";
    private static final String SEND_SEARCH_META_RESULTS_SERVICE = "loadSearchMetaInfoInSession";
    private static final String WARNING_PREFIX = "WNG";

    @Inject
    private OFRestClient ofRestClient;
    @Inject
    private SearchService searchService;
    @Inject
    private SiteVariations siteVariations;
    @Inject
    private SearchSessionLoaderService searchSessionLoaderService;
    @Inject
    private JsonUtils jsonUtils;

    @SuppressFBWarnings("RCN_REDUNDANT_NULLCHECK_OF_NONNULL_VALUE")
    public void sendSearchResponse(SearchRequest searchRequest, SearchResponse searchResponse, ResolverContext context) {
        if (!hasSearchEngineNoResultsWng(searchResponse)) {
            RequestInfo requestInfo = context.getRequestInfo();
            String servicePath = searchRequest.getItinerary().getExternalSelection() == null ? SEND_SEARCH_RESULTS_SERVICE : SEND_SEARCH_META_RESULTS_SERVICE;

            cleanSearchResponseDebugInfo(searchResponse);

            boolean usingOFCommonsRestService = siteVariations.isOFCommonsRestServiceEnabled(context.getVisitInformation());

            if (usingOFCommonsRestService) {
                searchSessionLoaderService.loadSearchInfoInSession(jsonUtils.toJson(searchResponse), context);
            } else {
                try (CloseableHttpResponse response = ofRestClient.doPost(servicePath, requestInfo, searchResponse)) {
                } catch (IOException ex) {
                    throw new ServiceException("search integration: error sending to onefront", ex, ServiceName.ONEFRONT);
                }
            }

        }
    }

    private boolean hasSearchEngineNoResultsWng(SearchResponse searchResponse) {
        return Optional.ofNullable(searchResponse)
            .map(searchService::getSearchMessage)
            .map(MessageResponse::getCode)
            .map(code -> StringUtils.startsWith(code, WARNING_PREFIX))
            .orElse(false);
    }

    //TODO will be improved at feapi-554
    private void cleanSearchResponseDebugInfo(SearchResponse searchResponse) {
        ItinerarySearchResults itinerarySearchResults = searchResponse.getItinerarySearchResults();
        if (itinerarySearchResults != null) {
            itinerarySearchResults.getItineraryResults().forEach(itinerary -> itinerary.setDebugInfo(null));
        }
    }

}
