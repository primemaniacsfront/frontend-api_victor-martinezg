package com.odigeo.frontend.onefront;

import com.google.inject.Singleton;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.commons.collections4.CollectionUtils;

@Singleton
@Deprecated //onefront integration
public class OFShoppingCartIntegration {

    public Map<Integer, Integer> buildSegmentKeysMap(FareItinerary engineItinerary) {
        Map<Integer, Integer> segmentKeys = new HashMap<>();

        Stream.of(
            engineItinerary.getFirstSegments(),
            engineItinerary.getSecondSegments(),
            engineItinerary.getThirdSegments(),
            engineItinerary.getFourthSegments(),
            engineItinerary.getFifthSegments(),
            engineItinerary.getSixthSegments())
            .filter(CollectionUtils::isNotEmpty)
            .forEach(legSegments -> {
                int key = 0;
                for (Integer segmentId : legSegments) {
                    segmentKeys.put(segmentId, key);
                    key++;
                }
            });

        return segmentKeys;
    }
}
