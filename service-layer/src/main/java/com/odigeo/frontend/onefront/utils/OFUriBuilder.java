package com.odigeo.frontend.onefront.utils;

import com.google.inject.Singleton;
import com.odigeo.frontend.commons.context.RequestInfo;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.client.utils.URIBuilder;

@Singleton
public class OFUriBuilder {

    private static final String OF_FEAPI_PATH = "/travel/service/frontendapi/";
    private static final String MY_LOCAL_ENVIRONMENT_HOST = "mylocalenvironment.com";
    private static final int MY_LOCAL_ENVIRONMENT_PORT = 8080;

    public URI build(String serviceName, RequestInfo requestInfo) {
        try {
            URIBuilder uriBuilder = new URIBuilder(requestInfo.getRequestUrl()).setPath(OF_FEAPI_PATH + serviceName);

            if (MY_LOCAL_ENVIRONMENT_HOST.equals(uriBuilder.getHost())) {
                uriBuilder.setPort(MY_LOCAL_ENVIRONMENT_PORT);
            } else {
                uriBuilder.setScheme("https");
            }

            return uriBuilder.build();

        } catch (URISyntaxException ex) {
            return null;
        }
    }

}
