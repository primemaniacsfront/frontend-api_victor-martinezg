package com.odigeo.frontend.onefront;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.onefront.utils.OFRestClient;
import java.io.IOException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

@Singleton
@Deprecated //onefront integration
public class OFDebugInfoIntegration {

    private static final String QAMODE_SETTINGS_SERVICE = "getQaModeSettings";

    @Inject
    private OFRestClient ofRestClient;
    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private Logger logger;

    static class OFDebugInfo {
        boolean dapiCaptureEnabled;
        boolean qaModeEnabled;
    }

    public void requestDebugInfo(ResolverContext context) {
        RequestInfo requestInfo = context.getRequestInfo();
        DebugInfo debugInfo = context.getDebugInfo();

        try (CloseableHttpResponse httpResponse = ofRestClient.doGet(QAMODE_SETTINGS_SERVICE, requestInfo)) {
            String response = EntityUtils.toString(httpResponse.getEntity());
            OFDebugInfo ofDebugInfo = jsonUtils.fromJson(response, OFDebugInfo.class);

            debugInfo.setEnabled(ofDebugInfo.qaModeEnabled || ofDebugInfo.dapiCaptureEnabled);
            debugInfo.setQaModeEnabled(ofDebugInfo.qaModeEnabled);
            debugInfo.setApiCaptureEnabled(ofDebugInfo.dapiCaptureEnabled);

        } catch (IOException ex) {
            logger.warning(this.getClass(), "error requesting debug info from onefront", ex);
        }
    }

}
