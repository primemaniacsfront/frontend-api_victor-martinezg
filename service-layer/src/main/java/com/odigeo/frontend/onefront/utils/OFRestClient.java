package com.odigeo.frontend.onefront.utils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.util.JsonUtils;
import java.io.IOException;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.Cookie;
import javax.ws.rs.core.Response.Status;
import org.apache.http.Header;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.SSLContexts;

@Singleton
public class OFRestClient {

    private static final int TIMEOUT = 120 * 1000;

    @Inject
    private JsonUtils jsonUtils;
    @Inject
    private OFUriBuilder uriBuilder;

    public CloseableHttpResponse doPost(String serviceName, RequestInfo requestInfo, Object body) throws IOException {
        URI uri = uriBuilder.build(serviceName, requestInfo);

        return sendRequest(uri, requestInfo, () -> {
            HttpPost httpPost = new HttpPost(uri);
            httpPost.setEntity(new StringEntity(jsonUtils.toJson(body), ContentType.APPLICATION_JSON));
            return httpPost;
        });
    }

    public CloseableHttpResponse doGet(String serviceName, RequestInfo requestInfo) throws IOException {
        URI uri = uriBuilder.build(serviceName, requestInfo);
        return sendRequest(uri, requestInfo, () -> new HttpGet(uri));
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    private CloseableHttpResponse sendRequest(URI uri, RequestInfo requestInfo, Supplier<HttpRequestBase> builder)
    throws IOException {

        try {
            RequestConfig requestConfig = createRequestConfig();
            CookieStore cookies = createCookies(uri, requestInfo);
            Collection<Header> headers = createHeaders(requestInfo);

            CloseableHttpClient httpClient = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .setDefaultCookieStore(cookies)
                .setDefaultHeaders(headers)
                .setSSLContext(SSLContexts.custom().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
                .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .build();

            HttpRequestBase httpRequest = builder.get();
            CloseableHttpResponse httpResponse = httpClient.execute(httpRequest);

            int httpStatus = httpResponse.getStatusLine().getStatusCode();
            if (httpStatus >= Status.BAD_REQUEST.getStatusCode()) {
                throw new IOException("wrong status code at of integration"
                    + ". Uri: " + uri.toString()
                    + ". Code: " + httpStatus);
            }

            return httpResponse;

        } catch (GeneralSecurityException | RuntimeException ex) {
            throw new IOException("exception while executing the request. Uri: " + uri.toString(), ex);
        }
    }

    private CookieStore createCookies(URI uri, RequestInfo requestInfo) {
        CookieStore cookieStore = new BasicCookieStore();
        Collection<Cookie> cookies = requestInfo.getCookies();

        cookies.forEach(cookie -> addCookieToStore(cookieStore, uri, cookie.getName(), cookie.getValue()));

        addCookieToStore(cookieStore, uri, requestInfo, SiteHeaders.VISIT_INFORMATION, SiteCookies.VISIT_INFORMATION);
        addCookieToStore(cookieStore, uri, requestInfo, SiteHeaders.OF_JSESSIONID, SiteCookies.OF_JSESSIONID);
        addCookieToStore(cookieStore, uri, requestInfo, SiteHeaders.SB_JSESSIONID, SiteCookies.SB_JSESSIONID);

        return cookieStore;
    }

    private void addCookieToStore(CookieStore cookieStore, URI uri, RequestInfo requestInfo,
                                  SiteHeaders siteHeader, SiteCookies siteCookie) {
        String cookieName = siteCookie.value();
        String cookieValue = requestInfo.getHeaderOrCookie(siteHeader, siteCookie);
        addCookieToStore(cookieStore, uri, cookieName, cookieValue);
    }

    private void addCookieToStore(CookieStore cookieStore, URI uri, String cookieName, String cookieValue) {
        BasicClientCookie clientCookie = new BasicClientCookie(cookieName, cookieValue);
        clientCookie.setDomain(uri.getHost());
        cookieStore.addCookie(clientCookie);
    }

    private Collection<Header> createHeaders(RequestInfo requestInfo) {
        SiteHeaders traceId = SiteHeaders.TRACE_ID;
        SiteHeaders referer = SiteHeaders.REFERER;
        return Stream.of(
                new BasicHeader(traceId.value(), requestInfo.getHeader(traceId)),
                new BasicHeader(referer.value(), requestInfo.getHeader(referer))
        ).collect(Collectors.toList());
    }

    private RequestConfig createRequestConfig() {
        return RequestConfig.custom()
            .setConnectTimeout(TIMEOUT)
            .setConnectionRequestTimeout(TIMEOUT)
            .setSocketTimeout(TIMEOUT)
            .build();
    }

}
