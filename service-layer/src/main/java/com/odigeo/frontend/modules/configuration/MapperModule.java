package com.odigeo.frontend.modules.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.frontend.resolvers.accommodation.product.mappers.AvailableCollectionMethodsMapper;
import com.odigeo.frontend.resolvers.accommodation.product.mappers.AvailableCollectionMethodsMapperImpl;
import com.odigeo.frontend.resolvers.checkout.mappers.CheckoutMapper;
import com.odigeo.frontend.resolvers.checkout.mappers.CheckoutMapperImpl;
import com.odigeo.frontend.resolvers.checkout.mappers.UserPaymentInteractionMapper;
import com.odigeo.frontend.resolvers.checkout.mappers.UserPaymentInteractionMapperImpl;
import com.odigeo.frontend.resolvers.deals.mappers.DateRangeMapper;
import com.odigeo.frontend.resolvers.deals.mappers.DateRangeMapperImpl;
import com.odigeo.frontend.resolvers.deals.mappers.MoneyMapper;
import com.odigeo.frontend.resolvers.deals.mappers.MoneyMapperImpl;
import com.odigeo.frontend.resolvers.itinerary.mappers.ItineraryMapper;
import com.odigeo.frontend.resolvers.itinerary.mappers.ItineraryMapperImpl;
import com.odigeo.frontend.resolvers.payment.mappers.CreditCardBinDetailsMapper;
import com.odigeo.frontend.resolvers.payment.mappers.CreditCardBinDetailsMapperImpl;
import com.odigeo.frontend.resolvers.shoppingcart.buyer.mappers.BuyerMapper;
import com.odigeo.frontend.resolvers.shoppingcart.buyer.mappers.BuyerMapperImpl;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartMapperImpl;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts.NonEssentialProductsMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts.NonEssentialProductsMapperImpl;
import com.odigeo.frontend.resolvers.shoppingcart.traveller.mappers.ShoppingTravellerMapper;
import com.odigeo.frontend.resolvers.shoppingcart.traveller.mappers.ShoppingTravellerMapperImpl;
import com.odigeo.frontend.resolvers.trip.bookingimportantmessage.mapper.BookingImportantMessageMapper;
import com.odigeo.frontend.resolvers.trip.bookingimportantmessage.mapper.BookingImportantMessageMapperImpl;
import com.odigeo.frontend.resolvers.trip.mapper.TripMapper;
import com.odigeo.frontend.resolvers.trip.mapper.TripMapperImpl;
import com.odigeo.frontend.resolvers.user.mappers.CreditCardsMapper;
import com.odigeo.frontend.resolvers.user.mappers.CreditCardsMapperImpl;
import com.odigeo.frontend.resolvers.user.mappers.TravellerMapper;
import com.odigeo.frontend.resolvers.user.mappers.TravellerMapperImpl;

public class MapperModule extends AbstractModule {

    @Override
    protected void configure() {
        configureBookingMappers();
        configureTravellerMappers();
        configurePaymentMappers();
        configureItineraryMappers();
        configureNonEssentialProductsMappers();
    }

    private void configureTravellerMappers() {
        bind(ShoppingCartMapper.class).to(ShoppingCartMapperImpl.class);
        bind(TravellerMapper.class).to(TravellerMapperImpl.class);
        bind(BuyerMapper.class).to(BuyerMapperImpl.class);
        bind(ShoppingTravellerMapper.class).to(ShoppingTravellerMapperImpl.class);
    }

    private void configureBookingMappers() {
        bind(DateRangeMapper.class).to(DateRangeMapperImpl.class);
        bind(TripMapper.class).to(TripMapperImpl.class);
        bind(BookingImportantMessageMapper.class).to(BookingImportantMessageMapperImpl.class);
        bind(CheckoutMapper.class).to(CheckoutMapperImpl.class);
    }

    private void configurePaymentMappers() {
        bind(CreditCardBinDetailsMapper.class).to(CreditCardBinDetailsMapperImpl.class);
        bind(CreditCardsMapper.class).to(CreditCardsMapperImpl.class);
        bind(MoneyMapper.class).to(MoneyMapperImpl.class);
        bind(AvailableCollectionMethodsMapper.class).to(AvailableCollectionMethodsMapperImpl.class);
        bind(UserPaymentInteractionMapper.class).to(UserPaymentInteractionMapperImpl.class);
    }

    private void configureItineraryMappers() {
        bind(ItineraryMapper.class).to(ItineraryMapperImpl.class);
    }

    private void configureNonEssentialProductsMappers() {
        bind(NonEssentialProductsMapper.class).to(NonEssentialProductsMapperImpl.class);
    }
}
