package com.odigeo.frontend.modules;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.insurance.api.last.InsuranceApiService;

public class InsuranceApiServiceModule extends AbstractRestUtilsModule<InsuranceApiService> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 10000;

    public InsuranceApiServiceModule(ServiceNotificator... notificators) {
        super(InsuranceApiService.class, notificators);
    }

    @Override
    protected ServiceConfiguration<InsuranceApiService> getServiceConfiguration(Class<InsuranceApiService> aClass) {
        return new ServiceConfiguration.Builder<>(InsuranceApiService.class)
            .withConnectionConfigurationBuilder(createConfiguration())
            .build();
    }

    private ConnectionConfiguration.Builder createConfiguration() {
        return new ConnectionConfiguration.Builder()
            .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
            .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS);
    }

}
