package com.odigeo.frontend.modules.popularity;

import com.edreamsodigeo.seo.sslppopularitystats.popularity.v1.BookingPopularityService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.cache.CacheInterceptor;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class BookingPopularityServiceModule extends AbstractRestUtilsModule<BookingPopularityService> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 10000;

    public BookingPopularityServiceModule(ServiceNotificator... serviceNotificators) {
        super(BookingPopularityService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration<BookingPopularityService> getServiceConfiguration(Class<BookingPopularityService> aClass) {
        return new ServiceConfiguration.Builder<>(BookingPopularityService.class)
                .withConnectionConfigurationBuilder(createConnectionConfiguration())
                .withInterceptorConfiguration(createInterceptorConfiguration())
                .build();
    }

    private ConnectionConfiguration.Builder createConnectionConfiguration() {
        return new ConnectionConfiguration.Builder()
                .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS);
    }

    private InterceptorConfiguration<BookingPopularityService> createInterceptorConfiguration() {
        return new InterceptorConfiguration<>().addInterceptor(
            new CacheInterceptor().addCache(BookingPopularityService.class, new BookingPopularityClientCache())
        );
    }
}
