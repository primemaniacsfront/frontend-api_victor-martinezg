package com.odigeo.frontend.modules;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.frontend.services.shoppingbasket.ShoppingBasketApiV2;

public class ShoppingBasketV2ApiModule extends AbstractRestUtilsModule<ShoppingBasketApiV2> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 10000;

    public ShoppingBasketV2ApiModule(ServiceNotificator... notificators) {
        super(ShoppingBasketApiV2.class, notificators);
    }

    @Override
    protected ServiceConfiguration<ShoppingBasketApiV2> getServiceConfiguration(Class<ShoppingBasketApiV2> aClass) {
        return new ServiceConfiguration.Builder<>(ShoppingBasketApiV2.class)
                .withConnectionConfigurationBuilder(createConfiguration())
                .build();
    }

    private ConnectionConfiguration.Builder createConfiguration() {
        return new ConnectionConfiguration.Builder()
                .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS);
    }

}
