package com.odigeo.frontend.modules.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.frontend.resolvers.user.mappers.MembershipMapper;
import com.odigeo.frontend.resolvers.user.mappers.MembershipMapperImpl;
import com.odigeo.frontend.resolvers.user.mappers.MembershipSubscriptionProductMapper;
import com.odigeo.frontend.resolvers.user.mappers.MembershipSubscriptionProductMapperImpl;
import com.odigeo.frontend.resolvers.user.mappers.ModifyMembershipSubscriptionProductMapper;
import com.odigeo.frontend.resolvers.user.mappers.ModifyMembershipSubscriptionProductMapperImpl;
import com.odigeo.frontend.resolvers.user.mappers.UserDescriptionMapper;
import com.odigeo.frontend.resolvers.user.mappers.UserDescriptionMapperImpl;

public class MembershipMapperModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(MembershipMapper.class).to(MembershipMapperImpl.class);
        bind(UserDescriptionMapper.class).to(UserDescriptionMapperImpl.class);
        bind(ModifyMembershipSubscriptionProductMapper.class).to(ModifyMembershipSubscriptionProductMapperImpl.class);
        bind(MembershipSubscriptionProductMapper.class).to(MembershipSubscriptionProductMapperImpl.class);
    }
}
