package com.odigeo.frontend.modules;

import com.odigeo.accommodation.scoring.service.AccommodationScoringService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class AccommodationScoringServiceModule extends AbstractRestUtilsModule<AccommodationScoringService> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 10000;

    public AccommodationScoringServiceModule(ServiceNotificator... serviceNotificators) {
        super(AccommodationScoringService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration<AccommodationScoringService> getServiceConfiguration(Class<AccommodationScoringService> aClass) {
        return new ServiceConfiguration.Builder<>(AccommodationScoringService.class)
                .withConnectionConfigurationBuilder(createConfiguration())
                .build();
    }

    private ConnectionConfiguration.Builder createConfiguration() {
        return new ConnectionConfiguration.Builder()
                .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS);
    }
}
