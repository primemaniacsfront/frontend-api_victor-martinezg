package com.odigeo.frontend.modules;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.hcsapi.v9.HcsApiService;

public class HotelContentsServiceModule extends AbstractRestUtilsModule<HcsApiService> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 10000;

    public HotelContentsServiceModule(ServiceNotificator... notificators) {
        super(HcsApiService.class, notificators);
    }

    @Override
    protected ServiceConfiguration<HcsApiService> getServiceConfiguration(Class<HcsApiService> aClass) {
        return new ServiceConfiguration.Builder<>(HcsApiService.class)
            .withConnectionConfigurationBuilder(createConfiguration())
            .build();
    }

    private ConnectionConfiguration.Builder createConfiguration() {
        return new ConnectionConfiguration.Builder()
            .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
            .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS);
    }

}
