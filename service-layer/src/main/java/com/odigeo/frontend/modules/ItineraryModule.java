package com.odigeo.frontend.modules;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.itineraryapi.v1.ItineraryApiService;

public class ItineraryModule extends AbstractRestUtilsModule<ItineraryApiService> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 10000;

    public ItineraryModule(ServiceNotificator... notificators) {
        super(ItineraryApiService.class, notificators);
    }

    @Override
    protected ServiceConfiguration<ItineraryApiService> getServiceConfiguration(Class<ItineraryApiService> aClass) {
        return new ServiceConfiguration.Builder<>(ItineraryApiService.class)
            .withConnectionConfigurationBuilder(createConfiguration())
            .build();
    }

    private ConnectionConfiguration.Builder createConfiguration() {
        return new ConnectionConfiguration.Builder()
            .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
            .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS);
    }

}
