package com.odigeo.frontend.modules;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.commons.rest.monitoring.interceptor.MonitorFactory;
import com.odigeo.userprofiles.api.v2.UserApiService;

public class UserDescriptionApiServiceModule extends AbstractRestUtilsModule<UserApiService> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 10000;
    private static final int MAX_CONCURRENT_CONNECTIONS = 50;

    public UserDescriptionApiServiceModule(ServiceNotificator... notificators) {
        super(UserApiService.class, notificators);
    }

    @Override
    protected ServiceConfiguration<UserApiService> getServiceConfiguration(Class<UserApiService> aClass) {
        return new ServiceConfiguration.Builder<>(UserApiService.class)
                .withConnectionConfigurationBuilder(createConfiguration())
                .withInterceptorConfiguration(createInterceptorConfig())
                .build();
    }

    private ConnectionConfiguration.Builder createConfiguration() {
        return new ConnectionConfiguration.Builder()
                .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS)
                .maxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS);
    }

    private InterceptorConfiguration<UserApiService> createInterceptorConfig() {
        MonitorFactory monitorFactory = createMonitorFactory();
        InterceptorConfiguration<UserApiService> interceptorConfig = new InterceptorConfiguration<>();

        interceptorConfig
                .addInterceptor(monitorFactory.newHttpRequestInterceptor())
                .addInterceptor(monitorFactory.newHttpResponseInterceptor())
                .addInterceptor(monitorFactory.newRestUtilsInterceptor());

        return interceptorConfig;
    }

    MonitorFactory createMonitorFactory() {
        return new MonitorFactory();
    }

}
