package com.odigeo.frontend.modules.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.mappers.AccommodationShoppingItemMapper;
import com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.mappers.AccommodationShoppingItemMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationSummaryMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationSummaryMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.details.mappers.AccommodationDetailResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.details.mappers.AccommodationDetailResponseMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.SearchRoomAvailabilityResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.SearchRoomAvailabilityResponseMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.AccommodationSearchResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.AccommodationSearchResponseMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.mappers.AccommodationStaticContentResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.mappers.AccommodationStaticContentResponseMapperImpl;

public class AccommodationMappers extends AbstractModule {

    @Override
    protected void configure() {
        bind(SearchRoomAvailabilityResponseMapper.class).to(SearchRoomAvailabilityResponseMapperImpl.class);
        bind(AccommodationSearchResponseMapper.class).to(AccommodationSearchResponseMapperImpl.class);
        bind(AccommodationStaticContentResponseMapper.class).to(AccommodationStaticContentResponseMapperImpl.class);
        bind(AccommodationDetailResponseMapper.class).to(AccommodationDetailResponseMapperImpl.class);
        bind(AccommodationShoppingItemMapper.class).to(AccommodationShoppingItemMapperImpl.class);
        bind(AccommodationSummaryMapper.class).to(AccommodationSummaryMapperImpl.class);
    }
}
