package com.odigeo.frontend.modules.onefront;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.DefaultRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.frontend.contract.onefront.SearchSessionLoaderService;

public class SearchSessionLoaderServiceModule extends DefaultRestUtilsModule<SearchSessionLoaderService> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 120 * 1000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 120 * 1000;
    private static final int MAX_CONCURRENT_CONNECTION = 20;

    public SearchSessionLoaderServiceModule(ServiceNotificator... notificators) {
        super(SearchSessionLoaderService.class, notificators);
    }

    @Override
    protected ServiceConfiguration<SearchSessionLoaderService> getServiceConfiguration(Class<SearchSessionLoaderService> aClass) {
        return new ServiceConfiguration.Builder<>(SearchSessionLoaderService.class)
                .withConnectionConfigurationBuilder(createConfiguration())
                .build();
    }

    private ConnectionConfiguration.Builder createConfiguration() {
        return new ConnectionConfiguration.Builder()
                .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS)
                .maxConcurrentConnections(MAX_CONCURRENT_CONNECTION);
    }


}
