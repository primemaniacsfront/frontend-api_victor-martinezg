package com.odigeo.frontend.modules.popularity;

import com.edreams.persistance.cache.Cache;
import com.edreams.persistance.cache.impl.SimpleMemoryOnlyCache;
import com.odigeo.commons.rest.cache.ClientCache;
import org.apache.log4j.Logger;

import java.io.Serializable;

public class BookingPopularityClientCache implements ClientCache<String, Object> {
    private static final Logger LOGGER = Logger.getLogger(BookingPopularityClientCache.class);

    private static final String CACHE_STORAGE_SPACE_NAME = "BookingPopularityClientCache";
    private static final String CACHE_KEY_NAME = "bookingPopularity,";

    private final Cache cache;

    public BookingPopularityClientCache() {
        cache = new SimpleMemoryOnlyCache(CACHE_STORAGE_SPACE_NAME, CACHE_KEY_NAME, Serializable.class);
    }

    public Object getEntry(String cacheKey) {
        return this.cache.fetchValue(cacheKey);
    }

    public void addEntry(String cacheKey, Object value) {
        if (value == null) {
            this.cache.addMissedEntry(cacheKey);
        } else if (value instanceof Serializable) {
            this.cache.addEntry(cacheKey, Serializable.class.cast(value));
        } else {
            LOGGER.error(value.getClass().getCanonicalName() + " is not cacheable because is not serializable");
        }
    }

    public boolean isMissedEntry(String cacheKey) {
        return this.cache.isMissedEntry(cacheKey);
    }

    public void clear() {
        this.cache.flush();
    }

    public int size() {
        return this.cache.getCacheSize();
    }
}
