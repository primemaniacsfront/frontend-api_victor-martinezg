package com.odigeo.frontend.modules.configuration;

import com.edreamsodigeo.bookingimportantmessage.v1.client.BookingImportantMessageResourceModule;
import com.edreamsodigeo.collections.userpaymentinteraction.client.UserPaymentInteractionResourceModule;
import com.edreamsodigeo.collections.userpaymentinteraction.contract.UserPaymentInteractionResource;
import com.edreamsodigeo.insuranceproduct.api.last.module.InsuranceProductServiceModule;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.client.ItineraryFootprintCalculatorServiceDefaultModule;
import com.edreamsodigeo.payments.customercreditcardbinconfig.client.CustomerCreditCardBinConfigResourceModule;
import com.edreamsodigeo.payments.paymentinstrument.client.creditcard.CreditCardPciScopeResourceModule;
import com.google.inject.Module;
import com.odigeo.accommodation.offer.configuration.service.AccommodationOfferServiceModule;
import com.odigeo.accommodation.product.configuration.service.AccommodationProductServiceModule;
import com.odigeo.accommodation.review.service.AccommodationReviewRetrieverServiceModule;
import com.odigeo.checkin.api.v1.AutomaticCheckInApiServiceModule;
import com.odigeo.checkin.api.v1.CheckinStatusApiServiceModule;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.curex.client.configuration.CurrencyExchangeServiceModule;
import com.odigeo.frontend.modules.AccommodationScoringServiceModule;
import com.odigeo.frontend.modules.CollectionMethodApiServiceModule;
import com.odigeo.frontend.modules.HotelContentsServiceModule;
import com.odigeo.frontend.modules.InsuranceApiServiceModule;
import com.odigeo.frontend.modules.ItineraryModule;
import com.odigeo.frontend.modules.ShoppingBasketV2ApiModule;
import com.odigeo.frontend.modules.UserDescriptionApiServiceModule;
import com.odigeo.frontend.modules.UserDescriptionSSOApiServiceModule;
import com.odigeo.frontend.modules.onefront.SearchSessionLoaderServiceModule;
import com.odigeo.frontend.modules.popularity.BookingPopularityServiceModule;
import com.odigeo.geoapi.v4.client.module.AirportServiceCacheModule;
import com.odigeo.geoapi.v4.client.module.AutoCompleteServiceCacheModule;
import com.odigeo.geoapi.v4.client.module.CityServiceCacheModule;
import com.odigeo.geoapi.v4.client.module.CountryServiceCacheModule;
import com.odigeo.geoapi.v4.client.module.GeoNodeServiceCacheModule;
import com.odigeo.geoapi.v4.client.module.ItineraryLocationServiceCacheModule;
import com.odigeo.machinelearning.itineraryrating.api.v2.client.MeItineraryRatingServiceDefaultModule;
import com.odigeo.marketing.search.price.v1.modules.SearchPriceApiModule;
import com.odigeo.marketing.subscriptions.v1.modules.SubscriptionApiServiceModule;
import com.odigeo.membership.client.enhanced.MemberUserAreaServiceModule;
import com.odigeo.membership.client.enhanced.MembershipSearchApiModule;
import com.odigeo.membership.client.enhanced.MembershipServiceModule;
import com.odigeo.membership.offer.client.MembershipOfferServiceCacheModule;
import com.odigeo.searchengine.v2.SearchEngineModule;
import com.odigeo.shoppingbasket.client.ShoppingBasketServiceModule;
import com.odigeo.tracking.client.context.TrackingClientConfiguration;
import com.odigeo.tracking.client.track.method.TrackingSystemModule;
import com.odigeo.travelcompanion.client.TripServiceModule;
import com.odigeo.visitengineapi.v1.VisitEngine;
import com.odigeo.visitengineapi.v1.client.configuration.VisitEngineModule;
import com.odigeo.visitengineapi.v1.client.configuration.multitest.TestAssignmentServiceModule;
import com.odigeo.visitengineapi.v1.client.configuration.multitest.TestDimensionServiceModule;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentService;
import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

public interface ModuleLoader {

    //Add here the configuration of your services
    static Module[] load(ServiceNotificator... notificators) {
        return ArrayUtils.toArray(
            new ServiceLayerModule(),
            new MapperModule(),
            new MembershipMapperModule(),
            new GeoLocationMappers(),
            new AccommodationMappers(),
            new CurrencyExchangeServiceModule.Builder().build(notificators),
            new SearchEngineModule.Builder().withServiceNotificatorList(Arrays.asList(notificators)).build(),
            new MembershipServiceModule.Builder().build(notificators),
            new MembershipOfferServiceCacheModule(notificators),
            new MemberUserAreaServiceModule.Builder().build(notificators),
            new MembershipSearchApiModule.Builder().build(notificators),
            new VisitEngineModule(VisitEngine.class, notificators),
            new TestDimensionServiceModule(notificators),
            new TestAssignmentServiceModule(TestAssignmentService.class, notificators),
            new MeItineraryRatingServiceDefaultModule(notificators),
            new ItineraryFootprintCalculatorServiceDefaultModule(notificators),
            new ItineraryLocationServiceCacheModule(notificators),
            new UserDescriptionApiServiceModule(notificators),
            new UserDescriptionSSOApiServiceModule(notificators),
            new ItineraryModule(notificators),
            new TripServiceModule.Builder().withServiceNotificators(notificators).build(),
            new SubscriptionApiServiceModule(notificators),
            new HotelContentsServiceModule(notificators),
            new AccommodationReviewRetrieverServiceModule(notificators),
            new InsuranceApiServiceModule(notificators),
            new InsuranceProductServiceModule(notificators),
            new ShoppingBasketV2ApiModule(notificators),
            new TrackingSystemModule(buildTrackingClientConfig(), notificators),
            new CityServiceCacheModule(notificators),
            new CountryServiceCacheModule(notificators),
            new AirportServiceCacheModule(notificators),
            new GeoNodeServiceCacheModule(notificators),
            new AutoCompleteServiceCacheModule(notificators),
            new CollectionMethodApiServiceModule(notificators),
            new AccommodationScoringServiceModule(notificators),
            new BookingPopularityServiceModule(notificators),
            new AutomaticCheckInApiServiceModule.Builder().build(notificators),
            new CheckinStatusApiServiceModule.Builder().build(notificators),
            new BookingImportantMessageResourceModule(notificators),
            new SearchPriceApiModule(new ConnectionConfiguration.Builder()
                .connectionTimeoutInMillis(10000)
                .build(), notificators),
            new CreditCardPciScopeResourceModule(notificators),
            new UserPaymentInteractionResourceModule(UserPaymentInteractionResource.class, notificators),
            new SearchSessionLoaderServiceModule(notificators),
            new CustomerCreditCardBinConfigResourceModule(notificators),
            new ShoppingBasketServiceModule(notificators),
            new AccommodationProductServiceModule(notificators),
            new AccommodationOfferServiceModule(notificators)
        );
    }

    static TrackingClientConfiguration buildTrackingClientConfig() {
        return new TrackingClientConfiguration.Builder("frontend-api")
            .bufferSize(1000)
            .bufferMaxIntervalMillis(900)
            .bufferThreads(10)
            .bufferQueueLimit(1000)
            .conversionQueueLimit(100)
            .conversionThreads(10)
            .build();
    }

}
