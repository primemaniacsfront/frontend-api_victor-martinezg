package com.odigeo.frontend.modules;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.commons.rest.monitoring.interceptor.MonitorFactory;
import com.odigeo.userprofiles.api.v2.SSOApiService;

public class UserDescriptionSSOApiServiceModule extends AbstractRestUtilsModule<SSOApiService> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 10000;
    private static final int MAX_CONCURRENT_CONNECTIONS = 50;

    public UserDescriptionSSOApiServiceModule(ServiceNotificator ...notificators) {
        super(SSOApiService.class, notificators);
    }

    @Override
    protected ServiceConfiguration<SSOApiService> getServiceConfiguration(Class<SSOApiService> aClass) {
        return new ServiceConfiguration.Builder<>(SSOApiService.class)
                .withConnectionConfigurationBuilder(createConfiguration())
                .withInterceptorConfiguration(createInterceptorConfig())
                .build();
    }

    private ConnectionConfiguration.Builder createConfiguration() {
        return new ConnectionConfiguration.Builder()
                .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS)
                .maxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS);
    }

    private InterceptorConfiguration<SSOApiService> createInterceptorConfig() {
        MonitorFactory monitorFactory = createMonitorFactory();
        InterceptorConfiguration<SSOApiService> interceptorConfig = new InterceptorConfiguration<>();

        interceptorConfig
                .addInterceptor(monitorFactory.newHttpRequestInterceptor())
                .addInterceptor(monitorFactory.newHttpResponseInterceptor())
                .addInterceptor(monitorFactory.newRestUtilsInterceptor());

        return interceptorConfig;
    }

    MonitorFactory createMonitorFactory() {
        return new MonitorFactory();
    }
}
