package com.odigeo.frontend.modules.configuration;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionActions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Names;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.accommodation.rooms.handlers.RoomsHandler;
import com.odigeo.frontend.resolvers.accommodation.rooms.handlers.RoomsHandlerImpl;
import com.odigeo.frontend.resolvers.prime.models.MembershipPages;
import com.odigeo.frontend.resolvers.shoppingcart.perks.handlers.ItineraryPerksHandler;
import com.odigeo.frontend.resolvers.shoppingcart.perks.handlers.PerksHandler;
import com.odigeo.frontend.resolvers.shoppingcart.perks.handlers.ShoppingCartPerksHandler;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import com.odigeo.frontend.resolvers.prime.operations.AddMembershipSubscriptionAction;
import com.odigeo.frontend.resolvers.prime.operations.EditMembershipSubscriptionAction;
import com.odigeo.frontend.resolvers.prime.operations.MembershipSubscriptionShoppingCartAction;
import com.odigeo.frontend.resolvers.prime.operations.RemoveMembershipSubscriptionAction;
import com.odigeo.frontend.resolvers.prime.pages.MembershipPage;

import java.util.Arrays;

public class ServiceLayerModule extends AbstractModule {
    public static final ImmutableMap<MembershipSubscriptionActions, Class<? extends MembershipSubscriptionShoppingCartAction<ModifyMembershipSubscription, ResolverContext>>> SUBSCRIPTION_ACTIONS_CONFIG = Maps.immutableEnumMap(ImmutableMap.of(
            MembershipSubscriptionActions.ADD, AddMembershipSubscriptionAction.class,
            MembershipSubscriptionActions.EDIT, EditMembershipSubscriptionAction.class,
            MembershipSubscriptionActions.REMOVE, RemoveMembershipSubscriptionAction.class
    ));

    @Override
    public void configure() {
        configureBindingImplementations();
    }

    private void configureBindingImplementations() {
        bind(RoomsHandler.class).to(RoomsHandlerImpl.class);
        bind(PerksHandler.class).annotatedWith(Names.named("ItineraryPerksHandler"))
            .to(ItineraryPerksHandler.class)
            .in(Scopes.SINGLETON);
        bind(PerksHandler.class).annotatedWith(Names.named("ShoppingCartPerksHandler"))
            .to(ShoppingCartPerksHandler.class)
            .in(Scopes.SINGLETON);
        configureMembershipPages();
        configureMembershipSubscriptionActions();
    }

    private void configureMembershipPages() {
        MapBinder<MembershipPages, MembershipPage> binderMembershipPages = MapBinder.newMapBinder(binder(), MembershipPages.class, MembershipPage.class);
        Arrays.stream(MembershipPages.values()).forEach(page -> binderMembershipPages.addBinding(page).to(page.value()));
    }

    private void configureMembershipSubscriptionActions() {
        MapBinder<MembershipSubscriptionActions, MembershipSubscriptionShoppingCartAction<ModifyMembershipSubscription, ResolverContext>> subscriptionProductOperationMapBinder = MapBinder
                .newMapBinder(binder(), new SubscriptionActionsTypeLiteral(), new OperationTypeLiteral());
        SUBSCRIPTION_ACTIONS_CONFIG.keySet().forEach(key -> subscriptionProductOperationMapBinder.addBinding(key).to(SUBSCRIPTION_ACTIONS_CONFIG.get(key)));
    }

    private static class OperationTypeLiteral extends TypeLiteral<MembershipSubscriptionShoppingCartAction<ModifyMembershipSubscription, ResolverContext>> {
    }

    private static class SubscriptionActionsTypeLiteral extends TypeLiteral<MembershipSubscriptionActions> {
    }
}
