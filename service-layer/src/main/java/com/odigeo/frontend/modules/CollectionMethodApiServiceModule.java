package com.odigeo.frontend.modules;

import com.odigeo.collectionmethod.v3.CollectionMethodApiService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class CollectionMethodApiServiceModule extends AbstractRestUtilsModule<CollectionMethodApiService> {

    private static final int CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final int SOCKET_TIMEOUT_IN_MILLIS = 10000;
    private static final int MAX_CONCURRENT_CONNECTIONS = 10;

    public CollectionMethodApiServiceModule(ServiceNotificator... serviceNotificators) {
        super(CollectionMethodApiService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration<CollectionMethodApiService> getServiceConfiguration(Class<CollectionMethodApiService> aClass) {
        return new ServiceConfiguration.Builder<>(CollectionMethodApiService.class)
            .withConnectionConfigurationBuilder(createConfiguration())
            .build();
    }

    private ConnectionConfiguration.Builder createConfiguration() {
        return new ConnectionConfiguration.Builder()
            .connectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
            .socketTimeoutInMillis(SOCKET_TIMEOUT_IN_MILLIS)
            .maxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS);
    }

}
