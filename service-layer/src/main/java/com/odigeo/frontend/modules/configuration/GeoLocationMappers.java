package com.odigeo.frontend.modules.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.frontend.resolvers.geolocation.country.mappers.CountryMapper;
import com.odigeo.frontend.resolvers.geolocation.country.mappers.CountryMapperImpl;
import com.odigeo.frontend.resolvers.geolocation.suggestions.mappers.GeolocationSuggestionsMapper;
import com.odigeo.frontend.resolvers.geolocation.suggestions.mappers.GeolocationSuggestionsMapperImpl;

public class GeoLocationMappers extends AbstractModule {

    @Override
    protected void configure() {

        bind(CountryMapper.class).to(CountryMapperImpl.class);
        bind(GeolocationSuggestionsMapper.class).to(GeolocationSuggestionsMapperImpl.class);
    }
}
