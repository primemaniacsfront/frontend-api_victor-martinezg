package com.odigeo.frontend.commons.exception;

import javax.ws.rs.core.Response.Status;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class ServiceExceptionTest {

    private static final Status DEFAULT_STATUS = Status.INTERNAL_SERVER_ERROR;
    private static final ErrorCodes DEFAULT_ERROR_CODE = ErrorCodes.INTERNAL_GENERIC;

    @Mock
    private Exception cause;
    @Mock
    private Status status;
    @Mock
    private ErrorCodes errorCode;

    private final String message = "message";

    @BeforeClass
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testServiceException() {
        testException(new ServiceException(message, cause, status, errorCode), message, cause, status, errorCode, ServiceName.FRONTEND_API);
    }

    @Test
    public void testServiceExceptionWithMessage() {
        testException(new ServiceException(message, ServiceName.FRONTEND_API), message, null, DEFAULT_STATUS, DEFAULT_ERROR_CODE, ServiceName.FRONTEND_API);
    }

    @Test
    public void testServiceExceptonWithCause() {
        testException(new ServiceException(cause, ServiceName.FRONTEND_API), null, cause, DEFAULT_STATUS, DEFAULT_ERROR_CODE, ServiceName.FRONTEND_API);
    }

    @Test
    public void testServiceExceptionWithMessageAndCause() {
        testException(new ServiceException(message, cause, ServiceName.FRONTEND_API), message, cause, DEFAULT_STATUS, DEFAULT_ERROR_CODE, ServiceName.FRONTEND_API);
    }

    @Test
    public void testServiceExceptionWithMessageAndStatus() {
        testException(new ServiceException(message, status, ServiceName.FRONTEND_API), message, null, status, DEFAULT_ERROR_CODE, ServiceName.FRONTEND_API);
    }

    @Test
    public void testServiceExceptionWithMessageAndCauseAndStatus() {
        testException(new ServiceException(message, cause, status, ServiceName.FRONTEND_API), message, cause, status, DEFAULT_ERROR_CODE, ServiceName.FRONTEND_API);
    }

    @Test
    public void testServiceExceptionWithMessageAndStatusAndErrrorCode() {
        testException(new ServiceException(message, status, errorCode, ServiceName.FRONTEND_API), message, null, status, errorCode, ServiceName.FRONTEND_API);
    }

    private void testException(ServiceException exception, String message, Throwable cause,
                               Status status, ErrorCodes errorCode, ServiceName serviceName) {

        assertEquals(exception.getMessage(), message);
        assertSame(exception.getCause(), cause);
        assertSame(exception.getStatus(), status);
        assertSame(exception.getErrorCode(), errorCode);
        assertSame(exception.getServiceName(), serviceName);
    }

}
