package com.odigeo.frontend.commons.context;

import bean.test.BeanTest;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;

public class ResolverContextTest extends BeanTest<ResolverContext> {

    private ResolverContext bean;

    @BeforeMethod
    public void init() {
        bean = new ResolverContext();
    }

    @Override
    protected ResolverContext getBean() {
        bean.setVisitInformation(mock(VisitInformation.class));
        bean.setRequestInfo(mock(RequestInfo.class));
        bean.setResponseInfo(mock(ResponseInfo.class));
        bean.setDebugInfo(mock(DebugInfo.class));

        return bean;
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetInvalidVisitInformation() {
        bean.getVisitInformation();
    }

}
