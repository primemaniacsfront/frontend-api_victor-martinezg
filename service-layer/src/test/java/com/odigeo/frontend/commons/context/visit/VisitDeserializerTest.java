package com.odigeo.frontend.commons.context.visit;

import com.odigeo.frontend.commons.context.visit.mappers.BrandMapper;
import com.odigeo.frontend.commons.context.visit.mappers.BrowserMapper;
import com.odigeo.frontend.commons.context.visit.mappers.DeviceMapper;
import com.odigeo.frontend.commons.context.visit.mappers.OSMapper;
import com.odigeo.frontend.commons.context.visit.mappers.SiteMapper;
import com.odigeo.frontend.commons.context.visit.mappers.WebInterfaceMapper;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Browser;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.OS;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import com.odigeo.frontend.services.VisitEngineService;
import com.odigeo.visitengineapi.v1.marketing.MarketingPortal;
import com.odigeo.visitengineapi.v1.multitest.TestTokenSet;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.website.Website;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class VisitDeserializerTest {

    private static final String VISIT_CODE = "visit";

    @Mock
    private VisitEngineService visitEngineService;
    @Mock
    private BrandMapper brandMapper;
    @Mock
    private SiteMapper siteMapper;
    @Mock
    private WebInterfaceMapper webInterfaceMapper;
    @Mock
    private DeviceMapper deviceMapper;
    @Mock
    private BrowserMapper browserMapper;
    @Mock
    private OSMapper osMapper;
    @Mock
    private VisitResponse visitResponse;
    @Mock
    private Website website;

    @InjectMocks
    private VisitDeserializer deserializer;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(visitEngineService.deserialize(VISIT_CODE)).thenReturn(visitResponse);
        when(visitResponse.getWebsite()).thenReturn(website);
    }

    @Test
    public void testDeserializeVisit() {
        Long visitId = 1L;

        Site site = mock(Site.class);
        Brand brand = mock(Brand.class);
        Device device = mock(Device.class);
        Browser browser = mock(Browser.class);
        OS os = mock(OS.class);
        WebInterface webInterface = mock(WebInterface.class);

        when(visitResponse.getVisitId()).thenReturn(visitId);
        when(visitResponse.getVisitInformation()).thenReturn(VISIT_CODE);

        when(siteMapper.map(visitResponse)).thenReturn(site);
        when(brandMapper.map(visitResponse)).thenReturn(brand);
        when(deviceMapper.map(visitResponse)).thenReturn(device);
        when(browserMapper.map(visitResponse)).thenReturn(browser);
        when(osMapper.map(visitResponse)).thenReturn(os);
        when(webInterfaceMapper.map(visitResponse)).thenReturn(webInterface);

        Locale locale = mock(Locale.class);
        String localeCode = "code";
        when(locale.toString()).thenReturn(localeCode);
        when(visitResponse.getLocale()).thenReturn(locale);

        Currency currency = mock(Currency.class);
        String currencyCode = "code";
        when(website.getCurrency()).thenReturn(currency);
        when(currency.getCurrencyCode()).thenReturn(currencyCode);

        MarketingPortal marketing = mock(MarketingPortal.class);
        String mktPortal = "mktPortal";
        when(visitResponse.getMarketingPortal()).thenReturn(marketing);
        when(marketing.getCode()).thenReturn(mktPortal);

        String defaultCountry = "defaultCountry";
        when(website.getDefaultCountry()).thenReturn(defaultCountry);

        String userIp = "userIp";
        when(visitResponse.getUserIp()).thenReturn(userIp);

        String userDevice = "userDevice";
        when(visitResponse.getUserDevice()).thenReturn(userDevice);

        TestTokenSet testTokenSet = mock(TestTokenSet.class);
        Map<String, Integer> dimensionPartitionMap = new HashMap<>(0);
        when(visitResponse.getTestTokenSet()).thenReturn(testTokenSet);
        when(testTokenSet.getDimensionPartitionMapRepresentation()).thenReturn(dimensionPartitionMap);

        VisitInformation visit = deserializer.deserialize(VISIT_CODE);

        assertEquals(visit.getVisitId(), visitId);
        assertEquals(visit.getVisitCode(), VISIT_CODE);

        assertSame(visit.getSite(), site);
        assertSame(visit.getBrand(), brand);
        assertSame(visit.getDevice(), device);
        assertSame(visit.getBrowser(), browser);
        assertSame(visit.getOs(), os);
        assertSame(visit.getWebInterface(), webInterface);

        assertEquals(visit.getLocale(), localeCode);
        assertEquals(visit.getCurrency(), currencyCode);
        assertEquals(visit.getMktPortal(), mktPortal);
        assertEquals(visit.getWebsiteDefaultCountry(), defaultCountry);
        assertEquals(visit.getUserIp(), userIp);
        assertEquals(visit.getUserDevice(), userDevice);

        assertSame(visit.getDimensionPartitionMap(), dimensionPartitionMap);
    }

    @Test
    public void testDeserializeVisitNull() {
        when(visitEngineService.deserialize(VISIT_CODE)).thenReturn(null);
        assertNull(deserializer.deserialize(VISIT_CODE));
    }

    @Test
    public void testInitLocaleNullLocale() {
        when(visitResponse.getLocale()).thenReturn(null);
        assertEquals(deserializer.deserialize(VISIT_CODE).getLocale(), StringUtils.EMPTY);
    }

    @Test
    public void testInitCurrencyNullWebsite() {
        when(visitResponse.getWebsite()).thenReturn(null);
        assertEquals(deserializer.deserialize(VISIT_CODE).getCurrency(), StringUtils.EMPTY);
    }

    @Test
    public void testInitCurrencyNullCurrency() {
        when(website.getCurrency()).thenReturn(null);
        assertEquals(deserializer.deserialize(VISIT_CODE).getCurrency(), StringUtils.EMPTY);
    }

    @Test
    public void testInitMktPortalNullMarketing() {
        when(visitResponse.getMarketingPortal()).thenReturn(null);
        assertEquals(deserializer.deserialize(VISIT_CODE).getMktPortal(), StringUtils.EMPTY);
    }

    @Test
    public void testInitWebsiteDefaultCountryNullWebsite() {
        when(visitResponse.getWebsite()).thenReturn(null);

        VisitInformation visit = deserializer.deserialize(VISIT_CODE);
        assertEquals(visit.getWebsiteDefaultCountry(), StringUtils.EMPTY);
    }

    @Test
    public void testInitDimensionPartitionMapNull() {
        when(visitResponse.getTestTokenSet()).thenReturn(null);

        VisitInformation visit = deserializer.deserialize(VISIT_CODE);
        assertEquals(visit.getDimensionPartitionMap(), Collections.emptyMap());
    }

}
