package com.odigeo.frontend.commons.util;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapStaticConfiguration;
import com.google.maps.GeoApiContext;
import com.google.maps.ImageResult;
import com.google.maps.StaticMapsRequest;
import com.google.maps.errors.ApiException;
import com.google.maps.errors.InvalidRequestException;
import com.odigeo.frontend.commons.mappers.GoogleMapsRequestMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.AssertJUnit.assertEquals;

public class GoogleMapsUtilsTest {

    private static final String EMPTY_STRING = "";
    private static final String APY_KEY = "apyKey";
    private static final String SECRET_KEY = "apyKey";

    @Mock
    private GoogleMapsRequestMapper googleMapsRequestMapper;
    @Mock
    private MapStaticConfiguration request;
    @Mock
    private StaticMapsRequest mapRequest;
    @Mock
    private GeoApiContext context;

    @InjectMocks
    private GoogleMapsUtils utils;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        when(googleMapsRequestMapper.mapGoogleMapsStaticRequest(eq(request), anyDouble(), anyDouble(), eq(context))).thenReturn(mapRequest);

    }

    @Test
    public void testCreateContext() {
        assertNotNull(utils.createContext("getApiKey", "getSecretKey", ""));
    }

    @Test
    public void testCreateContextNull() {
        assertNull(utils.createContext(APY_KEY, EMPTY_STRING, EMPTY_STRING));
        assertNull(utils.createContext(null, EMPTY_STRING, EMPTY_STRING));
        assertNull(utils.createContext(EMPTY_STRING, SECRET_KEY, EMPTY_STRING));
        assertNull(utils.createContext(null, SECRET_KEY, EMPTY_STRING));
    }

    @Test
    public void testCreateContextWrongSecretKey() {
        try {
            assertNotNull(utils.createContext(APY_KEY, "wrong", EMPTY_STRING));
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Private key is invalid.");
        }
    }

    @Test
    public void testGetStaticMap() throws IOException, InterruptedException, ApiException {
        byte[] expectedResponse = "imageData".getBytes(StandardCharsets.UTF_8);
        ImageResult imageResult = new ImageResult("contentType", expectedResponse);
        when(mapRequest.await()).thenReturn(imageResult);
        assertEquals(utils.getStaticMap(request, 8d, 88d, context), expectedResponse);
    }

    @Test(expectedExceptions = ApiException.class)
    public void testGetStaticMapWithException() throws IOException, InterruptedException, ApiException {
        when(mapRequest.await()).thenThrow(new InvalidRequestException("exception"));
        utils.getStaticMap(request, 8d, 88d, context);
    }

    @Test
    public void testGetStaticMapOrElseThrow() throws IOException, InterruptedException {
        ImageResult imageResult = new ImageResult("contentType", null);
        try {
            when(mapRequest.await()).thenReturn(imageResult);
            utils.getStaticMap(request, 8d, 88d, context);
        } catch (ApiException e) {
            assertEquals(e.getMessage(), "ImageData not found");
        }
    }
}
