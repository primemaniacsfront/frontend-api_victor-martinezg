package com.odigeo.frontend.commons;

import com.google.inject.Guice;
import com.odigeo.commons.rest.monitoring.ModuleInfo;
import com.odigeo.commons.rest.monitoring.ModuleInfoLoader;
import com.odigeo.commons.rest.monitoring.MonitoringConfigurationException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ModuleReleaseInfoTest {

    private static final String DEFAULT_ID = "frontend-api";
    private static final String DEFAULT_VERSION = "UNKNOWN";

    @Mock
    private ModuleInfoLoader moduleInfoLoader;
    @Mock
    private ModuleInfo moduleInfo;

    private ModuleReleaseInfo module;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        module = new ModuleReleaseInfo();

        Guice.createInjector(binder -> binder.bind(ModuleInfoLoader.class).toProvider(() -> moduleInfoLoader))
            .injectMembers(module);
    }

    @Test
    public void testFormatModuleInfoDefault() {
        when(moduleInfoLoader.load()).thenThrow(MonitoringConfigurationException.class);
        assertEquals(module.formatModuleInfo(), DEFAULT_ID + ":" + DEFAULT_VERSION);
    }

    @Test
    public void testFormatModuleInfo() {
        String moduleInfoFormated = "info";
        when(moduleInfoLoader.load()).thenReturn(moduleInfo);
        when(moduleInfo.toString()).thenReturn(moduleInfoFormated);

        String firstCall = module.formatModuleInfo();
        String secondCall = module.formatModuleInfo();

        assertEquals(firstCall, moduleInfoFormated);
        assertEquals(secondCall, moduleInfoFormated);
        verify(moduleInfoLoader).load();
    }

}
