package com.odigeo.frontend.commons;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SiteHeadersTest {

    private static final SiteHeaders SITE_HEADER = SiteHeaders.VISIT_INFORMATION;
    private static final String SITE_HEADER_NAME = "X-visit";

    @Test
    public void testValue() {
        assertEquals(SITE_HEADER.value(), SITE_HEADER_NAME);
    }
}
