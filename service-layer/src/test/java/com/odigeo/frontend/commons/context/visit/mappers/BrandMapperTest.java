package com.odigeo.frontend.commons.context.visit.mappers;

import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.website.Website;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class BrandMapperTest {

    @Mock
    private VisitResponse visitResponse;
    @Mock
    private Website website;

    private BrandMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new BrandMapper();

        when(visitResponse.getWebsite()).thenReturn(website);
    }

    @Test
    public void testMap() {
        Brand brand = Brand.ED;

        when(website.getBrand()).thenReturn(brand.name());
        assertSame(mapper.map(visitResponse), brand);
    }

    @Test
    public void testMapWebsiteNull() {
        when(visitResponse.getWebsite()).thenReturn(null);
        assertSame(mapper.map(visitResponse), Brand.UNKNOWN);
    }

    @Test
    public void testMapWebsiteUnknown() {
        when(website.getCode()).thenReturn("unknown");
        assertSame(mapper.map(visitResponse), Brand.UNKNOWN);
    }

}
