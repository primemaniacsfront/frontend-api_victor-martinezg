package com.odigeo.frontend.commons.util;

import com.odigeo.frontend.commons.exception.RsaException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class QueryParamUtilsTest {

    private static final String PARAM_1 = "param1";
    private static final String PARAM_2 = "param2";
    private static final String VALUE_1 = "value1";
    private static final String VALUE_2 = "value2";
    private static final long BOOKING_ID = 1L;
    private static final String EMAIL = "test@test.com";
    private static final String ENC_MSG = "encMsg";

    @Mock
    private EncryptUtils encryptUtils;
    @InjectMocks
    private QueryParamUtils queryParamUtils;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetQueryParamsString() {
        assertEquals(queryParamUtils.getQueryParamsString(Collections.emptyMap()), StringUtils.EMPTY);
        assertEquals(queryParamUtils.getQueryParamsString(Collections.emptyMap(), PARAM_1, StringUtils.EMPTY), PARAM_1);
        assertEquals(queryParamUtils.getQueryParamsString(Collections.emptyMap(), StringUtils.EMPTY, PARAM_1), PARAM_1);

        Map<String, Object> params = Stream.of(
                new AbstractMap.SimpleEntry<>(PARAM_1, VALUE_1)
        ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
        String queryParams = queryParamUtils.getQueryParamsString(params, StringUtils.EMPTY, StringUtils.EMPTY);
        assertEquals(queryParams, PARAM_1 + "=" + VALUE_1);

        params.put(PARAM_2, VALUE_2);
        queryParams = queryParamUtils.getQueryParamsString(params, StringUtils.EMPTY, StringUtils.EMPTY);
        assertEquals(queryParams, PARAM_1 + "=" + VALUE_1 + "&" + PARAM_2 + "=" + VALUE_2);
    }

    @Test
    public void testGetQueryParamsEncrypted() {
        checkBuildParamsEncrypted(Collections.emptyMap(), StringUtils.EMPTY);
        checkBuildParamsEncrypted(Collections.emptyMap(), "encMessage");
    }

    private void checkBuildParamsEncrypted(Map<String, Object> params, String encryptedMessage) {
        try {
            when(encryptUtils.encryptBase64URLSafe(any())).thenReturn(encryptedMessage);
        } catch (RsaException e) {
            Assert.fail();
        }
        assertEquals(queryParamUtils.getQueryParamsEncrypted(params), encryptedMessage);
    }

    @Test
    public void testGetQueryParamsEncryptedException() throws RsaException {
        when(encryptUtils.encryptBase64URLSafe(any())).thenThrow(RsaException.class);
        assertEquals(queryParamUtils.getQueryParamsEncrypted(Collections.emptyMap()), StringUtils.EMPTY);
    }

    @Test
    public void testGenerateMyTripsRedirectionToken() {
        try {
            when(encryptUtils.encryptBase64URLSafe(any())).thenReturn(ENC_MSG);
        } catch (RsaException e) {
            Assert.fail();
        }
        assertEquals(queryParamUtils.generateMyTripsRedirectionToken(BOOKING_ID, EMAIL), ENC_MSG);
    }

}
