package com.odigeo.frontend.commons.util;

import graphql.schema.DataFetchingEnvironment;
import java.util.Collections;
import java.util.Map;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class JsonUtilsTest {

    private static final String PARAM_NAME = "paramName";
    private static final String PARAM_VALUE = "paramValue";
    private static final String FIELD_NAME = "fieldName";
    private static final Integer FIELD_VALUE = 1;
    private static final String JSON = "{\"" + FIELD_NAME + "\":" + FIELD_VALUE + "}";
    private static final Map<String, Integer> JSON_MAP = Collections.singletonMap(FIELD_NAME, FIELD_VALUE);

    @Mock
    private DataFetchingEnvironment env;

    private JsonUtils jsonUtils;

    private static class DtoTest {
        public Integer fieldName;
    }

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        jsonUtils = new JsonUtils();
    }

    @Test
    public void testToJson() {
        assertEquals(jsonUtils.toJson(JSON_MAP), JSON);
    }

    @Test
    public void testFromJson() {
        DtoTest result = jsonUtils.fromJson(JSON, DtoTest.class);
        assertEquals(result.fieldName, FIELD_VALUE);
    }

    @Test
    public void testFromDataFetching() {
        when(env.getArguments()).thenReturn(Collections.singletonMap(PARAM_NAME, PARAM_VALUE));
        assertEquals(jsonUtils.fromDataFetching(env), PARAM_VALUE);
    }

    @Test
    public void testFromDataFetchingWithParam() {
        when(env.getArgument(PARAM_NAME)).thenReturn(PARAM_VALUE);
        assertEquals(jsonUtils.fromDataFetching(env, PARAM_NAME), PARAM_VALUE);
    }

    @Test
    public void testFromDataFetchingWithOptionalParam() {
        assertNull(jsonUtils.fromDataFetching(env));
    }

    @Test
    public void testFromDataFetchingWithClass() {
        when(env.getArguments()).thenReturn(Collections.singletonMap(PARAM_NAME, JSON_MAP));
        assertEquals(jsonUtils.fromDataFetching(env, DtoTest.class).fieldName, FIELD_VALUE);
    }

    @Test
    public void testFromDataFetchingWithClassAndParam() {
        when(env.getArgument(PARAM_NAME)).thenReturn(JSON_MAP);
        assertEquals(jsonUtils.fromDataFetching(env, DtoTest.class, PARAM_NAME).fieldName, FIELD_VALUE);
    }

}
