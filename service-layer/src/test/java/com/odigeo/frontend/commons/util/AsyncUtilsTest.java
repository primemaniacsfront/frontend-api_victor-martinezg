package com.odigeo.frontend.commons.util;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.exception.ServiceException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.odigeo.frontend.commons.exception.ServiceName;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class AsyncUtilsTest {

    private static final String TEST_VALUE = "TEST";
    private static final String EXCEPTION_VALUE = "TEST";

    @Mock
    private Future<String> future;
    @Mock
    private Logger logger;

    @InjectMocks
    private AsyncUtils asyncUtils;

    private ExecutorService executor;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testShutdownAndWait() throws ExecutionException, InterruptedException {
        executor = Executors.newFixedThreadPool(1);
        Future<String> future = executor.submit(() -> TEST_VALUE);
        asyncUtils.shutdownAndWait(executor);
        assertEquals(future.get(), TEST_VALUE);
    }

    @Test
    public void testMapFuture() throws ExecutionException, InterruptedException {
        when(future.get()).thenReturn(TEST_VALUE);
        assertEquals(asyncUtils.mapFuture(future, String.class), TEST_VALUE);
    }

    @Test
    public void testMapFutureNullWrongCast() throws ExecutionException, InterruptedException {
        when(future.get()).thenReturn(TEST_VALUE);
        assertNull(asyncUtils.mapFuture(future, Integer.class));
    }

    @Test
    public void testMapFutureNullInterrupted() throws ExecutionException, InterruptedException {
        when(future.get()).thenThrow(new InterruptedException());
        assertNull(asyncUtils.mapFuture(future, Integer.class));
    }

    @Test
    public void testMapFutureNullException() throws ExecutionException, InterruptedException {
        when(future.get()).thenThrow(new ExecutionException(new NullPointerException()));
        assertNull(asyncUtils.mapFuture(future, Integer.class));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testMapThrowException() throws ExecutionException, InterruptedException {
        when(future.get()).thenThrow(new ExecutionException(new ServiceException(EXCEPTION_VALUE, ServiceName.FRONTEND_API)));
        asyncUtils.mapFuture(future, String.class);
    }

}
