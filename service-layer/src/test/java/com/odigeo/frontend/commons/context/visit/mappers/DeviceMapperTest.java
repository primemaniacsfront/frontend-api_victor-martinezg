package com.odigeo.frontend.commons.context.visit.mappers;

import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.visitengineapi.generic.interfaces.DeviceType;
import com.odigeo.visitengineapi.v1.interfaces.Interface;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class DeviceMapperTest {

    @Mock
    private VisitResponse visitResponse;
    @Mock
    private Interface clientInterface;

    private DeviceMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new DeviceMapper();

        when(visitResponse.getClientInterface()).thenReturn(clientInterface);
    }

    @Test
    public void testMap() {
        Stream.of(
            new ImmutablePair<>(DeviceType.DESKTOP, Device.DESKTOP),
            new ImmutablePair<>(DeviceType.SMARTPHONE, Device.MOBILE),
            new ImmutablePair<>(DeviceType.TABLET, Device.TABLET)
        ).forEach(pair -> {
            when(clientInterface.getDeviceType()).thenReturn(pair.left.name());
            assertSame(mapper.map(visitResponse), pair.right);
        });
    }

    @Test
    public void testMapClientInterfaceNull() {
        when(visitResponse.getClientInterface()).thenReturn(null);
        assertSame(mapper.map(visitResponse), Device.OTHER);
    }

    @Test
    public void testMapUnknown() {
        when(clientInterface.getDeviceType()).thenReturn("unknown");
        assertSame(mapper.map(visitResponse), Device.OTHER);
    }

}
