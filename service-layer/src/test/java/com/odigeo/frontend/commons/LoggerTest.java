package com.odigeo.frontend.commons;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class LoggerTest {

    @Mock
    private org.apache.log4j.Logger apacheLogger;
    @Mock
    private Object message;
    @Mock
    private Throwable exception;

    @Spy
    private Logger logger;
    private final Class<?> loggerClass = LoggerTest.class;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        doReturn(apacheLogger).when(logger).getLogger(eq(loggerClass));
    }

    @Test
    public void testWarning() {
        logger.warning(loggerClass, message, exception);
        verify(apacheLogger).warn(eq(message), eq(exception));
    }

    @Test
    public void testWarningWithMessage() {
        logger.warning(loggerClass, message);
        verify(apacheLogger).warn(eq(message));
    }

    @Test
    public void testWarningWithException() {
        logger.warning(loggerClass, exception);
        verify(apacheLogger).warn(isNull(), eq(exception));
    }

    @Test
    public void testError() {
        logger.error(loggerClass, message, exception);
        verify(apacheLogger).error(eq(message), eq(exception));
    }

    @Test
    public void testErrorWithMessage() {
        logger.error(loggerClass, message);
        verify(apacheLogger).error(eq(message));
    }

    @Test
    public void testErrorWithException() {
        logger.error(loggerClass, exception);
        verify(apacheLogger).error(isNull(), eq(exception));
    }

}
