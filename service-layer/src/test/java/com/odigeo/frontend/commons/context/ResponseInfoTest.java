package com.odigeo.frontend.commons.context;

import com.odigeo.frontend.commons.SiteCookies;
import java.util.Collection;
import java.util.Map;
import javax.servlet.http.Cookie;

import com.odigeo.frontend.commons.SiteHeaders;
import org.apache.commons.collections4.IterableUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class ResponseInfoTest {

    protected static final String COOKIE_NAME = "cookieName";
    protected static final String COOKIE_VALUE = "cookieValue";
    protected static final String HEADER_VALUE = "headerValue";

    private ResponseInfo responseInfo;

    @BeforeMethod
    public void init() {
        responseInfo = new ResponseInfo();
    }

    @Test
    public void testGetEmptyCookies() {
        Collection<Cookie> cookies = responseInfo.getCookies();

        assertEquals(cookies.size(), 0);
    }

    @Test
    public void testGetCookies() {
        SiteCookies siteCookie = mock(SiteCookies.class);
        when(siteCookie.value()).thenReturn(COOKIE_NAME);
        responseInfo.addCookie(siteCookie, COOKIE_VALUE);

        assertEquals(responseInfo.getCookies().size(), 1);
    }

    @Test
    public void testAddCookies() {
        SiteCookies siteCookie = givenAddCookiesTest();

        responseInfo.addCookie(siteCookie, COOKIE_VALUE);
        Collection<Cookie> cookies = responseInfo.getCookies();
        Cookie cookie = IterableUtils.first(cookies);

        thenAddCookiesTest(cookies, cookie);
    }

    @Test
    public void testAddCookiesMaxExpirationAge() {
        SiteCookies siteCookie = givenAddCookiesTest();

        responseInfo.addCookie(siteCookie, COOKIE_VALUE, ResponseInfo.DEFAULT_COOKIE_EXPIRATION);
        Collection<Cookie> cookies = responseInfo.getCookies();
        Cookie cookie = IterableUtils.first(cookies);

        thenAddCookiesTest(cookies, cookie);
    }

    private SiteCookies givenAddCookiesTest() {
        SiteCookies siteCookie = mock(SiteCookies.class);
        when(siteCookie.value()).thenReturn(COOKIE_NAME);
        return siteCookie;
    }

    @Test
    public void testGetCookie() {
        SiteCookies siteCookie = givenAddCookiesTest();
        responseInfo.addCookie(siteCookie, COOKIE_VALUE);
        Cookie cookie = new Cookie(COOKIE_NAME, COOKIE_VALUE);

        assertEquals(responseInfo.getCookie(siteCookie).getValue(), cookie.getValue());
    }

    private void thenAddCookiesTest(Collection<Cookie> cookies, Cookie cookie) {
        assertEquals(cookies.size(), 1);
        assertEquals(cookie.getName(), COOKIE_NAME);
        assertEquals(cookie.getValue(), COOKIE_VALUE);
        assertEquals(cookie.getPath(), ResponseInfo.DEFAULT_COOKIE_PATH);
        assertEquals(cookie.getMaxAge(), ResponseInfo.DEFAULT_COOKIE_EXPIRATION);
        assertTrue(cookie.isHttpOnly());
        assertTrue(cookie.getSecure());
    }

    @Test
    public void getEmptyHeaders() {
        Map<SiteHeaders, String> headers = responseInfo.getHeaders();

        assertEquals(headers.size(), 0);
    }

    @Test
    public void getHeaders() {
        Map<SiteHeaders, String> headers = responseInfo.getHeaders();
        headers.put(SiteHeaders.SB_JSESSIONID, "value");

        assertEquals(responseInfo.getHeaders().size(), 1);
    }

    @Test
    public void testPutHeader() {
        SiteCookies siteCookie = mock(SiteCookies.class);
        when(siteCookie.value()).thenReturn(COOKIE_NAME);

        responseInfo.putHeader(SiteHeaders.SB_JSESSIONID, HEADER_VALUE);
        Map<SiteHeaders, String> headers = responseInfo.getHeaders();

        assertEquals(headers.size(), 1);
        assertEquals(headers.get(SiteHeaders.SB_JSESSIONID), HEADER_VALUE);
        assertNull(headers.get(SiteHeaders.TRACE_ID));
    }
}
