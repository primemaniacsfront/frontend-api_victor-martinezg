package com.odigeo.frontend.commons;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SiteCookiesTest {

    private static final SiteCookies SITE_COOKIE = SiteCookies.MKT_TRACKING;
    private static final String SITE_COOKIE_NAME = "mktTrack";

    @Test
    public void getGetValue() {
        assertEquals(SITE_COOKIE.value(), SITE_COOKIE_NAME);
    }

}
