package com.odigeo.frontend.commons.context.visit.mappers;

import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import com.odigeo.visitengineapi.generic.interfaces.Interface;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class WebInterfaceMapperTest {

    @Mock
    private VisitResponse visitResponse;
    @Mock
    private com.odigeo.visitengineapi.v1.interfaces.Interface clientInterface;

    private WebInterfaceMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new WebInterfaceMapper();

        when(visitResponse.getClientInterface()).thenReturn(clientInterface);
    }

    @Test
    public void testMap() {
        Stream.of(
            new ImmutablePair<>(Interface.ONE_FRONT_DESKTOP, WebInterface.ONE_FRONT_DESKTOP),
            new ImmutablePair<>(Interface.ONE_FRONT_SMARTPHONE, WebInterface.ONE_FRONT_SMARTPHONE),
            new ImmutablePair<>(Interface.ONE_FRONT_TABLET, WebInterface.ONE_FRONT_TABLET)
        ).forEach(pair -> {
            when(clientInterface.getDescription()).thenReturn(pair.left.name());
            assertSame(mapper.map(visitResponse), pair.right);
        });
    }

    @Test
    public void testMapClientInterfaceNull() {
        when(visitResponse.getClientInterface()).thenReturn(null);
        assertSame(mapper.map(visitResponse), WebInterface.OTHER);
    }

    @Test
    public void testMapUnknownInterface() {
        when(clientInterface.getDescription()).thenReturn("unknown");
        assertSame(mapper.map(visitResponse), WebInterface.OTHER);
    }

}
