package com.odigeo.frontend.commons.util;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.frontend.commons.util.ShoppingInfoUtils.INVALID_SHOPPING_ID;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class ShoppingInfoUtilsTest {
    private static final long VALID_SHOPPING_ID = 1234L;
    private static final String INVALID_SHOPPING_ID_TEST = "12345A";
    @Mock
    private ShoppingInfo shoppingInfo;
    @InjectMocks
    private ShoppingInfoUtils shoppingInfoUtils;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetShoppingId() {
        when(shoppingInfo.getShoppingId()).thenReturn(String.valueOf(VALID_SHOPPING_ID));
        assertEquals(shoppingInfoUtils.getShoppingId(shoppingInfo), VALID_SHOPPING_ID);
    }

    @Test
    public void testGetShoppingIdWhenInvalid() {
        when(shoppingInfo.getShoppingId()).thenReturn(INVALID_SHOPPING_ID_TEST);
        assertEquals(shoppingInfoUtils.getShoppingId(shoppingInfo), INVALID_SHOPPING_ID);
    }

    @Test
    public void testGetShoppingIdWhenNull() {
        when(shoppingInfo.getShoppingId()).thenReturn(INVALID_SHOPPING_ID_TEST);
        assertEquals(shoppingInfoUtils.getShoppingId(null), INVALID_SHOPPING_ID);
    }

    @Test
    public void testGetShoppingType() {
        when(shoppingInfo.getShoppingType()).thenReturn(ShoppingType.CART);
        assertEquals(shoppingInfoUtils.getShoppingType(shoppingInfo), ShoppingType.CART);
    }

    @Test
    public void testGetShoppingTypeWhenNull() {
        when(shoppingInfo.getShoppingType()).thenReturn(ShoppingType.CART);
        assertNull(shoppingInfoUtils.getShoppingType(null));
    }
}
