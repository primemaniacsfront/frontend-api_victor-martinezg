package com.odigeo.frontend.commons;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.services.TestTokenService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.function.Function;

import static com.odigeo.frontend.commons.SiteVariations.AIRLINE_STEERING_ALIAS;
import static com.odigeo.frontend.commons.SiteVariations.DPT_IP_TOUCHPOINT_ENABLED;
import static com.odigeo.frontend.commons.SiteVariations.FARE_FAMILY_DESKTOP_ENABLED;
import static com.odigeo.frontend.commons.SiteVariations.FARE_FAMILY_MOBILE_ENABLED;
import static com.odigeo.frontend.commons.SiteVariations.IS_AIRLINE_CAMPAIGN_ENABLED;
import static com.odigeo.frontend.commons.SiteVariations.IS_PRIME_DAY_CAMPAIGN_ENABLED;
import static com.odigeo.frontend.commons.SiteVariations.IS_PRIME_USER_FROM_COOKIE;
import static com.odigeo.frontend.commons.SiteVariations.MEMBERSHIP_DISPLAYED_META;
import static com.odigeo.frontend.commons.SiteVariations.MEMBERSHIP_RECAPTURE_M;
import static com.odigeo.frontend.commons.SiteVariations.MEMBERSHIP_SUBSCRIPTION_TIERS;
import static com.odigeo.frontend.commons.SiteVariations.PRIME_ENABLED;
import static com.odigeo.frontend.commons.SiteVariations.RESIDENT_DISCOUNT_IN_PAX;
import static com.odigeo.frontend.services.TestTokenService.PARTITION_B;
import static com.odigeo.frontend.services.TestTokenService.PARTITION_C;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SiteVariationsTest {
    @Mock
    private TestTokenService testTokenService;
    @Mock
    private VisitInformation visit;

    @InjectMocks
    private SiteVariations siteVariations;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testIsFareFamilyTestEnabledDesktop() {
        when(visit.getDevice()).thenReturn(Device.DESKTOP);
        when(testTokenService.find(eq(FARE_FAMILY_DESKTOP_ENABLED), eq(visit))).thenReturn(PARTITION_B);

        assertTrue(siteVariations.isFareFamilyTestEnabled(visit));
    }

    @Test
    public void testIsFareFamilyTestEnabledMobile() {
        when(visit.getDevice()).thenReturn(Device.MOBILE);
        when(testTokenService.find(eq(FARE_FAMILY_MOBILE_ENABLED), eq(visit))).thenReturn(PARTITION_C);

        assertTrue(siteVariations.isFareFamilyTestEnabled(visit));
    }

    @Test
    public void testIsFareFamilyTestDisabled() {
        when(visit.getDevice()).thenReturn(Device.OTHER);
        assertFalse(siteVariations.isFareFamilyTestEnabled(visit));
    }

    @Test
    public void testIsPrimeTestEnabled() {
        checkEnabled(PRIME_ENABLED, siteVariations::isPrimeTestEnabled);
    }

    @Test
    public void testIsMembershipDisplayedMeta() {
        checkEnabled(MEMBERSHIP_DISPLAYED_META, siteVariations::isMembershipDisplayedMeta);
    }

    @Test
    public void testIsMembershipSubscriptionTiers() {
        checkPartition(MEMBERSHIP_SUBSCRIPTION_TIERS, siteVariations::getMembershipSubscriptionTiers, PARTITION_C);
    }

    @Test
    public void testIsMembershipRecaptureM() {
        checkEnabled(MEMBERSHIP_RECAPTURE_M, siteVariations::isMembershipRecaptureM);
    }

    @Test
    public void testIsDPTouchPointEnabled() {
        checkEnabled(DPT_IP_TOUCHPOINT_ENABLED, siteVariations::isDptIpTouchPointEnabled);
    }

    @Test
    public void testIsResidentDiscountInPaxEnabled() {
        checkEnabled(RESIDENT_DISCOUNT_IN_PAX, siteVariations::isResidentDiscountInPax);
    }

    @Test
    public void testIsPrimeUserFromCookieEnabled() {
        checkEnabled(IS_PRIME_USER_FROM_COOKIE, siteVariations::isPrimeUserFromCookieEnabled);
    }

    @Test
    public void testIsPrimeDayCampaignBPartitionEnabled() {
        checkEnabled(IS_PRIME_DAY_CAMPAIGN_ENABLED, siteVariations::isPrimeDayCampaignEnabled);
    }

    @Test
    public void testIsPrimeDayCampaignCPartitionEnabled() {
        checkEnabled(IS_PRIME_DAY_CAMPAIGN_ENABLED, siteVariations::isPrimeDayCampaignEnabled, PARTITION_C);
    }

    @Test
    public void testIsAirlineCampaignEnabled() {
        checkEnabled(IS_AIRLINE_CAMPAIGN_ENABLED, siteVariations::isAirlineCampaignEnabled);
    }

    @Test
    public void testAirlineSteeringPartition() {
        Integer partition = Integer.valueOf(1);
        when(testTokenService.find(AIRLINE_STEERING_ALIAS, visit)).thenReturn(partition);
        assertEquals(siteVariations.getAirlineSteeringPartition(visit), partition);
    }

    private void checkEnabled(String alias, Function<VisitInformation, Boolean> findAlias) {
        checkEnabled(alias, findAlias, PARTITION_B);
    }

    private void checkEnabled(String alias, Function<VisitInformation, Boolean> findAlias, Integer partition) {
        when(testTokenService.find(alias, visit)).thenReturn(partition);
        assertTrue(findAlias.apply(visit));
    }

    private void checkPartition(String alias, Function<VisitInformation, Integer> findAlias, Integer partition) {
        when(testTokenService.find(alias, visit)).thenReturn(partition);
        assertEquals(findAlias.apply(visit), partition);
    }
}
