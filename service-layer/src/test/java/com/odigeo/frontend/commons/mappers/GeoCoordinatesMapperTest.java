package com.odigeo.frontend.commons.mappers;

import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.geoapi.v4.responses.Coordinates;
import com.odigeo.searchengine.v2.responses.gis.GeoCoordinates;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class GeoCoordinatesMapperTest {

    private GeoCoordinatesMapper mapper = new GeoCoordinatesMapper();

    @Test
    public void testMapGeoCoordinates() {
        GeoCoordinates geoCoordinates = new GeoCoordinates();
        geoCoordinates.setLatitude(BigDecimal.ONE);
        geoCoordinates.setLongitude(BigDecimal.ZERO);

        GeoCoordinatesDTO dto = mapper.map(geoCoordinates);

        assertEquals(dto.getLatitude(), geoCoordinates.getLatitude());
        assertEquals(dto.getLongitude(), geoCoordinates.getLongitude());
    }

    @Test
    public void testMapCoordinates() {
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(BigDecimal.ONE);
        coordinates.setLongitude(BigDecimal.ZERO);

        GeoCoordinatesDTO dto = mapper.map(coordinates);

        assertEquals(dto.getLatitude(), coordinates.getLatitude());
        assertEquals(dto.getLongitude(), coordinates.getLongitude());
    }

    @Test
    public void testMapNullValues() {
        GeoCoordinatesDTO dto = mapper.map(new GeoCoordinates());

        assertNull(dto.getLatitude());
        assertNull(dto.getLongitude());
    }

    @Test
    public void testMapHcsNull() {
        GeoCoordinatesDTO dto = mapper.mapHcs(null);
        assertNull(dto);

        HotelSummary hotelSummary = new HotelSummary();
        dto = mapper.mapHcs(hotelSummary);
        assertNull(dto);
    }

    @Test
    public void testMapHcs() {
        HotelSummary hotelSummary = new HotelSummary();
        hotelSummary.setLatitude(1F);
        hotelSummary.setLongitude(2F);
        GeoCoordinatesDTO dto = mapper.mapHcs(hotelSummary);
        assertSame(dto.getLatitude(), new BigDecimal(hotelSummary.getLatitude()));
        assertSame(dto.getLongitude(), new BigDecimal(hotelSummary.getLongitude()));
    }

}
