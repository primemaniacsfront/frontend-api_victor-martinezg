package com.odigeo.frontend.commons.exception;

import org.apache.commons.lang.StringUtils;
import org.testng.annotations.Test;

public class RsaExceptionTest {

    @Test(expectedExceptions = RsaException.class)
    public void exceptionThrown() throws RsaException {
        throw new RsaException(StringUtils.EMPTY, new RuntimeException());
    }

}
