package com.odigeo.frontend.commons.context;

import bean.test.BeanTest;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import javax.servlet.http.Cookie;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class RequestInfoTest extends BeanTest<RequestInfo> {

    private static final String COOKIE_NAME = "cookieName";
    private static final String COOKIE_VALUE = "cookieValue";
    private static final String HEADER_NAME = "headerName";

    @Mock
    private Cookie cookie;
    @Mock
    private SiteCookies siteCookie;
    @Mock
    private SiteHeaders siteHeader;

    private RequestInfo requestInfo;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        requestInfo = new RequestInfo();

        when(cookie.getName()).thenReturn(COOKIE_NAME);
        when(cookie.getValue()).thenReturn(COOKIE_VALUE);
        when(siteCookie.value()).thenReturn(COOKIE_NAME);
        when(siteHeader.value()).thenReturn(HEADER_NAME);
    }

    @Override
    public RequestInfo getBean() {
        requestInfo.setRequestUrl("url");
        requestInfo.setServerName("server");
        requestInfo.setContextPath("contextPath");

        return requestInfo;
    }

    @Test
    public void testGetCookie() {
        requestInfo.addCookie(cookie);
        assertSame(requestInfo.getCookie(siteCookie), cookie);
    }

    @Test
    public void testGetCookieValue() {
        requestInfo.addCookie(cookie);
        assertEquals(requestInfo.getCookieValue(siteCookie), COOKIE_VALUE);
    }

    @Test
    public void testGetCookieValueNotExists() {
        assertNull(requestInfo.getCookieValue(siteCookie));
    }

    @Test
    public void testGetHeader() {
        String headerValue = "value";

        requestInfo.addHeader(siteHeader, headerValue);
        assertEquals(requestInfo.getHeader(siteHeader), headerValue);
    }

    @Test
    public void testGetHeaderOrCookieWithHeader() {
        String headerValue = "value";

        requestInfo.addHeader(siteHeader, headerValue);
        assertEquals(requestInfo.getHeaderOrCookie(siteHeader, siteCookie), headerValue);
    }

    @Test
    public void testGetHeaderOrCookieWithCookie() {
        requestInfo.addCookie(cookie);
        assertEquals(requestInfo.getHeaderOrCookie(siteHeader, siteCookie), COOKIE_VALUE);
    }

    @Test
    public void testGetHeaderOrCookieWithHeaderNull() {
        requestInfo.addHeader(siteHeader, "null");
        requestInfo.addCookie(cookie);
        assertEquals(requestInfo.getHeaderOrCookie(siteHeader, siteCookie), COOKIE_VALUE);
    }

}
