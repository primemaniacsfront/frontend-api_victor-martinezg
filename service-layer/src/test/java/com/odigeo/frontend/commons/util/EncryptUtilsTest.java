package com.odigeo.frontend.commons.util;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.frontend.commons.exception.RsaException;
import com.odigeo.frontend.services.configurations.rsa.RsaKeyConfig;
import org.apache.commons.lang.StringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.util.Base64;

import static com.odigeo.frontend.services.configurations.rsa.RsaKeyConfig.RSA;
import static org.testng.Assert.assertEquals;

public class EncryptUtilsTest {
    private static final String MODULUS_ENCRYPTION = "224418455667919760921969993712549894470527399333200779274189071052"
            + "103590585511690335772191411908426480686548094386334388591922838308097613829046126332522148524809179414862176969"
            + "577403362979397734856612958131305259486027628624616945428548988235825756785849393506701587807308149137709463731"
            + "341000380548398213965845471499917193083902990376155699766520388169999566293694076791490463632160423846024538835"
            + "937986305851199399159159915135443234510002328268238941884201684222151320246668050139817612747331829548379084052"
            + "21336550462802644536522491806115880439047895847889174937933216678991806037404489355376007152626388636005343";
    private static final String EXPONENT_ENCRYPTION = "65537";
    private static final String E_DECRYPTION = "670683716482578762747410515716923605459473220217597763532701702187543055"
            + "862769422908652233241319932412336043910562086353504341167752933436754153749843413461251951201309106397626705986"
            + "277570606532935841036049826023817589626354844972025506276391486224942802725364642461326264157500522995657726907"
            + "465007694496036692462518433706500217408012231779851424786801261617011625230858465594683447915279570967849100187"
            + "306634930469037355751090075681265846450042164287161430329156214269203329491202302419636813937768207292100870226"
            + "0937860995229503222729770681772830711368791110990162772195745302878299717661080682945304167070005377";
    private static final String M_DECRYPTION = "224418455667919760921969993712549894470527399333200779274189071052103590"
            + "585511690335772191411908426480686548094386334388591922838308097613829046126332522148524809179414862176969577403"
            + "362979397734856612958131305259486027628624616945428548988235825756785849393506701587807308149137709463731341000"
            + "380548398213965845471499917193083902990376155699766520388169999566293694076791490463632160423846024538835937986"
            + "305851199399159159915135443234510002328268238941884201684222151320246668050139817612747331829548379084052213365"
            + "50462802644536522491806115880439047895847889174937933216678991806037404489355376007152626388636005343";

    private static final String MESSAGE = "bookingid=1234567890&email=mytrips.qa@odigeo.com";
    private static final RsaKeyConfig RSA_KEY_CONFIG = new RsaKeyConfig();

    @BeforeMethod
    private void setUpTest() {
        ConfigurationEngine.init(binder -> binder.bind(RsaKeyConfig.class).toInstance(RSA_KEY_CONFIG));
        RSA_KEY_CONFIG.setModulusE(MODULUS_ENCRYPTION);
        RSA_KEY_CONFIG.setExponentE(EXPONENT_ENCRYPTION);
        RSA_KEY_CONFIG.setModulusD(M_DECRYPTION);
        RSA_KEY_CONFIG.setExponentD(E_DECRYPTION);
    }

    @Test
    public void encryptOK() throws RsaException {
        byte[] encryptedMessage = ConfigurationEngine.getInstance(EncryptUtils.class).encrypt(MESSAGE.getBytes());
        assertEquals(MESSAGE, decryptMessage(encryptedMessage));
    }

    @Test
    public void encryptUrlSafeOK() throws RsaException {
        String encryptedMessageUrlSafe = ConfigurationEngine.getInstance(EncryptUtils.class)
                .encryptBase64URLSafe(MESSAGE.getBytes());
        assertEquals(MESSAGE, decryptMessage(Base64.getUrlDecoder().decode(encryptedMessageUrlSafe)));
    }

    @Test(expectedExceptions = RsaException.class)
    public void encryptThrowsRsaException() throws RsaException {
        byte[] message = new byte[512];
        ConfigurationEngine.getInstance(EncryptUtils.class).encrypt(message);
    }

    private String decryptMessage(byte[] message) {
        try {
            Cipher cipher = Cipher.getInstance(RSA);
            cipher.init(Cipher.DECRYPT_MODE, KeyFactory.getInstance(RSA)
                    .generatePrivate(new RSAPrivateKeySpec(new BigInteger(M_DECRYPTION),
                            new BigInteger(E_DECRYPTION))));
            return new String(cipher.doFinal(message), StandardCharsets.UTF_8.name());

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeySpecException
                | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | UnsupportedEncodingException e) {
            return StringUtils.EMPTY;
        }
    }
}
