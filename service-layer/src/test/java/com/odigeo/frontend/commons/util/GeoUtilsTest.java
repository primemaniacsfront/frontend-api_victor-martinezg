package com.odigeo.frontend.commons.util;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoServiceException;
import com.odigeo.geoapi.v4.InvalidParametersException;
import com.odigeo.geoapi.v4.responses.Airport;
import com.odigeo.geoapi.v4.responses.City;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.ZoneId;
import java.util.TimeZone;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

public class GeoUtilsTest {

    @Mock
    private GeoService geoService;
    @Mock
    private Logger logger;

    @InjectMocks
    private GeoUtils geoUtils;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetZoneIdFromAirportIata() throws GeoNodeNotFoundException, InvalidParametersException, GeoServiceException {
        City city = new City();
        city.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        Airport airport = new Airport();
        airport.setCity(city);
        when(geoService.getAirportByIataCode(any())).thenReturn(airport);

        assertEquals(geoUtils.getZoneIdFromAirportIata("LAX"), ZoneId.of("America/Los_Angeles"));
    }

    @Test
    public void testetZoneIdFromAirportIataReturnsUTCAsDefault() throws GeoNodeNotFoundException, InvalidParametersException, GeoServiceException {
        when(geoService.getAirportByIataCode(any())).thenThrow(new GeoNodeNotFoundException(""));
        assertEquals(geoUtils.getZoneIdFromAirportIata("XXX"), ZoneId.of("UTC"));
    }
}
