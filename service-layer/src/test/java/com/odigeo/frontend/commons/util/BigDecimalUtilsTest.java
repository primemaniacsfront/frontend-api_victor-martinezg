package com.odigeo.frontend.commons.util;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.testng.Assert.assertEquals;

public class BigDecimalUtilsTest {

    private static final BigDecimal VALUE_120 = new BigDecimal(120);
    private static final BigDecimal VALUE_116 = new BigDecimal(116);
    private static final BigDecimal VALUE_RESULT_3_33 = new BigDecimal(3.33).setScale(2, RoundingMode.HALF_DOWN);
    private static final BigDecimal VALUE_RESULT_100 = new BigDecimal(100).setScale(2, RoundingMode.HALF_DOWN);

    BigDecimalUtils utils = new BigDecimalUtils();

    @Test
    public void testDifferenceInPercentage() {
        assertEquals(utils.differenceInPercentage(VALUE_120, VALUE_116), VALUE_RESULT_3_33);
    }

    @Test
    public void testDifferenceInPercentageSymmetrical() {
        assertEquals(utils.differenceInPercentage(VALUE_116, VALUE_120), VALUE_RESULT_3_33);
    }

    @Test
    public void testDifferenceInPercentageFirstNull() {
        assertEquals(utils.differenceInPercentage(null, VALUE_116), BigDecimal.ZERO);
    }

    @Test
    public void testDifferenceInPercentageSecondNull() {
        assertEquals(utils.differenceInPercentage(VALUE_120, null), BigDecimal.ZERO);
    }

    @Test
    public void testDifferenceInPercentageFirstZero() {
        assertEquals(utils.differenceInPercentage(VALUE_120, BigDecimal.ZERO), VALUE_RESULT_100);
    }

    @Test
    public void testDifferenceInPercentageSecondZero() {
        assertEquals(utils.differenceInPercentage(BigDecimal.ZERO, VALUE_116), VALUE_RESULT_100);
    }

    @Test
    public void testDifferenceInPercentageBothZero() {
        assertEquals(utils.differenceInPercentage(BigDecimal.ZERO, BigDecimal.ZERO), BigDecimal.ZERO);
    }

}
