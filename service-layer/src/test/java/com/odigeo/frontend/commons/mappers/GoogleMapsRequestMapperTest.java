package com.odigeo.frontend.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapImageFormat;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapParam;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapStaticConfiguration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Size;
import com.google.maps.GeoApiContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertTrue;

public class GoogleMapsRequestMapperTest {

    @Mock
    private MapStaticConfiguration request;
    @Mock
    private GeoApiContext context;

    @InjectMocks
    private GoogleMapsRequestMapper googleMapsRequestMapper;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        Size size = new Size(1, 2);
        when(request.getSize()).thenReturn(size);
        when(request.getScale()).thenReturn(null);
    }

    @Test
    public void testMapGoogleMapsStaticRequest() {
        when(request.getZoom()).thenReturn(2);
        when(request.getMarkerUrl()).thenReturn("markerUtl");
        when(request.getScale()).thenReturn(1);
        when(request.getFormat()).thenReturn(MapImageFormat.GIF);
        when(request.getMapType()).thenReturn(MapType.TERRAIN);
        when(request.getAdditionalParams()).thenReturn(Collections.singletonList(new MapParam("style", "color:red")));
        googleMapsRequestMapper.mapGoogleMapsStaticRequest(request, 1, 2, context);
        verify(request).getSize();
        verify(request).getZoom();
        verify(request).getMarkerUrl();
        verify(request).getScale();
        verify(request).getFormat();
        verify(request).getMapType();
        verify(request).getAdditionalParams();
    }

    @Test
    public void testMapGoogleMapsStaticRequestCustomImageFormat() {
        when(request.getFormat()).thenReturn(MapImageFormat.JPG_BASELINE);
        googleMapsRequestMapper.mapGoogleMapsStaticRequest(request, 1, 2, context);
        verify(request).getFormat();
    }

    @Test
    public void testMapGoogleMapsStaticRequestWrongScale() {
        when(request.getScale()).thenReturn(8);
        try {
            googleMapsRequestMapper.mapGoogleMapsStaticRequest(request, 1, 2, context);
        } catch (ServiceException e) {
            assertTrue(e.getMessage().contains("Allowed values :1, 2, 4"));
        }
    }

}
