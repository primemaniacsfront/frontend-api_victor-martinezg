package com.odigeo.frontend.commons.context;

import bean.test.BeanTest;

public class DebugInfoTest extends BeanTest<DebugInfo> {

    @Override
    public DebugInfo getBean() {
        DebugInfo bean = new DebugInfo();
        bean.setEnabled(true);
        bean.setQaModeEnabled(true);
        bean.setApiCaptureEnabled(true);

        return bean;
    }

}
