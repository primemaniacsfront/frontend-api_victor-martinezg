package com.odigeo.frontend.commons.util;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.frontend.commons.util.DocumentValidatorUtils.SPAIN;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

public class DocumentValidatorUtilsTest {

    private static final String VALID_CIF_TWO_LETTERS = "R2485053I";
    private static final String VALID_CIF_ONE_LETTER = "C01361948";

    private DocumentValidatorUtils documentValidatorUtils;

    @BeforeMethod
    public void setUp() {
        documentValidatorUtils = new DocumentValidatorUtils();
    }

    @Test
    public void cifValidatorOK() {
        assertTrue(documentValidatorUtils.isValidCIF(VALID_CIF_TWO_LETTERS, SPAIN));
        assertTrue(documentValidatorUtils.isValidCIF(VALID_CIF_ONE_LETTER, SPAIN));
    }

    @Test
    public void cifValidatorWrongSize() {
        assertFalse(documentValidatorUtils.isValidCIF("R248505I", SPAIN));
    }

    @Test
    public void cifValidatorKO() {
        assertFalse(documentValidatorUtils.isValidCIF("C01361949", SPAIN));
    }

    @Test
    public void isNecessaryValidateIdLowerCase() {
        assertTrue(documentValidatorUtils.isValidCIF(VALID_CIF_TWO_LETTERS, "es"));
    }

    @Test
    public void isNotNecessaryValidateId() {
        assertTrue(documentValidatorUtils.isValidCIF("WRONG", "de"));
    }
}
