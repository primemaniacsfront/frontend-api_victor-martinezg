package com.odigeo.frontend.commons.context;

import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.DebugModeConfiguration;
import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.DebugModeConfigurationSerializer;
import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.exception.SerializeException;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.visit.VisitDeserializer;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.monitoring.MetricsHandler;
import org.apache.commons.lang3.ArrayUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class ResolverContextInitializerTest {

    @Mock
    private VisitDeserializer visitDeserializer;
    @Mock
    private DebugModeConfigurationSerializer debugModeConfigurationSerializer;

    @InjectMocks
    private ResolverContextInitializer initializer;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateContext() {
        HttpServletRequest servletRequest = mock(HttpServletRequest.class);
        DebugInfo debugInfo = mock(DebugInfo.class);

        String requestUrl = "url";
        String serverName = "serverName";
        String ctxPath = "path";
        when(servletRequest.getRequestURL()).thenReturn(new StringBuffer(requestUrl));
        when(servletRequest.getServerName()).thenReturn(serverName);
        when(servletRequest.getContextPath()).thenReturn(ctxPath);

        Cookie cookie = mock(Cookie.class);
        SiteCookies siteCookie = mock(SiteCookies.class);
        String cookieName = "cookieName";
        when(cookie.getName()).thenReturn(cookieName);
        when(siteCookie.value()).thenReturn(cookieName);
        when(servletRequest.getCookies()).thenReturn(ArrayUtils.toArray(cookie));

        SiteHeaders siteHeader = SiteHeaders.VISIT_INFORMATION;
        String headerName = siteHeader.value();
        String headerValue = "headerValue";
        Enumeration<String> headers = Collections.enumeration(Collections.singletonList(headerName));
        when(servletRequest.getHeaderNames()).thenReturn(headers);
        when(servletRequest.getHeader(headerName)).thenReturn(headerValue);

        VisitInformation visit = mock(VisitInformation.class);
        when(visitDeserializer.deserialize(headerValue)).thenReturn(visit);

        ResolverContext context = initializer.createContext(servletRequest, debugInfo);

        RequestInfo requestInfo = context.getRequestInfo();
        assertEquals(requestInfo.getRequestUrl(), requestUrl);
        assertEquals(requestInfo.getServerName(), serverName);
        assertEquals(requestInfo.getContextPath(), ctxPath);
        assertSame(requestInfo.getCookie(siteCookie), cookie);
        assertEquals(requestInfo.getHeader(siteHeader), headerValue);

        assertSame(context.getVisitInformation(), visit);
        assertSame(context.getDebugInfo(), debugInfo);

        assertNotNull(context.getResponseInfo());
    }

    @Test
    public void testCreateContextMap() throws SerializeException {
        HttpServletRequest servletRequest = mock(HttpServletRequest.class);

        String requestUrl = "url";
        String serverName = "serverName";
        when(servletRequest.getRequestURL()).thenReturn(new StringBuffer(requestUrl));
        when(servletRequest.getServerName()).thenReturn(serverName);

        SiteCookies siteCookie = SiteCookies.DEBUG_CONFIGURATION;
        String cookieName = siteCookie.value();
        String cookieValue = "headerValue";
        Cookie cookie = new Cookie(cookieName, cookieValue);
        Cookie[] cookies = Collections.singletonList(cookie).toArray(new Cookie[0]);

        when(servletRequest.getCookies()).thenReturn(cookies);

        SiteHeaders siteHeader = SiteHeaders.VISIT_INFORMATION;
        String headerName = siteHeader.value();
        String headerValue = "headerValue";
        Enumeration<String> headers = Collections.enumeration(Collections.singletonList(headerName));
        when(servletRequest.getHeaderNames()).thenReturn(headers);
        when(servletRequest.getHeader(headerName)).thenReturn(headerValue);

        VisitInformation visit = mock(VisitInformation.class);
        when(visit.getVisitId()).thenReturn(1L);
        when(visitDeserializer.deserialize(headerValue)).thenReturn(visit);

        DebugModeConfiguration debugModeConfiguration = mock(DebugModeConfiguration.class);
        when(debugModeConfiguration.getVisitId()).thenReturn(1L);
        when(debugModeConfigurationSerializer.deserialize(anyString())).thenReturn(debugModeConfiguration);
        Map<Class<?>, Object> contextMap = initializer.createContextMap(servletRequest);

        assertNotNull(contextMap.get(MetricsHandler.class));
        assertNotNull(contextMap.get(VisitInformation.class));
        assertNotNull(contextMap.get(DebugModeConfiguration.class));
    }

    @Test
    public void testCreateContextMapWithDifferentVisitIdDebug() throws SerializeException {
        HttpServletRequest servletRequest = mock(HttpServletRequest.class);

        String requestUrl = "url";
        String serverName = "serverName";
        when(servletRequest.getRequestURL()).thenReturn(new StringBuffer(requestUrl));
        when(servletRequest.getServerName()).thenReturn(serverName);

        SiteCookies siteCookie = SiteCookies.DEBUG_CONFIGURATION;
        String cookieName = siteCookie.value();
        String cookieValue = "headerValue";
        Cookie cookie = new Cookie(cookieName, cookieValue);
        Cookie[] cookies = Collections.singletonList(cookie).toArray(new Cookie[0]);

        when(servletRequest.getCookies()).thenReturn(cookies);

        SiteHeaders siteHeader = SiteHeaders.VISIT_INFORMATION;
        String headerName = siteHeader.value();
        String headerValue = "headerValue";
        Enumeration<String> headers = Collections.enumeration(Collections.singletonList(headerName));
        when(servletRequest.getHeaderNames()).thenReturn(headers);
        when(servletRequest.getHeader(headerName)).thenReturn(headerValue);

        VisitInformation visit = mock(VisitInformation.class);
        when(visit.getVisitId()).thenReturn(1L);
        when(visitDeserializer.deserialize(headerValue)).thenReturn(visit);

        DebugModeConfiguration debugModeConfiguration = mock(DebugModeConfiguration.class);
        when(debugModeConfiguration.getVisitId()).thenReturn(2L);
        when(debugModeConfigurationSerializer.deserialize(anyString())).thenReturn(debugModeConfiguration);
        Map<Class<?>, Object> contextMap = initializer.createContextMap(servletRequest);

        assertNotNull(contextMap.get(MetricsHandler.class));
        assertNotNull(contextMap.get(VisitInformation.class));
        assertNull(contextMap.get(DebugModeConfiguration.class));
    }

    @Test
    public void testCreateContextMapWithoutDebug() throws SerializeException {
        HttpServletRequest servletRequest = mock(HttpServletRequest.class);

        String requestUrl = "url";
        String serverName = "serverName";
        when(servletRequest.getRequestURL()).thenReturn(new StringBuffer(requestUrl));
        when(servletRequest.getServerName()).thenReturn(serverName);

        SiteHeaders siteHeader = SiteHeaders.VISIT_INFORMATION;
        String headerName = siteHeader.value();
        String headerValue = "headerValue";
        Enumeration<String> headers = Collections.enumeration(Collections.singletonList(headerName));
        when(servletRequest.getHeaderNames()).thenReturn(headers);
        when(servletRequest.getHeader(headerName)).thenReturn(headerValue);

        VisitInformation visit = mock(VisitInformation.class);
        when(visitDeserializer.deserialize(headerValue)).thenReturn(visit);

        when(debugModeConfigurationSerializer.deserialize(anyString())).thenReturn(new DebugModeConfiguration());
        Map<Class<?>, Object> contextMap = initializer.createContextMap(servletRequest);

        assertNotNull(contextMap.get(MetricsHandler.class));
        assertNotNull(contextMap.get(VisitInformation.class));
        assertNull(contextMap.get(DebugModeConfiguration.class));
    }

}
