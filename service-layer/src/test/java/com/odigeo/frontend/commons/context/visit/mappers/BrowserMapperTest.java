package com.odigeo.frontend.commons.context.visit.mappers;

import com.odigeo.frontend.commons.context.visit.types.Browser;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.useragent.UserAgentInfo;
import com.odigeo.visitengineapi.v1.useragent.UserAgentInfoKey;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class BrowserMapperTest {

    private static final String CHROME = "CHROME";
    private static final String MOZILLA = "MOZILLA";
    private static final String INTERNET_EXPLORER = "INTERNET_EXPLORER";
    private static final String EDGE = "EDGE";
    private static final String SAFARI = "SAFARI";

    @Mock
    private VisitResponse visitResponse;
    @Mock
    private UserAgentInfo userAgentInfo;
    @Mock
    private UserAgentInfoKey userAgentInfoKey;

    private BrowserMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new BrowserMapper();

        when(visitResponse.getUserAgentInfo()).thenReturn(userAgentInfo);
        when(userAgentInfo.getUserAgentInfoKey()).thenReturn(userAgentInfoKey);
    }

    @Test
    public void testMap() {
        Stream.of(
            new ImmutablePair<>(CHROME, Browser.CHROME),
            new ImmutablePair<>(MOZILLA, Browser.FIREFOX),
            new ImmutablePair<>(INTERNET_EXPLORER, Browser.IE),
            new ImmutablePair<>(EDGE, Browser.EDGE),
            new ImmutablePair<>(SAFARI, Browser.SAFARI),
            new ImmutablePair<>("unknown", Browser.OTHER)
        ).forEach(pair -> {
            when(userAgentInfoKey.getUserAgentFamily()).thenReturn(pair.left);
            assertSame(mapper.map(visitResponse), pair.right);
        });
    }

    @Test
    public void testMapUserAgentInfoNull() {
        when(visitResponse.getUserAgentInfo()).thenReturn(null);
        assertSame(mapper.map(visitResponse), Browser.OTHER);
    }

    @Test
    public void testMapUserAgentInfoKeyNull() {
        when(userAgentInfo.getUserAgentInfoKey()).thenReturn(null);
        assertSame(mapper.map(visitResponse), Browser.OTHER);
    }

}
