package com.odigeo.frontend.commons.exception;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ErrorCodesTest {

    @Test
    public void testEnum() {
        ErrorCodes error = ErrorCodes.INTERNAL_GENERIC;
        String errorCode = "INT-01";
        String errorDescription = "Internal error";

        assertEquals(error.getCode(), errorCode);
        assertEquals(error.getDescription(), errorDescription);
    }

}
