package com.odigeo.frontend.commons.context.visit;

import bean.test.BeanTest;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Browser;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.OS;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import java.util.HashMap;

import static org.mockito.Mockito.mock;

public class VisitInformationTest extends BeanTest<VisitInformation> {

    @Override
    public VisitInformation getBean() {
        VisitInformation bean = new VisitInformation();

        bean.setVisitId(1L);
        bean.setVisitCode("visitCode");

        bean.setSite(mock(Site.class));
        bean.setBrand(mock(Brand.class));
        bean.setDevice(mock(Device.class));
        bean.setBrowser(mock(Browser.class));
        bean.setOs(mock(OS.class));
        bean.setWebInterface(mock(WebInterface.class));

        bean.setLocale("locale");
        bean.setCurrency("currency");
        bean.setMktPortal("mktPortal");
        bean.setWebsiteDefaultCountry("defaultCountry");
        bean.setUserIp("userIp");
        bean.setUserDevice("userDevice");

        bean.setDimensionPartitionMap(new HashMap<>(0));

        return bean;
    }

}
