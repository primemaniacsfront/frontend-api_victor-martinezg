package com.odigeo.frontend.commons.util;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertNull;

public class DateUtilsTest {

    private DateUtils dateUtils;

    @BeforeMethod
    public void init() {
        dateUtils = new DateUtils();
    }

    @Test
    public void testConvertFromCalendar() {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneOffset.UTC);
        Calendar calendar = Calendar.getInstance();
        String calendarFormatted = formatter.format(calendar.getTime().toInstant());

        ZonedDateTime zonedDateTime = dateUtils.convertFromCalendar(calendar);
        assertEquals(zonedDateTime.format(formatter), calendarFormatted);
    }

    @Test
    public void testConvetFromIsoToCalendar() {
        String date = "2019-12-01";

        Calendar calendar = dateUtils.convertFromIsoToCalendar(date);

        assertEquals(calendar.get(Calendar.YEAR), 2019);
        assertEquals(calendar.get(Calendar.MONTH), Calendar.DECEMBER);
        assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 1);
    }

    @Test
    public void testConvertFromIsoToLocalDate() {
        String date = "2019-12-01";

        LocalDate localDate = dateUtils.convertFromIsoToLocalDate(date);

        assertEquals(localDate.getYear(), 2019);
        assertEquals(localDate.getMonth(), Month.DECEMBER);
        assertEquals(localDate.getDayOfMonth(), 1);
    }

    @Test
    public void testIsBeforeOrEqualsIsEquals() {
        LocalDate date = LocalDate.now();
        assertTrue(dateUtils.isBeforeOrEquals(date, date));
    }

    @Test
    public void testIsBeforeOrEqualsIsBefore() {
        LocalDate date = LocalDate.now();
        LocalDate comparedDate = date.plus(1, ChronoUnit.DAYS);
        assertTrue(dateUtils.isBeforeOrEquals(date, comparedDate));
    }

    @Test
    public void testIsBeforeOrEqualsIsAfter() {
        LocalDate date = LocalDate.now();
        LocalDate comparedDate = date.minus(1, ChronoUnit.DAYS);
        assertFalse(dateUtils.isBeforeOrEquals(date, comparedDate));
    }

    @Test
    public void testFormatToOffsetDateTime() {
        String datetime = dateUtils.formatToOffsetDateTime("1990-12-11", "12:00", ZoneId.of("America/Los_Angeles"));
        assertEquals(datetime, "1990-12-11T12:00:00-08:00");
    }

    @Test
    public void testFormatToOffsetDateTimeEmptyValuesReturnsNull() {
        String datetime = dateUtils.formatToOffsetDateTime(null, null, null);
        assertNull(datetime);
    }

}
