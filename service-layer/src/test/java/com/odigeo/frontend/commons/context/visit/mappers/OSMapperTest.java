package com.odigeo.frontend.commons.context.visit.mappers;

import com.odigeo.frontend.commons.context.visit.types.OS;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.useragent.UserAgentInfo;
import com.odigeo.visitengineapi.v1.useragent.UserAgentInfoKey;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class OSMapperTest {

    private static final String WINDOWS = "WINDOWS";
    private static final String MAC = "MAC";
    private static final String POSIX = "POSIX";
    private static final String ANDROID = "ANDROID";
    private static final String IOS = "IOS";

    @Mock
    private VisitResponse visitResponse;
    @Mock
    private UserAgentInfo userAgentInfo;
    @Mock
    private UserAgentInfoKey userAgentInfoKey;

    private OSMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new OSMapper();

        when(visitResponse.getUserAgentInfo()).thenReturn(userAgentInfo);
        when(userAgentInfo.getUserAgentInfoKey()).thenReturn(userAgentInfoKey);
    }

    @Test
    public void testMap() {
        Stream.of(
            new ImmutablePair<>(WINDOWS, OS.WINDOWS),
            new ImmutablePair<>(MAC, OS.MAC),
            new ImmutablePair<>(POSIX, OS.POSIX),
            new ImmutablePair<>(ANDROID, OS.ANDROID),
            new ImmutablePair<>(IOS, OS.IOS),
            new ImmutablePair<>("unknown", OS.OTHER)
        ).forEach(pair -> {
            when(userAgentInfoKey.getOsType()).thenReturn(pair.left);
            assertSame(mapper.map(visitResponse), pair.right);
        });
    }

    @Test
    public void testMapNullUserAgentInfo() {
        when(visitResponse.getUserAgentInfo()).thenReturn(null);
        assertSame(mapper.map(visitResponse), OS.OTHER);
    }

    @Test
    public void testMapNullUserAgentInfoKey() {
        when(userAgentInfo.getUserAgentInfoKey()).thenReturn(null);
        assertSame(mapper.map(visitResponse), OS.OTHER);
    }

}
