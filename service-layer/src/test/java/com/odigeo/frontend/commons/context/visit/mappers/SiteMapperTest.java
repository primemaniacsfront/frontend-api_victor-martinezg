package com.odigeo.frontend.commons.context.visit.mappers;

import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import com.odigeo.visitengineapi.v1.website.Website;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class SiteMapperTest {

    @Mock
    private VisitResponse visitResponse;
    @Mock
    private Website website;

    private SiteMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new SiteMapper();

        when(visitResponse.getWebsite()).thenReturn(website);
    }

    @Test
    public void testMap() {
        Site site = Site.ES;

        when(website.getCode()).thenReturn(site.name());
        assertSame(mapper.map(visitResponse), site);
    }

    @Test
    public void testMapWebsiteNull() {
        when(visitResponse.getWebsite()).thenReturn(null);
        assertSame(mapper.map(visitResponse), Site.UNKNOWN);
    }

    @Test
    public void testMapWebsiteUnknown() {
        when(website.getCode()).thenReturn("unknown");
        assertSame(mapper.map(visitResponse), Site.UNKNOWN);
    }

}
