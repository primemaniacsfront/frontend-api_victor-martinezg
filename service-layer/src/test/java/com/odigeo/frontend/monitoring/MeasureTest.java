package com.odigeo.frontend.monitoring;

import com.odigeo.commons.monitoring.metrics.Buckets;
import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import java.util.Collections;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class MeasureTest {

    private static final String NAME = "name";

    private static final String TAG_VALUE = "tagValue";
    private static final String TAG_LABEL = "tagLabel";
    private static final String DIRECT_VALUE = "directValue";
    private static final String DIRECT_LABEL = "directLabel";

    private Measure.Builder measureBuilder;

    @BeforeMethod
    public void init() {
        measureBuilder = new Measure.Builder(NAME);
    }

    @Test
    public void testGetMetric() {
        MeasureTags tag = mock(MeasureTags.class);
        Buckets buckets = mock(Buckets.class);
        VisitInformation visit = mock(VisitInformation.class);

        when(tag.getValue(eq(visit))).thenReturn(TAG_VALUE);
        when(tag.getLabel()).thenReturn(TAG_LABEL);

        Measure measure = measureBuilder.tag(tag).buckets(buckets).build();
        Metric metric = measure.getMetric(visit, Collections.singletonMap(DIRECT_LABEL, DIRECT_VALUE));

        assertEquals(metric.getId(), DIRECT_LABEL + Metric.SEPARATOR
            + DIRECT_VALUE + Metric.SEPARATOR
            + TAG_LABEL + Metric.SEPARATOR
            + TAG_VALUE + Metric.SEPARATOR
            + NAME);

        assertSame(metric.getBuckets(), buckets);
    }

    @Test
    public void testGetMetricWithoutVisit() {
        MeasureTags tag = mock(MeasureTags.class);
        Buckets buckets = mock(Buckets.class);

        when(tag.getLabel()).thenReturn(TAG_LABEL);

        Measure measure = measureBuilder.tag(tag).buckets(buckets).build();
        Metric metric = measure.getMetric(Collections.singletonMap(DIRECT_LABEL, DIRECT_VALUE));

        assertEquals(metric.getId(), DIRECT_LABEL + Metric.SEPARATOR
                + DIRECT_VALUE + Metric.SEPARATOR
                + NAME);

        assertSame(metric.getBuckets(), buckets);
    }

    @Test
    public void testGetName() {
        Measure measure = measureBuilder.build();
        assertEquals(measure.getName(), NAME);
    }

}
