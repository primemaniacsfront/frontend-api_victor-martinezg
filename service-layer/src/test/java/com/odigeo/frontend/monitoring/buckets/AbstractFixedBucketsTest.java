package com.odigeo.frontend.monitoring.buckets;

import com.odigeo.commons.monitoring.metrics.bucket.Bucket;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class AbstractFixedBucketsTest {

    @Mock
    private BucketCalculator calculator;

    private AbstractFixedBuckets buckets;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        buckets = () -> calculator;
    }

    @Test
    public void testGetBucketName() {
        long value = 1L;

        buckets.getBucketName(value);
        verify(calculator).calculate(eq(value));
    }

    @Test
    public void testGetBucketSet() {
        Bucket bucket = mock(Bucket.class);
        when(calculator.getBuckets()).thenReturn(ArrayUtils.toArray(bucket));

        Set<Bucket> bucketSet = buckets.getBucketSet();
        assertEquals(bucketSet.size(), 1);
        assertTrue(bucketSet.contains(bucket));
    }

}
