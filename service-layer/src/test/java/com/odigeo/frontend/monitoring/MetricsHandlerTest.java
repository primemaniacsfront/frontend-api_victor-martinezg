package com.odigeo.frontend.monitoring;

import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MetricsHandlerTest {

    @Mock
    private VisitInformation visit;
    @Mock
    private Measure measure;
    @Mock
    private Metric metric;

    private MetricsHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = spy(MetricsHandler.class);

        when(measure.getMetric(eq(visit), any())).thenReturn(metric);

        doNothing().when(handler).addValueToHistogram(eq(metric), anyLong());
        doNothing().when(handler).incrementCounter(eq(metric));
    }

    @Test
    public void testTrackTime() {
        long time = 1L;

        handler.trackTime(measure, time, visit);
        verify(handler).addValueToHistogram(eq(metric), eq(time));
        verify(measure).getMetric(eq(visit), eq(Collections.emptyMap()));
    }

    @Test
    public void testTrackTimeWithDirectValues() {
        long time = 1L;
        Map<String, String> directTags = new HashMap<>(0);

        handler.trackTime(measure, time, directTags, visit);
        verify(handler).addValueToHistogram(eq(metric), eq(time));
        verify(measure).getMetric(eq(visit), eq(directTags));
    }

    @Test
    public void testTrackTimeWithoutTime() {
        long time = 0L;

        handler.trackTime(measure, time, new HashMap<>(0), visit);
        verify(handler, never()).addValueToHistogram(eq(metric), eq(time));
    }

    @Test
    public void testStartMetric() {
        handler.startMetric(measure);
        handler.stopMetric(measure, visit);

        verify(handler).trackTime(eq(measure), anyLong(), eq(Collections.emptyMap()), eq(visit));
    }

    @Test
    public void testStartMetricWithDirectTags() {
        Map<String, String> directTags = new HashMap<>(0);

        handler.startMetric(measure);
        handler.stopMetric(measure, directTags, visit);

        verify(handler).trackTime(eq(measure), anyLong(), eq(directTags), eq(visit));
    }

    @Test
    public void testStartMetricWithoutTime() {
        handler.stopMetric(measure, visit);

        verify(handler, never()).trackTime(any(), anyLong(), any(), any());
    }

    @Test
    public void testTrackCounter() {
        handler.trackCounter(measure, visit);
        verify(handler).incrementCounter(eq(metric));
        verify(measure).getMetric(eq(visit), eq(Collections.emptyMap()));
    }

    @Test
    public void testTrackCounterWithoutVisit() {

        when(measure.getMetric(any())).thenReturn(metric);

        handler.trackCounter(measure);
        verify(handler).incrementCounter(eq(metric));
    }

    @Test
    public void testTrackCounterWithDirectTags() {
        Map<String, String> directTags = new HashMap<>(0);

        handler.trackCounter(measure, directTags, visit);
        verify(handler).incrementCounter(eq(metric));
        verify(measure).getMetric(eq(visit), eq(directTags));
    }

}
