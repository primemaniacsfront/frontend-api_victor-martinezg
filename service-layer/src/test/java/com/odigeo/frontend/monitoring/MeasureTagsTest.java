package com.odigeo.frontend.monitoring;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Browser;
import com.odigeo.frontend.commons.context.visit.types.OS;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class MeasureTagsTest {

    private static final MeasureTags TAG = MeasureTags.BRAND;

    @Mock
    private VisitInformation visit;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetLabel() {
        assertEquals(TAG.getLabel(), StringUtils.lowerCase(TAG.name()));
    }

    @Test
    public void testGetBrandValue() {
        Brand brand = Brand.ED;

        when(visit.getBrand()).thenReturn(brand);
        assertEquals(MeasureTags.BRAND.getValue(visit), brand.name());
    }

    @Test
    public void testGetBrowserValue() {
        Browser browser = Browser.CHROME;

        when(visit.getBrowser()).thenReturn(browser);
        assertEquals(MeasureTags.BROWSER.getValue(visit), browser.name());
    }

    @Test
    public void testGetDeviceValue() {
        WebInterface websiteInterface = WebInterface.ONE_FRONT_DESKTOP;

        when(visit.getWebInterface()).thenReturn(websiteInterface);
        assertEquals(MeasureTags.DEVICE.getValue(visit), websiteInterface.name());
    }

    @Test
    public void testGetOsValue() {
        OS os = OS.WINDOWS;

        when(visit.getOs()).thenReturn(os);
        assertEquals(MeasureTags.OS.getValue(visit), os.name());
    }

    @Test
    public void testGetWebsiteValue() {
        Site website = Site.ES;

        when(visit.getSite()).thenReturn(website);
        assertEquals(MeasureTags.WEBSITE.getValue(visit), website.name());
    }

}
