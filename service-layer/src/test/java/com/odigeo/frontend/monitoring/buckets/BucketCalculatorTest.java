package com.odigeo.frontend.monitoring.buckets;

import com.odigeo.commons.monitoring.metrics.bucket.Bucket;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class BucketCalculatorTest {

    private BucketCalculator calculator;

    @Test
    public void testCalculateValid() {
        calculator = new BucketCalculator(ValidBucket.values());

        Stream<Pair<Long, String>> pairs = Stream.of(
            new ImmutablePair<>(0L, "0to1999"),
            new ImmutablePair<>(1500L, "0to1999"),
            new ImmutablePair<>(1999L, "0to1999"),
            new ImmutablePair<>(2000L, "2000to3999"),
            new ImmutablePair<>(2300L, "2000to3999"),
            new ImmutablePair<>(3999L, "2000to3999"),
            new ImmutablePair<>(4000L, "4000orMore"));

        pairs.forEach(pair -> assertEquals(calculator.calculate(pair.getLeft()), pair.getRight()));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testCalculateInvalidMax() {
        calculator = new BucketCalculator(InvalidBucket.values());
    }

    @Test
    public void testGetBuckets() {
        Bucket[] buckets = ValidBucket.values();
        calculator = new BucketCalculator(buckets);

        assertSame(calculator.getBuckets(), buckets);
    }

    private enum ValidBucket implements Bucket {
        FROM_0_TO_1999("0to1999", "1999"),
        FROM_2000_TO_3999("2000to3999", "3999"),
        FROM_4000_TO_INF("4000orMore", "+Inf");

        private final String name;
        private final String upperBound;

        ValidBucket(String name, String upperBound) {
            this.name = name;
            this.upperBound = upperBound;
        }

        public String getName() {
            return this.name;
        }

        public String getUpperBound() {
            return this.upperBound;
        }
    }

    private enum InvalidBucket implements Bucket {
        FROM_0_TO_99("0to108", "108"),
        FROM_99_TO_100("109to110", "110"),
        FROM_100_TO_INF("110orMore", "+Inf");

        private final String name;
        private final String upperBound;

        InvalidBucket(String name, String upperBound) {
            this.name = name;
            this.upperBound = upperBound;
        }

        public String getName() {
            return this.name;
        }

        public String getUpperBound() {
            return this.upperBound;
        }
    }

}
