package com.odigeo.frontend.monitoring.buckets;

import com.odigeo.commons.monitoring.metrics.bucket.Bucket;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FixedBucketsTest {

    @Test
    public void testFixedBucketsExample() {
        testFixedBuckets(FixedBuckets.FIXED_BUCKETS_EXAMPLE, FixedBucketsExample.values());
    }

    private void testFixedBuckets(FixedBuckets fixedBucket, Bucket[] buckets) {
        BucketCalculator calculator = fixedBucket.getBucketCalculator();
        assertEquals(calculator.getBuckets(), buckets);
    }

}
