package com.odigeo.frontend.monitoring;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class MeasureConstantsTest {

    private static final Measure MEASURE_FIELD_VALUE = new Measure.Builder(StringUtils.EMPTY).build();

    private static class MeasureConstantsWrapper extends MeasureConstants {
        public static final Measure MEASURE_FIELD = MEASURE_FIELD_VALUE;
        public static final String NOT_MEASURE_FIELD = StringUtils.EMPTY;

        MeasureConstantsWrapper() throws IllegalAccessException {
            super();
        }
    }

    @Test
    public void testGetMeasure() throws Exception {
        MeasureConstantsWrapper constants = new MeasureConstantsWrapper();

        assertSame(constants.getMeasure("MEASURE_FIELD"), MEASURE_FIELD_VALUE);
        assertNull(constants.getMeasure("NOT_MEASURE_FIELD"));
    }

}
