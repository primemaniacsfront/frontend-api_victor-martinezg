package com.odigeo.frontend.services.accommodation;

import com.odigeo.accommodation.offer.model.AccommodationDealKey;
import com.odigeo.accommodation.offer.model.Availability;
import com.odigeo.accommodation.offer.model.AvailabilityId;
import com.odigeo.accommodation.offer.model.RoomGroupDealKey;
import com.odigeo.accommodation.offer.model.SearchId;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AccommodationOfferServiceTest {

    @Mock
    private com.odigeo.accommodation.offer.AccommodationOfferService accommodationOfferService;

    @InjectMocks
    private AccommodationOfferService testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    public void testCheckAvailability() {
        Availability response = new Availability();
        response.setAvailabilityId(AvailabilityId.builder().id("456").build());
        when(accommodationOfferService.checkAvailability(any(SearchId.class), any(AccommodationDealKey.class), any(RoomGroupDealKey.class))).thenReturn(response);
        String availability = testClass.checkAvailability("123", "0", "0");
        assertNotNull(availability);
        assertEquals(availability, response.getAvailabilityId().getId());
    }

}
