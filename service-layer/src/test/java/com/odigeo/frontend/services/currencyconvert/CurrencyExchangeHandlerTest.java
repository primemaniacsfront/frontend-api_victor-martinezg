package com.odigeo.frontend.services.currencyconvert;

import com.odigeo.curex.client.currencyconverter.CurrencyConverterFactory;
import com.odigeo.curex.client.currencyconverter.DefaultCurrencyConverter;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CurrencyExchangeHandlerTest {

    private static final BigDecimal CONV_RATE = new BigDecimal("1.5");

    @Mock
    CurrencyConverterFactory currencyConverterFactory;
    @Mock
    DefaultCurrencyConverter defaultCurrencyConverter;

    @InjectMocks
    private CurrencyExchangeHandler currencyExchangeHandler;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        when(currencyConverterFactory.getCurrentCurrencyConverter()).thenReturn(defaultCurrencyConverter);
    }

    @Test
    public void getActiveCurrencyConvertRate() throws Exception {
        when(defaultCurrencyConverter.getConversionRate(anyString(), anyString())).thenReturn(CONV_RATE);
        BigDecimal conversionRate = currencyExchangeHandler.getCurrencyConvertRate("GBP", "EUR");
        assertEquals(conversionRate, CONV_RATE);
    }

}
