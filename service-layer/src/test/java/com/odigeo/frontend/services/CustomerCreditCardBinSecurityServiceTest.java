package com.odigeo.frontend.services;

import com.edreamsodigeo.payments.customercreditcardbinconfig.contract.CreditCardBin;
import com.edreamsodigeo.payments.customercreditcardbinconfig.contract.CustomerCreditCardBinConfigResource;
import com.edreamsodigeo.payments.customercreditcardbinconfig.contract.CustomerCreditCardBinConfigResourceException;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class CustomerCreditCardBinSecurityServiceTest {

    private static final CreditCardBin CREDIT_CARD_BIN = CreditCardBin.valueOf("4596639999");

    @Mock
    private CustomerCreditCardBinConfigResource customerCreditCardBinConfigResource;

    @Mock
    private PrimeService primeService;

    @Mock
    private Logger logger;

    @InjectMocks
    private CustomerCreditCardBinSecurityService customerCreditCardBinSecurityService;

    private final VisitInformation visitInformation = new VisitInformation();

    @BeforeMethod
    public void init() throws CustomerCreditCardBinConfigResourceException {
        MockitoAnnotations.openMocks(this);
        when(customerCreditCardBinConfigResource.getLength()).thenReturn(7);
    }

    @Test
    public void testIsSecureBinNumberForPrime() throws CustomerCreditCardBinConfigResourceException {
        when(primeService.isPrimeSite(visitInformation)).thenReturn(true);
        when(customerCreditCardBinConfigResource.isBlocked(any())).thenReturn(Boolean.FALSE);
        assertFalse(customerCreditCardBinSecurityService.isCreditCardBlockedForSubscription(CREDIT_CARD_BIN.getBin(), visitInformation));
    }

    @Test
    public void testIsSecureBinNumberBecauseWebsiteIsNotPrime() throws CustomerCreditCardBinConfigResourceException {
        when(primeService.isPrimeSite(visitInformation)).thenReturn(false);
        when(customerCreditCardBinConfigResource.isBlocked(any())).thenReturn(Boolean.FALSE);
        assertFalse(customerCreditCardBinSecurityService.isCreditCardBlockedForSubscription(CREDIT_CARD_BIN.getBin(), visitInformation));
    }

    @Test
    public void testIsNotSecureBinNumberForPrime() throws CustomerCreditCardBinConfigResourceException {
        when(primeService.isPrimeSite(visitInformation)).thenReturn(true);
        when(customerCreditCardBinConfigResource.isBlocked(any())).thenReturn(Boolean.TRUE);
        assertTrue(customerCreditCardBinSecurityService.isCreditCardBlockedForSubscription(CREDIT_CARD_BIN.getBin(), visitInformation));
    }

    @Test
    public void testIsSecureBinNumberForPrimeWhenCreditCardBinThrowsException() throws CustomerCreditCardBinConfigResourceException {
        when(primeService.isPrimeSite(visitInformation)).thenReturn(true);
        when(customerCreditCardBinConfigResource.isBlocked(any())).thenThrow(CustomerCreditCardBinConfigResourceException.class);
        assertFalse(customerCreditCardBinSecurityService.isCreditCardBlockedForSubscription(CREDIT_CARD_BIN.getBin(), visitInformation));
        verify(logger, times(1)).error(any(), any(), any());
    }
}
