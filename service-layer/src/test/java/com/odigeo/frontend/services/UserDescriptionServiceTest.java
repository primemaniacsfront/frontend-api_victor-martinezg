package com.odigeo.frontend.services;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.userprofiles.api.v2.SSOApiService;
import com.odigeo.userprofiles.api.v2.UserApiService;
import com.odigeo.userprofiles.api.v2.model.SSOToken;
import com.odigeo.userprofiles.api.v2.model.TrafficInterface;
import com.odigeo.userprofiles.api.v2.model.User;
import com.odigeo.userprofiles.api.v2.model.requests.PasswordChangeRequest;
import com.odigeo.userprofiles.api.v2.model.requests.credentials.SSOTokenUserCredentials;
import com.odigeo.userprofiles.api.v2.model.requests.credentials.UserCredentials;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.InvalidValidationException;
import com.odigeo.userprofiles.api.v2.model.responses.exceptions.RemoteException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class UserDescriptionServiceTest {
    private static final String SSO_TOKEN_VALUE = "TOKEN";

    @Mock
    private UserApiService userService;
    @Mock
    private SSOApiService ssoApiService;
    @Mock
    private User user;
    @Mock
    private SSOToken ssoToken;
    @Mock
    private UserCredentials credentials;
    @Mock
    private PasswordChangeRequest passwordChangeRequest;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private RequestInfo requestInfo;

    @InjectMocks
    private UserDescriptionService service;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(context.getVisitInformation()).thenReturn(visit);
        when(context.getRequestInfo()).thenReturn(requestInfo);
    }

    @Test
    public void testGetUser() throws Exception {
        when(userService.getUser(credentials)).thenReturn(user);
        assertEquals(service.getUser(credentials), user);
    }

    @Test
    public void testGetUserInvalid() throws Exception {
        when(userService.getUser(any())).thenThrow(InvalidValidationException.class);
        assertNull(service.getUser(credentials));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getGetUserExceptions() throws Exception {
        when(userService.getUser(any())).thenThrow(new RemoteException("RemoteException"));
        service.getUser(credentials);
    }

    @Test
    public void testGetUserByToken() throws Exception {
        Site site = Site.ES;
        String locale = "locale";
        String token = "token";

        when(visit.getSite()).thenReturn(site);
        when(visit.getBrand()).thenReturn(Brand.ED);
        when(visit.getWebInterface()).thenReturn(WebInterface.ONE_FRONT_DESKTOP);
        when(visit.getLocale()).thenReturn(locale);

        when(requestInfo.getCookieValue(SiteCookies.SSO_TOKEN)).thenReturn(token);

        when(userService.getUser(argThat(argument -> {
            SSOTokenUserCredentials credentials = (SSOTokenUserCredentials) argument;
            return credentials.getToken().equals(token)
                    && credentials.getLocale().equals(locale)
                    && credentials.getWebsite().equals(site.name())
                    && credentials.getBrand() == com.odigeo.userprofiles.api.v2.model.Brand.ED
                    && credentials.getTrafficInterface() == TrafficInterface.ONE_FRONT_DESKTOP;
        }))).thenReturn(user);

        assertEquals(service.getUserByToken(context), user);
    }

    @Test
    public void testGetUserByTokenWithoutToken() {
        assertNull(service.getUserByToken(context));
    }

    @Test
    public void testGetTrafficInterface() {
        Stream.of(
                new ImmutablePair<>(WebInterface.ONE_FRONT_DESKTOP, TrafficInterface.ONE_FRONT_DESKTOP),
                new ImmutablePair<>(WebInterface.ONE_FRONT_SMARTPHONE, TrafficInterface.ONE_FRONT_SMARTPHONE),
                new ImmutablePair<>(WebInterface.OTHER, TrafficInterface.OTHERS)
        ).forEach(pair -> {
            when(visit.getWebInterface()).thenReturn(pair.left);
            assertSame(service.getTrafficInterface(visit), pair.right);
        });
    }

    @Test
    public void testSetUserPassword() throws Exception {
        when(userService.setUserPassword(passwordChangeRequest)).thenReturn(user);
        assertEquals(service.setUserPassword(passwordChangeRequest), user);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testSetUserPasswordExceptions() throws Exception {
        when(userService.setUserPassword(any())).thenThrow(new RemoteException("RemoteException"));
        service.setUserPassword(passwordChangeRequest);
    }

    @Test
    public void testGenerateToken() throws Exception {
        when(ssoApiService.generateToken(credentials)).thenReturn(ssoToken);
        assertEquals(service.generateToken(credentials), ssoToken);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGenerateTokenExceptions() throws Exception {
        when(ssoApiService.generateToken(any())).thenThrow(new RemoteException("RemoteException"));
        service.generateToken(credentials);
    }
    @Test
    public void testIsUserLoggedWhenEmpty() {
        assertFalse(service.isUserLogged(context));
    }

    @Test
    public void testIsUserLoggedWhenCookiePresent() {
        when(context.getRequestInfo()).thenReturn(requestInfo);
        when(requestInfo.getCookieValue(SiteCookies.SSO_TOKEN)).thenReturn(SSO_TOKEN_VALUE);
        assertTrue(service.isUserLogged(context));
    }
}
