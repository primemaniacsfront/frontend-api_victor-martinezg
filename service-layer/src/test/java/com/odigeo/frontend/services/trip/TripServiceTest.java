package com.odigeo.frontend.services.trip;

import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.itineraryapi.v1.InvalidParametersException;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import com.odigeo.travelcompanion.v2.model.user.Brand;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;


public class TripServiceTest {

    private static final Long BOOKING_ID = 1233321L;
    private static final String EMAIL = "EMAIL";
    private static final Locale LOCALE = Locale.ENGLISH;
    private static final Brand BRAND = Brand.ED;

    @Mock
    private BookingDetail bookingDetail;
    @Mock
    private com.odigeo.travelcompanion.v2.TripService tripService;
    @InjectMocks
    private TripService service;

    @BeforeMethod
    public void setUp() {
        service = new TripService();
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetTrip() {
        when(tripService.getTrip(any(), any(), any(), any(), any())).thenReturn(bookingDetail);
        BookingDetail a = service.getTrip(BOOKING_ID, LOCALE, EMAIL, BRAND);
        assertEquals(a, bookingDetail);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetTripError() {
        when(tripService.getTrip(any(), any(), any(), any(), any())).thenThrow(new InvalidParametersException(""));
        service.getTrip(BOOKING_ID, LOCALE, EMAIL, BRAND);
    }
}
