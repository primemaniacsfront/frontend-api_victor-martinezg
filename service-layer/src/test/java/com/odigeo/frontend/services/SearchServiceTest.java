package com.odigeo.frontend.services;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.searchengine.v2.SearchEngineServiceRest;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetailResponse;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;
import com.odigeo.searchengine.v2.responses.error.MessageResponse;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static com.odigeo.frontend.services.SearchService.WNG_025;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class SearchServiceTest {

    private static final String INTERNAL_ERROR_PREFIX = "INT";
    private static final String VALIDATION_ERROR_PREFIX = "ERR";

    @Mock
    private SearchEngineServiceRest searchEngineService;
    @Mock
    private SearchResponse searchResponse;
    @Mock
    private SearchMethodRequest searchRequest;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private Logger logger;

    @InjectMocks
    private SearchService searchService;

    @BeforeMethod
    public void init() {

        MockitoAnnotations.openMocks(this);
        when(searchEngineService.search(searchRequest)).thenReturn(searchResponse);
    }

    @Test
    public void testExecuteSearch() {
        when(searchResponse.getSearchId()).thenReturn(1L);
        assertSame(searchService.executeSearch(searchRequest), searchResponse);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testExecuteSearchWithValidationError() {
        executeSearchWithMessage(VALIDATION_ERROR_PREFIX);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testExecuteSearchWithInternalError() {
        executeSearchWithMessage(INTERNAL_ERROR_PREFIX);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testExecuteSearchWithException() {
        when(searchEngineService.search(any())).thenThrow(RuntimeException.class);
        searchService.executeSearch(searchRequest);
    }

    @Test
    public void testWrittingLogger() {
        executeSearchWithMessage(WNG_025);
        verify(logger, times(1)).warning(any(), any(String.class));
    }

    private void executeSearchWithMessage(String errorCode) {
        MessageResponse messageResponse = mock(MessageResponse.class);

        when(searchResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));
        when(messageResponse.getCode()).thenReturn(errorCode);
        assertSame(searchService.executeSearch(searchRequest), searchResponse);
    }

    @Test
    public void testGetSearchResult() {
        SearchResults searchResults = mock(SearchResults.class);
        long searchId = 0L;
        Integer fareItineraryIndex = 0;

        when(searchEngineService.getSearchResult(searchId, fareItineraryIndex, null)).thenReturn(searchResults);
        assertSame(searchService.getSearchResult(searchId, fareItineraryIndex), searchResults);
    }

    @Test
    public void testGetAccommodationDetails() {
        AccommodationDetailResponse response = mock(AccommodationDetailResponse.class);
        long searchId = 0L;
        long dealKey = 1L;
        String visit = "visit";

        when(searchEngineService.getAccommodationDetails(searchId, dealKey, visit)).thenReturn(response);
        assertSame(searchService.getAccommodationDetails(searchId, dealKey, visit), response);
    }

    @Test
    public void testSearchRoomAvailability() {
        SearchRoomResponse response = mock(SearchRoomResponse.class);
        long searchId = 0L;
        long dealKey = 1L;
        String visit = "visit";

        when(searchEngineService.searchRoomAvailability(searchId, dealKey, visit)).thenReturn(response);
        assertSame(searchService.getRoomsAvailability(searchId, dealKey, visit), response);
    }

    @Test
    public void testGetSearchResults() {
        long searchId = 0L;
        String visit = "visit";
        searchService.getSearchResults(searchId, visit);
        verify(searchEngineService).getSearchResults(eq(searchId), eq(visit));
    }

}
