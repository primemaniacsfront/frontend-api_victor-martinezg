package com.odigeo.frontend.services.shoppingbasket;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResponseInfo;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.shoppingbasket.v2.requests.CreateRequest;
import com.odigeo.shoppingbasket.v2.requests.ProductRequest;
import com.odigeo.shoppingbasket.v2.responses.ShoppingBasketResponse;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ShoppingBasketV2ServiceTest {

    private static final String ERROR_MSG = "ERROR";
    private static final long SHOPPING_BASKET_ID = 1L;

    @Mock
    private com.odigeo.shoppingbasket.v2.ShoppingBasketService shoppingBasket;
    @Mock
    private ShoppingBasketResponse shoppingBasketResponse;
    @Mock
    private ResolverContext context;
    @Mock
    private ShoppingBasketServiceFactory shoppingBasketServiceFactory;
    @Mock
    private RequestInfo requestInfo;
    @Mock
    private ResponseInfo responseInfo;

    @InjectMocks
    private ShoppingBasketV2Service shoppingBasketV2Service;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(shoppingBasket.create(any())).thenReturn(shoppingBasketResponse);
        when(shoppingBasket.getShoppingBasket(anyLong())).thenReturn(shoppingBasketResponse);
        when(shoppingBasket.addProduct(anyLong(), any())).thenReturn(shoppingBasketResponse);
        when(shoppingBasket.removeProduct(anyLong(), any())).thenReturn(shoppingBasketResponse);
        when(context.getResponseInfo()).thenReturn(responseInfo);
        BasicCookieStore basicCookieStore = new BasicCookieStore();
        basicCookieStore.addCookie(new BasicClientCookie("JSESSIONID", "value"));
        when(shoppingBasketServiceFactory.getShoppingBasketService(any()))
            .thenReturn(new ImmutablePair<>(shoppingBasket, basicCookieStore));
    }

    @Test
    public void create() {
        assertSame(shoppingBasketV2Service.create(new CreateRequest(), context), shoppingBasketResponse);
        verify(shoppingBasket).create(any());
        verify(responseInfo).addCookie(any(SiteCookies.class), anyString());
        verify(responseInfo).putHeader(any(SiteHeaders.class), anyString());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void createValidationError() {
        when(shoppingBasketResponse.getErrorType()).thenReturn(ERROR_MSG);

        shoppingBasketV2Service.create(new CreateRequest(), context);

        verify(shoppingBasket).create(any());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void createRuntimeExceptionByType() {
        when(shoppingBasket.create(any())).thenThrow(new RuntimeException(ERROR_MSG));

        shoppingBasketV2Service.create(new CreateRequest(), context);

        verify(shoppingBasket).create(any());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void createRuntimeExceptionNull() {
        when(shoppingBasket.create(any())).thenReturn(null);

        shoppingBasketV2Service.create(new CreateRequest(), context);

        verify(shoppingBasket).create(any());
    }

    @Test
    public void getShoppingBasket() {
        assertSame(shoppingBasketV2Service.getShoppingBasket(SHOPPING_BASKET_ID, context), shoppingBasketResponse);
        verify(shoppingBasket).getShoppingBasket(anyLong());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getShoppingBasketValidationErrorByType() {
        when(shoppingBasketResponse.getErrorType()).thenReturn(ERROR_MSG);

        shoppingBasketV2Service.getShoppingBasket(SHOPPING_BASKET_ID, context);

        verify(shoppingBasket).getShoppingBasket(anyLong());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getShoppingBasketValidationErrorEmpty() {
        when(shoppingBasket.getShoppingBasket(anyLong())).thenReturn(null);

        shoppingBasketV2Service.getShoppingBasket(SHOPPING_BASKET_ID, context);

        verify(shoppingBasket).getShoppingBasket(anyLong());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getShoppingBasketRuntimeException() {
        when(shoppingBasket.getShoppingBasket(anyLong())).thenThrow(new RuntimeException(ERROR_MSG));

        shoppingBasketV2Service.getShoppingBasket(SHOPPING_BASKET_ID, context);

        verify(shoppingBasket).getShoppingBasket(anyLong());
    }

    @Test
    public void addProduct() {
        shoppingBasketV2Service.addProduct(SHOPPING_BASKET_ID, new ProductRequest(), context);

        verify(shoppingBasket).addProduct(anyLong(), any());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void addProductValidationErrorBytType() {
        when(shoppingBasketResponse.getErrorType()).thenReturn(ERROR_MSG);

        shoppingBasketV2Service.addProduct(SHOPPING_BASKET_ID, new ProductRequest(), context);

        verify(shoppingBasket).addProduct(anyLong(), any());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void addProductValidationErrorEmpty() {
        when(shoppingBasket.addProduct(anyLong(), any())).thenReturn(null);

        shoppingBasketV2Service.addProduct(SHOPPING_BASKET_ID, new ProductRequest(), context);

        verify(shoppingBasket).addProduct(anyLong(), any());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void addProductRuntimeException() {
        when(shoppingBasket.addProduct(anyLong(), any())).thenThrow(new RuntimeException(ERROR_MSG));

        shoppingBasketV2Service.addProduct(SHOPPING_BASKET_ID, new ProductRequest(), context);

        verify(shoppingBasket).addProduct(anyLong(), any());
    }

    @Test
    public void removeProduct() {
        shoppingBasketV2Service.removeProduct(SHOPPING_BASKET_ID, new ProductRequest(), context);

        verify(shoppingBasket).removeProduct(anyLong(), any());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void removeProductValidationErrorByType() {
        when(shoppingBasketResponse.getErrorType()).thenReturn(ERROR_MSG);

        shoppingBasketV2Service.removeProduct(SHOPPING_BASKET_ID, new ProductRequest(), context);

        verify(shoppingBasket).removeProduct(anyLong(), any());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void removeProductValidationErrorEmpty() {
        when(shoppingBasket.removeProduct(anyLong(), any())).thenReturn(null);

        shoppingBasketV2Service.removeProduct(SHOPPING_BASKET_ID, new ProductRequest(), context);

        verify(shoppingBasket).removeProduct(anyLong(), any());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void removeProductRuntimeException() {
        when(shoppingBasket.removeProduct(anyLong(), any())).thenThrow(new RuntimeException(ERROR_MSG));

        shoppingBasketV2Service.removeProduct(SHOPPING_BASKET_ID, new ProductRequest(), context);

        verify(shoppingBasket).removeProduct(anyLong(), any());
    }

}
