package com.odigeo.frontend.services.configurations.dapi;

import com.google.inject.Guice;
import com.odigeo.dapi.client.DistributionApiConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class DapiConnectionFactoryTest {

    @Mock
    private DapiConnectionConfig config;

    private DapiConnectionFactory controller;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        controller = new DapiConnectionFactory();

        Guice.createInjector(binder -> {
            binder.bind(DapiConnectionConfig.class).toProvider(() -> config);
        }).injectMembers(controller);
    }

    @Test
    public void testGetDapiConnectionFactory() {
        when(config.getDapiHost()).thenReturn(StringUtils.EMPTY);
        when(config.isUseSecureForBooking()).thenReturn(Boolean.FALSE);
        DistributionApiConnectionFactory factory = controller.getDapiConnectionFactory();
        assertNotNull(factory);
    }
}
