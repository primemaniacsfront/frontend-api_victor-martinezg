package com.odigeo.frontend.services.configurations.dapi;

import com.google.inject.Guice;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class DapiCallbackTest {

    private Object[] arguments;
    private Method method;

    @Mock
    private DapiCallback dapiCallback;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        Guice.createInjector(binder -> {
            binder.bind(DapiCallback.class).toProvider(() -> dapiCallback);
        });
    }

    @Test
    public void testBeforeCall() throws Exception {
        getArguments();
        dapiCallback.beforeCall(method, arguments);
        verify(dapiCallback, times(1)).beforeCall(method, arguments);
    }

    @Test
    public void testOnError() throws Exception {
        getArguments();
        Exception e = new Exception();
        dapiCallback.onError(method, arguments, e);
        verify(dapiCallback, times(1)).onError(method, arguments, e);
    }

    @Test
    public void testAfterCall() throws Exception {
        getArguments();
        dapiCallback.afterCall(method, arguments);
        verify(dapiCallback, times(1)).afterCall(method, arguments);
    }

    private void getArguments() throws NoSuchMethodException {
        Object  object = new Object();
        String methodName = "toString";
        method = object.getClass().getMethod(methodName);
        arguments = new String[]{"aaa", "bbb"};
    }
}
