package com.odigeo.frontend.services.onefront;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SearchSessionLoaderServiceCacheKeyTest extends BeanTest<SearchSessionLoaderServiceCacheKey> {

    private static final String COOKIE_CACHE_KEY = "key";

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(SearchSessionLoaderServiceCacheKey.class)
                .suppress(Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    @Test
    public void testHashCode() {
        assertEquals(getBean().hashCode(), getBean().hashCode());
    }

    @Override
    protected SearchSessionLoaderServiceCacheKey getBean() {
        return new SearchSessionLoaderServiceCacheKey(COOKIE_CACHE_KEY);
    }

}
