package com.odigeo.frontend.services.onefront;

import com.odigeo.commons.rest.configuration.URLConfiguration;
import com.odigeo.frontend.commons.context.RequestInfo;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class OFURLConfigurationBuilderTest {

    private static final String MY_LOCAL_ENVIRONMENT_HOST = "mylocalenvironment.com";
    private static final String MY_LOCAL_ENVIRONMENT_PROTOCOL = "http";
    private static final int MY_LOCAL_ENVIRONMENT_PORT = 8080;

    private static final String OF2_LOCAL_ENVIRONMENT_HOST = "c02gj05rmd6t.local";
    private static final String OF2_LOCAL_ENVIRONMENT_PROTOCOL = "https";
    private static final int OF2_LOCAL_ENVIRONMENT_PORT = -1;

    @Mock
    private RequestInfo requestInfo;

    private OFURLConfigurationBuilder builder;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        builder = new OFURLConfigurationBuilder();
    }

    @Test
    public void testBuild() {
        String host = "frontend-html5";

        when(requestInfo.getRequestUrl()).thenReturn("http://" + host + ":8080/travel");

        URLConfiguration configuration = builder.build(com.odigeo.frontend.contract.onefront.SearchSessionLoaderService.class, requestInfo);
        assertEquals(configuration.getProtocol(), "http");
        assertEquals(configuration.getHost(), host);
        assertEquals(configuration.getService(), "/travel");
        assertEquals(configuration.getPort(), 8080);
    }

    @Test
    public void testBuildMyLocalEnvironment() {
        when(requestInfo.getRequestUrl()).thenReturn("http://" + MY_LOCAL_ENVIRONMENT_HOST + ":8080/travel");

        URLConfiguration configuration = builder.build(com.odigeo.frontend.contract.onefront.SearchSessionLoaderService.class, requestInfo);
        assertEquals(configuration.getProtocol(), MY_LOCAL_ENVIRONMENT_PROTOCOL);
        assertEquals(configuration.getHost(), MY_LOCAL_ENVIRONMENT_HOST);
        assertEquals(configuration.getService(), "/travel");
        assertEquals(configuration.getPort(), MY_LOCAL_ENVIRONMENT_PORT);

    }

    @Test
    public void testBuildOF2LocalEnvironment() {
        when(requestInfo.getRequestUrl()).thenReturn("http://" + OF2_LOCAL_ENVIRONMENT_HOST + ":8080/travel");

        URLConfiguration configuration = builder.build(com.odigeo.frontend.contract.onefront.SearchSessionLoaderService.class, requestInfo);
        assertEquals(configuration.getProtocol(), OF2_LOCAL_ENVIRONMENT_PROTOCOL);
        assertEquals(configuration.getHost(), OF2_LOCAL_ENVIRONMENT_HOST);
        assertEquals(configuration.getService(), "/travel");
        assertEquals(configuration.getPort(), OF2_LOCAL_ENVIRONMENT_PORT);
    }



    @Test
    public void testBuildThrowsException() {
        String undefinedUrl = "|";

        when(requestInfo.getRequestUrl()).thenReturn(undefinedUrl);
        assertNull(builder.build(com.odigeo.frontend.contract.onefront.SearchSessionLoaderService.class, requestInfo));
    }
}
