package com.odigeo.frontend.services.checkout;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Buyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCart;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.StatusInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteractionId;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.QueryParamUtils;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutFactory;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutHandler;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeRequest;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeToken;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.CHECKOUT_STATUS;
import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.CTX_PATH;
import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.PAYMENT_INTERACTION_ID;
import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.SHOPPING_ID;
import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.TRIPS_REDIRECT_TOKEN;
import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.URL_REDIRECT;
import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.URL_REDIRECT_PROTOCOL;
import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.URL_REDIRECT_PSD2_KO;
import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.URL_REDIRECT_PSD2_LOADING;
import static com.odigeo.frontend.services.checkout.UserInteractionNeededService.URL_REDIRECT_PSD2_OK;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class UserInteractionNeededServiceTest {

    private static final long BOOKING_ID = 1L;
    private static final String ACCEPTED = "accepted";
    private static final String GBP = "GBP";
    private static final String CURRENCY = "currency";
    private static final String ORDER_ID = "orderId";
    private static final String STATUS = "status";
    private static final String SERVER_NAME = "localhost";
    private static final String EMAIL = "a@test.com";
    private static final int STEP_1 = 1;
    private static final int STEP_2 = 2;
    private static final String REDIRECT_URL = "redirectUrl";
    private static final String USER_PAY_INT_ID = "1";
    private static final String MSG_ENCRYPTED = "53022cf12c5959ddf3e733128930dd3d52e3ea";

    @Mock
    private CheckoutFactory checkoutFactory;
    @Mock
    private ResolverContext context;
    @Mock
    private CheckoutHandler handler;
    @Mock
    private QueryParamUtils queryParamUtils;
    @Mock
    private CheckoutResponse response;
    @Mock
    private CheckoutResumeToken token;
    @Mock
    private StatusInfo status;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private Buyer buyer;
    @Mock
    private UserPaymentInteractionId userPaymentInteractionId;

    @InjectMocks
    UserInteractionNeededService service;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(checkoutFactory.getInstance(any())).thenReturn(handler);
        when(userPaymentInteractionId.getId()).thenReturn(USER_PAY_INT_ID);
    }

    @Test
    public void testResumeGetWithCorrectParameters() {
        ArgumentCaptor<CheckoutResumeRequest> requestArgumentCaptor = ArgumentCaptor.forClass(CheckoutResumeRequest.class);
        mockCheckoutResumeToken(PaymentMethod.CREDITCARD.toString(), ShoppingType.CART, STEP_1);
        when(queryParamUtils.getQueryParamsString(any())).thenReturn(REDIRECT_URL);
        mockCheckoutResponse(CheckoutStatus.SUCCESS);
        when(handler.resume(requestArgumentCaptor.capture(), any())).thenReturn(response);
        when(token.getUserPaymentInteractionId()).thenReturn(USER_PAY_INT_ID);
        when(queryParamUtils.getQueryParamsString(anyMap(), anyString(), anyString())).thenReturn(REDIRECT_URL);
        String redirectUrl = service.resume(getStringMap(), context, token, SERVER_NAME);
        assertEquals(redirectUrl, URL_REDIRECT_PROTOCOL + SERVER_NAME + CTX_PATH + REDIRECT_URL);
        CheckoutResumeRequest checkoutRequest = requestArgumentCaptor.getValue();
        assertParameters(checkoutRequest);
    }

    @Test
    public void testResume() {
        when(handler.resume(any(), any())).thenReturn(response);
        when(queryParamUtils.generateMyTripsRedirectionToken(anyLong(), anyString())).thenReturn(MSG_ENCRYPTED);
        doCallRealMethod().when(queryParamUtils).getQueryParamsString(anyMap(), anyString(), anyString());

        testResumeByStatus(CheckoutStatus.SUCCESS, PaymentMethod.CREDITCARD, STEP_2, URL_REDIRECT_PSD2_OK);
        testResumeByStatus(CheckoutStatus.SUCCESS, PaymentMethod.PAYPAL, STEP_2, URL_REDIRECT);
        testResumeByStatus(CheckoutStatus.STOP, PaymentMethod.CREDITCARD, STEP_2, URL_REDIRECT_PSD2_KO);
        testResumeByStatus(CheckoutStatus.FAILED, PaymentMethod.CREDITCARD, STEP_2, URL_REDIRECT_PSD2_KO);
        testResumeByStatus(CheckoutStatus.FAILED, PaymentMethod.CREDITCARD, STEP_1, URL_REDIRECT_PSD2_KO);
        testResumeByStatus(CheckoutStatus.USER_PAYMENT_INTERACTION, PaymentMethod.CREDITCARD, STEP_2, URL_REDIRECT_PSD2_LOADING);
        testResumeByStatus(CheckoutStatus.USER_PAYMENT_INTERACTION, PaymentMethod.CREDITCARD, STEP_1, URL_REDIRECT_PSD2_KO);
        testResumeByStatus(CheckoutStatus.SUCCESS, PaymentMethod.KLARNA, STEP_2, URL_REDIRECT);
    }

    private void testResumeByStatus(CheckoutStatus status, PaymentMethod method, int step, String expectedRedirectPage) {
        mockCheckoutResponse(status);
        mockCheckoutResumeToken(method.toString(), ShoppingType.CART, step);
        String expectedUrl = URL_REDIRECT_PROTOCOL + SERVER_NAME + CTX_PATH + expectedRedirectPage + redirectUrlParams(step, status, expectedRedirectPage);
        String redirectUrl = service.resume(getStringMap(), context, token, SERVER_NAME);
        assertEquals(redirectUrl, expectedUrl);
    }

    private String redirectUrlParams(int step, CheckoutStatus status, String expectedRedirectPage) {
        String shoppingIdParam = "?" + SHOPPING_ID + "=" + BOOKING_ID;
        if (expectedRedirectPage.contains(URL_REDIRECT_PSD2_KO)) {
            return shoppingIdParam;
        } else {
            return step == STEP_1 ? shoppingIdParam + "&" + PAYMENT_INTERACTION_ID + "=" + USER_PAY_INT_ID
                    : shoppingIdParam + "&" + TRIPS_REDIRECT_TOKEN + "=" + MSG_ENCRYPTED + "&" + CHECKOUT_STATUS + "=" + status.name();
        }
    }

    private Map<String, String> getStringMap() {
        Map<String, String> params = new HashMap<>();

        params.put(STATUS, ACCEPTED);
        params.put(ORDER_ID, String.valueOf(BOOKING_ID));
        params.put(CURRENCY, GBP);
        return params;
    }

    private void mockCheckoutResponse(CheckoutStatus checkoutStatus) {
        when(status.getStatus()).thenReturn(checkoutStatus);
        when(response.getStatusInfo()).thenReturn(status);
        when(response.getUserPaymentInteractionId()).thenReturn(userPaymentInteractionId);
        mockShoppingCartInResponse();
    }

    private void mockCheckoutResumeToken(String paymentMethod, ShoppingType shoppingType, int step) {
        when(token.getPaymentMethod()).thenReturn(paymentMethod);
        when(token.getShoppingType()).thenReturn(shoppingType);
        when(token.getInteractionStep()).thenReturn(step);
        when(token.getShoppingId()).thenReturn(BOOKING_ID);
        when(token.getUserPaymentInteractionId()).thenReturn(USER_PAY_INT_ID);
    }

    private void mockShoppingCartInResponse() {
        when(buyer.getMail()).thenReturn(EMAIL);
        when(shoppingCart.getBuyer()).thenReturn(buyer);
        when(shoppingCart.getBookingId()).thenReturn(BOOKING_ID);
        when(response.getShoppingCart()).thenReturn(shoppingCart);
    }

    private void assertParameters(CheckoutResumeRequest checkoutRequest) {
        Map<String, String> additionalParameters = checkoutRequest.getParams();
        assertEquals(additionalParameters.get(STATUS), ACCEPTED);
        assertEquals(additionalParameters.get(ORDER_ID), String.valueOf(BOOKING_ID));
        assertEquals(additionalParameters.get(CURRENCY), GBP);
    }

}
