package com.odigeo.frontend.services;

import com.odigeo.dapi.client.AvailableProductsResponse;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.BookingService;
import com.odigeo.dapi.client.DistributionApiConnection;
import com.odigeo.dapi.client.DistributionApiConnectionFactory;
import com.odigeo.dapi.client.DistributionApiConnectionOptions;
import com.odigeo.dapi.client.FraudInfoRequest;
import com.odigeo.dapi.client.InvoiceRequest;
import com.odigeo.dapi.client.MessageResponse;
import com.odigeo.dapi.client.ModifyShoppingCartRequest;
import com.odigeo.dapi.client.PaymentDataRequest;
import com.odigeo.dapi.client.PersonalInfoRequest;
import com.odigeo.dapi.client.PromotionalCodesMaxUsageExecutionException;
import com.odigeo.dapi.client.ResumeDataRequest;
import com.odigeo.dapi.client.SearchExpiredException;
import com.odigeo.dapi.client.SelectionRequests;
import com.odigeo.dapi.client.SessionExpiredException;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.dapi.client.TestConfigurationSelectionRequest;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResponseInfo;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.services.configurations.dapi.DapiConnectionConfig;
import com.odigeo.frontend.services.configurations.dapi.DapiConnectionFactory;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class DapiServiceTest {
    private static final Long BOOKING_ID = 12L;
    private static final String ID = "id";
    private static final String PASSWORD = "password";
    @Mock
    private ResolverContext context;
    @Mock
    private RequestInfo requestInfo;
    @Mock
    private ResponseInfo responseInfo;
    @Mock
    private DapiConnectionFactory dapiConnectionFactory;
    @Mock
    private DistributionApiConnectionFactory factory;
    @Mock
    private BookingService bookingService;
    @Mock
    private DistributionApiConnection apiConnection;
    @Mock
    private ShoppingCartSummaryResponse summaryResponse;
    @Mock
    private SelectionRequests selectionRequests;
    @Mock
    private TestConfigurationSelectionRequest testConfigurationSelectionRequest;
    @Mock
    private DapiConnectionConfig dapiConnectionConfig;
    @Mock
    private PersonalInfoRequest personalInfoRequest;
    @Mock
    private PaymentDataRequest paymentDataRequest;
    @Mock
    private FraudInfoRequest fraudInfoRequest;
    @Mock
    private InvoiceRequest invoiceRequest;
    @Mock
    private BookingResponse bookingResponse;
    @Mock
    private AvailableProductsResponse availableProductsResponse;
    @Mock
    private ResumeDataRequest resumeDataRequest;
    @Mock
    private ModifyShoppingCartRequest modifyShoppingCartRequest;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @InjectMocks
    private DapiService dapiService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(context.getRequestInfo()).thenReturn(requestInfo);
        when(context.getResponseInfo()).thenReturn(responseInfo);
        when(dapiConnectionConfig.getCredentialId()).thenReturn(ID);
        when(dapiConnectionConfig.getPassword()).thenReturn(PASSWORD);
        configBookingService();
    }

    @Test
    public void testGetShoppingCart() throws Exception {
        when(bookingService.getShoppingCart(BOOKING_ID)).thenReturn(summaryResponse);
        ShoppingCartSummaryResponse response = dapiService.getShoppingCart(BOOKING_ID, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetShoppingCartSessionExpiredException() throws Exception {
        when(bookingService.getShoppingCart(BOOKING_ID))
                .thenThrow(SessionExpiredException.class);
        dapiService.getShoppingCart(BOOKING_ID, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetShoppingCartSearchExpiredException() throws Exception {
        when(bookingService.getShoppingCart(BOOKING_ID))
                .thenThrow(SearchExpiredException.class);
        dapiService.getShoppingCart(BOOKING_ID, context);
    }

    @Test
    public void testCreateShoppingCart() throws Exception {
        when(bookingService.createShoppingCart(selectionRequests, testConfigurationSelectionRequest, ID, PASSWORD)).thenReturn(summaryResponse);
        ShoppingCartSummaryResponse response = dapiService.createShoppingCart(selectionRequests, testConfigurationSelectionRequest, context);
        assertNotNull(response);
    }

    @Test
    public void testAddPersonalInfoToShoppingCart() throws Exception {
        when(bookingService.addPersonalInfoToShoppingCart(personalInfoRequest)).thenReturn(summaryResponse);
        ShoppingCartSummaryResponse response = dapiService.addPersonalInfoToShoppingCart(personalInfoRequest, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testCreateShoppingCartSearchExpiredException() throws Exception {
        when(bookingService.createShoppingCart(selectionRequests, testConfigurationSelectionRequest, ID, PASSWORD))
                .thenThrow(SearchExpiredException.class);
        dapiService.createShoppingCart(selectionRequests, testConfigurationSelectionRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testAddPersonalInfoToShoppingCartSearchExpiredException() throws Exception {
        when(bookingService.addPersonalInfoToShoppingCart(personalInfoRequest))
                .thenThrow(SearchExpiredException.class);
        dapiService.addPersonalInfoToShoppingCart(personalInfoRequest, context);
    }

    private void configBookingService() {
        when(dapiConnectionFactory.getDapiConnectionFactory()).thenReturn(factory);
        when(factory.getConnection()).thenReturn(apiConnection);
        when(apiConnection.getBookingService(any(DistributionApiConnectionOptions.class))).thenReturn(bookingService);
    }

    @Test
    public void testConfirmBooking() throws Exception {
        when(bookingResponse.getShoppingCart()).thenReturn(summaryResponse);
        when(bookingService.confirmBooking(paymentDataRequest, BOOKING_ID, String.valueOf(BOOKING_ID),
                invoiceRequest, fraudInfoRequest)).thenReturn(bookingResponse);
        BookingResponse response = dapiService.confirmBooking(paymentDataRequest, BOOKING_ID, String.valueOf(BOOKING_ID),
                invoiceRequest, fraudInfoRequest, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testConfirmBookingSessionExpiredException() throws Exception {
        when(bookingService.confirmBooking(paymentDataRequest, BOOKING_ID, String.valueOf(BOOKING_ID),
                invoiceRequest, fraudInfoRequest)).thenThrow(SessionExpiredException.class);
        dapiService.confirmBooking(paymentDataRequest, BOOKING_ID, String.valueOf(BOOKING_ID),
                invoiceRequest, fraudInfoRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testConfirmBookingPromoCodeException() throws Exception {
        when(bookingService.confirmBooking(paymentDataRequest, BOOKING_ID, String.valueOf(BOOKING_ID),
                invoiceRequest, fraudInfoRequest)).thenThrow(PromotionalCodesMaxUsageExecutionException.class);
        dapiService.confirmBooking(paymentDataRequest, BOOKING_ID, String.valueOf(BOOKING_ID),
                invoiceRequest, fraudInfoRequest, context);
    }

    @Test
    public void testResumeBooking() throws SessionExpiredException {
        when(bookingService.resumeBooking(BOOKING_ID, resumeDataRequest)).thenReturn(bookingResponse);
        BookingResponse response = dapiService.resumeBooking(BOOKING_ID, resumeDataRequest, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testResumeBookingSessionExpiredException() throws Exception {
        when(bookingService.resumeBooking(BOOKING_ID, resumeDataRequest)).thenThrow(SessionExpiredException.class);
        dapiService.resumeBooking(BOOKING_ID, resumeDataRequest, context);
    }

    @Test
    public void testGetAvailableProducts() throws Exception {
        configBookingService();
        when(bookingService.getAvailableProducts(BOOKING_ID, null)).thenReturn(availableProductsResponse);
        AvailableProductsResponse response = dapiService.getAvailableProducts(BOOKING_ID, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetAvailableProductsSessionExpiredException() throws Exception {
        configBookingService();
        when(bookingService.getAvailableProducts(BOOKING_ID, null)).thenThrow(SessionExpiredException.class);
        dapiService.getAvailableProducts(BOOKING_ID, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetAvailableProductsSearchExpiredException() throws Exception {
        configBookingService();
        when(bookingService.getAvailableProducts(BOOKING_ID, null)).thenThrow(SearchExpiredException.class);
        dapiService.getAvailableProducts(BOOKING_ID, context);
    }

    @Test
    public void testGetAvailableProductsReturnEmptyMessageResponses() throws Exception {
        configBookingService();
        when(bookingService.getAvailableProducts(BOOKING_ID, null)).thenReturn(availableProductsResponse);
        when(availableProductsResponse.getMessages()).thenReturn(Collections.emptyList());
        AvailableProductsResponse response = dapiService.getAvailableProducts(BOOKING_ID, context);
        assertNotNull(response);
    }

    @Test
    public void testGetAvailableProductsReturnNonEmptyMessageButNotanErrorResponses() throws Exception {
        configBookingService();
        when(bookingService.getAvailableProducts(BOOKING_ID, null)).thenReturn(availableProductsResponse);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode("OTHER");
        when(availableProductsResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));
        AvailableProductsResponse response = dapiService.getAvailableProducts(BOOKING_ID, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetAvailableProductsReturnERRErrorInsideResponses() throws Exception {
        configBookingService();
        when(bookingService.getAvailableProducts(BOOKING_ID, null)).thenReturn(availableProductsResponse);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode("ERR-OR");
        when(availableProductsResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));
        AvailableProductsResponse response = dapiService.getAvailableProducts(BOOKING_ID, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetAvailableProductsReturnINTErrorInsideResponses() throws Exception {
        configBookingService();
        when(bookingService.getAvailableProducts(BOOKING_ID, null)).thenReturn(availableProductsResponse);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode("INT-ERNAL");
        when(availableProductsResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));
        AvailableProductsResponse response = dapiService.getAvailableProducts(BOOKING_ID, context);
        assertNotNull(response);
    }

    @Test
    public void testAddToShoppingCart() throws Exception {
        configBookingService();
        when(bookingService.addToShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);

        ShoppingCartSummaryResponse response = dapiService.addToShoppingCart(modifyShoppingCartRequest, context);

        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testAddToShoppingCartSessionExpiredException() throws Exception {
        configBookingService();
        when(bookingService.addToShoppingCart(modifyShoppingCartRequest)).thenThrow(SessionExpiredException.class);
        dapiService.addToShoppingCart(modifyShoppingCartRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testAddToShoppingCartSearchExpiredException() throws Exception {
        configBookingService();
        when(bookingService.addToShoppingCart(modifyShoppingCartRequest)).thenThrow(SearchExpiredException.class);
        dapiService.addToShoppingCart(modifyShoppingCartRequest, context);
    }

    @Test
    public void testAddToShoppingCartReturnEmptyMessageResponses() throws Exception {
        configBookingService();
        when(bookingService.addToShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getMessages()).thenReturn(Collections.emptyList());

        ShoppingCartSummaryResponse response = dapiService.addToShoppingCart(modifyShoppingCartRequest, context);
        assertNotNull(response);
    }

    @Test
    public void testAddToShoppingCartReturnNonEmptyMessageButNotanErrorResponses() throws Exception {
        configBookingService();
        when(bookingService.addToShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode("OTHER");
        when(shoppingCartSummaryResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));

        ShoppingCartSummaryResponse response = dapiService.addToShoppingCart(modifyShoppingCartRequest, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testAddToShoppingCartReturnERRErrorInsideResponses() throws Exception {
        configBookingService();
        when(bookingService.addToShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode("ERR-OR");
        when(shoppingCartSummaryResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));

        ShoppingCartSummaryResponse response = dapiService.addToShoppingCart(modifyShoppingCartRequest, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testAddToShoppingCartReturnINTErrorInsideResponses() throws Exception {
        configBookingService();
        when(bookingService.addToShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode("INT-ERNAL");
        when(shoppingCartSummaryResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));

        ShoppingCartSummaryResponse response = dapiService.addToShoppingCart(modifyShoppingCartRequest, context);
        assertNotNull(response);
    }

    @Test
    public void testRemoveFromShoppingCart() throws Exception {
        configBookingService();
        when(bookingService.removeFromShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);

        ShoppingCartSummaryResponse response = dapiService.removeFromShoppingCart(modifyShoppingCartRequest, context);

        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testRemoveFromShoppingCartSessionExpiredException() throws Exception {
        configBookingService();
        when(bookingService.removeFromShoppingCart(modifyShoppingCartRequest)).thenThrow(SessionExpiredException.class);
        dapiService.removeFromShoppingCart(modifyShoppingCartRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testRemoveFromShoppingCartSearchExpiredException() throws Exception {
        configBookingService();
        when(bookingService.removeFromShoppingCart(modifyShoppingCartRequest)).thenThrow(SearchExpiredException.class);
        dapiService.removeFromShoppingCart(modifyShoppingCartRequest, context);
    }

    @Test
    public void testRemoveFromShoppingCartReturnEmptyMessageResponses() throws Exception {
        configBookingService();
        when(bookingService.removeFromShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getMessages()).thenReturn(Collections.emptyList());

        ShoppingCartSummaryResponse response = dapiService.removeFromShoppingCart(modifyShoppingCartRequest, context);
        assertNotNull(response);
    }

    @Test
    public void testRemoveFromShoppingCartReturnNonEmptyMessageButNotanErrorResponses() throws Exception {
        configBookingService();
        when(bookingService.removeFromShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode("OTHER");
        when(shoppingCartSummaryResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));

        ShoppingCartSummaryResponse response = dapiService.removeFromShoppingCart(modifyShoppingCartRequest, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testRemoveFromShoppingCartReturnERRErrorInsideResponses() throws Exception {
        configBookingService();
        when(bookingService.removeFromShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode("ERR-OR");
        when(shoppingCartSummaryResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));
        ShoppingCartSummaryResponse response = dapiService.removeFromShoppingCart(modifyShoppingCartRequest, context);
        assertNotNull(response);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testRemoveFromShoppingCartReturnINTErrorInsideResponses() throws Exception {
        configBookingService();
        when(bookingService.removeFromShoppingCart(modifyShoppingCartRequest)).thenReturn(shoppingCartSummaryResponse);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setCode("INT-ERNAL");
        when(shoppingCartSummaryResponse.getMessages()).thenReturn(Collections.singletonList(messageResponse));
        ShoppingCartSummaryResponse response = dapiService.removeFromShoppingCart(modifyShoppingCartRequest, context);
        assertNotNull(response);
    }
}
