package com.odigeo.frontend.services.membership;

import com.google.inject.Guice;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.frontend.services.membership.MembershipServiceHandler.SET_REMIND_ME_LATER;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipServiceHandlerTest {
    private static final long MEMBERSHIP_ID = 1234L;
    private static final String VALID_TOKEN_REQUEST = "abcde";
    private static final String VALID_TOKEN_RESPONSE = "name=Rodrigo&email=rodrigo@diazdevivar.com";

    @Mock
    private MembershipService membershipService;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private FreeTrialCandidateRequest freeTrialCandidateRequest;
    @InjectMocks
    private MembershipServiceHandler membershipServiceHandler;
    @Captor
    private ArgumentCaptor<UpdateMembershipRequest> updateMembershipRequestArgumentCaptor;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(MembershipService.class).toProvider(() -> membershipService);
        });
    }

    @Test
    public void testEnableAutoRenewal() {
        when(membershipService.updateMembership(any(UpdateMembershipRequest.class))).thenReturn(Boolean.TRUE);
        membershipServiceHandler.enableAutoRenewal(MEMBERSHIP_ID);
        verify(membershipService).updateMembership(any(UpdateMembershipRequest.class));
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_UPDATE_ENABLE_RENEWAL_STATUS);
    }

    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = "Failed to execute operation ENABLE_AUTO_RENEWAL")
    public void testEnableAutoRenewalFailed() {
        membershipServiceHandler.enableAutoRenewal(MEMBERSHIP_ID);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_UPDATE_FAILED);
    }

    @Test
    public void testDisableAutoRenewal() {
        when(membershipService.updateMembership(any(UpdateMembershipRequest.class))).thenReturn(Boolean.TRUE);
        membershipServiceHandler.disableAutoRenewal(MEMBERSHIP_ID);
        verify(membershipService).updateMembership(any(UpdateMembershipRequest.class));
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_UPDATE_DISABLE_RENEWAL_STATUS);
    }

    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = "Failed to execute operation DISABLE_AUTO_RENEWAL")
    public void testDisableAutoRenewalFailed() {
        membershipServiceHandler.disableAutoRenewal(MEMBERSHIP_ID);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_UPDATE_FAILED);
    }

    @Test
    public void testSetRemindMeLaterSuccessfully() {
        when(membershipService.updateMembership(any(UpdateMembershipRequest.class))).thenReturn(Boolean.TRUE);
        membershipServiceHandler.setRemindMeLater(MEMBERSHIP_ID);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_UPDATE_SET_REMINDER_LATER);
        verifySetRemindMeLaterParameters();
    }

    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = "Failed to execute operation SET_REMIND_ME_LATER")
    public void testSetRemindMeLaterFailed() {
        when(membershipService.updateMembership(any())).thenReturn(false);
        membershipServiceHandler.setRemindMeLater(MEMBERSHIP_ID);
        verifySetRemindMeLaterParameters();
    }

    private void verifySetRemindMeLaterParameters() {
        verify(membershipService).updateMembership(updateMembershipRequestArgumentCaptor.capture());
        assertEquals(String.valueOf(MEMBERSHIP_ID), updateMembershipRequestArgumentCaptor.getValue().getMembershipId());
        assertEquals(Boolean.TRUE, updateMembershipRequestArgumentCaptor.getValue().getRemindMeLater());
        assertEquals(SET_REMIND_ME_LATER, updateMembershipRequestArgumentCaptor.getValue().getOperation());
    }

    @Test
    public void testDecryptToken() {
        when(membershipService.decrypt(any())).thenReturn(VALID_TOKEN_RESPONSE);
        assertEquals(membershipServiceHandler.decryptToken(VALID_TOKEN_REQUEST), VALID_TOKEN_RESPONSE);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_DECRYPT_TOKEN);
    }

    @Test
    public void testIsEligibleForFreeTrial() {
        when(membershipService.isEligibleForFreeTrial(freeTrialCandidateRequest)).thenReturn(Boolean.TRUE);
        assertTrue(membershipServiceHandler.isEligibleForFreeTrial(freeTrialCandidateRequest));
        verify(metricsHandler, never()).trackCounter(MeasureConstants.MEMBERSHIP_FREE_TRIAL_ABUSER);

    }

    @Test
    public void testIsEligibleForFreeTrialWhenFalse() {
        when(membershipService.isEligibleForFreeTrial(freeTrialCandidateRequest)).thenReturn(Boolean.FALSE);
        assertFalse(membershipServiceHandler.isEligibleForFreeTrial(freeTrialCandidateRequest));
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_FREE_TRIAL_ABUSER);
    }
}
