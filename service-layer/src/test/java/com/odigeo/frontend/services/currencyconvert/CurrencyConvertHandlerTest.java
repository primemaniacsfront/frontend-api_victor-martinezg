package com.odigeo.frontend.services.currencyconvert;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CurrencyConvertHandlerTest {

    @Mock
    private CurrencyExchangeHandler currencyExchangeHandler;
    @Mock
    private VisitInformation visitInformation;

    @InjectMocks
    private CurrencyConvertHandler currencyConvertHandler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(currencyExchangeHandler.getCurrencyConvertRate(anyString(), anyString())).thenReturn(BigDecimal.TEN);
        when(visitInformation.getCurrency()).thenReturn("EUR");
    }

    @Test
    public void testCurrencyExchange() {
        BigDecimal convertedAmount = currencyConvertHandler.convertToSiteCurrency(visitInformation, BigDecimal.TEN, "GBP");
        assertEquals(convertedAmount, BigDecimal.valueOf(100).setScale(2));
    }

}
