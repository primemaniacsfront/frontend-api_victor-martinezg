package com.odigeo.frontend.services;

import com.google.inject.Guice;
import com.odigeo.collectionmethod.v3.CollectionMethodApiService;
import com.odigeo.collectionmethod.v3.CreditCardBinDetails;
import com.odigeo.collectionmethod.v3.InvalidParametersException;
import com.odigeo.frontend.commons.exception.ServiceException;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.UndeclaredThrowableException;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CollectionMethodServiceTest {

    private static final String BIN = "12334567890";

    @Mock
    private CollectionMethodApiService collectionMethodApiService;
    @Mock
    private CreditCardBinDetails binDetails;

    private CollectionMethodService service;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        service = new CollectionMethodService();

        Guice.createInjector(binder -> {
            binder.bind(CollectionMethodApiService.class).toProvider(() -> collectionMethodApiService);
        }).injectMembers(service);
    }

    @Test
    public void testGetCreditCardBinDetails() {
        when(binDetails.getCreditCardBin()).thenReturn(BIN);
        when(collectionMethodApiService.getCreditCardBinDetails(anyString())).thenReturn(binDetails);
        CreditCardBinDetails creditCardBinDetails = service.getCreditCardBinDetails(BIN);
        assertEquals(creditCardBinDetails.getCreditCardBin(), BIN);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetCreditCardBinDetailsEx() {
        when(collectionMethodApiService.getCreditCardBinDetails(anyString())).thenThrow(InvalidParametersException.class);
        service.getCreditCardBinDetails(StringUtils.EMPTY);

        when(collectionMethodApiService.getCreditCardBinDetails(anyString())).thenThrow(UndeclaredThrowableException.class);
        service.getCreditCardBinDetails(StringUtils.EMPTY);
    }

}
