package com.odigeo.frontend.services.onefront;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import org.apache.http.client.CookieStore;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchSessionLoaderServiceBrowserStorageHelperTest {
    private static final String JSESSIONID = "OF1JSESSIONID";
    private static final String VALUE = "VALUE";
    private static final String OTHER_COOKIE = "OTHER_COOKIE";

    @Mock
    ResolverContext context;
    @Mock
    CookieStore cookieStore;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createSearchSessionLoaderServiceCookieStore() {
        RequestInfo requestInfo = Mockito.mock(RequestInfo.class);
        when(requestInfo.getHeaderOrCookie(SiteHeaders.OF_JSESSIONID, SiteCookies.OF_JSESSIONID))
                .thenReturn(VALUE);
        when(context.getRequestInfo()).thenReturn(requestInfo);

        CookieStore cookieStore = SearchSessionLoaderServiceBrowserStorageHelper.createSearchSessionLoaderCookieStore(context);

        assertTrue(cookieStore.getCookies().size() == 3);
        assertEquals(cookieStore.getCookies().get(0).getName(), JSESSIONID);
    }

    @Test
    public void createSearchSessionLoaderServiceCookieStoreNoCookies() {
        RequestInfo requestInfo = Mockito.mock(RequestInfo.class);
        when(requestInfo.getHeaderOrCookie(SiteHeaders.OF_JSESSIONID, SiteCookies.OF_JSESSIONID))
                .thenReturn(null);
        when(context.getRequestInfo()).thenReturn(requestInfo);

        CookieStore cookieStore = SearchSessionLoaderServiceBrowserStorageHelper.createSearchSessionLoaderCookieStore(context);

        assertTrue(cookieStore.getCookies().size() == 3);
        assertEquals(cookieStore.getCookies().get(0).getValue(), null);
        assertEquals(cookieStore.getCookies().get(1).getValue(), null);
        assertEquals(cookieStore.getCookies().get(2).getValue(), null);
    }

}
