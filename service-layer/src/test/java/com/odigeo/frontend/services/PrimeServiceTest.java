package com.odigeo.frontend.services;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.prime.PrimeMarketsConfiguration;
import com.odigeo.frontend.resolvers.prime.models.PrimeMarket;
import com.odigeo.frontend.resolvers.prime.models.PrimeMarketStatus;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.userprofiles.api.v2.model.User;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class PrimeServiceTest {

    private static final Site ENABLED_SITE = Site.ES;
    public static final String MKT_PORTAL = "kayak";
    private static final int ONE_MONTH = 1;
    private static final int TWO_MONTHS = 2;

    @Mock
    private MembershipHandler membershipHandler;
    @Mock
    private VisitInformation visit;
    @Mock
    private PrimeMarketsConfiguration primeMarketsConfiguration;
    @Mock
    private PrimeMarket primeMarket;
    @Mock
    private SiteVariations siteVariations;
    @Mock
    private Membership membership;
    @Mock
    private User user;

    @InjectMocks
    private PrimeService primeService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(MembershipHandler.class).toProvider(() -> membershipHandler);
        });
    }

    @Test
    public void testGetPrimeInfo() {
        Long userId = 1L;
        when(user.getId()).thenReturn(userId);
        when(visit.getSite()).thenReturn(ENABLED_SITE);
        when(membershipHandler.getMembership(eq(user), eq(visit))).thenReturn(membership);

        assertEquals(membershipHandler.getMembership(user, visit), membership);
    }

    @Test
    public void testGetPrimeInfoWithoutUser() {
        when(visit.getSite()).thenReturn(ENABLED_SITE);
        assertNull(membershipHandler.getMembership(null, visit));
    }

    @Test
    public void testIsPrimeSiteRollout() {
        when(visit.getSite()).thenReturn(ENABLED_SITE);
        when(primeMarketsConfiguration.getPrimeMarket(ENABLED_SITE)).thenReturn(primeMarket);
        when(primeMarket.getStatus()).thenReturn(PrimeMarketStatus.ROLLOUT);
        assertTrue(primeService.isPrimeSite(visit));
    }

    @Test
    public void testIsPrimeSiteInABActive() {
        when(visit.getSite()).thenReturn(ENABLED_SITE);
        when(primeMarketsConfiguration.getPrimeMarket(ENABLED_SITE)).thenReturn(primeMarket);
        when(primeMarket.getStatus()).thenReturn(PrimeMarketStatus.IN_AB);
        when(siteVariations.isPrimeTestEnabled(eq(visit))).thenReturn(Boolean.TRUE);
        assertTrue(primeService.isPrimeSite(visit));
    }

    @Test
    public void testIsPrimeSiteInABNotActive() {
        when(visit.getSite()).thenReturn(ENABLED_SITE);
        when(primeMarketsConfiguration.getPrimeMarket(ENABLED_SITE)).thenReturn(primeMarket);
        when(primeMarket.getStatus()).thenReturn(PrimeMarketStatus.IN_AB);
        assertFalse(primeService.isPrimeSite(visit));
    }

    @Test
    public void testIsPrimeSiteUnknownStatus() {
        when(visit.getSite()).thenReturn(ENABLED_SITE);
        when(primeMarketsConfiguration.getPrimeMarket(ENABLED_SITE)).thenReturn(primeMarket);
        assertFalse(primeService.isPrimeSite(visit));
    }

    @Test
    public void testIsPrimeSiteWithExcludedMktPortal() {
        Set<String> excludedMktPortal = new HashSet<>();
        excludedMktPortal.add(MKT_PORTAL);
        when(visit.getSite()).thenReturn(ENABLED_SITE);
        when(visit.getMktPortal()).thenReturn(MKT_PORTAL);
        when(primeMarketsConfiguration.getPrimeMarket(ENABLED_SITE)).thenReturn(primeMarket);
        when(primeMarket.getExcludedMktPortals()).thenReturn(excludedMktPortal);
        when(primeMarket.getStatus()).thenReturn(PrimeMarketStatus.ROLLOUT);
        assertFalse(primeService.isPrimeSite(visit));
    }

    @Test
    public void testIsPrimeSiteWithExcludedMktPortalButABEnabled() {
        Set<String> excludedMktPortal = new HashSet<>(Arrays.asList(MKT_PORTAL));
        when(visit.getSite()).thenReturn(ENABLED_SITE);
        when(visit.getMktPortal()).thenReturn(MKT_PORTAL);
        when(siteVariations.isMembershipDisplayedMeta(eq(visit))).thenReturn(Boolean.TRUE);
        when(primeMarketsConfiguration.getPrimeMarket(ENABLED_SITE)).thenReturn(primeMarket);
        when(primeMarket.getExcludedMktPortals()).thenReturn(excludedMktPortal);
        when(primeMarket.getStatus()).thenReturn(PrimeMarketStatus.ROLLOUT);
        assertTrue(primeService.isPrimeSite(visit));
    }

    @Test
    public void testGetExpiringCreditCardPeriodForPrimeMarket() {
        when(primeMarketsConfiguration.getPrimeMarket(ENABLED_SITE)).thenReturn(primeMarket);
        when(primeMarket.getExpiringCreditCardPeriod()).thenReturn(TWO_MONTHS);
        assertEquals(primeService.getExpiringCreditCardPeriod(ENABLED_SITE), TWO_MONTHS);
    }

    @Test
    public void testGetExpiringCreditCardPeriodForNonPrimeMarket() {
        when(primeMarketsConfiguration.getPrimeMarket(ENABLED_SITE)).thenReturn(null);
        when(primeMarket.getExpiringCreditCardPeriod()).thenReturn(ONE_MONTH);
        assertEquals(primeService.getExpiringCreditCardPeriod(ENABLED_SITE), ONE_MONTH);
    }

}
