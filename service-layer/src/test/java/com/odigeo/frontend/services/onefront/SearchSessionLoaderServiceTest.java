package com.odigeo.frontend.services.onefront;

import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResponseInfo;
import com.odigeo.shoppingbasket.v2.responses.ShoppingBasketResponse;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SearchSessionLoaderServiceTest {

    private static final String ERROR_MSG = "ERROR";
    private static final long SHOPPING_BASKET_ID = 1L;

    @Mock
    private com.odigeo.frontend.contract.onefront.SearchSessionLoaderService searchSessionLoaderService;
    @Mock
    private ShoppingBasketResponse shoppingBasketResponse;
    @Mock
    private ResolverContext context;
    @Mock
    private SearchSessionLoaderServiceFactory searchSessionLoaderServiceFactory;
    @Mock
    private RequestInfo requestInfo;
    @Mock
    private ResponseInfo responseInfo;

    @InjectMocks
    private SearchSessionLoaderService searchSessionLoaderService1;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(context.getResponseInfo()).thenReturn(responseInfo);
        when(context.getRequestInfo()).thenReturn(requestInfo);
        BasicCookieStore basicCookieStore = new BasicCookieStore();
        basicCookieStore.addCookie(new BasicClientCookie("JSESSIONID", "value"));
        when(searchSessionLoaderServiceFactory.getSearchSessionLoaderService(any(), any()))
                .thenReturn(new ImmutablePair<>(searchSessionLoaderService, basicCookieStore));
    }

    @Test
    public void loadSearchInfoInSession() {
        searchSessionLoaderService1.loadSearchInfoInSession("searchResponse", context);
        verify(searchSessionLoaderService).loadSearchInfoInSession(any());
    }

}
