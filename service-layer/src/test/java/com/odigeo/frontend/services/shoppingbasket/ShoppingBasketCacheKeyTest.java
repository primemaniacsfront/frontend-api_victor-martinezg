package com.odigeo.frontend.services.shoppingbasket;

import bean.test.BeanTest;
import com.odigeo.dapi.client.SerializableCookie;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ShoppingBasketCacheKeyTest extends BeanTest<ShoppingBasketCacheKey> {

    private static final String COOKIE_CACHE_KEY = "key";

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(ShoppingBasketCacheKey.class)
            .suppress(Warning.NONFINAL_FIELDS)
            .usingGetClass()
            .verify();
    }

    @Test
    public void testHashCode() {
        assertEquals(getBean().hashCode(), getBean().hashCode());
    }

    @Override
    protected ShoppingBasketCacheKey getBean() {
        return new ShoppingBasketCacheKey(COOKIE_CACHE_KEY);
    }

    private SerializableCookie getCookie(boolean withApiCookie) {
        if (withApiCookie) {
            return getCookie("API1=api1", false);
        }
        return getCookie("JSESSIONID=session", true);
    }

    private SerializableCookie getCookie(String nameAndValue, boolean secure) {
        return new SerializableCookie(null, nameAndValue, "/", "JSESSIONID", secure);
    }
}
