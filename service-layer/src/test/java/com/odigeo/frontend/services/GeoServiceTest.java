package com.odigeo.frontend.services;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.resolvers.geolocation.suggestions.models.GeolocationSuggestionsRequestDTO;
import com.odigeo.geoapi.v4.AutoCompleteService;
import com.odigeo.geoapi.v4.CityService;
import com.odigeo.geoapi.v4.CountryService;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoServiceException;
import com.odigeo.geoapi.v4.InvalidParametersException;
import com.odigeo.geoapi.v4.ItineraryLocationService;
import com.odigeo.geoapi.v4.request.ProductType;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Country;
import com.odigeo.geoapi.v4.responses.ItineraryLocation;
import com.odigeo.geoapi.v4.responses.LocationDescriptionSorted;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class GeoServiceTest {

    private static final String SEARCH_WORD = "BAR";
    private static final String LOCALE = "es_ES";
    private static final String WEBSITE = "ES";
    private static final String INPUT_TYPE = "DEPARTURE";
    public static final int GEO_NODE = 25;

    @Mock
    private ItineraryLocationService itineraryService;
    @Mock
    private CityService cityService;
    @Mock
    private CountryService countryService;
    @Mock
    private AutoCompleteService autoCompleteService;
    @Mock
    private Logger logger;

    @InjectMocks
    private GeoService geoService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetItineraryLocation() throws Exception {
        int geoId = 1;
        ItineraryLocation location = mock(ItineraryLocation.class);

        when(itineraryService.getItineraryLocationByGeoNodeId(geoId)).thenReturn(location);
        assertSame(geoService.getItineraryLocation(geoId), location);
    }

    @Test
    public void testGetItineraryLocationNotFound() throws Exception {
        when(itineraryService.getItineraryLocationByGeoNodeId(anyInt())).thenThrow(GeoNodeNotFoundException.class);
        assertNull(geoService.getItineraryLocation(1));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetItineraryLocationException() throws Exception {
        when(itineraryService.getItineraryLocationByGeoNodeId(anyInt())).thenThrow(GeoServiceException.class);
        geoService.getItineraryLocation(1);
    }

    @Test
    public void testGetCity() throws Exception {
        int geoId = 1;
        City city = mock(City.class);

        when(cityService.getEntityByGeoNodeId(geoId)).thenReturn(city);
        assertSame(geoService.getCityByGeonodeId(geoId), city);
    }

    @Test(expectedExceptions = GeoNodeNotFoundException.class)
    public void testGetCityNotFound() throws Exception {
        when(cityService.getEntityByGeoNodeId(anyInt())).thenThrow(GeoNodeNotFoundException.class);
        geoService.getCityByGeonodeId(1);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetCityException() throws Exception {
        when(cityService.getEntityByGeoNodeId(anyInt())).thenThrow(GeoServiceException.class);
        geoService.getCityByGeonodeId(1);
    }

    @Test
    public void testGetGeolocationSuggestions() throws Exception {
        GeolocationSuggestionsRequestDTO geolocationSuggestionsRequestDTO = buildGeolocationSuggestionsRequest();
        List<LocationDescriptionSorted> geolocationSuggestions = Collections.singletonList(mock(LocationDescriptionSorted.class));

        when(autoCompleteService.autoCompleteSorted(SEARCH_WORD, LOCALE, ProductType.FLIGHT, WEBSITE, INPUT_TYPE,
                Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE)).thenReturn(geolocationSuggestions);

        assertSame(geoService.getGeolocationSuggestions(geolocationSuggestionsRequestDTO), geolocationSuggestions);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetGeolocationSuggestionsException() throws Exception {
        GeolocationSuggestionsRequestDTO geolocationSuggestionsRequestDTO = buildGeolocationSuggestionsRequest();

        when(autoCompleteService.autoCompleteSorted(SEARCH_WORD, LOCALE, ProductType.FLIGHT, WEBSITE, INPUT_TYPE,
                Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE)).thenThrow(InvalidParametersException.class);

        geoService.getGeolocationSuggestions(geolocationSuggestionsRequestDTO);
    }

    @Test
    public void testGetCountries() throws GeoServiceException {
        Country countryMock = mock(Country.class);
        when(countryMock.getGeoNodeId()).thenReturn(GEO_NODE);

        List<Country> countries = Collections.singletonList(countryMock);
        when(countryService.retrieveAll()).thenReturn(countries);
        assertSame(countryService.retrieveAll().get(0).getGeoNodeId(), GEO_NODE);
    }

    private GeolocationSuggestionsRequestDTO buildGeolocationSuggestionsRequest() {
        GeolocationSuggestionsRequestDTO request = new GeolocationSuggestionsRequestDTO();

        request.setSearchWord(SEARCH_WORD);
        request.setLocale(LOCALE);
        request.setProductType(ProductType.FLIGHT);
        request.setWebsite(WEBSITE);
        request.setInputType(INPUT_TYPE);
        request.setIncludeMultiLanguage(Boolean.TRUE);
        request.setIncludeRelatedLocations(Boolean.TRUE);
        request.setIncludeSearchInAllWords(Boolean.TRUE);
        request.setIncludeCountry(Boolean.TRUE);
        request.setIncludeRegion(Boolean.TRUE);
        request.setIncludeNearestLocations(Boolean.TRUE);

        return request;
    }
}
