package com.odigeo.frontend.services.accommodation;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationBuyer;
import com.odigeo.accommodation.product.model.CreateProduct;
import com.odigeo.accommodation.product.model.ProductId;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AccommodationProductServiceTest {

    @Mock
    private com.odigeo.accommodation.product.AccommodationProductService accommodationProductService;

    @InjectMocks
    private AccommodationProductService testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    public void testCreateProduct() {
        UUID uuid = UUID.randomUUID();
        when(accommodationProductService.createProduct(any(CreateProduct.class))).thenReturn(new ProductId(uuid));
        String product = testClass.createProduct("offerId", getAccommodationBuyer());
        assertNotNull(product);
        assertEquals(product, uuid.toString());
    }

    private AccommodationBuyer getAccommodationBuyer() {
        AccommodationBuyer buyer = new AccommodationBuyer();
        buyer.setDayOfBirth(30);
        buyer.setMonthOfBirth(8);
        buyer.setYearOfBirth(1990);
        return buyer;
    }
}
