package com.odigeo.frontend.services.configurations.dapi;

import bean.test.BeanTest;

import javax.servlet.http.Cookie;
import java.util.Date;

public class DapiCookieHelperTest extends BeanTest<DapiCookieHelper> {

    @Override
    protected DapiCookieHelper getBean() {
        Cookie cookie = new Cookie("name", "val");
        DapiCookieHelper bean = new DapiCookieHelper(cookie);
        bean.setExpirationDate(new Date());
        return bean;
    }
}
