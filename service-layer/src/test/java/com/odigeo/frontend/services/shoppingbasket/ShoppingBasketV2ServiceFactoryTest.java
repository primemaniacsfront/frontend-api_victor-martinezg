package com.odigeo.frontend.services.shoppingbasket;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.commons.rest.monitoring.MonitoringConfigurationException;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.shoppingbasket.v2.ShoppingBasketService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class ShoppingBasketV2ServiceFactoryTest {

    private static final String SESSION_VALUE = "sessionValue";
    private static final String OTHER_SESSION_VALUE = "otherSessionValue";
    private static final String JSESSIONID = "JSESSIONID";
    private static final String OTHER_COOKIE_NAME = "API1";
    private static final String OTHER_COOKIE_VALUE = "api1";
    private static final String SBSERVICE_CACHE_NAME = "sbservice";

    private ShoppingBasketServiceFactory shoppingBasketServiceCmfFactory;

    @Mock
    private ShoppingBasketService service;
    @Mock
    private CookieStore cookieStore;
    @Mock
    private ShoppingBasketService shoppingBasketService;

    @BeforeMethod
    private void setUp() throws CacheException {
        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init();
        JCS.getInstance(SBSERVICE_CACHE_NAME).clear();

        shoppingBasketServiceCmfFactory = Mockito.spy(new ShoppingBasketServiceFactory());
        doNothing().when(shoppingBasketServiceCmfFactory).addMonitoringInterceptor(any());
    }

    private CookieStore getCookieStore(String value) {
        CookieStore cookieStore = new BasicCookieStore();

        cookieStore.addCookie(new BasicClientCookie(JSESSIONID, value));
        cookieStore.addCookie(new BasicClientCookie(OTHER_COOKIE_NAME, OTHER_COOKIE_VALUE));

        return cookieStore;
    }

    private CookieStore getCookieStoreWithPath(String value) {
        CookieStore cookieStore = new BasicCookieStore();

        BasicClientCookie jsessionidCookie = new BasicClientCookie(JSESSIONID, value);
        jsessionidCookie.setPath("path");
        cookieStore.addCookie(jsessionidCookie);
        cookieStore.addCookie(new BasicClientCookie(OTHER_COOKIE_NAME, OTHER_COOKIE_VALUE));

        return cookieStore;
    }

    @Test
    public void getShoppingBasketServiceOKWithoutCookie() {
        assertNotNull(shoppingBasketServiceCmfFactory.getShoppingBasketService(new BasicCookieStore()));

        verify(shoppingBasketServiceCmfFactory).addMonitoringInterceptor(any());
    }

    @Test
    public void getShoppingBasketServiceOKWithCookie() {
        assertNotNull(shoppingBasketServiceCmfFactory.getShoppingBasketService(getCookieStore(SESSION_VALUE)));

        verify(shoppingBasketServiceCmfFactory).addMonitoringInterceptor(any());
    }

    @Test
    public void getShoppingBasketServiceTestingCache() throws IllegalAccessException, IOException, InvocationTargetException {
        CookieStore cookieStore = getCookieStore(SESSION_VALUE);
        Pair<ShoppingBasketService, CookieStore> pairServiceCookie = shoppingBasketServiceCmfFactory.getShoppingBasketService(cookieStore);
        shoppingBasketServiceCmfFactory.updateServiceCache(pairServiceCookie);
        Pair<ShoppingBasketService, CookieStore> pairServiceCookieSecond = shoppingBasketServiceCmfFactory.getShoppingBasketService(cookieStore);

        verify(shoppingBasketServiceCmfFactory).createShoppingBasketService(any(), any());

        assertEquals(pairServiceCookie, pairServiceCookieSecond);
    }


    @Test(expectedExceptions = ServiceException.class)
    public void getShoppingBasketServiceIllegalAccessExceptionShouldThrowServiceException() throws IllegalAccessException, IOException, InvocationTargetException {
        doThrow(new IllegalAccessException())
            .when(shoppingBasketServiceCmfFactory)
            .createShoppingBasketService(any(ServiceConfiguration.class), any(CookieStore.class));

        shoppingBasketServiceCmfFactory.getShoppingBasketService(getCookieStore(SESSION_VALUE));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getShoppingBasketServiceIOExceptionShouldThrowServiceException() throws IllegalAccessException, IOException, InvocationTargetException {
        doThrow(new IOException())
            .when(shoppingBasketServiceCmfFactory)
            .createShoppingBasketService(any(ServiceConfiguration.class), any(CookieStore.class));

        shoppingBasketServiceCmfFactory.getShoppingBasketService(getCookieStore(SESSION_VALUE));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getShoppingBasketServiceInvocationTargetExceptionShouldThrowServiceException() throws IllegalAccessException, IOException, InvocationTargetException {
        doThrow(new InvocationTargetException(new Exception()))
            .when(shoppingBasketServiceCmfFactory)
            .createShoppingBasketService(any(ServiceConfiguration.class), any(CookieStore.class));

        shoppingBasketServiceCmfFactory.getShoppingBasketService(getCookieStore(SESSION_VALUE));
    }

    @Test
    public void getShoppingBasketServiceFactoryTwoCallsSameCookieAfterAnUpdate() {
        CookieStore cookieStoreWithPath = getCookieStoreWithPath(SESSION_VALUE);
        Pair<ShoppingBasketService, CookieStore> pair = shoppingBasketServiceCmfFactory.getShoppingBasketService(cookieStoreWithPath);
        shoppingBasketServiceCmfFactory.updateServiceCache(pair);

        assertEquals(pair.hashCode(), shoppingBasketServiceCmfFactory.getShoppingBasketService(cookieStoreWithPath).hashCode());
        verify(shoppingBasketServiceCmfFactory).addMonitoringInterceptor(any());
    }

    @Test
    public void noCacheInitialized() {
        assertFalse(shoppingBasketServiceCmfFactory.isCacheInitialized());
    }

    @Test
    public void updateServiceCacheWithNoCacheInitializedShouldNotBreakTheExecution() {
        Pair<ShoppingBasketService, CookieStore> pair = new ImmutablePair<>(service, cookieStore);

        shoppingBasketServiceCmfFactory.updateServiceCache(pair);

        assertTrue(shoppingBasketServiceCmfFactory.isCacheInitialized());
    }

    @Test
    public void getShoppingBasketServiceFactoryTwoCallsDifferentCookie() {
        Pair<ShoppingBasketService, CookieStore> pairServiceCookie = shoppingBasketServiceCmfFactory.getShoppingBasketService(getCookieStore(SESSION_VALUE));
        Pair<ShoppingBasketService, CookieStore> pairServiceCookieSecond = shoppingBasketServiceCmfFactory.getShoppingBasketService(getCookieStore(OTHER_SESSION_VALUE));

        assertNotEquals(pairServiceCookie.getRight(), pairServiceCookieSecond.getRight());

        verify(shoppingBasketServiceCmfFactory, times(2)).addMonitoringInterceptor(any());
    }

    @Test(expectedExceptions = MonitoringConfigurationException.class)
    public void getShoppingBasketServiceThrowMonitoringConfigurationException() {
        doCallRealMethod().when(shoppingBasketServiceCmfFactory).addMonitoringInterceptor(any());
        shoppingBasketServiceCmfFactory.getShoppingBasketService(getCookieStore(SESSION_VALUE));
    }

    @Test
    public void buildCacheDoesNotCreateACache() throws CacheException {
        doNothing().when(shoppingBasketServiceCmfFactory).buildCache();
        Pair<ShoppingBasketService, CookieStore> pairServiceCookieStore = shoppingBasketServiceCmfFactory.getShoppingBasketService(getCookieStore(SESSION_VALUE));
        shoppingBasketServiceCmfFactory.updateServiceCache(pairServiceCookieStore);
        Pair<ShoppingBasketService, CookieStore> pairServiceCookieStoreSecond = shoppingBasketServiceCmfFactory.getShoppingBasketService(pairServiceCookieStore.getRight());

        assertNotEquals(pairServiceCookieStore.getLeft(), pairServiceCookieStoreSecond.getLeft());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void buildCacheThrowsCacheException() throws CacheException {
        doThrow(new CacheException()).when(shoppingBasketServiceCmfFactory).buildCache();
        Pair<ShoppingBasketService, CookieStore> pairServiceCookieStore = shoppingBasketServiceCmfFactory.getShoppingBasketService(getCookieStore(SESSION_VALUE));

        shoppingBasketServiceCmfFactory.updateServiceCache(pairServiceCookieStore);
        Pair<ShoppingBasketService, CookieStore> pairServiceCookieStoreSecond = shoppingBasketServiceCmfFactory.getShoppingBasketService(pairServiceCookieStore.getRight());

        assertNotEquals(pairServiceCookieStore.getLeft(), pairServiceCookieStoreSecond.getLeft());
    }

    @Test
    public void buildCacheThrowsCacheExceptionInUpdate() throws CacheException {
        doThrow(new CacheException()).when(shoppingBasketServiceCmfFactory).buildCache();
        shoppingBasketServiceCmfFactory.updateServiceCache(new ImmutablePair<>(shoppingBasketService, new BasicCookieStore()));
    }
}
