package com.odigeo.frontend.services.onefront;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.rest.configuration.URLConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.commons.rest.monitoring.MonitoringConfigurationException;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.exception.ServiceException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class SearchSessionLoaderServiceFactoryTest {

    private static final String SESSION_VALUE = "sessionValue";
    private static final String OTHER_SESSION_VALUE = "otherSessionValue";
    private static final String JSESSIONID = "OF1JSESSIONID";
    private static final String OTHER_COOKIE_NAME = "API1";
    private static final String OTHER_COOKIE_VALUE = "api1";
    private static final String SESSIONLOADERSERVICE_CACHE_NAME = "sessionloaderservice";


    @Mock
    private OFURLConfigurationBuilder ofUrlConfigurationBuilder;
    @Mock
    private com.odigeo.frontend.contract.onefront.SearchSessionLoaderService service;
    @Mock
    private CookieStore cookieStore;
    @Mock
    private com.odigeo.frontend.contract.onefront.SearchSessionLoaderService searchSessionLoaderService;
    @Mock
    private RequestInfo requestInfo;
    @Mock
    private URIBuilder uriBuilder;
    @Mock
    private URLConfiguration ofurlConfiguration;
    @Mock
    private URI uri;
    @InjectMocks
    @Spy
    private SearchSessionLoaderServiceFactory searchSessionLoaderServiceFactory;

    @BeforeMethod
    private void setUp() throws CacheException {

        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init();
        JCS.getInstance(SESSIONLOADERSERVICE_CACHE_NAME).clear();

        doNothing().when(searchSessionLoaderServiceFactory).addMonitoringInterceptor(any());

        when(requestInfo.getRequestUrl()).thenReturn("https://mylocalenvironment.com:8080");
        when(uriBuilder.getHost()).thenReturn("local");

        when(ofUrlConfigurationBuilder.build(com.odigeo.frontend.contract.onefront.SearchSessionLoaderService.class, requestInfo)).thenReturn(ofurlConfiguration);
        when(ofurlConfiguration.getHost()).thenReturn("host");
        when(ofurlConfiguration.getProtocol()).thenReturn("https");
        when(ofurlConfiguration.getUri()).thenReturn(uri);

    }

    private CookieStore getCookieStore(String value) {
        CookieStore cookieStore = new BasicCookieStore();

        cookieStore.addCookie(new BasicClientCookie(JSESSIONID, value));
        cookieStore.addCookie(new BasicClientCookie(OTHER_COOKIE_NAME, OTHER_COOKIE_VALUE));

        return cookieStore;
    }

    private CookieStore getCookieStoreWithPath(String value) {
        CookieStore cookieStore = new BasicCookieStore();

        BasicClientCookie jsessionidCookie = new BasicClientCookie(JSESSIONID, value);
        jsessionidCookie.setPath("path");
        cookieStore.addCookie(jsessionidCookie);
        cookieStore.addCookie(new BasicClientCookie(OTHER_COOKIE_NAME, OTHER_COOKIE_VALUE));

        return cookieStore;
    }

    @Test
    public void cacheNotInitialized() {
        assertFalse(searchSessionLoaderServiceFactory.isCacheInitialized());
    }

    @Test
    public void getSearchSessionLoaderServiceOKWithoutCookie() {
        assertNotNull(searchSessionLoaderServiceFactory.getSearchSessionLoaderService(new BasicCookieStore(), requestInfo));

        verify(searchSessionLoaderServiceFactory).addMonitoringInterceptor(any());
    }

    @Test
    public void getSearchSessionLoaderServiceOKWithCookie() {
        assertNotNull(searchSessionLoaderServiceFactory.getSearchSessionLoaderService(getCookieStore(SESSION_VALUE), requestInfo));

        verify(searchSessionLoaderServiceFactory).addMonitoringInterceptor(any());
    }

    @Test
    public void getSearchSessionLoaderServiceTestingCache() throws IllegalAccessException, IOException, InvocationTargetException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException, KeyManagementException {
        CookieStore cookieStore = getCookieStore(SESSION_VALUE);
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pairServiceCookie = searchSessionLoaderServiceFactory.getSearchSessionLoaderService(cookieStore, requestInfo);
        searchSessionLoaderServiceFactory.updateServiceCache(pairServiceCookie);
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pairServiceCookieSecond = searchSessionLoaderServiceFactory.getSearchSessionLoaderService(cookieStore, requestInfo);

        verify(searchSessionLoaderServiceFactory).createSearchSessionLoaderService(any(), any(), any());

        assertEquals(pairServiceCookie, pairServiceCookieSecond);
    }


    @Test(expectedExceptions = ServiceException.class)
    public void getSearchSessionLoaderServiceIllegalAccessExceptionShouldThrowServiceException() throws IllegalAccessException, IOException, InvocationTargetException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException, KeyManagementException {
        doThrow(new IllegalAccessException())
                .when(searchSessionLoaderServiceFactory)
                .createSearchSessionLoaderService(any(ServiceConfiguration.class), any(CookieStore.class), any(RequestInfo.class));

        searchSessionLoaderServiceFactory.getSearchSessionLoaderService(getCookieStore(SESSION_VALUE), requestInfo);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getSearchSessionLoaderServiceIOExceptionShouldThrowServiceException() throws IllegalAccessException, IOException, InvocationTargetException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException, KeyManagementException {
        doThrow(new IOException())
                .when(searchSessionLoaderServiceFactory)
                .createSearchSessionLoaderService(any(ServiceConfiguration.class), any(CookieStore.class), any(RequestInfo.class));

        searchSessionLoaderServiceFactory.getSearchSessionLoaderService(getCookieStore(SESSION_VALUE), requestInfo);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getSearchSessionLoaderServiceInvocationTargetExceptionShouldThrowServiceException() throws IllegalAccessException, IOException, InvocationTargetException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException, KeyManagementException {
        doThrow(new InvocationTargetException(new Exception()))
                .when(searchSessionLoaderServiceFactory)
                .createSearchSessionLoaderService(any(ServiceConfiguration.class), any(CookieStore.class), any(RequestInfo.class));

        searchSessionLoaderServiceFactory.getSearchSessionLoaderService(getCookieStore(SESSION_VALUE), requestInfo);
    }

    @Test
    public void getSearchSessionLoaderServiceFactoryTwoCallsSameCookieAfterAnUpdate() {
        CookieStore cookieStoreWithPath = getCookieStoreWithPath(SESSION_VALUE);
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pair = searchSessionLoaderServiceFactory.getSearchSessionLoaderService(cookieStoreWithPath, requestInfo);
        searchSessionLoaderServiceFactory.updateServiceCache(pair);

        assertEquals(pair.hashCode(), searchSessionLoaderServiceFactory.getSearchSessionLoaderService(cookieStoreWithPath, requestInfo).hashCode());
        verify(searchSessionLoaderServiceFactory).addMonitoringInterceptor(any());
    }

    @Test
    public void updateServiceCacheWithNoCacheInitializedShouldNotBreakTheExecution() {
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pair = new ImmutablePair<>(service, cookieStore);

        searchSessionLoaderServiceFactory.updateServiceCache(pair);

        assertTrue(searchSessionLoaderServiceFactory.isCacheInitialized());
    }

    @Test
    public void getSearchSessionLoaderServiceFactoryTwoCallsDifferentCookie() {
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pairServiceCookie = searchSessionLoaderServiceFactory.getSearchSessionLoaderService(getCookieStore(SESSION_VALUE), requestInfo);
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pairServiceCookieSecond = searchSessionLoaderServiceFactory.getSearchSessionLoaderService(getCookieStore(OTHER_SESSION_VALUE), requestInfo);

        assertNotEquals(pairServiceCookie.getRight(), pairServiceCookieSecond.getRight());

        verify(searchSessionLoaderServiceFactory, times(2)).addMonitoringInterceptor(any());
    }

    @Test(expectedExceptions = MonitoringConfigurationException.class)
    public void getSearchSessionLoaderServiceThrowMonitoringConfigurationException() {
        doCallRealMethod().when(searchSessionLoaderServiceFactory).addMonitoringInterceptor(any());
        searchSessionLoaderServiceFactory.getSearchSessionLoaderService(getCookieStore(SESSION_VALUE), requestInfo);
    }

    @Test
    public void buildCacheDoesNotCreateACache() throws CacheException {
        doNothing().when(searchSessionLoaderServiceFactory).buildCache();
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pairServiceCookieStore = searchSessionLoaderServiceFactory.getSearchSessionLoaderService(getCookieStore(SESSION_VALUE), requestInfo);
        searchSessionLoaderServiceFactory.updateServiceCache(pairServiceCookieStore);
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pairServiceCookieStoreSecond = searchSessionLoaderServiceFactory.getSearchSessionLoaderService(pairServiceCookieStore.getRight(), requestInfo);

        assertNotEquals(pairServiceCookieStore.getLeft(), pairServiceCookieStoreSecond.getLeft());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void buildCacheThrowsCacheException() throws CacheException {
        doThrow(new CacheException()).when(searchSessionLoaderServiceFactory).buildCache();
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pairServiceCookieStore = searchSessionLoaderServiceFactory.getSearchSessionLoaderService(getCookieStore(SESSION_VALUE), requestInfo);

        searchSessionLoaderServiceFactory.updateServiceCache(pairServiceCookieStore);
        Pair<com.odigeo.frontend.contract.onefront.SearchSessionLoaderService, CookieStore> pairServiceCookieStoreSecond = searchSessionLoaderServiceFactory.getSearchSessionLoaderService(pairServiceCookieStore.getRight(), requestInfo);

        assertNotEquals(pairServiceCookieStore.getLeft(), pairServiceCookieStoreSecond.getLeft());
    }

    @Test
    public void buildCacheThrowsCacheExceptionInUpdate() throws CacheException {
        doThrow(new CacheException()).when(searchSessionLoaderServiceFactory).buildCache();
        searchSessionLoaderServiceFactory.updateServiceCache(new ImmutablePair<>(searchSessionLoaderService, new BasicCookieStore()));
    }
}
