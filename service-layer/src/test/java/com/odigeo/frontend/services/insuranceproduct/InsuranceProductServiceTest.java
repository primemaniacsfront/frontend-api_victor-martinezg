package com.odigeo.frontend.services.insuranceproduct;

import com.edreamsodigeo.insuranceproduct.api.last.InsuranceProductServiceException;
import com.edreamsodigeo.insuranceproduct.api.last.request.SelectRequest;
import com.edreamsodigeo.insuranceproduct.api.last.response.InsuranceProduct;
import com.edreamsodigeo.insuranceproduct.api.last.response.ProductId;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.product.v2.exception.ProductException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class InsuranceProductServiceTest {

    private static final String ERROR_MSG = "ERROR";
    private static final String PRODUCT_ID = "productId";

    @Mock
    private com.edreamsodigeo.insuranceproduct.api.last.InsuranceProductService insuranceProduct;

    @InjectMocks
    private InsuranceProductService insuranceProductService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void selectOffer() throws InsuranceProductServiceException {
        ProductId productId = new ProductId();
        when(insuranceProduct.select(any())).thenReturn(productId);
        assertSame(insuranceProductService.selectOffer(new SelectRequest()), productId);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void selectOfferInsuranceProductServiceException() throws InsuranceProductServiceException {
        when(insuranceProduct.select(any())).thenThrow(new InsuranceProductServiceException(ERROR_MSG));
        insuranceProductService.selectOffer(new SelectRequest());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void selectOfferInsuranceRuntimeException() throws InsuranceProductServiceException {
        when(insuranceProduct.select(any())).thenThrow(new RuntimeException(ERROR_MSG));
        insuranceProductService.selectOffer(new SelectRequest());
    }

    @Test
    public void getInsuranceProduct() throws ProductException {
        InsuranceProduct insuranceProductObj = new InsuranceProduct();
        when(insuranceProduct.getProduct(anyString())).thenReturn(insuranceProductObj);
        assertSame(insuranceProductService.getInsuranceProduct(PRODUCT_ID), insuranceProductObj);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getInsuranceProductProductException() throws ProductException {
        when(insuranceProduct.getProduct(anyString())).thenThrow(new ProductException(ERROR_MSG));
        insuranceProductService.getInsuranceProduct(PRODUCT_ID);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getInsuranceProductRuntimeException() throws ProductException {
        when(insuranceProduct.getProduct(anyString())).thenThrow(new RuntimeException(ERROR_MSG));
        insuranceProductService.getInsuranceProduct(PRODUCT_ID);
    }
}
