package com.odigeo.frontend.services;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.ItineraryFootprintCalculatorService;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.exception.ItineraryFootprintCalculatorApiException;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.exception.RequestValidationException;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.request.ItineraryFootprintCalculatorRequest;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.response.ItineraryFootprintCalculatorResponse;
import com.odigeo.frontend.commons.Logger;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.UndeclaredThrowableException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class CarbonFootprintServiceTest {

    @Mock
    private ItineraryFootprintCalculatorService carbonFootprintService;
    @Mock
    private Logger logger;

    @Mock
    private ItineraryFootprintCalculatorRequest footprintRequest;

    @InjectMocks
    private CarbonFootprintService service;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetRatings() throws Exception {
        ItineraryFootprintCalculatorResponse footprintResponse = mock(ItineraryFootprintCalculatorResponse.class);
        when(carbonFootprintService.getFootprint(footprintRequest)).thenReturn(footprintResponse);
        assertSame(carbonFootprintService.getFootprint(footprintRequest), footprintResponse);
    }

    @Test
    public void testGetFootprintItineraryFootprintCalculatorApiException() throws Exception {
        testException(ItineraryFootprintCalculatorApiException.class);
    }

    @Test
    public void testGetFootprintRequestValidationException() throws Exception {
        testException(RequestValidationException.class);
    }

    @Test
    public void testGetFootprintConnectException() throws Exception {
        testException(ConnectException.class);
    }

    @Test
    public void testGetFootprintSocketTimeoutException() throws Exception {
        testException(SocketTimeoutException.class);
    }

    @Test
    public void testGetFootprintUndeclaredException() throws Exception {
        testException(UndeclaredThrowableException.class);
    }

    @Test
    public void testGetFootprintException() throws Exception {
        testException(RuntimeException.class);
    }

    private void testException(Class<? extends Exception> ex) throws Exception {
        when(carbonFootprintService.getFootprint(eq(footprintRequest))).thenThrow(ex);
        assertNull(service.getCarbonFootprint(footprintRequest));
    }

}
