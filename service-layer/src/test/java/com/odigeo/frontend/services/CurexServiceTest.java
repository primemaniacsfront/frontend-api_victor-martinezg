package com.odigeo.frontend.services;

import com.odigeo.curex.client.currencyconverter.CurrencyConverterFactory;
import com.odigeo.curex.client.currencyconverter.DefaultCurrencyConverter;
import com.odigeo.curex.client.currencyconverter.exception.CurrencyConverterFactoryException;
import com.odigeo.frontend.commons.Logger;
import java.math.BigDecimal;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class CurexServiceTest {

    @Mock
    private CurrencyConverterFactory converterFactory;
    @Mock
    private DefaultCurrencyConverter defaultConverter;
    @Mock
    private Logger logger;

    @InjectMocks
    private CurexService curexService;

    @BeforeMethod
    public void init() throws Exception {
        MockitoAnnotations.openMocks(this);
        when(converterFactory.getCurrentCurrencyConverter()).thenReturn(defaultConverter);
    }

    @Test
    public void testGetCurrencyConverterRate() {
        BigDecimal rate = mock(BigDecimal.class);
        String sourceCurrency = "source";
        String destinationCurrency = "destination";

        when(defaultConverter.getConversionRate(sourceCurrency, destinationCurrency)).thenReturn(rate);
        assertSame(curexService.getCurrencyConvertRate(sourceCurrency, destinationCurrency), rate);
    }

    @Test
    public void testGetCurrencyConverterRateThrowsException() throws Exception {
        when(converterFactory.getCurrentCurrencyConverter()).thenThrow(CurrencyConverterFactoryException.class);
        assertNull(curexService.getCurrencyConvertRate("source", "destination"));
    }

    @Test
    public void testGetCurrencyConverterRateThrowsRuntimeException() {
        when(defaultConverter.getConversionRate(anyString(), anyString())).thenThrow(RuntimeException.class);
        assertNull(curexService.getCurrencyConvertRate("source", "destination"));
    }

}
