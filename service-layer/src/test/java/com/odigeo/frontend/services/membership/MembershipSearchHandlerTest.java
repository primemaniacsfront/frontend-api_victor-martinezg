package com.odigeo.frontend.services.membership;

import com.google.inject.Guice;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.prime.handlers.MembershipSearchRequestHandler;
import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MembershipResponse;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.mockito.Mockito.verify;

public class MembershipSearchHandlerTest {
    private static final long USER_ID = 123L;
    private static final String WEBSITE_CODE = "ES";
    private static final String EMAIL = "buyer@edreams.com";
    private List<MembershipResponse> membershipResponseList;

    @Mock
    private MembershipSearchApi membershipSearchApi;
    @Mock
    private MembershipResponse firstMembershipResponse;
    @Mock
    private MembershipResponse lastMembershipResponse;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private MembershipSearchRequestHandler membershipSearchRequestHandler;
    @Mock
    private MembershipSearchRequest membershipSearchRequest;
    @InjectMocks
    private MembershipSearchHandler membershipSearchHandler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(MembershipSearchApi.class).toProvider(() -> membershipSearchApi);
        });
        membershipResponseList = Arrays.asList(firstMembershipResponse, lastMembershipResponse);
    }

    @Test
    public void testGetMembershipByUserId() {
        when(membershipSearchApi.searchMemberships(membershipSearchRequest)).thenReturn(membershipResponseList);
        when(membershipSearchRequestHandler.build(USER_ID, WEBSITE_CODE)).thenReturn(membershipSearchRequest);
        assertEquals(membershipSearchHandler.getCurrentMembership(USER_ID, WEBSITE_CODE), firstMembershipResponse);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_GET_CURRENT_MEMBERSHIP);
    }

    @Test
    public void testGetMembershipByEmail() {
        when(membershipSearchApi.searchMemberships(membershipSearchRequest)).thenReturn(membershipResponseList);
        when(membershipSearchRequestHandler.build(EMAIL, WEBSITE_CODE)).thenReturn(membershipSearchRequest);
        assertEquals(membershipSearchHandler.getCurrentMembership(EMAIL, WEBSITE_CODE), firstMembershipResponse);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_GET_CURRENT_MEMBERSHIP);
    }

    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = "Failed to searchMembership")
    public void testExceptionInSearchMembership() {
        MembershipServiceException membershipServiceException = new MembershipServiceException(Response.Status.UNAUTHORIZED, "", new Exception());
        when(membershipSearchApi.searchMemberships(membershipSearchRequest)).thenThrow(membershipServiceException);
        when(membershipSearchRequestHandler.build(EMAIL, WEBSITE_CODE)).thenReturn(membershipSearchRequest);
        membershipSearchHandler.getCurrentMembership(EMAIL, WEBSITE_CODE);
    }
}
