package com.odigeo.frontend.services.configurations.dapi;

import bean.test.BeanTest;
import org.apache.commons.lang3.StringUtils;

public class DapiConnectionConfigTest extends BeanTest<DapiConnectionConfig> {
    @Override
    protected DapiConnectionConfig getBean() {
        DapiConnectionConfig bean = new DapiConnectionConfig();
        bean.setDapiHost(StringUtils.EMPTY);
        bean.setUseSecureForBooking(Boolean.FALSE);
        return bean;
    }
}
