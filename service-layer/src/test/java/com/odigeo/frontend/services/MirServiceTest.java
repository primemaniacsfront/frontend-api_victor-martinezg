package com.odigeo.frontend.services;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.MeItineraryRatingService;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.exception.MeItineraryRatingApiException;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.exception.RequestValidationException;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.request.ItineraryRatingRequest;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.response.ItineraryRatingResponse;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class MirServiceTest {

    @Mock
    private MeItineraryRatingService ratingService;
    @Mock
    private Logger logger;

    @Mock
    private ItineraryRatingRequest ratingRequest;

    @InjectMocks
    private MirService mirService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetRatings() throws Exception {
        ItineraryRatingResponse ratingResponse = mock(ItineraryRatingResponse.class);
        when(ratingService.getRatings(ratingRequest)).thenReturn(ratingResponse);
        assertSame(ratingService.getRatings(ratingRequest), ratingResponse);
    }

    @Test
    public void testGetRatingsMeItineraryRatingApiException() throws Exception {
        testException(MeItineraryRatingApiException.class);
    }

    @Test
    public void testGetRatingsRequestValidationException() throws Exception {
        testException(RequestValidationException.class);
    }

    @Test
    public void testGetRatingsConnectException() throws Exception {
        testException(ConnectException.class);
    }

    @Test
    public void testGetRatingsSocketTimeoutException() throws Exception {
        testException(SocketTimeoutException.class);
    }

    @Test
    public void testGetRatingsUndeclaredException() throws Exception {
        testException(UndeclaredThrowableException.class);
    }

    @Test
    public void testGetRatingsException() throws Exception {
        testException(RuntimeException.class);
    }

    private void testException(Class<? extends Exception> ex) throws Exception {
        when(ratingService.getRatings(eq(ratingRequest))).thenThrow(ex);
        assertNull(mirService.getRatings(ratingRequest));
    }

}
