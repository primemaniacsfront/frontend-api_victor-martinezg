package com.odigeo.frontend.services;

import com.odigeo.visitengineapi.v1.VisitEngineException;
import com.odigeo.visitengineapi.v1.client.autonomous.VisitEngineClient;
import com.odigeo.visitengineapi.v1.request.InvalidParametersException;
import com.odigeo.visitengineapi.v1.response.VisitResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class VisitEngineServiceTest {

    @Mock
    private VisitEngineClient visitEngineClient;

    @InjectMocks
    private VisitEngineService visitEngineService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testDeseralize() throws Exception {
        VisitResponse visitResponse = mock(VisitResponse.class);
        String visit = "visit";

        when(visitEngineClient.buildVisit(visit)).thenReturn(visitResponse);
        assertSame(visitEngineService.deserialize(visit), visitResponse);
    }

    @Test
    public void testDeserializeVisitEngineException() throws Exception {
        buildVisitErrorTest(VisitEngineException.class);
    }

    @Test
    public void testDeserializeParamException() throws Exception {
        buildVisitErrorTest(InvalidParametersException.class);
    }

    @Test
    public void testDeserializeRuntimeException() throws Exception {
        buildVisitErrorTest(RuntimeException.class);
    }

    private void buildVisitErrorTest(Class<? extends Exception> ex) throws Exception {
        when(visitEngineClient.buildVisit("visit")).thenThrow(ex);
        assertNull(visitEngineService.deserialize("visit"));
    }

}
