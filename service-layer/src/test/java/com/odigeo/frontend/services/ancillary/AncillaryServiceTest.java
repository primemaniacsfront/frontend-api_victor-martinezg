package com.odigeo.frontend.services.ancillary;

import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.itineraryapi.v1.InvalidParametersException;
import com.odigeo.itineraryapi.v1.ItineraryApiService;
import com.odigeo.itineraryapi.v1.request.AncillariesServicesType;
import com.odigeo.itineraryapi.v1.request.SelectAncillaryRequest;
import com.odigeo.itineraryapi.v1.response.AvailableAncillariesOptionsResponse;
import com.odigeo.itineraryapi.v1.response.Response;
import org.apache.commons.lang3.StringUtils;
import com.odigeo.itineraryapi.v1.response.SelectAncillaryServiceResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashSet;

import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class AncillaryServiceTest {

    private static final String ERROR_MSG = "ERROR";
    private static final long BOOKING_ID = 1L;
    private static final HashSet<AncillariesServicesType> SERVICES = new HashSet<>();

    @Mock
    private ResolverContext context;

    @Mock
    private VisitInformation visitInformation;

    @Mock
    private DebugInfo debugInfo;

    @Mock
    private ItineraryApiService itineraryApiService;

    @Mock
    private AvailableAncillariesOptionsResponse ancillariesOptionsResponse;

    @Mock
    private Response<AvailableAncillariesOptionsResponse> itineraryResponse;

    @Mock
    private SelectAncillaryRequest selectAncillaryRequest;

    @Mock
    private SelectAncillaryServiceResponse selectAncillaryServiceResponse;

    @Mock
    private Response<SelectAncillaryServiceResponse> itinerarySelectResponse;

    @InjectMocks
    private AncillaryService ancillaryService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void getAncillariesOptions() {
        itineraryResponse.setResponse(ancillariesOptionsResponse);
        when(context.getVisitInformation()).thenReturn(visitInformation);
        when(visitInformation.getVisitId()).thenReturn(BOOKING_ID);
        when(visitInformation.getVisitCode()).thenReturn(StringUtils.EMPTY);
        when(context.getDebugInfo()).thenReturn(debugInfo);
        when(debugInfo.isQaModeEnabled()).thenReturn(true);
        when(itineraryApiService.getAncillaryServiceOptions(any(), any())).thenReturn(itineraryResponse);
        assertSame(ancillaryService.ancillariesOptions(BOOKING_ID, SERVICES, context), itineraryResponse.getResponse());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getAncillariesOptionsWithInvalidParameters() throws ServiceException {
        itineraryResponse.setResponse(ancillariesOptionsResponse);
        when(context.getVisitInformation()).thenReturn(visitInformation);
        when(visitInformation.getVisitId()).thenReturn(BOOKING_ID);
        when(visitInformation.getVisitCode()).thenReturn(StringUtils.EMPTY);
        when(context.getDebugInfo()).thenReturn(debugInfo);
        when(debugInfo.isQaModeEnabled()).thenReturn(true);
        when(itineraryApiService.getAncillaryServiceOptions(any(), any()))
            .thenThrow(new InvalidParametersException(ERROR_MSG));
        ancillaryService.ancillariesOptions(BOOKING_ID, null, context);
    }

    @Test
    public void selectAncillaries() {
        itinerarySelectResponse.setResponse(selectAncillaryServiceResponse);
        when(itineraryApiService.selectAncillaryService(BOOKING_ID, selectAncillaryRequest)).thenReturn(itinerarySelectResponse);
        SelectAncillaryServiceResponse outputResponse = ancillaryService.selectAncillaries(BOOKING_ID, selectAncillaryRequest);
        assertSame(outputResponse, itinerarySelectResponse.getResponse());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void selectAncillariesWithInvalidParameters() {
        itinerarySelectResponse.setResponse(selectAncillaryServiceResponse);
        when(itineraryApiService.selectAncillaryService(BOOKING_ID, selectAncillaryRequest))
                .thenThrow(new InvalidParametersException(ERROR_MSG));
        SelectAncillaryServiceResponse outputResponse = ancillaryService.selectAncillaries(BOOKING_ID, selectAncillaryRequest);
    }
}
