package com.odigeo.frontend.services.managemybooking;

import com.edreamsodigeo.bookingimportantmessage.contract.v1.BookingImportantMessageResource;
import com.edreamsodigeo.bookingimportantmessage.contract.v1.exception.BookingImportantMessageResourceException;
import com.edreamsodigeo.bookingimportantmessage.contract.v1.exception.BookingImportantMessageValidationException;
import com.edreamsodigeo.bookingimportantmessage.contract.v1.model.cancellation.CancellationMessage;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BookingImportantMessageServiceTest {

    private static final long BOOKING_ID = 123L;
    private static final String EMAIL = "test@edreamsodigeo.com";
    private static final String TOKEN = "test_token";
    private static final String EXC_MESSAGE = "exception_message";

    @Mock
    private BookingImportantMessageResource bookingImportantMessageResource;

    @InjectMocks
    private BookingImportantMessageService bookingImportantMessageService;

    @BeforeMethod
    private void setUpTest() {
        openMocks(this);
    }

    @Test
    public void getImportantMessagesByBookingIdAndEmailOK() throws BookingImportantMessageResourceException, BookingImportantMessageValidationException {
        when(bookingImportantMessageResource.getImportantMessagesByBookingIdAndEmail(anyLong(), any())).thenReturn(Collections.singletonList(new CancellationMessage()));
        assertEquals(bookingImportantMessageService.getImportantMessagesByBookingIdAndEmail(BOOKING_ID, EMAIL).size(), 1);
    }

    @Test
    public void getImportantMessagesByBookingIdAndEmailKO() throws BookingImportantMessageResourceException, BookingImportantMessageValidationException {
        when(bookingImportantMessageResource.getImportantMessagesByBookingIdAndEmail(anyLong(), any())).thenThrow(new BookingImportantMessageResourceException(EXC_MESSAGE));
        assertTrue(bookingImportantMessageService.getImportantMessagesByBookingIdAndEmail(BOOKING_ID, EMAIL).isEmpty());
    }

    @Test
    public void getImportantMessagesByTokenOK() throws BookingImportantMessageResourceException, BookingImportantMessageValidationException {
        when(bookingImportantMessageResource.getImportantMessagesByToken(any())).thenReturn(Collections.singletonList(new CancellationMessage()));
        assertEquals(bookingImportantMessageService.getImportantMessagesByToken(TOKEN).size(), 1);
    }

    @Test
    public void getImportantMessagesByTokenKO() throws BookingImportantMessageResourceException, BookingImportantMessageValidationException {
        when(bookingImportantMessageResource.getImportantMessagesByToken(any())).thenThrow(new BookingImportantMessageResourceException(EXC_MESSAGE));
        assertTrue(bookingImportantMessageService.getImportantMessagesByToken(TOKEN).isEmpty());
    }

}
