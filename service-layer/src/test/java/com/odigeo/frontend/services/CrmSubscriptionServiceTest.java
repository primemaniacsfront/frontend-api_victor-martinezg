package com.odigeo.frontend.services;

import com.odigeo.marketing.subscriptions.rest.v1.parameters.FdoSubscription;
import com.odigeo.marketing.subscriptions.rest.v1.parameters.PriceAlertSubscription;
import com.odigeo.marketing.subscriptions.rest.v1.services.SubscriptionApiService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CrmSubscriptionServiceTest {

    @Mock
    private SubscriptionApiService subscriptionService;

    @InjectMocks
    private CrmSubscriptionService service;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testSubscribeToPriceAlert() {
        PriceAlertSubscription subscription = mock(PriceAlertSubscription.class);
        service.subscribeToPriceAlert(subscription);

        verify(subscriptionService).subscribeToPriceAlert(subscription);
    }

    @Test
    public void testSubscribeToFdo() {
        FdoSubscription subscription = mock(FdoSubscription.class);
        service.subscribeToFdo(subscription);

        verify(subscriptionService).subscribeToFdo(subscription);
    }

}
