package com.odigeo.frontend.services.configurations.dapi;

import org.testng.annotations.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class CollectionBasedHttpRequestCookiesTest {

    @Test
    public void testGetCookies() {
        CollectionBasedHttpRequestCookies requestCookies = new CollectionBasedHttpRequestCookies(Collections.emptyList());
        assertEquals(requestCookies.getCookies().size(), 0);
    }
}
