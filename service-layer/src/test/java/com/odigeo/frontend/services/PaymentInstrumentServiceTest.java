package com.odigeo.frontend.services;

import com.edreamsodigeo.payments.paymentinstrument.contract.PaymentInstrumentResourceException;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScope;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScopeResource;
import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentToken;
import com.odigeo.frontend.commons.exception.ServiceException;
import org.apache.commons.lang.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class PaymentInstrumentServiceTest {

    private static final String TOKEN = "CREDIT_CARD-679389cb-6c73-4c78-8035-5dd839dd710c";

    @Mock
    private CreditCardPciScopeResource creditCardPciScopeResource;
    @Mock
    private CreditCardPciScope creditCardPciScope;
    @Mock
    private PaymentInstrumentToken paymentInstrumentToken;
    @InjectMocks
    private PaymentInstrumentService service;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetCreditCardFromPaymentInstrumentToken() throws PaymentInstrumentResourceException {
        when(creditCardPciScopeResource.getPciScope(any())).thenReturn(creditCardPciScope);
        CreditCardPciScope returned = service.getCreditCardFromPaymentInstrumentToken(TOKEN);
        assertNotNull(returned);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetCreditCardFromPaymentInstrumentTokenWithException() throws PaymentInstrumentResourceException {
        when(creditCardPciScopeResource.getPciScope(any())).thenThrow(PaymentInstrumentResourceException.class);
        service.getCreditCardFromPaymentInstrumentToken(TOKEN);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void tokenizeException() throws PaymentInstrumentResourceException {
        when(creditCardPciScopeResource.tokenize(any())).thenThrow(PaymentInstrumentResourceException.class);
        service.tokenize(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
    }

    @Test
    public void tokenizeCard() throws PaymentInstrumentResourceException {
        when(creditCardPciScopeResource.tokenize(any())).thenReturn(paymentInstrumentToken);
        assertNotNull(service.tokenize(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY));
    }

}
