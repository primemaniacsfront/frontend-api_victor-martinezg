package com.odigeo.frontend.services;

import com.edreamsodigeo.seo.sslppopularitystats.exceptions.InvalidParametersException;
import com.edreamsodigeo.seo.sslppopularitystats.exceptions.PopularityStatsException;
import com.edreamsodigeo.seo.sslppopularitystats.popularity.v1.BookingPopularityService;
import com.edreamsodigeo.seo.sslppopularitystats.popularity.v1.model.responses.CityBookingAggregation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.exception.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class PopularityStatsServiceTest {

    private static final String WEBSITE_CODE = "ES";
    private static final String AGGREGATE_BY = "AGGREGATE_BY";
    private static final Integer SIZE = 50;
    private static final String ERROR = "ERROR";

    @Mock
    private List<CityBookingAggregation> result;
    @Mock
    private BookingPopularityService bookingPopularityService;

    @InjectMocks
    private PopularityStatsService service;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetPopularDestinationCities() throws PopularityStatsException, InvalidParametersException {
        when(bookingPopularityService.citiesAggregateByWebsite(eq(WEBSITE_CODE), eq(AGGREGATE_BY), eq(SIZE))).thenReturn(result);
        assertEquals(service.getPopularDestinationCities(Site.ES, AGGREGATE_BY, SIZE), result);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetPopularDestinationCitiesInvalidParametersException() throws PopularityStatsException, InvalidParametersException {
        when(bookingPopularityService.citiesAggregateByWebsite(eq(WEBSITE_CODE), eq(AGGREGATE_BY), eq(SIZE))).thenThrow(new InvalidParametersException(ERROR));
        service.getPopularDestinationCities(Site.ES, AGGREGATE_BY, SIZE);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetPopularDestinationCitiesPopularityStatsException() throws PopularityStatsException, InvalidParametersException {
        when(bookingPopularityService.citiesAggregateByWebsite(eq(WEBSITE_CODE), eq(AGGREGATE_BY), eq(SIZE))).thenThrow(new PopularityStatsException(ERROR));
        service.getPopularDestinationCities(Site.ES, AGGREGATE_BY, SIZE);
    }

}
