package com.odigeo.frontend.services.configurations.dapi;

import org.testng.annotations.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class CollectionBasedHttpResponseCookiesTest {

    @Test
    public void testGetCookies() {
        CollectionBasedHttpResponseCookies rspCookies = new CollectionBasedHttpResponseCookies(Collections.emptyList());
        assertEquals(rspCookies.cookies.size(), 0);
    }
}
