package com.odigeo.frontend.services;

import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.marketing.search.price.v1.requests.SearchPricesRequest;
import com.odigeo.marketing.search.price.v1.responses.SearchPriceResponse;
import com.odigeo.marketing.search.price.v1.services.SearchPriceApiService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.UndeclaredThrowableException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SearchPriceServiceTest {

    private static final String ERROR = "ERROR";

    @Mock
    private SearchPricesRequest request;
    @Mock
    private SearchPriceResponse result;
    @Mock
    private SearchPriceApiService searchPriceApiService;

    @InjectMocks
    private SearchPriceService service;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testSearchRoutePrices() {
        when(searchPriceApiService.searchRoutePrices(eq(request))).thenReturn(result);
        assertEquals(service.searchRoutePrices(request), result);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testSearchRoutePricesUndeclaredThrowableException() {
        when(searchPriceApiService.searchRoutePrices(eq(request))).thenThrow(new UndeclaredThrowableException(new Throwable(), ERROR));
        service.searchRoutePrices(request);
    }

}
