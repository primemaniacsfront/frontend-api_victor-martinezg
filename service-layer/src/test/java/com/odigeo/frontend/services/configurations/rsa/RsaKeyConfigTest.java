package com.odigeo.frontend.services.configurations.rsa;

import com.edreams.configuration.ConfigurationEngine;
import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.testng.Assert.assertNotNull;

public class RsaKeyConfigTest {

    private RsaKeyConfig rsaKeyConfig;

    @BeforeTest
    private void setUp() {
        ConfigurationEngine.init();
        rsaKeyConfig = ConfigurationEngine.getInstance(RsaKeyConfig.class);
    }

    @Test
    public void setters() {
        rsaKeyConfig.setExponentD(StringUtils.EMPTY);
        rsaKeyConfig.setExponentE(StringUtils.EMPTY);
        rsaKeyConfig.setModulusD(StringUtils.EMPTY);
        rsaKeyConfig.setModulusE(StringUtils.EMPTY);
    }


    @Test
    public void privateKey() throws InvalidKeySpecException, NoSuchAlgorithmException {
        assertNotNull(rsaKeyConfig.getPrivateKey());
    }

    @Test
    public void publicKey() throws InvalidKeySpecException, NoSuchAlgorithmException {
        assertNotNull(rsaKeyConfig.getPublicKey());
    }
}
