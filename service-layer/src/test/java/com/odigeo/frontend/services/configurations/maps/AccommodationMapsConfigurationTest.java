package com.odigeo.frontend.services.configurations.maps;

import bean.test.BeanTest;

public class AccommodationMapsConfigurationTest extends BeanTest<AccommodationMapsConfiguration> {

    @Override
    protected AccommodationMapsConfiguration getBean() {
        AccommodationMapsConfiguration bean = new AccommodationMapsConfiguration();
        bean.setApiKey("apiKey");
        bean.setSecretKey("secretKey");
        bean.setClientId("clientId");
        return bean;
    }
}
