package com.odigeo.frontend.services.shoppingbasket;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResponseInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ShoppingBasketBrowserStorageHelperTest {

    private static final String JSESSIONID = "JSESSIONID";
    private static final String VALUE = "VALUE";
    private static final String OTHER_COOKIE = "OTHER_COOKIE";

    @Mock
    ResolverContext context;
    @Mock
    CookieStore cookieStore;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void setSbCookieWithJsession() {
        ResponseInfo responseInfo = new ResponseInfo();
        when(context.getResponseInfo()).thenReturn(responseInfo);

        ShoppingBasketBrowserStorageHelper.setSbCookie(context, VALUE);

        assertEquals(responseInfo.getCookies().size(), 1);
    }

    @Test
    public void setSbCookieWithoutResponseInfoDoesNotFail() {
        when(context.getResponseInfo()).thenReturn(null);

        ShoppingBasketBrowserStorageHelper.setSbCookie(context, VALUE);
    }

    @Test
    public void setSbCookieWithoutContextDoesNotFail() {
        ShoppingBasketBrowserStorageHelper.setSbCookie(null, VALUE);
    }

    @Test
    public void putSbHeaderWithJsession() {
        ResponseInfo responseInfo = new ResponseInfo();
        when(context.getResponseInfo()).thenReturn(responseInfo);
        responseInfo.getHeaders().put(SiteHeaders.SB_JSESSIONID, VALUE);

        ShoppingBasketBrowserStorageHelper.putSbHeader(context, VALUE);

        assertEquals(responseInfo.getHeaders().size(), 1);
    }

    @Test
    public void putSbHeaderWithoutResponseInfoDoesNotFail() {
        when(context.getResponseInfo()).thenReturn(null);

        ShoppingBasketBrowserStorageHelper.putSbHeader(context, VALUE);
    }

    @Test
    public void putSbHeaderWithoutContextDoesNotFail() {
        ShoppingBasketBrowserStorageHelper.putSbHeader(null, VALUE);
    }

    @Test
    public void createShoppingBasketCookieStore() {
        RequestInfo requestInfo = Mockito.mock(RequestInfo.class);
        when(requestInfo.getHeaderOrCookie(SiteHeaders.SB_JSESSIONID, SiteCookies.SB_JSESSIONID))
            .thenReturn(VALUE);
        when(context.getRequestInfo()).thenReturn(requestInfo);

        CookieStore cookieStore = ShoppingBasketBrowserStorageHelper.createShoppingBasketCookieStore(context);

        assertTrue(cookieStore.getCookies().size() == 1);
        assertEquals(cookieStore.getCookies().get(0).getName(), JSESSIONID);
    }

    @Test
    public void createShoppingBasketCookieStoreNoSbCookie() {
        RequestInfo requestInfo = Mockito.mock(RequestInfo.class);
        when(requestInfo.getHeaderOrCookie(SiteHeaders.SB_JSESSIONID, SiteCookies.SB_JSESSIONID))
            .thenReturn(null);
        when(context.getRequestInfo()).thenReturn(requestInfo);

        CookieStore cookieStore = ShoppingBasketBrowserStorageHelper.createShoppingBasketCookieStore(context);

        assertTrue(cookieStore.getCookies().isEmpty());
    }

    @Test
    public void createShoppingBasketCookieStoreNoCookies() {
        RequestInfo requestInfo = new RequestInfo();
        when(context.getRequestInfo()).thenReturn(requestInfo);

        CookieStore cookieStore = ShoppingBasketBrowserStorageHelper.createShoppingBasketCookieStore(context);

        assertTrue(cookieStore.getCookies().isEmpty());
    }

    @Test
    public void getSbJsessionCookieNoCookieStore() {
        org.apache.http.cookie.Cookie sbJsessionCookie = ShoppingBasketBrowserStorageHelper.getSbJsessionidCookie(null);

        assertTrue(StringUtils.isEmpty(sbJsessionCookie.getValue()));
    }

    @Test
    public void getSbJsessionCookieNoJsession() {
        BasicClientCookie cookie = new BasicClientCookie(OTHER_COOKIE, VALUE);
        List<org.apache.http.cookie.Cookie> cookies = Collections.singletonList(cookie);
        when(cookieStore.getCookies()).thenReturn(cookies);

        org.apache.http.cookie.Cookie sbJsessionCookie = ShoppingBasketBrowserStorageHelper.getSbJsessionidCookie(cookieStore);

        assertTrue(StringUtils.isEmpty(sbJsessionCookie.getValue()));
    }

    @Test
    public void getSbJsessionCookieNoCookies() {
        when(cookieStore.getCookies()).thenReturn(Collections.emptyList());

        org.apache.http.cookie.Cookie sbJsessionCookie = ShoppingBasketBrowserStorageHelper.getSbJsessionidCookie(cookieStore);

        assertTrue(StringUtils.isEmpty(sbJsessionCookie.getValue()));
    }

    @Test
    public void getSbJsessionCookie() {
        when(cookieStore.getCookies()).thenReturn(Collections.singletonList(new BasicClientCookie(JSESSIONID, VALUE)));

        org.apache.http.cookie.Cookie sbJsessionCookie = ShoppingBasketBrowserStorageHelper.getSbJsessionidCookie(cookieStore);

        assertEquals(sbJsessionCookie.getName(), JSESSIONID);
    }
}
