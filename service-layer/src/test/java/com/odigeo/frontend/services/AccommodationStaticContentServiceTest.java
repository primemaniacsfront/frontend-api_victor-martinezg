package com.odigeo.frontend.services;

import com.odigeo.hcsapi.v9.HcsApiService;
import com.odigeo.hcsapi.v9.beans.RoomsDetailRequest;
import com.odigeo.hcsapi.v9.beans.RoomsDetailResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AccommodationStaticContentServiceTest {

    @Mock
    private HcsApiService hcsApiService;

    @InjectMocks
    private AccommodationStaticContentService service;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testExecuteSearch() {
        RoomsDetailRequest roomsDetailRequest = mock(RoomsDetailRequest.class);
        RoomsDetailResponse roomsDetailResponse = mock(RoomsDetailResponse.class);

        when(hcsApiService.getRoomsDetail(roomsDetailRequest)).thenReturn(roomsDetailResponse);
        assertEquals(service.getRoomsDetail(roomsDetailRequest), roomsDetailResponse);
    }

}
