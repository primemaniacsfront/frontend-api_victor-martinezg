package com.odigeo.frontend.services;

import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.itineraryapi.v1.InvalidParametersException;
import com.odigeo.itineraryapi.v1.ItineraryApiService;
import com.odigeo.itineraryapi.v1.response.Response;
import com.odigeo.itineraryapi.v1.response.RetrieveFareFamiliesResponse;
import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class ItineraryServiceTest {

    @Mock
    private ItineraryApiService itineraryApiService;

    @InjectMocks
    private ItineraryService itineraryService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testRetrieveFareFamilies() {
        Response<RetrieveFareFamiliesResponse> response = mock(Response.class);
        RetrieveFareFamiliesResponse fareFamilyResponse = mock(RetrieveFareFamiliesResponse.class);

        long searchId = 1L;
        int fareItineraryId = 2;
        String segmentKeys = "segmentKeys";

        when(response.getResponse()).thenReturn(fareFamilyResponse);
        when(itineraryApiService.retrieveFareFamilies(searchId, fareItineraryId, segmentKeys)).thenReturn(response);
        assertSame(itineraryService.retrieveFareFamilies(searchId, fareItineraryId, segmentKeys), fareFamilyResponse);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testRetrieveFareFamilesThrowsInvalidParameterException() {
        when(itineraryApiService.retrieveFareFamilies(anyLong(), anyInt(), anyString()))
            .thenThrow(InvalidParametersException.class);

        itineraryService.retrieveFareFamilies(0L, 0, StringUtils.EMPTY);
    }

}
