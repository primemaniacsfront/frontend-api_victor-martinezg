package com.odigeo.frontend.services.checkout;

import com.edreamsodigeo.collections.userpaymentinteraction.contract.UserPaymentInteractionResource;
import com.edreamsodigeo.collections.userpaymentinteraction.contract.exceptions.ResourceNotFoundException;
import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteraction;
import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteractionId;
import com.odigeo.dapi.client.UserInteractionNeededResponse;
import com.odigeo.frontend.resolvers.checkout.mappers.UserPaymentInteractionMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class UserPaymentInteractionServiceTest {

    private static final String USER_PAYMENT_INT_ID = "679389cb-6c73-4c78-8035-5dd839dd710c";

    @Mock
    private UserPaymentInteractionResource userPaymentInteractionResource;
    @Mock
    private UserPaymentInteractionMapper userPaymentInteractionMapper;
    @Mock
    private UserPaymentInteraction userPaymentInteraction;
    @Mock
    private UserPaymentInteractionId userPaymentInteractionId;
    @Mock
    private UserInteractionNeededResponse.Parameters params;
    @Mock
    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteraction userPaymentInteractionContract;

    @InjectMocks
    private UserPaymentInteractionService testClass;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateUserPaymentInteraction() {
        when(userPaymentInteractionMapper.mapFromDapi(any())).thenReturn(userPaymentInteraction);
        when(userPaymentInteractionResource.creation(any())).thenReturn(userPaymentInteractionId);
        assertSame(testClass.createUserPaymentInteraction(any()), userPaymentInteractionId);
    }

    @Test
    public void testUpdateOutcomeUserPaymentInteraction() {
        testClass.updateOutcomeUserPaymentInteraction(USER_PAYMENT_INT_ID, Collections.emptyMap());
        verify(userPaymentInteractionResource, times(1)).creationOutcome(any(), any());
    }

    @Test
    public void testUpdateOutcomeUserPaymentInteractionWithParams() {
        testClass.updateOutcomeUserPaymentInteraction(USER_PAYMENT_INT_ID, params);
        verify(userPaymentInteractionResource, times(1)).creationOutcome(any(), any());
    }

    @Test
    public void testRetrieveUserPaymentInteraction() throws ResourceNotFoundException {
        when(userPaymentInteractionResource.retrieve(any())).thenReturn(userPaymentInteraction);
        when(userPaymentInteractionMapper.mapUserPaymentInteractionToContract(any())).thenReturn(userPaymentInteractionContract);
        testClass.retrieveUserPaymentInteraction(new UserPaymentInteractionId(USER_PAYMENT_INT_ID));
        assertSame(testClass.retrieveUserPaymentInteraction(any()), userPaymentInteractionContract);
    }
}
