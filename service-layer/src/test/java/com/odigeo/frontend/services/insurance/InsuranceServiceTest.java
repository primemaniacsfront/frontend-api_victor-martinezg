package com.odigeo.frontend.services.insurance;

import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.insurance.api.last.InsuranceApiServiceException;
import com.odigeo.insurance.api.last.UnexpectedInsuranceApiServiceException;
import com.odigeo.insurance.api.last.insurance.InsuranceOffers;
import com.odigeo.insurance.api.last.request.InvalidParametersException;
import com.odigeo.insurance.api.last.request.PostBookingOfferRequest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class InsuranceServiceTest {

    private static final String ERROR_MSG = "ERROR";

    @Mock
    private com.odigeo.insurance.api.last.InsuranceApiService insuranceApi;

    @InjectMocks
    private InsuranceService insuranceApiService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void getOffers() throws UnexpectedInsuranceApiServiceException, InsuranceApiServiceException, InvalidParametersException {
        InsuranceOffers insuranceOffers = new InsuranceOffers();
        when(insuranceApi.getOffers(any())).thenReturn(insuranceOffers);
        assertSame(insuranceApiService.getOffers(new PostBookingOfferRequest()), insuranceOffers);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getOffersUnexpectedInsuranceApiServiceException() throws UnexpectedInsuranceApiServiceException, InsuranceApiServiceException, InvalidParametersException {
        when(insuranceApi.getOffers(any())).thenThrow(new UnexpectedInsuranceApiServiceException(ERROR_MSG));
        insuranceApiService.getOffers(new PostBookingOfferRequest());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getOffersInsuranceApiServiceException() throws UnexpectedInsuranceApiServiceException, InsuranceApiServiceException, InvalidParametersException {
        when(insuranceApi.getOffers(any())).thenThrow(new InsuranceApiServiceException(ERROR_MSG));
        insuranceApiService.getOffers(new PostBookingOfferRequest());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getOffersInvalidParametersException() throws UnexpectedInsuranceApiServiceException, InsuranceApiServiceException, InvalidParametersException {
        when(insuranceApi.getOffers(any())).thenThrow(new InvalidParametersException(ERROR_MSG));
        insuranceApiService.getOffers(new PostBookingOfferRequest());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void getOffersRuntimeException() throws UnexpectedInsuranceApiServiceException, InsuranceApiServiceException, InvalidParametersException {
        when(insuranceApi.getOffers(any())).thenThrow(new RuntimeException(ERROR_MSG));
        insuranceApiService.getOffers(new PostBookingOfferRequest());
    }

}
