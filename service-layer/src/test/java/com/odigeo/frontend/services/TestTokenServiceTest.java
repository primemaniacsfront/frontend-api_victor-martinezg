package com.odigeo.frontend.services;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.visitengineapi.v1.VisitEngineException;
import com.odigeo.visitengineapi.v1.multitest.MultitestException;
import com.odigeo.visitengineapi.v1.multitest.TestAssignment;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentNotFoundException;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentService;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class TestTokenServiceTest {

    private static final Integer DEFAULT_PARTITION_NUMBER = 1;
    private static final Integer UNDEFINED_PARTITION_NUMBER = 0;

    @Mock
    private TestAssignmentService testAssignmentService;
    @Mock
    private VisitInformation visit;
    @Mock
    private Map<String, Integer> dimensions;
    @Mock
    private Logger logger;

    @InjectMocks
    private TestTokenService testTokenService;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(visit.getDimensionPartitionMap()).thenReturn(dimensions);
    }

    @Test
    public void testFind() throws Exception {
        String alias = "alias";
        Integer partitionValue = 2;
        TestAssignment token = createToken(alias, partitionValue);

        when(testAssignmentService.getTestAssignment(alias)).thenReturn(token);
        assertEquals(testTokenService.find(alias, visit), partitionValue);
    }

    @Test
    public void testFindTokenIsNull() {
        assertEquals(testTokenService.find("fake", visit), DEFAULT_PARTITION_NUMBER);
    }

    @Test
    public void testFindTokenDisabled() throws Exception {
        String alias = "alias";
        TestAssignment token = createToken(alias, DEFAULT_PARTITION_NUMBER + 1);

        when(token.isEnabled()).thenReturn(false);
        when(testAssignmentService.getTestAssignment(alias)).thenReturn(token);
        assertEquals(testTokenService.find(alias, visit), DEFAULT_PARTITION_NUMBER);
    }

    @Test
    public void testFindWinnerPartition() throws Exception {
        String alias = "alias";
        Integer winnerPartitionValue = 2;
        TestAssignment token = createToken(alias, winnerPartitionValue + 1);

        when(token.getWinnerPartition()).thenReturn(winnerPartitionValue);
        when(testAssignmentService.getTestAssignment(alias)).thenReturn(token);
        assertEquals(testTokenService.find(alias, visit), winnerPartitionValue);
    }

    @Test
    public void testFindThrowsTestAssignmentException() throws Exception {
        testFindThrowsException(TestAssignmentNotFoundException.class);
    }

    @Test
    public void testFindThrowsVisitException() throws Exception {
        testFindThrowsException(VisitEngineException.class);
    }

    @Test
    public void testFindThrowsRuntimeException() throws Exception {
        testFindThrowsException(RuntimeException.class);
    }

    @Test
    public void testFindMultiple() throws Exception {
        Integer partitionValue1 = 1;
        Integer partitionValue2 = 2;
        String alias1 = "alias1";
        String alias2 = "alias2";

        List<TestAssignment> tokens = Arrays.asList(
            createToken(alias1, partitionValue1),
            createToken(alias2, partitionValue2));

        when(testAssignmentService.findAllTestAssignment()).thenReturn(tokens);

        List<Integer> result = testTokenService.findMultiple(Arrays.asList(alias1, alias2), visit);
        assertEquals(result.get(0), partitionValue1);
        assertEquals(result.get(1), partitionValue2);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testFindMultipleThrowsException() throws Exception {
        when(testAssignmentService.findAllTestAssignment()).thenThrow(new MultitestException());
        testTokenService.findMultiple(Collections.emptyList(), visit);
    }

    private void testFindThrowsException(Class<? extends Exception> ex) throws Exception {
        when(testAssignmentService.getTestAssignment(anyString())).thenThrow(ex);
        assertEquals(testTokenService.find("alias", visit), DEFAULT_PARTITION_NUMBER);
    }

    private TestAssignment createToken(String alias, Integer partitionValue) {
        TestAssignment token = mock(TestAssignment.class);
        String partitionName = alias;

        when(token.isEnabled()).thenReturn(true);
        when(token.getWinnerPartition()).thenReturn(UNDEFINED_PARTITION_NUMBER);
        when(token.getAssignedTestLabel()).thenReturn(partitionName);
        when(token.getTestName()).thenReturn(alias);
        when(dimensions.get(partitionName)).thenReturn(partitionValue);

        return token;
    }

}
