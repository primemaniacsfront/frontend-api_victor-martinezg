package com.odigeo.frontend.onefront.utils;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.util.JsonUtils;

import java.io.IOException;
import java.net.URI;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.Cookie;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.IterableUtils;
import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class OFRestClientTest {

    private static final int TIMEOUT = 120 * 1000;

    private static final String SERVICE_NAME = "service";

    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private OFUriBuilder uriBuilder;
    @Mock
    private CloseableHttpClient httpClient;
    @Mock
    private HttpClientBuilder httpClientBuilder;
    @Mock
    private CloseableHttpResponse httpResponse;
    @Mock
    private URI uri;
    @Mock
    private StatusLine status;
    @Mock
    private RequestInfo requestInfo;

    @InjectMocks
    private OFRestClient restClient;

    private MockedStatic<HttpClients> httpClientsMock;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        httpClientsMock = mockStatic(HttpClients.class);
        httpClientsMock.when(HttpClients::custom).thenReturn(httpClientBuilder);

        String host = "host";
        when(uri.getHost()).thenReturn(host);
        when(uriBuilder.build(SERVICE_NAME, requestInfo)).thenReturn(uri);

        initializeCookies(host);
        initializeHeaders();

        when(httpClientBuilder.setDefaultRequestConfig(argThat(requestConfig ->
                requestConfig.getConnectTimeout() == TIMEOUT
                        && requestConfig.getConnectionRequestTimeout() == TIMEOUT
                        && requestConfig.getSocketTimeout() == TIMEOUT
        ))).thenReturn(httpClientBuilder);
        when(httpClientBuilder.setSSLContext(any())).thenReturn(httpClientBuilder);
        when(httpClientBuilder.setSSLHostnameVerifier(any())).thenReturn(httpClientBuilder);
        when(httpClientBuilder.build()).thenReturn(httpClient);

        when(httpResponse.getStatusLine()).thenReturn(status);
        when(status.getStatusCode()).thenReturn(Status.OK.getStatusCode());
    }

    private void initializeCookies(String host) {
        String clientCookieName = "name";
        String clientCookieValue = "value";
        String visitValue = "visit";
        String jsessionValue = "jsession";
        String sbJsessionValue = "sbjsession";

        List<Cookie> clientCookies = Arrays.asList(
                new Cookie(clientCookieName, clientCookieValue),
                new Cookie(SiteCookies.VISIT_INFORMATION.value(), "fakeVisit"),
                new Cookie(SiteCookies.OF_JSESSIONID.value(), "fakeJsession"),
                new Cookie(SiteCookies.SB_JSESSIONID.value(), "fakeSbJsession"));
        when(requestInfo.getCookies()).thenReturn(clientCookies);

        when(requestInfo.getHeaderOrCookie(SiteHeaders.VISIT_INFORMATION, SiteCookies.VISIT_INFORMATION))
                .thenReturn(visitValue);
        when(requestInfo.getHeaderOrCookie(SiteHeaders.OF_JSESSIONID, SiteCookies.OF_JSESSIONID))
                .thenReturn(jsessionValue);
        when(requestInfo.getHeaderOrCookie(SiteHeaders.SB_JSESSIONID, SiteCookies.SB_JSESSIONID))
                .thenReturn(sbJsessionValue);

        Map<String, String> apacheCookies = Stream.of(
                new SimpleEntry<>(clientCookieName, clientCookieValue),
                new SimpleEntry<>(SiteCookies.VISIT_INFORMATION.value(), visitValue),
                new SimpleEntry<>(SiteCookies.OF_JSESSIONID.value(), jsessionValue),
                new SimpleEntry<>(SiteCookies.SB_JSESSIONID.value(), sbJsessionValue)
        ).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));

        when(httpClientBuilder.setDefaultCookieStore(argThat(cookieStore ->
                cookieStore.getCookies().stream().allMatch(cookie ->
                        cookie.getValue().equals(apacheCookies.get(cookie.getName()))
                                && cookie.getDomain().equals(host)
                                && cookie.getPath() == null
                )))).thenReturn(httpClientBuilder);
    }

    private void initializeHeaders() {
        String traceIdValue = "traceId";
        String refererValue = "referer";

        when(requestInfo.getHeader(SiteHeaders.TRACE_ID)).thenReturn(traceIdValue);
        when(requestInfo.getHeader(SiteHeaders.REFERER)).thenReturn(refererValue);

        when(httpClientBuilder.setDefaultHeaders(argThat(headers -> {
            Header traceId = IterableUtils.first(headers);
            Header referer = IterableUtils.get(headers, 1);

            return headers.size() == 2
                    && traceId.getName().equals(SiteHeaders.TRACE_ID.value())
                    && traceId.getValue().equals(traceIdValue)
                    && referer.getName().equals(SiteHeaders.REFERER.value())
                    && referer.getValue().equals(refererValue);
        }))).thenReturn(httpClientBuilder);
    }

    @AfterMethod
    public void end() {
        httpClientsMock.close();
    }

    @Test
    public void testDoPost() throws IOException {
        String body = "body";
        String jsonBody = "jsonBody";

        when(jsonUtils.toJson(body)).thenReturn(jsonBody);
        when(httpClient.execute(argThat(httpRequest -> {
            return httpRequest.getURI().equals(uri)
                    && httpRequest.getMethod().equals(HttpMethod.POST);
        }))).thenReturn(httpResponse);

        assertSame(restClient.doPost(SERVICE_NAME, requestInfo, body), httpResponse);
        verify(httpClientBuilder).setSSLContext(any());
        verify(httpClientBuilder).setSSLHostnameVerifier(any());
    }

    @Test(expectedExceptions = IOException.class)
    public void testDoPostThrowsException() throws IOException {
        when(httpClient.execute(any())).thenThrow(new RuntimeException());
        restClient.doPost(SERVICE_NAME, requestInfo, null);
    }

    @Test
    public void testDoGet() throws IOException {
        when(httpClient.execute(argThat(httpRequest -> httpRequest.getURI().equals(uri)
                && httpRequest.getMethod().equals(HttpMethod.GET)
        ))).thenReturn(httpResponse);

        assertSame(restClient.doGet(SERVICE_NAME, requestInfo), httpResponse);
        verify(httpClientBuilder).setSSLContext(any());
        verify(httpClientBuilder).setSSLHostnameVerifier(any());
    }

    @Test(expectedExceptions = IOException.class)
    public void testDoGetThrowsException() throws IOException {
        when(httpClient.execute(any())).thenThrow(new RuntimeException());
        restClient.doGet(SERVICE_NAME, requestInfo);
    }

}
