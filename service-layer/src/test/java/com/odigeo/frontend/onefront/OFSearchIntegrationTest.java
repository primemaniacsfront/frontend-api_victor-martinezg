package com.odigeo.frontend.onefront;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelectionRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.onefront.utils.OFRestClient;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.frontend.services.onefront.SearchSessionLoaderService;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.error.MessageResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OFSearchIntegrationTest {

    private static final String SEND_SEARCH_RESULTS_SERVICE = "loadSearchInfoInSession";
    private static final String SEND_SEARCH_META_RESULTS_SERVICE = "loadSearchMetaInfoInSession";
    private static final String WNG_024 = "WNG-024";
    private static final String WNG_025 = "WNG-025";

    @Mock
    private SiteVariations siteVariations;
    @Mock
    private OFRestClient ofRestClient;
    @Mock
    private SearchService searchService;
    @Mock
    private ResolverContext context;
    @Mock
    private SearchResponse searchResponse;
    @Mock
    private SearchRequest searchRequest;
    @Mock
    private SearchSessionLoaderService searchSessionLoaderService;
    @Mock
    private JsonUtils jsonUtils;

    @InjectMocks
    private OFSearchIntegration searchIntegration;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testSendSearchResponseUsingHttpClient() throws Exception {
        RequestInfo requestInfo = mock(RequestInfo.class);
        CloseableHttpResponse response = mock(CloseableHttpResponse.class);

        givenItinerary();
        when(context.getRequestInfo()).thenReturn(requestInfo);
        when(ofRestClient.doPost(SEND_SEARCH_RESULTS_SERVICE, requestInfo, searchResponse)).thenReturn(response);
        when(siteVariations.isOFCommonsRestServiceEnabled(context.getVisitInformation())).thenReturn(false);

        searchIntegration.sendSearchResponse(searchRequest, searchResponse, context);
        verify(response).close();
    }

    @Test
    public void testSendSearchResponseWhenExternalSelectionUsingHttpClient() throws Exception {
        RequestInfo requestInfo = mock(RequestInfo.class);
        CloseableHttpResponse response = mock(CloseableHttpResponse.class);

        givenItineraryWithExternalSelection();
        when(context.getRequestInfo()).thenReturn(requestInfo);
        when(ofRestClient.doPost(SEND_SEARCH_META_RESULTS_SERVICE, requestInfo, searchResponse)).thenReturn(response);
        when(siteVariations.isOFCommonsRestServiceEnabled(context.getVisitInformation())).thenReturn(false);

        searchIntegration.sendSearchResponse(searchRequest, searchResponse, context);
        verify(response).close();
    }

    @Test
    public void testSendSearchResponseUsingCommonsRest() throws Exception {
        RequestInfo requestInfo = mock(RequestInfo.class);

        givenItinerary();
        when(context.getRequestInfo()).thenReturn(requestInfo);
        when(siteVariations.isOFCommonsRestServiceEnabled(context.getVisitInformation())).thenReturn(true);
        doNothing().when(searchSessionLoaderService).loadSearchInfoInSession(anyString(), any(ResolverContext.class));

        searchIntegration.sendSearchResponse(searchRequest, searchResponse, context);
    }

    @Test
    public void testSendSearchResponseWhenExternalSelectionUsingCommonsRest() throws Exception {
        RequestInfo requestInfo = mock(RequestInfo.class);

        givenItineraryWithExternalSelection();
        when(context.getRequestInfo()).thenReturn(requestInfo);
        when(siteVariations.isOFCommonsRestServiceEnabled(context.getVisitInformation())).thenReturn(true);

        searchIntegration.sendSearchResponse(searchRequest, searchResponse, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testSendSearchResponseException() throws Exception {
        givenItinerary();
        when(ofRestClient.doPost(anyString(), any(), any())).thenThrow(IOException.class);
        searchIntegration.sendSearchResponse(searchRequest, searchResponse, context);
    }

    @Test
    public void testSendSearchResponseWithWarning() throws Exception {
        assertSendSearchResponseWithWarning(WNG_024);
        assertSendSearchResponseWithWarning(WNG_025);
    }

    private void assertSendSearchResponseWithWarning(String warningCode) throws Exception {
        MessageResponse message = mock(MessageResponse.class);
        when(searchService.getSearchMessage(searchResponse)).thenReturn(message);
        when(message.getCode()).thenReturn(warningCode);

        searchIntegration.sendSearchResponse(searchRequest, searchResponse, context);
        verify(ofRestClient, never()).doPost(anyString(), any(), any());
    }

    private void givenItinerary() {
        ItineraryRequest itinerary = mock(ItineraryRequest.class);

        when(searchRequest.getItinerary()).thenReturn(itinerary);
    }

    private void givenItineraryWithExternalSelection() {
        ItineraryRequest itinerary = mock(ItineraryRequest.class);
        ExternalSelectionRequest externalSelection = mock(ExternalSelectionRequest.class);

        when(searchRequest.getItinerary()).thenReturn(itinerary);
        when(itinerary.getExternalSelection()).thenReturn(externalSelection);
    }

}
