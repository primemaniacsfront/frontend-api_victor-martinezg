package com.odigeo.frontend.onefront.utils;

import com.odigeo.frontend.commons.context.RequestInfo;
import java.net.URI;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class OFUriBuilderTest {

    private static final String OF_FEAPI_PATH = "/travel/service/frontendapi/";
    private static final String MY_LOCAL_ENVIRONMENT_HOST = "mylocalenvironment.com";
    private static final int MY_LOCAL_ENVIRONMENT_PORT = 8080;

    private static final String SERVICE_NAME = "service";

    @Mock
    private RequestInfo requestInfo;

    private OFUriBuilder builder;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        builder = new OFUriBuilder();
    }

    @Test
    public void testBuild() {
        String host = "www.google.com";

        when(requestInfo.getRequestUrl()).thenReturn("https://" + host);

        URI uri = builder.build(SERVICE_NAME, requestInfo);
        assertEquals(uri.getScheme(), "https");
        assertEquals(uri.getHost(), host);
        assertEquals(uri.getPath(), OF_FEAPI_PATH + SERVICE_NAME);
        assertEquals(uri.getPort(), -1);
    }

    @Test
    public void testBuildMyLocalEnvironment() {
        String url = "https://" + MY_LOCAL_ENVIRONMENT_HOST;

        when(requestInfo.getRequestUrl()).thenReturn(url);
        assertEquals(builder.build(SERVICE_NAME, requestInfo).getPort(), MY_LOCAL_ENVIRONMENT_PORT);
    }

    @Test
    public void testBuildThrowsException() {
        String undefinedUrl = "|";

        when(requestInfo.getRequestUrl()).thenReturn(undefinedUrl);
        assertNull(builder.build(SERVICE_NAME, requestInfo));
    }

}
