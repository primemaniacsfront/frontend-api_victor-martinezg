package com.odigeo.frontend.onefront;

import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class OFShoppingCartIntegrationTest {

    private OFShoppingCartIntegration integration;

    @BeforeMethod
    public void init() {
        integration = new OFShoppingCartIntegration();
    }

    @Test
    public void testBuildSegmentKeysMap() {
        FareItinerary fareItinerary = mock(FareItinerary.class);
        Integer segment1 = 1;
        Integer segment2 = 2;
        Integer segment3 = 3;

        when(fareItinerary.getFirstSegments()).thenReturn(Collections.singletonList(segment1));
        when(fareItinerary.getSecondSegments()).thenReturn(Arrays.asList(segment2, segment3));
        when(fareItinerary.getThirdSegments()).thenReturn(Collections.emptyList());
        when(fareItinerary.getFourthSegments()).thenReturn(Collections.emptyList());
        when(fareItinerary.getFifthSegments()).thenReturn(Collections.emptyList());
        when(fareItinerary.getSixthSegments()).thenReturn(Collections.emptyList());

        Map<Integer, Integer> segmentKeys = integration.buildSegmentKeysMap(fareItinerary);

        assertEquals(segmentKeys.get(segment1).intValue(), 0);
        assertEquals(segmentKeys.get(segment2).intValue(), 0);
        assertEquals(segmentKeys.get(segment3).intValue(), 1);
    }
}
