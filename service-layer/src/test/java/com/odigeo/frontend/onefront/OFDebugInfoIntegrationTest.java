package com.odigeo.frontend.onefront;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.onefront.OFDebugInfoIntegration.OFDebugInfo;
import com.odigeo.frontend.onefront.utils.OFRestClient;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OFDebugInfoIntegrationTest {

    private static final String QAMODE_SETTINGS_SERVICE = "getQaModeSettings";

    @Mock
    private OFRestClient ofRestClient;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private Logger logger;
    @Mock
    private ResolverContext context;
    @Mock
    private DebugInfo debugInfo;
    @Mock
    private RequestInfo requestInfo;
    @Mock
    private CloseableHttpResponse httpResponse;

    @InjectMocks
    private OFDebugInfoIntegration debugInfoIntegration;
    private OFDebugInfo ofDebugInfo;

    @BeforeMethod
    public void init() throws Exception {
        MockitoAnnotations.openMocks(this);

        String json = "json";
        HttpEntity httpEntity = EntityBuilder.create().setText(json).build();
        ofDebugInfo = new OFDebugInfo();

        when(httpResponse.getEntity()).thenReturn(httpEntity);
        when(jsonUtils.fromJson(json, OFDebugInfo.class)).thenReturn(ofDebugInfo);
        when(ofRestClient.doGet(QAMODE_SETTINGS_SERVICE, requestInfo)).thenReturn(httpResponse);

        when(context.getDebugInfo()).thenReturn(debugInfo);
        when(context.getRequestInfo()).thenReturn(requestInfo);
    }

    @Test
    public void testRequestDebugInfo() throws Exception {
        ofDebugInfo.dapiCaptureEnabled = true;
        ofDebugInfo.qaModeEnabled = true;

        debugInfoIntegration.requestDebugInfo(context);
        verify(debugInfo).setEnabled(true);
        verify(debugInfo).setQaModeEnabled(true);
        verify(debugInfo).setApiCaptureEnabled(true);
        verify(httpResponse).close();
    }

    @Test
    public void testRequestDebugInfoQaModeDisabled() throws Exception {
        ofDebugInfo.dapiCaptureEnabled = true;

        debugInfoIntegration.requestDebugInfo(context);
        verify(debugInfo).setEnabled(true);
        verify(debugInfo).setQaModeEnabled(false);
        verify(debugInfo).setApiCaptureEnabled(true);
        verify(httpResponse).close();
    }

    @Test
    public void testRequestDebugInfoDisabled() throws Exception {
        debugInfoIntegration.requestDebugInfo(context);
        verify(debugInfo).setEnabled(false);
        verify(debugInfo).setQaModeEnabled(false);
        verify(debugInfo).setApiCaptureEnabled(false);
        verify(httpResponse).close();
    }

    @Test
    public void testRequestDebugInfoThrowsException() throws Exception {
        when(ofRestClient.doGet(any(), any())).thenThrow(IOException.class);

        debugInfoIntegration.requestDebugInfo(context);
        verify(logger).warning(eq(OFDebugInfoIntegration.class), anyString(), any());
    }

}
