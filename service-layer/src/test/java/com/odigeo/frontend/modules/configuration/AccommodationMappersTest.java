package com.odigeo.frontend.modules.configuration;

import com.google.inject.Binder;
import com.google.inject.binder.AnnotatedBindingBuilder;
import com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.mappers.AccommodationShoppingItemMapper;
import com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.mappers.AccommodationShoppingItemMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationSummaryMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationSummaryMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.details.mappers.AccommodationDetailResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.details.mappers.AccommodationDetailResponseMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.SearchRoomAvailabilityResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.SearchRoomAvailabilityResponseMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.AccommodationSearchResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.AccommodationSearchResponseMapperImpl;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.mappers.AccommodationStaticContentResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.mappers.AccommodationStaticContentResponseMapperImpl;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccommodationMappersTest {

    @Mock
    private Binder binder;
    @Mock
    private AnnotatedBindingBuilder annotatedBindingBuilder;

    private AccommodationMappers mappers;

    @BeforeMethod
    public void setUp() throws IllegalAccessException {
        MockitoAnnotations.openMocks(this);
        mappers = new AccommodationMappers();
        FieldUtils.writeField(mappers, "binder", binder, true);
        when(binder.bind((Class<Object>) any())).thenReturn(annotatedBindingBuilder);
    }

    @Test
    public void testConfigure() {
        mappers.configure();
        verifyBinder(SearchRoomAvailabilityResponseMapper.class, SearchRoomAvailabilityResponseMapperImpl.class);
        verifyBinder(AccommodationSearchResponseMapper.class, AccommodationSearchResponseMapperImpl.class);
        verifyBinder(AccommodationStaticContentResponseMapper.class, AccommodationStaticContentResponseMapperImpl.class);
        verifyBinder(AccommodationDetailResponseMapper.class, AccommodationDetailResponseMapperImpl.class);
        verifyBinder(AccommodationShoppingItemMapper.class, AccommodationShoppingItemMapperImpl.class);
        verifyBinder(AccommodationSummaryMapper.class, AccommodationSummaryMapperImpl.class);
    }

    public void verifyBinder(Class<?> mapper, Class<?> impl) {
        verify(binder).bind(eq(mapper));
        verify(annotatedBindingBuilder).to(eq(impl));
    }
}
