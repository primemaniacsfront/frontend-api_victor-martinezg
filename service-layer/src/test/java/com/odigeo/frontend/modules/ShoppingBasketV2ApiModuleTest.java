package com.odigeo.frontend.modules;

import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.frontend.services.shoppingbasket.ShoppingBasketApiV2;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ShoppingBasketV2ApiModuleTest {

    private static final Integer CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final Integer SOCKET_TIMEOUT_IN_MILLIS = 10000;

    @Test
    public void testGetServiceConfiguration() {
        ShoppingBasketV2ApiModule module = new ShoppingBasketV2ApiModule();
        ServiceConfiguration<ShoppingBasketApiV2> serviceConfig =
                module.getServiceConfiguration(ShoppingBasketApiV2.class);

        ConnectionConfiguration connectionConfig = serviceConfig.getConnectionConfiguration();

        assertEquals(connectionConfig.getConnectionTimeoutInMillis(), CONNECTION_TIMEOUT_IN_MILLIS);
        assertEquals(connectionConfig.getSocketTimeoutInMillis(), SOCKET_TIMEOUT_IN_MILLIS);
    }
}
