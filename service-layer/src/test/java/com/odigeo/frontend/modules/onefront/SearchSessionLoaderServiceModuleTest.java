package com.odigeo.frontend.modules.onefront;

import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;


import com.odigeo.frontend.contract.onefront.SearchSessionLoaderService;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SearchSessionLoaderServiceModuleTest {

    private static final Integer CONNECTION_TIMEOUT_IN_MILLIS = 120 * 1000;
    private static final Integer SOCKET_TIMEOUT_IN_MILLIS = 120 * 1000;
    private static final Integer MAX_CONCURRENT_CONNECTION = 20;

    @Test
    public void testGetServiceConfiguration() {
        SearchSessionLoaderServiceModule module = new SearchSessionLoaderServiceModule();
        ServiceConfiguration<SearchSessionLoaderService> serviceConfig =
                module.getServiceConfiguration(SearchSessionLoaderService.class);

        ConnectionConfiguration connectionConfig = serviceConfig.getConnectionConfiguration();

        assertEquals(connectionConfig.getConnectionTimeoutInMillis(), CONNECTION_TIMEOUT_IN_MILLIS);
        assertEquals(connectionConfig.getSocketTimeoutInMillis(), SOCKET_TIMEOUT_IN_MILLIS);
        assertEquals(connectionConfig.getMaxConcurrentConnections(), MAX_CONCURRENT_CONNECTION);
    }

}
