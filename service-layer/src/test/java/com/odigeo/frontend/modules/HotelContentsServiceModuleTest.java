package com.odigeo.frontend.modules;

import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.hcsapi.v9.HcsApiService;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class HotelContentsServiceModuleTest {

    private static final Integer CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final Integer SOCKET_TIMEOUT_IN_MILLIS = 10000;

    @Test
    public void testGetServiceConfiguration() {
        HotelContentsServiceModule module = new HotelContentsServiceModule();
        ServiceConfiguration<HcsApiService> serviceConfig =
            module.getServiceConfiguration(HcsApiService.class);

        ConnectionConfiguration connectionConfig = serviceConfig.getConnectionConfiguration();

        assertEquals(connectionConfig.getConnectionTimeoutInMillis(), CONNECTION_TIMEOUT_IN_MILLIS);
        assertEquals(connectionConfig.getSocketTimeoutInMillis(), SOCKET_TIMEOUT_IN_MILLIS);
    }

}
