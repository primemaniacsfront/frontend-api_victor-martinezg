package com.odigeo.frontend.modules;

import com.odigeo.commons.rest.Interceptor;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.commons.rest.monitoring.interceptor.MonitorFactory;
import com.odigeo.userprofiles.api.v2.SSOApiService;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponseInterceptor;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class UserDescriptionSSOApiServiceModuleTest {
    private static final Integer CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final Integer SOCKET_TIMEOUT_IN_MILLIS = 10000;
    private static final Integer MAX_CONCURRENT_CONNECTIONS = 50;

    @Test
    public void testGetServiceConfiguration() {
        MonitorFactory monitorFactory = mock(MonitorFactory.class);
        HttpRequestInterceptor requestInterceptor = mock(HttpRequestInterceptor.class);
        HttpResponseInterceptor responseInterceptor = mock(HttpResponseInterceptor.class);
        Interceptor restInterceptor = mock(Interceptor.class);

        UserDescriptionSSOApiServiceModule module = new UserDescriptionSSOApiServiceModule() {
            @Override
            MonitorFactory createMonitorFactory() {
                return monitorFactory;
            }
        };

        when(monitorFactory.newHttpRequestInterceptor()).thenReturn(requestInterceptor);
        when(monitorFactory.newHttpResponseInterceptor()).thenReturn(responseInterceptor);
        when(monitorFactory.newRestUtilsInterceptor()).thenReturn(restInterceptor);

        ServiceConfiguration<SSOApiService> serviceConfig = module.getServiceConfiguration(SSOApiService.class);
        ConnectionConfiguration connectionConfig = serviceConfig.getConnectionConfiguration();
        InterceptorConfiguration<SSOApiService> interceptorConfig = serviceConfig.getInterceptorConfiguration();

        assertEquals(connectionConfig.getConnectionTimeoutInMillis(), CONNECTION_TIMEOUT_IN_MILLIS);
        assertEquals(connectionConfig.getSocketTimeoutInMillis(), SOCKET_TIMEOUT_IN_MILLIS);
        assertEquals(connectionConfig.getMaxConcurrentConnections(), MAX_CONCURRENT_CONNECTIONS);

        assertTrue(interceptorConfig.getHttpRequestInterceptors().contains(requestInterceptor));
        assertTrue(interceptorConfig.getHttpResponseInterceptors().contains(responseInterceptor));
        assertTrue(interceptorConfig.getInterceptors().contains(restInterceptor));
    }
}
