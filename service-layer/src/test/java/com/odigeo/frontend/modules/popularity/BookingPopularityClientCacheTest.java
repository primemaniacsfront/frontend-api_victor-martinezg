package com.odigeo.frontend.modules.popularity;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.Serializable;

import static org.testng.Assert.*;

public class BookingPopularityClientCacheTest {

    private static final String CACHE_KEY = "CACHE_KEY";

    BookingPopularityClientCache cache = new BookingPopularityClientCache();

    @Mock
    Serializable serializable;
    @Mock
    Object noSerializable;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        cache.clear();
    }

    @Test
    public void testCacheEntry() {
        cache.addEntry(CACHE_KEY, serializable);
        assertEquals(cache.getEntry(CACHE_KEY), serializable);
    }

    @Test
    public void testCacheMissedEntry() {
        cache.addEntry(CACHE_KEY, null);
        assertNull(cache.getEntry(CACHE_KEY));
        assertTrue(cache.isMissedEntry(CACHE_KEY));
    }

    @Test
    public void testCacheNoSerializable() {
        cache.addEntry(CACHE_KEY, noSerializable);
        assertNull(cache.getEntry(CACHE_KEY));
        assertFalse(cache.isMissedEntry(CACHE_KEY));
    }

    @Test
    public void testCacheClear() {
        cache.addEntry(CACHE_KEY, serializable);
        cache.clear();
        assertNull(cache.getEntry(CACHE_KEY));
    }

    @Test
    public void testCacheSize() {
        cache.addEntry(CACHE_KEY, serializable);
        assertEquals(cache.size(), 1);
    }


}
