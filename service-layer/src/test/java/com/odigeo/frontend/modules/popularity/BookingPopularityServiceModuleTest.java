package com.odigeo.frontend.modules.popularity;

import com.edreamsodigeo.seo.sslppopularitystats.popularity.v1.BookingPopularityService;
import com.odigeo.commons.rest.Interceptor;
import com.odigeo.commons.rest.cache.CacheInterceptor;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import org.testng.annotations.Test;

import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BookingPopularityServiceModuleTest {

    private static final Integer CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final Integer SOCKET_TIMEOUT_IN_MILLIS = 10000;

    private BookingPopularityServiceModule module = new BookingPopularityServiceModule();

    @Test
    public void testGetServiceConfiguration() {
        ServiceConfiguration<BookingPopularityService> serviceConfig =
                module.getServiceConfiguration(BookingPopularityService.class);

        ConnectionConfiguration connectionConfig = serviceConfig.getConnectionConfiguration();
        Set<Interceptor> interceptors = serviceConfig.getInterceptorConfiguration().getInterceptors();
        Interceptor interceptor = interceptors.stream().findFirst().orElse(null);

        assertEquals(connectionConfig.getConnectionTimeoutInMillis(), CONNECTION_TIMEOUT_IN_MILLIS);
        assertEquals(connectionConfig.getSocketTimeoutInMillis(), SOCKET_TIMEOUT_IN_MILLIS);

        assertEquals(interceptors.size(), 1);
        assertTrue(interceptor instanceof CacheInterceptor);
    }

}
