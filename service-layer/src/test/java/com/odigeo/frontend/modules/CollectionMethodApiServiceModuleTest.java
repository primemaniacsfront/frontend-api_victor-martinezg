package com.odigeo.frontend.modules;

import com.odigeo.collectionmethod.v3.CollectionMethodApiService;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CollectionMethodApiServiceModuleTest {

    private static final Integer CONNECTION_TIMEOUT_IN_MILLIS = 10000;
    private static final Integer SOCKET_TIMEOUT_IN_MILLIS = 10000;
    private static final Integer MAX_CONCURRENT_CONNECTIONS = 10;

    @Test
    public void testGetServiceConfiguration() {
        CollectionMethodApiServiceModule module = new CollectionMethodApiServiceModule();
        ServiceConfiguration<CollectionMethodApiService> serviceConfig =
                module.getServiceConfiguration(CollectionMethodApiService.class);

        ConnectionConfiguration connectionConfig = serviceConfig.getConnectionConfiguration();

        assertEquals(connectionConfig.getConnectionTimeoutInMillis(), CONNECTION_TIMEOUT_IN_MILLIS);
        assertEquals(connectionConfig.getSocketTimeoutInMillis(), SOCKET_TIMEOUT_IN_MILLIS);
        assertEquals(connectionConfig.getMaxConcurrentConnections(), MAX_CONCURRENT_CONNECTIONS);
    }

}
