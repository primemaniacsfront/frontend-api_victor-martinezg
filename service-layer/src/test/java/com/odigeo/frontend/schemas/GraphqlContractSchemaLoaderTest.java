package com.odigeo.frontend.schemas;

import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertNotNull;

public class GraphqlContractSchemaLoaderTest {
    @Test
    public void testGetSchemaList() {
        GraphqlContractSchemaLoader loader = new GraphqlContractSchemaLoader();
        List<String> schemas = loader.getSchemaList();
        assertNotNull(schemas);
    }
}
