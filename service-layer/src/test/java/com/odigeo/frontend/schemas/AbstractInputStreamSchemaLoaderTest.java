package com.odigeo.frontend.schemas;

import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertTrue;

public class AbstractInputStreamSchemaLoaderTest {
    @Test
    public void testGetInputStream() {
        AbstractInputStreamSchemaLoader loader = mock(AbstractInputStreamSchemaLoader.class);
        when(loader.getSchemaList())
            .thenReturn(Collections.emptyList());
        doCallRealMethod()
            .when(loader)
            .getReaderList();

        assertTrue(loader.getReaderList().isEmpty());
    }
}
