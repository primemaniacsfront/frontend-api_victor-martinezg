package com.odigeo.frontend.resolvers.search.handlers.helper;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.ItineraryCo2FootprintResult;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.request.ItineraryFootprintCalculatorRequest;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.response.ItineraryFootprintCalculatorResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.AsyncUtils;
import com.odigeo.frontend.resolvers.search.handlers.carbonfootprint.CarbonFootprintHandler;
import com.odigeo.frontend.resolvers.search.handlers.carbonfootprint.CarbonFootprintResponse;
import com.odigeo.frontend.resolvers.search.handlers.carbonfootprint.ItineraryFootprintCalculatorRequestMapper;
import com.odigeo.frontend.resolvers.search.handlers.mir.ItineraryRatingRequestMapper;
import com.odigeo.frontend.resolvers.search.handlers.mir.MirHandler;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.services.CarbonFootprintService;
import com.odigeo.frontend.services.MirService;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.ItineraryRatingResult;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.request.ItineraryRatingRequest;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.response.ItineraryRatingResponse;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

public class MirFootprintItineraryHandlerTest {

    @Mock
    private MirHandler mirHandler;
    @Mock
    private MirService mirService;
    @Mock
    private ItineraryRatingRequestMapper itineraryRatingRequestMapper;

    @Mock
    private CarbonFootprintHandler carbonFootprintHandler;
    @Mock
    private CarbonFootprintService carbonFootprintService;
    @Mock
    private ItineraryFootprintCalculatorRequestMapper itineraryFootprintCalculatorRequestMapper;

    @Mock
    private VisitInformation visit;
    @Mock
    private ResolverContext context;
    @Mock
    private AsyncUtils asyncUtils;

    @Mock
    private SearchResponseDTO searchResponseDTO;
    @Mock
    private SearchRequest searchRequestDTO;

    private CarbonFootprintResponse carbonFootprintResponse = new CarbonFootprintResponse();
    private ItineraryRatingResponse itineraryRatingResponse = new ItineraryRatingResponse();

    private MirFootprintItineraryHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new MirFootprintItineraryHandler();

        Guice.createInjector(binder -> {
            binder.bind(MirHandler.class).toProvider(() -> mirHandler);
            binder.bind(MirService.class).toProvider(() -> mirService);
            binder.bind(ItineraryRatingRequestMapper.class).toProvider(() -> itineraryRatingRequestMapper);
            binder.bind(CarbonFootprintHandler.class).toProvider(() -> carbonFootprintHandler);
            binder.bind(CarbonFootprintService.class).toProvider(() -> carbonFootprintService);
            binder.bind(ItineraryFootprintCalculatorRequestMapper.class).toProvider(() -> itineraryFootprintCalculatorRequestMapper);
            binder.bind(AsyncUtils.class).toProvider(() -> asyncUtils);
        }).injectMembers(handler);
        when(context.getVisitInformation()).thenReturn(visit);
        when(itineraryFootprintCalculatorRequestMapper.map(any(), any(), any())).thenReturn(new ItineraryFootprintCalculatorRequest());
        when(itineraryRatingRequestMapper.map(any(), any(), any())).thenReturn(new ItineraryRatingRequest());
        when(asyncUtils.mapFuture(any(), eq(CarbonFootprintResponse.class))).thenReturn(carbonFootprintResponse);
        when(asyncUtils.mapFuture(any(), eq(ItineraryRatingResponse.class))).thenReturn(itineraryRatingResponse);
    }

    @Test
    public void testIsEnabled() {
        when(searchResponseDTO.getItineraries()).thenReturn(Collections.singletonList(new SearchItineraryDTO()));
        when(searchRequestDTO.getTripType()).thenReturn(TripType.ONE_WAY);

        configureFootprintCalculatorService();
        configureMirService();
        handler.addMirRatingAndFootprintInfoItinerary(searchRequestDTO, searchResponseDTO, context);
        verify(mirHandler).addRatingAndReturningUserDeviceTracking(any(), any(), any());
        verify(carbonFootprintHandler).addCarbonFootprintInfo(any(), any(), any());
    }

    @Test
    public void testNotEnabledForItineraries() {

        when(searchResponseDTO.getItineraries()).thenReturn(Collections.emptyList());
        when(searchRequestDTO.getTripType()).thenReturn(TripType.ONE_WAY);

        handler.addMirRatingAndFootprintInfoItinerary(searchRequestDTO, searchResponseDTO, context);
        verifyNoInteractions(mirHandler);
        verifyNoInteractions(carbonFootprintHandler);
    }

    @Test
    public void testNotEnabledForTripType() {

        when(searchResponseDTO.getItineraries()).thenReturn(Collections.singletonList(new SearchItineraryDTO()));
        when(searchRequestDTO.getTripType()).thenReturn(TripType.MULTIPLE_DESTINATIONS);

        handler.addMirRatingAndFootprintInfoItinerary(searchRequestDTO, searchResponseDTO, context);
        verifyNoInteractions(mirHandler);
        verifyNoInteractions(carbonFootprintHandler);
    }

    @Test
    public void testResponseIsNull() {

        when(searchResponseDTO.getItineraries()).thenReturn(Collections.singletonList(new SearchItineraryDTO()));
        when(searchRequestDTO.getTripType()).thenReturn(TripType.ONE_WAY);
        when(asyncUtils.mapFuture(any(), eq(CarbonFootprintResponse.class))).thenReturn(null);
        when(asyncUtils.mapFuture(any(), eq(ItineraryRatingResponse.class))).thenReturn(null);

        handler.addMirRatingAndFootprintInfoItinerary(searchRequestDTO, searchResponseDTO, context);
        verifyNoInteractions(mirHandler);
        verifyNoInteractions(carbonFootprintHandler);
    }

    private void configureFootprintCalculatorService() {
        when(carbonFootprintService.getCarbonFootprint(any()))
                .thenReturn(getItineraryFootprintCalculatorResponse());
    }

    private void configureMirService() {
        when(mirService.getRatings(any())).thenReturn(getItineraryRatingResponse());
    }

    private ItineraryFootprintCalculatorResponse getItineraryFootprintCalculatorResponse() {
        ItineraryFootprintCalculatorResponse itineraryFootprintCalculatorResponse = new ItineraryFootprintCalculatorResponse();
        itineraryFootprintCalculatorResponse.setItineraryCo2FootprintResults(getItineraryCo2FootprintResults());
        return itineraryFootprintCalculatorResponse;
    }

    private List<ItineraryCo2FootprintResult> getItineraryCo2FootprintResults() {
        ItineraryCo2FootprintResult result = new ItineraryCo2FootprintResult();
        result.setKilosCo2e(BigDecimal.TEN);
        result.setKilosCo2(BigDecimal.TEN);
        result.setItineraryIndex(0);
        return Collections.singletonList(result);
    }

    private ItineraryRatingResponse getItineraryRatingResponse() {
        ItineraryRatingResponse itineraryRatingResponse = new ItineraryRatingResponse();
        itineraryRatingResponse.setItineraryRatingResults(getItineraryRatingResults());
        return itineraryRatingResponse;
    }

    private List<ItineraryRatingResult> getItineraryRatingResults() {
        ItineraryRatingResult itineraryRatingResult = new ItineraryRatingResult();
        itineraryRatingResult.setItineraryIndex(0);
        itineraryRatingResult.setRating(BigDecimal.ONE);
        return Collections.singletonList(itineraryRatingResult);
    }
}
