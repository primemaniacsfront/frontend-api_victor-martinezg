package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.odigeo.frontend.commons.mappers.GeoCoordinatesMapper;
import com.odigeo.frontend.commons.util.GeoUtils;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.GeoNode;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AccommodationLocationHandlerTest {

    @Mock
    private GeoLocationHandler geoLocationHandler;
    @Mock
    private GeoCoordinatesMapper geoCoordinatesMapper;
    @Mock
    private GeoUtils geoUtils;
    @Mock
    private GeoNode geoNode;

    @InjectMocks
    private AccommodationLocationHandler testClass;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testPopulateSearchedLocation() {
        Integer searchedCityGeoNodeId = 1;
        String searchedIataCode = "BCN";
        when(geoLocationHandler.computeDistanceFromGeonode(any(), any())).thenReturn(1.0);
        AccommodationSearchResponseDTO response = getAccommodationSearchResponseDTO();
        testClass.populateSearchedLocation(response, searchedCityGeoNodeId, searchedIataCode);
        assertEquals(response.getAccommodationResponse().getAccommodations().get(0).getDistanceFromCityCenter(), new Double(1.0));
    }

    private AccommodationSearchResponseDTO getAccommodationSearchResponseDTO() {
        AccommodationSearchResponseDTO response = new AccommodationSearchResponseDTO();
        AccommodationResponseDTO accommodationResponse = new AccommodationResponseDTO();
        SearchAccommodationDTO searchAccommodationDTO = new SearchAccommodationDTO();
        AccommodationDealInformationDTO accommodationDeal = new AccommodationDealInformationDTO();
        accommodationDeal.setCoordinates(new GeoCoordinatesDTO());
        searchAccommodationDTO.setAccommodationDeal(accommodationDeal);
        accommodationResponse.setAccommodations(Collections.singletonList(searchAccommodationDTO));
        response.setAccommodationResponse(accommodationResponse);
        return response;
    }


}
