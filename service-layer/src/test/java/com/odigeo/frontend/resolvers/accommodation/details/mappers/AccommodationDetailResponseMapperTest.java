package com.odigeo.frontend.resolvers.accommodation.details.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationDetail;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImage;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImageQuality;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImageType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImages;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.resolvers.accommodation.details.models.AccommodationDetailsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationImageQualityDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationRoomServiceDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.FacilityDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.FacilityGroupsDTO;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class AccommodationDetailResponseMapperTest {

    @Mock
    private AccommodationDetailsResponseDTO accommodationDetailsResponseDTO;
    @Mock
    private AccommodationDetailDTO accommodationDetailDTO;
    @Mock
    private AccommodationDealInformationDTO accommodationDealInformationDTO;
    @Mock
    private AccommodationImageDTO accommodationImageDTO;
    @Mock
    private FacilityGroupsDTO facilityGroupsDTO;
    @Mock
    private FacilityDTO facilityDTO;
    @Mock
    private AccommodationRoomServiceDTO accommodationRoomServiceDTO;

    AccommodationDetailResponseMapper mapper;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        mapper = new AccommodationDetailResponseMapperImpl();

        when(accommodationImageDTO.getUrl()).thenReturn("url");
        when(accommodationImageDTO.getThumbnailUrl()).thenReturn("getThumbnailUrl");
        when(accommodationImageDTO.getType()).thenReturn(AccommodationImageType.POOL);
        when(accommodationImageDTO.getQuality()).thenReturn(AccommodationImageQualityDTO.MEDIUM);

        when(accommodationDetailsResponseDTO.getAccommodationDetail()).thenReturn(accommodationDetailDTO);
        when(accommodationDetailDTO.getAccommodationDealInformation()).thenReturn(accommodationDealInformationDTO);
        when(facilityGroupsDTO.getCode()).thenReturn("getCode");
        when(facilityGroupsDTO.getDescription()).thenReturn("getDescription");
        when(facilityDTO.getCode()).thenReturn("facilityDTO.getCode()");
        when(facilityDTO.getDescription()).thenReturn("facilityDTO.getDescription()");
        when(accommodationRoomServiceDTO.getDescription()).thenReturn("accommodationRoomServiceDTO.getDescription()");
        when(accommodationRoomServiceDTO.getType()).thenReturn("accommodationRoomServiceDTO.getType()");
    }

    @Test
    public void testMapModelToContract() {
        when(accommodationDetailDTO.getAccommodationImages()).thenReturn(Arrays.asList(accommodationImageDTO));
        when(accommodationDetailDTO.getStateProvince()).thenReturn("getStateProvince");
        when(accommodationDetailDTO.getCountry()).thenReturn("getCountry");
        when(accommodationDetailDTO.getPostalCode()).thenReturn("getPostalCode");
        when(accommodationDetailDTO.getMapUrls()).thenReturn(Collections.singletonList("getMapUrls"));
        when(accommodationDetailDTO.getLocationDescription()).thenReturn("getLocationDescription");
        when(accommodationDetailDTO.getSurroundingAreaInfo()).thenReturn("getSurroundingAreaInfo");
        when(accommodationDetailDTO.getCheckInPolicy()).thenReturn("getCheckInPolicy");
        when(accommodationDetailDTO.getCheckOutPolicy()).thenReturn("getCheckOutPolicy");
        when(accommodationDetailDTO.getHotelPolicy()).thenReturn("getHotelPolicy");
        when(accommodationDetailDTO.getCreditCardTypes()).thenReturn(Collections.singletonList("getCreditCardTypes"));
        when(accommodationDetailDTO.getPaymentMethod()).thenReturn("getPaymentMethod");
        when(accommodationDetailDTO.getPhoneNumber()).thenReturn("getPhoneNumber");
        when(accommodationDetailDTO.getMail()).thenReturn("getMail");
        when(accommodationDetailDTO.getWeb()).thenReturn("getWeb");
        when(accommodationDetailDTO.getFax()).thenReturn("getFax");
        when(accommodationDetailDTO.getFacilityGroups()).thenReturn(Collections.singletonList(facilityGroupsDTO));
        when(facilityGroupsDTO.getFacilities()).thenReturn(Collections.singletonList(facilityDTO));
        when(accommodationDetailDTO.getAccommodationRoomServices()).thenReturn(Collections.singletonList(accommodationRoomServiceDTO));
        when(accommodationDetailDTO.getNumberOfRooms()).thenReturn("8");
        when(accommodationDetailDTO.getHotelType()).thenReturn(AccommodationType.GUEST_HOUSE);
        when(accommodationDealInformationDTO.getCoordinates()).thenReturn(new GeoCoordinatesDTO(BigDecimal.ONE, BigDecimal.TEN));

        AccommodationDetail response = mapper.map(accommodationDetailsResponseDTO).getAccommodationDetail();
        assertEquals(response.getStateProvince(), accommodationDetailDTO.getStateProvince());
        assertEquals(response.getCountry(), accommodationDetailDTO.getCountry());
        assertEquals(response.getPostalCode(), accommodationDetailDTO.getPostalCode());
        assertEquals(response.getMapUrls(), accommodationDetailDTO.getMapUrls());
        assertEquals(response.getLocationDescription(), accommodationDetailDTO.getLocationDescription());
        assertEquals(response.getSurroundingAreaInfo(), accommodationDetailDTO.getSurroundingAreaInfo());
        assertEquals(response.getCheckInPolicy(), accommodationDetailDTO.getCheckInPolicy());
        assertEquals(response.getCheckOutPolicy(), accommodationDetailDTO.getCheckOutPolicy());
        assertEquals(response.getCheckOutPolicy(), accommodationDetailDTO.getCheckOutPolicy());
        assertEquals(response.getHotelPolicy(), accommodationDetailDTO.getHotelPolicy());
        assertEquals(response.getCreditCardTypes().get(0), accommodationDetailDTO.getCreditCardTypes().get(0));
        assertEquals(response.getPaymentMethod(), accommodationDetailDTO.getPaymentMethod());
        assertEquals(response.getPhoneNumber(), accommodationDetailDTO.getPhoneNumber());
        assertEquals(response.getMail(), accommodationDetailDTO.getMail());
        assertEquals(response.getFax(), accommodationDetailDTO.getFax());
        assertEquals(response.getWeb(), accommodationDetailDTO.getWeb());
        assertEquals(response.getFacilityGroups().get(0).getCode(), accommodationDetailDTO.getFacilityGroups().get(0).getCode());
        assertEquals(response.getFacilityGroups().get(0).getDescription(), accommodationDetailDTO.getFacilityGroups().get(0).getDescription());
        assertEquals(response.getFacilityGroups().get(0).getFacilities().get(0).getCode(), accommodationDetailDTO.getFacilityGroups().get(0).getFacilities().get(0).getCode());
        assertEquals(response.getFacilityGroups().get(0).getFacilities().get(0).getDescription(), accommodationDetailDTO.getFacilityGroups().get(0).getFacilities().get(0).getDescription());
        assertEquals(response.getAccommodationRoomServices().get(0).getDescription(), accommodationDetailDTO.getAccommodationRoomServices().get(0).getDescription());
        assertEquals(response.getAccommodationRoomServices().get(0).getType(), accommodationDetailDTO.getAccommodationRoomServices().get(0).getType());
        assertEquals(response.getNumberOfRooms(), accommodationDetailDTO.getNumberOfRooms());
        assertEquals(response.getHotelType(), AccommodationType.GUEST_HOUSE);
        assertEquals(response.getAccommodationDealInformation().getCoordinates().getLongitude(),
            accommodationDealInformationDTO.getCoordinates().getLongitude());
        assertEquals(response.getAccommodationDealInformation().getCoordinates().getLatitude(),
            accommodationDealInformationDTO.getCoordinates().getLatitude());
    }

    @Test
    public void testMapImagesByQuality() {
        AccommodationImageDTO accommodationImage = mock(AccommodationImageDTO.class);
        when(accommodationImage.getUrl()).thenReturn("getUrl");
        when(accommodationImage.getThumbnailUrl()).thenReturn("ThumbnailUrl");
        when(accommodationImage.getType()).thenReturn(AccommodationImageType.AREA_INFORMATION);
        when(accommodationImage.getQuality()).thenReturn(AccommodationImageQualityDTO.LOW);

        List<AccommodationImages> images = mapper.mapImagesByQuality(Arrays.asList(accommodationImageDTO, accommodationImage));

        images.forEach(image -> {
            if (image.getQuality().equals(AccommodationImageQuality.LOW)) {
                assertAccommodationImage(image.getSources().get(0), AccommodationImageQuality.LOW, AccommodationImageType.AREA_INFORMATION, accommodationImage);
            } else {
                assertAccommodationImage(image.getSources().get(0), AccommodationImageQuality.MEDIUM, AccommodationImageType.POOL, accommodationImageDTO);
            }
        });
    }

    @Test
    public void testMapAccommodationImage() {
        AccommodationImage mapped = mapper.mapAccommodationImage(accommodationImageDTO);
        assertAccommodationImage(mapped, AccommodationImageQuality.MEDIUM, AccommodationImageType.POOL, accommodationImageDTO);
    }

    private void assertAccommodationImage(AccommodationImage mapped, AccommodationImageQuality quality,
                                          AccommodationImageType type, AccommodationImageDTO accommodationImageDTO) {
        assertEquals(mapped.getQuality(), quality);
        assertEquals(mapped.getType(), type);
        assertEquals(mapped.getThumbnailUrl(), accommodationImageDTO.getThumbnailUrl());
        assertEquals(mapped.getUrl(), accommodationImageDTO.getUrl());
    }
}
