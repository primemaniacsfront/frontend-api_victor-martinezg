package com.odigeo.frontend.resolvers.accommodation.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationReviewHandler;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AccommodationSearchReviewsHandlerTest {
    @Mock
    private AccommodationReviewHandler accommodationReviewHandler;
    @InjectMocks
    private AccommodationSearchReviewsHandler testClass;

    private static final Integer DEDUPID = 123;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testPopulateReviews() {
        Float rating = 2.5f;
        Integer totalReviews = 5;

        AccommodationSearchResponseDTO response = buildAccommodationSearchResponseDTO();
        HashMap<Integer, AccommodationReview> reviewsByProvider = getReviewsByProvider(rating, totalReviews);
        when(accommodationReviewHandler.getReviewsByProvider(anyList(), anyString())).thenReturn(reviewsByProvider);
        testClass.populateReviews(response);
        AccommodationReview userReview = response.getAccommodationResponse().getAccommodations().get(0).getUserReview();
        assertNotNull(userReview);
        assertEquals(userReview.getDedupId(), DEDUPID);
        assertEquals(userReview.getRating(), rating);
        assertEquals(userReview.getTotalReviews(), totalReviews);
    }

    private HashMap<Integer, AccommodationReview> getReviewsByProvider(Float rating, Integer totalReviews) {
        HashMap<Integer, AccommodationReview> reviewsByProvider = new HashMap<>();
        AccommodationReview value = new AccommodationReview();
        value.setRating(rating);
        value.setTotalReviews(totalReviews);
        value.setDedupId(DEDUPID);
        reviewsByProvider.put(DEDUPID, value);
        return reviewsByProvider;
    }

    private AccommodationSearchResponseDTO buildAccommodationSearchResponseDTO() {
        AccommodationSearchResponseDTO accommodationSearchResponseDTO = new AccommodationSearchResponseDTO();
        accommodationSearchResponseDTO.setAccommodationResponse(buildAccommodationResponse());
        return accommodationSearchResponseDTO;
    }

    private AccommodationResponseDTO buildAccommodationResponse() {
        AccommodationResponseDTO accommodationResponseDTO = new AccommodationResponseDTO();
        accommodationResponseDTO.setAccommodations(Collections.singletonList(buildSearchAccommodation()));
        return accommodationResponseDTO;
    }

    private SearchAccommodationDTO buildSearchAccommodation() {
        SearchAccommodationDTO searchAccommodationDTO = new SearchAccommodationDTO();
        searchAccommodationDTO.setDedupId(DEDUPID);
        return searchAccommodationDTO;
    }
}
