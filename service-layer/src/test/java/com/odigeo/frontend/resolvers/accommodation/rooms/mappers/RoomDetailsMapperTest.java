package com.odigeo.frontend.resolvers.accommodation.rooms.mappers;


import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDetailsDTO;
import com.odigeo.searchengine.v2.responses.accommodation.RoomDeal;
import org.testng.annotations.Test;

import static com.odigeo.frontend.resolvers.accommodation.rooms.mappers.RoomDetailsMapper.JOIN;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class RoomDetailsMapperTest {

    private static final String SEPARATOR = " - ";
    private static final String DESCRIPTION_NAME = "Double Room";
    private static final String DESCRIPTION = "Basic Double Room with Balcony";

    RoomDetailsMapper roomDetailsMapper = new RoomDetailsMapper();

    @Test
    public void testMap() {
        RoomDeal roomDeal = new RoomDeal();
        roomDeal.setDescription(DESCRIPTION_NAME + SEPARATOR + DESCRIPTION);
        RoomDetailsDTO response = roomDetailsMapper.map(roomDeal);
        assertEquals(response.getName(), DESCRIPTION_NAME);
        assertEquals(response.getDescription(), DESCRIPTION);
    }

    @Test
    public void testMapNull() {
        RoomDeal roomDeal = new RoomDeal();
        roomDeal.setDescription(null);
        assertNull(roomDetailsMapper.map(roomDeal));
    }

    @Test
    public void testMapWithoutFullDescription() {
        RoomDeal roomDeal = new RoomDeal();
        roomDeal.setDescription(DESCRIPTION_NAME);

        RoomDetailsDTO response = roomDetailsMapper.map(roomDeal);
        assertEquals(response.getName(), DESCRIPTION_NAME);
        assertNull(response.getDescription());
    }

    @Test
    public void testMapWithoutExtraFullDescription() {
        RoomDeal roomDeal = new RoomDeal();
        roomDeal.setDescription(DESCRIPTION_NAME + SEPARATOR + DESCRIPTION + SEPARATOR + DESCRIPTION);

        RoomDetailsDTO response = roomDetailsMapper.map(roomDeal);
        assertEquals(response.getName(), DESCRIPTION_NAME);
        assertEquals(response.getDescription(), DESCRIPTION + JOIN + DESCRIPTION);
    }
}
