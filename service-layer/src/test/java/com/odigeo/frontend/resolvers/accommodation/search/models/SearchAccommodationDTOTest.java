package com.odigeo.frontend.resolvers.accommodation.search.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ThreeValuedLogic;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.BoardTypeDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;

import java.math.BigDecimal;
import java.util.Collections;

public class SearchAccommodationDTOTest extends BeanTest<SearchAccommodationDTO> {

    @Override
    protected SearchAccommodationDTO getBean() {
        SearchAccommodationDTO bean = new SearchAccommodationDTO();
        bean.setKey("key");
        bean.setDedupId(1);
        bean.setScore(BigDecimal.TEN);
        bean.setPriceCalculator(new AccommodationPriceCalculatorDTO());
        bean.setProviderPrice(new Money());
        bean.setPriceBreakdown(new PriceBreakdownDTO());
        bean.setContentKey("CK");
        bean.setUsersRates(Collections.singletonList(new AccommodationReview()));
        bean.setCancellationFree(ThreeValuedLogic.TRUE);
        bean.setPaymentAtDestination(true);
        bean.setAccommodationDeal(new AccommodationDealInformationDTO());
        bean.setAccommodationFacilities(Collections.singletonList("AF"));
        bean.setUserReview(new AccommodationReview());
        bean.setDistanceFromCityCenter(8d);
        bean.setRoomsLeft(88);
        bean.setBoardType(BoardTypeDTO.AI);
        return bean;
    }
}
