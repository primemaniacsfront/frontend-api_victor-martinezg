package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.odigeo.product.v2.model.Price;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class InsuranceProductMapperTest {

    private static final String CURRENCY_CODE = "EUR";
    private static final BigDecimal AMOUNT = BigDecimal.TEN;
    private static final Price TOTAL_PRICE = new Price();
    private static final String PRODUCT_ID = "productId";
    private static final String POLICY = "policy";

    @Mock
    private MoneyMapper moneyMapper;
    @Mock
    private InsuranceUrlsMapper insuranceUrlsMapper;

    @InjectMocks
    private InsuranceProductMapper insuranceProductMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(moneyMapper.map(Mockito.any(Price.class))).thenReturn(getMoney());
    }

    @Test
    public void map() {
        InsuranceProduct insuranceProduct = insuranceProductMapper.map(getInsuranceProduct());

        assertEquals(insuranceProduct.getProductId(), PRODUCT_ID);
        assertEquals(insuranceProduct.getOfferKey(), POLICY);
        assertEquals(insuranceProduct.getPrice().getAmount(), AMOUNT);
        assertEquals(insuranceProduct.getPrice().getCurrency(), CURRENCY_CODE);
    }

    @Test
    public void completeInsuranceProduct() {
        InsuranceProduct insuranceProductToComplete = new InsuranceProduct();
        InsuranceProduct insuranceProductWithExtraInfo = getInsuranceProductWithExtraInfo();

        insuranceProductMapper.completeInsuranceProduct(insuranceProductToComplete, insuranceProductWithExtraInfo);

        assertEquals(insuranceProductToComplete.getOfferKey(), insuranceProductWithExtraInfo.getOfferKey());
    }

    private InsuranceProduct getInsuranceProductWithExtraInfo() {
        InsuranceProduct insuranceProductWithExtraInfo = new InsuranceProduct();

        insuranceProductWithExtraInfo.setOfferKey(PRODUCT_ID);

        return insuranceProductWithExtraInfo;
    }

    private com.edreamsodigeo.insuranceproduct.api.last.response.InsuranceProduct getInsuranceProduct() {
        com.edreamsodigeo.insuranceproduct.api.last.response.InsuranceProduct insuranceProduct = new com.edreamsodigeo.insuranceproduct.api.last.response.InsuranceProduct();

        insuranceProduct.setPolicy(POLICY);
        insuranceProduct.setId(PRODUCT_ID);
        insuranceProduct.setSellingPrice(TOTAL_PRICE);

        return insuranceProduct;
    }

    private Money getMoney() {
        Money money = new Money();

        money.setAmount(AMOUNT);
        money.setCurrency(CURRENCY_CODE);

        return money;
    }
}
