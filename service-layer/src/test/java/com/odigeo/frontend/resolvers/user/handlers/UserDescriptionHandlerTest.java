package com.odigeo.frontend.resolvers.user.handlers;

import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.User;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class UserDescriptionHandlerTest {

    @Mock
    private UserDescriptionService userService;
    @Mock
    private User user;
    @Mock
    private ResolverContext context;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetUserInfo() {
        VisitInformation visit = mock(VisitInformation.class);
        Long userId = 1L;

        when(context.getVisitInformation()).thenReturn(visit);
        when(userService.getUserByToken(context)).thenReturn(user);
        when(user.getId()).thenReturn(userId);

        User userInfo = userService.getUserByToken(context);
        assertEquals(userInfo.getId(), userId);
    }

    @Test
    public void testGetUserInfoInvalidUser() {
        assertNull(userService.getUserByToken(context));
    }
}
