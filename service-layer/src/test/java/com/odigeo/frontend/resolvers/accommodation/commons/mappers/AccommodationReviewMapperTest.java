package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.odigeo.accommodation.review.retrieve.bean.Review;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class AccommodationReviewMapperTest {

    private AccommodationReviewMapper mapper = new AccommodationReviewMapper();

    @Test
    public void testMap() {
        Review review = getReview();
        AccommodationReview dto = mapper.map(review);

        assertEquals(dto.getDedupId(), review.getDedupId());
        assertEquals(dto.getProviderCode(), review.getProviderCode());
        assertEquals(dto.getProviderReviewId(), review.getProviderReviewId());
        assertEquals(dto.getRating().floatValue(), review.getRating() * 0.05);
        assertEquals(dto.getTotalReviews(), review.getTotalReviews());
    }

    @Test
    public void testMapList() {
        Review review = getReview();

        Map<Integer, AccommodationReview> dtoMap = mapper.mapList(Collections.singletonList(review));
        AccommodationReview dto = dtoMap.get(review.getDedupId());

        assertEquals(dto.getDedupId(), review.getDedupId());
        assertEquals(dto.getProviderCode(), review.getProviderCode());
        assertEquals(dto.getProviderReviewId(), review.getProviderReviewId());
        assertEquals(dto.getRating().floatValue(), review.getRating() * 0.05);
        assertEquals(dto.getTotalReviews(), review.getTotalReviews());
    }

    @Test
    public void testMapNullValues() {
        AccommodationReview dto = mapper.map(new Review());
        assertNull(dto.getDedupId());
        assertNull(dto.getProviderCode());
        assertNull(dto.getProviderReviewId());
        assertNull(dto.getRating());
        assertNull(dto.getTotalReviews());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testMapNullObject() {
        AccommodationReview dto = mapper.map(null);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testMapListNullObject() {
        Map<Integer, AccommodationReview> dtoMap = mapper.mapList(null);
    }

    private Review getReview() {
        Review review = new Review();
        review.setDedupId(123);
        review.setProviderCode("TA");
        review.setProviderReviewId("345");
        review.setRating(90.0f);
        review.setTotalReviews(10);
        return review;
    }

}
