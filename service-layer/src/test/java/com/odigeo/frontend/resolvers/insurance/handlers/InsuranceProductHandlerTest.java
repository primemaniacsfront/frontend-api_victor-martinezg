package com.odigeo.frontend.resolvers.insurance.handlers;

import com.edreamsodigeo.insuranceproduct.api.last.request.SelectRequest;
import com.edreamsodigeo.insuranceproduct.api.last.response.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectInsuranceOfferRequest;
import com.odigeo.frontend.resolvers.insurance.mappers.InsuranceProductMapper;
import com.odigeo.frontend.resolvers.insurance.mappers.SelectRequestMapper;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ProductIdMapper;
import com.odigeo.frontend.services.insuranceproduct.InsuranceProductService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class InsuranceProductHandlerTest {

    private static final String PRODUCT_ID = "productId";
    private static final InsuranceProduct INSURANCE_PRODUCT_MAPPED = new InsuranceProduct();
    private static final SelectRequest SELECT_REQUEST_MAPPED = new SelectRequest();
    private static final ProductType INSURANCE = ProductType.INSURANCE;

    @Mock
    private InsuranceProductService insuranceProductService;
    @Mock
    private SelectRequestMapper selectRequestMapper;
    @Mock
    private InsuranceProductMapper insuranceProductMapper;
    @Mock
    private ProductIdMapper productIdMapper;

    @InjectMocks
    private InsuranceProductHandler insuranceProductHandler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(selectRequestMapper.map(any(SelectInsuranceOfferRequest.class)))
                .thenReturn(SELECT_REQUEST_MAPPED);
        when(insuranceProductMapper.map(any(com.edreamsodigeo.insuranceproduct.api.last.response.InsuranceProduct.class)))
                .thenReturn(INSURANCE_PRODUCT_MAPPED);
        doNothing().when(insuranceProductMapper).completeInsuranceProduct(any(), any());
        ProductId productId = new ProductId();
        productId.setId(PRODUCT_ID);
        when(insuranceProductService.selectOffer(any())).thenReturn(productId);
        when(insuranceProductService.getInsuranceProduct(anyString())).thenReturn(new com.edreamsodigeo.insuranceproduct.api.last.response.InsuranceProduct());
        when(productIdMapper.map(any(), any())).thenReturn(getProductId());
    }

    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId getProductId() {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId productId = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId();
        productId.setId(PRODUCT_ID);
        productId.setType(INSURANCE);
        return productId;
    }

    @Test
    public void selectInsuranceOfferRequest() {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId productId = insuranceProductHandler.selectInsuranceOfferRequest(new SelectInsuranceOfferRequest());

        assertSame(PRODUCT_ID, productId.getId());
        verify(insuranceProductService).selectOffer(any());
    }

    @Test
    public void getInsuranceProduct() {
        InsuranceProduct insuranceProduct = insuranceProductHandler.getInsuranceProduct(PRODUCT_ID);

        assertSame(INSURANCE_PRODUCT_MAPPED, insuranceProduct);
        verify(insuranceProductService).getInsuranceProduct(anyString());
    }

    @Test
    public void completeInsuranceProduct() {
        insuranceProductHandler.completeInsuranceProduct(new InsuranceProduct());
    }

}
