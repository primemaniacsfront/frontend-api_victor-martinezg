package com.odigeo.frontend.resolvers.insurance.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferType;
import com.odigeo.frontend.resolvers.insurance.mappers.InsuranceOffersMapper;
import com.odigeo.frontend.resolvers.insurance.mappers.PostBookingOfferRequestMapper;
import com.odigeo.frontend.services.insurance.InsuranceService;
import com.odigeo.insurance.api.last.insurance.InsuranceOffers;
import com.odigeo.insurance.api.last.request.PostBookingOfferRequest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class InsuranceHandlerTest {

    private static final String VISIT_INFORMATION_SERIALIZED = "visitInformationSerialized";
    private static final long BOOKING_ID = 1L;
    private static final  InsuranceOfferType OFFER_INSURANCE = new InsuranceOfferType();
    private static final List<InsuranceOfferType> OFFER_PRODUCTS = Collections.singletonList(OFFER_INSURANCE);

    @Mock
    private InsuranceService insuranceApiService;
    @Mock
    private InsuranceOffersMapper insuranceOffersMapper;
    @Mock
    private PostBookingOfferRequestMapper postBookingOfferRequestMapper;

    @InjectMocks
    private InsuranceHandler insuranceHandler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        PostBookingOfferRequest postBookingOfferRequest = new PostBookingOfferRequest();
        when(postBookingOfferRequestMapper.map(BOOKING_ID, VISIT_INFORMATION_SERIALIZED))
            .thenReturn(postBookingOfferRequest);

        InsuranceOffers insuranceOffers = new InsuranceOffers();
        when(insuranceApiService.getOffers(postBookingOfferRequest))
            .thenReturn(insuranceOffers);

        when(insuranceOffersMapper.map(insuranceOffers))
            .thenReturn(OFFER_PRODUCTS);
    }

    @Test
    public void getInsurances() {
        assertSame(insuranceHandler.getInsuranceOffers(BOOKING_ID, VISIT_INFORMATION_SERIALIZED), OFFER_PRODUCTS);
    }
}
