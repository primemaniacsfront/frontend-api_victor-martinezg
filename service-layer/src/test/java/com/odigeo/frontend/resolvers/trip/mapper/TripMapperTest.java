package com.odigeo.frontend.resolvers.trip.mapper;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BookingStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CabinClass;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Traveller;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerGender;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Trip;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripItinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripLeg;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripSection;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class TripMapperTest {
    private static final String TRIP_RESPONSE = "com/odigeo/frontend/resolvers/trip/tripResponse.json";

    private static final String BOOKING_ID = "7651251015";
    private static final String EMAIL = "test@edreamsodigeo.com";
    private static final BookingStatus BOOKING_STATUS = BookingStatus.CONTRACT;
    private static final String CHECKIN_PRODUCT = "AC_AUTO_CHECKIN";
    private static final String BUYER_NAME = "Pepe";
    private static final String BUYER_LAST_NAME = "Garcia";
    private static final String TRAVELLER_TYPE = "ADULT";
    private static final String DEPARTURE_CITY = "Ámsterdam";
    private static final String DEPARTURE_COUNTRY_CODE = "NL";
    private static final String DEPARTURE = "Schiphol";
    private static final String DESTINATION = "John F Kennedy Intl Airport";
    private static final String DESTINATION_CITY = "Nueva York";
    private static final String DESTINATION_COUNTRY_CODE = "US";
    private static final Long ARRIVAL_DATE = 1658347200000L;
    private static final Long DEPARTURE_DATE = 1658325300000L;
    private static final Long CREATION_DATE = 1628603455000L;
    private static final CabinClass CABIN_CLASS = CabinClass.ECONOMIC_DISCOUNTED;
    private static final TransportType TRANSPORT_TYPE = TransportType.PLANE;

    private final TripMapper tripMapper = new TripMapperImpl();


    @Test
    public void tripMapperShouldMapCorrectly() throws IOException {
        BookingDetail bookingDetail = this.createTripResponse(TRIP_RESPONSE);
        Trip trip = tripMapper.mapToTripContract(bookingDetail);
        assertNotNull(trip);
        checkTripResponse(trip);
    }

    private BookingDetail createTripResponse(String filePath) throws IOException {
        InputStream json = this.getClass().getClassLoader().getResourceAsStream(filePath);
        return new ObjectMapper().readValue(json, BookingDetail.class);
    }

    private void checkTripResponse(Trip trip) {
        assertEquals(trip.getId(), BOOKING_ID);
        assertEquals(trip.getBookingStatus(), BOOKING_STATUS);
        assertEquals(trip.getAdditions().get(0).getOfferId(), CHECKIN_PRODUCT);
        assertEquals(trip.getBuyer().getName(), BUYER_NAME);
        assertEquals(trip.getBuyer().getMail(), EMAIL);
        assertEquals(Long.valueOf(trip.getCreationDate()), CREATION_DATE);
        checkTraveller(trip.getTravellers());
        checkServiceOptions(trip.getServiceOptions());
        checkItinerary(trip.getItinerary());
        checkSections(trip.getItinerary());
    }

    private void checkServiceOptions(List<NonEssentialProduct> serviceOptions) {
        assertEquals(serviceOptions.size(), 4);
        assertEquals(serviceOptions.get(0).getOfferId(), "STATUS");
        assertEquals(serviceOptions.get(1).getOfferId(), "FLIGHT_NOTIFICATION");
        assertEquals(serviceOptions.get(2).getOfferId(), "CP_CONTACT_CENTRE_REDUCED_RATE_PHONE_NUMBER");
        assertEquals(serviceOptions.get(3).getOfferId(), "CC_FREE_CANCELLATION_OR_MODIFICATION");
    }

    private void checkTraveller(List<Traveller> traveller) {
        assertEquals(traveller.size(), 2);
        assertEquals(traveller.get(0).getName(), BUYER_NAME);
        assertEquals(traveller.get(0).getFirstLastName(), BUYER_LAST_NAME);
        assertEquals(traveller.get(0).getTravellerType(), TRAVELLER_TYPE);
        assertEquals(traveller.get(1).getName(), "Bruno");
        assertEquals(traveller.get(1).getFirstLastName(), "Agostini");
        assertEquals(traveller.get(1).getTravellerGender(), TravellerGender.MALE);
    }

    private void checkItinerary(TripItinerary itinerary) {
        assertNotNull(itinerary);
        assertEquals(itinerary.getOrigin().getName(), DEPARTURE);
        assertEquals(itinerary.getOrigin().getCityName(), DEPARTURE_CITY);
        assertEquals(itinerary.getOrigin().getCountryCode(), DEPARTURE_COUNTRY_CODE);
        assertEquals(itinerary.getDestination().getName(), DESTINATION);
        assertEquals(itinerary.getDestination().getCityName(), DESTINATION_CITY);
        assertEquals(itinerary.getDestination().getCityName(), DESTINATION_CITY);
        assertEquals(itinerary.getDestination().getCountryCode(), DESTINATION_COUNTRY_CODE);
        assertEquals(Long.valueOf(itinerary.getDepartureDate()), DEPARTURE_DATE);
        assertEquals(Long.valueOf(itinerary.getArrivalDate()), ARRIVAL_DATE);
    }

    private void checkSections(TripItinerary itinerary) {
        TripLeg leg = itinerary.getLegs().get(0);
        List<TripSection> sections = leg.getSections();
        assertNotNull(leg);
        assertEquals(leg.getSections().size(), 2);
        assertEquals(sections.size(), 2);
        assertEquals(sections.get(0).getCabinClass(), CABIN_CLASS);
        assertEquals(sections.get(0).getTransportType(), TRANSPORT_TYPE);
        assertEquals(sections.get(0).getDeparture().getName(), DEPARTURE);
        assertEquals(sections.get(0).getDeparture().getCityName(), DEPARTURE_CITY);
        assertEquals(sections.get(0).getFlightCode(), "TP673");
        assertEquals(sections.get(1).getCabinClass(), CABIN_CLASS);
        assertEquals(sections.get(1).getTransportType(), TRANSPORT_TYPE);
        assertEquals(sections.get(1).getArrival().getCityName(), DESTINATION_CITY);
        assertEquals(sections.get(1).getArrival().getName(), DESTINATION);
        assertEquals(sections.get(1).getFlightCode(), "TP209");
    }
}
