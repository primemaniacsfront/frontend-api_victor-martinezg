package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceUrl;
import com.odigeo.insurance.api.last.insurance.ConditionsUrl;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class InsuranceUrlsMapperTest {

    private static final InsuranceUrl INSURANCE_URL = new InsuranceUrl();

    @Mock
    private InsuranceUrlMapper insuranceUrlMapper;

    @InjectMocks
    private InsuranceUrlsMapper insuranceUrlsMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(insuranceUrlMapper.map(any())).thenReturn(INSURANCE_URL);
    }

    @Test
    public void map() {
        List<ConditionsUrl> conditionsUrls = Arrays.asList(new ConditionsUrl(), new ConditionsUrl());

        List<InsuranceUrl> insuranceUrls = insuranceUrlsMapper.map(conditionsUrls);

        assertEquals(insuranceUrls.size(), conditionsUrls.size());
        assertEquals(insuranceUrls.get(0), INSURANCE_URL);
    }

}
