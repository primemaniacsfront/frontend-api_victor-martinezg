package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Price;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerception;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.PricePerceptionType;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class NonEssentialProductPriceMapperTest {

    private static final String CURRENCY = "EUR";
    private static final BigDecimal AMOUNT = BigDecimal.TEN;

    @Mock
    private PricePerceptionMapper pricePerceptionMapper;
    @Mock
    private AncillaryConfiguration ancillaryConfiguration;
    @InjectMocks
    private NonEssentialProductPriceMapper mapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void mapTest() {
        when(ancillaryConfiguration.getPricePerception()).thenReturn(PricePerceptionType.PERPAX);
        PricePerception pricePerception = new PricePerception();
        when(pricePerceptionMapper.map(PricePerceptionType.PERPAX)).thenReturn(pricePerception);

        Price price = mapper.map(AMOUNT, CURRENCY, ancillaryConfiguration);

        assertEquals(price.getPerception(), pricePerception);
        assertEquals(price.getValue().getAmount(), AMOUNT);
        assertEquals(price.getValue().getCurrency(), CURRENCY);
    }

}
