package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.onefront.OFShoppingCartIntegration;
import com.odigeo.frontend.resolvers.search.handlers.campaign.CampaignConfigHandler;
import com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering.AirlineSteeringHandler;
import com.odigeo.frontend.resolvers.search.handlers.farefamily.FareFamilyHandler;
import com.odigeo.frontend.resolvers.search.handlers.helper.MirFootprintItineraryHandler;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.frontend.resolvers.search.handlers.legend.SegmentMapper;
import com.odigeo.frontend.resolvers.search.handlers.legend.TransportHandler;
import com.odigeo.frontend.resolvers.search.handlers.pricing.AbstractPricePolicy;
import com.odigeo.frontend.resolvers.search.handlers.pricing.PerksHandler;
import com.odigeo.frontend.resolvers.search.handlers.pricing.PriceHandler;
import com.odigeo.frontend.resolvers.search.handlers.pricing.PricePolicyFactory;
import com.odigeo.frontend.resolvers.search.handlers.vin.VinHandler;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.error.MessageResponse;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.FareItineraryPriceCalculator;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerarySearchResults;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class SearchResponseHandlerTest {

    private static final String ITINERARY_INDEX_SEPARATOR = ",";
    private static final String ITINERARY_UNIQUE_INDEX_SEPARATOR = "_";

    private static final Integer ITINERARY_INDEX = 0;
    private static final Integer ITINERARIES_LINK = 1;
    private static final String ITINERARY_KEY = ITINERARY_INDEX + ITINERARY_INDEX_SEPARATOR + "VY";

    @Mock
    private SearchResponse searchResponse;
    @Mock
    private SearchRequest searchRequest;
    @Mock
    private VisitInformation visit;
    @Mock
    private ItinerarySearchResults itinerarySearchResults;
    @Mock
    private FareItineraryPriceCalculator price;
    @Mock
    private BaggageHandler baggageHandler;
    @Mock
    private TicketHandler ticketHandler;
    @Mock
    private PriceHandler priceHandler;
    @Mock
    private PricePolicyFactory pricePolicyFactory;
    @Mock
    private FeeType feeType;
    @Mock
    private AbstractPricePolicy abstractPricePolicy;
    @Mock
    private MirFootprintItineraryHandler mirFootprintItineraryHandler;
    @Mock
    private AirlineSteeringHandler airlineSteeringHandler;
    @Mock
    private OFShoppingCartIntegration ofShoppingCartIntegration;
    @Mock
    private HotelXSellingHandler hotelXSellingHandler;
    @Mock
    private SegmentMapper segmentMapper;
    @Mock
    private SearchEngineContextBuilder seContextBuilder;
    @Mock
    private SearchEngineContext seContext;
    @Mock
    private RebookingHandler rebookingHandler;
    @Mock
    private VinHandler vinHandler;
    @Mock
    private FareFamilyHandler fareFamilyHandler;
    @Mock
    private ResolverContext context;
    @Mock
    private SearchService searchService;
    @Mock
    private TransportHandler transportHandler;
    @Mock
    private CampaignConfigHandler campaignConfigHandler;
    @Mock
    private PerksHandler perksHandler;
    @Mock
    private MetasHandler metasHandler;
    @Mock
    private SiteVariations siteVariations;

    private Map<Integer, SegmentDTO> segmentMap;

    @InjectMocks
    private SearchResponseHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        segmentMap = new HashMap<>();

        when(searchResponse.getItinerarySearchResults()).thenReturn(itinerarySearchResults);
        when(seContextBuilder.build(itinerarySearchResults)).thenReturn(seContext);
        when(segmentMapper.buildSegmentMap(seContext)).thenReturn(segmentMap);
        when(pricePolicyFactory.getPricePolicy(context)).thenReturn(abstractPricePolicy);
        when(abstractPricePolicy.calculateDefaultFeeType()).thenReturn(feeType);

        ItineraryRequest itineraryRequestDTO = mock(ItineraryRequest.class);
        when(searchRequest.getItinerary()).thenReturn(itineraryRequestDTO);

        when(context.getVisitInformation()).thenReturn(visit);

        when(siteVariations.isPrimeDayCampaignEnabled(visit)).thenReturn(true);
        when(siteVariations.isAirlineCampaignEnabled(visit)).thenReturn(true);
    }

    @Test
    public void testBuildSearchResponse() {
        Long searchId = 1L;
        String currencyCode = "currency";
        Set<String> carriersWithRebooking = new HashSet<>(0);

        when(seContext.getCurrency()).thenReturn(currencyCode);
        when(itinerarySearchResults.getItineraryResults()).thenReturn(Collections.emptyList());
        when(searchResponse.getSearchId()).thenReturn(searchId);
        when(rebookingHandler.isRebookingAvailable(searchRequest, visit)).thenReturn(Boolean.TRUE);
        when(rebookingHandler.getCarriersWithRebooking(searchRequest, visit)).thenReturn(carriersWithRebooking);

        MessageResponse messageResponse = mock(MessageResponse.class);
        String messageCode = "code";
        String messageDesc = "message";
        when(searchService.getSearchMessage(searchResponse)).thenReturn(messageResponse);
        when(messageResponse.getCode()).thenReturn(messageCode);
        when(messageResponse.getDescription()).thenReturn(messageDesc);

        SearchResponseDTO searchResponseDTO = handler.buildSearchResponse(searchResponse, searchRequest, context);

        assertEquals(searchResponseDTO.getCurrencyCode(), currencyCode);
        assertEquals(searchResponseDTO.getSearchId(), searchId);
        assertTrue(searchResponseDTO.getItineraries().isEmpty());
        assertTrue(searchResponseDTO.getIsRebookingAvailable());
        assertEquals(searchResponseDTO.getCarriersWithRebooking(), carriersWithRebooking);
        assertEquals(searchResponseDTO.getSearchCode().getCode(), messageCode);
        assertEquals(searchResponseDTO.getSearchCode().getMessage(), messageDesc);
        assertEquals(searchResponseDTO.getDefaultFeeType(), feeType);

        verify(mirFootprintItineraryHandler).addMirRatingAndFootprintInfoItinerary(searchRequest, searchResponseDTO, context);
        verify(airlineSteeringHandler).boostingAirline(searchResponseDTO, context);
        verify(metasHandler).populateExternalSelection(searchResponse, searchResponseDTO);
    }

    @Test
    public void testBuildSearchResponseEmpty() {
        long searchId = 0;
        when(searchResponse.getSearchId()).thenReturn(searchId);
        when(searchResponse.getItinerarySearchResults()).thenReturn(null);

        SearchResponseDTO searchResponseDTO = handler.buildSearchResponse(searchResponse, searchRequest, context);

        assertEquals(searchResponseDTO.getSearchId(), new Long(searchId));
        assertTrue(searchResponseDTO.getItineraries().isEmpty());
        assertNull(searchResponseDTO.getCurrencyCode());
        assertNull(searchResponseDTO.getDefaultFeeType());
        assertNull(searchResponseDTO.getSearchCode());
    }

    @Test
    public void testBuildItineraryResponse() {
        BigDecimal sorPrice = BigDecimal.ONE;
        Integer segmentId = 0;
        Integer segmentKey = 0;
        BigDecimal passengers = BigDecimal.valueOf(3);

        FareItinerary fareItinerary = createFareItinerary();
        SegmentDTO segmentDTO = mock(SegmentDTO.class);

        when(fareItinerary.getFirstSegments()).thenReturn(Collections.singletonList(segmentId));
        when(price.getSortPrice()).thenReturn(sorPrice);
        when(searchRequest.getItinerary().getNumAdults()).thenReturn(1);
        when(searchRequest.getItinerary().getNumChildren()).thenReturn(1);
        when(searchRequest.getItinerary().getNumInfants()).thenReturn(1);

        segmentMap.put(segmentId, segmentDTO);

        when(ofShoppingCartIntegration.buildSegmentKeysMap(fareItinerary))
                .thenReturn(Collections.singletonMap(segmentId, segmentKey));

        List<Fee> itineraryFees = new ArrayList<>(0);
        when(priceHandler.calculateItineraryPrices(seContext, fareItinerary, passengers, abstractPricePolicy)).thenReturn(itineraryFees);

        Set<TransportType> transportTypes = new HashSet<>(0);
        when(transportHandler.calculateItineraryTransportTypes(argThat(itineraryLegs -> itineraryLegs.get(0).getSegments().get(0) == segmentDTO)))
                .thenReturn(transportTypes);

        Integer ticketsLeft = 2;
        when(ticketHandler.calculateSeatsLeft(argThat(itineraryLegs -> itineraryLegs.get(0).getSegments().get(0) == segmentDTO)))
                .thenReturn(ticketsLeft);

        Integer freeCancellationId = 1;
        ZonedDateTime freeCancellation = mock(ZonedDateTime.class);
        when(seContext.getFreeCancellationMap())
                .thenReturn(Collections.singletonMap(freeCancellationId, freeCancellation));
        when(fareItinerary.getFreeCancellationLimit()).thenReturn(freeCancellationId);

        SearchResponseDTO searchResponseDTO = handler.buildSearchResponse(searchResponse, searchRequest, context);

        List<SearchItineraryDTO> itineraryDTOList = searchResponseDTO.getItineraries();
        SearchItineraryDTO itinerary = itineraryDTOList.get(0);
        LegDTO legDTO = itinerary.getLegs().get(0);
        List<SegmentDTO> segmentDTOList = legDTO.getSegments();

        assertEquals(searchResponseDTO.getItineraries().size(), 1);
        assertEquals(itinerary.getId(), ITINERARY_KEY + ITINERARY_UNIQUE_INDEX_SEPARATOR + "0");
        assertEquals(itinerary.getKey(), ITINERARY_KEY);
        assertEquals(itinerary.getItinerariesLink(), ITINERARIES_LINK);
        assertEquals(itinerary.getIndex(), ITINERARY_INDEX);
        assertEquals(itinerary.getFreeCancellation(), freeCancellation);
        assertEquals(itinerary.getSortPrice(), sorPrice);
        assertEquals(itinerary.getTicketsLeft(), ticketsLeft);
        assertEquals(itinerary.getTransportTypes(), transportTypes);
        assertSame(itinerary.getFees(), itineraryFees);

        assertEquals(itinerary.getLegs().size(), 1);
        assertEquals(legDTO.getSegmentKeys().get(segmentId), segmentKey);

        assertEquals(segmentDTOList.size(), 1);
        assertEquals(segmentDTOList.get(0), segmentDTO);

        verify(rebookingHandler).populateHasFreeRebooking(itinerary, searchRequest, visit);
        verify(rebookingHandler).populateHasReliableCarriers(itinerary);
        verify(rebookingHandler).populateHasRefundCarriers(itinerary);
        verify(fareFamilyHandler).populateIsAvailable(itinerary, visit);
        verify(hotelXSellingHandler).populateIsEnabled(itinerary, searchRequest, visit);
        verify(baggageHandler).populateItineraryBaggageConditions(eq(seContext), eq(fareItinerary), any());

        verify(baggageHandler).populateSegmentBaggageConditions(segmentMap);
        verify(vinHandler).populateVinInfo(itineraryDTOList, seContext);
        verify(campaignConfigHandler).populateCampaignConfig(itinerary, visit);
        verify(perksHandler).populatePerks(seContext, fareItinerary, passengers.intValue());
    }

    @Test
    public void testBuildItineraryResponseOneSegmentPerLeg() {
        FareItinerary fareItinerary = createFareItinerary();
        SegmentDTO segmentDTO0 = mock(SegmentDTO.class);
        SegmentDTO segmentDTO1 = mock(SegmentDTO.class);
        SegmentDTO segmentDTO2 = mock(SegmentDTO.class);
        Integer segmentId0 = 0;
        Integer segmentId1 = 1;
        Integer segmentId2 = 2;

        when(fareItinerary.getFirstSegments()).thenReturn(Collections.singletonList(segmentId0));
        when(fareItinerary.getSecondSegments()).thenReturn(Arrays.asList(segmentId1, segmentId2));
        segmentMap.put(segmentId0, segmentDTO0);
        segmentMap.put(segmentId1, segmentDTO1);
        segmentMap.put(segmentId2, segmentDTO2);

        SearchResponseDTO searchResponseDTO = handler.buildSearchResponse(searchResponse, searchRequest, context);
        List<SearchItineraryDTO> itineraryDTOList = searchResponseDTO.getItineraries();
        List<LegDTO> legDTOList0 = itineraryDTOList.get(0).getLegs();
        List<LegDTO> legDTOList1 = itineraryDTOList.get(1).getLegs();

        assertEquals(itineraryDTOList.size(), 2);
        assertEquals(legDTOList0.size(), 2);
        assertEquals(legDTOList1.size(), 2);

        legDTOList0.forEach(legDTO -> assertEquals(legDTO.getSegments().size(), 1));
        legDTOList1.forEach(legDTO -> assertEquals(legDTO.getSegments().size(), 1));

        assertEquals(legDTOList0.get(0).getSegments().get(0), segmentDTO0);
        assertEquals(legDTOList1.get(0).getSegments().get(0), segmentDTO0);

        SegmentDTO segmentDTO01 = legDTOList0.get(1).getSegments().get(0);
        SegmentDTO segmentDTO11 = legDTOList1.get(1).getSegments().get(0);

        assertTrue(segmentDTO01 == segmentDTO1 && segmentDTO11 == segmentDTO2
                || segmentDTO01 == segmentDTO2 && segmentDTO11 == segmentDTO1);
    }

    private FareItinerary createFareItinerary() {
        FareItinerary fareItinerary = mock(FareItinerary.class);

        when(seContext.getItineraryResults()).thenReturn(Collections.singletonList(fareItinerary));
        when(fareItinerary.getKey()).thenReturn(ITINERARY_KEY);
        when(fareItinerary.getItinerary()).thenReturn(ITINERARIES_LINK);
        when(fareItinerary.getPrice()).thenReturn(price);
        when(fareItinerary.getFirstSegments()).thenReturn(Collections.emptyList());
        when(fareItinerary.getSecondSegments()).thenReturn(Collections.emptyList());
        when(fareItinerary.getThirdSegments()).thenReturn(Collections.emptyList());
        when(fareItinerary.getFourthSegments()).thenReturn(Collections.emptyList());
        when(fareItinerary.getFifthSegments()).thenReturn(Collections.emptyList());
        when(fareItinerary.getSixthSegments()).thenReturn(Collections.emptyList());

        return fareItinerary;
    }

}
