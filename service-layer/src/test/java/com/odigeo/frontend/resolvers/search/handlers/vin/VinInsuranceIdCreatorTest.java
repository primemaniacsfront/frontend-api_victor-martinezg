package com.odigeo.frontend.resolvers.search.handlers.vin;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsurancePolicy;
import com.odigeo.frontend.contract.ancillaries.InsuranceOfferDTO;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

public class VinInsuranceIdCreatorTest {

    private VinInsuranceIdCreator creator;

    @BeforeMethod
    public void init() {
        creator = new VinInsuranceIdCreator();
    }

    @Test
    public void testIdCreation() {
        InsuranceOfferDTO insurance1 = createInsurance("url1", mock(InsurancePolicy.class));
        InsuranceOfferDTO insurance2 = createInsurance("url2", mock(InsurancePolicy.class));

        creator.createInsuranceId(insurance1);
        creator.createInsuranceId(insurance2);

        assertNotNull(insurance1.getId());
        assertNotNull(insurance2.getId());
        assertNotEquals(insurance1.getId(), insurance2.getId());
    }

    @Test
    public void testIdRecovery() {
        String url = "url";
        InsurancePolicy policy = mock(InsurancePolicy.class);
        InsuranceOfferDTO insurance1 = createInsurance(url, policy);
        InsuranceOfferDTO insurance2 = createInsurance(url, policy);

        creator.createInsuranceId(insurance1);
        creator.createInsuranceId(insurance2);

        assertNotNull(insurance1.getId());
        assertNotNull(insurance2.getId());
        assertEquals(insurance1.getId(), insurance2.getId());
    }

    private InsuranceOfferDTO createInsurance(String url, InsurancePolicy policy) {
        InsuranceOfferDTO insurance = new InsuranceOfferDTO();
        insurance.setUrl(url);
        insurance.setPolicy(policy);

        return insurance;
    }

}
