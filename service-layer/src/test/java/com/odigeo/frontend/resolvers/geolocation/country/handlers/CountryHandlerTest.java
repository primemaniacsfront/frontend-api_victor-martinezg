package com.odigeo.frontend.resolvers.geolocation.country.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetCountriesRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetCountriesResponse;
import com.odigeo.frontend.resolvers.geolocation.country.mappers.CountryMapper;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.GeoServiceException;
import com.odigeo.geoapi.v4.responses.Country;
import com.odigeo.geoapi.v4.utils.LocalizedText;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CountryHandlerTest {
    private static final int ID = 30;
    private static final String NAME = "SPAIN";
    private static final String CODE = "30";
    private static final String PHONE_PREFIX = "+34";
    public static final String LOCALE = "es_ES";

    @InjectMocks
    private CountryHandler countryHandler;
    @Mock
    private GeoService geoService;
    @Mock
    private LocalizedText name;
    @Mock
    private CountryMapper countryMapper;
    @Mock
    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country mappedCountry;
    @Mock
    private GetCountriesRequest getCountriesRequest;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(name.getText(any(Locale.class))).thenReturn(NAME);
        when(countryMapper.mapModelToContract(any(), any())).thenReturn(mappedCountry);
        when(getCountriesRequest.getLocale()).thenReturn(LOCALE);
    }

    @Test
    public void testGetCountriesRequest() throws GeoServiceException {
        setUpGeoServiceResponse();
        setUpMappedCountry();
        GetCountriesResponse getCountriesResponse = countryHandler.getCountries(getCountriesRequest);
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country country = getCountriesResponse.getCountries().get(0);
        assertEquals(country.getId(), ID);
        assertEquals(country.getName(), NAME);
        assertEquals(country.getPhonePrefix(), PHONE_PREFIX);
        assertEquals(country.getCode(), CODE);
    }

    private void setUpGeoServiceResponse() throws GeoServiceException {
        List<Country> countries = new ArrayList<>();
        Country country = new Country();
        country.setGeoNodeId(ID);
        country.setName(name);
        country.setCountryCode(CODE);
        country.setPhonePrefix(PHONE_PREFIX);
        countries.add(country);
        when(geoService.getCountries()).thenReturn(countries);
    }

    private void setUpMappedCountry() {
        when(mappedCountry.getId()).thenReturn(ID);
        when(mappedCountry.getName()).thenReturn(NAME);
        when(mappedCountry.getPhonePrefix()).thenReturn(PHONE_PREFIX);
        when(mappedCountry.getCode()).thenReturn(CODE);
    }
}
