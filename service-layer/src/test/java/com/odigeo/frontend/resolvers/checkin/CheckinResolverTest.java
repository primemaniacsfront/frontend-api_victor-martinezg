package com.odigeo.frontend.resolvers.checkin;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinAvailability;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateCheckinResponse;
import com.odigeo.checkin.api.v1.exceptions.AutomaticCheckInException;
import com.odigeo.checkin.api.v1.model.CheckinNotAvailableException;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.checkin.models.CheckinQuery;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class CheckinResolverTest {

    private static final String CHECKIN_STATUS_QUERY = "checkinStatus";
    private static final String CHECKIN_AVAILABILITY_QUERY = "checkinAvailability";
    private static final String CREATE_CHECKIN_MUTATION = "createCheckinRequest";

    private static final String BOOKINGID_FIELD = "bookingID";
    private static final String CITIZENSHIP_FIELD = "citizenship";
    private static final long BOOKINGID = 1234567890L;
    private static final String CITIZENSHIP = "ES";

    @Mock
    private CheckinHandler checkinHandler;
    @Mock
    private JsonUtils jsonUtils;

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visitInfo;
    @Mock
    private CheckinAvailability availability;
    @Mock
    private CheckinStatus status;
    @Mock
    private CreateCheckinResponse createCheckinResponse;
    @Mock
    private CheckinRequest checkinRequest;
    @InjectMocks
    private CheckinResolver checkinResolver;

    private CheckinQuery checkinQuery;


    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visitInfo);
        when(jsonUtils.fromDataFetching(env, Long.class, BOOKINGID_FIELD)).thenReturn(BOOKINGID);
        when(jsonUtils.fromDataFetching(env, String.class, CITIZENSHIP_FIELD)).thenReturn(CITIZENSHIP);
        when(jsonUtils.fromDataFetching(env, CheckinRequest.class)).thenReturn(checkinRequest);

        checkinQuery = new CheckinQuery(BOOKINGID, CITIZENSHIP);
    }

    @Test
    public void testAddToBuilderQueries() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        checkinResolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(CHECKIN_STATUS_QUERY));
        assertNotNull(queries.get(CHECKIN_AVAILABILITY_QUERY));
    }

    @Test
    public void testAddToBuilderMutations() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        checkinResolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> mutations = runtimeWiring.getDataFetchers().get("Mutation");

        assertNotNull(mutations.get(CREATE_CHECKIN_MUTATION));
    }

    @Test
    public void testCheckinAvailabilityFetcher() throws AutomaticCheckInException {
        when(checkinHandler.getCheckinAvailability(any(CheckinQuery.class))).thenReturn(availability);
        assertSame(checkinResolver.checkinAvailabilityFetcher(env), availability);
    }

    @Test
    public void testCheckinStatusFetcher() throws AutomaticCheckInException, CheckinNotAvailableException {
        when(checkinHandler.getCheckinStatus(any(CheckinQuery.class), any(VisitInformation.class))).thenReturn(status);
        assertSame(checkinResolver.checkinStatusFetcher(env), status);
    }

    @Test
    public void testCreateCheckinFetcher() {
        when(checkinHandler.createCheckin(any(CheckinRequest.class), any(VisitInformation.class))).thenReturn(createCheckinResponse);
        assertSame(checkinResolver.createCheckinFetcher(env), createCheckinResponse);
    }
}
