package com.odigeo.frontend.resolvers.accommodation.search;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchResponse;
import com.odigeo.accommodation.scoring.service.ScoringServiceException;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.accommodation.search.handlers.AccommodationSearchHandler;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.accommodation.search.AccommodationSearchResolver.SEARCH_TYPE;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;


public class AccommodationSearchResolverTest {

    private static final String SEARCH_ACCOMMODATION_QUERY = "searchAccommodation";

    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private AccommodationSearchHandler accommodationSearchHandler;

    @InjectMocks
    private AccommodationSearchResolver resolver;


    @BeforeMethod
    public void init() throws ScoringServiceException {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(resolverContext);
        when(env.getGraphQlContext()).thenReturn(context);
        when(resolverContext.getVisitInformation()).thenReturn(visit);
        when(context.get(MetricsHandler.class)).thenReturn(metricsHandler);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(SEARCH_ACCOMMODATION_QUERY));
    }

    @Test
    public void testSearchAccommodationFetcher() {
        AccommodationSearchResponse response = mock(AccommodationSearchResponse.class);
        AccommodationSearchRequest requestParams = mock(AccommodationSearchRequest.class);
        when(jsonUtils.fromDataFetching(eq(env), eq(AccommodationSearchRequest.class)))
            .thenReturn(requestParams);
        when(accommodationSearchHandler.searchAccommodation(requestParams, resolverContext, visit)).thenReturn(response);
        assertSame(resolver.searchAccommodationFetcher(env), response);
        verify(requestParams).setSearchType(SEARCH_TYPE);
    }
}
