package com.odigeo.frontend.resolvers.accommodation.product.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.odigeo.dapi.client.CollectionMethod;
import com.odigeo.dapi.client.CreditCardLevel;
import com.odigeo.dapi.client.CreditCardType;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartCollectionOption;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class AvailableCollectionMethodsMapperTest {

    private static final String CREDIT_CAR_LEVEL_CODE = "credit_car_level_code";
    private static final String CREDIT_CAR_TYPE_CODE = "credit_car_type_code";
    private static final String CREDIT_CAR_TYPE_NAME = "credit_car_type_name";
    private static final BigDecimal FEE = new BigDecimal(88);
    private static final BigDecimal FEE_OTHER = new BigDecimal(888);
    private static final String COLLECTION_METHOD_TYPE = "SECURE3D";

    @Mock
    private ShoppingCartCollectionOption shoppingCartCollectionOption;
    @Mock
    private CollectionMethod collectionMethod;
    @Mock
    private CreditCardLevel creditCardLevel;
    @Mock
    private CreditCardType creditCardType;

    private AvailableCollectionMethodsMapper mapper;
    private ShoppingCart shoppingCart;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mapper = new AvailableCollectionMethodsMapperImpl();
        shoppingCart = new ShoppingCart();

        when(shoppingCartCollectionOption.getMethod()).thenReturn(collectionMethod);
        when(shoppingCartCollectionOption.getFee()).thenReturn(FEE).thenReturn(FEE_OTHER);

        when(collectionMethod.getCreditCardLevel()).thenReturn(creditCardLevel);
        when(collectionMethod.getCreditCardType()).thenReturn(creditCardType);
        when(collectionMethod.getId()).thenReturn(8);
        when(collectionMethod.getType()).thenReturn(COLLECTION_METHOD_TYPE);

        when(creditCardLevel.getCode()).thenReturn(CREDIT_CAR_LEVEL_CODE);
        when(creditCardType.getCode()).thenReturn(CREDIT_CAR_TYPE_CODE);
        when(creditCardType.getName()).thenReturn(CREDIT_CAR_TYPE_NAME);
    }

    @Test
    public void testPaymentInformationContractToModelWhenIsNull() {
        assertTrue(mapper.paymentInformationContractToModel(shoppingCart).getCollectionOptions().isEmpty());
    }

    @Test
    public void testPaymentInformationContractToModel() {
        shoppingCart.getCollectionOptions().add(shoppingCartCollectionOption);
        shoppingCart.getCollectionOptions().add(shoppingCartCollectionOption);
        AvailableCollectionMethodsResponse response = mapper.paymentInformationContractToModel(shoppingCart);
        assertFalse(response.getCollectionOptions().isEmpty());
        assertEvaluation(response.getCollectionOptions().get(0));
        assertEquals(response.getCollectionOptions().get(1).getFee(), FEE_OTHER);
    }

    @Test
    public void testCollectionMethodContractToModel() {
        assertEvaluation(mapper.collectionMethodContractToModel(shoppingCartCollectionOption));
    }


    private void assertEvaluation(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CollectionMethod method) {
        assertFalse(method.getIsChosenMethod());
        assertEquals(method.getId(), 8);
        assertEquals(method.getType(), PaymentMethod.SECURE3D);
        assertEquals(method.getFee(), FEE);
        assertEquals(method.getCreditCardLevel().getCode(), CREDIT_CAR_LEVEL_CODE);
        assertEquals(method.getCreditCardType().getCode(), CREDIT_CAR_TYPE_CODE);
        assertEquals(method.getCreditCardType().getName(), CREDIT_CAR_TYPE_NAME);
    }
}
