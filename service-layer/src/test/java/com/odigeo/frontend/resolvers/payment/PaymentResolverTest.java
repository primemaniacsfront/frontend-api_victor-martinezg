package com.odigeo.frontend.resolvers.payment;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCardBinDetails;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCardDateDetailsResponse;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.payment.mappers.CreditCardBinDetailsMapper;
import com.odigeo.frontend.services.CollectionMethodService;
import com.odigeo.frontend.services.CustomerCreditCardBinSecurityService;
import com.odigeo.frontend.services.PrimeService;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.YearMonth;
import java.util.Map;

import static com.odigeo.frontend.resolvers.payment.PaymentResolver.CREDIT_CARD_DATE_DETAILS_RESPONSE_QUERY;
import static com.odigeo.frontend.resolvers.payment.PaymentResolver.GET_CREDIT_CARD_BIN_DETAILS_QUERY;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class PaymentResolverTest {

    private static final String BIN = "1234567890";
    private static final String CURRENT_MONTH = "2022-03";
    private static final String ONE_MONTH_FUTURE = "2022-04";
    private static final String TWO_MONTHS_FUTURE = "2022-05";
    private static final YearMonth CURRENT_YEAR_MONTH = YearMonth.of(2022, 03);
    private static final Integer DEFAULT_EXPIRING_PERIOD = 1;

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private CollectionMethodService collectionMethodService;
    @Mock
    private CreditCardBinDetailsMapper binDetailsMapper;
    @Mock
    private CustomerCreditCardBinSecurityService customerCreditCardBinSecurityService;
    @Mock
    private com.odigeo.collectionmethod.v3.CreditCardBinDetails creditCardBinDetails;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private PrimeService primeService;
    @Mock
    private Site site;
    @Mock
    private VisitInformation visitInformation;

    private CreditCardBinDetails binDetails;

    private PaymentResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        resolver = spy(PaymentResolver.class);
        binDetails = buildCreditCardBinDetailsByDefault();

        Guice.createInjector(binder -> {
            binder.bind(JsonUtils.class).toProvider(() -> jsonUtils);
            binder.bind(CollectionMethodService.class).toProvider(() -> collectionMethodService);
            binder.bind(CreditCardBinDetailsMapper.class).toProvider(() -> binDetailsMapper);
            binder.bind(PrimeService.class).toProvider(() -> primeService);
            binder.bind(CustomerCreditCardBinSecurityService.class).toProvider(() -> customerCreditCardBinSecurityService);
        }).injectMembers(resolver);

    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(GET_CREDIT_CARD_BIN_DETAILS_QUERY));
        assertNotNull(queries.get(CREDIT_CARD_DATE_DETAILS_RESPONSE_QUERY));
    }

    @Test
    public void testGetCreditCardBinDetailsNonPrimeBlocked() {
        mockVisitInformation();
        when(jsonUtils.fromDataFetching(env)).thenReturn(BIN);
        when(collectionMethodService.getCreditCardBinDetails(BIN)).thenReturn(creditCardBinDetails);
        when(binDetailsMapper.creditCardBinDetailsContractToModel(creditCardBinDetails)).thenReturn(binDetails);
        when(customerCreditCardBinSecurityService.isCreditCardBlockedForSubscription(BIN, visitInformation)).thenReturn(Boolean.FALSE);

        CreditCardBinDetails creditCardBinDetailsResult = resolver.getCreditCardBinDetails(env);

        assertSame(creditCardBinDetailsResult, binDetails);
        assertFalse(creditCardBinDetailsResult.getIsPrimeBlocking());
    }

    @Test
    public void testGetCreditCardBinDetailsPrimeBlocked() {
        mockVisitInformation();
        when(jsonUtils.fromDataFetching(env)).thenReturn(BIN);
        when(collectionMethodService.getCreditCardBinDetails(BIN)).thenReturn(creditCardBinDetails);
        when(binDetailsMapper.creditCardBinDetailsContractToModel(creditCardBinDetails)).thenReturn(binDetails);
        when(customerCreditCardBinSecurityService.isCreditCardBlockedForSubscription(BIN, visitInformation)).thenReturn(Boolean.TRUE);

        CreditCardBinDetails creditCardBinDetailsResult = resolver.getCreditCardBinDetails(env);

        assertSame(creditCardBinDetailsResult, binDetails);
        assertTrue(creditCardBinDetailsResult.getIsPrimeBlocking());
    }

    @Test
    public void testCreditCardDateDetailsInvalidCard() {
        when(jsonUtils.fromDataFetching(env)).thenReturn(ONE_MONTH_FUTURE);
        when(resolver.getCurrentYearMonth()).thenReturn(CURRENT_YEAR_MONTH);
        mockExpiringPeriod();

        CreditCardDateDetailsResponse creditCardDateDetailsResponse = resolver.creditCardDateDetailsResponse(env);
        assertFalse(creditCardDateDetailsResponse.getIsValidDateForPrime());
    }

    @Test
    public void testCreditCardDateDetailsValidCard() {
        when(jsonUtils.fromDataFetching(env)).thenReturn(TWO_MONTHS_FUTURE);
        when(resolver.getCurrentYearMonth()).thenReturn(CURRENT_YEAR_MONTH);
        mockExpiringPeriod();

        CreditCardDateDetailsResponse creditCardDateDetailsResponse = resolver.creditCardDateDetailsResponse(env);
        assertTrue(creditCardDateDetailsResponse.getIsValidDateForPrime());
    }

    @Test
    public void testCreditCardDateDetailsInvalidCardToday() {
        when(jsonUtils.fromDataFetching(env)).thenReturn(CURRENT_MONTH);
        when(resolver.getCurrentYearMonth()).thenReturn(CURRENT_YEAR_MONTH);
        mockExpiringPeriod();

        CreditCardDateDetailsResponse creditCardDateDetailsResponse = resolver.creditCardDateDetailsResponse(env);
        assertFalse(creditCardDateDetailsResponse.getIsValidDateForPrime());
    }

    private void mockExpiringPeriod() {
        mockVisitInformation();
        when(visitInformation.getSite()).thenReturn(site);
        when(primeService.getExpiringCreditCardPeriod(site)).thenReturn(DEFAULT_EXPIRING_PERIOD);
    }

    private void mockVisitInformation() {
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
        when(graphQLContext.get(VisitInformation.class)).thenReturn(visitInformation);
    }

    private CreditCardBinDetails buildCreditCardBinDetailsByDefault() {
        CreditCardBinDetails creditCardBinDetails = new CreditCardBinDetails();
        creditCardBinDetails.setCreditCardBin(BIN);
        creditCardBinDetails.setCreditCardCountryCode("ES");
        creditCardBinDetails.setCreditCardTypeCode("1234");
        return creditCardBinDetails;
    }

}
