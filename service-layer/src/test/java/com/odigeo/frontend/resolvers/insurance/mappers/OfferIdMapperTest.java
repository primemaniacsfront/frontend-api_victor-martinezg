package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.insuranceproduct.api.last.request.OfferId;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class OfferIdMapperTest {

    private static final String OFFER_ID = "offerId";
    private static final String VERSION = "version";

    @Test
    public void map() {
        OfferIdMapper offerIdMapper = new OfferIdMapper();

        OfferId offerIdRequest = offerIdMapper.map(new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.OfferId(OFFER_ID, VERSION));

        assertEquals(offerIdRequest.getId(), OFFER_ID);
        assertEquals(offerIdRequest.getVersion(), VERSION);
    }

}
