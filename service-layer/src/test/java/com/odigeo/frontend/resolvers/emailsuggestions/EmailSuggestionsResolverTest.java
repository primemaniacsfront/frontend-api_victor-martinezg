package com.odigeo.frontend.resolvers.emailsuggestions;

import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.emailsuggestions.handlers.EmailSuggestionsHandler;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class EmailSuggestionsResolverTest {

    private static final String GET_EMAIL_SUGGESTIONS = "getEmailSuggestions";
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private EmailSuggestionsHandler emailSuggestionsHandler;
    @InjectMocks
    private EmailSuggestionsResolver resolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(GET_EMAIL_SUGGESTIONS));
    }

    @Test
    public void testEmailSuggestionsDataFetcher() {
        String email = "gmail.com";
        when(jsonUtils.fromDataFetching(env)).thenReturn(email);
        when(emailSuggestionsHandler.getEmailSuggestions(email, env.getContext())).thenReturn(Collections.emptyList());
        assertNotNull(resolver.emailSuggestionsDataFetcher(env));
    }
}
