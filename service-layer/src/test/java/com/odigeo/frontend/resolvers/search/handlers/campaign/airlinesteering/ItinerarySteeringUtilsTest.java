package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering;

import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ItinerarySteeringUtilsTest extends AirlineSteeringAbstractTest {

    private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);
    private static final BigDecimal NINETY = BigDecimal.valueOf(90);

    @Test
    public void testGetPrice() {
        SearchItineraryDTO itineraryDTO = searchItineraryDTOMock("3", 0.1, AIREUROPA_AIRLINE, 10);
        assertEquals(ItinerarySteeringUtils.getPrice(itineraryDTO, FEE_TYPE.getId()).get().floatValue(), 10);
    }

    @Test
    public void testGetEmptyPrice() {
        SearchItineraryDTO itineraryDTO = searchItineraryDTOMock("3", 0.1, AIREUROPA_AIRLINE, 10);
        assertFalse(ItinerarySteeringUtils.getPrice(itineraryDTO, "x").isPresent());
    }

    @Test
    public void testIsPriceLowerPercentageWhenPriceLowerItinerary() {
        assertTrue(ItinerarySteeringUtils.isPriceLowerPercentage(NINETY, ONE_HUNDRED, BigDecimal.ZERO));
    }

    @Test
    public void testIsPriceLowerPercentageWhenPercentageIsEquals() {
        assertTrue(ItinerarySteeringUtils.isPriceLowerPercentage(ONE_HUNDRED, NINETY, BigDecimal.TEN));
    }

    @Test
    public void testIsPriceLowerPercentageWhenPriceIsBigger() {
        assertFalse(ItinerarySteeringUtils.isPriceLowerPercentage(ONE_HUNDRED.add(BigDecimal.valueOf(0.01)), NINETY, BigDecimal.TEN));
    }

}
