package com.odigeo.frontend.resolvers.search.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelection;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchCode;

import java.util.ArrayList;
import java.util.HashSet;

import static org.mockito.Mockito.mock;

public class SearchResponseDTOTest extends BeanTest<SearchResponseDTO> {

    @Override
    public SearchResponseDTO getBean() {
        SearchResponseDTO bean = new SearchResponseDTO();
        bean.setSearchId(1L);
        bean.setSearchCode(mock(SearchCode.class));
        bean.setItineraries(new ArrayList<>(0));
        bean.setCurrencyCode("currency");
        bean.setDefaultFeeType(mock(FeeType.class));
        bean.setIsRebookingAvailable(Boolean.TRUE);
        bean.setCarriersWithRebooking(new HashSet<>(0));
        bean.setExternalSelection(mock(ExternalSelection.class));

        return bean;
    }
}
