package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;

import java.util.Collections;

public class FacilityGroupsDTOTest extends BeanTest<FacilityGroupsDTO> {

    @Override
    protected FacilityGroupsDTO getBean() {
        FacilityGroupsDTO bean = new FacilityGroupsDTO();
        bean.setDescription("description");
        bean.setCode("code");
        bean.setFacilities(Collections.singletonList(new FacilityDTO()));
        return bean;
    }
}
