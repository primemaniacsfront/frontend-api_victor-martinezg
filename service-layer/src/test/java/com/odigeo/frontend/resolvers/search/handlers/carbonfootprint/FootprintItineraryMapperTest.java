package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.Itinerary;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.SegmentResult;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class FootprintItineraryMapperTest {

    @Mock
    private FootprintSegmentResultMapper footprintSegmentResultMapper;

    @InjectMocks
    private FootprintItineraryMapper mapper;

    private static final Integer INDEX = 1;
    private static final int ITINERARIES_TO_CALCULATE = 300;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(footprintSegmentResultMapper.map(Mockito.any())).thenReturn(new SegmentResult());
    }

    @Test
    public void testMap() {
        List<SearchItineraryDTO> searchItineraryDTOList = getSearchItineraryListMock(5, false);
        List<Itinerary> itineraryList = mapper.map(searchItineraryDTOList);
        assertNotNull(itineraryList);
        assertEquals(itineraryList.size(), searchItineraryDTOList.size());
        for (int i = 0; i <  itineraryList.size(); i++) {
            Itinerary itinerary = itineraryList.get(i);
            assertEquals(itinerary.getItineraryIndex(), Integer.valueOf(i));
            assertNotNull(itinerary.getSegmentResults());
            assertEquals(itinerary.getSegmentResults().size(), INDEX.intValue());
        }
    }

    @Test
    public void testMapMoreThanMaxConstant() {
        List<SearchItineraryDTO> searchItineraryDTOList = getSearchItineraryListMock(ITINERARIES_TO_CALCULATE + 1, false);
        List<Itinerary> itineraryList = mapper.map(searchItineraryDTOList);
        assertNotNull(itineraryList);
        assertEquals(itineraryList.size(), ITINERARIES_TO_CALCULATE);
    }

    @Test
    public void testMapWhenVINItineraries() {
        List<SearchItineraryDTO> searchItineraryDTOList = getSearchItineraryListMock(ITINERARIES_TO_CALCULATE, true);
        List<Itinerary> itineraryList = mapper.map(searchItineraryDTOList);
        assertNotNull(itineraryList);
        assertEquals(itineraryList.size(), ITINERARIES_TO_CALCULATE - 1);
    }

    @Test
    public void testMapWhenOnlyVINItineraries() {
        List<SearchItineraryDTO> searchItineraryDTOList = getSearchItineraryListMock(ITINERARIES_TO_CALCULATE, true);
        searchItineraryDTOList.forEach(s -> s.setVin(Boolean.TRUE));
        List<Itinerary> itineraryList = mapper.map(searchItineraryDTOList);
        assertNotNull(itineraryList);
        assertEquals(itineraryList.size(), ITINERARIES_TO_CALCULATE);
    }

    private SearchItineraryDTO getSearchItineraryDTOMock(int index, boolean firstIsVin) {
        SearchItineraryDTO searchItineraryDTO = new SearchItineraryDTO();
        searchItineraryDTO.setIndex(index);
        searchItineraryDTO.setLegs(Collections.singletonList(new LegDTO()));
        if (firstIsVin && index == 0) {
            searchItineraryDTO.setVin(Boolean.TRUE);
        } else {
            searchItineraryDTO.setVin(Boolean.FALSE);
        }
        return searchItineraryDTO;
    }

    private List<SearchItineraryDTO> getSearchItineraryListMock(int itineraries, boolean firstIsVin) {
        List<SearchItineraryDTO> searchItineraryDTOList = new ArrayList<>();
        for (int i = 0; i < itineraries; i++) {
            searchItineraryDTOList.add(getSearchItineraryDTOMock(i, firstIsVin));
        }
        return searchItineraryDTOList;
    }

}
