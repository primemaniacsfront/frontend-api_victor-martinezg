package com.odigeo.frontend.resolvers.user;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCards;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PersonalInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserCreditCardsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserTraveller;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.user.handlers.UserCreditCardsHandler;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.frontend.resolvers.user.handlers.UserPasswordHandler;
import com.odigeo.frontend.resolvers.user.handlers.UserTravellersHandler;
import com.odigeo.frontend.resolvers.user.mappers.UserDescriptionMapper;
import com.odigeo.frontend.resolvers.user.mappers.UserDescriptionMapperImpl;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.User;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class UserResolverTest {

    private static final String QUERY = "Query";
    private static final String MUTATION = "Mutation";
    private static final String USER = "User";
    private static final String USER_INFO_QUERY = "userInfo";
    private static final String GET_USER_DESCRIPTION_QUERY = "getUser";
    private static final String GET_USER_TRAVELLERS_QUERY = "getTravellers";
    private static final String CHANGE_PASSWORD = "changePassword";
    private static final String GET_USER_CREDIT_CARDS_QUERY = "getUserCreditCards";
    private static final String TRAVELLERS = "travellers";
    private static final String CREDIT_CARDS = "creditCards";
    private static final Long ID_LONG = 3L;

    @Mock
    private UserDescriptionService userService;
    @Mock
    private UserTravellersHandler userTravellersHandler;
    @Mock
    private MembershipHandler membershipHandler;
    @Mock
    private UserCreditCardsHandler userCreditCardsHandler;
    @Mock
    private UserPasswordHandler userPasswordHandler;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private UserDescriptionMapper mapper;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private UserCreditCardsResponse userCreditCardsResponse;

    @InjectMocks
    private UserResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new UserDescriptionMapperImpl();
        Guice.createInjector(binder -> {
            binder.bind(UserDescriptionMapper.class).toProvider(() -> mapper);
            binder.bind(UserDescriptionService.class).toProvider(() -> userService);
            binder.bind(UserCreditCardsHandler.class).toProvider(() -> userCreditCardsHandler);
            binder.bind(UserTravellersHandler.class).toProvider(() -> userTravellersHandler);
            binder.bind(MembershipHandler.class).toProvider(() -> membershipHandler);
            binder.bind(UserPasswordHandler.class).toProvider(() -> userPasswordHandler);
        }).injectMembers(resolver);
        when(env.getContext()).thenReturn(context);
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(QUERY);
        Map<String, DataFetcher> mutations = runtimeWiring.getDataFetchers().get(MUTATION);
        Map<String, DataFetcher> users = runtimeWiring.getDataFetchers().get(USER);

        assertNotNull(queries.get(USER_INFO_QUERY));
        assertNotNull(queries.get(GET_USER_DESCRIPTION_QUERY));
        assertNotNull(queries.get(GET_USER_TRAVELLERS_QUERY));
        assertNotNull(queries.get(GET_USER_CREDIT_CARDS_QUERY));
        assertNotNull(mutations.get(CHANGE_PASSWORD));
        assertNotNull(users.get(TRAVELLERS));
        assertNotNull(users.get(CREDIT_CARDS));
    }

    @Test
    public void testInfoDataFetcher() {
        User user = mock(User.class);
        UserInfo userInfoDTO = new UserInfo();
        userInfoDTO.setName("name");
        Membership membership = mock(Membership.class);
        when(userService.getUserByToken(context)).thenReturn(user);
        when(membershipHandler.getMembershipFromLoggedUser(env)).thenReturn(membership);
        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setName("name");
        UserTraveller userTravellers = new UserTraveller();
        userTravellers.setPersonalInfo(personalInfo);
        List<UserTraveller> travellers = Collections.singletonList(userTravellers);
        when(userTravellersHandler.getTravellers(user)).thenReturn(travellers);
        assertEquals(resolver.userInfoDataFetcher(env).getName(), userInfoDTO.getName());
    }

    @Test
    public void testUserDescriptionDataFetcher() {
        User user = new User();
        user.setId(ID_LONG);
        when(userService.getUserByToken(context)).thenReturn(user);
        assertEquals((Long) resolver.userDescriptionDataFetcher(env).getId(), ID_LONG);
    }

    @Test
    public void testTravellersDataFetcher() {
        UserTraveller traveller = mock(UserTraveller.class);
        ArrayList<UserTraveller> travellers = new ArrayList<>();
        travellers.add(traveller);
        when(userTravellersHandler.getTravellers(env)).thenReturn(travellers);
        assertEquals(resolver.userTravellersDataFetcher(env).get(0), traveller);
    }

    @Test
    public void testUserCreditCardsDataFetcher() {
        CreditCards creditCards = mock(CreditCards.class);
        List<CreditCards> creditCardsList = Collections.singletonList(creditCards);
        when(userCreditCardsResponse.getCreditCards()).thenReturn(creditCardsList);
        when(userCreditCardsHandler.getCreditCards(env)).thenReturn(userCreditCardsResponse);
        assertEquals(resolver.userCreditCardsDataFetcher(env).getCreditCards(), creditCardsList);
    }

    @Test
    public void testChangePasswordDataFetcher() {
        User user = new User();
        user.setId(ID_LONG);
        when(userPasswordHandler.changePasswordWithCode(any(), eq(env))).thenReturn(user);
        assertEquals((Long) resolver.changePasswordDataFetcher(env).getId(), ID_LONG);
    }
}
