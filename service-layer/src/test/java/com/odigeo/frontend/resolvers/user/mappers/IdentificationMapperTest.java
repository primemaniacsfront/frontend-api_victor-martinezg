package com.odigeo.frontend.resolvers.user.mappers;

import com.odigeo.userprofiles.api.v2.model.Identification;
import com.odigeo.userprofiles.api.v2.model.IdentificationType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class IdentificationMapperTest {

    private static final Long ID = 1L;
    private static final String COUNTRY_CODE = "ES";
    private static final String IDENTIFICATION_ID = "123456789A";
    @Mock
    private Identification identification;

    private IdentificationMapper mapper;
    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Identification identificationModel;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new IdentificationMapperImpl();
    }

    @Test
    public void testFrequentFlyerContractToModel() {
        Date date = new Date();
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        when(identification.getId()).thenReturn(ID);
        when(identification.getIdentificationCountryCode()).thenReturn(COUNTRY_CODE);
        when(identification.getIdentificationExpirationDate()).thenReturn(date);
        when(identification.getIdentificationId()).thenReturn(IDENTIFICATION_ID);
        when(identification.getIdentificationType()).thenReturn(IdentificationType.NATIONAL_ID_CARD);

        identificationModel = mapper.identificationContractToModel(identification);

        assertEquals(identificationModel.getIdentificationCountryCode(), identification.getIdentificationCountryCode());
        assertEquals(identificationModel.getIdentificationExpirationDate(), zonedDateTime);
        assertEquals(identificationModel.getIdentificationId(), identification.getIdentificationId());
        assertEquals(identificationModel.getIdentificationType(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.IdentificationType.NATIONAL_ID_CARD);

    }
}
