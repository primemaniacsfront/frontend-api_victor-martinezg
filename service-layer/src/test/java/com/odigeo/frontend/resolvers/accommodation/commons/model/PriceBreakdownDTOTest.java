package com.odigeo.frontend.resolvers.accommodation.commons.model;

import bean.test.BeanTest;

import java.math.BigDecimal;

public class PriceBreakdownDTOTest extends BeanTest<PriceBreakdownDTO> {

    @Override
    protected PriceBreakdownDTO getBean() {
        PriceBreakdownDTO bean = new PriceBreakdownDTO();
        bean.setCurrency("COL");

        bean.setPrice(BigDecimal.ONE);
        bean.setPriceWithoutDiscount(BigDecimal.TEN);
        bean.setDiscount(new DiscountDTO());
        bean.setPricePerNight(BigDecimal.ONE);
        return bean;
    }
}
