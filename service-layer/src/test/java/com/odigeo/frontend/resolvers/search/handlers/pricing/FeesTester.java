package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;

import java.math.BigDecimal;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

class FeesTester {

    static void checkFee(List<Fee> itineraryFees, String feeId, BigDecimal price,
                         String name, PaymentMethod paymentMethod) {

        Fee itineraryFee = itineraryFees.stream()
            .filter(fee -> fee.getType().getId().equals(feeId))
            .findAny()
            .orElse(null);

        assertNotNull(itineraryFee);
        assertEquals(itineraryFee.getPrice().getAmount(), price);
        assertEquals(itineraryFee.getType().getName(), name);
        assertSame(itineraryFee.getType().getPaymentMethod(), paymentMethod);
    }

}
