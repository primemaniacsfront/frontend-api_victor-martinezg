package com.odigeo.frontend.resolvers.accommodation.search.models;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class AccommodationProviderKeyDTOTest extends BeanTest<AccommodationProviderKeyDTO> {

    @Override
    protected AccommodationProviderKeyDTO getBean() {
        return new AccommodationProviderKeyDTO("id", "id2");
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(AccommodationProviderKeyDTO.class)
                .suppress(Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
