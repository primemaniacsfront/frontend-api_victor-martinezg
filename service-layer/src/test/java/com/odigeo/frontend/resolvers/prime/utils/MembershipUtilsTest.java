package com.odigeo.frontend.resolvers.prime.utils;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipDuration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipDurationTimeUnit;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.odigeo.frontend.resolvers.prime.models.MembershipStatus;
import com.odigeo.frontend.resolvers.prime.models.MembershipType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static com.odigeo.frontend.resolvers.prime.utils.MembershipUtilsPredicateProvider.IS_MEMBERSHIP_ONE_MONTH_DURATION;
import static com.odigeo.frontend.resolvers.prime.utils.MembershipUtilsPredicateProvider.IS_MEMBERSHIP_PRICE_ZERO;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipUtilsTest {
    public static final String CURRENCY = "EUR";
    public static final int ONE = 1;
    @Mock
    private Membership membership;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testIsActiveMembership() {
        when(membership.getStatus()).thenReturn(MembershipStatus.ACTIVATED.name());
        assertTrue(MembershipUtils.isActivated(membership));
    }

    @Test
    public void testIsActiveMembershipWhenDeactivated() {
        when(membership.getStatus()).thenReturn(MembershipStatus.DEACTIVATED.name());
        assertFalse(MembershipUtils.isActivated(membership));
    }

    @Test
    public void testIsActiveMembershipWhenPTC() {
        when(membership.getStatus()).thenReturn(MembershipStatus.PENDING_TO_COLLECT.name());
        assertFalse(MembershipUtils.isActivated(membership));
    }

    @Test
    public void testIsActiveMembershipWhenPTA() {
        when(membership.getStatus()).thenReturn(MembershipStatus.PENDING_TO_ACTIVATE.name());
        assertFalse(MembershipUtils.isActivated(membership));
    }

    @Test
    public void testIsFreeTrialWhenBasicFree() {
        Money totalPrice = new Money(BigDecimal.ZERO, CURRENCY);
        MembershipDuration duration = new MembershipDuration(ONE, MembershipDurationTimeUnit.DAYS);
        when(membership.getType()).thenReturn(MembershipType.BASIC_FREE.name());
        when(membership.getTotalPrice()).thenReturn(totalPrice);
        when(membership.getDuration()).thenReturn(duration);
        assertFalse(MembershipUtils.isFreeTrial(membership));
    }

    @Test
    public void testIsFreeTrial() {
        Money totalPrice = new Money(BigDecimal.ZERO, CURRENCY);
        MembershipDuration duration = new MembershipDuration(ONE, MembershipDurationTimeUnit.DAYS);
        when(membership.getType()).thenReturn(MembershipType.BASIC.name());
        when(membership.getTotalPrice()).thenReturn(totalPrice);
        when(membership.getDuration()).thenReturn(duration);
        assertTrue(MembershipUtils.isFreeTrial(membership));
    }

    @Test
    public void test1MMembership() {
        MembershipDuration duration = new MembershipDuration(ONE, MembershipDurationTimeUnit.MONTHS);
        when(membership.getDuration()).thenReturn(duration);
        assertTrue(IS_MEMBERSHIP_ONE_MONTH_DURATION.test(membership));
    }

    @Test
    public void test14DMembership() {
        MembershipDuration duration = new MembershipDuration(14, MembershipDurationTimeUnit.DAYS);
        when(membership.getDuration()).thenReturn(duration);
        assertTrue(IS_MEMBERSHIP_ONE_MONTH_DURATION.test(membership));
    }

    @Test
    public void test2MMembership() {
        MembershipDuration duration = new MembershipDuration(2, MembershipDurationTimeUnit.MONTHS);
        when(membership.getDuration()).thenReturn(duration);
        assertFalse(IS_MEMBERSHIP_ONE_MONTH_DURATION.test(membership));
    }

    @Test
    public void testIsPriceZeroReturnsNull() {
        when(membership.getTotalPrice()).thenReturn(null);
        assertTrue(IS_MEMBERSHIP_PRICE_ZERO.test(membership));
    }

    @Test
    public void testIsPriceZeroWhenAmountIsNull() {
        Money totalPrice = new Money();
        when(membership.getTotalPrice()).thenReturn(totalPrice);
        assertTrue(IS_MEMBERSHIP_PRICE_ZERO.test(membership));
    }

    @Test
    public void testIsPriceZeroWhenAmountIsZero() {
        Money totalPrice = new Money(BigDecimal.ZERO, CURRENCY);
        when(membership.getTotalPrice()).thenReturn(totalPrice);
        assertTrue(IS_MEMBERSHIP_PRICE_ZERO.test(membership));
    }

    @Test
    public void testIsPriceZeroWhenAmountIsPositive() {
        Money totalPrice = new Money(BigDecimal.ONE, CURRENCY);
        when(membership.getTotalPrice()).thenReturn(totalPrice);
        assertFalse(IS_MEMBERSHIP_PRICE_ZERO.test(membership));
    }
}
