package com.odigeo.frontend.resolvers.accommodation.product.handlers.productapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationBuyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateAccommodationProductResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Guest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.services.accommodation.AccommodationProductService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CreateAccommodationProductTest {


    @Mock
    private AccommodationProductService accommodationProductService;
    @Mock
    private ResolverContext context;
    @Mock
    private AccommodationBuyer accommodationBuyer;

    @InjectMocks
    private CreateAccommodationProduct testClass;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    public void test() {
        when(accommodationProductService.createProduct("123", accommodationBuyer)).thenReturn("productId");
        CreateAccommodationProductResponse productAccommodation = testClass.createProductAccommodation("123", Collections.singletonList(new Guest()), accommodationBuyer, context);
        assertNotNull(productAccommodation);
        assertEquals("productId", productAccommodation.getAccommodationProduct().getProductId());
    }

}
