package com.odigeo.frontend.resolvers.trip.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Trip;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.resolvers.trip.mapper.TripMapper;
import com.odigeo.frontend.services.trip.TripService;
import com.odigeo.travelcompanion.v2.model.booking.BookingDetail;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class TripHandlerTest {

    private static final String BOOKING_ID = "7651251015";
    private static final String EMAIL = "test@edreamsodigeo.com";

    @Mock
    private VisitInformation visitInformation;
    @Mock
    private TripService tripService;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private TripMapper tripMapper;

    private final Trip trip = new Trip();

    @InjectMocks
    private TripHandler tripHandler;

    @BeforeMethod
    public void init() throws IOException {
        openMocks(this);
        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visitInformation);
        BookingDetail bookingDetail = new BookingDetail();
        when(tripService.getTrip(any(), any(), any(), any())).thenReturn(bookingDetail);
        when(tripMapper.mapToTripContract(any())).thenReturn(trip);
    }

    @Test
    public void testGetTrip() {
        Brand brand = Brand.ED;
        String locale = "es_ES";
        when(visitInformation.getBrand()).thenReturn(brand);
        when(visitInformation.getLocale()).thenReturn(locale);
        Trip tripResponse = tripHandler.getTrip(Long.getLong(BOOKING_ID), EMAIL, visitInformation);
        assertNotNull(tripResponse);
        verify(tripService).getTrip(any(), any(), any(), any());
    }

    @Test
    public void testGetTripResponse() {
        Brand brand = Brand.ED;
        String locale = "es_ES";
        when(visitInformation.getBrand()).thenReturn(brand);
        when(visitInformation.getLocale()).thenReturn(locale);
        Trip tripResponse = tripHandler.getTrip(Long.getLong(BOOKING_ID), EMAIL, visitInformation);
        assertNotNull(tripResponse);
        assertEquals(tripResponse, trip);
    }
}
