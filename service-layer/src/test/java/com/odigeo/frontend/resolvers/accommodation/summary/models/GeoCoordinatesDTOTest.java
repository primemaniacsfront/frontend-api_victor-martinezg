package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;

import java.math.BigDecimal;

public class GeoCoordinatesDTOTest extends BeanTest<GeoCoordinatesDTO> {

    public GeoCoordinatesDTO getBean() {
        GeoCoordinatesDTO bean = new GeoCoordinatesDTO();
        bean.setLongitude(BigDecimal.ONE);
        bean.setLatitude(BigDecimal.ONE);

        return bean;
    }
}
