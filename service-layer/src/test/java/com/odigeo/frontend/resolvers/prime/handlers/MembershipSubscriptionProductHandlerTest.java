package com.odigeo.frontend.resolvers.prime.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.ShoppingInfoUtils;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import com.odigeo.frontend.resolvers.prime.operations.MembershipShoppingCartOperations;
import com.odigeo.frontend.resolvers.prime.operations.MembershipShoppingOperationsFactory;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class MembershipSubscriptionProductHandlerTest {

    private static final Long SHOPPING_ID = 1L;
    private static final ShoppingType SHOPPING_TYPE = ShoppingType.CART;
    @Mock
    private MembershipShoppingOperationsFactory membershipShoppingOperationsFactory;
    @Mock
    private ShoppingInfoUtils shoppingInfoUtils;
    @Mock
    private ShoppingInfo shoppingInfo;
    @Mock
    private MembershipSubscriptionResponse membershipSubscriptionResponse;
    @Mock
    private ResolverContext context;
    @Mock
    private MembershipShoppingCartOperations shoppingCartOperations;
    @Mock
    private ModifyMembershipSubscription modifyMembershipSubscription;
    @InjectMocks
    MembershipSubscriptionProductHandler membershipSubscriptionProductHandler;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        given(shoppingInfoUtils.getShoppingType(eq(shoppingInfo)))
                .willReturn(SHOPPING_TYPE);
        given(shoppingInfoUtils.getShoppingId(eq(shoppingInfo))).willReturn(SHOPPING_ID);
        given(membershipShoppingOperationsFactory.getMembershipShoppingOperations(eq(SHOPPING_TYPE))).willReturn(shoppingCartOperations);
        given(shoppingCartOperations.getMembershipSubscriptionResponse(eq(SHOPPING_ID), eq(context))).willReturn(membershipSubscriptionResponse);
    }

    @Test
    public void testGetSubscriptionFromShoppingCart() {
        MembershipSubscriptionResponse subscriptionFromShoppingCart = membershipSubscriptionProductHandler.getSubscriptionFromShoppingCart(shoppingInfo, context);
        verify(membershipShoppingOperationsFactory).getMembershipShoppingOperations(eq(SHOPPING_TYPE));
        verify(shoppingInfoUtils).getShoppingId(shoppingInfo);
        verify(shoppingCartOperations).getMembershipSubscriptionResponse(eq(SHOPPING_ID), eq(context));
        assertEquals(subscriptionFromShoppingCart, membershipSubscriptionResponse);
    }

    @Test
    public void testUpdateMembershipSubscription() {
        given(shoppingCartOperations.updateMembershipSubscriptionProduct(eq(modifyMembershipSubscription), eq(context)))
                .willReturn(membershipSubscriptionResponse);
        given(modifyMembershipSubscription.getShoppingInfo()).willReturn(shoppingInfo);
        MembershipSubscriptionResponse subscriptionUpdatedInShoppingCart = membershipSubscriptionProductHandler.updateMembershipSubscription(modifyMembershipSubscription, context);
        verify(shoppingCartOperations).updateMembershipSubscriptionProduct(eq(modifyMembershipSubscription), eq(context));
        assertEquals(subscriptionUpdatedInShoppingCart, membershipSubscriptionResponse);
    }
}
