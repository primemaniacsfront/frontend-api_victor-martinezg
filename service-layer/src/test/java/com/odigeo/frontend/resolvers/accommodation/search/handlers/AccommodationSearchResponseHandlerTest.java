package com.odigeo.frontend.resolvers.accommodation.search.handlers;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.PriceBreakdownHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDeal;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDealPriceCalculatorAdapter;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationSearchResults;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationSearchReview;
import com.odigeo.searchengine.v2.responses.accommodation.BoardType;
import com.odigeo.searchengine.v2.responses.error.MessageResponse;
import com.odigeo.searchengine.v2.responses.fee.FeeDetails;
import com.odigeo.searchengine.v2.responses.fee.FeeInfo;
import com.odigeo.searchengine.v2.responses.logic.ThreeValuedLogic;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Currency;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class AccommodationSearchResponseHandlerTest {

    @Mock
    private SearchResponse searchResponse;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private ContentKeyMapper contentKeyMapper;
    @Mock
    private PriceBreakdownHandler priceBreakdownHandler;

    @InjectMocks
    private AccommodationSearchResponseHandler testClass;

    private float dpDiscount = 1f;
    private static final int NUM_ADULTS = 1;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(priceBreakdownHandler.mapPriceBreakdown(any(AccommodationDeal.class), any(VisitInformation.class), anyInt())).thenReturn(new PriceBreakdownDTO());
    }

    @Test
    public void testBuildResponse() {
        setUpSearchResponse();
        AccommodationSearchResponseDTO accommodationSearchResponseDTO = testClass.buildSearchResponse(searchResponse, visitInformation, NUM_ADULTS);
        Long searchId = 123L;
        assertEquals(accommodationSearchResponseDTO.getSearchId(), searchId);
        AccommodationResponseDTO accommodationResponse = accommodationSearchResponseDTO.getAccommodationResponse();
        assertEquals(accommodationResponse.getAccommodations().size(), 1);
        SearchAccommodationDTO searchAccommodationDTO = accommodationResponse.getAccommodations().get(0);

        assertNotNull(searchAccommodationDTO.getPriceBreakdown());
        verify(priceBreakdownHandler).mapPriceBreakdown(any(AccommodationDeal.class), any(VisitInformation.class), anyInt());

        assertEquals(accommodationResponse.getCurrency(), "EUR");
        assertNull(accommodationResponse.getMetadata());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testErrorMessage() {
        when(searchResponse.getMessages()).thenReturn(Collections.singletonList(new MessageResponse()));
        testClass.buildSearchResponse(searchResponse, visitInformation, NUM_ADULTS);
    }

    private void setUpSearchResponse() {
        when(searchResponse.getSearchId()).thenReturn(123L);
        AccommodationSearchResults accommodationSearchResults = new AccommodationSearchResults();
        accommodationSearchResults.setPriceCurrency(Currency.getInstance("EUR"));
        accommodationSearchResults.getAccommodationResults().add(getAccommodationDeal());
        when(searchResponse.getAccommodationSearchResults()).thenReturn(accommodationSearchResults);
    }

    private AccommodationDeal getAccommodationDeal() {
        AccommodationDeal accommodationDeal = new AccommodationDeal();
        accommodationDeal.setPrice(getAccommodationDealPriceCalculatorAdapter());
        accommodationDeal.setDynpackFareDiscount(dpDiscount);
        accommodationDeal.setProviderCurrency("EUR");
        accommodationDeal.setCancellationFree(ThreeValuedLogic.TRUE);
        accommodationDeal.setBoardType(BoardType.AI);
        accommodationDeal.getUsersRates().add(buildAccommodationSearchReview());
        return accommodationDeal;
    }

    private AccommodationSearchReview buildAccommodationSearchReview() {
        AccommodationSearchReview accommodationSearchReview = new AccommodationSearchReview();
        accommodationSearchReview.setSource("TA");
        accommodationSearchReview.setAverageUsersRate(BigDecimal.TEN);
        accommodationSearchReview.setNumberOfUsersRates(1);
        accommodationSearchReview.setProviderReviewId("123");
        return accommodationSearchReview;
    }

    private AccommodationDealPriceCalculatorAdapter getAccommodationDealPriceCalculatorAdapter() {
        AccommodationDealPriceCalculatorAdapter accommodationDealPriceCalculatorAdapter = new AccommodationDealPriceCalculatorAdapter();
        accommodationDealPriceCalculatorAdapter.setTotalPrice(BigDecimal.TEN);
        accommodationDealPriceCalculatorAdapter.setMarkup(BigDecimal.ONE);
        accommodationDealPriceCalculatorAdapter.setFeeInfo(getFeeInfo());
        return accommodationDealPriceCalculatorAdapter;
    }

    private FeeInfo getFeeInfo() {
        FeeInfo feeInfo = new FeeInfo();
        FeeDetails feeDetails = new FeeDetails();
        feeDetails.setDiscount(BigDecimal.ONE);
        feeDetails.setTax(BigDecimal.ONE);
        feeInfo.setPaymentFee(feeDetails);
        feeInfo.setSearchFee(feeDetails);
        return feeInfo;
    }
}
