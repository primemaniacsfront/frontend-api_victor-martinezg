package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.odigeo.shoppingbasket.v2.requests.CreateRequest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CreateRequestMapperTest {

    private static final String VISIT_INFORMATION_SERIALIZED = "VisitInformationSerialized";
    private static final String BUNDLE_ID = "BundleId";

    @Test
    public void map() {
        CreateRequestMapper createRequestMapper = new CreateRequestMapper();

        CreateRequest createRequest = createRequestMapper.map(BUNDLE_ID, VISIT_INFORMATION_SERIALIZED);

        assertEquals(createRequest.getBundleId(), BUNDLE_ID);
        assertEquals(createRequest.getVisitInformation(), VISIT_INFORMATION_SERIALIZED);
    }
}
