package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.odigeo.shoppingbasket.v2.model.Price;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

public class MoneyMapperTest {

    private static final BigDecimal AMOUNT = BigDecimal.TEN;
    private static final String CURRENCY_CODE = "EUR";

    @Test
    public void mapShoppingBasketPrice() {
        MoneyMapper moneyMapper =  new MoneyMapper();

        Money money = moneyMapper.map(getShoppingBasketPrice());

        assertEquals(money.getCurrency(), CURRENCY_CODE);
        assertEquals(money.getAmount(), AMOUNT);
    }

    private Price getShoppingBasketPrice() {
        Price price = new Price();

        price.setAmount(AMOUNT);
        price.setCurrency(Currency.getInstance(CURRENCY_CODE));

        return price;
    }

}
