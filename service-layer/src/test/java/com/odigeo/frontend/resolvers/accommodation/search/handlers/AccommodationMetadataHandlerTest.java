package com.odigeo.frontend.resolvers.accommodation.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchMetadata;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ThreeValuedLogic;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.DiscountDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;
import com.odigeo.frontend.resolvers.accommodation.search.configuration.AccommodationHistogramRangeByCurrencyConfiguration;
import com.odigeo.frontend.resolvers.accommodation.search.configuration.AccommodationPriceRangeByCurrencyConfiguration;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory.STARS_2;
import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType.HOTEL;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AccommodationMetadataHandlerTest {


    @Mock
    private VisitInformation visitInformation;
    @Mock
    private AccommodationPriceRangeByCurrencyConfiguration accommodationPriceRangeByCurrencyConfiguration;
    @Mock
    private AccommodationHistogramRangeByCurrencyConfiguration accommodationHistogramRangeByCurrencyConfiguration;

    @InjectMocks
    private AccommodationMetadataHandler testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void test() {
        AccommodationSearchResponseDTO response = buildAccommodationSearchResponseDTO();
        HashMap<String, Long> currencyRange = new HashMap<>();
        currencyRange.put("EUR", 25L);
        when(accommodationPriceRangeByCurrencyConfiguration.getCurrencyRange()).thenReturn(currencyRange);
        when(accommodationHistogramRangeByCurrencyConfiguration.getCurrencyRange()).thenReturn(currencyRange);
        when(visitInformation.getCurrency()).thenReturn("EUR");
        testClass.populateMetadata(response, 1, visitInformation);
        AccommodationSearchMetadata metadata = response.getAccommodationResponse().getMetadata();
        assertNotNull(metadata);
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("HOTEL") && f.getFilterType().equals("PROPERTY_TYPE") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("SPA") && f.getFilterType().equals("FACILITY") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("STARS_2") && f.getFilterType().equals("STARS") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("COVID_MEASURES") && f.getFilterType().equals("HEALTH") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("CANCELLATION_FREE") && f.getFilterType().equals("RESERVATION_POLICY") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("PAY_AT_DESTINATION") && f.getFilterType().equals("RESERVATION_POLICY") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("1") && f.getFilterType().equals("DISTANCE_TO_CENTER") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("3") && f.getFilterType().equals("DISTANCE_TO_CENTER") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("5") && f.getFilterType().equals("DISTANCE_TO_CENTER") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("3") && f.getFilterType().equals("GUEST_RATING") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("4") && f.getFilterType().equals("GUEST_RATING") && f.getCount() == 1).findAny().orElse(null));
        assertNotNull(metadata.getFiltersMetadata().stream().filter(f -> f.getFilterName().equals("5") && f.getFilterType().equals("GUEST_RATING") && f.getCount() == 1).findAny().orElse(null));
        assertEquals(metadata.getPriceMetadata().getMaxPrice(), BigDecimal.ONE);
        assertEquals(metadata.getPriceMetadata().getMaxDiscountPercentage(), BigDecimal.ONE);
        assertEquals(metadata.getPriceMetadata().getPricePerNightRange(), BigDecimal.valueOf(50));
        assertEquals(metadata.getPriceMetadata().getHistogramRange(), BigDecimal.valueOf(25));
    }

    private AccommodationSearchResponseDTO buildAccommodationSearchResponseDTO() {
        AccommodationSearchResponseDTO accommodationSearchResponseDTO = new AccommodationSearchResponseDTO();
        accommodationSearchResponseDTO.setAccommodationResponse(buildAccommodationResponse());
        return accommodationSearchResponseDTO;
    }

    private AccommodationResponseDTO buildAccommodationResponse() {
        AccommodationResponseDTO accommodationResponseDTO = new AccommodationResponseDTO();
        accommodationResponseDTO.setAccommodations(Collections.singletonList(buildSearchAccommodation()));
        return accommodationResponseDTO;
    }

    private SearchAccommodationDTO buildSearchAccommodation() {
        SearchAccommodationDTO searchAccommodationDTO = new SearchAccommodationDTO();
        searchAccommodationDTO.setAccommodationFacilities(Arrays.asList("SPA", "COVID_MEASURES"));
        searchAccommodationDTO.setAccommodationDeal(buildAccommodationDeal());
        searchAccommodationDTO.setPriceBreakdown(buildPriceBreakdown());
        searchAccommodationDTO.setDistanceFromCityCenter(0.9);
        searchAccommodationDTO.setUserReview(buildUserReviews());
        return searchAccommodationDTO;
    }

    private AccommodationReview buildUserReviews() {
        AccommodationReview accommodationReview = new AccommodationReview();
        accommodationReview.setRating(5f);
        return accommodationReview;
    }

    private PriceBreakdownDTO buildPriceBreakdown() {
        PriceBreakdownDTO priceBreakdownDTO = new PriceBreakdownDTO();
        priceBreakdownDTO.setPrice(BigDecimal.ONE);
        priceBreakdownDTO.setDiscount(new DiscountDTO());
        priceBreakdownDTO.getDiscount().setPercentage(BigDecimal.ONE);
        return priceBreakdownDTO;
    }

    private AccommodationDealInformationDTO buildAccommodationDeal() {
        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();
        accommodationDealInformationDTO.setType(HOTEL);
        accommodationDealInformationDTO.setCategory(STARS_2);
        accommodationDealInformationDTO.setCancellationFree(ThreeValuedLogic.TRUE);
        accommodationDealInformationDTO.setPaymentAtDestination(Boolean.TRUE);
        return accommodationDealInformationDTO;
    }

}
