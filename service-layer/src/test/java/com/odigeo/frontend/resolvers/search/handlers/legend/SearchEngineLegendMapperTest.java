package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.contract.location.LocationDTO;
import com.odigeo.frontend.resolvers.search.mappers.LocationMapper;
import com.odigeo.searchengine.v2.responses.baggage.BaggageConditions;
import com.odigeo.searchengine.v2.responses.baggage.BaggageEstimationFees;
import com.odigeo.searchengine.v2.responses.collection.CollectionEstimationFees;
import com.odigeo.searchengine.v2.responses.collection.CollectionMethod;
import com.odigeo.searchengine.v2.responses.gis.Location;
import com.odigeo.searchengine.v2.responses.itinerary.Carrier;
import com.odigeo.searchengine.v2.responses.itinerary.CrossFaringItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.FreeCancellationLimit;
import com.odigeo.searchengine.v2.responses.itinerary.HubItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.Itineraries;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerariesLegend;
import com.odigeo.searchengine.v2.responses.itinerary.ItineraryProvider;
import com.odigeo.searchengine.v2.responses.itinerary.Section;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;
import com.odigeo.searchengine.v2.responses.itinerary.TechnicalStop;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class SearchEngineLegendMapperTest {

    @Mock
    private ItinerariesLegend legend;
    @Mock
    private DateUtils dateUtils;
    @Mock
    private LocationMapper locationMapper;
    @Mock
    private Itineraries itineraries;

    private SearchEngineLegendMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new SearchEngineLegendMapper();

        Guice.createInjector(binder -> {
            binder.bind(DateUtils.class).toProvider(() -> dateUtils);
            binder.bind(LocationMapper.class).toProvider(() -> locationMapper);
        }).injectMembers(mapper);

        when(legend.getItineraries()).thenReturn(itineraries);
    }

    @Test
    public void testBuildCarrierMap() {
        Integer id = 0;
        String code = "code";
        String name = "name";

        Carrier carrier = mock(Carrier.class);

        when(carrier.getId()).thenReturn(id);
        when(carrier.getCode()).thenReturn(code);
        when(carrier.getName()).thenReturn(name);

        when(legend.getCarriers()).thenReturn(Collections.singletonList(carrier));

        Map<Integer, com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier> carriers = mapper.buildCarrierMap(legend);
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier carrierDTO = carriers.get(id);

        assertEquals(carriers.size(), 1);
        assertEquals(carrierDTO.getId(), code);
        assertEquals(carrierDTO.getName(), name);
    }

    @Test
    public void testBuildLocationMap() {
        Integer id = 0;
        String cityName = "cityName";
        String name = "name";
        String iata = "iata";
        String cityIata = "cityIata";
        String countryCode = "country";
        String countryName = "countryName";
        LocationType locationType = mock(LocationType.class);

        Location location = mock(Location.class);

        when(location.getGeoNodeId()).thenReturn(id);
        when(location.getCityName()).thenReturn(cityName);
        when(location.getName()).thenReturn(name);
        when(location.getIataCode()).thenReturn(iata);
        when(location.getCityIataCode()).thenReturn(cityIata);
        when(location.getCountryCode()).thenReturn(countryCode);
        when(location.getCountryName()).thenReturn(countryName);

        when(locationMapper.map(location)).thenReturn(locationType);
        when(legend.getLocations()).thenReturn(Collections.singletonList(location));

        Map<Integer, LocationDTO> locations = mapper.buildLocationMap(legend);
        LocationDTO locationDTO = locations.get(id);

        assertEquals(locations.size(), 1);
        assertEquals(locationDTO.getId(), id);
        assertEquals(locationDTO.getCityName(), cityName);
        assertEquals(locationDTO.getName(), name);
        assertEquals(locationDTO.getIata(), iata);
        assertEquals(locationDTO.getCityIata(), cityIata);
        assertEquals(locationDTO.getLocationType(), locationType);
        assertEquals(locationDTO.getCountryCode(), countryCode);
        assertEquals(locationDTO.getCountryName(), countryName);
    }

    @Test
    public void testBuildFreeCancellationMap() {
        Integer id = 0;
        Calendar limitTime = Calendar.getInstance();
        ZonedDateTime convertedLimitTime = mock(ZonedDateTime.class);

        FreeCancellationLimit cancellationLimit = mock(FreeCancellationLimit.class);

        when(cancellationLimit.getId()).thenReturn(id);
        when(cancellationLimit.getLimitTime()).thenReturn(limitTime);
        when(dateUtils.convertFromCalendar(limitTime)).thenReturn(convertedLimitTime);
        when(legend.getFreeCancellationLimits()).thenReturn(Collections.singletonList(cancellationLimit));

        mapBuildTest(mapper.buildFreeCancellationMap(legend), convertedLimitTime, id);
    }

    @Test
    public void testBuildBaggageMap() {
        Integer id = 0;

        BaggageConditions baggageConditions = mock(BaggageConditions.class);
        BaggageEstimationFees baggageEstimationFees = mock(BaggageEstimationFees.class);

        when(baggageEstimationFees.getId()).thenReturn(id);
        when(baggageEstimationFees.getBaggageConditions()).thenReturn(baggageConditions);
        when(legend.getBaggageEstimationFees()).thenReturn(Collections.singletonList(baggageEstimationFees));

        mapBuildTest(mapper.buildBaggageMap(legend), baggageConditions, id);
    }

    @Test
    public void testBuildCollectionMethodMap() {
        Integer id = 1;
        CollectionMethod collectionMethod = mock(CollectionMethod.class);

        when(legend.getCollectionMethods()).thenReturn(Collections.singletonList(collectionMethod));
        when(collectionMethod.getId()).thenReturn(id);

        mapBuildTest(mapper.buildCollectionMethodMap(legend), collectionMethod, id);
    }

    @Test
    public void testBuildCollectionFeesMap() {
        Integer id = 1;
        CollectionEstimationFees collectionEstimation = mock(CollectionEstimationFees.class);

        when(legend.getCollectionEstimationFees()).thenReturn(Collections.singletonList(collectionEstimation));
        when(collectionEstimation.getId()).thenReturn(id);

        mapBuildTest(mapper.buildCollectionFeesMap(legend), collectionEstimation, id);
    }

    @Test
    public void testBuildTechStopsMap() {
        SectionResult sectionResult = mock(SectionResult.class);
        Section section = mock(Section.class);
        TechnicalStop technicalStop = mock(TechnicalStop.class);
        LocationDTO locationDTO = mock(LocationDTO.class);
        Calendar departureDate = mock(Calendar.class);
        Calendar arrivalDate = mock(Calendar.class);
        ZonedDateTime techDepartureDate = mock(ZonedDateTime.class);
        ZonedDateTime techArrivalDate = mock(ZonedDateTime.class);

        Integer sectionId = 1;
        Integer locationId = 2;

        Map<Integer, LocationDTO> locationMap = Collections.singletonMap(locationId, locationDTO);

        when(legend.getSectionResults()).thenReturn(Collections.singletonList(sectionResult));
        when(sectionResult.getId()).thenReturn(sectionId);
        when(sectionResult.getSection()).thenReturn(section);
        when(section.getTechnicalStops()).thenReturn(Collections.singletonList(technicalStop));
        when(technicalStop.getLocation()).thenReturn(locationId);
        when(technicalStop.getDepartureDate()).thenReturn(departureDate);
        when(technicalStop.getArrivalDate()).thenReturn(arrivalDate);

        when(dateUtils.convertFromCalendar(eq(departureDate))).thenReturn(techDepartureDate);
        when(dateUtils.convertFromCalendar(eq(arrivalDate))).thenReturn(techArrivalDate);

        Map<Integer, List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop>> techStopsMap = mapper.buildTechStopsMap(legend, locationMap);
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop technicalStopDTO = techStopsMap.get(sectionId).get(0);

        assertEquals(techStopsMap.size(), 1);
        assertEquals(techStopsMap.get(sectionId).size(), 1);
        assertEquals(technicalStopDTO.getLocation(), locationDTO);
        assertEquals(technicalStopDTO.getDepartureDate(), techDepartureDate);
        assertEquals(technicalStopDTO.getArrivalDate(), techArrivalDate);
    }

    @Test
    public void testBuildSectionResultMap() {
        SectionResult sectionResult = mock(SectionResult.class);
        Integer sectionResultId = 1;

        when(legend.getSectionResults()).thenReturn(Collections.singletonList(sectionResult));
        when(sectionResult.getId()).thenReturn(sectionResultId);

        mapBuildTest(mapper.buildSectionResultMap(legend), sectionResult, sectionResultId);
    }

    @Test
    public void testBuildHubMap() {
        HubItinerary hub = mock(HubItinerary.class);
        Integer hubId = 1;

        when(itineraries.getHubs()).thenReturn(Collections.singletonList(hub));
        when(hub.getId()).thenReturn(hubId);

        mapBuildTest(mapper.buildHubMap(legend), hub, hubId);
    }

    @Test
    public void testBuildSimplesMap() {
        ItineraryProvider simple = mock(ItineraryProvider.class);
        Integer simpleId = 1;

        when(itineraries.getSimples()).thenReturn(Collections.singletonList(simple));
        when(simple.getId()).thenReturn(simpleId);

        mapBuildTest(mapper.buildSimplesMap(legend), simple, simpleId);
    }

    @Test
    public void testBuildCrossFaringMap() {
        CrossFaringItinerary crossFaring = mock(CrossFaringItinerary.class);
        Integer crossFaringId = 1;

        when(itineraries.getCrossFarings()).thenReturn(Collections.singletonList(crossFaring));
        when(crossFaring.getId()).thenReturn(crossFaringId);

        mapBuildTest(mapper.buildCrossFaringMap(legend), crossFaring, crossFaringId);
    }

    private <T> void mapBuildTest(Map<Integer, T> mapToTest, T mock, Integer mockId) {
        assertEquals(mapToTest.size(), 1);
        assertSame(mapToTest.get(mockId), mock);
    }

}
