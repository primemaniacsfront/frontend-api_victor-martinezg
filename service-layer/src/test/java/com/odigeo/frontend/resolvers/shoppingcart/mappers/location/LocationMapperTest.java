package com.odigeo.frontend.resolvers.shoppingcart.mappers.location;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class LocationMapperTest {

    @Mock
    private com.odigeo.dapi.client.Location location;
    @Mock
    private VisitInformation visit;
    private LocationMapper mapper;
    private ShoppingCartContext context;

    @BeforeMethod
    public void init() {
        initMocks(this);
        mapper = new LocationMapperImpl();
        context = new ShoppingCartContext(visit);
    }

    @Test
    public void testLocationContractToModel() {
        assertNull(mapper.locationContractToModel(null));
    }

    @Test
    public void testLocationIdContractToModel() {
        assertNull(mapper.locationIdContractToModel(null, context));
    }

    @Test
    public void testLocationContractToModelDecorator() {
        assertNull(mapper.locationContractToModel(null));
        when(location.getType()).thenReturn(LocationMapperDecorator.TRAIN_STATION);
        Location loc = mapper.locationContractToModel(location);
        assertEquals(loc.getLocationType(), LocationType.TRAIN_STATION);

        when(location.getType()).thenReturn(LocationMapperDecorator.BUS_STATION);
        loc = mapper.locationContractToModel(location);
        assertEquals(loc.getLocationType(), LocationType.AIRPORT);

        when(location.getType()).thenReturn(" ");
        loc = mapper.locationContractToModel(location);
        assertEquals(loc.getLocationType(), LocationType.OTHER);
    }

}
