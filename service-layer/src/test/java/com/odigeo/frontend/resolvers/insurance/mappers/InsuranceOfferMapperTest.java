package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceUrl;
import com.odigeo.insurance.api.last.insurance.ConditionsUrl;
import com.odigeo.insurance.api.last.insurance.InsuranceResponse;
import com.odigeo.insurance.api.last.insurance.offer.SimpleInsuranceOffer;
import com.odigeo.insurance.api.last.request.OfferId;
import com.odigeo.insurance.api.last.util.Money;
import com.odigeo.insurance.api.last.util.SingleMoney;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class InsuranceOfferMapperTest {

    private static final com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money MONEY = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money();
    private static final InsuranceOfferId INSURANCE_OFFER_ID = new InsuranceOfferId();
    private static final long INSURANCE_ID = 1L;
    private static final String POLICY = "POLICY";
    private static final String TOTAL_PRICE_AMOUNT_CURRENCY_CODE = "EUR";
    private static final BigDecimal TOTAL_PRICE_AMOUNT = BigDecimal.TEN;
    private static final SingleMoney TOTAL_PRICE = new SingleMoney(TOTAL_PRICE_AMOUNT, TOTAL_PRICE_AMOUNT_CURRENCY_CODE);
    private static final InsuranceUrl INSURANCE_URL = new InsuranceUrl();
    private static final List<InsuranceUrl> INSURANCE_URL_LIST = Collections.singletonList(INSURANCE_URL);

    @Mock
    private MoneyMapper moneyMapper;
    @Mock
    private InsuranceOfferIdMapper insuranceOfferIdMapper;
    @Mock
    private InsuranceUrlsMapper insuranceUrlsMapper;

    @InjectMocks
    private InsuranceOfferMapper offerProductMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(insuranceOfferIdMapper.map(Mockito.any())).thenReturn(INSURANCE_OFFER_ID);
        when(moneyMapper.map(Mockito.any(Money.class))).thenReturn(MONEY);
        when(insuranceUrlsMapper.map(Mockito.anyList())).thenReturn(INSURANCE_URL_LIST);
    }

    @Test
    public void map() {
        SimpleInsuranceOffer simpleInsuranceOffer = getSimpleInsuranceOffer();

        InsuranceOfferType insuranceOfferType = offerProductMapper.map(simpleInsuranceOffer);

        assertEquals(insuranceOfferType.getOfferId(), INSURANCE_OFFER_ID);
        assertEquals(insuranceOfferType.getOfferKey(), POLICY);
        assertEquals(insuranceOfferType.getPrice(), MONEY);
        List<InsuranceUrl> conditionUrls = insuranceOfferType.getConditionUrls();
        assertEquals(conditionUrls.size(), INSURANCE_URL_LIST.size());
        assertEquals(conditionUrls.get(0), INSURANCE_URL);
    }

    private SimpleInsuranceOffer getSimpleInsuranceOffer() {
        SimpleInsuranceOffer simpleInsuranceOffer = new SimpleInsuranceOffer();

        InsuranceResponse insuranceResponse = new InsuranceResponse();
        insuranceResponse.setId(INSURANCE_ID);
        insuranceResponse.setPolicy(POLICY);
        insuranceResponse.setConditionsUrls(Collections.singletonList(new ConditionsUrl()));
        simpleInsuranceOffer.setInsurance(insuranceResponse);
        simpleInsuranceOffer.setTotalPrice(TOTAL_PRICE);
        OfferId offerId = new OfferId();
        simpleInsuranceOffer.setOfferId(offerId);

        return simpleInsuranceOffer;
    }
}
