package com.odigeo.frontend.resolvers.shoppingcart;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCart;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketActionStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketResponse;
import com.google.inject.Guice;
import com.odigeo.dapi.client.AvailableProductsResponse;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.services.DapiService;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class ShoppingCartResolverTest {

    private static final String GET_SHOPPING_CART_QUERY = "getShoppingCart";
    private static final String UPDATE_PRODUCTS_SHOPPING_BASKET_MUTATION = "updateProductsInShoppingBasket";
    private static final String QUERY = "Query";
    private static final String MUTATION = "Mutation";
    private static final Long BOOKING_ID = 12L;
    private static final String POLICY = "POLICY";
    private static final String POLICY_2 = "POLICY2";

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DapiService dapiService;
    @Mock
    private ShoppingCartHandler shoppingCartHandler;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private com.odigeo.dapi.client.ShoppingCart shoppingCartDAPI;
    @Mock
    private ShoppingCartSummaryResponse summaryResponse;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private UpdateProductsInShoppingBasketRequest updateProductsInShoppingBasketRequest;

    private ShoppingCartResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        resolver = new ShoppingCartResolver();

        Guice.createInjector(binder -> {
            binder.bind(DapiService.class).toProvider(() -> dapiService);
            binder.bind(ShoppingCartHandler.class).toProvider(() -> shoppingCartHandler);
            binder.bind(JsonUtils.class).toProvider(() -> jsonUtils);
        }).injectMembers(resolver);

        when(env.getContext()).thenReturn(context);
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
        when(graphQLContext.get(VisitInformation.class)).thenReturn(visitInformation);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(QUERY);
        Map<String, DataFetcher> mutations = runtimeWiring.getDataFetchers().get(MUTATION);

        assertNotNull(queries.get(GET_SHOPPING_CART_QUERY));
        assertNotNull(mutations.get(UPDATE_PRODUCTS_SHOPPING_BASKET_MUTATION));
    }

    @Test
    public void testGetShoppingCart() {
        when(jsonUtils.fromDataFetching(env)).thenReturn(BOOKING_ID);
        when(dapiService.getShoppingCart(BOOKING_ID, context)).thenReturn(summaryResponse);
        when(summaryResponse.getShoppingCart()).thenReturn(shoppingCartDAPI);
        when(shoppingCartHandler.buildShoppingCart(eq(shoppingCartDAPI), eq(visitInformation))).thenReturn(shoppingCart);

        assertSame(resolver.getShoppingCart(env), shoppingCart);
        verify(summaryResponse).getShoppingCart();
        verify(graphQLContext).put(eq(com.odigeo.dapi.client.ShoppingCartSummaryResponse.class), eq(summaryResponse));
    }

    @Test
    public void updateProductsInShoppingBasketTest() {
        when(jsonUtils.fromDataFetching(env, UpdateProductsInShoppingBasketRequest.class))
                .thenReturn(updateProductsInShoppingBasketRequest);
        when(updateProductsInShoppingBasketRequest.getShoppingId()).thenReturn(String.valueOf(BOOKING_ID));

        ProductId product1 = new ProductId(POLICY, ProductType.INSURANCE);
        ProductId product2 = new ProductId(POLICY_2, ProductType.INSURANCE);

        List<UpdateProductsInShoppingBasketActionStatus> productsRemoved = Collections.singletonList(new UpdateProductsInShoppingBasketActionStatus(product1, true));
        List<UpdateProductsInShoppingBasketActionStatus> productsAdded = Collections.singletonList(new UpdateProductsInShoppingBasketActionStatus(product2, true));

        when(shoppingCartHandler.getAvailableProductsFromDapi(any(), any())).thenReturn(new AvailableProductsResponse());
        when(shoppingCartHandler.removeProducts(any(), any(), any())).thenReturn(productsRemoved);
        when(shoppingCartHandler.addProducts(any(), any(), any())).thenReturn(productsAdded);

        UpdateProductsInShoppingBasketResponse response = resolver.updateProductsInShoppingBasket(env);

        assertTrue(response.getStatus().size() == 2);
        assertEquals(response.getStatus().get(0).getProductId().getId(), POLICY);
        assertTrue(response.getStatus().get(0).getSuccess());
        assertEquals(response.getStatus().get(1).getProductId().getId(), POLICY_2);
        assertTrue(response.getStatus().get(1).getSuccess());
    }

}
