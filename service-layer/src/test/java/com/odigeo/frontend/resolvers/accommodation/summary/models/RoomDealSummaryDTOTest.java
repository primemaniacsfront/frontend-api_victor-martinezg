package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;
import com.odigeo.frontend.resolvers.accommodation.commons.model.BoardTypeDTO;

public class RoomDealSummaryDTOTest extends BeanTest<RoomDealSummaryDTO> {

    public RoomDealSummaryDTO getBean() {
        RoomDealSummaryDTO bean = new RoomDealSummaryDTO();
        bean.setRoomId("roomID");
        bean.setRoomKey("roomKey");
        bean.setDescription("description");
        bean.setBedsDescription("beds description");
        bean.setCancelPolicy("cancellation policy");
        bean.setSmokingPreference(RoomSmokingPreferenceDTO.NON_SMOKING);
        bean.setProviderId("OC");
        bean.setCancellationFree(true);
        bean.setDepositRequired(true);
        bean.setBoardType(BoardTypeDTO.RO);
        bean.setPaymentAtDestination(true);

        return bean;
    }
}
