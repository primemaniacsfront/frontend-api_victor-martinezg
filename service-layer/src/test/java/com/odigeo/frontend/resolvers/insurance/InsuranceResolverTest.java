package com.odigeo.frontend.resolvers.insurance;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOffersRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.OfferId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectInsuranceOfferRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketId;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.insurance.handlers.InsuranceHandler;
import com.odigeo.frontend.resolvers.insurance.handlers.InsuranceProductHandler;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class InsuranceResolverTest {

    private static final String VISIT_INFORMATION_SERIALIZED = "visitInformationSerialized";
    private static final String GET_INSURANCE_OFFERS_QUERY = "getInsuranceOffers";
    private static final String SELECT_INSURANCE_OFFER_MUTATION = "selectInsuranceOffer";
    private static final String VERSION = "v1";
    private static final String OFFER_ID = "offerId";

    @Mock
    private InsuranceHandler insuranceHandler;
    @Mock
    private InsuranceProductHandler insuranceProductHandler;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;

    @InjectMocks
    private InsuranceResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(env.getContext()).thenReturn(context);
        VisitInformation visitInformation = mock(VisitInformation.class);
        when(context.getVisitInformation()).thenReturn(visitInformation);
        when(visitInformation.getVisitCode()).thenReturn(VISIT_INFORMATION_SERIALIZED);
    }

    @Test
    public void addToBuilderQueries() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(GET_INSURANCE_OFFERS_QUERY));
    }

    @Test
    public void addToBuilderMutation() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Mutation");

        assertNotNull(queries.get(SELECT_INSURANCE_OFFER_MUTATION));
    }

    @Test
    public void testInsuranceOffersFetcher() {
        InsuranceOffersRequest insuranceOffersRequest = new InsuranceOffersRequest();
        when(jsonUtils.fromDataFetching(env, InsuranceOffersRequest.class)).thenReturn(insuranceOffersRequest);
        InsuranceOfferType insuranceOfferType = new InsuranceOfferType();
        List<InsuranceOfferType> insuranceOfferTypes = Collections.singletonList(insuranceOfferType);
        when(insuranceHandler.getInsuranceOffers(anyLong(), anyString())).thenReturn(insuranceOfferTypes);

        assertSame(insuranceOfferTypes, resolver.insuranceOffersFetcher(env).getInsurances());
    }

    @Test
    public void testSelectInsuranceOfferFetcher() {
        SelectInsuranceOfferRequest selectInsuranceOfferRequest = new SelectInsuranceOfferRequest();
        selectInsuranceOfferRequest.setOfferId(new OfferId(OFFER_ID, VERSION));
        selectInsuranceOfferRequest.setShoppingBasketId(new ShoppingBasketId(60L));
        when(jsonUtils.fromDataFetching(env, SelectInsuranceOfferRequest.class)).thenReturn(selectInsuranceOfferRequest);
        ProductId productId = new ProductId();

        when(insuranceProductHandler.selectInsuranceOfferRequest(any())).thenReturn(productId);

        assertSame(productId, resolver.selectInsuranceOfferFetcher(env).getProductId());
    }

}
