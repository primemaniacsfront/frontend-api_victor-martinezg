package com.odigeo.frontend.resolvers.accommodation.summary.mappers;

import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationRoomServiceDTO;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationRoomService;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class AccommodationRoomServicesMapperTest {

    private AccommodationRoomServicesMapper mapper = new AccommodationRoomServicesMapper();

    @Test
    public void testMap() {
        AccommodationRoomService accommodationRoomService = new AccommodationRoomService();
        accommodationRoomService.setType("type");
        accommodationRoomService.setDescription("description");

        AccommodationRoomServiceDTO dto = mapper.map(accommodationRoomService);

        assertEquals(dto.getType(), accommodationRoomService.getType());
        assertEquals(dto.getDescription(), accommodationRoomService.getDescription());
    }

    @Test
    public void testMapNullValues() {
        AccommodationRoomServiceDTO dto = mapper.map(new AccommodationRoomService());

        assertNull(dto.getType());
        assertNull(dto.getDescription());
    }

}
