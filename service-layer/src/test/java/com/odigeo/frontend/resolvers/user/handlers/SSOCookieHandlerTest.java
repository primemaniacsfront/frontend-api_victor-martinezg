package com.odigeo.frontend.resolvers.user.handlers;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResponseInfo;
import com.odigeo.userprofiles.api.v2.model.SSOToken;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SSOCookieHandlerTest {
    private static final String TOKEN = "abcdefghijklmnopqrstuvwxyz";
    private static final int MAX_EXPIRATION_AGE_SSO_TOKEN_COOKIE = 60 * 60;
    private static final int MAX_EXPIRATION_AGE_SSO_TOKEN_PERSISTENT_LOGIN_COOKIE = 60 * 60 * 24 * 180;
    @Mock
    private ResolverContext context;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateSSOCookieWithoutPersistentLogin() {
        ResponseInfo responseInfo = setUpCreateSSOCookie(false);
        assertEquals(responseInfo.getCookie(SiteCookies.SSO_TOKEN).getMaxAge(), MAX_EXPIRATION_AGE_SSO_TOKEN_COOKIE);
    }

    @Test
    public void testCreateSSOCookieWithPersistentLogin() {
        ResponseInfo responseInfo = setUpCreateSSOCookie(true);
        assertEquals(responseInfo.getCookie(SiteCookies.SSO_TOKEN).getMaxAge(), MAX_EXPIRATION_AGE_SSO_TOKEN_PERSISTENT_LOGIN_COOKIE);
    }

    private ResponseInfo setUpCreateSSOCookie(boolean persistentLogin) {
        ResponseInfo responseInfo = new ResponseInfo();
        when(context.getResponseInfo()).thenReturn(responseInfo);
        SSOCookieHandler.createSSOCookie(context, setUpSSOToken(), persistentLogin);
        return responseInfo;
    }

    private SSOToken setUpSSOToken() {
        SSOToken ssoToken = new SSOToken();
        ssoToken.setToken(TOKEN);

        return ssoToken;
    }
}
