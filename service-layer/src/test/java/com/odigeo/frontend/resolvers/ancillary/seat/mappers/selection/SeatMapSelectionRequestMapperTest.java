package com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MoneyRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedSeatRequest;
import com.odigeo.frontend.resolvers.ancillary.seat.mappers.MoneyMapper;
import com.odigeo.itineraryapi.v1.request.Money;
import com.odigeo.itineraryapi.v1.request.SeatMapSelectionRequest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SeatMapSelectionRequestMapperTest {

    @Mock
    private MoneyMapper moneyMapper;

    @InjectMocks
    private SeatMapSelectionRequestMapper seatMapSelectionRequestMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(moneyMapper.map(any(MoneyRequest.class))).thenReturn(new Money());
    }

    @Test
    public void map() {
        List<SelectedSeatRequest> inputSelectedSeatRequestList = new ArrayList<>();
        SelectedSeatRequest inputSelectedSeatRequest = new SelectedSeatRequest();
        inputSelectedSeatRequest.setSection(0);
        SeatRequest inputSeatRequest = new SeatRequest();
        inputSeatRequest.setColumn("A");
        inputSeatRequest.setFloor(0);
        inputSeatRequest.setRow(1);
        inputSeatRequest.setSeatMapRow(1);
        inputSeatRequest.setDisplayedPrice(new MoneyRequest());
        inputSelectedSeatRequest.setSeat(inputSeatRequest);
        inputSelectedSeatRequestList.add(inputSelectedSeatRequest);

        List<SeatMapSelectionRequest> outputSeatMapSelectionRequestList = seatMapSelectionRequestMapper.map(inputSelectedSeatRequestList);
        SeatMapSelectionRequest outputSeatMapSelectionRequest = outputSeatMapSelectionRequestList.get(0);

        assertEquals(outputSeatMapSelectionRequestList.size(), inputSelectedSeatRequestList.size());
        assertEquals(outputSeatMapSelectionRequest.getColumn(), inputSeatRequest.getColumn());
        assertEquals(outputSeatMapSelectionRequest.getFloor().intValue(), inputSeatRequest.getFloor());
        assertEquals(outputSeatMapSelectionRequest.getRow().intValue(), inputSeatRequest.getRow());
        assertEquals(outputSeatMapSelectionRequest.getSeatMapRow().intValue(), inputSeatRequest.getSeatMapRow());
    }

    @Test
    public void mapNullInput() {
        List<SeatMapSelectionRequest> outputSeatMapSelectionRequestList = seatMapSelectionRequestMapper.map(null);

        assertTrue(outputSeatMapSelectionRequestList.isEmpty());
    }
}

