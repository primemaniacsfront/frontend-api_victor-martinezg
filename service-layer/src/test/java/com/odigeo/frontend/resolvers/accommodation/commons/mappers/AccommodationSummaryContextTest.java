package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import bean.test.BeanTest;
import com.odigeo.dapi.client.AccommodationDealInformation;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.geoapi.v4.responses.City;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

public class AccommodationSummaryContextTest extends BeanTest<AccommodationSummaryContext> {

    @Mock
    private AccommodationShoppingItem accommodationShoppingItem;
    @Mock
    private AccommodationDealInformation accommodationDealInformation;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(accommodationShoppingItem.getAccommodationDealInformation()).thenReturn(accommodationDealInformation);
    }

    @Override
    protected AccommodationSummaryContext getBean() {
        AccommodationSummaryContext bean = new AccommodationSummaryContext(new City());
        return bean;
    }

    @Test
    public void testFillPaymentAtDestinationWithoutDealInfo() {
        AccommodationSummaryContext context = getBean();
        context.fillPaymentAtDestination(accommodationShoppingItem);
        assertFalse(context.isPaymentAtDestination());
    }

    @Test
    public void testFillPaymentAtDestination() {
        when(accommodationDealInformation.isPaymentAtDestination()).thenReturn(true);
        AccommodationSummaryContext context = getBean();
        context.fillPaymentAtDestination(accommodationShoppingItem);
        assertTrue(context.isPaymentAtDestination());
    }
}
