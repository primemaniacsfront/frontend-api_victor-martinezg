package com.odigeo.frontend.resolvers.search.handlers.farefamily;

import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.product.ProductCategorySearchResult;
import com.odigeo.searchengine.v2.searchresults.product.itinerary.AbstractFareItinerary;
import com.odigeo.searchengine.v2.searchresults.product.itinerary.pricing.FareItineraryPriceCalculator;
import com.odigeo.searchengine.v2.searchresults.util.money.SingleCurrencyMoney;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SearchResultPriceMapperTest {

    private static final String ITINERARY = "ITINERARY";
    private static final BigDecimal AMOUNT = BigDecimal.ONE;

    @Mock
    SearchResults searchResults;
    @Mock
    Map<String, ProductCategorySearchResult> categorySearchResultMap;
    @Mock
    ProductCategorySearchResult productCategorySearchResult;
    @Mock
    List searchResultItems;

    @Mock
    AbstractFareItinerary abstractFareItinerary;
    @Mock
    FareItineraryPriceCalculator fareItineraryPriceCalculator;
    @Mock
    SingleCurrencyMoney money;

    private SearchResultPriceMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new SearchResultPriceMapper();

        when(searchResults.getCategoryResultsMap()).thenReturn(categorySearchResultMap);
        when(categorySearchResultMap.get(ITINERARY)).thenReturn(productCategorySearchResult);
        when(productCategorySearchResult.getSearchItems()).thenReturn(searchResultItems);
        when(searchResultItems.get(0)).thenReturn(abstractFareItinerary);

        when(abstractFareItinerary.getFareItineraryPriceCalculator()).thenReturn(fareItineraryPriceCalculator);
        when(fareItineraryPriceCalculator.getApparentPrice()).thenReturn(money);
        when(money.isSingleMoney()).thenReturn(Boolean.TRUE);
        when(money.getAmount()).thenReturn(AMOUNT);
    }

    @Test
    public void testMap() {
        assertEquals(mapper.map(searchResults), AMOUNT);
    }

}
