package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelection;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.searchengine.v2.responses.ExternalSelectionResponse;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerarySearchResults;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class MetasHandlerTest {

    @Mock
    private SearchResponse searchResponse;
    @Mock
    private ItinerarySearchResults itinerarySearchResults;
    @Mock
    private ExternalSelectionResponse externalSelectionResponse;

    private MetasHandler handler;
    private SearchResponseDTO searchResponseDTO;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new MetasHandler();
        searchResponseDTO = new SearchResponseDTO();
    }

    @Test
    public void testPopulateExternalSelection()  {
        String fareItineraryId = "id";
        List<String> segmentKeys = new ArrayList<>(0);

        when(searchResponse.getItinerarySearchResults()).thenReturn(itinerarySearchResults);
        when(itinerarySearchResults.getExternalSelectionResponse()).thenReturn(externalSelectionResponse);
        when(externalSelectionResponse.getFareItineraryKey()).thenReturn(fareItineraryId);
        when(externalSelectionResponse.getSegmentKeys()).thenReturn(segmentKeys);

        handler.populateExternalSelection(searchResponse, searchResponseDTO);
        ExternalSelection externalSelection = searchResponseDTO.getExternalSelection();

        assertEquals(externalSelection.getId(), fareItineraryId);
        assertSame(externalSelection.getSegmentKeys(), segmentKeys);
    }

    @Test
    public void testPopulateExternalSelectionNoItineraries() {
        handler.populateExternalSelection(searchResponse, searchResponseDTO);
        assertNull(searchResponseDTO.getExternalSelection());
    }

    @Test
    public void testPopulateExternalSelectionNoExternalResponse() {
        when(searchResponse.getItinerarySearchResults()).thenReturn(itinerarySearchResults);
        handler.populateExternalSelection(searchResponse, searchResponseDTO);
        assertNull(searchResponseDTO.getExternalSelection());
    }

}
