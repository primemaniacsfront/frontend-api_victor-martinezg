package com.odigeo.frontend.resolvers.checkout.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.resolvers.checkout.handlers.dapi.ShoppingCartCheckoutHandler;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertSame;

public class CheckoutFactoryTest {

    @Mock
    private ShoppingCartCheckoutHandler shoppingCartCheckoutHandler;

    @InjectMocks
    private CheckoutFactory factory;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetInstance() {
        CheckoutHandler handler = factory.getInstance(ShoppingType.CART);
        assertSame(handler, shoppingCartCheckoutHandler);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetInstanceException() {
        factory.getInstance(ShoppingType.BASKET_V3);
    }

}
