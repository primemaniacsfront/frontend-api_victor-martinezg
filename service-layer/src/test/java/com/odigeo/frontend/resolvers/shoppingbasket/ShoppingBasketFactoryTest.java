package com.odigeo.frontend.resolvers.shoppingbasket;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Inject;
import com.odigeo.frontend.resolvers.shoppingbasket.factories.ShoppingBasketFactory;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.ShoppingBasketHandler;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.ShoppingBasketOperations;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.ShoppingBasketV2Handler;
import org.apache.jcs.access.exception.CacheException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ShoppingBasketFactoryTest {

    @Mock
    private ShoppingBasketV2Handler shoppingBasketV2Handler;
    @Inject
    private ShoppingBasketHandler shoppingBasketHandler;

    @InjectMocks
    private ShoppingBasketFactory testClass;

    @BeforeMethod
    private void setUp() throws CacheException {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testDefaultCase() {
        ShoppingBasketOperations instance = testClass.getInstance(null);
        assertEquals(instance, shoppingBasketV2Handler);
    }

    @Test
    public void testDefaultCaseV2() {
        ShoppingBasketOperations instance = testClass.getInstance(ShoppingType.BASKET_V2);
        assertEquals(instance, shoppingBasketV2Handler);
    }

    @Test
    public void testV3Case() {
        ShoppingBasketOperations instance = testClass.getInstance(ShoppingType.BASKET_V3);
        assertEquals(instance, shoppingBasketHandler);
    }
}
