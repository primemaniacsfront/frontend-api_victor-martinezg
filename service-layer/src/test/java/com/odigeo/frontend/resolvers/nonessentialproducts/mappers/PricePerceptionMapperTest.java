package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerception;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.PricePerceptionType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class PricePerceptionMapperTest {

    private PricePerceptionMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new PricePerceptionMapper();
    }

    @Test
    public void map() {
        PricePerception pricePerception = mapper.map(PricePerceptionType.PERPAX);

        assertNotNull(pricePerception);
        assertEquals(pricePerception.getType(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerceptionType.PERPAX);
    }

    @Test
    public void mapShouldReturnDefault() {
        PricePerception pricePerception = mapper.map(null);

        assertNotNull(pricePerception);
        assertEquals(pricePerception.getType(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerceptionType.PERPLAN);
    }

}
