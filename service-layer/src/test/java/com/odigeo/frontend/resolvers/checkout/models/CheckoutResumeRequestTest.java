package com.odigeo.frontend.resolvers.checkout.models;

import bean.test.BeanTest;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;

public class CheckoutResumeRequestTest extends BeanTest<CheckoutResumeRequest> {

    @Override
    protected CheckoutResumeRequest getBean() {
        CheckoutResumeRequest bean = new CheckoutResumeRequest();
        bean.setShoppingId(1L);
        bean.setInteractionStep(1);
        bean.setParams(Collections.emptyMap());
        bean.setUserPaymentInteractionId(StringUtils.EMPTY);
        return bean;
    }

}
