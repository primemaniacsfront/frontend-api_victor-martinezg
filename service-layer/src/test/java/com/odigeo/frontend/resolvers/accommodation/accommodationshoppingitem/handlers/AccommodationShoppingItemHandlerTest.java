package com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.handlers;

import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.mappers.AccommodationShoppingItemMapper;
import graphql.GraphQLContext;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccommodationShoppingItemHandlerTest {
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private AccommodationShoppingItem shoppingItem;
    @Mock
    private com.odigeo.dapi.client.ShoppingCart shoppingCart;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    private AccommodationShoppingItemMapper mapper;

    @InjectMocks
    private AccommodationShoppingItemHandler handler;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.openMocks(this);
        when(graphQLContext.get(eq(com.odigeo.dapi.client.ShoppingCartSummaryResponse.class))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getAccommodationShoppingItem()).thenReturn(shoppingItem);
    }

    @Test
    public void testMap() {
        handler.map(graphQLContext);
        verify(graphQLContext).get(eq(com.odigeo.dapi.client.ShoppingCartSummaryResponse.class));
        verify(mapper).map(eq(shoppingItem));
    }
}
