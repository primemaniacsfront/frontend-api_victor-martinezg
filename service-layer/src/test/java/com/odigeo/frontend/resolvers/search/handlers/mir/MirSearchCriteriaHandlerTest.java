package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.DeviceType;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.SearchCriteria;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.TripType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class MirSearchCriteriaHandlerTest {

    private static final String GLOBAL_WORD = "GLOBAL";

    private static final Brand BRAND = Brand.ED;
    private static final Site SITE = Site.ES;

    @Mock
    private MirDeviceTypeMapper mirDeviceTypeMapper;
    @Mock
    private MirTripTypeMapper mirTripTypeMapper;
    @Mock
    private VisitInformation visit;
    @Mock
    private SearchRequest searchRequestDTO;
    @Mock
    private SearchResponseDTO searchResponseDTO;
    @Mock
    private ItineraryRequest itineraryRequestDTO;
    @Mock
    private SectionDTO sectionDTO;

    private MirSearchCriteriaHandler builder;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        builder = new MirSearchCriteriaHandler();

        Guice.createInjector(binder -> {
            binder.bind(MirDeviceTypeMapper.class).toProvider(() -> mirDeviceTypeMapper);
            binder.bind(MirTripTypeMapper.class).toProvider(() -> mirTripTypeMapper);
        }).injectMembers(builder);

        when(visit.getSite()).thenReturn(SITE);
        when(visit.getBrand()).thenReturn(BRAND);

        SearchItineraryDTO itineraryDTO = mock(SearchItineraryDTO.class);
        LegDTO legDTO = mock(LegDTO.class);
        SegmentDTO segmentDTO = mock(SegmentDTO.class);

        when(searchRequestDTO.getItinerary()).thenReturn(itineraryRequestDTO);
        when(searchResponseDTO.getItineraries()).thenReturn(Collections.singletonList(itineraryDTO));
        when(itineraryDTO.getLegs()).thenReturn(Collections.singletonList(legDTO));
        when(legDTO.getSegments()).thenReturn(Collections.singletonList(segmentDTO));
        when(segmentDTO.getSections()).thenReturn(Collections.singletonList(sectionDTO));
    }

    @Test
    public void testBuildRatingSearchCriteria() {
        Long visitId = 1L;
        Long searchId = 2L;
        Integer numAdults = 1;
        Integer numChildren = 2;
        String defaultCountry = "country";

        TripType tripType = mock(TripType.class);
        DeviceType deviceType = mock(DeviceType.class);
        ZonedDateTime departureDate = mock(ZonedDateTime.class);

        when(visit.getVisitId()).thenReturn(visitId);
        when(visit.getWebsiteDefaultCountry()).thenReturn(defaultCountry);
        when(searchResponseDTO.getSearchId()).thenReturn(searchId);
        when(itineraryRequestDTO.getNumAdults()).thenReturn(numAdults);
        when(itineraryRequestDTO.getNumChildren()).thenReturn(numChildren);
        when(sectionDTO.getDepartureDate()).thenReturn(departureDate);
        when(mirTripTypeMapper.map(searchRequestDTO)).thenReturn(tripType);
        when(mirDeviceTypeMapper.map(visit)).thenReturn(deviceType);

        SearchCriteria searchCriteria = builder.buildRatingSearchCriteria(searchRequestDTO, searchResponseDTO, visit);

        assertEquals(searchCriteria.getVisitId(), visitId);
        assertEquals(searchCriteria.getSearchId(), searchId);
        assertEquals(searchCriteria.getNumberOfAdults(), numAdults);
        assertEquals(searchCriteria.getNumberOfChildren(), numChildren);
        assertFalse(searchCriteria.getDynPack());
        assertEquals(searchCriteria.getTripType(), tripType);
        assertEquals(searchCriteria.getDeviceType(), deviceType);
        assertEquals(searchCriteria.getBrand(), BRAND.name());
        assertEquals(searchCriteria.getMarket(), defaultCountry);
        assertEquals(searchCriteria.getSearchDateUTC().toLocalDate(), LocalDate.now());
        assertEquals(searchCriteria.getDepartureDateUTC(), departureDate);
    }

    @Test
    public void testBuildRatingSearchCriteriaWithGlobalMarket() {
        when(visit.getSite()).thenReturn(Site.GB);

        SearchCriteria searchCriteria = builder.buildRatingSearchCriteria(searchRequestDTO, searchResponseDTO, visit);
        assertEquals(searchCriteria.getMarket(), GLOBAL_WORD);
    }

}
