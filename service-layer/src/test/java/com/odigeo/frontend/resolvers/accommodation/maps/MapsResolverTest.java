package com.odigeo.frontend.resolvers.accommodation.maps;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationStaticMapRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MapStaticConfiguration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.StaticMapResponse;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.errors.InvalidRequestException;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.GoogleMapsUtils;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationDataHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.HotelDetailsRequestDTO;
import com.odigeo.frontend.services.configurations.maps.AccommodationMapsConfiguration;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.hcsapi.v9.beans.HotelSummaryResponse;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static com.odigeo.frontend.resolvers.accommodation.maps.MapsResolver.STATIC_MAP;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class MapsResolverTest {
    private static final byte[] BYTE_IMAGE = "image??".getBytes(StandardCharsets.UTF_8);
    private static final String PROVIDER_ID = "12";
    private static final String PROVIDER_HOTEL_ID = "21";

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private GraphQLContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private AccommodationStaticMapRequest accommodationStaticMapRequest;
    @Mock
    private AccommodationProviderKeyDTO providerKey;
    @Mock
    private HotelSummaryResponse summaryResponse;
    @Mock
    private HotelSummary hotelSummary;
    @Mock
    private MapStaticConfiguration mapStaticConfiguration;
    @Mock
    private GeoApiContext geoApiContext;
    @Mock
    private AccommodationMapsConfiguration mapsConfiguration;

    @Mock
    private GoogleMapsUtils googleMapsUtils;
    @Mock
    private ContentKeyMapper contentKeyMapper;
    @Mock
    private AccommodationDataHandler accommodationDataHandler;
    private HotelSupplierKey hotelSupplierKey;
    private Map<HotelSupplierKey, HotelSummary> hotelSummaryMap = new HashMap<>();

    @InjectMocks
    private MapsResolver resolver;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        when(env.getGraphQlContext()).thenReturn(context);
        when(context.get(eq(VisitInformation.class))).thenReturn(visit);
        when(contentKeyMapper.extractProviderKeyFromContentKey(anyString())).thenReturn(providerKey);
        when(accommodationStaticMapRequest.getContentKey()).thenReturn(PROVIDER_ID + "#" + PROVIDER_HOTEL_ID);
        when(providerKey.getProviderId()).thenReturn(PROVIDER_ID);
        when(providerKey.getAccommodationProviderId()).thenReturn(PROVIDER_HOTEL_ID);
        when(hotelSummary.getLatitude()).thenReturn(8f);
        when(hotelSummary.getLongitude()).thenReturn(88f);
        when(jsonUtils.fromDataFetching(eq(env), eq(AccommodationStaticMapRequest.class))).thenReturn(accommodationStaticMapRequest);
        when(accommodationStaticMapRequest.getStaticMapConfiguration()).thenReturn(mapStaticConfiguration);
        when(accommodationDataHandler.getAccommodationSummary(any(HotelDetailsRequestDTO.class), eq(visit))).thenReturn(summaryResponse);
        hotelSupplierKey = new HotelSupplierKey(PROVIDER_HOTEL_ID, PROVIDER_ID);
        hotelSummaryMap.put(hotelSupplierKey, hotelSummary);
        when(mapsConfiguration.getApiKey()).thenReturn("");
        when(mapsConfiguration.getSecretKey()).thenReturn("");
        when(mapsConfiguration.getClientId()).thenReturn("");
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);
        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");
        assertNotNull(queries.get(STATIC_MAP));
    }

    @Test
    public void testStaticMapFetcher() throws IOException, InterruptedException, ApiException {
        when(summaryResponse.getHotelSummary()).thenReturn(hotelSummaryMap);
        when(googleMapsUtils.getStaticMap(any(MapStaticConfiguration.class), anyDouble(), anyDouble(), eq(geoApiContext))).thenReturn(BYTE_IMAGE);
        when(googleMapsUtils.createContext(anyString(), anyString(), anyString())).thenReturn(geoApiContext);
        StaticMapResponse response = resolver.staticMapFetcher(env);
        assertEquals(response.getImage(), Base64.getEncoder().encodeToString(BYTE_IMAGE));
        verify(mapsConfiguration).getApiKey();
        verify(mapsConfiguration).getSecretKey();
        verify(mapsConfiguration).getClientId();
    }

    @Test
    public void testStaticMapFetcherNullCoordinates() {
        when(summaryResponse.getHotelSummary()).thenReturn(hotelSummaryMap);
        when(hotelSummary.getLatitude()).thenReturn(null);
        when(hotelSummary.getLongitude()).thenReturn(null);

        assertNull(resolver.staticMapFetcher(env));
    }

    @Test
    public void testStaticMapFetcherNullHotelSummary() {
        hotelSummaryMap.clear();
        assertNull(resolver.staticMapFetcher(env));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testStaticMapFetcherNull() throws IOException, InterruptedException, ApiException {
        when(googleMapsUtils.createContext(anyString(), anyString(), anyString())).thenReturn(geoApiContext);
        when(summaryResponse.getHotelSummary()).thenReturn(hotelSummaryMap);
        when(googleMapsUtils.getStaticMap(any(MapStaticConfiguration.class), anyDouble(), anyDouble(), eq(geoApiContext))).thenThrow(new InvalidRequestException("exception"));

        resolver.staticMapFetcher(env);
    }

    @Test
    public void testRetrieveWrongHotelSummary() throws IOException, InterruptedException, ApiException {
        when(summaryResponse.getHotelSummary()).thenReturn(hotelSummaryMap);
        when(googleMapsUtils.getStaticMap(any(MapStaticConfiguration.class), anyDouble(), anyDouble(), eq(geoApiContext))).thenReturn(BYTE_IMAGE);
        when(googleMapsUtils.createContext(anyString(), anyString(), anyString())).thenReturn(geoApiContext);
        when(providerKey.getProviderId()).thenReturn("wrong");
        when(providerKey.getAccommodationProviderId()).thenReturn("wrong");
        assertNull(resolver.staticMapFetcher(env));
        verify(mapsConfiguration, never()).getApiKey();
        verify(mapsConfiguration, never()).getSecretKey();
        verify(mapsConfiguration, never()).getClientId();
    }
}
