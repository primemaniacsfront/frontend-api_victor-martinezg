package com.odigeo.frontend.resolvers.accommodation.rooms.models;

import bean.test.BeanTest;
import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;

public class RoomDealDTOTest extends BeanTest<RoomDealDTO> {

    @Override
    protected RoomDealDTO getBean() {
        RoomDealDTO bean = new RoomDealDTO();
        bean.setDescription("description");
        bean.setKey("key");
        bean.setProviderHotelId("providerhotelid");
        bean.setProviderId("providerid");
        bean.setProviderRoomId("providerRoomid");
        bean.setDetails(new RoomDetailsDTO());
        bean.setType("roomtype");
        bean.setBoardType("AI");
        bean.setPriceBreakdown(new PriceBreakdownDTO());
        bean.setRoomsLeft(1);
        return bean;
    }
}
