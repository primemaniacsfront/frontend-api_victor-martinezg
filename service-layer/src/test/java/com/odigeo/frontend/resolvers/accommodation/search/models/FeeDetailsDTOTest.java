package com.odigeo.frontend.resolvers.accommodation.search.models;

import bean.test.BeanTest;

import java.math.BigDecimal;


public class FeeDetailsDTOTest extends BeanTest<FeeDetailsDTO> {

    @Override
    protected FeeDetailsDTO getBean() {
        FeeDetailsDTO bean = new FeeDetailsDTO();
        bean.setDiscount(BigDecimal.ONE);
        bean.setTax(BigDecimal.ONE);
        return bean;
    }
}
