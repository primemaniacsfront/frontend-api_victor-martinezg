package com.odigeo.frontend.resolvers.prime.operations;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.exception.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class MembershipShoppingOperationsFactoryTest {
    private static final String EXCEPTION_BASKET_V2 = "shoppingType BASKET_V2 is not implemented";
    private static final String EXCEPTION_BASKET_V3 = "shoppingType BASKET_V3 is not implemented";

    @Mock
    private MembershipShoppingCartOperations membershipShoppingCartOperations;
    @InjectMocks
    private MembershipShoppingOperationsFactory factory;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(MembershipShoppingCartOperations.class).toProvider(() -> membershipShoppingCartOperations);
        });
    }

    @Test
    public void testGetMembershipShoppingCartOperations() {
        assertEquals(factory.getMembershipShoppingOperations(ShoppingType.CART), membershipShoppingCartOperations);
    }

    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = EXCEPTION_BASKET_V2)
    public void testGetMembershipShoppingBasketV2Operations() {
        factory.getMembershipShoppingOperations(ShoppingType.BASKET_V2);
    }

    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = EXCEPTION_BASKET_V3)
    public void testGetMembershipShoppingBasketV3Operations() {
        factory.getMembershipShoppingOperations(ShoppingType.BASKET_V3);
    }
}
