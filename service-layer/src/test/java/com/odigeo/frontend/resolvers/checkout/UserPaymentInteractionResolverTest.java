package com.odigeo.frontend.resolvers.checkout;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteraction;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteractionRequest;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.checkout.handlers.UserPaymentInteractionHandler;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class UserPaymentInteractionResolverTest {

    private static final String USER_PAYMENT_INTERACTION = "retrieveUserPaymentInteraction";
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private UserPaymentInteraction userPaymentInteraction;
    @Mock
    private UserPaymentInteractionHandler handler;
    @Mock
    private UserPaymentInteractionRequest userPaymentInteractionRequest;
    @InjectMocks
    private UserPaymentInteractionResolver resolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(jsonUtils.fromDataFetching(env, UserPaymentInteractionRequest.class)).thenReturn(userPaymentInteractionRequest);
        when(handler.retrieveUserPaymentInteraction(userPaymentInteractionRequest)).thenReturn(userPaymentInteraction);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> query = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(query.get(USER_PAYMENT_INTERACTION));
    }

    @Test
    public void testRetrieveUserPaymentInteractionFetcher() {
        assertEquals(resolver.retrieveUserPaymentInteractionFetcher(env), userPaymentInteraction);
    }
}
