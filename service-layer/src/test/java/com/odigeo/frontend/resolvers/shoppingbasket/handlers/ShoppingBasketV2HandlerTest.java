package com.odigeo.frontend.resolvers.shoppingbasket.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddProductToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductIdRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RemoveProductFromShoppingBasketRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.dapi.ShoppingCartAvailableCollectionMethods;
import com.odigeo.frontend.resolvers.insurance.handlers.InsuranceProductHandler;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.CreateRequestMapper;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ProductRequestMapper;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ShoppingBasketResponseMapper;
import com.odigeo.frontend.services.shoppingbasket.ShoppingBasketV2Service;
import com.odigeo.shoppingbasket.v2.model.Product;
import com.odigeo.shoppingbasket.v2.requests.CreateRequest;
import com.odigeo.shoppingbasket.v2.requests.ProductRequest;
import com.odigeo.shoppingbasket.v2.responses.ShoppingBasketResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Objects;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ShoppingBasketV2HandlerTest {

    private static final com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketResponse SHOPPING_BASKET = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketResponse();
    private static final AvailableCollectionMethodsResponse AVAILABLE_COLLECTION_METHOD_RESPONSE = new AvailableCollectionMethodsResponse();
    private static final String VISIT_INFORMATION_SERIALIZED = "visitInformationSerialized";
    private static final String BUNDLE_ID = "bundleId";
    private static final String PRODUCT_ID = "productId";
    private static final String SHOPPING_BASKET_ID = "1";
    private static final String ADDITIONAL_INFO = "additionalInfo";
    private static final String ERROR_TYPE = "errorType";
    private static final ShoppingBasketResponse SHOPPING_BASKET_RESPONSE = getShoppingBasketResponse();

    @Mock
    private ShoppingCartAvailableCollectionMethods shoppingAvailableCollectionMethods;
    @Mock
    private ShoppingBasketV2Service shoppingBasketV2Service;
    @Mock
    private ShoppingBasketResponseMapper shoppingBasketMapper;
    @Mock
    private ProductRequestMapper productRequestMapper;
    @Mock
    private CreateRequestMapper createRequestMapper;
    @Mock
    private InsuranceProductHandler insuranceProductHandler;
    @Mock
    private ResolverContext context;

    @InjectMocks
    private ShoppingBasketV2Handler shoppingBasketV2Handler;

    private static ShoppingBasketResponse getShoppingBasketResponse() {
        ShoppingBasketResponse shoppingBasketResponse = new ShoppingBasketResponse();

        shoppingBasketResponse.setShoppingBasket(getShoppingBasketWithProducts());
        shoppingBasketResponse.setAdditionalInfo(ADDITIONAL_INFO);
        shoppingBasketResponse.setErrorType(ERROR_TYPE);

        return shoppingBasketResponse;
    }

    private static com.odigeo.shoppingbasket.v2.model.ShoppingBasket getShoppingBasketWithProducts() {
        com.odigeo.shoppingbasket.v2.model.ShoppingBasket shoppingBasket = new com.odigeo.shoppingbasket.v2.model.ShoppingBasket();

        shoppingBasket.setProducts(Arrays.asList(getProduct(null), getProduct(ProductType.INSURANCE)));

        return shoppingBasket;
    }

    private static Product getProduct(ProductType productType) {
        Product product = new Product();

        product.setType(Objects.nonNull(productType) ? productType.name() : null);

        return product;
    }

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(createRequestMapper.map(anyString(), anyString())).thenReturn(new CreateRequest());
        when(shoppingBasketV2Service.create(any(), any())).thenReturn(new ShoppingBasketResponse());
        when(shoppingBasketV2Service.getShoppingBasket(1L, context)).thenReturn(new ShoppingBasketResponse());
        when(shoppingBasketMapper.map(any(com.odigeo.shoppingbasket.v2.responses.ShoppingBasketResponse.class))).thenReturn(SHOPPING_BASKET);
        when(productRequestMapper.map(anyString(), anyString(), anyString())).thenReturn(getProductRequest());
        doNothing().when(insuranceProductHandler).completeInsuranceProduct(any());
        VisitInformation visitInformation = mock(VisitInformation.class);
        when(context.getVisitInformation()).thenReturn(visitInformation);
        when(visitInformation.getVisitCode()).thenReturn(VISIT_INFORMATION_SERIALIZED);
    }

    @Test
    public void createShoppingBasket() {
        assertEquals(shoppingBasketV2Handler.createShoppingBasket(BUNDLE_ID, context), SHOPPING_BASKET);
    }

    @Test
    public void getShoppingBasket() {
        assertEquals(shoppingBasketV2Handler.getShoppingBasket(SHOPPING_BASKET_ID, context), SHOPPING_BASKET);
    }

    @Test
    public void removeProductFromShoppingBasket() {
        RemoveProductFromShoppingBasketRequest removeProductFromShoppingBasketRequest = new RemoveProductFromShoppingBasketRequest();
        removeProductFromShoppingBasketRequest.setShoppingBasketId(SHOPPING_BASKET_ID);
        ProductIdRequest productIdRequest = new ProductIdRequest();
        productIdRequest.setId(PRODUCT_ID);
        productIdRequest.setType(ProductType.INSURANCE);
        removeProductFromShoppingBasketRequest.setProduct(productIdRequest);
        doNothing().when(shoppingBasketV2Service).removeProduct(anyLong(), any(), any());
        when(shoppingBasketV2Service.getShoppingBasket(anyLong(), any())).thenReturn(SHOPPING_BASKET_RESPONSE);

        shoppingBasketV2Handler.removeProduct(removeProductFromShoppingBasketRequest, context);

        verify(shoppingBasketV2Service).removeProduct(anyLong(), any(), any());
    }

    @Test
    public void addProductToShoppingBasket() {
        AddProductToShoppingBasketRequest addProductToShoppingBasketRequest = new AddProductToShoppingBasketRequest();
        ProductIdRequest productIdRequest = new ProductIdRequest();
        productIdRequest.setId(PRODUCT_ID);
        productIdRequest.setType(ProductType.INSURANCE);
        addProductToShoppingBasketRequest.setProduct(productIdRequest);
        addProductToShoppingBasketRequest.setShoppingBasketId(SHOPPING_BASKET_ID);

        shoppingBasketV2Handler.addProduct(addProductToShoppingBasketRequest, context);

        verify(shoppingBasketV2Service).addProduct(anyLong(), any(), any());
    }

    @Test
    public void getAvailableCollectionMethods() {
        when(shoppingAvailableCollectionMethods.getAvailableCollectionMethods(anyString(), any(ResolverContext.class))).thenReturn(AVAILABLE_COLLECTION_METHOD_RESPONSE);
        assertEquals(shoppingBasketV2Handler.getAvailableCollectionMethods(SHOPPING_BASKET_ID, context), AVAILABLE_COLLECTION_METHOD_RESPONSE);
    }

    private ProductRequest getProductRequest() {
        ProductRequest productRequest = new ProductRequest();

        productRequest.setProduct(getProduct(ProductType.INSURANCE));

        return productRequest;
    }
}
