package com.odigeo.frontend.resolvers.search.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AirlineCampaignConfig;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CampaignConfig;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CarbonFootprint;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Perks;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayConfig;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayFareType;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class SearchItineraryDTOTest extends BeanTest<SearchItineraryDTO> {

    private SearchItineraryDTO bean;

    @BeforeMethod
    public void init() {
        bean = new SearchItineraryDTO();
    }

    public SearchItineraryDTO getBean() {
        bean.setId("id");
        bean.setKey("key");
        bean.setItinerariesLink(1);
        bean.setFreeCancellation(mock(ZonedDateTime.class));
        bean.setTicketsLeft(1);
        bean.setMeRating(BigDecimal.ONE);
        bean.setLegs(Collections.emptyList());
        bean.setIndex(3);
        bean.setSortPrice(mock(BigDecimal.class));
        bean.setFees(Collections.singletonList(mock(Fee.class)));
        bean.setHotelXSellingEnabled(Boolean.TRUE);
        bean.setHasFreeRebooking(Boolean.TRUE);
        bean.setHasReliableCarriers(Boolean.TRUE);
        bean.setHasRefundCarriers(Boolean.TRUE);
        bean.setIsFareUpgradeAvailable(Boolean.TRUE);
        bean.setTransportTypes(new HashSet<>(0));
        bean.setPerks(mock(Perks.class));
        bean.setPrimeDayConfig(initPrimeDayConfig());
        bean.setCarbonFootprint(initCarbonFootprint());
        bean.setVin(Boolean.FALSE);
        bean.setCampaignConfig(initCampaignConfig());
        return bean;
    }

    @Test
    public void testGetSegmentsCarrierIds() {
        LegDTO leg = mock(LegDTO.class);
        SegmentDTO segment = mock(SegmentDTO.class);
        Carrier carrier = mock(Carrier.class);
        String carrierId = "id";

        bean.setLegs(Collections.singletonList(leg));

        when(leg.getSegments()).thenReturn(Collections.singletonList(segment));
        when(segment.getCarrier()).thenReturn(carrier);
        when(carrier.getId()).thenReturn(carrierId);

        Set<String> segmentsCarrierIds = bean.getSegmentsCarrierIds();

        assertTrue(segmentsCarrierIds.contains(carrierId));
        assertSame(bean.getSegmentsCarrierIds(), segmentsCarrierIds);
    }

    @Test
    public void testGetSectionsCarrierIds() {
        LegDTO leg = mock(LegDTO.class);
        SegmentDTO segment = mock(SegmentDTO.class);
        SectionDTO section = mock(SectionDTO.class);
        Carrier carrier = mock(Carrier.class);
        String carrierId = "id";

        bean.setLegs(Collections.singletonList(leg));

        when(leg.getSegments()).thenReturn(Collections.singletonList(segment));
        when(segment.getSections()).thenReturn(Collections.singletonList(section));
        when(section.getCarrier()).thenReturn(carrier);
        when(carrier.getId()).thenReturn(carrierId);

        Set<String> sectionsCarrierIds = bean.getSectionsCarrierIds();

        assertTrue(sectionsCarrierIds.contains(carrierId));
        assertSame(bean.getSectionsCarrierIds(), sectionsCarrierIds);
    }

    private PrimeDayConfig initPrimeDayConfig() {
        PrimeDayConfig primeDayConfig = new PrimeDayConfig();
        primeDayConfig.setIsPrimeDayFare(true);
        primeDayConfig.setPrimeDayFareType(PrimeDayFareType.GOLD);
        return primeDayConfig;
    }

    private CampaignConfig initCampaignConfig() {
        return new CampaignConfig(initPrimeDayConfig(), new AirlineCampaignConfig());
    }

    private CarbonFootprint initCarbonFootprint() {
        CarbonFootprint carbonFootprint = new CarbonFootprint();
        carbonFootprint.setTotalCo2Kilos(BigDecimal.ONE);
        carbonFootprint.setTotalCo2eKilos(BigDecimal.TEN);
        carbonFootprint.setEcoPercentageThanAverage(Integer.MAX_VALUE);
        carbonFootprint.setIsEco(true);
        return carbonFootprint;
    }
}
