package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.odigeo.accommodation.scoring.service.AccommodationScore;
import com.odigeo.accommodation.scoring.service.AccommodationScores;
import com.odigeo.accommodation.scoring.service.AccommodationScoringService;
import com.odigeo.accommodation.scoring.service.GetScoresRequest;
import com.odigeo.accommodation.scoring.service.ScoringServiceException;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationScoringRequestDTO;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.AccommodationScoringRequestMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.searchengine.v2.requests.LocationRequest;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.responses.accommodation.SearchType;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AccommodationSortingHandlerTest {

    @Mock
    private AccommodationScoringRequestMapper accommodationScoringRequestMapper;
    @Mock
    private AccommodationScoringService accommodationScoringService;
    @Mock
    private AccommodationSearchResponseDTO accommodationSearchResponseDTO;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private SearchMethodRequest searchRequest;
    @Mock
    private com.odigeo.searchengine.v2.requests.AccommodationSearchRequest accommodationSearchRequest;

    @InjectMocks
    private AccommodationSortingHandler accommodationSortingHandler;

    @BeforeMethod
    public void init() throws ScoringServiceException {
        MockitoAnnotations.openMocks(this);
        when(accommodationScoringRequestMapper.map(any(AccommodationScoringRequestDTO.class))).thenReturn(new GetScoresRequest());
        when(accommodationScoringService.getScores(any(GetScoresRequest.class))).thenReturn(getAccommodationScores());
    }

    @Test
    public void testScores() {
        AccommodationResponseDTO accommodationResponseDTO = new AccommodationResponseDTO();
        SearchAccommodationDTO searchAccommodationDTO = new SearchAccommodationDTO();
        searchAccommodationDTO.setDedupId(123);
        accommodationResponseDTO.setAccommodations(Collections.singletonList(searchAccommodationDTO));
        when(accommodationSearchResponseDTO.getAccommodationResponse()).thenReturn(accommodationResponseDTO);
        when(accommodationSearchResponseDTO.getSearchId()).thenReturn(123L);
        mockAccommodationSearchRequest();
        when(searchRequest.getSearchRequest()).thenReturn(accommodationSearchRequest);
        accommodationSortingHandler.populateScoring(visitInformation, accommodationSearchResponseDTO, searchRequest);
        assertEquals(searchAccommodationDTO.getScore(), BigDecimal.ONE);
    }

    private AccommodationScores getAccommodationScores() {
        AccommodationScores accommodationScores = new AccommodationScores();
        AccommodationScore accommodationScore = new AccommodationScore();
        accommodationScore.setInternalAccommodationId(123);
        accommodationScore.setScores(Collections.singletonMap("OFFLINE", BigDecimal.ONE));
        accommodationScores.setAccommodationScores(Collections.singletonList(accommodationScore));

        return accommodationScores;
    }

    private void mockAccommodationSearchRequest() {
        when(accommodationSearchRequest.getCheckInDate()).thenReturn(Calendar.getInstance());
        when(accommodationSearchRequest.getCheckOutDate()).thenReturn(Calendar.getInstance());
        when(accommodationSearchRequest.getRoomRequests()).thenReturn(Collections.emptyList());
        when(accommodationSearchRequest.getSearchType()).thenReturn(SearchType.STANDALONE);
        when(accommodationSearchRequest.getDestination()).thenReturn(new LocationRequest());
    }
}
