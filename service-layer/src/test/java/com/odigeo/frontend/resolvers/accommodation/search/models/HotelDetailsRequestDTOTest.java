package com.odigeo.frontend.resolvers.accommodation.search.models;


import bean.test.BeanTest;

import java.util.Collections;

public class HotelDetailsRequestDTOTest extends BeanTest<HotelDetailsRequestDTO> {

    @Override
    protected HotelDetailsRequestDTO getBean() {
        HotelDetailsRequestDTO bean = new HotelDetailsRequestDTO();
        bean.setProviderKeys(Collections.emptyList());
        return bean;
    }
}
