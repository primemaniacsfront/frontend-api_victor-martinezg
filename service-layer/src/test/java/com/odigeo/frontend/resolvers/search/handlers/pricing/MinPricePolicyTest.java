package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.google.inject.Guice;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.frontend.resolvers.search.mappers.PaymentMethodMapper;
import com.odigeo.searchengine.v2.responses.collection.CollectionEstimationFees;
import com.odigeo.searchengine.v2.responses.collection.CollectionMethod;
import com.odigeo.searchengine.v2.responses.collection.CollectionMethodKeyPrice;
import com.odigeo.searchengine.v2.responses.collection.CreditCardType;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.FareItineraryPriceCalculator;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MinPricePolicyTest {

    private static final String DEFAULT_FEE_TYPE_ID = "CHEAPEST";

    @Mock
    private FareItinerary fareItinerary;
    @Mock
    private SearchEngineContext seContext;
    @Mock
    private PaymentMethodMapper pmMapper;
    @Mock
    private PaymentMethodManager pmManager;

    private List<CollectionMethodKeyPrice> keyPrices;
    private Map<Integer, CollectionMethod> collectionMethodMap;

    private MinPricePolicy policy;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        policy = new MinPricePolicy();

        Guice.createInjector(binder -> {
            binder.bind(PaymentMethodMapper.class).toProvider(() -> pmMapper);
            binder.bind(PaymentMethodManager.class).toProvider(() -> pmManager);
        }).injectMembers(policy);

        keyPrices = new ArrayList<>();
        collectionMethodMap = new HashMap<>();

        CollectionEstimationFees collectionFees = mock(CollectionEstimationFees.class);
        Integer collectionFeesId = 1;
        when(fareItinerary.getCollectionMethodFees()).thenReturn(collectionFeesId);
        when(collectionFees.getCollectionMethodFees()).thenReturn(keyPrices);

        when(seContext.getCollectionFeesMap()).thenReturn(Collections.singletonMap(collectionFeesId, collectionFees));
        when(seContext.getCollectionMethodMap()).thenReturn(collectionMethodMap);
    }

    @Test
    public void testCalculateFees() {
        BigDecimal price = BigDecimal.TEN;
        BigDecimal cheapestPrice = new BigDecimal("5");
        BigDecimal filteredPrice = BigDecimal.ZERO;
        BigDecimal sortPrice = new BigDecimal("20");
        BigDecimal bigestPrice = sortPrice.subtract(cheapestPrice).add(price);

        Integer id = 1;
        Integer cheapestId = 2;
        Integer filteredId = 3;

        String type = "type";
        String cheapestType = "cheapestType";
        String filteredType = "filteredType";

        PaymentMethod payment = mock(PaymentMethod.class);
        PaymentMethod paymentCheapest = mock(PaymentMethod.class);
        PaymentMethod paymentFiltered = mock(PaymentMethod.class);

        createPaymentMethod(price, id, type, payment, true);
        createPaymentMethod(cheapestPrice, cheapestId, cheapestType, paymentCheapest, true);
        createPaymentMethod(filteredPrice, filteredId, filteredType, paymentFiltered, false);

        String cheapestCardName = "cheapestCreditCard";
        createCreditCard(cheapestId, cheapestCardName);

        FareItineraryPriceCalculator priceCalculator = mock(FareItineraryPriceCalculator.class);
        when(fareItinerary.getPrice()).thenReturn(priceCalculator);
        when(priceCalculator.getSortPrice()).thenReturn(sortPrice);

        List<Fee> itineraryFees = policy.calculateFees(seContext, fareItinerary);

        assertEquals(itineraryFees.size(), 3);
        FeesTester.checkFee(itineraryFees, id.toString(), bigestPrice, null, payment);
        FeesTester.checkFee(itineraryFees, cheapestId.toString(), sortPrice, cheapestCardName, paymentCheapest);
        FeesTester.checkFee(itineraryFees, DEFAULT_FEE_TYPE_ID, sortPrice, cheapestCardName, paymentCheapest);
    }

    private void createPaymentMethod(BigDecimal price, Integer collectionId, String collectionType,
                                     PaymentMethod payment, boolean isSupported) {

        CollectionMethodKeyPrice keyPrice = mock(CollectionMethodKeyPrice.class);
        CollectionMethod collectionMethod = mock(CollectionMethod.class);

        when(keyPrice.getPrice()).thenReturn(price);
        when(keyPrice.getCollectionMethodKey()).thenReturn(collectionId);

        when(collectionMethod.getId()).thenReturn(collectionId);
        when(collectionMethod.getType()).thenReturn(collectionType);

        keyPrices.add(keyPrice);
        collectionMethodMap.put(collectionId, collectionMethod);

        when(pmMapper.map(collectionType)).thenReturn(payment);
        when(pmManager.isSupported(collectionType)).thenReturn(isSupported);
    }

    private void createCreditCard(Integer collectionId, String cardName) {
        CollectionMethod collectionMethod = collectionMethodMap.get(collectionId);

        CreditCardType creditCard = mock(CreditCardType.class);
        when(collectionMethod.getCreditCardType()).thenReturn(creditCard);
        when(creditCard.getName()).thenReturn(cardName);
    }

    @Test
    public void testCalculateFeesEmpty() {
        when(seContext.getCollectionFeesMap()).thenReturn(Collections.emptyMap());

        List<Fee> itineraryFees = policy.calculateFees(seContext, fareItinerary);
        assertTrue(itineraryFees.isEmpty());
    }

    @Test
    public void testCalculateDefaultFeeTypeId() {
        assertEquals(policy.calculateDefaultFeeTypeId(), DEFAULT_FEE_TYPE_ID);
    }

}
