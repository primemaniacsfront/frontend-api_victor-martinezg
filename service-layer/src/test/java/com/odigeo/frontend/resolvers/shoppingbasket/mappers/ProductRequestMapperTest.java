package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.odigeo.shoppingbasket.v2.requests.ProductRequest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ProductRequestMapperTest {

    private static final String ID = "id";
    private static final String TYPE = "type";
    private static final String STATUS = "status";

    @Test
    public void mapProductIdType() {
        ProductRequestMapper productRequestMapper = new ProductRequestMapper();

        ProductRequest map = productRequestMapper.map(ID, TYPE, STATUS);

        assertEquals(map.getProduct().getId(), ID);
        assertEquals(map.getProduct().getType(), TYPE);
        assertEquals(map.getProduct().getStatus(), STATUS);
    }

}
