package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.google.inject.Singleton;
import com.odigeo.shoppingbasket.v2.model.Product;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

@Singleton
public class InsuranceProductMapperTest {

    private static final String PRODUCT_ID = "ID";
    private static final Money MONEY = new Money();

    @Mock
    private MoneyMapper moneyMapper;

    @InjectMocks
    private InsuranceProductMapper insuranceProductMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        when(moneyMapper.map(Mockito.any())).thenReturn(MONEY);
    }

    @Test
    public void map() {
        Product product = new Product();
        product.setId(PRODUCT_ID);

        InsuranceProduct insuranceProduct = insuranceProductMapper.map(product);

        assertEquals(insuranceProduct.getProductId(), PRODUCT_ID);
        assertEquals(insuranceProduct.getPrice(), MONEY);
        assertNull(insuranceProduct.getOfferKey());
    }

}
