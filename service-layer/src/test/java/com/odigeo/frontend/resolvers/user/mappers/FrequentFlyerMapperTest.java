package com.odigeo.frontend.resolvers.user.mappers;

import com.odigeo.userprofiles.api.v2.model.FrequentFlyer;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class FrequentFlyerMapperTest {
    private static final String AIRLINE_CODE = "IB";
    private static final String FREQUENT_FLYER_NUMBER = "123456";
    @Mock
    private FrequentFlyer frequentFlyer;

    private FrequentFlyerMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new FrequentFlyerMapperImpl();
    }

    @Test
    public void testFrequentFlyerContractToModel() {
        when(frequentFlyer.getAirlineCode()).thenReturn(AIRLINE_CODE);
        when(frequentFlyer.getFrequentFlyerNumber()).thenReturn(FREQUENT_FLYER_NUMBER);

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FrequentFlyerCard frequentFlyerCard = mapper.frequentFlyerContractToModel(frequentFlyer);

        assertEquals(frequentFlyerCard.getCarrier(), frequentFlyer.getAirlineCode());
        assertEquals(frequentFlyerCard.getNumber(), frequentFlyer.getFrequentFlyerNumber());
    }
}
