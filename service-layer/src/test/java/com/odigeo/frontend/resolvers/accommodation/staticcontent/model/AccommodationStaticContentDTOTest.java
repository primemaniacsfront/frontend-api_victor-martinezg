package com.odigeo.frontend.resolvers.accommodation.staticcontent.model;

import bean.test.BeanTest;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;


public class AccommodationStaticContentDTOTest extends BeanTest<AccommodationStaticContentDTO> {

    @Override
    protected AccommodationStaticContentDTO getBean() {
        AccommodationStaticContentDTO bean = new AccommodationStaticContentDTO();
        bean.setAccommodationDeal(new AccommodationDealInformationDTO());
        bean.setContentKey("contentKey");
        return bean;
    }
}
