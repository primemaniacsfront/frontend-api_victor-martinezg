package com.odigeo.frontend.resolvers.ancillary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AncillaryResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatMapSegment;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedAncillariesResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectAncillariesRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedAncillaryProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AncillariesSelectionRequest;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.ancillary.seat.mappers.SeatMapSegmentMapper;
import com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection.AncillarySelectionRequestMapper;
import com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection.SelectedAncillariesResponseMapper;
import com.odigeo.frontend.services.ancillary.AncillaryService;
import com.odigeo.itineraryapi.v1.request.AncillariesServicesType;
import com.odigeo.itineraryapi.v1.response.AncillaryOptionsContainer;
import com.odigeo.itineraryapi.v1.response.AvailableAncillariesOptionsResponse;
import com.odigeo.itineraryapi.v1.response.SelectAncillaryServiceResponse;
import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class AncillaryHandlerTest {

    private static final long BOOKING_ID = 1L;
    private static final String SHOPPING_BASKET_ID = "123456";
    private static final Set<AncillariesServicesType> ANCILLARIES_SERVICES = new HashSet<>();
    private static final ResolverContext CONTEXT = new ResolverContext();

    private static final List<SeatMapSegment> SEAT_MAP_SEGMENT_LIST = Collections.singletonList(new SeatMapSegment());

    @Mock
    private AncillaryService ancillaryService;

    @Mock
    private SeatMapSegmentMapper seatMapSegmentMapper;

    @Mock
    private AncillaryResponse ancillaryResponse;

    @Mock
    private AvailableAncillariesOptionsResponse availableAncillariesOptionsResponse;

    @Mock
    private ResolverContext context;

    @Mock
    private VisitInformation visitInformation;

    @Mock
    private DebugInfo debugInfo;

    @Mock
    private SelectAncillariesRequest selectAncillariesRequest;

    @Mock
    private AncillarySelectionRequestMapper ancillarySelectionRequestMapper;

    @Mock
    private SelectAncillaryServiceResponse selectAncillaryServiceResponse;

    @Mock
    private SelectedAncillariesResponseMapper selectedAncillariesResponseMapper;

    @Mock
    private SelectedAncillariesResponse selectedAncillariesResponse;

    @InjectMocks
    private AncillaryHandler ancillaryHandler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void getAncillaries() {
        setUpItineraryApiResponse(availableAncillariesOptionsResponse, new AncillaryOptionsContainer(), SEAT_MAP_SEGMENT_LIST);
        assertEquals(ancillaryHandler.getAncillaries(
                BOOKING_ID,
                ANCILLARIES_SERVICES,
                CONTEXT
        ).getSeats(), SEAT_MAP_SEGMENT_LIST);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testAvailableAncillariesResponseNull() {
        when(ancillaryService.ancillariesOptions(anyLong(), anySet(), any(ResolverContext.class))).thenThrow(ServiceException.class);
        ancillaryHandler.getAncillaries(
            1,
            ANCILLARIES_SERVICES,
            CONTEXT
        );
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testAncillaryOptionsContainerNull() {
        when(ancillaryService.ancillariesOptions(anyLong(), anySet(), any(ResolverContext.class)))
            .thenReturn(availableAncillariesOptionsResponse);
        when(availableAncillariesOptionsResponse.getAncillaryOptionsContainer()).thenReturn(null);
        ancillaryHandler.getAncillaries(
            1,
            ANCILLARIES_SERVICES,
            CONTEXT
        );
    }

    @Test
    public void selectAncillaries() {
        setUpSelectAncillariesTest();
        when(selectedAncillariesResponse.getProducts()).thenReturn(Collections.singletonList(new SelectedAncillaryProduct()));

        SelectedAncillariesResponse outputSelectedAncillariesResponse = ancillaryHandler.selectAncillaries(selectAncillariesRequest, context);

        assertEquals(outputSelectedAncillariesResponse.getProducts().size(), selectedAncillariesResponse.getProducts().size());
    }

    @Test
    public void selectAncillariesEmptySelection() {
        setUpSelectAncillariesTest();
        when(selectedAncillariesResponse.getProducts()).thenReturn(Collections.emptyList());

        SelectedAncillariesResponse outputSelectedAncillariesResponse = ancillaryHandler.selectAncillaries(selectAncillariesRequest, context);

        assertTrue(outputSelectedAncillariesResponse.getProducts().isEmpty());
    }

    private void setUpItineraryApiResponse(AvailableAncillariesOptionsResponse itineraryApiResponse,
                                           AncillaryOptionsContainer container,
                                           List<SeatMapSegment> segments) {
        when(ancillaryService.ancillariesOptions(BOOKING_ID, ANCILLARIES_SERVICES, CONTEXT))
                .thenReturn(itineraryApiResponse);
        when(availableAncillariesOptionsResponse.getAncillaryOptionsContainer()).thenReturn(container);
        when(seatMapSegmentMapper.map(availableAncillariesOptionsResponse.getAncillaryOptionsContainer()))
                .thenReturn(segments);
        when(ancillaryResponse.getSeats()).thenReturn(segments);
    }

    private void setUpSelectAncillariesTest() {
        when(context.getVisitInformation()).thenReturn(visitInformation);
        when(visitInformation.getVisitId()).thenReturn(BOOKING_ID);
        when(visitInformation.getVisitCode()).thenReturn(StringUtils.EMPTY);
        when(context.getDebugInfo()).thenReturn(debugInfo);
        when(debugInfo.isEnabled()).thenReturn(false);
        when(selectAncillariesRequest.getBookingID()).thenReturn(String.valueOf(BOOKING_ID));
        when(selectAncillariesRequest.getShoppingBasketID()).thenReturn(SHOPPING_BASKET_ID);
        when(ancillarySelectionRequestMapper.map(any(AncillariesSelectionRequest.class))).thenReturn(Collections.emptyList());
        when(ancillaryService.selectAncillaries(eq(BOOKING_ID), any())).thenReturn(selectAncillaryServiceResponse);
        when(selectedAncillariesResponseMapper.map(selectAncillaryServiceResponse)).thenReturn(selectedAncillariesResponse);
    }
}
