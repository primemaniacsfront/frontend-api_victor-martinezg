package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MoneyRequest;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;

public class MoneyMapperTest {
    private static final BigDecimal MONEY_AMOUNT = new BigDecimal("1000.58");
    private static final BigDecimal PROVIDER_PRICE = new BigDecimal("500.58");
    private static final BigDecimal FEE = new BigDecimal("500.00");
    private static final String CURRENCY_CODE = "EUR";

    @Test
    public void mapMoney() {
        MoneyMapper moneyMapper = new MoneyMapper();

        Money mappedMoney = moneyMapper.map(getInput());
        Money output = getOutput();

        assertEquals(mappedMoney.getAmount(), output.getAmount());
        assertEquals(mappedMoney.getCurrency(), output.getCurrency());
    }

    @Test
    public void mapMoneyRequest() {
        MoneyMapper moneyMapper = new MoneyMapper();

        com.odigeo.itineraryapi.v1.request.Money mappedMoney = moneyMapper.map(getRequestInput());
        com.odigeo.itineraryapi.v1.request.Money output = getRequestOutput();

        assertEquals(mappedMoney.getAmount(), output.getAmount());
        assertEquals(mappedMoney.getCurrencyCode(), output.getCurrencyCode());
    }

    private  com.odigeo.itineraryapi.v1.response.Money getInput() {
        com.odigeo.itineraryapi.v1.response.Money moneyInput = new  com.odigeo.itineraryapi.v1.response.Money();

        moneyInput.setCurrencyCode(CURRENCY_CODE);
        moneyInput.setAmount(MONEY_AMOUNT);

        return moneyInput;
    }

    private Money getOutput() {
        Money moneyOutput = new Money();

        moneyOutput.setCurrency(CURRENCY_CODE);
        moneyOutput.setAmount(MONEY_AMOUNT);

        return moneyOutput;
    }

    private MoneyRequest getRequestInput() {
        MoneyRequest moneyInput = new MoneyRequest();

        moneyInput.setCurrency(CURRENCY_CODE);
        moneyInput.setAmount(MONEY_AMOUNT.doubleValue());

        return moneyInput;
    }

    private com.odigeo.itineraryapi.v1.request.Money getRequestOutput() {
        com.odigeo.itineraryapi.v1.request.Money moneyOutput = new com.odigeo.itineraryapi.v1.request.Money();

        moneyOutput.setCurrencyCode(CURRENCY_CODE);
        moneyOutput.setAmount(MONEY_AMOUNT);

        return moneyOutput;
    }
}
