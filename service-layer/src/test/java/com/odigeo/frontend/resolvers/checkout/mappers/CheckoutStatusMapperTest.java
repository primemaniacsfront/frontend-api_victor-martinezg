package com.odigeo.frontend.resolvers.checkout.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutStatus;
import com.odigeo.dapi.client.BookingResponseStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CheckoutStatusMapperTest {

    private CheckoutStatusMapper mapper;

    @BeforeMethod
    public void init() {
        mapper = new CheckoutStatusMapperImpl();
    }

    @Test
    public void testMapShoppingCartStatusToContract() {
        assertEquals(mapper.mapShoppingCartStatusToContract(null), CheckoutStatus.FAILED);

        assertEquals(mapper.mapShoppingCartStatusToContract(BookingResponseStatus.BOOKING_CONFIRMED), CheckoutStatus.SUCCESS);
        assertEquals(mapper.mapShoppingCartStatusToContract(BookingResponseStatus.BOOKING_REPRICING), CheckoutStatus.REPRICING);
        assertEquals(mapper.mapShoppingCartStatusToContract(BookingResponseStatus.USER_INTERACTION_NEEDED), CheckoutStatus.USER_PAYMENT_INTERACTION);
        assertEquals(mapper.mapShoppingCartStatusToContract(BookingResponseStatus.BROKEN_FLOW), CheckoutStatus.STOP);
        assertEquals(mapper.mapShoppingCartStatusToContract(BookingResponseStatus.BOOKING_STOP), CheckoutStatus.STOP);
    }

}
