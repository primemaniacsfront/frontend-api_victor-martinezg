package com.odigeo.frontend.resolvers.user;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipTokenResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPageName;
import com.odigeo.frontend.resolvers.prime.pages.MembershipPageFactory;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.frontend.services.UserDescriptionService;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.user.MembershipResolver.GET_USER_MEMBERSHIP_QUERY;
import static com.odigeo.frontend.resolvers.user.MembershipResolver.UPDATE_USER_MEMBERSHIP_QUERY;
import static com.odigeo.frontend.resolvers.user.MembershipResolver.TOKEN_INFO;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class MembershipResolverTest {
    private static final String VALID_TOKEN_REQUEST = "abcde";

    @Mock
    private MembershipHandler membershipHandler;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private Membership membership;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private MembershipTokenResponse membershipTokenResponse;
    @Mock
    private MembershipPageFactory membershipPageFactory;
    @Mock
    private UserDescriptionService userDescriptionService;
    @Mock
    private MembershipRequest membershipRequest;
    @Mock
    private ShoppingInfo shoppingInfo;
    @Mock
    private ResolverContext context;
    @InjectMocks
    private MembershipResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
        when(env.getGraphQlContext()).thenReturn(GraphQLContext.newContext().build());
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");
        Map<String, DataFetcher> mutations = runtimeWiring.getDataFetchers().get("Mutation");

        assertNotNull(queries.get(GET_USER_MEMBERSHIP_QUERY));
        assertNotNull(queries.get(TOKEN_INFO));
        assertNotNull(mutations.get(UPDATE_USER_MEMBERSHIP_QUERY));
    }

    @Test
    public void testInfoDataFetcher() {
        when(membershipHandler.getMembershipFromLoggedUser(env)).thenReturn(membership);
        assertEquals(resolver.membershipFromLoggedUserDataFetcher(env), membership);
    }

    @Test
    public void testUpdateMembershipDataFetcher() {
        when(membershipHandler.updateMembershipData(eq(env))).thenReturn(membership);
        assertEquals(resolver.updateMembershipDataFetcher(env), membership);
    }

    @Test
    public void testGetTokenInfo() {
        when(jsonUtils.fromDataFetching(eq(env))).thenReturn(VALID_TOKEN_REQUEST);
        when(membershipHandler.getTokenInfo(VALID_TOKEN_REQUEST)).thenReturn(membershipTokenResponse);
        assertEquals(resolver.tokenInfo(env), membershipTokenResponse);
    }
    @Test
    public void testMembershipPagesFetcher() {
        when(membershipPageFactory.getPage(eq(env))).thenReturn(MembershipPageName.SUBSCRIPTION_FTL);
        assertEquals(resolver.membershipPagesFetcher(env).getPages().size(), 1);
        assertEquals(resolver.membershipPagesFetcher(env).getPages().get(0), MembershipPageName.SUBSCRIPTION_FTL);
    }

    @Test
    public void testMembershipPagesFetcherWhenNull() {
        when(membershipPageFactory.getPage(eq(env))).thenReturn(null);
        assertEquals(resolver.membershipPagesFetcher(env).getPages().size(), 0);
    }

    @Test
    public void testMembershipDataFetcherWhenUserLogged() {
        when(membershipHandler.getMembership(env, null)).thenReturn(membership);
        assertSame(resolver.membershipDataFetcher(env), membership);
    }

    @Test
    public void testMembershipDataFetcherWhenUserNotLogged() {
        when(jsonUtils.fromDataFetching(env, MembershipRequest.class)).thenReturn(membershipRequest);
        when(membershipRequest.getShoppingInfo()).thenReturn(shoppingInfo);
        when(membershipHandler.getMembership(env, shoppingInfo)).thenReturn(membership);
        assertSame(resolver.membershipDataFetcher(env), membership);
    }

    @Test
    public void testMembershipDataFetcherWhenUserLoggedNotPrimeUser() {
        when(userDescriptionService.isUserLogged(context)).thenReturn(Boolean.TRUE);
        when(membershipHandler.getMembershipFromLoggedUser(env)).thenReturn(null);
        assertNull(resolver.membershipDataFetcher(env));
    }

    @Test
    public void testMembershipDataFetcherWhenUserNotLoggedNotPrimeUser() {
        when(jsonUtils.fromDataFetching(env, MembershipRequest.class)).thenReturn(membershipRequest);
        when(membershipRequest.getShoppingInfo()).thenReturn(shoppingInfo);
        when(membershipHandler.getMembership(env, shoppingInfo)).thenReturn(null);
        assertNull(resolver.membershipDataFetcher(env));
    }
}
