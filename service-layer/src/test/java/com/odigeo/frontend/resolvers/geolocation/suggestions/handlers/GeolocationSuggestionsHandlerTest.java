package com.odigeo.frontend.resolvers.geolocation.suggestions.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestion;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsInputType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.geolocation.suggestions.mappers.GeolocationSuggestionsMapper;
import com.odigeo.frontend.resolvers.geolocation.suggestions.models.GeolocationSuggestionsRequestDTO;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.request.ProductType;
import com.odigeo.geoapi.v4.responses.LocationDescriptionSorted;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class GeolocationSuggestionsHandlerTest {

    private static final Integer GEO_NODE_ID = 9646;
    private static final String IATA_CODE = "BCN";
    private static final String CITY_NAME = "Barcelona";
    private static final String COUNTRY_NAME = "Spain";
    private static final String GEO_NODE_TYPE_CITY = "CITY";
    private static final String SEARCH_WORD = "bar";
    private static final String LOCALE = "es_ES";
    private static final String WEBSITE = "ES";
    private static final String INPUT_TYPE = "DEPARTURE";

    @Mock
    private VisitInformation visit;
    @Mock
    private GeoService geoService;
    @Mock
    private GeolocationSuggestionsRequest geolocationSuggestionsRequest;
    @Mock
    private GeolocationSuggestionsRequestDTO geolocationSuggestionsRequestDTO;
    @Mock
    private GeolocationSuggestion geolocationSuggestion;
    @Mock
    private ResolverContext context;
    @Mock
    private GeolocationSuggestionsMapper geolocationSuggestionsMapper;
    @InjectMocks
    private GeolocationSuggestionsHandler geolocationSuggestionsHandler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(context.getVisitInformation()).thenReturn(visit);
        when(visit.getSite()).thenReturn(Site.ES);
        when(visit.getLocale()).thenReturn(LOCALE);
    }

    @Test
    public void testBuildGeolocationSuggestionsRequest() {
        setUpGeolocationSuggestionsRequest();

        GeolocationSuggestionsRequestDTO request = geolocationSuggestionsHandler.buildGeolocationSuggestionsRequest(geolocationSuggestionsRequest, visit);

        assertEquals(request.getSearchWord(), SEARCH_WORD);
        assertEquals(request.getLocale(), LOCALE);
        assertEquals(request.getProductType(), ProductType.FLIGHT);
        assertEquals(request.getWebsite(), WEBSITE);
        assertEquals(request.getInputType(), INPUT_TYPE);
    }

    @Test
    public void testGetGeolocationSuggestions() {
        setUpGeolocationSuggestionsRequest();
        setUpGeoServiceResponse();
        setUpMapperConversion();

        when(geolocationSuggestionsRequestDTO.getIncludeCitiesOnly()).thenReturn(true);

        GeolocationSuggestionsResponse geolocationSuggestionsResponse = geolocationSuggestionsHandler.getGeolocationSuggestions(geolocationSuggestionsRequestDTO);
        GeolocationSuggestion expectedGeolocationSuggestion = geolocationSuggestionsResponse.getGeolocationSuggestions().get(0);

        assertEquals(expectedGeolocationSuggestion.getGeoNodeId(), geolocationSuggestion.getGeoNodeId());
        assertEquals(expectedGeolocationSuggestion.getIataCode(), geolocationSuggestion.getIataCode());
        assertEquals(expectedGeolocationSuggestion.getCityName(), geolocationSuggestion.getCityName());
        assertEquals(expectedGeolocationSuggestion.getCountryName(), geolocationSuggestion.getCountryName());
    }

    @Test
    public void testGetGeolocationSuggestionsEmpty() {
        setUpGeolocationSuggestionsRequest();

        when(geoService.getGeolocationSuggestions(geolocationSuggestionsRequestDTO)).thenReturn(Collections.emptyList());

        GeolocationSuggestionsResponse geolocationSuggestionsResponse = geolocationSuggestionsHandler.getGeolocationSuggestions(geolocationSuggestionsRequestDTO);
        List<GeolocationSuggestion> expectedGeolocationSuggestions = geolocationSuggestionsResponse.getGeolocationSuggestions();

        assertEquals(expectedGeolocationSuggestions.size(), 0);
    }

    private void setUpGeolocationSuggestionsRequest() {
        when(geolocationSuggestionsRequest.getSearchWord()).thenReturn(SEARCH_WORD);
        when(geolocationSuggestionsRequest.getInputType()).thenReturn(GeolocationSuggestionsInputType.DEPARTURE);
        when(geolocationSuggestionsRequest.getProductType()).thenReturn(GeolocationSuggestionsProductType.FLIGHT);
        when(geolocationSuggestionsRequest.getIncludeCitiesOnly()).thenReturn(true);
    }

    private void setUpGeoServiceResponse() {
        List<LocationDescriptionSorted> locationDescriptionSortedList = new ArrayList<>();
        LocationDescriptionSorted locationDescriptionSorted = new LocationDescriptionSorted();
        locationDescriptionSorted.setGeoNodeId(GEO_NODE_ID);
        locationDescriptionSorted.setIataCode(IATA_CODE);
        locationDescriptionSorted.setCityName(CITY_NAME);
        locationDescriptionSorted.setCountryName(COUNTRY_NAME);
        locationDescriptionSorted.setGeoNodeType(GEO_NODE_TYPE_CITY);
        locationDescriptionSortedList.add(locationDescriptionSorted);
        when(geoService.getGeolocationSuggestions(geolocationSuggestionsRequestDTO)).thenReturn(locationDescriptionSortedList);
    }

    private void setUpMapperConversion() {
        when(geolocationSuggestion.getGeoNodeId()).thenReturn(GEO_NODE_ID);
        when(geolocationSuggestion.getIataCode()).thenReturn(IATA_CODE);
        when(geolocationSuggestion.getCityName()).thenReturn(CITY_NAME);
        when(geolocationSuggestion.getCountryName()).thenReturn(COUNTRY_NAME);
        when(geolocationSuggestionsMapper.map(any())).thenReturn(Collections.singletonList(geolocationSuggestion));
    }
}
