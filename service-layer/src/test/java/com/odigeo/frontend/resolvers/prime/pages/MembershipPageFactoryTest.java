package com.odigeo.frontend.resolvers.prime.pages;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPagesRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.commons.util.ShoppingInfoUtils;
import com.odigeo.frontend.resolvers.prime.models.MembershipPages;
import com.odigeo.frontend.resolvers.prime.models.MembershipStatus;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.frontend.services.UserDescriptionService;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNull;

public class MembershipPageFactoryTest {
    private static final long SHOPPING_ID = 12345L;
    private static final ShoppingType SHOPPING_CART = ShoppingType.CART;

    @Mock
    private MembershipHandler membershipHandler;
    @Mock
    private UserDescriptionService userDescriptionService;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private Membership membership;
    @Mock
    private MembershipSubscriptionPage membershipSubscriptionPage;
    @Mock
    private MembershipManagerPage membershipManagerPage;
    @Mock
    private MembershipPagesRequest membershipPagesRequest;
    @Mock
    private ShoppingInfo shoppingInfo;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private ShoppingInfoUtils shoppingInfoUtils;
    @Mock
    Map<MembershipPages, MembershipPage> membershipPages;

    @InjectMocks
    private MembershipPageFactory factory;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(MembershipHandler.class).toProvider(() -> membershipHandler);
            binder.bind(MembershipSubscriptionPage.class).toProvider(() -> membershipSubscriptionPage);
            binder.bind(MembershipManagerPage.class).toProvider(() -> membershipManagerPage);
            binder.bind(UserDescriptionService.class).toProvider(() -> userDescriptionService);
            binder.bind(ShoppingInfoUtils.class).toProvider(() -> shoppingInfoUtils);
        });
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
        when(membershipPages.get(MembershipPages.SUBSCRIPTION_FUNNEL)).thenReturn(membershipSubscriptionPage);
        when(membershipPages.get(MembershipPages.ACTIVE_MEMBERSHIP)).thenReturn(membershipManagerPage);
        when(jsonUtils.fromDataFetching(env, MembershipPagesRequest.class)).thenReturn(membershipPagesRequest);
        when(membershipPagesRequest.getShoppingInfo()).thenReturn(shoppingInfo);
        when(env.getContext()).thenReturn(resolverContext);
    }

    @Test
    public void testGetMembershipPagesWhenActiveLoggedMember() {
        when(userDescriptionService.isUserLogged(resolverContext)).thenReturn(Boolean.TRUE);
        when(membershipHandler.getMembershipFromLoggedUser(env)).thenReturn(membership);
        when(membership.getStatus()).thenReturn(MembershipStatus.ACTIVATED.name());
        factory.getPage(env);
        verify(membershipManagerPage).getPage(eq(env));
    }

    @Test
    public void testGetMembershipPagesWhenActiveNotLoggedMember() {
        when(shoppingInfo.getShoppingId()).thenReturn(String.valueOf(SHOPPING_ID));
        when(shoppingInfo.getShoppingType()).thenReturn(SHOPPING_CART);
        when(membershipHandler.getMembershipFromShoppingPersonalInfo(shoppingInfo, resolverContext)).thenReturn(membership);
        when(membership.getStatus()).thenReturn(MembershipStatus.ACTIVATED.name());
        factory.getPage(env);
        verify(membershipManagerPage).getPage(eq(env));
    }

    @Test
    public void testGetMembershipPagesWhenPTCMember() {
        when(membershipHandler.getMembershipFromLoggedUser(env)).thenReturn(membership);
        when(membership.getStatus()).thenReturn(MembershipStatus.PENDING_TO_COLLECT.name());
        factory.getPage(env);
        verify(membershipSubscriptionPage).getPage(eq(env));
    }

    @Test
    public void testGetMembershipPagesWhenNotMember() {
        when(membershipHandler.getMembershipFromLoggedUser(env)).thenReturn(null);
        factory.getPage(env);
        verify(membershipSubscriptionPage).getPage(eq(env));
    }

    @Test
    public void testGetMembershipPageWhenShoppingCartError() {
        when(membershipHandler.getMembershipFromShoppingPersonalInfo(shoppingInfo, resolverContext)).thenThrow(new ServiceException("Error", ServiceName.FRONTEND_API));
        assertNull(factory.getMembershipFromShoppingPersonalInfo(env));
    }
}
