package com.odigeo.frontend.resolvers.shoppingcart.mappers.traveller;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.IdentificationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredFieldType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCartTraveller;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerAgeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerTitleType;
import com.odigeo.dapi.client.AirlineGroup;
import com.odigeo.dapi.client.BaggageApplicationLevel;
import com.odigeo.dapi.client.BaggageConditions;
import com.odigeo.dapi.client.BaggageDescriptor;
import com.odigeo.dapi.client.BaggageType;
import com.odigeo.dapi.client.Carrier;
import com.odigeo.dapi.client.FrequentFlyerCard;
import com.odigeo.dapi.client.RequiredField;
import com.odigeo.dapi.client.SegmentTypeIndex;
import com.odigeo.dapi.client.SelectableBaggageDescriptor;
import com.odigeo.dapi.client.Traveller;
import com.odigeo.dapi.client.TravellerGender;
import com.odigeo.dapi.client.TravellerIdentificationType;
import com.odigeo.dapi.client.TravellerInformationDescription;
import com.odigeo.dapi.client.TravellerMeal;
import com.odigeo.dapi.client.TravellerTitle;
import com.odigeo.dapi.client.TravellerType;
import com.odigeo.frontend.resolvers.shoppingcart.traveller.mappers.ShoppingTravellerMapper;
import com.odigeo.frontend.resolvers.shoppingcart.traveller.mappers.ShoppingTravellerMapperImpl;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;

public class ShoppingTravellerMapperTest {

    private static final String CARRIER_CODE = "code";
    private static final BigDecimal FEE_INCLUDED = BigDecimal.TEN;
    private static final BigDecimal PROPENSITY = BigDecimal.TEN;
    private static final Integer BAG_PIECES = 2;
    private static final Integer BAG_KILOS = 2;
    private static final BigDecimal BAG_PRICE = BigDecimal.ONE;
    private static final String COUNTRY = "country";
    private static final String NAME = "name";
    private static final int NUM_PASSENGER = 1;
    private static final String FIRST_LAST_NAME = "firstName";
    private static final String SECOND_LAST_NAME = "secondName";
    private static final String IDENTIFICATION = "id";
    private static final String LOCALITY_CODE = "lCode";
    private static final String FF_NUMBER = "ff_number";


    @Mock
    private TravellerInformationDescription travellerInfo;
    @Mock
    private Traveller traveller;
    @Mock
    private FrequentFlyerCard frequentFlyerCard;
    @Mock
    private AirlineGroup airlineGroup;
    @Mock
    private BaggageConditions baggageConditions;
    @Mock
    private BaggageDescriptor baggageDescriptor;
    @Mock
    private SelectableBaggageDescriptor selectableBaggage;
    @Mock
    private Carrier carrier;

    private ShoppingTravellerMapper mapper;
    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCartTraveller travellerModel;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new ShoppingTravellerMapperImpl();
    }

    @Test
    public void testTravellerInfoToModel() {
        assertNull(mapper.travellerInfoToModel(null));
    }

    @Test
    public void testTravellerContractToModel() {
        assertNull(mapper.travellerContractToModel(null));

        Map<TravellerIdentificationType, IdentificationType> idTypes = Stream.of(
                new AbstractMap.SimpleImmutableEntry<>(TravellerIdentificationType.PASSPORT, IdentificationType.PASSPORT),
                new AbstractMap.SimpleImmutableEntry<>(TravellerIdentificationType.NIE, IdentificationType.NIE),
                new AbstractMap.SimpleImmutableEntry<>(TravellerIdentificationType.NIF, IdentificationType.NIF),
                new AbstractMap.SimpleImmutableEntry<>(TravellerIdentificationType.NATIONAL_ID_CARD, IdentificationType.NATIONAL_ID_CARD),
                new AbstractMap.SimpleImmutableEntry<>(TravellerIdentificationType.BIRTH_DATE, IdentificationType.BIRTH_DATE))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        idTypes.forEach((k, v) -> {
            when(traveller.getIdentificationType()).thenReturn(k);
            travellerModel = mapper.travellerContractToModel(traveller);
            assertEquals(travellerModel.getIdentificationType(), v);
        });

    }

    @Test
    public void testTravellerListInfoToModel() {
        configTravellerInfo();
        List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerInformationDescription> result = mapper.travellerListInfoToModel(Collections.singletonList(travellerInfo));
        result.forEach(tInfo -> checkRequiredTravellerInfo(tInfo));
    }

    @Test
    public void testTravellerListContractToModel() {
        configTraveller();
        List<ShoppingCartTraveller> result = mapper.travellerListContractToModel(Collections.singletonList(traveller));
        result.forEach(travellerData -> checkTraveller(travellerData));
    }

    private void configTraveller() {
        when(traveller.getNumPassenger()).thenReturn(NUM_PASSENGER);
        when(traveller.getName()).thenReturn(NAME);
        when(traveller.getFirstLastName()).thenReturn(FIRST_LAST_NAME);
        when(traveller.getSecondLastName()).thenReturn(SECOND_LAST_NAME);
        when(traveller.getTravellerType()).thenReturn(TravellerType.ADULT);
        when(traveller.getIdentification()).thenReturn(IDENTIFICATION);
        when(traveller.getLocalityCodeOfResidence()).thenReturn(LOCALITY_CODE);
        when(traveller.getTitle()).thenReturn(TravellerTitle.MR);
        when(traveller.getIdentificationType()).thenReturn(TravellerIdentificationType.PASSPORT);
        when(traveller.getDateOfBirth()).thenReturn(Calendar.getInstance());
        when(traveller.getTravellerGender()).thenReturn(TravellerGender.MALE);
        when(traveller.getMeal()).thenReturn(TravellerMeal.STANDARD);
        when(traveller.getNationalityCountryCode()).thenReturn(COUNTRY);
        when(traveller.getCountryCodeOfResidence()).thenReturn(COUNTRY);
        when(traveller.getIdentificationExpirationDate()).thenReturn(Calendar.getInstance());
        when(traveller.getIdentificationIssueCountryCode()).thenReturn(COUNTRY);
        when(frequentFlyerCard.getCarrier()).thenReturn(CARRIER_CODE);
        when(frequentFlyerCard.getNumber()).thenReturn(FF_NUMBER);
        when(traveller.getFrequentFlyerNumbers()).thenReturn(Arrays.asList(frequentFlyerCard));
    }

    private void configTravellerInfo() {
        when(travellerInfo.getTravellerType()).thenReturn(TravellerType.ADULT);
        when(travellerInfo.getNeedsTitle()).thenReturn(RequiredField.MANDATORY);
        when(travellerInfo.getNeedsGender()).thenReturn(RequiredField.OPTIONAL);
        when(travellerInfo.getNeedsBirthDate()).thenReturn(RequiredField.NOT_REQUIRED);
        when(travellerInfo.getNeedsIdentification()).thenReturn(RequiredField.CONDITIONAL_REQUIRED);
        when(airlineGroup.getCarriers()).thenReturn(Arrays.asList(carrier));
        when(travellerInfo.getFrequentFlyerAirlineGroups()).thenReturn(Arrays.asList(airlineGroup));
        when(travellerInfo.getIdentificationTypes()).thenReturn(Arrays.asList(TravellerIdentificationType.values()));
        when(travellerInfo.getAvailableMeals()).thenReturn(Arrays.asList(TravellerMeal.values()));
        configBaggageConditions();
    }

    private void configBaggageConditions() {
        when(baggageConditions.getBaggageFeeIncludedInPrice()).thenReturn(FEE_INCLUDED);
        when(baggageConditions.getBaggagePropensity()).thenReturn(PROPENSITY);
        when(baggageConditions.getSegmentTypeIndex()).thenReturn(SegmentTypeIndex.FIRST);
        when(baggageConditions.getBaggageApplicationLevel()).thenReturn(BaggageApplicationLevel.SEGMENT);
        when(baggageDescriptor.getBaggageType()).thenReturn(BaggageType.BAGGAGE);
        when(baggageDescriptor.getKilos()).thenReturn(BAG_KILOS);
        when(baggageDescriptor.getPieces()).thenReturn(BAG_PIECES);
        when(baggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(baggageDescriptor);
        when(selectableBaggage.getPrice()).thenReturn(BAG_PRICE);
        when(selectableBaggage.getBaggageDescriptor()).thenReturn(baggageDescriptor);
        when(baggageConditions.getSelectableBaggageDescriptor()).thenReturn(Arrays.asList(selectableBaggage));
        when(travellerInfo.getBaggageConditions()).thenReturn(Arrays.asList(baggageConditions));
    }

    private void checkRequiredTravellerInfo(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerInformationDescription
                                                travellerInfo) {
        assertEquals(travellerInfo.getTravellerType(), TravellerAgeType.ADULT);
        assertEquals(travellerInfo.getNeedsTitle(), RequiredFieldType.MANDATORY);
        assertEquals(travellerInfo.getNeedsGender(), RequiredFieldType.OPTIONAL);
        assertEquals(travellerInfo.getNeedsBirthDate(), RequiredFieldType.NOT_REQUIRED);
        assertEquals(travellerInfo.getNeedsIdentification(), RequiredFieldType.CONDITIONAL_REQUIRED);
        assertFalse(travellerInfo.getAvailableMeals().isEmpty());
        assertFalse(travellerInfo.getIdentificationTypes().isEmpty());
        assertFalse(travellerInfo.getFrequentFlyerAirlineGroups().isEmpty());
        checkBaggageConditions(travellerInfo.getBaggageConditions().get(0));
    }

    private void checkBaggageConditions(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageConditions baggageConditions) {
        assertEquals(baggageConditions.getBaggageFeeIncludedInPrice(), FEE_INCLUDED);
        assertEquals(baggageConditions.getBaggagePropensity(), PROPENSITY);
        assertEquals(baggageConditions.getSegmentTypeIndex(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentTypeIndex.FIRST);
        assertEquals(baggageConditions.getBaggageApplicationLevel(),
            com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageApplicationLevel.SEGMENT);
        checkBaggageDescriptor(baggageConditions.getBaggageDescriptorIncludedInPrice());
        checkBaggageDescriptor(baggageConditions.getSelectableBaggageDescriptor().get(0).getBaggageDescriptor());
        assertEquals(baggageConditions.getSelectableBaggageDescriptor().get(0).getPrice(), BAG_PRICE);
    }

    private void checkBaggageDescriptor(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageDescriptor baggageDescriptor) {
        assertEquals(baggageDescriptor.getKilos(), BAG_KILOS);
        assertEquals(baggageDescriptor.getPieces(), BAG_PIECES);
        assertEquals(baggageDescriptor.getBaggageType(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType.BAGGAGE);
    }

    private void checkTraveller(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCartTraveller traveller) {
        assertEquals(traveller.getNumPassenger(), NUM_PASSENGER);
        assertEquals(traveller.getPersonalInfo().getName(), NAME);
        assertEquals(traveller.getPersonalInfo().getFirstLastName(), FIRST_LAST_NAME);
        assertEquals(traveller.getPersonalInfo().getSecondLastName(), SECOND_LAST_NAME);
        assertEquals(traveller.getPersonalInfo().getAgeType(), TravellerAgeType.ADULT);
        assertEquals(traveller.getIdentification(), IDENTIFICATION);
        assertEquals(traveller.getLocalityCodeOfResidence(), LOCALITY_CODE);
        assertEquals(traveller.getPersonalInfo().getTitle(), TravellerTitleType.MR);
        assertEquals(traveller.getIdentificationType(), IdentificationType.PASSPORT);
        assertEquals(traveller.getMeal(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerMeal.STANDARD);
        assertEquals(traveller.getPersonalInfo().getTravellerGender(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerGender.MALE);
        assertEquals(traveller.getPersonalInfo().getNationalityCountryCode(), COUNTRY);
        assertEquals(traveller.getCountryCodeOfResidence(), COUNTRY);
        assertEquals(traveller.getIdentificationIssueCountryCode(), COUNTRY);
        assertEquals(traveller.getFrequentFlyerNumbers().get(0).getCarrier(), CARRIER_CODE);
        assertEquals(traveller.getFrequentFlyerNumbers().get(0).getNumber(), FF_NUMBER);
    }

}
