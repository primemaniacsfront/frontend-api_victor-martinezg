package com.odigeo.frontend.resolvers.checkout;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutFactory;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutHandler;
import graphql.GraphQLContext;
import graphql.execution.DataFetcherResult;
import graphql.execution.ExecutionStepInfo;
import graphql.execution.ResultPath;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.checkout.CheckoutResolver.CHECKOUT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class CheckoutResolverTest {

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private CheckoutFactory factory;
    @Mock
    private CheckoutHandler handler;
    @Mock
    private CheckoutRequest checkoutRequest;
    @Mock
    private DataFetcherResult<Object> dataFetcherResult;
    @Mock
    private CheckoutFetcherResult checkoutFetcherResult;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private ExecutionStepInfo excutionStepInfo;

    @InjectMocks
    private CheckoutResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> mutations = runtimeWiring.getDataFetchers().get("Mutation");

        assertNotNull(mutations.get(CHECKOUT));
    }

    @Test
    public void testCheckout() {
        when(jsonUtils.fromDataFetching(env, CheckoutRequest.class)).thenReturn(checkoutRequest);
        when(factory.getInstance(any())).thenReturn(handler);
        when(handler.checkout(checkoutRequest, context, graphQLContext)).thenReturn(checkoutFetcherResult);
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
        when(env.getExecutionStepInfo()).thenReturn(excutionStepInfo);
        when(env.getExecutionStepInfo().getPath()).thenReturn(ResultPath.rootPath());
        assertSame(resolver.checkoutFetcher(env).getData(), checkoutFetcherResult.getData());
    }

}
