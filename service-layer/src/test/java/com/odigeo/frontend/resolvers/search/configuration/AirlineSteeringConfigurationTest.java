package com.odigeo.frontend.resolvers.search.configuration;

import bean.test.BeanTest;

import java.math.BigDecimal;
import java.util.ArrayList;

public class AirlineSteeringConfigurationTest extends BeanTest<AirlineSteeringConfiguration> {

    @Override
    protected AirlineSteeringConfiguration getBean() {
        AirlineSteeringConfiguration bean = new AirlineSteeringConfiguration();
        bean.setAirlines(new ArrayList<>());
        bean.setPercentage(BigDecimal.TEN);
        return bean;
    }

}
