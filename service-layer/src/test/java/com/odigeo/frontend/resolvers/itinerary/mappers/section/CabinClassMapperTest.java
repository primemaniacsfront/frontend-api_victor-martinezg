package com.odigeo.frontend.resolvers.itinerary.mappers.section;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CabinClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;

public class CabinClassMapperTest {

    private CabinClassMapper mapper;

    @BeforeMethod
    public void init() {
        mapper = new CabinClassMapperImpl();
    }

    @Test
    public void testCabinClassToCabinType() {
        assertEquals(mapper.cabinClassToCabinType(null), CabinClass.TOURIST);
        assertEquals(mapper.cabinClassToCabinType(com.odigeo.dapi.client.CabinClass.BUSINESS), CabinClass.BUSINESS);
        assertEquals(mapper.cabinClassToCabinType(com.odigeo.dapi.client.CabinClass.FIRST), CabinClass.FIRST);
        assertEquals(mapper.cabinClassToCabinType(com.odigeo.dapi.client.CabinClass.TOURIST), CabinClass.TOURIST);
        assertEquals(mapper.cabinClassToCabinType(com.odigeo.dapi.client.CabinClass.PREMIUM_ECONOMY), CabinClass.PREMIUM_ECONOMY);
        assertEquals(mapper.cabinClassToCabinType(com.odigeo.dapi.client.CabinClass.ECONOMIC_DISCOUNTED), CabinClass.ECONOMIC_DISCOUNTED);
    }

}
