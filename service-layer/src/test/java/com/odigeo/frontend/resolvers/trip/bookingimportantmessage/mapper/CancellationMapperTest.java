package com.odigeo.frontend.resolvers.trip.bookingimportantmessage.mapper;

import com.edreamsodigeo.bookingimportantmessage.contract.v1.model.cancellation.CancellationRefundStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Cancellation;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class CancellationMapperTest {

    private final CancellationMapper cancellationMapper = new CancellationMapper() {
        @Override
        public Cancellation toContract(com.edreamsodigeo.bookingimportantmessage.contract.v1.model.cancellation.Cancellation cancellation) {
            return null;
        }

        @Override
        public com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CancellationRefundStatus toContract(CancellationRefundStatus cancellationRefundStatus) {
            return null;
        }
    };

    @Test
    public void toContractKO() {
        Map<CancellationRefundStatus, LocalDateTime> cancellationRefundStatusDateMap = null;
        assertNull(cancellationMapper.toContract(cancellationRefundStatusDateMap));
    }

    @Test
    public void toContractOK() {
        Map<CancellationRefundStatus, LocalDateTime> cancellationRefundStatusDateMap = new HashMap<>();
        assertNotNull(cancellationMapper.toContract(cancellationRefundStatusDateMap));
    }

}
