package com.odigeo.frontend.resolvers.checkout.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckoutResumeTokenTest extends BeanTest<CheckoutResumeToken> {

    private static final long SHOPPING_ID = 1L;
    private static final int STEP = 1;
    private static final String USER_PAY_INTERACTION = "1";

    @Override
    protected CheckoutResumeToken getBean() {
        CheckoutResumeToken bean = new CheckoutResumeToken();
        bean.setInteractionStep(STEP);
        bean.setPaymentMethod(PaymentMethod.CREDITCARD.toString());
        bean.setShoppingId(SHOPPING_ID);
        bean.setShoppingType(ShoppingType.CART);
        bean.setUserPaymentInteractionId(USER_PAY_INTERACTION);
        return bean;
    }

    @Test
    public void testValueOf() {
        CheckoutResumeToken token = CheckoutResumeToken.valueOf(SHOPPING_ID + CheckoutResumeToken.SEPARATOR
                + ShoppingType.CART + CheckoutResumeToken.SEPARATOR
                + PaymentMethod.CREDITCARD.toString() + CheckoutResumeToken.SEPARATOR
                + STEP + CheckoutResumeToken.SEPARATOR
                + USER_PAY_INTERACTION);
        Assert.assertEquals(token.getShoppingId(), SHOPPING_ID);
        Assert.assertEquals(token.getShoppingType(), ShoppingType.CART);
        Assert.assertEquals(token.getPaymentMethod(), PaymentMethod.CREDITCARD.toString());
        Assert.assertEquals(token.getInteractionStep(), STEP);
        Assert.assertEquals(token.getUserPaymentInteractionId(), USER_PAY_INTERACTION);
    }

    @Test
    public void testToString() {
        CheckoutResumeToken token = new CheckoutResumeToken();
        token.setInteractionStep(STEP);
        token.setPaymentMethod(PaymentMethod.CREDITCARD.toString());
        token.setShoppingId(SHOPPING_ID);
        token.setShoppingType(ShoppingType.CART);
        token.setUserPaymentInteractionId(USER_PAY_INTERACTION);
        Assert.assertEquals(token.toString(), SHOPPING_ID + CheckoutResumeToken.SEPARATOR
                + ShoppingType.CART + CheckoutResumeToken.SEPARATOR
                + PaymentMethod.CREDITCARD.toString() + CheckoutResumeToken.SEPARATOR
                + STEP + CheckoutResumeToken.SEPARATOR
                + USER_PAY_INTERACTION);
    }

}
