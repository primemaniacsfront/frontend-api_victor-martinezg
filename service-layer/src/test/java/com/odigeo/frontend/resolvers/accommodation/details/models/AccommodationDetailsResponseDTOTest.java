package com.odigeo.frontend.resolvers.accommodation.details.models;

import bean.test.BeanTest;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;

public class AccommodationDetailsResponseDTOTest extends BeanTest<AccommodationDetailsResponseDTO> {

    @Override
    protected AccommodationDetailsResponseDTO getBean() {
        AccommodationDetailsResponseDTO bean = new AccommodationDetailsResponseDTO();
        bean.setAccommodationDetail(new AccommodationDetailDTO());
        return bean;
    }
}
