package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.google.inject.Guice;
import com.odigeo.accommodation.review.retrieve.AccommodationReviewRetrieverService;
import com.odigeo.accommodation.review.retrieve.bean.Review;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationReviewMapper;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;

public class AccommodationReviewHandlerTest {

    @Mock
    private AccommodationReviewRetrieverService accommodationReviewRetrieverService;
    @Mock
    private AccommodationReviewMapper accommodationReviewMapper;

    private AccommodationReviewHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new AccommodationReviewHandler();

        Guice.createInjector(binder -> {
            binder.bind(AccommodationReviewRetrieverService.class).toProvider(() -> accommodationReviewRetrieverService);
            binder.bind(AccommodationReviewMapper.class).toProvider(() -> accommodationReviewMapper);
        }).injectMembers(handler);
    }

    @Test
    public void testGetSingleReviewByProvider() {
        Review review = new Review();
        review.setDedupId(123);
        review.setProviderCode("TA");
        List<Review> reviews = Arrays.asList(review);
        when(accommodationReviewRetrieverService.getReviews(any())).thenReturn(reviews);

        AccommodationReview reviewDTO = new AccommodationReview();
        reviewDTO.setDedupId(123);
        reviewDTO.setRating(3.0f);
        reviewDTO.setTotalReviews(10);
        reviewDTO.setProviderReviewId("456");
        reviewDTO.setProviderCode("TA");
        when(accommodationReviewMapper.map(review)).thenReturn(reviewDTO);

        AccommodationReview result = handler.getSingleReviewByProvider(123, "TA");

        assertSame(result, reviewDTO);
    }

    @Test
    public void testGetAccommodationReviewDifferentProviderCode() {
        Review review = new Review();
        review.setDedupId(123);
        review.setProviderCode("BC");
        List<Review> reviews = Arrays.asList(review);
        when(accommodationReviewRetrieverService.getReviews(any())).thenReturn(reviews);

        AccommodationReview reviewDTO = new AccommodationReview();
        reviewDTO.setDedupId(123);
        reviewDTO.setRating(3.4f);
        reviewDTO.setTotalReviews(10);
        reviewDTO.setProviderReviewId("456");
        reviewDTO.setProviderCode("BC");
        when(accommodationReviewMapper.map(review)).thenReturn(reviewDTO);

        AccommodationReview result = handler.getSingleReviewByProvider(123, "TA");

        checkNullValues(result);
    }

    @Test
    public void testGetAccommodationReviewNoResults() {
        List<Review> reviews = Collections.emptyList();
        when(accommodationReviewRetrieverService.getReviews(any())).thenReturn(reviews);
        when(accommodationReviewMapper.map(null)).thenReturn(null);

        AccommodationReview result = handler.getSingleReviewByProvider(123, "TA");

        checkNullValues(result);
    }

    private void checkNullValues(AccommodationReview result) {
        assertNull(result.getDedupId());
        assertNull(result.getProviderCode());
        assertNull(result.getProviderReviewId());
        assertNull(result.getRating());
        assertNull(result.getTotalReviews());
    }

}
