package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.prime.models.MembershipStatus;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.frontend.services.PrimeService;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.User;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PrimeHandlerTest {

    @Mock
    private UserDescriptionService userService;
    @Mock
    private PrimeService primeService;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private SiteVariations siteVariations;
    @Mock
    private MembershipHandler membershipHandler;
    @Mock
    private Membership membership;
    @Mock
    private User user;

    @InjectMocks
    private PrimeHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(MembershipHandler.class).toProvider(() -> membershipHandler);
        });
        when(context.getVisitInformation()).thenReturn(visit);
    }

    @Test
    public void testIsPrimeMarketWithSite() {
        when(primeService.isPrimeSite(visit)).thenReturn(true);
        assertTrue(handler.isPrimeMarket(context));
    }

    @Test
    public void testIsPrimeMarketWithUser() {
        when(userService.getUserByToken(context)).thenReturn(user);
        when(membershipHandler.getMembership(user, visit)).thenReturn(membership);
        when(membership.getStatus()).thenReturn(MembershipStatus.ACTIVATED.name());
        assertTrue(handler.isPrimeMarket(context));
    }

    @Test
    public void testIsNotPrimeMarket() {
        assertFalse(handler.isPrimeMarket(context));
    }

}
