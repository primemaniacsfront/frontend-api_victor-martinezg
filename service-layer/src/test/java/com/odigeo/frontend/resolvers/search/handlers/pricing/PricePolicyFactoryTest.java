package com.odigeo.frontend.resolvers.search.handlers.pricing;


import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.search.handlers.PrimeHandler;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class PricePolicyFactoryTest {

    @Mock
    private MinPricePolicy minPricePolicy;
    @Mock
    private PrimePricePolicy primePricePolicy;
    @Mock
    private PrimeHandler primeHandler;
    @Mock
    private ResolverContext resolverContext;

    @InjectMocks
    private PricePolicyFactory pricePolicyFactory;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetPricePolicyNonPrime() {
        when(primeHandler.isPrimeMarket(resolverContext)).thenReturn(Boolean.FALSE);
        assertEquals(pricePolicyFactory.getPricePolicy(resolverContext), minPricePolicy);
    }

    @Test
    public void testGetPricePolicyForPrime() {
        when(primeHandler.isPrimeMarket(resolverContext)).thenReturn(Boolean.TRUE);
        assertEquals(pricePolicyFactory.getPricePolicy(resolverContext), primePricePolicy);
    }

}
