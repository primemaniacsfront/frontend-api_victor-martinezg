package com.odigeo.frontend.resolvers.user.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ChangePasswordWithCodeRequest;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.ResponseInfo;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.SSOToken;
import com.odigeo.userprofiles.api.v2.model.User;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class UserPasswordHandlerTest {
    @InjectMocks
    private UserPasswordHandler handler;
    @Mock
    private UserDescriptionService userService;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private VisitInformation visit;
    @Mock
    private ChangePasswordWithCodeRequest changePasswordWithCodeRequest;

    private static final String TOKEN = "abcdefghijklmnopqrstuvwxyz";
    private static final String EMAIL = "jimcarrey@aceventura.com";
    private static final String PASSWORD = "1234";
    private static final String HASH_CODE = "asdkjasdf";
    private static final Site SITE = Site.ES;
    private static final String LOCALE = "locale";

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(UserDescriptionService.class).toProvider(() -> userService);
        }).injectMembers(handler);
        when(env.getContext()).thenReturn(context);
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
        when(graphQLContext.get(VisitInformation.class)).thenReturn(visit);
    }

    @Test
    public void testChangePasswordWithCode() {
        User user = new User();
        user.setEmail(EMAIL);
        ResponseInfo responseInfo = new ResponseInfo();
        setUpVisitInformation();
        when(userService.setUserPassword(any())).thenReturn(user);
        when(userService.generateToken(any())).thenReturn(setUpSSOToken());
        when(context.getResponseInfo()).thenReturn(responseInfo);

        assertNotNull(handler.changePasswordWithCode(changePasswordWithCodeRequest, env));
        assertEquals(responseInfo.getCookies().size(), 1);
    }

    @Test
    public void testChangePasswordWithCodePersistentLoginFalse() {
        User user = new User();
        user.setEmail(EMAIL);
        ResponseInfo responseInfo = new ResponseInfo();
        setUpVisitInformation();
        when(userService.setUserPassword(any())).thenReturn(user);
        when(userService.generateToken(any())).thenReturn(setUpSSOToken());
        when(context.getResponseInfo()).thenReturn(responseInfo);
        assertNotNull(handler.changePasswordWithCode(changePasswordWithCodeRequest, env));
        assertEquals(responseInfo.getCookie(SiteCookies.SSO_TOKEN).getMaxAge(), 3600);
    }

    @Test
    public void testChangePasswordWithCodePersistentLoginTrue() {
        User user = new User();
        user.setEmail(EMAIL);
        ChangePasswordWithCodeRequest changePasswordWithCodeRequest = new ChangePasswordWithCodeRequest();
        changePasswordWithCodeRequest.setPersistentLogin(true);
        changePasswordWithCodeRequest.setHashCode(HASH_CODE);
        changePasswordWithCodeRequest.setNewPassword(PASSWORD);
        ResponseInfo responseInfo = new ResponseInfo();

        setUpVisitInformation();
        when(userService.setUserPassword(any())).thenReturn(user);
        when(userService.generateToken(any())).thenReturn(setUpSSOToken());
        when(context.getResponseInfo()).thenReturn(responseInfo);
        assertNotNull(handler.changePasswordWithCode(changePasswordWithCodeRequest, env));
        assertEquals(responseInfo.getCookie(SiteCookies.SSO_TOKEN).getMaxAge(), 15552000);
    }

    @Test
    public void testChangePasswordWithCodeRequestNull() {
        setUpVisitInformation();
        assertNull(handler.changePasswordWithCode(null, env));
    }

    private void setUpVisitInformation() {
        when(visit.getSite()).thenReturn(SITE);
        when(visit.getBrand()).thenReturn(Brand.ED);
        when(visit.getWebInterface()).thenReturn(WebInterface.ONE_FRONT_DESKTOP);
        when(visit.getLocale()).thenReturn(LOCALE);
    }

    private SSOToken setUpSSOToken() {
        SSOToken ssoToken = new SSOToken();
        ssoToken.setToken(TOKEN);

        return ssoToken;
    }
}
