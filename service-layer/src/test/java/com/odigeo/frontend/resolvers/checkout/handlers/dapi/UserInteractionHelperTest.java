package com.odigeo.frontend.resolvers.checkout.handlers.dapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CollectionMethodRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.frontend.resolvers.checkout.handlers.dapi.UserInteractionHelper.ALT_PAYMENT_METHOD;
import static com.odigeo.frontend.resolvers.checkout.handlers.dapi.UserInteractionHelper.RETURN_URL;
import static com.odigeo.frontend.resolvers.checkout.handlers.dapi.UserInteractionHelper.RETURN_URL_TAG;
import static com.odigeo.frontend.resolvers.checkout.handlers.dapi.UserInteractionHelper.SCHEME;
import static com.odigeo.frontend.resolvers.checkout.handlers.dapi.UserInteractionHelper.SLASH;
import static com.odigeo.frontend.resolvers.checkout.handlers.dapi.UserInteractionHelper.STEP;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class UserInteractionHelperTest {

    private static final String BOOKING_ID = "123";
    private static final String SERVER_NAME = "server/";
    private static final String CTX_PATH = "ctx";
    private static final String PAY_INTERACTION_ID = "1";

    @Mock
    private CheckoutRequest checkoutRequest;
    @Mock
    private CollectionMethodRequest collectionMethodRequest;

    @InjectMocks
    private UserInteractionHelper testClass;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetReturnUrlTag() {
        assertEquals(testClass.getReturnUrlTag(), RETURN_URL_TAG);
    }

    @Test
    public void testBuildReturnUrl() {
        when(collectionMethodRequest.getCollectionMethodType()).thenReturn(PaymentMethod.SOFORT);
        when(checkoutRequest.getShoppingId()).thenReturn(BOOKING_ID);
        when(checkoutRequest.getCollectionMethod()).thenReturn(collectionMethodRequest);
        when(checkoutRequest.getShoppingType()).thenReturn(ShoppingType.CART);
        String tokenUrl = BOOKING_ID + SLASH + ShoppingType.CART.name() + SLASH + ALT_PAYMENT_METHOD + SLASH + STEP + SLASH + PAY_INTERACTION_ID;
        String finisUrl = SCHEME + SERVER_NAME + CTX_PATH + SLASH + RETURN_URL + SLASH + tokenUrl;
        String returnUrl = testClass.buildReturnUrl(checkoutRequest, SERVER_NAME, CTX_PATH, PAY_INTERACTION_ID);
        assertEquals(returnUrl, finisUrl);
    }

}
