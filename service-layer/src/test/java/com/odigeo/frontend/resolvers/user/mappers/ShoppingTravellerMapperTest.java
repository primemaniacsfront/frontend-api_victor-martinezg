package com.odigeo.frontend.resolvers.user.mappers;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerAgeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerGender;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerMeal;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerTitleType;
import com.odigeo.userprofiles.api.v2.model.FrequentFlyer;
import com.odigeo.userprofiles.api.v2.model.Identification;
import com.odigeo.userprofiles.api.v2.model.IdentificationType;
import com.odigeo.userprofiles.api.v2.model.MealType;
import com.odigeo.userprofiles.api.v2.model.Profile;
import com.odigeo.userprofiles.api.v2.model.Title;
import com.odigeo.userprofiles.api.v2.model.Traveller;
import com.odigeo.userprofiles.api.v2.model.TypeOfTraveller;
import com.odigeo.userprofiles.api.v2.model.UserAddress;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class ShoppingTravellerMapperTest {

    private static final String NAME = "Name";
    private static final String FEMALE = "FEMALE";
    private static final String FIRST_LAST_NAME = "First";
    private static final String SECOND_LAST_NAME = "Second";
    private static final String PREFIX = "+34";
    private static final String PHONE_NUMBER = "123456789";
    private static final String PREFIX_ALTERNATE_PHONE_NUMBER = "+1";
    private static final String MOBILE_PHONE_NUMBER = "1234567890";
    private static final String ALTERNATE_PHONE_NUMBER = "1234567891";
    private static final String CPF = "CPF";
    private static final String COUNTRY_CODE = "ES";
    private static final String IDENTIFICATION_ID = "123456789A";
    private static final String ADDRESS = "Address";
    private static final String ADDRESS_TYPE = "Street";
    private static final String CITY = "City";
    private static final String COUNTRY = "Country";
    private static final String POSTAL_CODE = "12345";
    private static final String ALIAS = "Alias";
    private static final String STATE = "Alias";
    private static final String AIRLINE_CODE = "IB";
    private static final String FREQUENT_FLYER_NUMBER = "123456";
    private static final String EMAIL = "email";
    private static final Date DATE = new Date();
    private static final ZonedDateTime ZONED_DATE_TIME = ZonedDateTime.ofInstant(DATE.toInstant(), ZoneId.systemDefault());

    @Mock
    private Traveller traveller;
    @Mock
    private Profile profile;
    @Mock
    private Identification identification;
    @Mock
    private UserAddress userAddress;
    @Mock
    private FrequentFlyer frequentFlyer;

    private TravellerMapper mapper;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new TravellerMapperImpl();

        when(traveller.getProfile()).thenReturn(profile);
        when(profile.getName()).thenReturn(NAME);
        when(profile.getTitle()).thenReturn(Title.MRS);
        when(profile.getFirstLastName()).thenReturn(FIRST_LAST_NAME);
        when(profile.getSecondLastName()).thenReturn(SECOND_LAST_NAME);
        when(profile.getBirthDate()).thenReturn(DATE);
        when(profile.getPrefixPhoneNumber()).thenReturn(PREFIX);
        when(profile.getPhoneNumber()).thenReturn(PHONE_NUMBER);
        when(profile.getPrefixAlternatePhoneNumber()).thenReturn(PREFIX_ALTERNATE_PHONE_NUMBER);
        when(profile.getMobilePhoneNumber()).thenReturn((MOBILE_PHONE_NUMBER));
        when(profile.getAlternatePhoneNumber()).thenReturn(ALTERNATE_PHONE_NUMBER);
        when(profile.getIsDefault()).thenReturn(true);
        when(profile.getCpf()).thenReturn(CPF);
        when(profile.getNationalityCountryCode()).thenReturn(COUNTRY_CODE);

        when(identification.getId()).thenReturn(1L);
        when(identification.getIdentificationCountryCode()).thenReturn(COUNTRY_CODE);
        when(identification.getIdentificationExpirationDate()).thenReturn(DATE);
        when(identification.getIdentificationId()).thenReturn(IDENTIFICATION_ID);
        when(identification.getIdentificationType()).thenReturn(IdentificationType.NATIONAL_ID_CARD);
        List<Identification> identifications = new ArrayList<>();
        identifications.add(identification);
        when(profile.getIdentificationList()).thenReturn(identifications);

        when(userAddress.getAddress()).thenReturn(ADDRESS);
        when(userAddress.getAddressType()).thenReturn(ADDRESS_TYPE);
        when(userAddress.getCity()).thenReturn(CITY);
        when(userAddress.getCountry()).thenReturn(COUNTRY);
        when(userAddress.getPostalCode()).thenReturn(POSTAL_CODE);
        when(userAddress.getIsPrimary()).thenReturn(true);
        when(userAddress.getAlias()).thenReturn(ALIAS);
        when(userAddress.getState()).thenReturn(STATE);
        when(profile.getAddress()).thenReturn(userAddress);

        when(traveller.getId()).thenReturn(123L);
        when(traveller.getBuyer()).thenReturn(true);
        when(traveller.getTypeOfTraveller()).thenReturn(TypeOfTraveller.ADULT);

        when(frequentFlyer.getAirlineCode()).thenReturn(AIRLINE_CODE);
        when(frequentFlyer.getFrequentFlyerNumber()).thenReturn(FREQUENT_FLYER_NUMBER);
        when(frequentFlyer.getId()).thenReturn(1L);
        List<FrequentFlyer> frequentFlyerList = new ArrayList<>();
        frequentFlyerList.add(frequentFlyer);
        when(traveller.getFrequentFlyerList()).thenReturn(frequentFlyerList);

        when(traveller.getImportedFromLocal()).thenReturn(true);
        when(traveller.getMealType()).thenReturn(MealType.BABY);
        when(traveller.getEmail()).thenReturn(EMAIL);

    }

    @Test
    public void testTravellerContractToModel() {

        assertNull(mapper.travellerContractToModel(null));
        when(profile.getGender()).thenReturn(FEMALE);

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserTraveller travellerModel = mapper.travellerContractToModel(traveller);
        assertEquals(travellerModel.getPersonalInfo().getName(), traveller.getProfile().getName());
        assertEquals(travellerModel.getPersonalInfo().getTravellerGender(), TravellerGender.FEMALE);
        assertEquals(travellerModel.getPersonalInfo().getTitle(), TravellerTitleType.MRS);
        assertEquals(travellerModel.getPersonalInfo().getFirstLastName(), traveller.getProfile().getFirstLastName());
        assertEquals(travellerModel.getPersonalInfo().getSecondLastName(), traveller.getProfile().getSecondLastName());
        assertEquals(travellerModel.getPersonalInfo().getBirthDate(), dateFormat.format(DATE));
        assertEquals(travellerModel.getPrefixPhoneNumber(), traveller.getProfile().getPrefixPhoneNumber());
        assertEquals(travellerModel.getPhoneNumber(), traveller.getProfile().getPhoneNumber());
        assertEquals(travellerModel.getPrefixAlternatePhoneNumber(), traveller.getProfile().getPrefixAlternatePhoneNumber());
        assertEquals(travellerModel.getMobilePhoneNumber(), traveller.getProfile().getMobilePhoneNumber());
        assertEquals(travellerModel.getAlternatePhoneNumber(), traveller.getProfile().getAlternatePhoneNumber());
        assertEquals(travellerModel.getIsDefault(), traveller.getProfile().getIsDefault());
        assertEquals(travellerModel.getCpf(), traveller.getProfile().getCpf());
        assertEquals(travellerModel.getPersonalInfo().getNationalityCountryCode(), traveller.getProfile().getNationalityCountryCode());
        assertEquals(travellerModel.getIdentificationList().get(0).getIdentificationCountryCode(), traveller.getProfile().getIdentificationList().get(0).getIdentificationCountryCode());
        assertEquals(travellerModel.getIdentificationList().get(0).getIdentificationExpirationDate(), ZONED_DATE_TIME);
        assertEquals(travellerModel.getIdentificationList().get(0).getIdentificationId(), traveller.getProfile().getIdentificationList().get(0).getIdentificationId());
        assertEquals(travellerModel.getIdentificationList().get(0).getIdentificationType(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.IdentificationType.NATIONAL_ID_CARD);
        assertEquals(travellerModel.getAddress().getAddress(), traveller.getProfile().getAddress().getAddress());
        assertEquals(travellerModel.getAddress().getAddressType(), traveller.getProfile().getAddress().getAddressType());
        assertEquals(travellerModel.getAddress().getCity(), traveller.getProfile().getAddress().getCity());
        assertEquals(travellerModel.getAddress().getCountry(), traveller.getProfile().getAddress().getCountry());
        assertEquals(travellerModel.getAddress().getPostalCode(), traveller.getProfile().getAddress().getPostalCode());
        assertEquals(travellerModel.getAddress().getIsPrimary(), traveller.getProfile().getAddress().getIsPrimary());
        assertEquals(travellerModel.getAddress().getAlias(), traveller.getProfile().getAddress().getAlias());
        assertEquals(travellerModel.getAddress().getState(), traveller.getProfile().getAddress().getState());
        assertEquals(travellerModel.getId(), (long) traveller.getId());
        assertEquals(travellerModel.getBuyer(), traveller.getBuyer());
        assertEquals(travellerModel.getPersonalInfo().getAgeType(), TravellerAgeType.ADULT);
        assertEquals(travellerModel.getFrequentFlyerNumbers().get(0).getCarrier(), traveller.getFrequentFlyerList().get(0).getAirlineCode());
        assertEquals(travellerModel.getFrequentFlyerNumbers().get(0).getNumber(), traveller.getFrequentFlyerList().get(0).getFrequentFlyerNumber());
        assertEquals(travellerModel.getImportedFromLocal(), traveller.getImportedFromLocal());
        assertEquals(travellerModel.getMeal(), TravellerMeal.BABY);
        assertEquals(travellerModel.getEmail(), traveller.getEmail());

    }

    @Test
    public void testTravellerContractToModelWithEmptyGender() {
        launchDefaultGenderTest("");
    }

    @Test
    public void testTravellerContractToModelWithNullGender() {
        launchDefaultGenderTest(null);
    }

    @Test
    public void testTravellerContractToModelWithUnknownGender() {
        launchDefaultGenderTest("unknown");
    }

    private void launchDefaultGenderTest(String value) {
        when(profile.getGender()).thenReturn(value);

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserTraveller travellerModel = mapper.travellerContractToModel(traveller);
        assertEquals(travellerModel.getPersonalInfo().getTravellerGender(), TravellerGender.MALE);
    }
}
