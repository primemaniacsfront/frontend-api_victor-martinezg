package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentToken;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCards;
import com.google.inject.Guice;
import com.odigeo.frontend.services.PaymentInstrumentService;
import com.odigeo.userprofiles.api.v2.model.CreditCard;
import com.odigeo.userprofiles.api.v2.model.CreditCardType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class CreditCardsMapperTest {

    private static final String CREDIT_CARD_NUMBER = "5055203244802134";
    private static final String HIDDEN_CREDIT_CARD_NUMBER = "****2134";
    private static final Long ID = 124135L;
    private static final String EXPIRATION_DATE_MONTH = "10";
    private static final String EXPIRATION_DATE_YEAR = "2028";
    private static final String OWNER = "owner";
    private static final String TOKEN = "00000000-0000-0000-0000-000000000001";
    private CreditCardsMapper mapper;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Mock
    private CreditCard creditCard;
    @Mock
    private PaymentInstrumentService paymentInstrumentService;
    @Mock
    private PaymentInstrumentToken paymentInstrumentToken;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mapper = new CreditCardsMapperImpl();
        Guice.createInjector(binder -> {
            binder.bind(PaymentInstrumentService.class).toProvider(() -> paymentInstrumentService);
        }).injectMembers(mapper);
        when(paymentInstrumentToken.getUuid()).thenReturn(new UUID(0L, 1L));
        when(paymentInstrumentService.tokenize(anyString(), anyString(), anyString(), anyString())).thenReturn(paymentInstrumentToken);
    }

    @Test
    public void testCreditCardsContractToModel() {
        CreditCards creditCardsModel = setCreditCard();
        assertEquals(creditCardsModel.getCreditCardNumber(), mapper.hideCreditCards(creditCard.getCreditCardNumber()));
        assertEquals(creditCardsModel.getCreditCardTypeId().name(), creditCard.getCreditCardTypeId().name());
        assertEquals(creditCardsModel.getId(), creditCard.getId().longValue());
        assertEquals(creditCardsModel.getCreationDate(), dateFormat.format(creditCard.getCreationDate()));
        assertEquals(creditCardsModel.getExpirationDateMonth(), creditCard.getExpirationDateMonth());
        assertEquals(creditCardsModel.getExpirationDateYear(), creditCard.getExpirationDateYear());
        assertEquals(creditCardsModel.getIsDefault(), creditCard.getIsDefault());
        assertEquals(creditCardsModel.getLastUsageDate(), dateFormat.format(creditCard.getLastUsageDate()));
        assertEquals(creditCardsModel.getModifiedDate(), dateFormat.format(creditCard.getModifiedDate()));
        assertEquals(creditCardsModel.getOwner(), creditCard.getOwner());
    }

    private CreditCards setCreditCard() {
        when(creditCard.getCreditCardNumber()).thenReturn(CREDIT_CARD_NUMBER);
        when(creditCard.getCreationDate()).thenReturn(new Date());
        when(creditCard.getCreditCardTypeId()).thenReturn(CreditCardType.VISA_CREDIT);
        when(creditCard.getId()).thenReturn(ID);
        when(creditCard.getExpirationDateMonth()).thenReturn(EXPIRATION_DATE_MONTH);
        when(creditCard.getExpirationDateYear()).thenReturn(EXPIRATION_DATE_YEAR);
        when(creditCard.getIsDefault()).thenReturn(true);
        when(creditCard.getLastUsageDate()).thenReturn(new Date());
        when(creditCard.getModifiedDate()).thenReturn(new Date());
        when(creditCard.getOwner()).thenReturn(OWNER);
        return mapper.creditCardsContractToModel(creditCard);
    }

    @Test
    public void testCreditCardsContractToModelIsNull() {
        CreditCards creditCardsModel = mapper.creditCardsContractToModel(null);
        assertNull(creditCardsModel);
    }

    @Test
    public void testCreditCardsIsHidden() {
        String hiddenCarNumber = mapper.hideCreditCards(CREDIT_CARD_NUMBER);
        assertEquals(hiddenCarNumber, HIDDEN_CREDIT_CARD_NUMBER);
    }

    @Test
    public void tokenIsCorrect() {
        CreditCards creditCardsModel = setCreditCard();
        assertEquals(creditCardsModel.getPaymentInstrumentTokenWithoutCvv(), TOKEN);
    }
}
