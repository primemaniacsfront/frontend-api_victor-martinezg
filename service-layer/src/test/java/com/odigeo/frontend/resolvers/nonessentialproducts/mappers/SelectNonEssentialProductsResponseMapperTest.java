package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectNonEssentialProductsResponse;
import com.odigeo.frontend.resolvers.shoppingbasket.mappers.ProductIdMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class SelectNonEssentialProductsResponseMapperTest {

    private static final String POLICY = "POLICY";

    @Mock
    private ProductIdMapper productIdMapper;

    @InjectMocks
    private SelectNonEssentialProductsResponseMapper selectNonEssentialProductsResponseMapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testMap() {
        List<String> requestedPolicies = Collections.singletonList(POLICY);
        when(productIdMapper.map(any(), any())).thenReturn(new ProductId(POLICY, ProductType.INSURANCE));

        SelectNonEssentialProductsResponse response = selectNonEssentialProductsResponseMapper.map(requestedPolicies);

        assertEquals(response.getProductId().size(), 1);
        assertEquals(response.getProductId().get(0).getId(), POLICY);
        assertEquals(response.getProductId().get(0).getType(), ProductType.INSURANCE);
    }

    @Test
    public void testMapEmptyList() {
        SelectNonEssentialProductsResponse response = selectNonEssentialProductsResponseMapper.map(new ArrayList<>());

        assertNotNull(response);
        assertTrue(response.getProductId().isEmpty());
    }

    @Test
    public void testMapNullParam() {
        SelectNonEssentialProductsResponse response = selectNonEssentialProductsResponseMapper.map(null);

        assertNotNull(response);
        assertTrue(response.getProductId().isEmpty());
    }
}
