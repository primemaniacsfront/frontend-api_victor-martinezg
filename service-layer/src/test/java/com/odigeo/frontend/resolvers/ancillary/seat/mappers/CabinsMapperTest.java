package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Cabin;
import com.odigeo.itineraryapi.v1.response.SeatMapCabin;
import com.odigeo.itineraryapi.v1.response.SeatMapCabinCharacteristics;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CabinsMapperTest {

    @Mock
    private SeatMapper seatMapper;

    @InjectMocks
    private CabinsMapper cabinsMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(seatMapper.map(any())).thenReturn(new ArrayList<>());
    }

    @Test
    public void mapValidProviderCabinList() {
        List<SeatMapCabin> providerCabinList = getInput();
        List<Cabin> mappedCabinList = cabinsMapper.map(providerCabinList);
        List<Cabin> expectedCabinList = getOutput();
        Cabin expectedMappedCabin = expectedCabinList.get(0);
        Cabin mappedCabin = mappedCabinList.get(0);

        assertEquals(providerCabinList.size(), mappedCabinList.size(), expectedCabinList.size());
        assertEquals(mappedCabin.getSeats(), expectedMappedCabin.getSeats());
        assertEquals(mappedCabin.getDistribution(), expectedMappedCabin.getDistribution());
        assertEquals(mappedCabin.getExitRows(), expectedMappedCabin.getExitRows());
        assertEquals(mappedCabin.getExtraLegSeats(), expectedMappedCabin.getExtraLegSeats());
        assertEquals(mappedCabin.getFloor(), expectedMappedCabin.getFloor());
        assertEquals(mappedCabin.getSeatMapRowFrom(), expectedMappedCabin.getSeatMapRowFrom());
        assertEquals(mappedCabin.getSeatMapRowTo(), expectedMappedCabin.getSeatMapRowTo());
        assertEquals(mappedCabin.getFirstSeatsRow(), expectedMappedCabin.getFirstSeatsRow());
        assertEquals(mappedCabin.getLastSeatsRow(), expectedMappedCabin.getLastSeatsRow());
    }

    @Test
    public void mapEmptyProviderCabinList() {
        List<SeatMapCabin> providerCabinList = new ArrayList<>();
        List<Cabin> mappedCabinList = cabinsMapper.map(providerCabinList);
        assertEquals(providerCabinList.size(), mappedCabinList.size());
    }

    private List<SeatMapCabin> getInput() {
        List<SeatMapCabin> inputSeatMapCabinList = new ArrayList<>();
        SeatMapCabin mockSeatMapCabin = new SeatMapCabin();

        SeatMapCabinCharacteristics mockSeatMapCabinCharacteristics = new SeatMapCabinCharacteristics();
        List<String> mockSeatDistribution = Stream.of(
            "A",
            "B",
            "C",
            "AISLE",
            "D",
            "E",
            "F"
        ).collect(Collectors.toList());

        List<Integer> mockExitRows = Stream.of(17, 18).collect(Collectors.toList());

        List<String> mockExtraLegSeats = Stream.of(
            "8A",
            "8B",
            "8C",
            "8D",
            "8E",
            "8F"
        ).collect(Collectors.toList());

        mockSeatMapCabinCharacteristics.setCabinColumnDistribution(mockSeatDistribution);
        mockSeatMapCabinCharacteristics.setExitRows(mockExitRows);
        mockSeatMapCabinCharacteristics.setExtraLegSeats(mockExtraLegSeats);

        mockSeatMapCabin.setSeats(new ArrayList<>());
        mockSeatMapCabin.setSeatMapRowsCharacteristics(mockSeatMapCabinCharacteristics);
        mockSeatMapCabin.setCabinFloor(0);
        mockSeatMapCabin.setSeatMapRowFrom(0);
        mockSeatMapCabin.setSeatMapRowTo(13);
        mockSeatMapCabin.setFirstSeatsRow(0);
        mockSeatMapCabin.setLastSeatsRow(21);

        inputSeatMapCabinList.add(mockSeatMapCabin);

        return inputSeatMapCabinList;
    }

    private List<Cabin> getOutput() {
        List<Cabin> outputCabinList = new ArrayList<>();
        Cabin outputCabin = new Cabin();

        List<String> mockDistribution = Stream.of(
            "A",
            "B",
            "C",
            "AISLE",
            "D",
            "E",
            "F"
        ).collect(Collectors.toList());

        List<Integer> mockExitRows =  Stream.of(17, 18).collect(Collectors.toList());

        List<String> mockExtraLegSeats = Stream.of(
            "8A",
            "8B",
            "8C",
            "8D",
            "8E",
            "8F"
        ).collect(Collectors.toList());

        outputCabin.setSeats(new ArrayList<>());
        outputCabin.setDistribution(mockDistribution);
        outputCabin.setExitRows(mockExitRows);
        outputCabin.setExtraLegSeats(mockExtraLegSeats);
        outputCabin.setFloor(0);
        outputCabin.setSeatMapRowFrom(0);
        outputCabin.setSeatMapRowTo(13);
        outputCabin.setFirstSeatsRow(0);
        outputCabin.setLastSeatsRow(21);

        outputCabinList.add(outputCabin);
        return outputCabinList;
    }
}
