package com.odigeo.frontend.resolvers.accommodation.details.handlers;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.exceptions.CityNotFoundException;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationDataHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.resolvers.accommodation.details.mappers.AccommodationDetailsMapper;
import com.odigeo.frontend.resolvers.accommodation.details.models.AccommodationDetailsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.HotelDetailsRequestDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.GeoNode;
import com.odigeo.hcsapi.v9.beans.Hotel;
import com.odigeo.hcsapi.v9.beans.HotelDetailsResponse;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;
import com.odigeo.hcsapi.v9.beans.Legend;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;


public class AccommodationDetailsHandlerTest {

    private static final Integer GEO_NODE = 1;
    private static final Double DEFAULT_DISTANCE = 8d;

    @Mock
    private AccommodationDetailsMapper accommodationDetailsMapper;
    @Mock
    private AccommodationDataHandler accommodationDataHandler;
    @Mock
    private AccommodationProviderKeyDTO accommodationProviderKey;
    @Mock
    private VisitInformation visit;
    @Mock
    private HotelDetailsResponse hotelDetailsResponse;
    @Mock
    private GeoLocationHandler geoLocationHandler;
    @Mock
    private Logger logger;

    @Mock
    private AccommodationDetailsResponseDTO responseMock;
    @Mock
    private Legend legend;

    @InjectMocks
    AccommodationDetailsHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(accommodationProviderKey.getProviderId()).thenReturn("providerID");
        when(accommodationProviderKey.getAccommodationProviderId()).thenReturn("providerAccommodationId");
    }

    @Test
    public void testGetAccommodationDetailsWithoutItems() {
        AccommodationDetailsResponseDTO responseDTO = handler.getAccommodationDetails(accommodationProviderKey, visit, GEO_NODE);
        assertNull(responseDTO.getAccommodationDetail());
    }

    @Test
    public void testGetAccommodationDetailsEmpty() {
        AccommodationDetailsResponseDTO responseDTO = handler.getAccommodationDetails(accommodationProviderKey, visit, GEO_NODE);
        assertNull(responseDTO.getAccommodationDetail());
    }

    @Test
    public void testGetAccommodationDetails() {
        prepareAccommodationDetailsMock();
        AccommodationDetailsResponseDTO responseDTO = handler.getAccommodationDetails(accommodationProviderKey, visit, GEO_NODE);
        assertEquals(responseDTO, responseMock);
    }

    @Test
    public void testGetAccommodationDetailsWithDistanceToCenter() throws CityNotFoundException {
        AccommodationDetailDTO detailDTO = mock(AccommodationDetailDTO.class);
        AccommodationDealInformationDTO dealInformationDTO = mock(AccommodationDealInformationDTO.class);
        GeoCoordinatesDTO geoCoordinatesDTO = mock(GeoCoordinatesDTO.class);

        when(responseMock.getAccommodationDetail()).thenReturn(detailDTO);
        when(detailDTO.getAccommodationDealInformation()).thenReturn(dealInformationDTO);
        when(dealInformationDTO.getCoordinates()).thenReturn(geoCoordinatesDTO);

        prepareAccommodationDetailsMock();
        when(geoLocationHandler.getCityGeonode(anyInt(), isNull())).thenReturn(new City());

        when(geoLocationHandler.computeDistanceFromGeonode(any(GeoNode.class), any())).thenReturn(DEFAULT_DISTANCE);

        AccommodationDetailsResponseDTO responseDTO = handler.getAccommodationDetails(accommodationProviderKey, visit, GEO_NODE);
        assertEquals(responseDTO, responseMock);
        verify(detailDTO).setDistanceFromCityCenter(DEFAULT_DISTANCE);
    }

    private void prepareAccommodationDetailsMock() {
        Hotel hotel = mock(Hotel.class);
        Map<HotelSupplierKey, Hotel> hotelMap = new HashMap();
        hotelMap.put(new HotelSupplierKey(accommodationProviderKey.getAccommodationProviderId(), accommodationProviderKey.getProviderId()), hotel);

        when(accommodationDataHandler.getAccommodationDetails(any(HotelDetailsRequestDTO.class), eq(visit))).thenReturn(hotelDetailsResponse);
        when(hotelDetailsResponse.getLegend()).thenReturn(legend);
        when(hotelDetailsResponse.getHotel()).thenReturn(hotelMap);
        when(accommodationDetailsMapper.map(hotel, legend)).thenReturn(responseMock);
    }
}
