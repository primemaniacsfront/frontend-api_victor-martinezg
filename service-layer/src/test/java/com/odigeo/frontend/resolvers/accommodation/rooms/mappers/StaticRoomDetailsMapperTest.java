package com.odigeo.frontend.resolvers.accommodation.rooms.mappers;

import com.odigeo.frontend.resolvers.accommodation.commons.model.ImageDTO;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDealDTO;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDetailsDTO;
import com.odigeo.hcsapi.v9.beans.RoomDetails;
import com.odigeo.hcsapi.v9.beans.RoomImage;
import com.odigeo.hcsapi.v9.beans.RoomsDetailResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class StaticRoomDetailsMapperTest {

    @Mock
    private RoomImageMapper roomImageMapper;
    @Mock
    private RoomsDetailResponse roomsDetailResponse;

    @InjectMocks
    private StaticRoomDetailsMapper testClass;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(roomsDetailResponse.getProviderRoomIdByRoomId()).thenReturn(Collections.singletonMap("1", "1"));
    }


    @Test
    public void test() {
        when(roomImageMapper.map(any(RoomImage.class))).thenReturn(new ImageDTO());
        when(roomsDetailResponse.getRoomDetailByProviderRoomId()).thenReturn(Collections.singletonMap("1", getRoomDetails()));
        RoomDealDTO roomDealDTO = new RoomDealDTO();
        roomDealDTO.setProviderRoomId("1");
        List<RoomDealDTO> roomDealDTOS = Collections.singletonList(roomDealDTO);
        testClass.remapRoomDetails(roomDealDTOS, roomsDetailResponse);
        RoomDetailsDTO roomDetails = roomDealDTOS.get(0).getDetails();
        assertEquals(roomDetails.getName(), "roomName");
        assertEquals(roomDetails.getDescription(), "roomDescription- roomName");
        assertNotNull(roomDetails.getMainImage());
        assertEquals(roomDetails.getImages().size(), 1);
    }

    @Test
    public void testRoomDetailsWithoutRoomDetailsResponsePopulated() {
        when(roomsDetailResponse.getRoomDetailByProviderRoomId()).thenReturn(Collections.singletonMap("1", new RoomDetails()));
        RoomDealDTO roomDealDTO = new RoomDealDTO();
        roomDealDTO.setProviderRoomId("1");
        RoomDetailsDTO roomDetailsDTO = getRoomDetailsDTO("DTO.name", "DTO.description");
        roomDealDTO.setDetails(roomDetailsDTO);
        List<RoomDealDTO> roomDealDTOS = Collections.singletonList(roomDealDTO);

        testClass.remapRoomDetails(roomDealDTOS, roomsDetailResponse);

        RoomDetailsDTO roomDetails = roomDealDTOS.get(0).getDetails();
        assertEquals(roomDetails.getName(), roomDetailsDTO.getName());
        assertEquals(roomDetails.getDescription(), roomDetailsDTO.getDescription());
    }

    @Test
    public void testRoomDetailsWithoutRoomDetailsRsPopulated() {
        when(roomsDetailResponse.getRoomDetailByProviderRoomId()).thenReturn(Collections.singletonMap("1", new RoomDetails()));
        RoomDealDTO roomDealDTO = new RoomDealDTO();
        roomDealDTO.setProviderRoomId("1");
        roomDealDTO.setDescription("roomDealDescription");
        RoomDetailsDTO roomDetailsDTO = getRoomDetailsDTO("", "");
        roomDealDTO.setDetails(roomDetailsDTO);
        List<RoomDealDTO> roomDealDTOS = Collections.singletonList(roomDealDTO);

        testClass.remapRoomDetails(roomDealDTOS, roomsDetailResponse);

        RoomDetailsDTO roomDetails = roomDealDTOS.get(0).getDetails();
        assertEquals(roomDetails.getName(), null);
        assertEquals(roomDetails.getDescription(), roomDealDTO.getDescription());
    }

    private RoomDetailsDTO getRoomDetailsDTO(String name, String description) {
        RoomDetailsDTO roomDetailsDTO = new RoomDetailsDTO();
        roomDetailsDTO.setName(name);
        roomDetailsDTO.setDescription(description);
        return roomDetailsDTO;
    }

    private RoomDetails getRoomDetails() {
        RoomDetails roomDetails = new RoomDetails();
        roomDetails.setRoomDescription("roomDescription- roomName");
        roomDetails.setRoomName("roomName");
        roomDetails.setMainRoomImage(getRoomImage());
        roomDetails.setRoomImages(Collections.singletonList(getRoomImage()));
        return roomDetails;
    }

    private RoomImage getRoomImage() {
        RoomImage roomImage = new RoomImage();
        roomImage.setThumbnailUrl("turl");
        roomImage.setUrl("url");
        return roomImage;
    }

}
