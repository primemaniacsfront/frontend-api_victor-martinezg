package com.odigeo.frontend.resolvers.itinerary.mappers.segment;

import com.odigeo.dapi.client.SegmentResult;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class SegmentMapperTest {

    @Mock
    private SegmentResult result;
    @Mock
    private VisitInformation visit;

    private SegmentMapper mapper;
    private ShoppingCartContext context;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new SegmentMapperImpl();
        context = new ShoppingCartContext(visit);
    }

    @Test
    public void testSegmentContractToModel() {
        assertNull(mapper.segmentContractToModel(null, context));
        assertNotNull(mapper.segmentContractToModel(result, context));
    }

}
