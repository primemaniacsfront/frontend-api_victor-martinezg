package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NagType;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import org.apache.commons.collections4.CollectionUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class NagTypesMapperTest {

    @Mock
    AncillaryConfiguration ancillaryConfiguration;

    private NagTypesMapper mapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mapper = new NagTypesMapper();
    }

    @Test
    public void mapShouldReturnEmptyListWithNullValues() {
        when(ancillaryConfiguration.isContinuanceNagEnabled()).thenReturn(null);
        when(ancillaryConfiguration.isRejectNagEnabled()).thenReturn(null);

        List<NagType> nagTypes = mapper.map(ancillaryConfiguration);

        assertNotNull(nagTypes);
        assertTrue(CollectionUtils.isEmpty(nagTypes));
    }

    @Test
    public void mapShouldReturnEmptyListWithFalseValues() {
        when(ancillaryConfiguration.isContinuanceNagEnabled()).thenReturn(false);
        when(ancillaryConfiguration.isRejectNagEnabled()).thenReturn(false);

        List<NagType> nagTypes = mapper.map(ancillaryConfiguration);

        assertNotNull(nagTypes);
        assertTrue(CollectionUtils.isEmpty(nagTypes));
    }

    @Test
    public void mapShouldReturnTwoElementsWithTrueValues() {
        when(ancillaryConfiguration.isContinuanceNagEnabled()).thenReturn(true);
        when(ancillaryConfiguration.isRejectNagEnabled()).thenReturn(true);

        List<NagType> nagTypes = mapper.map(ancillaryConfiguration);

        assertTrue(CollectionUtils.isNotEmpty(nagTypes));
        assertEquals(nagTypes.size(), 2);
        assertTrue(nagTypes.contains(NagType.CONTINUANCE));
        assertTrue(nagTypes.contains(NagType.REJECT));
    }

}
