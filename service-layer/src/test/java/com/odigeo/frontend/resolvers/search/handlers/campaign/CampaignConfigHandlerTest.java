package com.odigeo.frontend.resolvers.search.handlers.campaign;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AirlineCampaignConfig;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CampaignConfig;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayConfig;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.itinerary.utils.ItineraryUtils;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class CampaignConfigHandlerTest {

    private static final Set<String> SECTIONS_CARRIERS_IDS = Collections.singleton("QR");
    private static final Set<String> SEGMENTS_CARRIERS_IDS = Collections.singleton("UX");
    @Mock
    private SiteVariations siteVariations;

    @Mock
    private PrimeDayHandler primeDayHandler;

    @Mock
    private AirlineCampaignHandler airlineCampaignHandler;

    @Mock
    private SearchItineraryDTO itinerary;

    @Mock
    private Itinerary shoppingItinerary;

    @Mock
    private ItineraryUtils itineraryUtils;

    @Mock
    private VisitInformation visit;

    @InjectMocks
    private CampaignConfigHandler handler;

    @Mock
    private PrimeDayConfig primeDayConfig;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(airlineCampaignHandler.populateCampaignConfig(itinerary.getSegmentsCarrierIds())).thenReturn(new AirlineCampaignConfig());
        when(primeDayHandler.populatePrimeDayConfig(itinerary.getSectionsCarrierIds(), visit)).thenReturn(primeDayConfig);
        when(itineraryUtils.getSectionsCarrierIds(eq(shoppingItinerary))).thenReturn(SECTIONS_CARRIERS_IDS);
        when(itineraryUtils.getSegmentsCarrierIds(eq(shoppingItinerary))).thenReturn(SEGMENTS_CARRIERS_IDS);
        when(airlineCampaignHandler.populateCampaignConfig(SEGMENTS_CARRIERS_IDS)).thenReturn(new AirlineCampaignConfig());
        when(primeDayHandler.populatePrimeDayConfig(SECTIONS_CARRIERS_IDS, visit)).thenReturn(primeDayConfig);
    }

    @Test
    public void testAllCampaignsEnabled() {
        when(siteVariations.isAirlineCampaignEnabled(any())).thenReturn(true);
        when(siteVariations.isPrimeDayCampaignEnabled(any())).thenReturn(true);
        CampaignConfig campaignConfig = handler.populateCampaignConfig(itinerary, visit);
        verify(primeDayHandler).populatePrimeDayConfig(itinerary.getSectionsCarrierIds(), visit);
        verify(airlineCampaignHandler).populateCampaignConfig(itinerary.getSegmentsCarrierIds());
        assertNotNull(campaignConfig);
        assertNotNull(campaignConfig.getAirlineCampaignConfig());
        assertNotNull(campaignConfig.getPrimeDayConfig());
        // TODO Delete when primeDayConfig isn't in itinerary
        verify(itinerary).setPrimeDayConfig(primeDayConfig);
    }

    @Test
    public void testPrimeDayCampaignNotEnabled() {
        when(siteVariations.isAirlineCampaignEnabled(any())).thenReturn(true);
        when(siteVariations.isPrimeDayCampaignEnabled(any())).thenReturn(false);
        CampaignConfig campaignConfig = handler.populateCampaignConfig(itinerary, visit);
        // TODO Delete when primeDayConfig isn't in itinerary
        verify(itinerary).setPrimeDayConfig(primeDayConfig);
        // Modify in clean primeDayConfig refactor
        // verifyNoInteractions(primeDayHandler);
        assertNull(campaignConfig.getPrimeDayConfig());
    }

    @Test
    public void testAirlineCampaignNotEnabled() {
        when(siteVariations.isAirlineCampaignEnabled(any())).thenReturn(false);
        when(siteVariations.isPrimeDayCampaignEnabled(any())).thenReturn(true);
        CampaignConfig campaignConfig = handler.populateCampaignConfig(itinerary, visit);
        verifyNoInteractions(airlineCampaignHandler);
        assertNull(campaignConfig.getAirlineCampaignConfig());
    }

    @Test
    public void testGetAllCampaignsEnabled() {
        when(siteVariations.isAirlineCampaignEnabled(any())).thenReturn(true);
        when(siteVariations.isPrimeDayCampaignEnabled(any())).thenReturn(true);
        CampaignConfig campaignConfig = handler.getCampaignConfig(shoppingItinerary, visit);

        verify(primeDayHandler).populatePrimeDayConfig(SECTIONS_CARRIERS_IDS, visit);
        verify(airlineCampaignHandler).populateCampaignConfig(SEGMENTS_CARRIERS_IDS);
        assertNotNull(campaignConfig);
        assertNotNull(campaignConfig.getAirlineCampaignConfig());
        assertNotNull(campaignConfig.getPrimeDayConfig());
        // TODO Delete when primeDayConfig isn't in itinerary
        verify(shoppingItinerary).setPrimeDayConfig(primeDayConfig);
    }

    @Test
    public void testGetPrimeDayCampaignNotEnabledItinerary() {
        when(siteVariations.isAirlineCampaignEnabled(any())).thenReturn(true);
        when(siteVariations.isPrimeDayCampaignEnabled(any())).thenReturn(false);
        CampaignConfig campaignConfig = handler.getCampaignConfig(shoppingItinerary, visit);

        // TODO Delete when primeDayConfig isn't in itinerary
        verify(shoppingItinerary).setPrimeDayConfig(primeDayConfig);
        // Modify in clean primeDayConfig refactor
        // verifyNoInteractions(primeDayHandler);
        assertNull(campaignConfig.getPrimeDayConfig());
    }

    @Test
    public void testGetAirlineCampaignNotEnabled() {
        when(siteVariations.isAirlineCampaignEnabled(any())).thenReturn(false);
        when(siteVariations.isPrimeDayCampaignEnabled(any())).thenReturn(true);
        CampaignConfig campaignConfig = handler.getCampaignConfig(shoppingItinerary, visit);


        verifyNoInteractions(airlineCampaignHandler);
        assertNull(campaignConfig.getAirlineCampaignConfig());
    }
}
