package com.odigeo.frontend.resolvers.search;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamilyRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PriceAlertRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.onefront.OFDebugInfoIntegration;
import com.odigeo.frontend.onefront.OFSearchIntegration;
import com.odigeo.frontend.resolvers.search.handlers.SearchChecker;
import com.odigeo.frontend.resolvers.search.handlers.SearchDebugHandler;
import com.odigeo.frontend.resolvers.search.handlers.SearchRequestHandler;
import com.odigeo.frontend.resolvers.search.handlers.SearchResponseHandler;
import com.odigeo.frontend.resolvers.search.handlers.alert.PriceAlertHandler;
import com.odigeo.frontend.resolvers.search.handlers.farefamily.FareFamilyHandler;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class SearchResolverTest {

    private static final String SEARCH_QUERY = "search";

    @Mock
    private SearchRequestHandler searchRequestHandler;
    @Mock
    private SearchResponseHandler searchResponseHandler;
    @Mock
    private FareFamilyHandler fareFamilyHandler;
    @Mock
    private PriceAlertHandler priceAlertHandler;
    @Mock
    private SearchService searchService;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private OFSearchIntegration ofSearchIntegration;
    @Mock
    private OFDebugInfoIntegration ofDebugInfoIntegration;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private SearchDebugHandler debugHandler;
    @Mock
    private SearchChecker searchChecker;

    @InjectMocks
    private SearchResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(env.getContext()).thenReturn(resolverContext);
        when(env.getGraphQlContext()).thenReturn(context);
        when(resolverContext.getVisitInformation()).thenReturn(visit);
        when(context.get(MetricsHandler.class)).thenReturn(metricsHandler);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(SEARCH_QUERY));
    }

    @Test
    public void testSearchFetcher() {
        SearchResponse searchResponse = mock(SearchResponse.class);
        SearchMethodRequest searchRequest = mock(SearchMethodRequest.class);
        SearchResponseDTO response = mock(SearchResponseDTO.class);
        SearchRequest requestParams = mock(SearchRequest.class);

        when(jsonUtils.fromDataFetching(env, SearchRequest.class)).thenReturn(requestParams);
        when(searchRequestHandler.buildSearchRequest(requestParams, resolverContext)).thenReturn(searchRequest);
        when(searchService.executeSearch(searchRequest)).thenReturn(searchResponse);
        when(searchResponseHandler.buildSearchResponse(searchResponse, requestParams, resolverContext)).thenReturn(response);

        assertSame(resolver.searchFetcher(env), response);
        verify(ofSearchIntegration).sendSearchResponse(requestParams, searchResponse, resolverContext);
        verify(ofDebugInfoIntegration).requestDebugInfo(resolverContext);
        verify(debugHandler).addApiCaptureInfo(resolverContext, searchRequest, searchResponse);
        verify(debugHandler).addQaModeSearchInfo(resolverContext, searchResponse);
        verify(searchChecker).check(requestParams, resolverContext);
    }

    @Test
    public void testPriceAlertFetcher() {
        PriceAlertRequest requestParams = mock(PriceAlertRequest.class);

        when(jsonUtils.fromDataFetching(env, PriceAlertRequest.class)).thenReturn(requestParams);

        assertTrue(resolver.priceAlertFetcher(env));
        verify(priceAlertHandler).subscribePriceAlerts(requestParams, visit);
    }

    @Test
    public void testFareFamilyFetcher() {
        FareFamilyRequest request = mock(FareFamilyRequest.class);
        List<FareFamily> response = Collections.singletonList(mock(FareFamily.class));

        when(jsonUtils.fromDataFetching(env, FareFamilyRequest.class)).thenReturn(request);
        when(fareFamilyHandler.retrieveFareFamilies(request, visit)).thenReturn(response);

        assertSame(resolver.fareFamilyFetcher(env), response);
    }

}
