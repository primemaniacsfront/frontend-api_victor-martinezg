package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CarbonFootprint;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CarbonFootprintDebugHandlerTest {

    @InjectMocks
    private CarbonFootprintDebugHandler carbonFootprintDebugHandler;

    private List<SearchItineraryDTO> itineraries;

    @Mock
    private SearchItineraryDTO searchItineraryDTO;

    @Mock
    private CarbonFootprint carbonFootprint;

    private static final BigDecimal AVERAGE_KILOS = BigDecimal.TEN;

    private static final String ITINERARY_ID = "ITINERARY_ID";

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(searchItineraryDTO.getId()).thenReturn(ITINERARY_ID);
        when(searchItineraryDTO.getCarbonFootprint()).thenReturn(carbonFootprint);

        itineraries = Collections.singletonList(searchItineraryDTO);

    }

    @Test
    public void testDebugInformation() {

        initCarbonFootprint(true, BigDecimal.ONE, BigDecimal.TEN, 30);

        Map<String, Object> debugInformation = carbonFootprintDebugHandler.getDebugInformation(itineraries, AVERAGE_KILOS);
        assertEquals(debugInformation.size(), 1);
        assertEquals(debugInformation.get(ITINERARY_ID), "isEco: true, totalCo2Kilos: 1, totalCo2eKilos: 10, percentage than average: 30. Kilos average search: 10");
    }

    @Test
    public void testDebugWithNullInformation() {

        initCarbonFootprint(false, null, null, null);

        Map<String, Object> debugInformation = carbonFootprintDebugHandler.getDebugInformation(itineraries, null);
        assertEquals(debugInformation.size(), 1);
        assertEquals(debugInformation.get(ITINERARY_ID), "isEco: false, totalCo2Kilos: null, totalCo2eKilos: null, percentage than average: null. Kilos average search: null");
    }

    @Test
    public void testDebugInformationFalse() {

        initCarbonFootprint(false, BigDecimal.valueOf(9.34), BigDecimal.valueOf(19.99), 9);

        Map<String, Object> debugInformation = carbonFootprintDebugHandler.getDebugInformation(itineraries, AVERAGE_KILOS);
        assertEquals(debugInformation.size(), 1);
        assertEquals(debugInformation.get(ITINERARY_ID), "isEco: false, totalCo2Kilos: 9.34, totalCo2eKilos: 19.99, percentage than average: 9. Kilos average search: 10");
    }

    @Test
    public void testDebugWhitNullCarbonFootprint() {

        when(searchItineraryDTO.getCarbonFootprint()).thenReturn(null);

        Map<String, Object> debugInformation = carbonFootprintDebugHandler.getDebugInformation(itineraries, AVERAGE_KILOS);
        assertEquals(debugInformation.size(), 1);
        assertEquals(debugInformation.get(ITINERARY_ID), "Non info. Kilos average search: 10");
    }

    @Test
    public void testDebugNonInformation() {

        itineraries = Collections.emptyList();

        Map<String, Object> debugInformation = carbonFootprintDebugHandler.getDebugInformation(itineraries, AVERAGE_KILOS);
        assertEquals(debugInformation.size(), 0);
    }

    private void initCarbonFootprint(boolean isEco, BigDecimal totalCo2Kilos, BigDecimal totalCo2eKilos, Integer ecoPercentageThanAverage) {
        when(carbonFootprint.getIsEco()).thenReturn(isEco);
        when(carbonFootprint.getTotalCo2eKilos()).thenReturn(totalCo2eKilos);
        when(carbonFootprint.getTotalCo2Kilos()).thenReturn(totalCo2Kilos);
        when(carbonFootprint.getEcoPercentageThanAverage()).thenReturn(ecoPercentageThanAverage);
    }
}
