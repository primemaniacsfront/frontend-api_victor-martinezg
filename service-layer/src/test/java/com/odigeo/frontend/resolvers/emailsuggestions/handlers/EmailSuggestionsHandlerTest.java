package com.odigeo.frontend.resolvers.emailsuggestions.handlers;

import com.google.common.collect.ImmutableMap;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.emailsuggestions.configuration.EmailSuggestionsConfiguration;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EmailSuggestionsHandlerTest {

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private EmailSuggestionsConfiguration emailSuggestionsConfiguration;

    private EmailSuggestionsHandler handler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visit);
        when(context.getVisitInformation().getLocale()).thenReturn("en");
        Site site = mock(Site.class);
        when(context.getVisitInformation().getSite()).thenReturn(site);
        configureParameters();
    }

    private void configureParameters() {
        doReturn(initSuggestEmailGenericDomainsTest()).when(emailSuggestionsConfiguration).getSuggestEmailGenericDomains();
        doReturn(initEmailProvidersAutocompleteTest()).when(emailSuggestionsConfiguration).getEmailProvidersAutocomplete();

        handler = new EmailSuggestionsHandler(emailSuggestionsConfiguration);
    }

    @Test
    public void testGetEmailSuggestions() {
        String email = "gmail";
        handler.getEmailSuggestions(email, context);
    }

    private List<String> initSuggestEmailGenericDomainsTest() {
        return Arrays.asList("gmail.com",
                "hotmail.com",
                "yahoo.com",
                "outlook.com",
                "facebook.com",
                "aol.com",
                "icloud.com",
                "att.net");
    }

    private Map<String, List<String>> initEmailProvidersAutocompleteTest() {
        return ImmutableMap.of("DE", Arrays.asList("gmx.de", "web.de"),
                "FR", Arrays.asList("hotmail.fr", "orange.fr", "yahoo.fr", "gmx.fr", "live.fr", "sfr.fr"));
    }
}
