package com.odigeo.frontend.resolvers.accommodation.search.models;

import bean.test.BeanTest;

public class FeeInfoDTOTest extends BeanTest<FeeInfoDTO> {

    @Override
    protected FeeInfoDTO getBean() {
        FeeInfoDTO bean = new FeeInfoDTO();
        bean.setPaymentFee(new FeeDetailsDTO());
        bean.setSearchFee(new FeeDetailsDTO());
        return bean;
    }
}
