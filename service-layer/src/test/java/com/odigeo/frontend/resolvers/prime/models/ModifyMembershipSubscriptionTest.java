package com.odigeo.frontend.resolvers.prime.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionActions;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import org.apache.commons.lang3.StringUtils;

public class ModifyMembershipSubscriptionTest extends BeanTest<ModifyMembershipSubscription> {

    @Override
    protected ModifyMembershipSubscription getBean() {
        return ModifyMembershipSubscription.builder()
                .action(MembershipSubscriptionActions.EDIT)
                .shoppingInfo(new ShoppingInfo())
                .email(StringUtils.EMPTY)
                .lastNames(StringUtils.EMPTY)
                .names(StringUtils.EMPTY)
                .offerId(StringUtils.EMPTY)
                .build();
    }
}
