package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatMapSegment;
import com.odigeo.itineraryapi.v1.response.AncillaryOptionsContainer;
import com.odigeo.itineraryapi.v1.response.SeatMapResultsResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SeatMapSegmentMapperTest {

    @Mock
    private SeatMapMapper seatMapMapper;

    @InjectMocks
    private SeatMapSegmentMapper seatMapSegmentMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(seatMapMapper.map(Mockito.any())).thenReturn(new ArrayList<>());
    }

    @Test
    public void mapValidSeatMapSegment() {
        AncillaryOptionsContainer providerSeatMapSegment = getInput();
        List<SeatMapSegment> mappedSeatMapSegmentList = seatMapSegmentMapper.map(providerSeatMapSegment);
        List<SeatMapSegment> expectedSeatMapSegmentList = getOutput();
        SeatMapSegment expectedSeatMapSegment = expectedSeatMapSegmentList.get(0);
        SeatMapSegment mappedSeatMapSegment = mappedSeatMapSegmentList.get(0);

        assertEquals(providerSeatMapSegment.getSeatMapResultsResponse().size(), mappedSeatMapSegmentList.size(), expectedSeatMapSegmentList.size());
        assertEquals(mappedSeatMapSegment.getSeatMaps(), expectedSeatMapSegment.getSeatMaps());
        assertEquals(mappedSeatMapSegment.getSegment(), expectedSeatMapSegment.getSegment());
    }

    private AncillaryOptionsContainer getInput() {
        AncillaryOptionsContainer mockAncillaryOptions = new AncillaryOptionsContainer();
        SeatMapResultsResponse mockSeatMapResultsResponse = new SeatMapResultsResponse();
        Set<SeatMapResultsResponse> mockSeatMapResultsResponseSet = new HashSet<>();

        mockSeatMapResultsResponse.setSeatMapPreferencesDescriptorItemList(new ArrayList<>());
        mockSeatMapResultsResponse.setSegment(0);

        mockSeatMapResultsResponseSet.add(mockSeatMapResultsResponse);

        mockAncillaryOptions.setSeatMapResultsResponse(mockSeatMapResultsResponseSet);

        return mockAncillaryOptions;
    }

    private List<SeatMapSegment> getOutput() {
        List<SeatMapSegment> outputSeatMapSegmentList = new ArrayList<>();
        SeatMapSegment outputSeatMapSegment = new SeatMapSegment();

        outputSeatMapSegment.setSeatMaps(new ArrayList<>());
        outputSeatMapSegment.setSegment(0);

        outputSeatMapSegmentList.add(outputSeatMapSegment);
        return outputSeatMapSegmentList;
    }
}
