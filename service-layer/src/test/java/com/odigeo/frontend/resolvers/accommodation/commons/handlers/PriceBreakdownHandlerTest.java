package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;
import com.odigeo.frontend.services.currencyconvert.CurrencyConvertHandler;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDeal;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDealPriceCalculatorAdapter;
import com.odigeo.searchengine.v2.responses.accommodation.RoomGroupDeal;
import com.odigeo.searchengine.v2.responses.accommodation.RoomGroupDealInformation;
import com.odigeo.searchengine.v2.responses.fee.FeeDetails;
import com.odigeo.searchengine.v2.responses.fee.FeeInfo;
import com.odigeo.searchengine.v2.responses.membership.MembershipPerks;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class PriceBreakdownHandlerTest {

    private static final BigDecimal PACKAGE_DISCOUNT = new BigDecimal(2);
    private static final BigDecimal PRIME_DISCOUNT = new BigDecimal(1);
    private static final BigDecimal TOTAL_PRICE = new BigDecimal(10);

    private static final String SITE_CURRENCY = "EUR";
    private static final int NUM_NIGHTS = 1;

    @Mock
    private CurrencyConvertHandler currencyConvertHandler;
    @Mock
    private VisitInformation visit;

    @InjectMocks
    private PriceBreakdownHandler testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(visit.getCurrency()).thenReturn(SITE_CURRENCY);
    }

    @Test
    public void testMapPriceBreakdown() {
        AccommodationDeal accommodationDeal = mock(AccommodationDeal.class);
        when(accommodationDeal.getProviderCurrency()).thenReturn(SITE_CURRENCY);
        MembershipPerks membershipPerks = mock(MembershipPerks.class);
        when(accommodationDeal.getMembershipPerks()).thenReturn(membershipPerks);
        when(membershipPerks.getFee()).thenReturn(PRIME_DISCOUNT);
        when(accommodationDeal.getDynpackFareDiscount()).thenReturn(PACKAGE_DISCOUNT.floatValue());
        AccommodationDealPriceCalculatorAdapter priceCal = mock(AccommodationDealPriceCalculatorAdapter.class);
        when(accommodationDeal.getPrice()).thenReturn(priceCal);
        when(priceCal.getTotalPrice()).thenReturn(TOTAL_PRICE);
        when(currencyConvertHandler.convertToSiteCurrency(any(VisitInformation.class), any(BigDecimal.class), anyString())).thenReturn(PACKAGE_DISCOUNT);


        PriceBreakdownDTO priceBreakdown = testClass.mapPriceBreakdown(accommodationDeal, visit, NUM_NIGHTS);
        BigDecimal expectedPercentage = new BigDecimal("23.00");

        assertEquals(priceBreakdown.getPrice(), TOTAL_PRICE);
        assertEquals(priceBreakdown.getPriceWithoutDiscount(), TOTAL_PRICE.add(PACKAGE_DISCOUNT.add(PRIME_DISCOUNT)));
        assertEquals(priceBreakdown.getDiscount().getPercentage(), expectedPercentage);
        assertEquals(priceBreakdown.getDiscount().getPackageAmount(), PACKAGE_DISCOUNT);
        assertEquals(priceBreakdown.getDiscount().getPrimeAmount(), PRIME_DISCOUNT);
    }

    @Test
    public void testMapPriceBreakdownRoom() {
        RoomGroupDeal roomGroupDeal = mock(RoomGroupDeal.class);
        RoomGroupDealInformation roomGroupDealInformation = mock(RoomGroupDealInformation.class);
        AccommodationDealPriceCalculatorAdapter priceCal = mock(AccommodationDealPriceCalculatorAdapter.class);
        when(roomGroupDeal.getRoomGroupDealInformation()).thenReturn(roomGroupDealInformation);
        when(roomGroupDeal.getDynpackFareDiscount()).thenReturn(PACKAGE_DISCOUNT.floatValue());
        when(roomGroupDealInformation.getPrice()).thenReturn(priceCal);
        when(priceCal.getTotalPrice()).thenReturn(TOTAL_PRICE);
        when(currencyConvertHandler.convertToSiteCurrency(any(VisitInformation.class), any(BigDecimal.class), anyString())).thenReturn(PACKAGE_DISCOUNT);
        FeeInfo feeInfo = mock(FeeInfo.class);
        when(priceCal.getFeeInfo()).thenReturn(feeInfo);
        FeeDetails feeDetails = mock(FeeDetails.class);
        when(feeInfo.getSearchFee()).thenReturn(feeDetails);
        when(feeDetails.getDiscount()).thenReturn(PRIME_DISCOUNT);

        PriceBreakdownDTO priceBreakdownDTO = testClass.mapPriceBreakdown(roomGroupDeal, SITE_CURRENCY, visit);

        BigDecimal expectedPercentage = new BigDecimal("23.00");

        assertEquals(priceBreakdownDTO.getCurrency(), visit.getCurrency());

        assertEquals(priceBreakdownDTO.getPrice(), TOTAL_PRICE);
        assertEquals(priceBreakdownDTO.getPriceWithoutDiscount(), TOTAL_PRICE.add(PACKAGE_DISCOUNT.add(PRIME_DISCOUNT)));
        assertEquals(priceBreakdownDTO.getDiscount().getPercentage(), expectedPercentage);
        assertEquals(priceBreakdownDTO.getDiscount().getPackageAmount(), PACKAGE_DISCOUNT);
        assertEquals(priceBreakdownDTO.getDiscount().getPrimeAmount(), PRIME_DISCOUNT);
    }
}
