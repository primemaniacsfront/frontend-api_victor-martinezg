package com.odigeo.frontend.resolvers.geolocation.country.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country;
import com.odigeo.geoapi.v4.utils.LocalizedText;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Locale;

import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class CountryMapperTest {
    public static final String COUNTRY_NAME = "SPAIN";
    public static final int ID = 12;
    public static final String PHONE = "+34";
    public static final String COUNTRY_CODE = "ES";
    private CountryMapper mapper;

    @Mock
    com.odigeo.geoapi.v4.responses.Country country;

    @Mock
    Locale locale;
    @Mock
    LocalizedText localizedText;

    @BeforeMethod
    public void init() {
        openMocks(this);
        mapper = new CountryMapperImpl();
    }

    @Test
    public void mapCountryList() {
        setUpCountry();
        Country countryDto = mapper.mapModelToContract(country, locale);
        assertEquals(countryDto.getName(), COUNTRY_NAME);
        assertEquals(countryDto.getCode(), COUNTRY_CODE);
        assertEquals(countryDto.getId(), ID);
        assertEquals(countryDto.getPhonePrefix(), PHONE);
    }

    private void setUpCountry() {
        when(localizedText.getText(any(Locale.class))).thenReturn(COUNTRY_NAME);
        when(country.getName()).thenReturn(localizedText);
        when(country.getGeoNodeId()).thenReturn(ID);
        when(country.getPhonePrefix()).thenReturn(PHONE);
        when(country.getCountryCode()).thenReturn(COUNTRY_CODE);
    }
}
