package com.odigeo.frontend.resolvers.deals.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Deal;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.odigeo.frontend.commons.util.BigDecimalUtils;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.utils.LocalizedText;
import com.odigeo.marketing.search.price.v1.requests.PriceType;
import com.odigeo.marketing.search.price.v1.responses.RoutePrice;
import com.odigeo.marketing.search.price.v1.util.Money;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.AbstractMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class DealsRoutePriceMapperTest {

    private static final LocalDate OUTBOUND_DATE = LocalDate.now().plus(Period.ofMonths(2));
    private static final LocalDate INBOUND_DATE = LocalDate.now().plus(Period.ofMonths(2))
            .plus(Period.ofDays(2));
    private static final String DESTINATION_IATA = "DES";
    private static final String DESTINATION_NAME = "Destination";
    private static final double DIFFERENCE_DISCOUNT = 20.0;

    private static final Money EUR_100 = new Money.Builder().amount(BigDecimal.valueOf(100.00)).currency("EUR").build();
    private static final Money EUR_80 = new Money.Builder().amount(BigDecimal.valueOf(80.00)).currency("EUR").build();

    private final Map<String, Money> apparentPrices = Stream.of(
            new AbstractMap.SimpleEntry<>(PriceType.MINIMUM_PURCHASABLE_PRICE.name(), EUR_100),
            new AbstractMap.SimpleEntry<>(PriceType.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE.name(), EUR_80)
    ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

    @Mock
    private RoutePrice routePrice;
    @Mock
    private Map<String, City> destinations;
    @Mock
    private Locale locale;
    @Mock
    private City destination;
    @Mock
    private LocalizedText destinationName;

    private final MoneyMapper moneyMapper = new MoneyMapperImpl();
    private final ApparentPricesMapper apparentPricesMapper = new ApparentPricesMapper();
    private final CityMapper cityMapper = new CityMapper();
    private final BigDecimalUtils bigDecimalUtils = new BigDecimalUtils();

    private final DealsRoutePriceMapper mapper = new DealsRoutePriceMapper();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Injector injector = Guice.createInjector(binder -> {
            binder.bind(MoneyMapper.class).toInstance(moneyMapper);
            binder.bind(ApparentPricesMapper.class).toInstance(apparentPricesMapper);
            binder.bind(CityMapper.class).toInstance(cityMapper);
            binder.bind(BigDecimalUtils.class).toInstance(bigDecimalUtils);
        });
        injector.injectMembers(mapper);

        when(destinationName.getText(locale)).thenReturn(DESTINATION_NAME);
        when(destination.getIataCode()).thenReturn(DESTINATION_IATA);
        when(destination.getName()).thenReturn(destinationName);
        when(destinations.get(DESTINATION_IATA)).thenReturn(destination);

        when(routePrice.getOutboundDate()).thenReturn(OUTBOUND_DATE);
        when(routePrice.getInboundDate()).thenReturn(INBOUND_DATE);
        when(routePrice.getInboundCityIataCode()).thenReturn(DESTINATION_IATA);
    }

    @Test
    public void testMap() {
        when(routePrice.getApparentPricesByPax()).thenReturn(apparentPrices);

        Deal deal = mapper.map(routePrice, destinations, locale);

        assertEquals(deal.getDepartureDate(), OUTBOUND_DATE);
        assertEquals(deal.getArrivalDate(), INBOUND_DATE);
        assertEquals(deal.getDestination().getName(), DESTINATION_NAME);
        assertEquals(deal.getDestination().getIata(), DESTINATION_IATA);
        assertEquals(deal.getDiscount(), DIFFERENCE_DISCOUNT);
        deal.getApparentPricesByPax()
            .stream()
            .forEach(apparentPrice -> {
                assertEquals(apparentPrice.getPrice().getAmount(), apparentPrices.get(apparentPrice.getId()).getAmount());
                assertEquals(apparentPrice.getPrice().getCurrency(), apparentPrices.get(apparentPrice.getId()).getCurrency());
            });
    }

    @Test
    public void testMapEmpty() {
        Deal deal = mapper.map(routePrice, destinations, locale);

        assertEquals(deal.getDepartureDate(), OUTBOUND_DATE);
        assertEquals(deal.getArrivalDate(), INBOUND_DATE);
        assertEquals(deal.getDestination().getName(), DESTINATION_NAME);
        assertEquals(deal.getDestination().getIata(), DESTINATION_IATA);
        assertEquals(deal.getDiscount(), 0D);
        assertEquals(deal.getApparentPricesByPax().size(), 0);
    }

}
