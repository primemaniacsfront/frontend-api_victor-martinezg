package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.insuranceproduct.api.last.request.OfferId;
import com.edreamsodigeo.insuranceproduct.api.last.request.ShoppingBasketId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectInsuranceOfferRequest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class SelectRequestMapperTest {

    private static final String VERSION = "V1";
    private static final long SHOPPING_PRODUCT_ID = 60L;
    private static final String ID = "offerId";

    @Mock
    private OfferIdMapper offerIdMapper;

    @Mock
    private ShoppingBasketIdMapper shoppingBasketIdMapper;

    @InjectMocks
    private SelectRequestMapper selectRequestMapper;

    @BeforeMethod
    public void init() {
        openMocks(this);
        when(offerIdMapper.map(any())).thenReturn(getOffers());
        when(shoppingBasketIdMapper.map(any())).thenReturn(getShoppingBasketId());
    }

    @Test
    public void mapOk() {
        com.edreamsodigeo.insuranceproduct.api.last.request.SelectRequest request = selectRequestMapper.map(new SelectInsuranceOfferRequest());
        assertEquals(request.getOfferId().getId(), getOffers().getId());
        assertEquals(request.getOfferId().getVersion(), getOffers().getVersion());
        assertEquals(request.getShoppingBasketId().getId(), getShoppingBasketId().getId());
    }

    private OfferId getOffers() {
        OfferId offerId = new OfferId();
        offerId.setId(ID);
        offerId.setVersion(VERSION);
        return offerId;
    }

    private ShoppingBasketId getShoppingBasketId() {
        ShoppingBasketId shoppingBasketId = new ShoppingBasketId();
        shoppingBasketId.setId(SHOPPING_PRODUCT_ID);
        return shoppingBasketId;
    }
}
