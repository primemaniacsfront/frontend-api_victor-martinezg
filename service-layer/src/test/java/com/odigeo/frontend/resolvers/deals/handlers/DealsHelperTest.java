package com.odigeo.frontend.resolvers.deals.handlers;

import com.edreamsodigeo.seo.sslppopularitystats.popularity.v1.model.responses.CityBookingAggregation;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.mappers.GeoCoordinatesMapper;
import com.odigeo.frontend.commons.util.GeoUtils;
import com.odigeo.frontend.resolvers.deals.configuration.DealsConfiguration;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.frontend.services.PopularityStatsService;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Coordinates;
import com.odigeo.marketing.search.price.v1.requests.IataPair;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class DealsHelperTest {

    private static final Integer MAX_POPULAR_DESTINATIONS = 50;
    private static final String AGGREGATE_BY = "AGGREGATE_BY";
    private static final Double MAX_DISTANCE_KM = 3000D;

    private static final String IATA_DEP = "DEP";
    private static final int ID_DEP = 1;

    private static final String IATA_ARR = "ARR";
    private static final int ID_ARR = 2;

    @Mock
    private City departure;
    @Mock
    private City destination;
    @Mock
    private CityBookingAggregation cityBookingAggregation;

    @Mock
    private DealsConfiguration dealsConfiguration;
    @Mock
    private GeoService geoService;
    @Mock
    private PopularityStatsService popularityStatsService;

    private final Site site = Site.GB;
    private final GeoUtils geoUtils = new GeoUtils();
    private final GeoCoordinatesMapper geoCoordinatesMapper = new GeoCoordinatesMapper();

    private final DealsHelper helper = new DealsHelper();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Injector injector = Guice.createInjector(binder -> {
            binder.bind(GeoUtils.class).toProvider(() -> geoUtils);
            binder.bind(GeoCoordinatesMapper.class).toProvider(() -> geoCoordinatesMapper);
            binder.bind(DealsConfiguration.class).toProvider(() -> dealsConfiguration);
            binder.bind(GeoService.class).toProvider(() -> geoService);
            binder.bind(PopularityStatsService.class).toProvider(() -> popularityStatsService);
        });
        injector.injectMembers(helper);

        when(dealsConfiguration.getMaxPopularDestinations()).thenReturn(MAX_POPULAR_DESTINATIONS);
        when(dealsConfiguration.getAggregateBy()).thenReturn(AGGREGATE_BY);
        when(dealsConfiguration.getMaxDistanceInKm()).thenReturn(MAX_DISTANCE_KM);

        when(departure.getGeoNodeId()).thenReturn(ID_DEP);
        when(departure.getIataCode()).thenReturn(IATA_DEP);

        Coordinates depCoordinates = mock(Coordinates.class);
        when(depCoordinates.getLatitude()).thenReturn(BigDecimal.valueOf(40.41));
        when(depCoordinates.getLongitude()).thenReturn(BigDecimal.valueOf(3.70));
        when(departure.getCoordinates()).thenReturn(depCoordinates);
        when(departure.getCity()).thenReturn(departure);

        when(destination.getGeoNodeId()).thenReturn(ID_ARR);
        when(destination.getIataCode()).thenReturn(IATA_ARR);

        Coordinates arrCoordinates = mock(Coordinates.class);
        when(arrCoordinates.getLatitude()).thenReturn(BigDecimal.valueOf(41.38));
        when(arrCoordinates.getLongitude()).thenReturn(BigDecimal.valueOf(2.16));
        when(destination.getCoordinates()).thenReturn(arrCoordinates);
        when(destination.getCity()).thenReturn(destination);
    }

    @Test
    public void testGetPopularDestinationCities() throws GeoNodeNotFoundException {
        when(dealsConfiguration.getBaseDestinations()).thenReturn(Collections.singletonList(ID_ARR));
        when(cityBookingAggregation.getCityGeonodeId()).thenReturn(ID_ARR);
        when(popularityStatsService.getPopularDestinationCities(site, AGGREGATE_BY, MAX_POPULAR_DESTINATIONS))
                .thenReturn(Collections.singletonList(cityBookingAggregation));
        when(geoService.getCityByGeonodeId(ID_ARR)).thenReturn(destination);

        Map<String, City> result = helper.getPopularDestinationCities(departure, site);

        assertEquals(result.size(), 1);
        assertEquals(result.get(IATA_ARR).getCity(), destination);
        verify(popularityStatsService).getPopularDestinationCities(any(Site.class), any(String.class), any(Integer.class));
        verify(geoService).getCityByGeonodeId(any(Integer.class));
    }

    @Test
    public void testGetPopularDestinationCitiesFiltersDeparture() throws GeoNodeNotFoundException {
        when(dealsConfiguration.getBaseDestinations()).thenReturn(Arrays.asList(ID_ARR, ID_DEP));
        when(cityBookingAggregation.getCityGeonodeId()).thenReturn(ID_ARR);
        when(popularityStatsService.getPopularDestinationCities(site, AGGREGATE_BY, MAX_POPULAR_DESTINATIONS))
                .thenReturn(Collections.singletonList(cityBookingAggregation));
        when(geoService.getCityByGeonodeId(ID_ARR)).thenReturn(destination);
        when(geoService.getCityByGeonodeId(ID_DEP)).thenReturn(departure);

        Map<String, City> result = helper.getPopularDestinationCities(departure, site);

        assertEquals(result.size(), 1);
        assertNull(result.get(IATA_DEP));
        assertEquals(result.get(IATA_ARR).getCity(), destination);
        verify(popularityStatsService).getPopularDestinationCities(any(Site.class), any(String.class), any(Integer.class));
        verify(geoService).getCityByGeonodeId(any(Integer.class));
    }

    @Test
    public void testGetPopularDestinationCitiesDistanceOutOfRange() throws GeoNodeNotFoundException {
        Coordinates arrCoordinates = mock(Coordinates.class);
        when(arrCoordinates.getLatitude()).thenReturn(BigDecimal.valueOf(40.71));
        when(arrCoordinates.getLongitude()).thenReturn(BigDecimal.valueOf(74.07));
        when(destination.getCoordinates()).thenReturn(arrCoordinates);

        when(dealsConfiguration.getBaseDestinations()).thenReturn(Collections.singletonList(ID_ARR));
        when(cityBookingAggregation.getCityGeonodeId()).thenReturn(ID_ARR);
        when(popularityStatsService.getPopularDestinationCities(site, AGGREGATE_BY, MAX_POPULAR_DESTINATIONS))
                .thenReturn(Collections.singletonList(cityBookingAggregation));
        when(geoService.getCityByGeonodeId(ID_ARR)).thenReturn(destination);

        Map<String, City> result = helper.getPopularDestinationCities(departure, site);

        assertEquals(result.size(), 0);
        assertNull(result.get(IATA_ARR));
        verify(popularityStatsService).getPopularDestinationCities(any(Site.class), any(String.class), any(Integer.class));
        verify(geoService).getCityByGeonodeId(any(Integer.class));
    }

    @Test
    public void testGetPopularDestinationCitiesNotValidIata() throws GeoNodeNotFoundException {
        when(destination.getIataCode()).thenReturn(null);
        when(dealsConfiguration.getBaseDestinations()).thenReturn(Collections.singletonList(ID_ARR));
        when(cityBookingAggregation.getCityGeonodeId()).thenReturn(ID_ARR);
        when(popularityStatsService.getPopularDestinationCities(site, AGGREGATE_BY, MAX_POPULAR_DESTINATIONS))
                .thenReturn(Collections.singletonList(cityBookingAggregation));
        when(geoService.getCityByGeonodeId(ID_ARR)).thenReturn(destination);

        Map<String, City> result = helper.getPopularDestinationCities(departure, site);

        assertEquals(result.size(), 0);
        assertNull(result.get(IATA_ARR));
        verify(popularityStatsService).getPopularDestinationCities(any(Site.class), any(String.class), any(Integer.class));
        verify(geoService).getCityByGeonodeId(any(Integer.class));
    }

    @Test
    public void testGetPopularDestinationCitiesFromPopularDestinationsOnly() throws GeoNodeNotFoundException {
        when(dealsConfiguration.getBaseDestinations()).thenReturn(Collections.emptyList());
        when(cityBookingAggregation.getCityGeonodeId()).thenReturn(ID_ARR);
        when(popularityStatsService.getPopularDestinationCities(site, AGGREGATE_BY, MAX_POPULAR_DESTINATIONS))
            .thenReturn(Collections.singletonList(cityBookingAggregation));
        when(geoService.getCityByGeonodeId(ID_ARR)).thenReturn(destination);

        Map<String, City> result = helper.getPopularDestinationCities(departure, site);

        assertEquals(result.size(), 1);
        assertEquals(result.get(IATA_ARR).getCity(), destination);
        verify(popularityStatsService).getPopularDestinationCities(any(Site.class), any(String.class), any(Integer.class));
        verify(geoService).getCityByGeonodeId(any(Integer.class));
    }

    @Test
    public void testGetPopularDestinationCitiesFromBaseDestinationsOnly() throws GeoNodeNotFoundException {
        when(dealsConfiguration.getBaseDestinations()).thenReturn(Collections.singletonList(ID_ARR));
        when(popularityStatsService.getPopularDestinationCities(site, AGGREGATE_BY, MAX_POPULAR_DESTINATIONS)).thenReturn(Collections.emptyList());
        when(geoService.getCityByGeonodeId(ID_ARR)).thenReturn(destination);

        Map<String, City> result = helper.getPopularDestinationCities(departure, site);

        assertEquals(result.size(), 1);
        assertEquals(result.get(IATA_ARR).getCity(), destination);
        verify(popularityStatsService).getPopularDestinationCities(any(Site.class), any(String.class), any(Integer.class));
        verify(geoService).getCityByGeonodeId(any(Integer.class));
    }

    @Test
    public void testGetRoutesIataPairs() {
        Set<IataPair> iataPairs = helper.getRoutesIataPairs(departure, Collections.singletonList(destination));
        IataPair iataPair = iataPairs.stream().findFirst().get();

        assertEquals(iataPairs.size(), 1);
        assertEquals(iataPair.getDepartureIata(), IATA_DEP);
        assertEquals(iataPair.getArrivalIata(), IATA_ARR);
    }

    @Test
    public void testGetRoutesIataPairsEmpty() {
        Set<IataPair> iataPairs = helper.getRoutesIataPairs(departure, Collections.emptyList());

        assertEquals(iataPairs.size(), 0);
    }

    @Test
    public void testGetCity() throws GeoNodeNotFoundException {
        when(geoService.getCityByGeonodeId(ID_DEP)).thenReturn(departure);

        assertEquals(helper.getCity(ID_DEP), departure);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetCityNotFound() throws GeoNodeNotFoundException {
        when(geoService.getCityByGeonodeId(ID_DEP)).thenThrow(new GeoNodeNotFoundException(ErrorCodes.INTERNAL_GENERIC.getDescription()));

        assertEquals(helper.getCity(ID_DEP), departure);
    }

    @Test
    public void testHasValidIata() {
        when(departure.getIataCode()).thenReturn(IATA_DEP);

        assertTrue(helper.hasValidIata(departure));
    }

    @Test
    public void testHasValidIataNull() {
        when(departure.getIataCode()).thenReturn(null);

        assertFalse(helper.hasValidIata(departure));
    }

    @Test
    public void testHasValidIataEmpty() {
        when(departure.getIataCode()).thenReturn("");

        assertFalse(helper.hasValidIata(departure));
    }

}
