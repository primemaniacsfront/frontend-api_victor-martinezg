package com.odigeo.frontend.resolvers.user.handlers;

import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentToken;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserCreditCardsResponse;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.user.mappers.CreditCardsMapper;
import com.odigeo.frontend.resolvers.user.mappers.CreditCardsMapperImpl;
import com.odigeo.frontend.services.PaymentInstrumentService;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.CreditCard;
import com.odigeo.userprofiles.api.v2.model.CreditCardType;
import com.odigeo.userprofiles.api.v2.model.User;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class UserCreditCardsHandlerTest {

    private static final String CREDIT_CARD_NUMBER = "5055203244802134";
    private static final String HIDDEN_CREDIT_CARD_NUMBER = "****2134";
    private static final Long ID = 124135L;
    private static final String EXPIRATION_DATE_MONTH = "10";
    private static final String EXPIRATION_DATE_YEAR = "2028";
    private static final String OWNER = "owner";
    @InjectMocks
    private UserCreditCardsHandler handler;
    @Mock
    private UserDescriptionService userService;
    @Mock
    private ResolverContext context;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private User user;
    @Mock
    private CreditCardsMapper mapper;
    @Mock
    private PaymentInstrumentService paymentInstrumentService;
    @Mock
    private PaymentInstrumentToken paymentInstrumentToken;
    private CreditCard creditCard;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mapper = new CreditCardsMapperImpl();
        Guice.createInjector(binder -> {
            binder.bind(CreditCardsMapper.class).toProvider(() -> mapper);
            binder.bind(UserDescriptionService.class).toProvider(() -> userService);
        }).injectMembers(handler);
        Guice.createInjector(binder -> {
            binder.bind(PaymentInstrumentService.class).toProvider(() -> paymentInstrumentService);
        }).injectMembers(mapper);
        when(paymentInstrumentToken.getUuid()).thenReturn(new UUID(0L, 1L));
        when(paymentInstrumentService.tokenize(anyString(), anyString(), anyString(), anyString())).thenReturn(paymentInstrumentToken);
        when(userService.getUserByToken(context)).thenReturn(user);
        when(env.getContext()).thenReturn(context);
        when(env.getGraphQlContext()).thenReturn(GraphQLContext.newContext().build());
        creditCard = new CreditCard();
        creditCard.setCreditCardNumber(CREDIT_CARD_NUMBER);
        creditCard.setCreditCardTypeId(CreditCardType.VISA_CREDIT);
        creditCard.setCreationDate(new Date());
        creditCard.setId(ID);
        creditCard.setIsDefault(true);
        creditCard.setExpirationDateMonth(EXPIRATION_DATE_MONTH);
        creditCard.setExpirationDateYear(EXPIRATION_DATE_YEAR);
        creditCard.setLastUsageDate(new Date());
        creditCard.setModifiedDate(new Date());
        creditCard.setOwner(OWNER);
        when(user.getCreditCards()).thenReturn(Collections.singletonList(creditCard));
    }

    @Test
    public void testGetCreditCards() {
        UserCreditCardsResponse userCreditCardsResponse = handler.getCreditCards(env);
        userCreditCardsResponse.getCreditCards().forEach(creditCards -> {
            assertEquals(creditCards.getOwner(), creditCard.getOwner());
            assertEquals(creditCards.getCreditCardTypeId().name(), creditCard.getCreditCardTypeId().name());
            assertEquals(creditCards.getCreditCardNumber(), HIDDEN_CREDIT_CARD_NUMBER);
            assertEquals(creditCards.getModifiedDate(), dateFormat.format(creditCard.getModifiedDate()));
            assertEquals(creditCards.getLastUsageDate(), dateFormat.format(creditCard.getLastUsageDate()));
            assertEquals(creditCards.getId(), creditCard.getId().longValue());
            assertEquals(creditCards.getIsDefault(), creditCard.getIsDefault());
            assertEquals(creditCards.getExpirationDateYear(), creditCard.getExpirationDateYear());
            assertEquals(creditCards.getExpirationDateMonth(), creditCard.getExpirationDateMonth());
            assertEquals(creditCards.getCreationDate(), dateFormat.format(creditCard.getCreationDate()));
        });
    }
}
