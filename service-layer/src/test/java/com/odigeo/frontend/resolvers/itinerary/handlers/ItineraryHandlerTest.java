package com.odigeo.frontend.resolvers.itinerary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Perks;
import com.google.inject.Guice;
import com.google.inject.name.Names;
import com.odigeo.dapi.client.Itinerary;
import com.odigeo.dapi.client.ItineraryShoppingItem;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.itinerary.mappers.ItineraryMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import com.odigeo.frontend.resolvers.shoppingcart.perks.handlers.PerksHandler;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ItineraryHandlerTest {

    @Mock
    private ResolverContext resolverContext;
    @Mock
    private DataFetchingEnvironment environment;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private ItineraryShoppingItem itineraryShoppingItem;
    @Mock
    private Itinerary itinerary;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private Site site;
    @Mock
    private PerksHandler<Perks> itineraryPerksHandler;
    @Mock
    private Perks perks;

    @Mock
    private ItineraryMapper mapper;
    @InjectMocks
    private ItineraryHandler handler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(PerksHandler.class)
                    .annotatedWith(Names.named("ItineraryPerksHandler"))
                    .toProvider(() -> itineraryPerksHandler);
        });

        when(environment.getContext()).thenReturn(resolverContext);
        when(environment.getGraphQlContext()).thenReturn(graphQLContext);
        when(itineraryPerksHandler.map(graphQLContext)).thenReturn(perks);
    }

    @Test
    public void testMapNull() {
        handler.map(environment);
        verify(mapper).itineraryContractToModel(isNull(), any(ShoppingCartContext.class));
    }

    @Test
    public void testMapItineraryNull() {
        when(graphQLContext.get(eq(ShoppingCartSummaryResponse.class))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        handler.map(environment);
        verify(mapper).itineraryContractToModel(isNull(), any(ShoppingCartContext.class));
        verify(shoppingCart).getItineraryShoppingItem();
    }


    @Test
    public void testMapWithVisit() {
        when(graphQLContext.get(VisitInformation.class)).thenReturn(visitInformation);
        when(visitInformation.getSite()).thenReturn(site);
        handler.map(environment);
        verify(graphQLContext).get(eq(VisitInformation.class));
    }


    @Test
    public void testMap() {
        when(graphQLContext.get(eq(ShoppingCartSummaryResponse.class))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getItineraryShoppingItem()).thenReturn(itineraryShoppingItem);
        when(itineraryShoppingItem.getItinerary()).thenReturn(itinerary);
        handler.map(environment);
        verify(mapper).itineraryContractToModel(eq(itinerary), any(ShoppingCartContext.class));
    }

    @Test
    public void testMapWithPerks() {
        when(graphQLContext.get(eq(ShoppingCartSummaryResponse.class))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getItineraryShoppingItem()).thenReturn(itineraryShoppingItem);
        when(itineraryShoppingItem.getItinerary()).thenReturn(itinerary);
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary mappedIti = mock(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary.class);
        when(mapper.itineraryContractToModel(eq(itinerary), any(ShoppingCartContext.class))).thenReturn(mappedIti);
        handler.map(environment);
        verify(itineraryPerksHandler).map(eq(graphQLContext));
    }
}
