package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class PaymentMethodManagerTest {

    private static final String SUPPORTED_PAYMENT_METHOD = PaymentMethod.CREDITCARD.name();

    @Test
    public void testIsSupported() {
        PaymentMethodManager pmManager = new PaymentMethodManager();
        assertTrue(pmManager.isSupported(SUPPORTED_PAYMENT_METHOD));
    }

}
