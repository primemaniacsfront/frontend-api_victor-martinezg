package com.odigeo.frontend.resolvers.checkin;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinAvailability;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateCheckinResponse;
import com.odigeo.checkin.api.v1.AutomaticCheckInApiService;
import com.odigeo.checkin.api.v1.CheckinStatusApiService;
import com.odigeo.checkin.api.v1.exceptions.AutomaticCheckInException;
import com.odigeo.checkin.api.v1.model.CheckinNotAvailableException;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.resolvers.checkin.mappers.CheckinAvailabilityMapper;
import com.odigeo.frontend.resolvers.checkin.mappers.CheckinStatusMapper;
import com.odigeo.frontend.resolvers.checkin.mappers.CreateCheckinRequestMapper;
import com.odigeo.frontend.resolvers.checkin.models.CheckinQuery;
import org.apache.commons.lang.LocaleUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;


public class CheckinHandlerTest {

    private static final long BOOKINGID = 1234567890L;
    private static final String CITIZENSHIP = "ES";

    @Mock
    private AutomaticCheckInApiService automaticCheckInApiService;

    @Mock
    private CheckinStatusApiService checkinStatusApiService;

    @Mock
    private CheckinStatusMapper checkinStatusMapper;

    @Mock
    private CheckinAvailabilityMapper checkinAvailabilityMapper;

    @Mock
    private CreateCheckinRequestMapper createCheckinRequestMapper;

    @Mock
    private CheckinStatus checkinStatus;

    @Mock
    private CheckinAvailability checkinAvailability;

    @Mock
    private CreateCheckinResponse createCheckinResponse;

    @Mock
    private CheckinRequest checkinRequest;

    @Mock
    private VisitInformation visitInformation;

    @InjectMocks
    private CheckinHandler checkinHandler;

    private CheckinQuery checkinQuery;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        checkinQuery = new CheckinQuery(BOOKINGID, CITIZENSHIP);
    }

    @Test
    public void testGetCheckinStatus() throws AutomaticCheckInException {
        when(checkinStatusMapper.map(any())).thenReturn(checkinStatus);
        assertEquals(checkinHandler.getCheckinStatus(checkinQuery, visitInformation), checkinStatus);
    }

    @Test
    public void testGetCheckinStatusWithFormatMobile() throws AutomaticCheckInException, CheckinNotAvailableException {
        final String locale = "es_ES";
        when(visitInformation.getDevice()).thenReturn(Device.MOBILE);
        when(visitInformation.getLocale()).thenReturn(locale);
        checkinHandler.getCheckinStatus(checkinQuery, visitInformation);

        verify(checkinStatusApiService, times(1)).getCheckinStatus(BOOKINGID, LocaleUtils.toLocale(locale), "mobile");
    }

    @Test
    public void testGetCheckinStatusNotFoundReturnsEmptyList() throws CheckinNotAvailableException, AutomaticCheckInException {
        when(checkinStatusApiService.getCheckinStatus(any(), any(), any())).thenThrow(new CheckinNotAvailableException());
        CheckinStatus status = checkinHandler.getCheckinStatus(checkinQuery, visitInformation);

        assertEquals(status.getSections().size(), 0);
    }

    @Test
    public void testGetCheckinAvailability() throws AutomaticCheckInException {
        when(checkinAvailabilityMapper.map(any())).thenReturn(checkinAvailability);
        assertEquals(checkinHandler.getCheckinAvailability(checkinQuery), checkinAvailability);
    }

    @Test
    public void testCreateCheckin() {
        CreateCheckinResponse checkinResponse = checkinHandler.createCheckin(checkinRequest, visitInformation);
        assertTrue(checkinResponse.getSuccess());
    }

    @Test
    public void testCreateCheckinThrowsException() throws AutomaticCheckInException {
        doThrow(new AutomaticCheckInException("")).when(automaticCheckInApiService).createAutomaticCheckIn(any());
        CreateCheckinResponse checkinResponse = checkinHandler.createCheckin(checkinRequest, visitInformation);
        assertFalse(checkinResponse.getSuccess());
    }
}
