package com.odigeo.frontend.resolvers.accommodation.rooms.handlers;


import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.PriceBreakdownHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.model.PriceBreakdownDTO;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.RoomDetailsMapper;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.AccommodationRoomsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDealDTO;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDetailsDTO;
import com.odigeo.searchengine.v2.responses.accommodation.BoardType;
import com.odigeo.searchengine.v2.responses.accommodation.RoomDeal;
import com.odigeo.searchengine.v2.responses.accommodation.RoomGroupDeal;
import com.odigeo.searchengine.v2.responses.accommodation.RoomGroupDealInformation;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AccommodationRoomsResponseHandlerTest {

    private static final String ROOM_DEAL_KEY = "RK";
    public static final String CURRENCY = "EUR";

    @Mock
    private RoomDetailsMapper roomDetailsMapper;
    @Mock
    private RoomDetailsDTO roomDetailsDTO;
    @Mock
    private SearchRoomResponse searchRoomResponse;
    @Mock
    private RoomDeal roomDeal;
    @Mock
    private VisitInformation visit;
    @Mock
    private PriceBreakdownHandler priceBreakdownHandler;

    @InjectMocks
    private AccommodationRoomsResponseHandler testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(roomDetailsMapper.map(any(RoomDeal.class))).thenReturn(roomDetailsDTO);
        when(roomDeal.getKey()).thenReturn(ROOM_DEAL_KEY);
        when(roomDeal.getBoardType()).thenReturn(BoardType.RO);
        when(visit.getCurrency()).thenReturn(CURRENCY);
    }

    @Test
    public void testMapResponse() {
        when(roomDeal.getRoomId()).thenReturn("123");
        when(roomDeal.getProviderId()).thenReturn("BC");
        when(roomDeal.getProviderHotelId()).thenReturn("789");
        when(searchRoomResponse.getSearchId()).thenReturn(123L);
        when(searchRoomResponse.getRoomDealLegend()).thenReturn(Collections.singletonList(roomDeal));
        AccommodationRoomsResponseDTO accommodationRoomsResponseDTO = testClass.buildAccommodationRoomResponse(searchRoomResponse, visit);
        assertEquals(accommodationRoomsResponseDTO.getSearchId(), 123L);
        RoomDealDTO roomDealDTO = accommodationRoomsResponseDTO.getRooms().get(0);
        assertEquals(roomDealDTO.getProviderHotelId(), "789");
        assertEquals(roomDealDTO.getProviderRoomId(), "123");
        assertEquals(roomDealDTO.getProviderId(), "BC");
        assertEquals(roomDealDTO.getKey(), ROOM_DEAL_KEY);
        assertEquals(roomDealDTO.getDetails(), roomDetailsDTO);
        assertEquals(accommodationRoomsResponseDTO.getCurrency(), CURRENCY);
    }

    @Test
    public void testMapResponseRoomGroupDeal() {
        RoomGroupDealInformation roomInformation = mock(RoomGroupDealInformation.class);
        PriceBreakdownDTO pbd = mock(PriceBreakdownDTO.class);
        RoomGroupDeal roomGroupDeal = mock(RoomGroupDeal.class);
        when(priceBreakdownHandler.mapPriceBreakdown(eq(roomGroupDeal), eq(CURRENCY), eq(visit))).thenReturn(pbd);
        when(searchRoomResponse.getSearchId()).thenReturn(123L);
        when(searchRoomResponse.getRoomDealLegend()).thenReturn(Collections.singletonList(roomDeal));
        when(roomGroupDeal.getRoomsKeys()).thenReturn(Collections.singletonList(ROOM_DEAL_KEY));
        when(roomInformation.getCancellationPolicy()).thenReturn("cancellation_policy");
        when(roomInformation.getProviderCurrency()).thenReturn(CURRENCY);
        when(roomGroupDeal.getRoomGroupDealInformation()).thenReturn(roomInformation);
        when(searchRoomResponse.getRoomGroupDeals()).thenReturn(Collections.singletonList(roomGroupDeal));

        AccommodationRoomsResponseDTO accommodationRoomsResponseDTO = testClass.buildAccommodationRoomResponse(searchRoomResponse, visit);
        RoomDealDTO roomDealDTO = accommodationRoomsResponseDTO.getRooms().get(0);
        assertEquals(roomDealDTO.getPriceBreakdown(), pbd);
        assertEquals(roomDealDTO.getCancellationPolicy(), "cancellation_policy");
        assertEquals(accommodationRoomsResponseDTO.getCurrency(), CURRENCY);
    }
}
