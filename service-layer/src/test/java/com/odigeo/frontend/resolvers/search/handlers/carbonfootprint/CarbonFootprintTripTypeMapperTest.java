package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.criteria.TripType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CarbonFootprintTripTypeMapperTest {

    @Mock
    private SearchRequest searchRequestDTO;

    private CarbonFootprintTripTypeMapper mapper;

    @BeforeClass
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new CarbonFootprintTripTypeMapper();
    }

    @Test
    public void testMapOneWay() {
        when(searchRequestDTO.getTripType())
                .thenReturn(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType.ONE_WAY);
        assertEquals(mapper.map(searchRequestDTO), TripType.ONE_WAY);
    }

    @Test
    public void testMapOther() {
        when(searchRequestDTO.getTripType()).thenReturn(null);
        assertEquals(mapper.map(searchRequestDTO), TripType.ROUND_TRIP);
    }

}
