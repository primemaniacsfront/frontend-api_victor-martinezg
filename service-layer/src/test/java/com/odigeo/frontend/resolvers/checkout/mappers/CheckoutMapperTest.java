package com.odigeo.frontend.resolvers.checkout.mappers;

import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScope;
import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentToken;
import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutFailure;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CollectionMethodRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MethodTypeCode;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Guice;
import com.odigeo.dapi.client.BookingProcessingSummaryResponse;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.BookingResponseStatus;
import com.odigeo.dapi.client.BookingSummary;
import com.odigeo.dapi.client.Buyer;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.util.QueryParamUtils;
import com.odigeo.frontend.resolvers.checkout.CheckoutFetcherResult;
import com.odigeo.frontend.resolvers.checkout.handlers.dapi.UserInteractionHelper;
import com.odigeo.frontend.resolvers.shoppingcart.models.ConfirmRequest;
import com.odigeo.frontend.services.PaymentInstrumentService;
import graphql.GraphQLContext;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class CheckoutMapperTest {

    private static final String BOOKING_ID = "12";
    private static final String CURRENCY = "EUR";
    private static final String TOKEN = "CREDIT_CARD-679389cb-6c73-4c78-8035-5dd839dd710c";
    private static final UUID UUID_VALUE = UUID.fromString("679389cb-6c73-4c78-8035-5dd839dd710c");
    private static final String CVV = "123";
    private static final String CC_MONTH = "02";
    private static final String CC_YEAR = "50";
    private static final String CC_HOLDER = "Name";
    private static final String CC_NUMBER = "411111111111";
    private static final String MAIL = "mail@test.com";
    private static final String ENCRYPTED_MESSAGE = "ENCRYPTED";
    private static final String ERR_003 = "INVALID_PARAMETERS";
    private static final String ERR_004 = "COLLECTION_OPTION_NOT_OFFERED";
    private static final String ERR_005 = "INVALID_EXPIRATION_DATE";
    private static final String ERR_006 = "PROFESSIONAL_INVOICING_INFO_NOT_INFORMED";
    private static final String ERR_008 = "INVALID_SESSION";
    private static final String ERR_009 = "VALIDATION_ERROR";
    private static final String ERR_018 = "ERROR_IN_PROVIDER";
    private static final String INT_001 = "INTERNAL_ERROR";
    private static final String USER_PAYMENT_INT_ID = "1";

    @Mock
    private BookingResponse bookingResponse;
    @Mock
    private CheckoutRequest checkoutRequest;
    @Mock
    private CollectionMethodRequest collectionMethodRequest;
    @Mock
    private PaymentInstrumentService paymentInstrumentService;
    @Mock
    private QueryParamUtils queryParamsUtils;
    @Mock
    private PaymentInstrumentToken paymentInstrumentToken;
    @Mock
    private CreditCardPciScope creditCardPciScope;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private BookingProcessingSummaryResponse bookingProcessingSummaryResponse;
    @Mock
    private BookingSummary bookingSummary;
    @Mock
    private Buyer buyer;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    private CheckoutFetcherResult checkoutFetcherResult;
    @Mock
    private UserInteractionHelper userInteractionHelper;

    private CheckoutMapper mapper;
    private CheckoutResponse checkoutResponse;
    private ConfirmRequest confirmRequest;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new CheckoutMapperImpl();

        Guice.createInjector(binder -> {
            binder.bind(PaymentInstrumentService.class).toProvider(() -> paymentInstrumentService);
            binder.bind(QueryParamUtils.class).toProvider(() -> queryParamsUtils);
            binder.bind(UserInteractionHelper.class).toProvider(() -> userInteractionHelper);
        }).injectMembers(mapper);
    }

    @Test
    public void testBuildShoppingCartConfirmRequest() {
        assertNull(mapper.buildShoppingCartConfirmRequest(null, null, null));

        try (MockedStatic<PaymentInstrumentToken> paymentInstrumentTokenMockedStatic = mockStatic(PaymentInstrumentToken.class)) {
            paymentInstrumentTokenMockedStatic.when(() -> PaymentInstrumentToken.valueOf(TOKEN)).thenReturn(paymentInstrumentToken);

            when(paymentInstrumentToken.getType()).thenReturn(PaymentInstrumentType.CREDIT_CARD);
            when(paymentInstrumentToken.getUuid()).thenReturn(UUID_VALUE);
            when(collectionMethodRequest.getPaymentInstrumentToken()).thenReturn(TOKEN);
            when(collectionMethodRequest.getCollectionMethodType()).thenReturn(PaymentMethod.PAYPAL);
            when(checkoutRequest.getCollectionMethod()).thenReturn(collectionMethodRequest);
            when(checkoutRequest.getShoppingId()).thenReturn(BOOKING_ID);
            when(checkoutRequest.getShoppingType()).thenReturn(ShoppingType.CART);
            confirmRequest = mapper.buildShoppingCartConfirmRequest(checkoutRequest, CURRENCY, graphQLContext);
            assertEquals(confirmRequest.getBookingId(), Long.parseLong(BOOKING_ID));

            when(paymentInstrumentService.getCreditCardFromPaymentInstrumentToken(TOKEN)).thenReturn(creditCardPciScope);
            when(collectionMethodRequest.getCollectionMethodType()).thenReturn(PaymentMethod.CREDITCARD);
            when(collectionMethodRequest.getMethodTypeCode()).thenReturn(MethodTypeCode.VI);
            configCreditCardPciScope();
            confirmRequest = mapper.buildShoppingCartConfirmRequest(checkoutRequest, CURRENCY, graphQLContext);
            assertEquals(confirmRequest.getBookingId(), Long.parseLong(BOOKING_ID));
            checkCreditCardRequest();
        }
    }

    @Test
    public void mapShoppingCartWhenConfirmedCheckoutResponse() {
        assertNull(mapper.mapShoppingCartToCheckoutResponse(null, null));

        when(bookingResponse.getStatus()).thenReturn(BookingResponseStatus.BOOKING_CONFIRMED);
        when(buyer.getMail()).thenReturn(MAIL);
        when(bookingSummary.getBuyer()).thenReturn(buyer);
        when(bookingProcessingSummaryResponse.getBookingSummary()).thenReturn(bookingSummary);
        when(bookingResponse.getBookingProcessingSummaryResponse()).thenReturn(bookingProcessingSummaryResponse);
        when(bookingResponse.getShoppingCart()).thenReturn(shoppingCartSummaryResponse);
        checkoutResponse = mapper.mapShoppingCartToCheckoutResponse(bookingResponse, USER_PAYMENT_INT_ID);
        assertEquals(checkoutResponse.getStatusInfo().getStatus(), CheckoutStatus.SUCCESS);
        assertEquals(MAIL, checkoutResponse.getShoppingCart().getBuyer().getMail());
        assertEquals(checkoutResponse.getUserPaymentInteractionId().getId(), USER_PAYMENT_INT_ID);
    }

    @Test
    public void mapShoppingCartWhenNotConfirmedResponse() {
        when(bookingResponse.getStatus()).thenReturn(BookingResponseStatus.BOOKING_ERROR);
        when(buyer.getMail()).thenReturn(MAIL);
        when(shoppingCart.getBuyer()).thenReturn(buyer);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(bookingResponse.getShoppingCart()).thenReturn(shoppingCartSummaryResponse);
        when(queryParamsUtils.generateMyTripsRedirectionToken(anyLong(), anyString())).thenReturn(ENCRYPTED_MESSAGE);

        checkoutResponse = mapper.mapShoppingCartToCheckoutResponse(bookingResponse, StringUtils.EMPTY);
        assertEquals(checkoutResponse.getStatusInfo().getStatus(), CheckoutStatus.FAILED);
        assertEquals(MAIL, checkoutResponse.getShoppingCart().getBuyer().getMail());
        assertEquals(ENCRYPTED_MESSAGE, checkoutResponse.getMyTripsRedirectionToken());
    }

    @Test
    public void mapDapiCodeErrorToCheckoutFailureTest() {
        assertEquals(mapper.mapDapiCodeErrorToCheckoutFailure(ERR_003), CheckoutFailure.INVALID_PARAMETERS);
        assertEquals(mapper.mapDapiCodeErrorToCheckoutFailure(ERR_004), CheckoutFailure.COLLECTION_OPTION_NOT_OFFERED);
        assertEquals(mapper.mapDapiCodeErrorToCheckoutFailure(ERR_005), CheckoutFailure.INVALID_EXPIRATION_DATE);
        assertEquals(mapper.mapDapiCodeErrorToCheckoutFailure(ERR_006), CheckoutFailure.PROFESSIONAL_INVOICING_INFO_NOT_INFORMED);
        assertEquals(mapper.mapDapiCodeErrorToCheckoutFailure(ERR_008), CheckoutFailure.INVALID_SESSION);
        assertEquals(mapper.mapDapiCodeErrorToCheckoutFailure(ERR_009), CheckoutFailure.VALIDATION_ERROR);
        assertEquals(mapper.mapDapiCodeErrorToCheckoutFailure(ERR_018), CheckoutFailure.ERROR_IN_PROVIDER);
        assertEquals(mapper.mapDapiCodeErrorToCheckoutFailure(INT_001), CheckoutFailure.INTERNAL_ERROR);
    }

    private void configCreditCardPciScope() {
        when(creditCardPciScope.getCvv()).thenReturn(CVV);
        when(creditCardPciScope.getExpirationMonth()).thenReturn(CC_MONTH);
        when(creditCardPciScope.getExpirationYear()).thenReturn(CC_YEAR);
        when(creditCardPciScope.getHolder()).thenReturn(CC_HOLDER);
        when(creditCardPciScope.getNumber()).thenReturn(CC_NUMBER);
        when(creditCardPciScope.getToken()).thenReturn(paymentInstrumentToken);
    }

    private void checkCreditCardRequest() {
        assertEquals(confirmRequest.getPaymentData().getBookingRequest().getCreditCardRequest().getCardNumber(), CC_NUMBER);
        assertEquals(confirmRequest.getPaymentData().getBookingRequest().getCreditCardRequest().getCardExpirationMonth(), CC_MONTH);
        assertEquals(confirmRequest.getPaymentData().getBookingRequest().getCreditCardRequest().getCardExpirationYear(), CC_YEAR);
        assertEquals(confirmRequest.getPaymentData().getBookingRequest().getCreditCardRequest().getCardOwner(), CC_HOLDER);
        assertEquals(confirmRequest.getPaymentData().getBookingRequest().getCreditCardRequest().getCardSecurityNumber(), CVV);
    }

}
