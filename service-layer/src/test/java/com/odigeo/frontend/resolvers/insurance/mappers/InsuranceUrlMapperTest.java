package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceUrl;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceUrlType;
import com.odigeo.insurance.api.last.insurance.ConditionsUrl;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class InsuranceUrlMapperTest {

    private static final InsuranceUrlType INSURANCE_URL_TYPE = InsuranceUrlType.EXTENDED;
    private static final String URL = "URL";
    private static final String URL_TYPE = "URL_TYPE";

    @Mock
    private InsuranceUrlTypeMapper insuranceUrlTypeMapper;

    @InjectMocks
    private InsuranceUrlMapper insuranceUrlMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(insuranceUrlTypeMapper.map(anyString())).thenReturn(INSURANCE_URL_TYPE);
    }

    @Test
    public void map() {
        ConditionsUrl conditionsUrl = getConditionsUrl();

        InsuranceUrl insuranceUrl = insuranceUrlMapper.map(conditionsUrl);

        assertEquals(insuranceUrl.getUrl(), URL);
        assertEquals(insuranceUrl.getType(), INSURANCE_URL_TYPE);
    }

    private ConditionsUrl getConditionsUrl() {
        ConditionsUrl conditionsUrl = new ConditionsUrl();

        conditionsUrl.setUrl(URL);
        conditionsUrl.setUrlType(URL_TYPE);

        return conditionsUrl;
    }

}
