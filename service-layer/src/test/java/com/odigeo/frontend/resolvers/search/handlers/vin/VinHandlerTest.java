package com.odigeo.frontend.resolvers.search.handlers.vin;

import com.google.common.collect.Sets;
import com.google.inject.Guice;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.frontend.contract.ancillaries.InsuranceOfferDTO;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.searchengine.v2.responses.itinerary.CrossFaringItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.HubItinerary;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VinHandlerTest {

    private static final String ORIGIN_FLIGHT = "origin";
    private static final String DESTINATION_FLIGHT = "destination";

    @Mock
    private SearchEngineContext seContext;
    @Mock
    private VinFlightNumberHandler flightNumberHandler;
    @Mock
    private VinInsuranceCreator insuranceCreator;
    @Mock
    private SectionDTO section1;
    @Mock
    private SectionDTO section2;
    @Mock
    private InsuranceOfferDTO insurance;
    @Mock
    private CrossFaringItinerary crossFaringItinerary;
    @Mock
    private HubItinerary hubItineraryOutbound;

    private List<SearchItineraryDTO> itineraries;

    private VinHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new VinHandler();

        Guice.createInjector(binder -> {
            binder.bind(VinFlightNumberHandler.class).toProvider(() -> flightNumberHandler);
            binder.bind(VinInsuranceCreator.class).toProvider(() -> insuranceCreator);
        }).injectMembers(handler);

        SearchItineraryDTO itinerary = mock(SearchItineraryDTO.class);
        LegDTO leg = mock(LegDTO.class);
        SegmentDTO segment = mock(SegmentDTO.class);
        Integer itineraryLink = 1;

        when(itinerary.getLegs()).thenReturn(Collections.singletonList(leg));
        when(itinerary.getItinerariesLink()).thenReturn(itineraryLink);
        when(leg.getSegments()).thenReturn(Collections.singletonList(segment));
        when(segment.getSections()).thenReturn(Arrays.asList(section1, section2));
        when(insuranceCreator.createInsuranceOffer(eq(itineraryLink), eq(seContext), any())).thenReturn(insurance);
        when(seContext.getHubMap())
                .thenReturn(Collections.singletonMap(itineraryLink, mock(HubItinerary.class)));

        itineraries = Collections.singletonList(itinerary);

        Set<String> originFlights = Sets.newHashSet(ORIGIN_FLIGHT);
        Set<String> destinationFlights = Sets.newHashSet(DESTINATION_FLIGHT);

        when(flightNumberHandler.findOriginHubFlightNumbers(eq(seContext))).thenReturn(originFlights);
        when(flightNumberHandler.findDestinationHubFlightNumbers(eq(seContext))).thenReturn(destinationFlights);
    }

    @Test
    public void testPopulateVinInfoIsVinOriginFlights() {
        createSections(ORIGIN_FLIGHT, DESTINATION_FLIGHT);

        handler.populateVinInfo(itineraries, seContext);

        verify(section1).setInsuranceOffer(eq(insurance));
        verify(section2, never()).setInsuranceOffer(any());
    }

    @Test
    public void testPopulateVinInfoIsVinDestinationFlights() {
        createSections(DESTINATION_FLIGHT, ORIGIN_FLIGHT);

        handler.populateVinInfo(itineraries, seContext);

        verify(section1).setInsuranceOffer(eq(insurance));
        verify(section2, never()).setInsuranceOffer(any());
    }

    @Test
    public void testPopulateVinInfoIsNotVinOrigin() {
        createSections("fake origin", DESTINATION_FLIGHT);

        handler.populateVinInfo(itineraries, seContext);

        verify(section1, never()).setInsuranceOffer(any());
        verify(section2, never()).setInsuranceOffer(any());
    }

    @Test
    public void testPopulateVinInfoNotVinDestination() {
        createSections(ORIGIN_FLIGHT, "fake destination");

        handler.populateVinInfo(itineraries, seContext);

        verify(section1, never()).setInsuranceOffer(any());
        verify(section2, never()).setInsuranceOffer(any());
    }

    @Test
    public void testPopulateVinInfoNotItineraryHub() {
        when(seContext.getHubMap()).thenReturn(Collections.emptyMap());

        handler.populateVinInfo(itineraries, seContext);

        verify(section1, never()).setInsuranceOffer(any());
        verify(section2, never()).setInsuranceOffer(any());
    }

    @Test
    public void testPopulateVinInfoWhenCrossfaringItineraryWithHubs() {
        Integer itineraryLink = 1;
        Integer crossfaringItineraryOutboundId = 2;
        Integer crossfaringItineraryInboundId = 3;

        SearchItineraryDTO itinerary = mock(SearchItineraryDTO.class);
        LegDTO leg = mock(LegDTO.class);
        SegmentDTO segmentOutbound = mock(SegmentDTO.class);
        SegmentDTO segmentInbound = mock(SegmentDTO.class);

        when(itinerary.getLegs()).thenReturn(Collections.singletonList(leg));
        when(itinerary.getItinerariesLink()).thenReturn(itineraryLink);
        when(leg.getSegments()).thenReturn(Stream.of(segmentOutbound, segmentInbound)
                .collect(Collectors.toList()));

        when(segmentInbound.getSections()).thenReturn(Arrays.asList(section1, section2));

        when(seContext.getCrossFaringMap())
                .thenReturn(Collections.singletonMap(itineraryLink, crossFaringItinerary));
        when(crossFaringItinerary.getId()).thenReturn(itineraryLink);
        when(crossFaringItinerary.getOut()).thenReturn(crossfaringItineraryOutboundId);
        when(crossFaringItinerary.getIn()).thenReturn(crossfaringItineraryInboundId);
        when(seContext.getHubMap())
                .thenReturn(Collections.singletonMap(crossfaringItineraryInboundId, mock(HubItinerary.class)));
        when(insuranceCreator.createInsuranceOffer(eq(crossfaringItineraryInboundId), eq(seContext), any())).thenReturn(insurance);
        createSections(ORIGIN_FLIGHT, DESTINATION_FLIGHT);

        itineraries = Collections.singletonList(itinerary);

        handler.populateVinInfo(itineraries, seContext);

        verify(section1).setInsuranceOffer(eq(insurance));
        verify(section2, never()).setInsuranceOffer(any());
    }

    private void createSections(String sectionKey1, String sectionKey2) {
        when(section1.getKey()).thenReturn(sectionKey1);
        when(section2.getKey()).thenReturn(sectionKey2);
    }

}
