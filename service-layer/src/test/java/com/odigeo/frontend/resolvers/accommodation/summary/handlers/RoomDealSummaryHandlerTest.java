package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSummaryRequest;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.mappers.RoomDealSummaryMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.RoomDealSummaryDTO;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.responses.accommodation.RoomDeal;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class RoomDealSummaryHandlerTest {

    private static final long SEARCH_ID = 1L;
    private static final int DEDUP_ID = 4;
    private static final String ACCOMMODATION_DEAL_KEY = "2";
    private static final String VISIT_CODE = "visit";
    @Mock
    private SearchService searchService;
    @Mock
    private VisitInformation visit;
    @Mock
    private RoomDealSummaryMapper roomDealSummaryMapper;
    @InjectMocks
    private RoomDealSummaryHandler handler;
    @Mock
    private AccommodationSummaryRequest request;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(visit.getVisitCode()).thenReturn(VISIT_CODE);
        when(request.getSearchId()).thenReturn(SEARCH_ID);
        when(request.getDedupId()).thenReturn(DEDUP_ID);
        when(request.getAccommodationDealKey()).thenReturn(ACCOMMODATION_DEAL_KEY);
    }

    @Test
    public void testGetAccommodationDetails() {
        SearchRoomResponse searchRoomResponse = new SearchRoomResponse();
        RoomDeal rd1 = new RoomDeal();
        rd1.setKey("1");
        RoomDeal rd2 = new RoomDeal();
        rd2.setKey("2");
        RoomDeal rd3 = new RoomDeal();
        rd3.setKey("3");
        searchRoomResponse.getRoomDealLegend().addAll(Arrays.asList(rd1, rd2, rd1, rd3));

        List<String> roomKeys = Arrays.asList("1", "2");
        when(request.getRoomKeys()).thenReturn(roomKeys);
        RoomDealSummaryDTO rd1DTO = new RoomDealSummaryDTO();
        rd1DTO.setRoomKey("1");
        RoomDealSummaryDTO rd2DTO = new RoomDealSummaryDTO();
        rd2DTO.setRoomKey("2");

        AccommodationDetailDTO accommodationDetail = new AccommodationDetailDTO();
        accommodationDetail.setAccommodationDealInformation(new AccommodationDealInformationDTO());
        accommodationDetail.getAccommodationDealInformation().setPaymentAtDestination(true);

        when(searchService.getRoomsAvailability(SEARCH_ID, Long.valueOf(ACCOMMODATION_DEAL_KEY), VISIT_CODE)).thenReturn(searchRoomResponse);
        when(roomDealSummaryMapper.map(rd1, true)).thenReturn(rd1DTO);
        when(roomDealSummaryMapper.map(rd2, true)).thenReturn(rd2DTO);

        List<RoomDealSummaryDTO> results = handler.getRoomsDealsByRoomKey(request, accommodationDetail, visit);

        assertEquals(results.size(), 2);
        assertEquals(results.get(0).getRoomKey(), rd1.getKey());
        assertEquals(results.get(1).getRoomKey(), rd2.getKey());
    }

    @Test
    public void testGetManyAccommodationDetailsAsRoomsKeys() {
        SearchRoomResponse searchRoomResponse = new SearchRoomResponse();
        RoomDeal rd1 = new RoomDeal();
        rd1.setKey("1");
        RoomDeal rd2 = new RoomDeal();
        rd2.setKey("2");
        RoomDeal rd3 = new RoomDeal();
        rd3.setKey("3");
        searchRoomResponse.getRoomDealLegend().addAll(Arrays.asList(rd1, rd2, rd3));

        List<String> roomKeys = Arrays.asList("1", "2", "1");
        when(request.getRoomKeys()).thenReturn(roomKeys);
        RoomDealSummaryDTO rd1DTO = new RoomDealSummaryDTO();
        rd1DTO.setRoomKey("1");
        RoomDealSummaryDTO rd2DTO = new RoomDealSummaryDTO();
        rd2DTO.setRoomKey("2");

        AccommodationDetailDTO accommodationDetail = new AccommodationDetailDTO();
        accommodationDetail.setAccommodationDealInformation(new AccommodationDealInformationDTO());
        accommodationDetail.getAccommodationDealInformation().setPaymentAtDestination(true);

        when(searchService.getRoomsAvailability(SEARCH_ID, Long.valueOf(ACCOMMODATION_DEAL_KEY), VISIT_CODE)).thenReturn(searchRoomResponse);
        when(roomDealSummaryMapper.map(rd1, true)).thenReturn(rd1DTO);
        when(roomDealSummaryMapper.map(rd2, true)).thenReturn(rd2DTO);

        List<RoomDealSummaryDTO> results = handler.getRoomsDealsByRoomKey(request, accommodationDetail, visit);

        assertEquals(results.size(), 3);
        assertEquals(results.get(0).getRoomKey(), rd1.getKey());
        assertEquals(results.get(1).getRoomKey(), rd2.getKey());
        assertEquals(results.get(2).getRoomKey(), rd1.getKey());
    }

    @Test
    public void testGetAccommodationDetailsNullValues() {
        SearchRoomResponse searchRoomResponse = new SearchRoomResponse();
        List<String> roomKeys = Arrays.asList("1", "2");
        when(request.getRoomKeys()).thenReturn(roomKeys);
        when(searchService.getRoomsAvailability(SEARCH_ID, Long.valueOf(ACCOMMODATION_DEAL_KEY), VISIT_CODE)).thenReturn(searchRoomResponse);
        List<RoomDealSummaryDTO> results = handler.getRoomsDealsByRoomKey(request, null, visit);
        assertTrue(results.isEmpty());
    }

}
