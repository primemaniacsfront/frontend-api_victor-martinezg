package com.odigeo.frontend.resolvers.insurance.mappers;

import com.odigeo.insurance.api.last.request.PostBookingOfferRequest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class PostBookingOfferRequestMapperTest {

    private static final String VISIT_INFORMATION_SERIALIZED = "VISIT_INFORMATION_SERIALIZED";
    private static final long BOOKING_ID = 1L;

    @Test
    public void map() {
        PostBookingOfferRequestMapper postBookingOfferRequestMapper = new PostBookingOfferRequestMapper();

        PostBookingOfferRequest postBookingOfferRequest = postBookingOfferRequestMapper.map(BOOKING_ID, VISIT_INFORMATION_SERIALIZED);

        assertEquals(postBookingOfferRequest.getBookingId(), String.valueOf(BOOKING_ID));
        assertEquals(postBookingOfferRequest.getVisitInformation(), VISIT_INFORMATION_SERIALIZED);
        assertNull(postBookingOfferRequest.getShoppingBasketId());
    }

}
