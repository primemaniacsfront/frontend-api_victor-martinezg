package com.odigeo.frontend.resolvers.prime.models;

import bean.test.BeanTest;

import java.util.HashSet;

public class PrimeMarketTest extends BeanTest<PrimeMarket> {
    @Override
    protected PrimeMarket getBean() {
        PrimeMarket primeMarket = new PrimeMarket();
        primeMarket.setExcludedMktPortals(new HashSet<>());
        primeMarket.setStatus(PrimeMarketStatus.ROLLOUT);
        return primeMarket;
    }
}
