package com.odigeo.frontend.resolvers.search.handlers.alert;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Site;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class PointOfSaleMapperTest {

    private static final String POS_ED = "edreams.es";
    private static final String POS_GV = "govoyages.fr";
    private static final String POS_OP = "opodo.de";
    private static final String POS_TL = "travellink.no";

    @Mock
    private VisitInformation visit;

    private PointOfSaleMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new PointOfSaleMapper();
    }

    @Test
    public void testMap() {
        Stream.of(
            new ImmutableTriple<>(Site.ES, Brand.ED, POS_ED),
            new ImmutableTriple<>(Site.GOFR, Brand.GV, POS_GV),
            new ImmutableTriple<>(Site.OPDE, Brand.OP, POS_OP),
            new ImmutableTriple<>(Site.TRNO, Brand.TL, POS_TL),
            new ImmutableTriple<>(Site.UNKNOWN, Brand.ED, StringUtils.EMPTY),
            new ImmutableTriple<>(Site.ES, Brand.UNKNOWN, StringUtils.EMPTY)
        ).forEach(triple -> {
            when(visit.getSite()).thenReturn(triple.left);
            when(visit.getBrand()).thenReturn(triple.middle);
            assertEquals(mapper.map(visit), triple.right);
        });
    }

}
