package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.DeviceType;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class MirDeviceTypeMapperTest {

    @Mock
    private VisitInformation visit;

    private MirDeviceTypeMapper mapper;

    @BeforeClass
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new MirDeviceTypeMapper();
    }

    @Test
    public void testMap() {
        Stream.of(
            new ImmutablePair<>(Device.DESKTOP, DeviceType.COMPUTER),
            new ImmutablePair<>(Device.MOBILE, DeviceType.SMARTPHONE),
            new ImmutablePair<>(Device.TABLET, DeviceType.TABLET),
            new ImmutablePair<>(Device.OTHER, DeviceType.OTHER)
        ).forEach(pair -> {
            when(visit.getDevice()).thenReturn(pair.left);
            assertSame(mapper.map(visit), pair.right);
        });
    }

}
