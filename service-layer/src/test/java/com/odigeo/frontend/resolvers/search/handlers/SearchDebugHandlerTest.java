package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CarbonFootprint;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.search.handlers.carbonfootprint.CarbonFootprintDebugHandler;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerarySearchResults;
import org.apache.commons.collections4.MapUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class SearchDebugHandlerTest {

    private static final String API_CAPTURE_LABEL = "apiCapture";
    private static final String API_CAPTURE_SEARCH_REQUEST_LABEL = "search-engine-request";
    private static final String API_CAPTURE_SEARCH_RESPONSE_LABEL = "search-engine-response";

    private static final String QAMODE_LABEL = "qamode";
    private static final String QAMODE_SEARCH_ENGINE_LABEL = "search-engine";
    private static final String QAMODE_ME_RATING_LABEL = "me-rating";
    private static final String QAMODE_FOOTPRINT_CALCULATOR = "footprint-calculator";

    @Mock
    private ResolverContext context;
    @Mock
    private DebugInfo debugInfo;
    @Mock
    private SearchMethodRequest request;
    @Mock
    private SearchResponse response;
    @Mock
    private CarbonFootprintDebugHandler carbonFootprintDebugHandler;

    private SearchDebugHandler handler;

    private Map<String, Object> debugInfoMap;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new SearchDebugHandler();
        Guice.createInjector(handler -> {
            handler.bind(CarbonFootprintDebugHandler.class).toProvider(() -> carbonFootprintDebugHandler);
        }).injectMembers(handler);

        debugInfoMap = new HashMap<>();

        when(context.getDebugInfo()).thenReturn(debugInfo);
        when(debugInfo.getDebugInfoMap()).thenReturn(debugInfoMap);

    }

    @Test
    public void testAddApiCaptureInfo() {
        when(debugInfo.isApiCaptureEnabled()).thenReturn(true);

        handler.addApiCaptureInfo(context, request, response);

        Map<?, ?> dapiCaptureMap = MapUtils.getMap(debugInfoMap, API_CAPTURE_LABEL);
        assertSame(dapiCaptureMap.get(API_CAPTURE_SEARCH_REQUEST_LABEL), request);
        assertSame(dapiCaptureMap.get(API_CAPTURE_SEARCH_RESPONSE_LABEL), response);
    }

    @Test
    public void testAddApiCaptureInfoDisabled() {
        when(debugInfo.isEnabled()).thenReturn(false);

        handler.addApiCaptureInfo(context, request, response);
        assertNull(debugInfo.getDebugInfoMap().get(API_CAPTURE_LABEL));
    }

    @Test
    public void testAddQaModeSearchInfo() {
        ItinerarySearchResults itinerarySearchResults = mock(ItinerarySearchResults.class);
        FareItinerary fareItinerary = mock(FareItinerary.class);
        String itineraryKey = "key";
        String itineraryDebugInfo = "debug";

        when(debugInfo.isQaModeEnabled()).thenReturn(true);
        when(response.getItinerarySearchResults()).thenReturn(itinerarySearchResults);
        when(itinerarySearchResults.getItineraryResults()).thenReturn(Collections.singletonList(fareItinerary));
        when(fareItinerary.getKey()).thenReturn(itineraryKey);
        when(fareItinerary.getDebugInfo()).thenReturn(itineraryDebugInfo);

        handler.addQaModeSearchInfo(context, response);

        Map qaModeMap = MapUtils.getMap(debugInfoMap, QAMODE_LABEL);
        Map<?, ?> searchMap = MapUtils.getMap(qaModeMap, QAMODE_SEARCH_ENGINE_LABEL);

        assertEquals(searchMap.get(itineraryKey), itineraryDebugInfo);
    }

    @Test
    public void testAddQaModeSearchInfoDisabled() {
        when(debugInfo.isQaModeEnabled()).thenReturn(false);

        handler.addQaModeSearchInfo(context, response);
        assertNull(debugInfo.getDebugInfoMap().get(QAMODE_LABEL));
    }

    @Test
    public void testAddQaModeSearchInfoNoItineraries() {
        when(debugInfo.isQaModeEnabled()).thenReturn(true);
        when(response.getItinerarySearchResults()).thenReturn(null);

        handler.addQaModeSearchInfo(context, response);
        Map qaModeMap = MapUtils.getMap(debugInfoMap, QAMODE_LABEL);
        Map<?, ?> searchMap = MapUtils.getMap(qaModeMap, QAMODE_SEARCH_ENGINE_LABEL);

        assertTrue(searchMap.isEmpty());
    }

    @Test
    public void testAddQaModeMeRatingInfo() {
        SearchItineraryDTO itinerary = mock(SearchItineraryDTO.class);
        String itineraryId = "id";
        BigDecimal itineraryRating = BigDecimal.TEN;
        List<SearchItineraryDTO> itineraries = Collections.singletonList(itinerary);

        when(debugInfo.isQaModeEnabled()).thenReturn(true);
        when(itinerary.getId()).thenReturn(itineraryId);
        when(itinerary.getMeRating()).thenReturn(itineraryRating);

        handler.addQaModeMeRatingInfo(context, itineraries);
        Map qaModeMap = MapUtils.getMap(debugInfoMap, QAMODE_LABEL);
        Map<?, ?> meRatingMap = MapUtils.getMap(qaModeMap, QAMODE_ME_RATING_LABEL);

        assertEquals(meRatingMap.get(itineraryId), itineraryRating);
    }

    @Test
    public void testAddQaModeMeRatingInfoDisabled() {
        when(debugInfo.isQaModeEnabled()).thenReturn(false);

        handler.addQaModeMeRatingInfo(context, Collections.emptyList());
        assertNull(debugInfo.getDebugInfoMap().get(QAMODE_LABEL));
    }

    @Test
    public void testAddQaModeCarbonFootprintInfo() {
        SearchItineraryDTO itinerary = mock(SearchItineraryDTO.class);
        String itineraryId = "id";
        List<SearchItineraryDTO> itineraries = Collections.singletonList(itinerary);

        when(debugInfo.isQaModeEnabled()).thenReturn(true);
        when(itinerary.getId()).thenReturn(itineraryId);
        when(itinerary.getCarbonFootprint()).thenReturn(mock(CarbonFootprint.class));
        Map<String, Object> carbonFootprintInfoMap = Collections.singletonMap(itineraryId, mock(CarbonFootprint.class));
        when(carbonFootprintDebugHandler.getDebugInformation(any(), any())).thenReturn(carbonFootprintInfoMap);
        handler.addQaModeCarbonFootprintInfo(context, itineraries, BigDecimal.TEN);

        Map qaModeMap = MapUtils.getMap(debugInfoMap, QAMODE_LABEL);
        assertEquals(MapUtils.getMap(qaModeMap, QAMODE_FOOTPRINT_CALCULATOR), carbonFootprintInfoMap);
    }

    @Test
    public void testAddQaModeFootprintCalculatorInfoDisabled() {
        when(debugInfo.isQaModeEnabled()).thenReturn(false);

        handler.addQaModeCarbonFootprintInfo(context, Collections.emptyList(), BigDecimal.TEN);
        assertNull(debugInfo.getDebugInfoMap().get(QAMODE_LABEL));
    }

}
