package com.odigeo.frontend.resolvers.itinerary.utils;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Leg;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.AssertJUnit.assertTrue;

public class ItineraryUtilsTest {

    private static final String CARRIER_ID = "UX";
    @Mock
    private Itinerary itinerary;
    @Mock
    private Leg leg;
    @Mock
    private Segment segment;
    @Mock
    private Section section;
    @Mock
    private Carrier carrier;

    private ItineraryUtils itineraryUtils;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        when(carrier.getId()).thenReturn(CARRIER_ID);
        itineraryUtils = new ItineraryUtils();
    }

    @Test
    public void testGetSectionsCarrierIds() {
        when(itinerary.getLegs()).thenReturn(Collections.singletonList(leg));
        when(leg.getSegments()).thenReturn(Collections.singletonList(segment));
        when(segment.getSections()).thenReturn(Collections.singletonList(section));
        when(section.getCarrier()).thenReturn(carrier);
        assertTrue(itineraryUtils.getSectionsCarrierIds(itinerary).contains(CARRIER_ID));
    }

    @Test
    public void testGetSegmentsCarrierIds() {
        when(itinerary.getLegs()).thenReturn(Collections.singletonList(leg));
        when(leg.getSegments()).thenReturn(Collections.singletonList(segment));
        when(segment.getCarrier()).thenReturn(carrier);
        assertTrue(itineraryUtils.getSegmentsCarrierIds(itinerary).contains(CARRIER_ID));
    }

    @Test
    public void testGetSectionsCarrierIdsEmpty() {
        assertTrue(itineraryUtils.getSectionsCarrierIds(itinerary).isEmpty());
    }

    @Test
    public void testGetSegmentsCarrierIdsEmpty() {
        assertTrue(itineraryUtils.getSegmentsCarrierIds(itinerary).isEmpty());
    }
}
