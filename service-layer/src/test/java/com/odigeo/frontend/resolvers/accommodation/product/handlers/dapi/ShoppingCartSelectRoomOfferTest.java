package com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi;

import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.DebugModeConfiguration;
import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.dapi.BookingStatus;
import com.edreamsodigeo.retail.frontenddebugmodeconfiguration.dapi.DapiSelectionDebugModeConfiguration;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectRoomOfferResponse;
import com.odigeo.dapi.client.SelectionRequests;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.dapi.client.TestConfigurationSelectionRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.dapi.ShoppingCartAddUser;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.services.UserDescriptionService;
import graphql.GraphQLContext;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ShoppingCartSelectRoomOfferTest {

    @Mock
    private ShoppingCartHandler shoppingCartHandler;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private UserDescriptionService userService;
    @Mock
    private ShoppingCartAddUser shoppingCartAddUser;

    @InjectMocks
    private ShoppingCartSelectRoomOffer testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void test() {
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = getShoppingCartSummaryResponse();
        when(shoppingCartHandler.createShoppingCart(any(SelectionRequests.class), any(ResolverContext.class), any(TestConfigurationSelectionRequest.class))).thenReturn(shoppingCartSummaryResponse);
        when(graphQLContext.get(DebugModeConfiguration.class)).thenReturn(getDebugModeConfiguration());
        SelectRoomOfferResponse selectRoomOfferResponse = testClass.selectRoomOffer("1", "0", "0", resolverContext, graphQLContext);
        assertNotNull(selectRoomOfferResponse);
        assertEquals(selectRoomOfferResponse.getDealId(), "123");
    }

    private DebugModeConfiguration getDebugModeConfiguration() {
        DebugModeConfiguration debugModeConfiguration = new DebugModeConfiguration();
        DapiSelectionDebugModeConfiguration dapiSelectionDebugModeConfiguration = new DapiSelectionDebugModeConfiguration();
        dapiSelectionDebugModeConfiguration.setBookingStatus(BookingStatus.CONTRACT);
        dapiSelectionDebugModeConfiguration.setFakeProvider(true);
        debugModeConfiguration.setDapiSelectionDebug(dapiSelectionDebugModeConfiguration);
        return debugModeConfiguration;
    }

    private ShoppingCartSummaryResponse getShoppingCartSummaryResponse() {
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = new ShoppingCartSummaryResponse();
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setBookingId(123L);
        shoppingCartSummaryResponse.setShoppingCart(shoppingCart);
        return shoppingCartSummaryResponse;
    }

}
