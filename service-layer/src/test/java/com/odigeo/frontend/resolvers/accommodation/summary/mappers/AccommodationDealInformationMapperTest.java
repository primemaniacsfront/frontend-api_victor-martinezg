package com.odigeo.frontend.resolvers.accommodation.summary.mappers;

import com.odigeo.frontend.commons.mappers.GeoCoordinatesMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationCategoryMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationDealInformationMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationImagesMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationImageQualityDTO;
import com.odigeo.hcsapi.v9.beans.Hotel;
import com.odigeo.hcsapi.v9.beans.HotelImage;
import com.odigeo.hcsapi.v9.beans.HotelImageQuality;
import com.odigeo.hcsapi.v9.beans.HotelImageType;
import com.odigeo.hcsapi.v9.beans.HotelStarRating;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;
import com.odigeo.hcsapi.v9.beans.HotelType;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDealInformation;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationImage;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationImageQuality;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationType;
import com.odigeo.searchengine.v2.responses.gis.GeoCoordinates;
import com.odigeo.searchengine.v2.responses.logic.ThreeValuedLogic;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory.STARS_4;
import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImageType.HOTEL;
import static com.odigeo.searchengine.v2.responses.accommodation.AccommodationCategory.STARS_5;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class AccommodationDealInformationMapperTest {

    @Mock
    private AccommodationImagesMapper accommodationImagesMapper;
    @Mock
    private GeoCoordinatesMapper geoCoordinatesMapper;
    @Mock
    private AccommodationCategoryMapper accommodationCategoryMapper;
    @InjectMocks
    private AccommodationDealInformationMapper mapper;

    @BeforeClass
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(accommodationCategoryMapper.mapStarToCategory(any(HotelStarRating.class))).thenCallRealMethod();
    }

    @Test
    public void testMap() {
        AccommodationDealInformation accommodationDealInformation = new AccommodationDealInformation();
        accommodationDealInformation.setName("name");
        accommodationDealInformation.setAddress("address");
        accommodationDealInformation.setCityName("city name");
        accommodationDealInformation.setDescription("description");
        accommodationDealInformation.setLocation("location");
        accommodationDealInformation.setChain("chain");
        accommodationDealInformation.setType(AccommodationType.HOTEL);

        AccommodationImage image = new AccommodationImage();
        image.setUrl("image url");
        image.setThumbnailUrl("thumbnail url");
        image.setType(com.odigeo.searchengine.v2.responses.accommodation.AccommodationImageType.HOTEL);
        image.setQuality(AccommodationImageQuality.HIGH);

        accommodationDealInformation.setMainAccommodationImage(image);
        accommodationDealInformation.setCancellationPolicyDescription("cancellation policy description");

        GeoCoordinates coordinates = new GeoCoordinates();
        coordinates.setLatitude(BigDecimal.ONE);
        coordinates.setLongitude(BigDecimal.ZERO);

        accommodationDealInformation.setCoordinates(coordinates);
        accommodationDealInformation.setCategory(STARS_5);
        accommodationDealInformation.setCancellationFree(ThreeValuedLogic.TRUE);
        accommodationDealInformation.setPaymentAtDestination(true);

        AccommodationImageDTO imageDTO = new AccommodationImageDTO();
        imageDTO.setUrl("image url");
        imageDTO.setThumbnailUrl("thumbnail url");
        imageDTO.setType(HOTEL);
        imageDTO.setQuality(AccommodationImageQualityDTO.HIGH);

        GeoCoordinatesDTO geoCoordinatesDTO = new GeoCoordinatesDTO();
        geoCoordinatesDTO.setLatitude(BigDecimal.ONE);
        geoCoordinatesDTO.setLongitude(BigDecimal.ZERO);

        when(accommodationImagesMapper.map(eq(image))).thenReturn(imageDTO);
        when(geoCoordinatesMapper.map(eq(coordinates))).thenReturn(geoCoordinatesDTO);

        AccommodationDealInformationDTO dto = mapper.map(accommodationDealInformation);

        assertEquals(dto.getName(), accommodationDealInformation.getName());
        assertEquals(dto.getAddress(), accommodationDealInformation.getAddress());
        assertEquals(dto.getCityName(), accommodationDealInformation.getCityName());
        assertEquals(dto.getDescription(), accommodationDealInformation.getDescription());
        assertEquals(dto.getLocation(), accommodationDealInformation.getLocation());
        assertEquals(dto.getChain(), accommodationDealInformation.getChain());
        assertEquals(dto.getType().name(), accommodationDealInformation.getType().value());
        assertEquals(dto.getMainAccommodationImage().getUrl(), image.getUrl());
        assertEquals(dto.getMainAccommodationImage().getThumbnailUrl(), image.getThumbnailUrl());
        assertEquals(dto.getMainAccommodationImage().getType().name(), image.getType().value());
        assertEquals(dto.getMainAccommodationImage().getQuality().value(), image.getQuality().value());
        assertEquals(dto.getCancellationPolicyDescription(), accommodationDealInformation.getCancellationPolicyDescription());
        assertEquals(dto.getCoordinates().getLatitude(), coordinates.getLatitude());
        assertEquals(dto.getCoordinates().getLongitude(), coordinates.getLongitude());
        assertEquals(dto.getCategory().name(), accommodationDealInformation.getCategory().value());
        assertEquals(dto.getCancellationFree().name(), accommodationDealInformation.getCancellationFree().value());
        assertEquals(dto.isPaymentAtDestination(), accommodationDealInformation.isPaymentAtDestination());

    }

    @Test
    public void testMapNullValues() {
        AccommodationDealInformation accommodationDealInformation = new AccommodationDealInformation();
        AccommodationImage image = new AccommodationImage();
        accommodationDealInformation.setMainAccommodationImage(image);
        GeoCoordinates coordinates = new GeoCoordinates();
        accommodationDealInformation.setCoordinates(coordinates);
        AccommodationImageDTO imageDTO = new AccommodationImageDTO();
        GeoCoordinatesDTO geoCoordinatesDTO = new GeoCoordinatesDTO();

        when(accommodationImagesMapper.map(eq(image))).thenReturn(imageDTO);
        when(geoCoordinatesMapper.map(eq(coordinates))).thenReturn(geoCoordinatesDTO);

        AccommodationDealInformationDTO dto = mapper.map(accommodationDealInformation);

        assertEquals(dto.getName(), accommodationDealInformation.getName());
        assertEquals(dto.getAddress(), accommodationDealInformation.getAddress());
        assertEquals(dto.getCityName(), accommodationDealInformation.getCityName());
        assertEquals(dto.getDescription(), accommodationDealInformation.getDescription());
        assertEquals(dto.getLocation(), accommodationDealInformation.getLocation());
        assertEquals(dto.getChain(), accommodationDealInformation.getChain());
        assertEquals(dto.getType(), accommodationDealInformation.getType());
        assertEquals(dto.getMainAccommodationImage().getUrl(), image.getUrl());
        assertEquals(dto.getMainAccommodationImage().getThumbnailUrl(), image.getThumbnailUrl());
        assertEquals(dto.getMainAccommodationImage().getType(), image.getType());
        assertEquals(dto.getMainAccommodationImage().getQuality(), image.getQuality());
        assertEquals(dto.getCancellationPolicyDescription(), accommodationDealInformation.getCancellationPolicyDescription());
        assertEquals(dto.getCoordinates().getLatitude(), coordinates.getLatitude());
        assertEquals(dto.getCoordinates().getLongitude(), coordinates.getLongitude());
        assertEquals(dto.getCategory(), accommodationDealInformation.getCategory());
        assertEquals(dto.getCancellationFree(), accommodationDealInformation.getCancellationFree());
        assertEquals(dto.isPaymentAtDestination(), accommodationDealInformation.isPaymentAtDestination());
    }

    @Test
    public void testMapHcs() {
        HotelImage hotelImage = new HotelImage();
        hotelImage.setUrl("image url");
        hotelImage.setThumbnail("thumbnail url");
        hotelImage.setType(HotelImageType.HOTEL);
        hotelImage.setQuality(HotelImageQuality.HIGH);

        HotelSummary hotelSummary = buildHotelSummary(hotelImage);
        Hotel hotel = buildHotel(hotelSummary, Collections.singletonList(hotelImage));

        when(accommodationImagesMapper.mapHcsImage(eq(hotelImage))).thenCallRealMethod();
        when(geoCoordinatesMapper.mapHcs(eq(hotelSummary))).thenCallRealMethod();

        AccommodationDealInformationDTO dto = mapper.mapHcs(hotel);

        assertEquals(dto.getName(), hotelSummary.getName());
        assertEquals(dto.getAddress(), hotelSummary.getAddress());
        assertEquals(dto.getCityName(), hotelSummary.getCity());
        assertEquals(dto.getDescription(), hotelSummary.getHotelLongDescription());
        assertEquals(dto.getLocation(), hotelSummary.getLocation());
        assertEquals(dto.getType().name(), hotelSummary.getType().toString());
        assertEquals(dto.getMainAccommodationImage().getUrl(), hotelImage.getUrl());
        assertEquals(dto.getMainAccommodationImage().getThumbnailUrl(), hotelImage.getThumbnail());
        assertEquals(dto.getMainAccommodationImage().getType().name(), HOTEL.name());
        assertEquals(dto.getMainAccommodationImage().getQuality().value(), AccommodationImageQualityDTO.HIGH.value());
        assertEquals(dto.getCancellationPolicyDescription(), hotel.getCancellationPolicy());
        assertEquals(dto.getCoordinates().getLatitude(), new BigDecimal(hotelSummary.getLatitude()));
        assertEquals(dto.getCoordinates().getLongitude(), new BigDecimal(hotelSummary.getLongitude()));
        assertEquals(dto.getCategory().name(), STARS_4.name());
    }

    @Test
    public void testMapHcsNullValues() {
        HotelImage hotelImage = new HotelImage();
        Hotel hotel = new Hotel();

        HotelSummary hotelSummary = new HotelSummary();
        HotelSupplierKey hotelSupplierKey = mock(HotelSupplierKey.class);
        when(hotelSupplierKey.getHotelSupplierId()).thenReturn("hotelSupplierId");
        when(hotelSupplierKey.getSupplierCode()).thenReturn("supplierCode");
        hotelSummary.setHotelKey(hotelSupplierKey);

        hotel.setHotelSummary(hotelSummary);

        when(accommodationImagesMapper.mapHcsImage(eq(hotelImage))).thenCallRealMethod();
        when(geoCoordinatesMapper.mapHcs(eq(hotelSummary))).thenCallRealMethod();

        AccommodationDealInformationDTO dto = mapper.mapHcs(hotel);

        assertNull(dto.getName());
        assertNull(dto.getAddress());
        assertNull(dto.getCityName());
        assertNull(dto.getDescription());
        assertNull(dto.getLocation());
        assertNull(dto.getType());
        assertNull(dto.getMainAccommodationImage());
        assertNull(dto.getCancellationPolicyDescription());
        assertNull(dto.getCoordinates());
        assertNull(dto.getCategory());

        hotelSummary.setMainHotelImage(hotelImage);

        dto = mapper.mapHcs(hotel);
        assertNull(dto.getMainAccommodationImage().getUrl());
    }

    private HotelSummary buildHotelSummary(HotelImage hotelImage) {
        HotelSummary hotelSummary = new HotelSummary();
        HotelSupplierKey hotelSupplierKey = mock(HotelSupplierKey.class);
        when(hotelSupplierKey.getHotelSupplierId()).thenReturn("hotelSupplierId");
        when(hotelSupplierKey.getSupplierCode()).thenReturn("supplierCode");
        hotelSummary.setHotelKey(hotelSupplierKey);
        hotelSummary.setName("name");
        hotelSummary.setLongitude(1F);
        hotelSummary.setLatitude(2F);
        hotelSummary.setAddress("address");
        hotelSummary.setCity("city");
        hotelSummary.setNeighbourhood("neighbourhood");
        hotelSummary.setStarRating(HotelStarRating.H4);
        hotelSummary.setChain("chain");
        hotelSummary.setType(HotelType.APARTAMENT);
        hotelSummary.setLocation("location");
        hotelSummary.setMainHotelImage(hotelImage);
        hotelSummary.setHotelShortDescription("hotelShortDescription");
        hotelSummary.setHotelLongDescription("hotelLongDescription");
        hotelSummary.setHotelFacilities(Collections.singletonList("facility"));
        return hotelSummary;
    }

    private Hotel buildHotel(HotelSummary hotelSummary, List<HotelImage> hotelImageList) {
        Hotel hotel = new Hotel();
        hotel.setCountry("name");
        hotel.setPostalCode("address");
        hotel.setCheckIn("city name");
        hotel.setCheckOut("description");
        hotel.setNumberOfRooms("location");
        hotel.setState("chain");
        hotel.setPaymentMethod("paymentMethod");
        hotel.setHotelPolicy("hotelPolicy");
        hotel.setCancellationPolicy("cancellationPolicy");
        hotel.setSurroundingAreaInfo("surroundingAreaInfo");
        hotel.setHotelSummary(hotelSummary);
        hotel.setHotelImages(hotelImageList);
        return hotel;
    }
}
