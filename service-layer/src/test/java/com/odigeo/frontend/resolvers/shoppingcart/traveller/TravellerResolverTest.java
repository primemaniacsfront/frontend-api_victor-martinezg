package com.odigeo.frontend.resolvers.shoppingcart.traveller;

import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.shoppingcart.traveller.handlers.TravellerHandler;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.shoppingcart.traveller.TravellerResolver.REQUIRED_TRAVELLERS_INFORMATION_FIELD;
import static com.odigeo.frontend.resolvers.shoppingcart.traveller.TravellerResolver.SHOPPING_CART_TYPE;
import static com.odigeo.frontend.resolvers.shoppingcart.traveller.TravellerResolver.TRAVELLERS_FIELD;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class TravellerResolverTest {


    @Mock
    private DataFetchingEnvironment environment;
    @Mock
    private TravellerHandler handler;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext graphQLContext;

    @InjectMocks
    private TravellerResolver resolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(environment.getContext()).thenReturn(resolverContext);
        when(environment.getGraphQlContext()).thenReturn(graphQLContext);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(SHOPPING_CART_TYPE);

        assertNotNull(queries.get(REQUIRED_TRAVELLERS_INFORMATION_FIELD));
        assertNotNull(queries.get(TRAVELLERS_FIELD));
    }

    @Test
    public void testRequiredTravellerInformationDataFetcher() {
        resolver.requiredTravellerInformationDataFetcher(environment);
        verify(handler).mapTravellerInformation(eq(graphQLContext));
    }

    @Test
    public void testTravellersDataFetcher() {
        resolver.travellersDataFetcher(environment);
        verify(handler).mapTravellers(eq(graphQLContext));
    }
}
