package com.odigeo.frontend.resolvers.accommodation.rooms.mappers;

import com.odigeo.frontend.resolvers.accommodation.commons.model.ImageDTO;
import com.odigeo.hcsapi.v9.beans.RoomImage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class RoomImageMapperTest {

    private RoomImageMapper testclass = new RoomImageMapper();

    @Test
    public void testMapper() {
        ImageDTO mappedImage = testclass.map(getRoomImage());
        assertEquals(mappedImage.getUrl(), "url");
        assertEquals(mappedImage.getThumbnailUrl(), "Turl");
    }

    private RoomImage getRoomImage() {
        RoomImage image = new RoomImage();
        image.setThumbnailUrl("Turl");
        image.setUrl("url");
        return image;
    }

}
