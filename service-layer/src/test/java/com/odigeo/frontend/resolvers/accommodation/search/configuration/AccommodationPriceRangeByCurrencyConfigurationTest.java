package com.odigeo.frontend.resolvers.accommodation.search.configuration;

import bean.test.BeanTest;

import java.util.HashMap;


public class AccommodationPriceRangeByCurrencyConfigurationTest extends BeanTest<AccommodationPriceRangeByCurrencyConfiguration> {

    @Override
    protected AccommodationPriceRangeByCurrencyConfiguration getBean() {
        AccommodationPriceRangeByCurrencyConfiguration bean = new AccommodationPriceRangeByCurrencyConfiguration();
        bean.setCurrencyRange(new HashMap<>());
        return bean;
    }

}

