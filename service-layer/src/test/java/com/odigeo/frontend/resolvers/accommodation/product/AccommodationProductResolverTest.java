package com.odigeo.frontend.resolvers.accommodation.product;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationBuyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateAccommodationProductRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateAccommodationProductResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectRoomOfferRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectRoomOfferResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.accommodation.product.factories.CreateProductAccommodationFactory;
import com.odigeo.frontend.resolvers.accommodation.product.factories.SelectRoomOfferFactory;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi.ShoppingCartCreateProductAccommodation;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi.ShoppingCartSelectRoomOffer;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class AccommodationProductResolverTest {


    private static final String SELECT_ROOM_OFFER_QUERY = "selectRoomOffer";
    private static final String CREATE_ACCOMMODATION_PRODUCT_QUERY = "createAccommodationProduct";

    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private ShoppingCartSelectRoomOffer shoppingCartSelectRoomOffer;
    @Mock
    private ShoppingCartCreateProductAccommodation shoppingCartCreateProductAccommodation;

    @Mock
    private SelectRoomOfferFactory selectRoomOfferFactory;
    @Mock
    private CreateProductAccommodationFactory createProductAccommodationFactory;

    @Mock
    private DataFetchingEnvironment environment;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private VisitInformation visitInformation;

    @InjectMocks
    private AccommodationProductResolver resolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(environment.getContext()).thenReturn(resolverContext);
        when(environment.getGraphQlContext()).thenReturn(graphQLContext);
        when(graphQLContext.get(eq(VisitInformation.class))).thenReturn(visitInformation);
        when(visitInformation.getVisitCode()).thenReturn("visitCode");
        when(selectRoomOfferFactory.getInstance(any())).thenReturn(shoppingCartSelectRoomOffer);
        when(createProductAccommodationFactory.getInstance(any())).thenReturn(shoppingCartCreateProductAccommodation);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> mutations = runtimeWiring.getDataFetchers().get("Mutation");

        assertNotNull(mutations.get(SELECT_ROOM_OFFER_QUERY));
        assertNotNull(mutations.get(CREATE_ACCOMMODATION_PRODUCT_QUERY));
    }

    @Test
    public void testCreateAccommodationProduct() {
        CreateAccommodationProductRequest requestParams = mock(CreateAccommodationProductRequest.class);
        when(requestParams.getDealId()).thenReturn("123");
        when(requestParams.getGuests()).thenReturn(Collections.emptyList());
        when(requestParams.getBuyer()).thenReturn(new AccommodationBuyer());
        when(jsonUtils.fromDataFetching(environment, CreateAccommodationProductRequest.class)).thenReturn(requestParams);
        CreateAccommodationProductResponse response = new CreateAccommodationProductResponse();
        when(shoppingCartCreateProductAccommodation.createProductAccommodation(anyString(), anyList(), any(AccommodationBuyer.class), any(ResolverContext.class)))
            .thenReturn(response);
        CreateAccommodationProductResponse accommodationProductFetcher = resolver.createAccommodationProductFetcher(environment);
        assertEquals(accommodationProductFetcher, response);
    }

    @Test
    public void testCreateAccommodationProductWrongDealId() {
        CreateAccommodationProductRequest requestParams = mock(CreateAccommodationProductRequest.class);
        when(requestParams.getDealId()).thenReturn("123-123");
        when(requestParams.getGuests()).thenReturn(Collections.emptyList());
        when(requestParams.getBuyer()).thenReturn(new AccommodationBuyer());
        when(jsonUtils.fromDataFetching(environment, CreateAccommodationProductRequest.class)).thenReturn(requestParams);
        try {
            resolver.createAccommodationProductFetcher(environment);
        } catch (ServiceException e) {
            assertTrue(e.getMessage().contains("Invalid dealId"));
            verify(shoppingCartCreateProductAccommodation, never())
                .createProductAccommodation(anyString(), anyList(), any(AccommodationBuyer.class), any(ResolverContext.class));
        }
    }

    @Test
    public void testSelectRoomOffer() {
        SelectRoomOfferRequest requestParams = mock(SelectRoomOfferRequest.class);
        when(requestParams.getSearchId()).thenReturn("123");
        when(requestParams.getAccommodationDealKey()).thenReturn("0");
        when(requestParams.getRoomDealKey()).thenReturn("0");
        when(jsonUtils.fromDataFetching(environment, SelectRoomOfferRequest.class)).thenReturn(requestParams);
        SelectRoomOfferResponse response = new SelectRoomOfferResponse();
        when(shoppingCartSelectRoomOffer.selectRoomOffer(anyString(), anyString(), anyString(), any(ResolverContext.class), any(GraphQLContext.class)))
            .thenReturn(response);
        SelectRoomOfferResponse selectRoomOfferResponse = resolver.selectRoomOfferFetcher(environment);
        assertEquals(selectRoomOfferResponse, response);
    }
}
