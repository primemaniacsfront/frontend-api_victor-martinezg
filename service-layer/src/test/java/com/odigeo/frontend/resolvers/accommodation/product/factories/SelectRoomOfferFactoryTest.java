package com.odigeo.frontend.resolvers.accommodation.product.factories;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.SelectRoomOfferHandler;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi.ShoppingCartSelectRoomOffer;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.productapi.AccommodationProductSelectRoomOffer;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SelectRoomOfferFactoryTest {

    @Mock
    private ShoppingCartSelectRoomOffer shoppingCartSelectRoomOffer;
    @Mock
    private AccommodationProductSelectRoomOffer accommodationProductSelectRoomOffer;

    @InjectMocks
    private SelectRoomOfferFactory testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetInstance() {
        SelectRoomOfferHandler instanceV3 = testClass.getInstance(ShoppingType.BASKET_V3);
        assertEquals(accommodationProductSelectRoomOffer, instanceV3);

        SelectRoomOfferHandler instanceV2 = testClass.getInstance(ShoppingType.BASKET_V2);
        assertEquals(shoppingCartSelectRoomOffer, instanceV2);

        SelectRoomOfferHandler defaultInstance = testClass.getInstance(null);
        assertEquals(shoppingCartSelectRoomOffer, defaultInstance);
    }

}
