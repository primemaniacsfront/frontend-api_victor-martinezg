package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.testng.Assert.assertEquals;

public class MoneyMapperTest {

    private static final BigDecimal AMOUNT = BigDecimal.TEN;
    private static final String CURRENCY_CODE = "EUR";

    @Test
    public void mapShoppingBasketPrice() {
        MoneyMapper moneyMapper =  new MoneyMapper();

        Money money = moneyMapper.map(getInsuranceMoney());

        assertEquals(money.getCurrency(), CURRENCY_CODE);
        assertEquals(money.getAmount(), AMOUNT);
    }

    @Test
    public void mapProductPrice() {
        MoneyMapper moneyMapper =  new MoneyMapper();

        Money money = moneyMapper.map(getProductPrice());

        assertEquals(money.getCurrency(), CURRENCY_CODE);
        assertEquals(money.getAmount(), AMOUNT);
    }

    private com.odigeo.insurance.api.last.util.Money getInsuranceMoney() {
        com.odigeo.insurance.api.last.util.SingleMoney price = new com.odigeo.insurance.api.last.util.SingleMoney();

        price.setAmount(AMOUNT);
        price.setCurrency(CURRENCY_CODE);

        return price;
    }

    private com.odigeo.product.v2.model.Price getProductPrice() {
        com.odigeo.product.v2.model.Price price = new com.odigeo.product.v2.model.Price();

        price.setAmount(AMOUNT);
        price.setCurrency(Currency.getInstance(CURRENCY_CODE));

        return price;
    }

}
