package com.odigeo.frontend.resolvers.itinerary.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Leg;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class TicketsLeftMapperTest {

    private static final int SEATS = 1;

    @Mock
    private Leg leg;
    @Mock
    private Segment segment;

    private TicketsLeftMapper mapper;
    private Itinerary itinerary;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new TicketsLeftMapperImpl();
        itinerary = new Itinerary();
    }

    @Test
    public void testSetTicketsLeft() {
        mapper.setTicketsLeft(itinerary);
        assertSame(itinerary.getTicketsLeft(), 0);

        itinerary.setLegs(Collections.singletonList(leg));
        mapper.setTicketsLeft(itinerary);
        assertSame(itinerary.getTicketsLeft(), 0);

        when(leg.getSegments()).thenReturn(Collections.singletonList(segment));
        mapper.setTicketsLeft(itinerary);
        assertSame(itinerary.getTicketsLeft(), 0);

        when(segment.getSeats()).thenReturn(SEATS);
        mapper.setTicketsLeft(itinerary);
        assertSame(itinerary.getTicketsLeft(), SEATS);

        Segment otherSegment = mock(Segment.class);
        when(otherSegment.getSeats()).thenReturn(0);
        when(leg.getSegments()).thenReturn(Arrays.asList(segment, otherSegment));
        mapper.setTicketsLeft(itinerary);
        assertSame(itinerary.getTicketsLeft(), SEATS);
    }

}

