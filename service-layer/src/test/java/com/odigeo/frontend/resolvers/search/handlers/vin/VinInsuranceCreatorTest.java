package com.odigeo.frontend.resolvers.search.handlers.vin;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsurancePolicy;
import com.google.inject.Guice;
import com.odigeo.frontend.contract.ancillaries.InsuranceOfferDTO;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.frontend.resolvers.search.mappers.InsurancePolicyMapper;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.searchengine.v2.responses.insurance.Insurance;
import com.odigeo.searchengine.v2.responses.insurance.InsuranceConditionsUrlType;
import com.odigeo.searchengine.v2.responses.insurance.InsuranceOffer;
import com.odigeo.searchengine.v2.responses.insurance.InsuranceUrl;
import com.odigeo.searchengine.v2.responses.itinerary.HubItinerary;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class VinInsuranceCreatorTest {

    @Mock
    private InsurancePolicyMapper policyMapper;
    @Mock
    private VinInsuranceIdCreator idCreator;
    @Mock
    private SearchItineraryDTO itinerary;
    @Mock
    private SearchEngineContext seContext;
    @Mock
    private InsuranceOffer insuranceOffer;

    private VinInsuranceCreator creator;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        creator = new VinInsuranceCreator();

        Guice.createInjector(binder -> binder.bind(InsurancePolicyMapper.class).toProvider(() -> policyMapper))
            .injectMembers(creator);

        HubItinerary hub = mock(HubItinerary.class);
        Integer itinerariesLink = 0;

        when(seContext.getHubMap()).thenReturn(Collections.singletonMap(itinerariesLink, hub));
        when(itinerary.getItinerariesLink()).thenReturn(itinerariesLink);
        when(hub.getHubItineraryInsurances()).thenReturn(insuranceOffer);
    }

    @Test
    public void testCreateInsuranceOfferExtended() {
        String extendedUrl = "extendedUrl";
        String basicUrl = "basicUrl";

        List<InsuranceUrl> insuranceUrls = Stream.of(
            createInsuranceUrl(extendedUrl, InsuranceConditionsUrlType.EXTENDED),
            createInsuranceUrl(basicUrl, InsuranceConditionsUrlType.BASIC)
        ).collect(Collectors.toList());

        createInsuranceOfferTest(insuranceUrls, extendedUrl);
    }

    @Test
    public void testCreateInsuranceOfferBasic() {
        String basicUrl = "basicUrl";

        List<InsuranceUrl> insuranceUrls =
            Collections.singletonList(createInsuranceUrl(basicUrl, InsuranceConditionsUrlType.BASIC));

        createInsuranceOfferTest(insuranceUrls, basicUrl);
    }

    private void createInsuranceOfferTest(List<InsuranceUrl> insuranceUrls, String rightUrl) {
        InsurancePolicy policy = mock(InsurancePolicy.class);
        Insurance insurance = mock(Insurance.class);

        when(policyMapper.map(eq(insurance))).thenReturn(policy);
        when(insurance.getConditionsUrls()).thenReturn(insuranceUrls);
        when(insuranceOffer.getInsurances()).thenReturn(Collections.singletonList(insurance));
        Integer itinerariesLink = 0;
        when(itinerary.getItinerariesLink()).thenReturn(itinerariesLink);
        InsuranceOfferDTO insuranceDTO = creator.createInsuranceOffer(itinerariesLink, seContext, idCreator);

        assertEquals(insuranceDTO.getUrl(), rightUrl);
        assertEquals(insuranceDTO.getPolicy(), policy);
        verify(idCreator).createInsuranceId(eq(insuranceDTO));
    }

    private InsuranceUrl createInsuranceUrl(String url, InsuranceConditionsUrlType type) {
        InsuranceUrl insuranceUrl = mock(InsuranceUrl.class);
        when(insuranceUrl.getType()).thenReturn(type);
        when(insuranceUrl.getUrl()).thenReturn(url);

        return insuranceUrl;
    }

}
