package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatCharacteristic;
import com.odigeo.frontend.commons.Logger;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SeatCharacteristicsMapperTest {
    @Mock
    private Logger logger;

    @InjectMocks
    private SeatCharacteristicsMapper seatCharacteristicsMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void mapValidCharacteristic() {
        List<com.odigeo.itineraryapi.v1.response.SeatCharacteristic> inputSeatCharacteristicList = new ArrayList<>();
        com.odigeo.itineraryapi.v1.response.SeatCharacteristic mockSeatCharacteristic =
                new com.odigeo.itineraryapi.v1.response.SeatCharacteristic();

        mockSeatCharacteristic.setSeatCharacteristic(SeatCharacteristic.CHARGEABLE_SEAT.name());

        inputSeatCharacteristicList.add(mockSeatCharacteristic);

        List<SeatCharacteristic> seatCharacteristicList = seatCharacteristicsMapper.map(inputSeatCharacteristicList);

        assertEquals(seatCharacteristicList.get(0), SeatCharacteristic.CHARGEABLE_SEAT);
    }

    @Test
    public void mapInvalidCharacteristic() {
        List<com.odigeo.itineraryapi.v1.response.SeatCharacteristic> inputSeatCharacteristicList = new ArrayList<>();
        com.odigeo.itineraryapi.v1.response.SeatCharacteristic mockSeatCharacteristic =
                new com.odigeo.itineraryapi.v1.response.SeatCharacteristic();

        mockSeatCharacteristic.setSeatCharacteristic("FOO");

        inputSeatCharacteristicList.add(mockSeatCharacteristic);
        List<SeatCharacteristic> seatCharacteristicList = seatCharacteristicsMapper.map(inputSeatCharacteristicList);

        assertTrue(seatCharacteristicList.isEmpty());
    }
}
