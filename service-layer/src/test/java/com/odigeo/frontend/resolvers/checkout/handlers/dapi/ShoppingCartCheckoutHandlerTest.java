package com.odigeo.frontend.resolvers.checkout.handlers.dapi;

import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteractionId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InvoiceRequest;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.UserInteractionNeededResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.DocumentValidatorUtils;
import com.odigeo.frontend.resolvers.checkout.CheckoutFetcherResult;
import com.odigeo.frontend.resolvers.checkout.handlers.CheckoutResponseHandler;
import com.odigeo.frontend.resolvers.checkout.mappers.CheckoutMapper;
import com.odigeo.frontend.resolvers.checkout.models.CheckoutResumeRequest;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.resolvers.shoppingcart.models.ConfirmRequest;
import com.odigeo.frontend.services.checkout.UserPaymentInteractionService;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class ShoppingCartCheckoutHandlerTest {

    private static final String CIF_OK = "C01361948";
    private static final String CIF_KO = "C0136194A";
    private static final String SPAIN = "ES";
    private static final String USER_PAYMENT_INT_ID = "679389cb-6c73-4c78-8035-5dd839dd710c";

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private ConfirmRequest confirmRequest;
    @Mock
    private CheckoutRequest checkoutRequest;
    @Mock
    private InvoiceRequest invoiceRequest;
    @Mock
    private CheckoutResponse checkoutResponse;
    @Mock
    private CheckoutMapper checkoutMapper;
    @Mock
    private ShoppingCartHandler shoppingCartHandler;
    @Mock
    private BookingResponse bookingResponse;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private DocumentValidatorUtils documentValidatorUtils;
    @Mock
    private CheckoutResponseHandler checkoutResponseHandler;
    @Mock
    private CheckoutFetcherResult checkoutFetcherResult;
    @Mock
    private CheckoutResumeRequest resumeRequest;
    @Mock
    private UserPaymentInteractionService userPaymentInteractionService;
    @Mock
    private UserInteractionNeededResponse userInteractionNeededResponse;
    @Mock
    private UserInteractionNeededResponse.Parameters params;
    @Mock
    private UserPaymentInteractionId userPaymentInteractionId;

    @InjectMocks
    private ShoppingCartCheckoutHandler testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(checkoutMapper.buildShoppingCartConfirmRequest(any(), any(), any())).thenReturn(confirmRequest);
        when(checkoutMapper.mapShoppingCartToCheckoutResponse(bookingResponse, userPaymentInteractionId.toString())).thenReturn(checkoutResponse);
        when(checkoutMapper.mapShoppingCartToCheckoutFetcherResult(bookingResponse, userPaymentInteractionId.toString(), Collections.EMPTY_LIST)).thenReturn(checkoutFetcherResult);
        when(shoppingCartHandler.confirm(any(), any())).thenReturn(bookingResponse);
        when(checkoutResponseHandler.buildCheckoutResponse(any(), any(), any())).thenReturn(new CheckoutFetcherResult());
    }

    @Test
    public void testCheckout() {
        CheckoutFetcherResult response = testClass.checkout(checkoutRequest, context, graphQLContext);
        assertNotNull(response);
        verify(documentValidatorUtils, never()).isValidCIF(anyString(), anyString());
        verify(userPaymentInteractionService, never()).createUserPaymentInteraction(any());
    }

    @Test
    public void testCheckoutUserPaymentInteraction() {
        when(bookingResponse.getUserInteractionNeededResponse()).thenReturn(userInteractionNeededResponse);
        when(userPaymentInteractionId.getId()).thenReturn(UUID.fromString(USER_PAYMENT_INT_ID));
        when(userPaymentInteractionService.createUserPaymentInteraction(userInteractionNeededResponse)).thenReturn(userPaymentInteractionId);
        CheckoutFetcherResult response = testClass.checkout(checkoutRequest, context, graphQLContext);
        assertNotNull(response);
        verify(userPaymentInteractionService, times(1)).createUserPaymentInteraction(eq(userInteractionNeededResponse));
    }

    @Test
    public void testCheckoutWithInvoice() {
        when(checkoutRequest.getInvoice()).thenReturn(invoiceRequest);
        when(invoiceRequest.getInvoiceId()).thenReturn(CIF_OK);
        when(invoiceRequest.getInvoiceCountryCode()).thenReturn(SPAIN);
        when(documentValidatorUtils.isValidCIF(eq(CIF_OK), eq(SPAIN))).thenReturn(true);

        testClass.checkout(checkoutRequest, context, graphQLContext);
        verify(documentValidatorUtils).isValidCIF(eq(CIF_OK), eq(SPAIN));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testCheckoutWithInvoiceKO() {
        when(checkoutRequest.getInvoice()).thenReturn(invoiceRequest);
        when(invoiceRequest.getInvoiceId()).thenReturn(CIF_KO);
        when(documentValidatorUtils.isValidCIF(eq(CIF_KO), isNull())).thenReturn(false);
        testClass.checkout(checkoutRequest, context, graphQLContext);
    }

    @Test
    public void testResume() {
        when(shoppingCartHandler.resume(anyLong(), any(), any())).thenReturn(bookingResponse);
        when(bookingResponse.getUserInteractionNeededResponse()).thenReturn(userInteractionNeededResponse);
        when(userInteractionNeededResponse.getParameters()).thenReturn(params);
        when(resumeRequest.getUserPaymentInteractionId()).thenReturn(USER_PAYMENT_INT_ID);
        when(checkoutMapper.mapShoppingCartToCheckoutResponse(any(), any())).thenReturn(checkoutResponse);
        CheckoutResponse response = testClass.resume(resumeRequest, context);
        assertNotNull(response);
        verify(userPaymentInteractionService, times(1)).updateOutcomeUserPaymentInteraction(eq(USER_PAYMENT_INT_ID), eq(params));
    }

}
