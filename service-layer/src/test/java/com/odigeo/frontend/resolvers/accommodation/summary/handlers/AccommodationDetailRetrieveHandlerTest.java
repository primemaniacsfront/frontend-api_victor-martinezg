package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSummaryRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.services.DapiService;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetailResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AccommodationDetailRetrieveHandlerTest {

    private static final String ACCOMMODATION_DEAL_KEY = "2";
    private static final long SEARCH_ID = 1L;
    private static final long BOOKING_ID = 8L;
    @Mock
    private SearchService searchService;
    @Mock
    private VisitInformation visit;
    @Mock
    private DapiService dapiService;
    @Mock
    private AccommodationSummaryRequest request;
    @Mock
    private ResolverContext context;
    @InjectMocks
    private AccommodationDetailRetrieveHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(visit.getVisitCode()).thenReturn("visit");
        when(request.getAccommodationDealKey()).thenReturn(ACCOMMODATION_DEAL_KEY);
        when(request.getSearchId()).thenReturn(SEARCH_ID);
        when(request.getBookingId()).thenReturn(BOOKING_ID);
    }

    @Test
    public void testGetAccommodationDetails() {
        AccommodationDetailResponse accommodationDetailResponse = new AccommodationDetailResponse();
        when(searchService.getAccommodationDetails(SEARCH_ID, Long.valueOf(ACCOMMODATION_DEAL_KEY), "visit")).thenReturn(accommodationDetailResponse);

        AccommodationDetailResponse response = handler.getAccommodationDetails(request, visit);

        assertEquals(response, accommodationDetailResponse);
    }

    @Test
    public void testGetShoppingCart() {
        handler.getShoppingCart(BOOKING_ID, context);
        verify(dapiService).getShoppingCart(BOOKING_ID, context);
    }

}
