package com.odigeo.frontend.resolvers.ancillary;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.*;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.ancillary.handlers.AncillaryHandler;
import com.odigeo.itineraryapi.v1.request.AncillariesServicesType;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DataFetchingFieldSelectionSet;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class AncillaryResolverTest {

    private static final String GET_ANCILLARY_OPTIONS_QUERY = "ancillaryOptions";
    private static final String SELECT_ANCILLARIES_QUERY = "selectAncillaries";
    private static final String BOOKING_ID = "bookingID";
    private static final String SEATS = "seats";

    @Mock
    private AncillaryHandler ancillaryHandler;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private DataFetchingFieldSelectionSet dataFetchingFieldSelectionSet;
    @Mock
    private ResolverContext context;
    @Mock
    private DebugInfo debugInfo;
    @Mock
    private AncillaryResponse ancillaryResponse;
    @Mock
    private SelectAncillariesRequest selectAncillariesRequest;
    @Mock
    private SelectedAncillariesResponse selectedAncillariesResponse;

    @InjectMocks
    private AncillaryResolver ancillaryResolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
        when(context.getDebugInfo()).thenReturn(debugInfo);
    }

    @Test
    public void addToBuilderQueries() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        ancillaryResolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(GET_ANCILLARY_OPTIONS_QUERY));
    }

    @Test
    public void addToBuilderMutations() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        ancillaryResolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Mutation");

        assertNotNull(queries.get(SELECT_ANCILLARIES_QUERY));
    }

    @Test
    public void testAncillaryOptionsFetcher() {
        final long bookingID = 1;
        when(jsonUtils.fromDataFetching(env, Long.class, BOOKING_ID)).thenReturn(bookingID);
        when(dataFetchingFieldSelectionSet.contains(SEATS)).thenReturn(true);
        when(env.getSelectionSet()).thenReturn(dataFetchingFieldSelectionSet);
        when(ancillaryHandler
            .getAncillaries(
                bookingID,
                Collections.singleton(AncillariesServicesType.SEATS),
                context)
            )
            .thenReturn(ancillaryResponse);
        assertSame(ancillaryResolver.ancillariesFetcher(env), ancillaryResponse);
    }

    @Test
    public void testSelectAncillaries() {
        when(jsonUtils.fromDataFetching(env, SelectAncillariesRequest.class)).thenReturn(selectAncillariesRequest);
        when(ancillaryHandler.selectAncillaries(selectAncillariesRequest, context)).thenReturn(selectedAncillariesResponse);

        assertSame(ancillaryResolver.selectAncillariesFetcher(env), selectedAncillariesResponse);
    }

}
