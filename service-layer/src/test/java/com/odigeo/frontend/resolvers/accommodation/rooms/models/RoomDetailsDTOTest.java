package com.odigeo.frontend.resolvers.accommodation.rooms.models;

import bean.test.BeanTest;
import com.odigeo.frontend.resolvers.accommodation.commons.model.ImageDTO;

import java.util.Collections;

public class RoomDetailsDTOTest extends BeanTest<RoomDetailsDTO> {

    @Override
    protected RoomDetailsDTO getBean() {
        RoomDetailsDTO bean = new RoomDetailsDTO();
        bean.setMainImage(new ImageDTO());
        bean.setImages(Collections.singletonList(new ImageDTO()));
        bean.setDescription("description");
        bean.setName("name");
        return bean;
    }
}
