package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import com.odigeo.frontend.contract.location.LocationDTO;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.baggage.BaggageConditions;
import com.odigeo.searchengine.v2.responses.collection.CollectionEstimationFees;
import com.odigeo.searchengine.v2.responses.collection.CollectionMethod;
import com.odigeo.searchengine.v2.responses.itinerary.CrossFaringItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.HubItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.Itineraries;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerariesLegend;
import com.odigeo.searchengine.v2.responses.itinerary.ItineraryProvider;
import com.odigeo.searchengine.v2.responses.itinerary.ItinerarySearchResults;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;
import com.odigeo.searchengine.v2.responses.itinerary.SegmentResult;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class SearchEngineContextBuilderTest {

    @Mock
    private SearchEngineLegendMapper legendMapper;

    @InjectMocks
    private SearchEngineContextBuilder seContextBuilder;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testBuild() {
        ItinerarySearchResults itinerarySearchResults = mock(ItinerarySearchResults.class);
        ItinerariesLegend legend = mock(ItinerariesLegend.class);
        Itineraries itineraries = mock(Itineraries.class);
        Currency currency = mock(Currency.class);
        String currencyCode = "currency";

        List<SegmentResult> segmentResults = new ArrayList<>(0);
        List<FareItinerary> itineraryResults = new ArrayList<>(0);
        List<HubItinerary> hubs = new ArrayList<>(0);
        Map<Integer, SectionResult> sectionResultMap = new HashMap<>(0);
        Map<Integer, ZonedDateTime> freeCancellationMap = new HashMap<>(0);
        Map<Integer, BaggageConditions> baggageMap = new HashMap<>(0);
        Map<Integer, CollectionMethod> collectionMethodMap = new HashMap<>(0);
        Map<Integer, CollectionEstimationFees> collectionFeesMap = new HashMap<>(0);
        Map<Integer, HubItinerary> hubMap = new HashMap<>(0);
        Map<Integer, ItineraryProvider> simplesMap = new HashMap<>(0);
        Map<Integer, CrossFaringItinerary> crossFaringMap = new HashMap<>(0);
        Map<Integer, Carrier> carrierMap = new HashMap<>(0);
        Map<Integer, LocationDTO> locationMap = new HashMap<>(0);
        Map<Integer, List<TechnicalStop>> techStopsMap = new HashMap<>(0);

        when(itinerarySearchResults.getLegend()).thenReturn(legend);
        when(itinerarySearchResults.getItineraryResults()).thenReturn(itineraryResults);
        when(itinerarySearchResults.getPriceCurrency()).thenReturn(currency);
        when(legend.getSegmentResults()).thenReturn(segmentResults);
        when(legend.getItineraries()).thenReturn(itineraries);
        when(itineraries.getHubs()).thenReturn(hubs);
        when(currency.getCurrencyCode()).thenReturn(currencyCode);

        when(legendMapper.buildSectionResultMap(eq(legend))).thenReturn(sectionResultMap);
        when(legendMapper.buildFreeCancellationMap(eq(legend))).thenReturn(freeCancellationMap);
        when(legendMapper.buildBaggageMap(eq(legend))).thenReturn(baggageMap);
        when(legendMapper.buildCollectionMethodMap(eq(legend))).thenReturn(collectionMethodMap);
        when(legendMapper.buildCollectionFeesMap(eq(legend))).thenReturn(collectionFeesMap);
        when(legendMapper.buildHubMap(eq(legend))).thenReturn(hubMap);
        when(legendMapper.buildSimplesMap(eq(legend))).thenReturn(simplesMap);
        when(legendMapper.buildCrossFaringMap(legend)).thenReturn(crossFaringMap);
        when(legendMapper.buildCarrierMap(eq(legend))).thenReturn(carrierMap);
        when(legendMapper.buildLocationMap(eq(legend))).thenReturn(locationMap);
        when(legendMapper.buildTechStopsMap(eq(legend), eq(locationMap))).thenReturn(techStopsMap);

        SearchEngineContext seContext = seContextBuilder.build(itinerarySearchResults);

        assertSame(seContext.getSegmentResults(), segmentResults);
        assertSame(seContext.getItineraryResults(), itineraryResults);
        assertSame(seContext.getHubs(), hubs);
        assertEquals(seContext.getCurrency(), currencyCode);
        assertSame(seContext.getSectionResultMap(), sectionResultMap);
        assertSame(seContext.getFreeCancellationMap(), freeCancellationMap);
        assertSame(seContext.getBaggageMap(), baggageMap);
        assertSame(seContext.getCollectionMethodMap(), collectionMethodMap);
        assertSame(seContext.getCollectionFeesMap(), collectionFeesMap);
        assertSame(seContext.getHubMap(), hubMap);
        assertSame(seContext.getSimplesMap(), simplesMap);
        assertSame(seContext.getCrossFaringMap(), crossFaringMap);
        assertSame(seContext.getCarrierMap(), carrierMap);
        assertSame(seContext.getLocationMap(), locationMap);
        assertSame(seContext.getTechStopsMap(), techStopsMap);
    }

}
