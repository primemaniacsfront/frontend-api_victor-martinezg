package com.odigeo.frontend.resolvers.trip.bookingimportantmessage;

import com.edreamsodigeo.bookingimportantmessage.contract.v1.model.cancellation.CancellationMessage;
import com.odigeo.frontend.resolvers.trip.bookingimportantmessage.mapper.BookingImportantMessageMapperImpl;
import com.odigeo.frontend.services.managemybooking.BookingImportantMessageService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class BookingImportantMessageHandlerTest {

    @Mock
    private BookingImportantMessageService bookingImportantMessageService;
    @Mock
    private BookingImportantMessageFilter bookingImportantMessageFilter;
    private BookingImportantMessageHandler bookingImportantMessageHandler;

    @BeforeMethod
    public void setUpTest() {
        openMocks(this);
        bookingImportantMessageHandler = new BookingImportantMessageHandler(bookingImportantMessageService, new BookingImportantMessageMapperImpl(), bookingImportantMessageFilter);
        when(bookingImportantMessageFilter.filter(any(), any())).thenReturn(Optional.of(new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CancellationMessage()));
    }

    @Test
    public void getAllMessagesForTokenOK() {
        when(bookingImportantMessageService.getImportantMessagesByToken(any())).thenReturn(Collections.singletonList(new CancellationMessage()));
        assertEquals(bookingImportantMessageHandler.getAllMessagesForToken("anytoken", null).size(), 1);
    }

    @Test
    public void getAllMessagesForBookingIdAndEmailOK() {
        when(bookingImportantMessageService.getImportantMessagesByBookingIdAndEmail(anyLong(), any())).thenReturn(Collections.singletonList(new CancellationMessage()));
        assertEquals(bookingImportantMessageHandler.getAllMessagesForBookingIdAndEmail(1234L, "test@edreamsodigeo.com", null).size(), 1);
    }

}
