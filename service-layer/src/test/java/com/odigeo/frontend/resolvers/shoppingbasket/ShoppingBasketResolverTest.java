package com.odigeo.frontend.resolvers.shoppingbasket;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AddProductToShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BundleId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RemoveProductFromShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.shoppingbasket.factories.ShoppingBasketFactory;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.ShoppingBasketHandler;
import com.odigeo.frontend.resolvers.shoppingbasket.handlers.ShoppingBasketV2Handler;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static graphql.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class ShoppingBasketResolverTest {

    private static final String BUNDLE_ID = "bundleId";
    private static final String VISIT_INFORMATION_SERIALIZED = "visitInformationSerialized";
    private static final String CREATE_SHOPPING_MUTATION = "createShoppingBasket";
    private static final String ADD_PRODUCT_TO_SHOPPING_BASKET_MUTATION = "addProductToShoppingBasket";
    private static final String REMOVE_PRODUCT_FROM_SHOPPING_BASKET_MUTATION = "removeProductFromShoppingBasket";
    private static final String GET_SHOPPING_QUERY = "getShoppingBasket";
    private static final String SHOPPING_BASKET_ID = "1";

    @Mock
    private ShoppingBasketV2Handler shoppingBasketV2Handler;
    @Mock
    private ShoppingBasketHandler shoppingBasketHandler;
    @Mock
    private ShoppingBasketFactory shoppingBasketFactory;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;

    @InjectMocks
    private ShoppingBasketResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(env.getContext()).thenReturn(context);
        VisitInformation visitInformation = mock(VisitInformation.class);
        when(context.getVisitInformation()).thenReturn(visitInformation);
        when(visitInformation.getVisitCode()).thenReturn(VISIT_INFORMATION_SERIALIZED);
        when(shoppingBasketFactory.getInstance(isNull())).thenReturn(shoppingBasketV2Handler);
        when(shoppingBasketFactory.getInstance(ShoppingType.BASKET_V2)).thenReturn(shoppingBasketV2Handler);
        when(shoppingBasketFactory.getInstance(ShoppingType.BASKET_V3)).thenReturn(shoppingBasketHandler);
    }

    @Test
    public void addToBuilderQuery() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(GET_SHOPPING_QUERY));
    }

    @Test
    public void addToBuilderMutation() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Mutation");

        assertNotNull(queries.get(CREATE_SHOPPING_MUTATION));
        assertNotNull(queries.get(ADD_PRODUCT_TO_SHOPPING_BASKET_MUTATION));
        assertNotNull(queries.get(REMOVE_PRODUCT_FROM_SHOPPING_BASKET_MUTATION));
    }

    @Test
    public void testShoppingBasketFetcher() {
        ShoppingBasketRequest shoppingBasketRequest = new ShoppingBasketRequest();
        shoppingBasketRequest.setShoppingBasketId(SHOPPING_BASKET_ID);
        when(jsonUtils.fromDataFetching(env, ShoppingBasketRequest.class)).thenReturn(shoppingBasketRequest);
        when(shoppingBasketV2Handler.getShoppingBasket(anyString(), any())).thenReturn(getShoppingBasketResponse());

        resolver.shoppingBasketFetcher(env);

        verify(shoppingBasketV2Handler).getShoppingBasket(anyString(), any());
    }

    @Test
    public void testShoppingBasketFetcherV3() {
        ShoppingBasketRequest shoppingBasketRequest = new ShoppingBasketRequest();
        shoppingBasketRequest.setShoppingBasketId(SHOPPING_BASKET_ID);
        shoppingBasketRequest.setShoppingType(ShoppingType.BASKET_V3);

        when(jsonUtils.fromDataFetching(env, ShoppingBasketRequest.class)).thenReturn(shoppingBasketRequest);
        when(shoppingBasketHandler.getShoppingBasket(anyString(), any())).thenReturn(getShoppingBasketResponse());

        resolver.shoppingBasketFetcher(env);

        verify(shoppingBasketHandler).getShoppingBasket(anyString(), any());
    }

    @Test
    public void testShoppingBasketAvailableCollectionMethods() {
        AvailableCollectionMethodsRequest collectionMethodRq = new AvailableCollectionMethodsRequest();
        collectionMethodRq.setShoppingId(SHOPPING_BASKET_ID);
        when(jsonUtils.fromDataFetching(env, AvailableCollectionMethodsRequest.class)).thenReturn(collectionMethodRq);
        when(shoppingBasketV2Handler.getAvailableCollectionMethods(anyString(), any())).thenReturn(new AvailableCollectionMethodsResponse());

        resolver.availableCollectionMethod(env);

        verify(shoppingBasketV2Handler).getAvailableCollectionMethods(anyString(), any());
    }

    @Test
    public void testCreateShoppingBasketFetcher() {
        CreateShoppingBasketRequest createShoppingBasketRequest = new CreateShoppingBasketRequest();
        BundleId bundleId = new BundleId();
        bundleId.setId(BUNDLE_ID);
        createShoppingBasketRequest.setBundleId(bundleId);
        when(jsonUtils.fromDataFetching(env, CreateShoppingBasketRequest.class)).thenReturn(createShoppingBasketRequest);
        ShoppingBasketResponse shoppingBasket = getShoppingBasketResponse();
        when(shoppingBasketV2Handler.createShoppingBasket(BUNDLE_ID, context)).thenReturn(shoppingBasket);

        ShoppingBasketResponse shoppingBasketFetcher = resolver.createShoppingBasketFetcher(env);

        assertSame(shoppingBasket, shoppingBasketFetcher);
    }

    @Test
    public void testAddProductsInShoppingBasketFetcher() {
        AddProductToShoppingBasketRequest addProductToShoppingBasketRequest = new AddProductToShoppingBasketRequest();
        addProductToShoppingBasketRequest.setShoppingBasketId(SHOPPING_BASKET_ID);
        when(jsonUtils.fromDataFetching(env, AddProductToShoppingBasketRequest.class)).thenReturn(addProductToShoppingBasketRequest);
        when(shoppingBasketV2Handler.addProduct(any(), any())).thenReturn(Boolean.TRUE);

        assertTrue(resolver.addProductsInShoppingBasketFetcher(env));

        verify(shoppingBasketV2Handler).addProduct(any(), any());
    }

    @Test
    public void testRemoveProductsFromShoppingBasketFetcher() {
        RemoveProductFromShoppingBasketRequest removeProductFromShoppingBasketRequest = new RemoveProductFromShoppingBasketRequest();
        when(jsonUtils.fromDataFetching(env, RemoveProductFromShoppingBasketRequest.class)).thenReturn(removeProductFromShoppingBasketRequest);
        when(shoppingBasketV2Handler.removeProduct(any(), any())).thenReturn(Boolean.TRUE);

        assertTrue(resolver.removeProductFromShoppingBasketFetcher(env));

        verify(shoppingBasketV2Handler).removeProduct(any(), any());
    }

    private ShoppingBasketResponse getShoppingBasketResponse() {
        return new ShoppingBasketResponse();
    }
}
