package com.odigeo.frontend.resolvers.shoppingbasket.handlers.dapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AvailableCollectionMethodsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CollectionMethod;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.resolvers.accommodation.product.mappers.AvailableCollectionMethodsMapper;
import com.odigeo.frontend.services.DapiService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ShoppingCartAvailableCollectionMethodsTest {

    @Mock
    private ResolverContext context;
    @Mock
    private DapiService dapiService;
    @Mock
    private AvailableCollectionMethodsMapper availableCollectionMethodsMapper;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private AvailableCollectionMethodsResponse collectionMethodResponse;

    @InjectMocks
    private ShoppingCartAvailableCollectionMethods shoppingAvailableCollectionMethods;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.openMocks(this);
        when(dapiService.getShoppingCart(anyLong(), any())).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(availableCollectionMethodsMapper.paymentInformationContractToModel(eq(shoppingCart))).thenReturn(collectionMethodResponse);
    }

    @Test
    public void testGetAvailableCollectionMethodEmpty() {
        shoppingAvailableCollectionMethods.getAvailableCollectionMethods("123", context);
        verify(dapiService).getShoppingCart(eq(Long.parseLong("123")), eq(context));
        verify(availableCollectionMethodsMapper).paymentInformationContractToModel(eq(shoppingCart));
        verify(collectionMethodResponse).getCollectionOptions();
    }

    @Test
    public void testGetAvailableCollectionMethods() {
        List<CollectionMethod> collectionMethodList = Arrays.asList(new CollectionMethod(), new CollectionMethod());
        when(collectionMethodResponse.getCollectionOptions()).thenReturn(collectionMethodList);
        shoppingAvailableCollectionMethods.getAvailableCollectionMethods("123", context);
        verify(dapiService).getShoppingCart(eq(Long.parseLong("123")), eq(context));
        verify(availableCollectionMethodsMapper).paymentInformationContractToModel(eq(shoppingCart));
        verify(collectionMethodResponse).getCollectionOptions();
        assertTrue(collectionMethodList.get(0).getIsChosenMethod());
        assertFalse(collectionMethodList.get(1).getIsChosenMethod());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetAvailableCollectionMethodWrongId() {
        shoppingAvailableCollectionMethods.getAvailableCollectionMethods("1234-123", context);
    }
}
