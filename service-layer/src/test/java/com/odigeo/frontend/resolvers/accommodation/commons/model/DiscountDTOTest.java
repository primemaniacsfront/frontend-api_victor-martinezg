package com.odigeo.frontend.resolvers.accommodation.commons.model;

import bean.test.BeanTest;

import java.math.BigDecimal;

public class DiscountDTOTest extends BeanTest<DiscountDTO> {

    @Override
    protected DiscountDTO getBean() {
        DiscountDTO bean = new DiscountDTO();
        bean.setPercentage(BigDecimal.ONE);
        bean.setPrimeAmount(BigDecimal.TEN);
        bean.setPackageAmount(BigDecimal.TEN.add(BigDecimal.ONE));
        return bean;
    }
}
