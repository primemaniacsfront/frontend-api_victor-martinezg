package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.ProductType;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.section.AbstractSection;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.section.FlightSection;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.section.TrainSection;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.location.LocationDTO;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.ZonedDateTime;

import static graphql.Assert.assertTrue;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class FootprintSectionMapperTest {

    private FootprintSectionMapper mapper;

    private static final Integer INTEGER = 1;
    private static final ZonedDateTime DEPARTURE_DATE = ZonedDateTime.now();
    private static final ZonedDateTime ARRIVAL_DATE = ZonedDateTime.now().plusDays(1);
    private static final String FLIGHT_CODE = "flightCode";
    private static final Integer ARRIVAL_ID = 2;
    private static final Integer DEPARTURE_ID = 3;
    private static final String IATA = "iata";
    private static final String CITY_IATA = "cityIata";

    @BeforeClass
    public void init() {
        mapper = new FootprintSectionMapper();
    }

    @Test
    public void testFlightSection() {
        AbstractSection section = mapper.map(getSectionDTOMock(TransportType.PLANE, LocationType.AIRPORT), INTEGER);
        assertNotNull(section);
        assertTrue(section instanceof FlightSection);
        assertEquals(((FlightSection) section).getFlightNumber(), FLIGHT_CODE);
        assertEquals(section.getProductType(), ProductType.FLIGHT);
        assertNotNull(section.getGeoInformation());
        assertEquals(section.getGeoInformation().getArrivalGeoNodeId(), ARRIVAL_ID);
        assertEquals(section.getGeoInformation().getArrivalIata(), IATA);
        assertEquals(section.getGeoInformation().getDepartureGeoNodeId(), DEPARTURE_ID);
        assertEquals(section.getGeoInformation().getDepartureIata(), IATA);
        assertEquals(section.getArrivalDateUTC(), ARRIVAL_DATE);
        assertEquals(section.getDepartureDateUTC(), DEPARTURE_DATE);
        assertEquals(section.getSectionIndex(), INTEGER);
    }

    @Test
    public void testTrainSection() {
        AbstractSection section = mapper.map(getSectionDTOMock(TransportType.TRAIN, LocationType.TRAIN_STATION), INTEGER);
        assertNotNull(section);
        assertTrue(section instanceof TrainSection);
        assertEquals(((TrainSection) section).getTrainCode(), FLIGHT_CODE);
        assertEquals(section.getProductType(), ProductType.TRAIN);
        assertNotNull(section.getGeoInformation());
        assertEquals(section.getGeoInformation().getArrivalGeoNodeId(), ARRIVAL_ID);
        assertEquals(section.getGeoInformation().getArrivalIata(), CITY_IATA);
        assertEquals(section.getGeoInformation().getDepartureGeoNodeId(), DEPARTURE_ID);
        assertEquals(section.getGeoInformation().getDepartureIata(), CITY_IATA);
        assertEquals(section.getArrivalDateUTC(), ARRIVAL_DATE);
        assertEquals(section.getDepartureDateUTC(), DEPARTURE_DATE);
        assertEquals(section.getSectionIndex(), INTEGER);
    }

    private SectionDTO getSectionDTOMock(TransportType transportType, LocationType locationType) {
        SectionDTO sectionDTO = new SectionDTO();
        sectionDTO.setDepartureDate(DEPARTURE_DATE);
        sectionDTO.setArrivalDate(ARRIVAL_DATE);
        sectionDTO.setFlightCode(FLIGHT_CODE);
        sectionDTO.setTransportType(transportType);
        sectionDTO.setDeparture(getLocationDTOMock(DEPARTURE_ID, locationType));
        sectionDTO.setDestination(getLocationDTOMock(ARRIVAL_ID, locationType));
        return sectionDTO;
    }

    private LocationDTO getLocationDTOMock(Integer id, LocationType locationType) {
        LocationDTO locationDTO = new LocationDTO();
        locationDTO.setCityIata("cityIata");
        locationDTO.setIata("iata");
        locationDTO.setId(id);
        locationDTO.setLocationType(locationType);
        return locationDTO;
    }

}
