package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageCondition;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.google.inject.Guice;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.contract.location.LocationDTO;
import com.odigeo.frontend.resolvers.search.mappers.BaggageConditionMapper;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.services.CurexService;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Continent;
import com.odigeo.geoapi.v4.responses.Coordinates;
import com.odigeo.geoapi.v4.responses.Country;
import com.odigeo.geoapi.v4.responses.ItineraryLocation;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.BaggageIncluded;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.GeographicalInformation;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.Itinerary;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.Segment;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.SegmentResult;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MirItineraryHandlerTest {

    private static final String CURRENCY_EUR = "EUR";
    private static final int ITINERARIES_TO_RATE = 150;

    @Mock
    private CurexService curexService;
    @Mock
    private BaggageConditionMapper baggageConditionMapper;
    @Mock
    private SearchResponseDTO searchResponseDTO;
    @Mock
    private SearchItineraryDTO itineraryDTO;
    @Mock
    private LegDTO legDTO;
    @Mock
    private SegmentDTO segmentDTO;
    @Mock
    private SectionDTO sectionDepartureDTO;
    @Mock
    private SectionDTO sectionDestinationDTO;
    @Mock
    private Carrier carrierDTO;
    @Mock
    private LocationDTO departureDTO;
    @Mock
    private LocationDTO destinationDTO;
    @Mock
    private GeoService geoService;

    private MirItineraryHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new MirItineraryHandler();

        Guice.createInjector(binder -> {
            binder.bind(CurexService.class).toProvider(() -> curexService);
            binder.bind(BaggageConditionMapper.class).toProvider(() -> baggageConditionMapper);
            binder.bind(GeoService.class).toProvider(() -> geoService);
        }).injectMembers(handler);

        when(searchResponseDTO.getItineraries()).thenReturn(Collections.singletonList(itineraryDTO));
        when(itineraryDTO.getLegs()).thenReturn(Collections.singletonList(legDTO));
        when(legDTO.getSegments()).thenReturn(Collections.singletonList(segmentDTO));

        when(segmentDTO.getCarrier()).thenReturn(carrierDTO);
        when(segmentDTO.getSections()).thenReturn(Arrays.asList(sectionDepartureDTO, sectionDestinationDTO));

        when(sectionDepartureDTO.getArrivalDate()).thenReturn(ZonedDateTime.now());
        when(sectionDestinationDTO.getDepartureDate()).thenReturn(ZonedDateTime.now());
        when(sectionDepartureDTO.getDeparture()).thenReturn(departureDTO);
        when(sectionDestinationDTO.getDestination()).thenReturn(destinationDTO);

        when(departureDTO.getLocationType()).thenReturn(mock(LocationType.class));
        when(destinationDTO.getLocationType()).thenReturn(mock(LocationType.class));
    }

    @Test
    public void testBuildItinerary() {
        String currencyCode = "code";
        Integer itineraryIndex = 1;
        BigDecimal conversionRate = new BigDecimal("0.5");
        BigDecimal sortPrice = BigDecimal.TEN;
        BigDecimal price = sortPrice.multiply(conversionRate);

        when(curexService.getCurrencyConvertRate(eq(currencyCode), eq(CURRENCY_EUR))).thenReturn(conversionRate);

        when(itineraryDTO.getIndex()).thenReturn(itineraryIndex);
        when(itineraryDTO.getFreeCancellation()).thenReturn(ZonedDateTime.now());
        when(itineraryDTO.getSortPrice()).thenReturn(sortPrice);
        when(legDTO.getSegments()).thenReturn(Collections.emptyList());
        when(searchResponseDTO.getCurrencyCode()).thenReturn(currencyCode);

        List<Itinerary> ratingItineraries = handler.buildRatingItineraries(searchResponseDTO);
        Itinerary ratingItinerary = ratingItineraries.get(0);

        assertEquals(ratingItineraries.size(), 1);
        assertEquals(ratingItinerary.getItineraryIndex(), itineraryIndex);
        assertTrue(ratingItinerary.getFreeCancellation());
        assertEquals(ratingItinerary.getApparentPriceInEur(), price);
        assertEquals(ratingItinerary.getSegmentResults().size(), 1);
    }

    @Test
    public void testBuildSegments() {
        Integer segmentIndex = 2;
        Integer segmentSeats = 3;
        Long segmentDuration = 1L;
        String carrierId = "carrier";

        BaggageCondition segmentBaggageCond = mock(BaggageCondition.class);
        BaggageIncluded baggageIncluded = mock(BaggageIncluded.class);
        ZonedDateTime departureDate = mock(ZonedDateTime.class);
        ZonedDateTime arrivalDate = mock(ZonedDateTime.class);

        when(segmentDTO.getId()).thenReturn(segmentIndex);
        when(segmentDTO.getDuration()).thenReturn(segmentDuration);
        when(segmentDTO.getSeats()).thenReturn(segmentSeats);
        when(segmentDTO.getBaggageCondition()).thenReturn(segmentBaggageCond);

        when(carrierDTO.getId()).thenReturn(carrierId);

        when(sectionDepartureDTO.getDepartureDate()).thenReturn(departureDate);
        when(sectionDestinationDTO.getArrivalDate()).thenReturn(arrivalDate);

        when(baggageConditionMapper.map(eq(segmentBaggageCond))).thenReturn(baggageIncluded);

        List<Itinerary> ratingItineraries = handler.buildRatingItineraries(searchResponseDTO);
        SegmentResult ratingLeg = ratingItineraries.get(0).getSegmentResults().get(0);
        Segment ratingSegment = ratingLeg.getSegments().get(0);

        assertEquals(ratingLeg.getSegments().size(), 1);

        assertEquals(ratingSegment.getSegmentIndex(), segmentIndex);
        assertEquals(ratingSegment.getFlightDuration(), segmentDuration);
        assertEquals(ratingSegment.getSeatsAvailable(), segmentSeats);
        assertEquals(ratingSegment.getBaggageIncluded(), baggageIncluded);
        assertEquals(ratingSegment.getMarketingCarrierCode(), carrierId);
        assertEquals(ratingSegment.getDepartureDateUTC(), departureDate);
        assertEquals(ratingSegment.getArrivalDateUTC(), arrivalDate);
    }

    @Test
    public void testPopulateSegmentStops() {
        ZonedDateTime departureDate = ZonedDateTime.now();
        ZonedDateTime arrivalDate = departureDate.plus(60, ChronoUnit.MINUTES);

        when(sectionDepartureDTO.getArrivalDate()).thenReturn(departureDate);
        when(sectionDestinationDTO.getDepartureDate()).thenReturn(arrivalDate);

        List<Itinerary> ratingItineraries = handler.buildRatingItineraries(searchResponseDTO);
        Segment ratingSegment = ratingItineraries.get(0)
            .getSegmentResults().get(0)
            .getSegments().get(0);

        assertEquals(ratingSegment.getNumStopovers(), new Integer(1));
        assertEquals(ratingSegment.getDurStopovers(), new Long(60));
    }

    @Test
    public void testPopulateSegmentGeoInfo() {
        LocationType departureLocation = LocationType.AIRPORT;
        LocationType destinationLocation = LocationType.TRAIN_STATION;
        Integer departureId = 1;
        Integer destinationId = 2;
        String departureIata = "depIata";
        String destinationIata = "destIata";
        String departureCity = "depCity";
        String destinationCity = "destCity";
        String departureCountry = "depCountry";
        String destinationCountry = "destCountry";

        when(departureDTO.getId()).thenReturn(departureId);
        when(departureDTO.getLocationType()).thenReturn(departureLocation);
        when(departureDTO.getIata()).thenReturn(departureIata);
        when(departureDTO.getCityIata()).thenReturn(departureCity);
        when(departureDTO.getCountryCode()).thenReturn(departureCountry);

        when(destinationDTO.getId()).thenReturn(destinationId);
        when(destinationDTO.getLocationType()).thenReturn(destinationLocation);
        when(destinationDTO.getIata()).thenReturn(destinationIata);
        when(destinationDTO.getCityIata()).thenReturn(destinationCity);
        when(destinationDTO.getCountryCode()).thenReturn(destinationCountry);

        ItineraryLocation departureItineraryLoc = mock(ItineraryLocation.class);
        ItineraryLocation destinationItineraryLoc = mock(ItineraryLocation.class);
        BigDecimal departureLat = mock(BigDecimal.class);
        BigDecimal destinationLat = mock(BigDecimal.class);
        BigDecimal departureLon = mock(BigDecimal.class);
        BigDecimal destinationLon = mock(BigDecimal.class);
        int departureContinent = 0;
        int destinationContinent = 1;

        when(geoService.getItineraryLocation(eq(departureId))).thenReturn(departureItineraryLoc);
        when(geoService.getItineraryLocation(eq(destinationId))).thenReturn(destinationItineraryLoc);

        when(departureItineraryLoc.getCoordinates()).thenReturn(mock(Coordinates.class));
        when(destinationItineraryLoc.getCoordinates()).thenReturn(mock(Coordinates.class));
        when(departureItineraryLoc.getCoordinates().getLatitude()).thenReturn(departureLat);
        when(departureItineraryLoc.getCoordinates().getLongitude()).thenReturn(departureLon);
        when(destinationItineraryLoc.getCoordinates().getLatitude()).thenReturn(destinationLat);
        when(destinationItineraryLoc.getCoordinates().getLongitude()).thenReturn(destinationLon);

        when(departureItineraryLoc.getCity()).thenReturn(mock(City.class));
        when(departureItineraryLoc.getCity().getCountry()).thenReturn(mock(Country.class));
        when(departureItineraryLoc.getCity().getCountry().getContinent()).thenReturn(mock(Continent.class));
        when(departureItineraryLoc.getCity().getCountry().getContinent().getContinentId()).thenReturn(departureContinent);
        when(destinationItineraryLoc.getCity()).thenReturn(mock(City.class));
        when(destinationItineraryLoc.getCity().getCountry()).thenReturn(mock(Country.class));
        when(destinationItineraryLoc.getCity().getCountry().getContinent()).thenReturn(mock(Continent.class));
        when(destinationItineraryLoc.getCity().getCountry().getContinent().getContinentId()).thenReturn(destinationContinent);

        List<Itinerary> ratingItineraries = handler.buildRatingItineraries(searchResponseDTO);
        GeographicalInformation geoInfo = ratingItineraries.get(0)
            .getSegmentResults().get(0)
            .getSegments().get(0)
            .getGeoInformation();

        assertEquals(geoInfo.getDepartureAirportIata(), departureIata);
        assertEquals(geoInfo.getDepartureCountryCode(), departureCountry);
        assertEquals(geoInfo.getDepartureCityIata(), departureCity);

        assertEquals(geoInfo.getArrivalAirportIata(), destinationCity);
        assertEquals(geoInfo.getArrivalCountryCode(), destinationCountry);
        assertEquals(geoInfo.getArrivalCityIata(), destinationCity);

        assertEquals(geoInfo.getDepartureCoordinates().getLatitude(), departureLat);
        assertEquals(geoInfo.getDepartureCoordinates().getLongitude(), departureLon);
        assertEquals(geoInfo.getArrivalCoordinates().getLatitude(), destinationLat);
        assertEquals(geoInfo.getArrivalCoordinates().getLongitude(), destinationLon);

        assertEquals(geoInfo.getDepartureContinentId(), Integer.toString(departureContinent));
        assertEquals(geoInfo.getArrivalContinentId(), Integer.toString(destinationContinent));
    }

    @Test
    public void testBuildRatingItineraries() {
        List<SearchItineraryDTO> itineraryDTOList = new ArrayList<>(ITINERARIES_TO_RATE);
        SearchItineraryDTO itineraryDTO1 = mock(SearchItineraryDTO.class);
        SearchItineraryDTO itineraryDTO2 = mock(SearchItineraryDTO.class);
        SearchItineraryDTO itineraryDTO3 = mock(SearchItineraryDTO.class);
        SearchResponseDTO searchResponseDTO = mock(SearchResponseDTO.class);

        Integer index1 = 0;
        Integer index2 = 1;
        int numberItineraries1 = ITINERARIES_TO_RATE - 1;
        int numberItineraries2 = 10;

        when(itineraryDTO1.getIndex()).thenReturn(index1);
        when(itineraryDTO2.getIndex()).thenReturn(index2);
        when(itineraryDTO3.getIndex()).thenReturn(2);

        when(searchResponseDTO.getItineraries()).thenReturn(itineraryDTOList);

        addItineraries(itineraryDTOList, itineraryDTO1, numberItineraries1);
        addItineraries(itineraryDTOList, itineraryDTO2, numberItineraries2);
        addItineraries(itineraryDTOList, itineraryDTO3, 10);

        List<Itinerary> ratingItineraries = handler.buildRatingItineraries(searchResponseDTO);

        assertEquals(ratingItineraries.size(), numberItineraries1 + numberItineraries2);

        int index = 0;

        for (; index < numberItineraries1; index++) {
            assertEquals(ratingItineraries.get(index).getItineraryIndex(), index1);
        }

        for (; index < ratingItineraries.size(); index++) {
            assertEquals(ratingItineraries.get(index).getItineraryIndex(), index2);
        }
    }

    private void addItineraries(List<SearchItineraryDTO> itineraryDTOList, SearchItineraryDTO itineraryDTO, int number) {
        for (int i = 0; i < number; i++) {
            itineraryDTOList.add(itineraryDTO);
        }
    }

    @Test
    public void testGeoServiceFails() {
        when(geoService.getItineraryLocation(anyInt())).thenThrow(RuntimeException.class);

        List<Itinerary> ratingItineraries = handler.buildRatingItineraries(searchResponseDTO);
        GeographicalInformation geoInfo = ratingItineraries.get(0)
            .getSegmentResults().get(0)
            .getSegments().get(0)
            .getGeoInformation();

        assertNull(geoInfo.getDepartureCoordinates());
        assertNull(geoInfo.getArrivalCoordinates());
        assertNull(geoInfo.getDepartureContinentId());
        assertNull(geoInfo.getArrivalContinentId());
    }

}
