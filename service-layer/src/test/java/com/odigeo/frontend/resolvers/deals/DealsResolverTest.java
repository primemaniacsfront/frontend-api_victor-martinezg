package com.odigeo.frontend.resolvers.deals;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Deal;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.DealsRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.deals.handlers.DealsHandler;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static com.odigeo.frontend.resolvers.deals.DealsResolver.DEALS_QUERY;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class DealsResolverTest {

    private static final String ERROR = "ERROR";

    @Mock
    private DealsRequest dealsRequest;
    @Mock
    private List<Deal> dealsResponse;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private DataFetchingEnvironment environment;

    @Mock
    private DealsHandler dealsHandler;
    @Mock
    private JsonUtils jsonUtils;

    @InjectMocks
    private DealsResolver resolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        when(jsonUtils.fromDataFetching(eq(environment), eq(DealsRequest.class)))
                .thenReturn(dealsRequest);
        when(environment.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visitInformation);
    }

    @Test
    public void addToBuilderQueries() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(DEALS_QUERY));
    }

    @Test
    public void testDealsFetcher() {
        when(dealsHandler.getDeals(dealsRequest, visitInformation)).thenReturn(dealsResponse);
        assertEquals(resolver.dealsFetcher(environment), dealsResponse);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testDealsFetcherServiceException() {
        when(dealsHandler.getDeals(dealsRequest, visitInformation)).thenThrow(new ServiceException(ERROR, ServiceName.FRONTEND_API));
        assertEquals(resolver.dealsFetcher(environment), dealsResponse);
    }

}
