package com.odigeo.frontend.resolvers.prime.operations;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionActions;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionResponse;
import com.odigeo.dapi.client.Buyer;
import com.odigeo.dapi.client.MembershipPerks;
import com.odigeo.dapi.client.MembershipSubscriptionShoppingItem;
import com.odigeo.dapi.client.OtherProductsShoppingItems;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.membershipoffer.MembershipOfferHandler;
import com.odigeo.frontend.resolvers.prime.handlers.SubscriptionResponseHandler;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.resolvers.user.mappers.MembershipSubscriptionProductMapper;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;

import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipSubscriptionActions.EDIT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipShoppingCartOperationsTest {
    private static final long SHOPPING_ID = 12345;
    private static final String EMAIL = "buyer@edreams.com";
    private static final String NAME = "TOM";
    private static final String LAST_NAME = "DOT";
    private static final String US = "US";
    private static final String OFFER_ID = UUID.randomUUID().toString();
    private static final SubscriptionResponse SUBSCRIPTION_RESPONSE = new SubscriptionResponse();
    private static final ModifyMembershipSubscription MODIFY_MEMBERSHIP_SUBSCRIPTION_PRODUCT = ModifyMembershipSubscription
            .builder()
            .action(EDIT)
            .offerId(OFFER_ID)
            .build();
    public static final MembershipSubscriptionResponse MEMBERSHIP_SUBSCRIPTION_RESPONSE = new MembershipSubscriptionResponse();

    @Mock
    private ShoppingCartHandler shoppingCartHandler;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private Buyer buyer;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private MembershipPerks membershipPerks;
    @Mock
    private Map<MembershipSubscriptionActions, BiFunction<ModifyMembershipSubscription, ResolverContext, ShoppingCartSummaryResponse>> membershipSubscriptionProductActions;
    @Mock
    private EditMembershipSubscriptionAction editMembershipSubscriptionAction;
    @Mock
    private SubscriptionResponseHandler subscriptionResponseHandler;
    @Mock
    private MembershipOfferHandler membershipOfferHandler;
    @Mock
    private MembershipSubscriptionOffer subscriptionOffer;
    @Mock
    private MembershipSubscriptionProductMapper membershipSubscriptionProductMapper;
    @Mock
    private OtherProductsShoppingItems otherProductsShoppingItems;
    @Mock
    private MembershipSubscriptionShoppingItem membershipSubscriptionProductItem;
    @Mock
    private BiFunction<ModifyMembershipSubscription, ResolverContext, Object> extractShoppingItemFromShoppingCart;
    @InjectMocks
    private MembershipShoppingCartOperations membershipShoppingCartOperations;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(context.getVisitInformation()).thenReturn(visitInformation);
    }

    @Test
    public void testBuildFreeTrialCandidateRequest() {
        setDefaultShoppingCartSummaryResponse();
        when(visitInformation.getWebsiteDefaultCountry()).thenReturn(US);
        FreeTrialCandidateRequest freeTrialCandidateRequest = membershipShoppingCartOperations.buildFreeTrialCandidateRequest(SHOPPING_ID, context);
        assertEquals(freeTrialCandidateRequest.getLastNames(), LAST_NAME);
        assertEquals(freeTrialCandidateRequest.getName(), NAME);
        assertEquals(freeTrialCandidateRequest.getWebsite(), US);
        assertEquals(freeTrialCandidateRequest.getEmail(), EMAIL);
    }

    @Test
    public void testGetBuyerEmail() {
        setDefaultShoppingCartSummaryResponse();
        assertEquals(membershipShoppingCartOperations.getBuyerEmail(SHOPPING_ID, context), EMAIL);
    }

    private void setDefaultShoppingCartSummaryResponse() {
        when(shoppingCartHandler.getShoppingCart(eq(SHOPPING_ID), eq(context))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getBuyer()).thenReturn(buyer);
        when(buyer.getMail()).thenReturn(EMAIL);
        when(buyer.getName()).thenReturn(NAME);
        when(buyer.getLastNames()).thenReturn(LAST_NAME);
        when(shoppingCart.getOtherProductsShoppingItemContainer()).thenReturn(otherProductsShoppingItems);
        when(otherProductsShoppingItems.getMembershipSubscriptionShoppingItems())
                .thenReturn(Collections.singletonList(membershipSubscriptionProductItem));
    }

    @Test
    public void testIsPrimeUserDiscountAppliedWhenPrimeUser() {
        setDefaultShoppingCartSummaryResponse();
        when(shoppingCart.getMembershipPerks()).thenReturn(membershipPerks);
        assertTrue(membershipShoppingCartOperations.isPrimeUserDiscountApplied(SHOPPING_ID, context));
    }

    @Test
    public void testIsPrimeUserDiscountAppliedWhenNonPrimeUser() {
        setDefaultShoppingCartSummaryResponse();
        when(shoppingCart.getMembershipPerks()).thenReturn(null);
        assertFalse(membershipShoppingCartOperations.isPrimeUserDiscountApplied(SHOPPING_ID, context));
    }

    @Test
    public void testGetMembershipProductFromShoppingCart() {
        setDefaultShoppingCartSummaryResponse();
        given(membershipSubscriptionProductMapper.fromMembershipShoppingItem(eq(membershipSubscriptionProductItem)))
                .willReturn(MEMBERSHIP_SUBSCRIPTION_RESPONSE);

        MembershipSubscriptionResponse membershipSubscriptionResponse = membershipShoppingCartOperations
                .getMembershipSubscriptionResponse(SHOPPING_ID, context);

        assertEquals(membershipSubscriptionResponse, MEMBERSHIP_SUBSCRIPTION_RESPONSE);
        verify(shoppingCartHandler).getShoppingCart(eq(SHOPPING_ID), eq(context));
    }

    @Test
    public void testUpdateMembershipProductNullShoppingItem() {
        given(membershipSubscriptionProductActions.get(eq(EDIT)))
                .willReturn(editMembershipSubscriptionAction);
        given(editMembershipSubscriptionAction.apply(eq(MODIFY_MEMBERSHIP_SUBSCRIPTION_PRODUCT), eq(context)))
                .willReturn(shoppingCartSummaryResponse);
        given(editMembershipSubscriptionAction.andThen(any())).willCallRealMethod();
        given(membershipOfferHandler.getSubscriptionOffer(eq(OFFER_ID)))
                .willReturn(subscriptionOffer);

        MembershipSubscriptionResponse membershipSubscriptionResponse = membershipShoppingCartOperations
                .updateMembershipSubscriptionProduct(MODIFY_MEMBERSHIP_SUBSCRIPTION_PRODUCT, context);

        assertNull(membershipSubscriptionResponse);
        verifyNoInteractions(subscriptionResponseHandler);
        verifyNoInteractions(membershipSubscriptionProductMapper);
    }

    @Test
    public void testUpdateMembershipProduct() {
        setDefaultShoppingCartSummaryResponse();
        given(membershipSubscriptionProductActions.get(eq(EDIT)))
                .willReturn(editMembershipSubscriptionAction);
        given(extractShoppingItemFromShoppingCart.apply(eq(MODIFY_MEMBERSHIP_SUBSCRIPTION_PRODUCT), eq(context)))
                .willReturn(membershipSubscriptionProductItem);
        given(editMembershipSubscriptionAction.andThen(any()))
                .willReturn(extractShoppingItemFromShoppingCart);
        given(membershipOfferHandler.getSubscriptionOffer(eq(OFFER_ID)))
                .willReturn(subscriptionOffer);
        given(subscriptionResponseHandler.buildSubscriptionResponse(eq(subscriptionOffer)))
                .willReturn(SUBSCRIPTION_RESPONSE);
        given(membershipSubscriptionProductMapper.fromMembershipShoppingItemAndSubscriptionResponse(eq(membershipSubscriptionProductItem), eq(SUBSCRIPTION_RESPONSE)))
                .willReturn(MEMBERSHIP_SUBSCRIPTION_RESPONSE);

        MembershipSubscriptionResponse membershipSubscriptionResponse = membershipShoppingCartOperations.updateMembershipSubscriptionProduct(MODIFY_MEMBERSHIP_SUBSCRIPTION_PRODUCT, context);

        assertEquals(membershipSubscriptionResponse, MEMBERSHIP_SUBSCRIPTION_RESPONSE);
        verify(subscriptionResponseHandler).buildSubscriptionResponse(eq(subscriptionOffer));
        InOrder inOrder = Mockito.inOrder(editMembershipSubscriptionAction, extractShoppingItemFromShoppingCart, membershipSubscriptionProductMapper);
        inOrder.verify(editMembershipSubscriptionAction).andThen(any());
        inOrder.verify(extractShoppingItemFromShoppingCart).apply(eq(MODIFY_MEMBERSHIP_SUBSCRIPTION_PRODUCT), eq(context));
        inOrder.verify(membershipSubscriptionProductMapper).fromMembershipShoppingItemAndSubscriptionResponse(eq(membershipSubscriptionProductItem), eq(SUBSCRIPTION_RESPONSE));
    }
}
