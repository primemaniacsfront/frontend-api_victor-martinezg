package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsModal;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsModalFeatureType;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import org.apache.commons.collections4.CollectionUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class NonEssentialProductsModalMapperTest {

    @Mock
    private AncillaryConfiguration ancillaryConfiguration;

    private NonEssentialProductsModalMapper mapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        mapper = new NonEssentialProductsModalMapper();
    }

    @Test
    public void mapShouldNotReturnFeaturesWithAllFlagsNull() {
        when(ancillaryConfiguration.getShowModalExclusions()).thenReturn(null);
        when(ancillaryConfiguration.getShowModalBenefits()).thenReturn(null);
        when(ancillaryConfiguration.getIsAttachableFromModal()).thenReturn(null);
        when(ancillaryConfiguration.getShowModalDualContract()).thenReturn(null);
        when(ancillaryConfiguration.getIsModalActive()).thenReturn(null);

        NonEssentialProductsModal nonEssentialProductsModal = mapper.map(ancillaryConfiguration);

        assertFalse(nonEssentialProductsModal.getActive());
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsModal.getFeatures()));
    }

    @Test
    public void mapShouldNotReturnFeaturesWithAllFlagsFalse() {
        when(ancillaryConfiguration.getShowModalExclusions()).thenReturn(false);
        when(ancillaryConfiguration.getShowModalBenefits()).thenReturn(false);
        when(ancillaryConfiguration.getIsAttachableFromModal()).thenReturn(false);
        when(ancillaryConfiguration.getShowModalDualContract()).thenReturn(false);
        when(ancillaryConfiguration.getIsModalActive()).thenReturn(false);

        NonEssentialProductsModal nonEssentialProductsModal = mapper.map(ancillaryConfiguration);

        assertFalse(nonEssentialProductsModal.getActive());
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsModal.getFeatures()));
    }

    @Test
    public void mapShouldReturnFeaturesWithAllFlagsTrue() {
        when(ancillaryConfiguration.getShowModalExclusions()).thenReturn(true);
        when(ancillaryConfiguration.getShowModalBenefits()).thenReturn(true);
        when(ancillaryConfiguration.getIsAttachableFromModal()).thenReturn(true);
        when(ancillaryConfiguration.getShowModalDualContract()).thenReturn(true);
        when(ancillaryConfiguration.getIsModalActive()).thenReturn(true);

        NonEssentialProductsModal nonEssentialProductsModal = mapper.map(ancillaryConfiguration);

        assertTrue(nonEssentialProductsModal.getActive());
        assertTrue(nonEssentialProductsModal.getFeatures().contains(NonEssentialProductsModalFeatureType.EXCLUSIONS));
        assertTrue(nonEssentialProductsModal.getFeatures().contains(NonEssentialProductsModalFeatureType.BENEFITS));
        assertTrue(nonEssentialProductsModal.getFeatures().contains(NonEssentialProductsModalFeatureType.ATTACHABLE));
        assertTrue(nonEssentialProductsModal.getFeatures().contains(NonEssentialProductsModalFeatureType.DUAL_CONTRACT));
    }
}
