package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NagType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsModal;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsThemeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsUIConfig;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class NonEssentialProductsUIConfigMapperTest {

    @Mock
    private NonEssentialProductsModalMapper nonEssentialProductsModalMapper;
    @Mock
    private NagTypesMapper nagTypesMapper;
    @Mock
    private NonEssentialProductsThemeTypeMapper nonEssentialProductsThemeTypeMapper;
    @Mock
    private AncillaryConfiguration ancillaryConfiguration;
    @InjectMocks
    private NonEssentialProductsUIConfigMapper mapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        when(ancillaryConfiguration.getPrechecked()).thenReturn(true);
    }

    @Test
    public void testData() {
        NonEssentialProductsModal nonEssentialProductsModal = new NonEssentialProductsModal();
        when(nonEssentialProductsModalMapper.map(ancillaryConfiguration)).thenReturn(nonEssentialProductsModal);
        List<NagType> nagTypes = new ArrayList<>();
        when(nagTypesMapper.map(ancillaryConfiguration)).thenReturn(nagTypes);
        NonEssentialProductsThemeType nonEssentialProductsThemeType = NonEssentialProductsThemeType.HIGHLIGHT;
        when(nonEssentialProductsThemeTypeMapper.map(ancillaryConfiguration)).thenReturn(nonEssentialProductsThemeType);
        Boolean prechecked = Boolean.TRUE;
        when(ancillaryConfiguration.getPrechecked()).thenReturn(prechecked);

        NonEssentialProductsUIConfig nonEssentialProductsUIConfig = mapper.map(ancillaryConfiguration);

        assertEquals(nonEssentialProductsUIConfig.getModal(), nonEssentialProductsModal);
        assertEquals(nonEssentialProductsUIConfig.getAvailableNags(), nagTypes);
        assertEquals(nonEssentialProductsUIConfig.getTheme(), nonEssentialProductsThemeType);
        assertEquals(nonEssentialProductsUIConfig.getPrechecked(), prechecked);
    }

    @Test
    public void testNullPrechecked() {
        when(ancillaryConfiguration.getPrechecked()).thenReturn(null);

        NonEssentialProductsUIConfig nonEssentialProductsUIConfig = mapper.map(ancillaryConfiguration);

        assertFalse(nonEssentialProductsUIConfig.getPrechecked());
    }

    @Test
    public void testFalsePrechecked() {
        when(ancillaryConfiguration.getPrechecked()).thenReturn(Boolean.FALSE);

        NonEssentialProductsUIConfig nonEssentialProductsUIConfig = mapper.map(ancillaryConfiguration);

        assertFalse(nonEssentialProductsUIConfig.getPrechecked());
    }

}
