package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrl;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrlType;
import com.odigeo.dapi.client.InsuranceConditionsUrlType;
import com.odigeo.dapi.client.InsuranceUrl;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class NonEssentialProductUrlMapperTest {

    private static final String URL = "URL";

    private NonEssentialProductUrlMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new NonEssentialProductUrlMapper();
    }

    @Test
    public void mapShouldReturnSimpleUrl() {
        InsuranceUrl insuranceUrl = new InsuranceUrl();
        insuranceUrl.setUrl(URL);
        insuranceUrl.setType(InsuranceConditionsUrlType.BASIC);

        NonEssentialProductUrl nonEssentialProductUrl = mapper.map(insuranceUrl);

        assertEquals(nonEssentialProductUrl.getUrl(), URL);
        assertEquals(nonEssentialProductUrl.getUrlType(), NonEssentialProductUrlType.SIMPLE);
    }
    @Test
    public void mapShouldReturnExtendedUrl() {
        InsuranceUrl insuranceUrl = new InsuranceUrl();
        insuranceUrl.setUrl(URL);
        insuranceUrl.setType(InsuranceConditionsUrlType.EXTENDED);

        NonEssentialProductUrl nonEssentialProductUrl = mapper.map(insuranceUrl);

        assertEquals(nonEssentialProductUrl.getUrl(), URL);
        assertEquals(nonEssentialProductUrl.getUrlType(), NonEssentialProductUrlType.EXTENDED);
    }
}
