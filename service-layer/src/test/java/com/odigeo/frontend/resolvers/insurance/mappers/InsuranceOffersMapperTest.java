package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferType;
import com.odigeo.insurance.api.last.insurance.InsuranceOffers;
import com.odigeo.insurance.api.last.insurance.offer.BundleInsuranceOffer;
import com.odigeo.insurance.api.last.insurance.offer.SimpleInsuranceOffer;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class InsuranceOffersMapperTest {

    private static final InsuranceOfferType INSURANCE_OFFER_TYPE = new InsuranceOfferType();

    @Mock
    private InsuranceOfferMapper insuranceOfferMapper;

    @InjectMocks
    private InsuranceOffersMapper insuranceOffersMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(insuranceOfferMapper.map(Mockito.any())).thenReturn(INSURANCE_OFFER_TYPE);
    }

    @Test
    public void map() {
        InsuranceOffers insuranceOffers = getInsuranceOffers();

        List<InsuranceOfferType> offerProducts = insuranceOffersMapper.map(insuranceOffers);

        assertTrue(offerProducts.size() == 1);
        assertEquals(offerProducts.get(0), INSURANCE_OFFER_TYPE);
    }

    private InsuranceOffers getInsuranceOffers() {
        InsuranceOffers insuranceOffers = new InsuranceOffers();

        insuranceOffers.setOffers(Arrays.asList(new SimpleInsuranceOffer(), new BundleInsuranceOffer()));

        return insuranceOffers;
    }
}
