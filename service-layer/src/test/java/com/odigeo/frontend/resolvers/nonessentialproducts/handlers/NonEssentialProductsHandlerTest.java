package com.odigeo.frontend.resolvers.nonessentialproducts.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductOffer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsOfferResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductIdRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectNonEssentialProductsResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketActionStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketRequest;
import com.odigeo.dapi.client.AvailableProductsResponse;
import com.odigeo.dapi.client.Insurance;
import com.odigeo.dapi.client.InsuranceOffer;
import com.odigeo.dapi.client.InsuranceShoppingItem;
import com.odigeo.dapi.client.Insurances;
import com.odigeo.dapi.client.OtherProductsShoppingItems;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsGroupsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.mappers.NonEssentialProductMapper;
import com.odigeo.frontend.resolvers.nonessentialproducts.mappers.NonEssentialProductOfferMapper;
import com.odigeo.frontend.resolvers.nonessentialproducts.mappers.SelectNonEssentialProductsResponseMapper;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class NonEssentialProductsHandlerTest {

    private static final String POLICY = "POLICY";
    private static final String ID = "ID";
    private static final long BOOKING_ID = 12L;

    @Mock
    private NonEssentialProductMapper nonEssentialProductMapper;
    @Mock
    private NonEssentialProductOfferMapper nonEssentialProductOfferMapper;
    @Mock
    private NonEssentialProductsGroupsConfiguration nonEssentialProductsGroupsConfiguration;
    @Mock
    private ResolverContext context;
    @Mock
    private Insurances insurances;
    @Mock
    private InsuranceOffer insuranceOffer;
    @Mock
    private VisitInformation visitInformation;
    @InjectMocks
    private NonEssentialProductsHandler nonEssentialProductsHandler;
    @Mock
    private SelectNonEssentialProductsResponseMapper selectNonEssentialProductsResponseMapper;
    @Mock
    private UpdateProductsInShoppingBasketRequest updateProductsRequest;
    @Mock
    private ShoppingCartHandler shoppingCartHandler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        when(context.getVisitInformation()).thenReturn(visitInformation);
    }

    @Test
    public void getAllNonEssentialProductsOffersShouldReturnEmptyGuaranteesAndRebooking() {
        AvailableProductsResponse availableProductsResponse = new AvailableProductsResponse();
        availableProductsResponse.setInsurances(insurances);

        NonEssentialProductsOfferResponse allNonEssentialProductsOffers = nonEssentialProductsHandler.getAllNonEssentialProductsOffers(1L, context);

        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getGuarantees()));
        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getRebooking()));
        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getServiceOptions()));
        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getInsurances()));
        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getAdditions()));
        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getFareRules()));
    }

    @Test
    public void getAllNonEssentialProductsOffers() {
        AvailableProductsResponse availableProductsResponse = getAvailableInsurances();
        when(nonEssentialProductsGroupsConfiguration.isRebooking(POLICY)).thenReturn(Boolean.TRUE);
        when(nonEssentialProductsGroupsConfiguration.isGuarantee(POLICY)).thenReturn(Boolean.FALSE);
        NonEssentialProduct nonEssentialProduct = new NonEssentialProduct();
        when(nonEssentialProductMapper.map(insuranceOffer, visitInformation)).thenReturn(nonEssentialProduct);
        NonEssentialProductOffer nonEssentialProductOffer = new NonEssentialProductOffer();
        nonEssentialProductOffer.setProduct(nonEssentialProduct);
        when(nonEssentialProductOfferMapper.map(insuranceOffer, visitInformation)).thenReturn(nonEssentialProductOffer);
        when(shoppingCartHandler.getAvailableProductsFromDapi(any(), any())).thenReturn(availableProductsResponse);

        NonEssentialProductsOfferResponse allNonEssentialProductsOffers = nonEssentialProductsHandler.getAllNonEssentialProductsOffers(BOOKING_ID, context);

        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getGuarantees()));
        assertTrue(allNonEssentialProductsOffers.getRebooking().size() == 1);
        assertEquals(allNonEssentialProductsOffers.getRebooking().get(0), nonEssentialProductOffer);
        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getServiceOptions()));
        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getInsurances()));
        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getAdditions()));
        assertTrue(CollectionUtils.isEmpty(allNonEssentialProductsOffers.getFareRules()));
    }

    @Test
    public void getSelectNonEssentialProductsResponseTest() {
        List<String> productsPolicies = Collections.singletonList(POLICY);

        List<ProductId> productIdList = Collections.singletonList(new ProductId(POLICY, ProductType.INSURANCE));

        SelectNonEssentialProductsResponse productsResponse = new SelectNonEssentialProductsResponse(productIdList);

        when(selectNonEssentialProductsResponseMapper.map(productsPolicies)).thenReturn(productsResponse);

        SelectNonEssentialProductsResponse response = nonEssentialProductsHandler.getSelectNonEssentialProductsResponse(productsPolicies);

        assertEquals(response.getProductId().size(), 1);
        assertEquals(response.getProductId().get(0).getId(), POLICY);
        assertEquals(ProductType.INSURANCE, response.getProductId().get(0).getType());
    }

    private ShoppingCartSummaryResponse configureShoppingCartSummaryResponse() {
        InsuranceShoppingItem insuranceShoppingItem = new InsuranceShoppingItem();
        insuranceShoppingItem.setId(ID);
        OtherProductsShoppingItems otherProductsShoppingItems = new OtherProductsShoppingItems();
        otherProductsShoppingItems.getInsuranceShoppingItems().addAll(Collections.singletonList(insuranceShoppingItem));
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setOtherProductsShoppingItemContainer(otherProductsShoppingItems);
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = new ShoppingCartSummaryResponse();
        shoppingCartSummaryResponse.setShoppingCart(shoppingCart);
        return shoppingCartSummaryResponse;
    }

    @Test
    public void getNonEssentialProductsActionStatus() {
        List<String> nonEssentialProductsIdsToRemove = Collections.singletonList(ID);
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = configureShoppingCartSummaryResponse();
        AvailableProductsResponse availableProductsResponse = getAvailableInsurances();

        List<UpdateProductsInShoppingBasketActionStatus> response = nonEssentialProductsHandler.getNonEssentialProductsActionStatus(nonEssentialProductsIdsToRemove, shoppingCartSummaryResponse, availableProductsResponse);

        assertEquals(response.size(), 1);
        assertEquals(response.get(0).getProductId().getId(), POLICY);
        assertTrue(response.get(0).getSuccess());
    }

    @Test
    public void getNonEssentialProductsActionStatusNoProductAdded() {
        List<String> nonEssentialProductsIdsToRemove = Collections.emptyList();
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = configureShoppingCartSummaryResponse();
        AvailableProductsResponse availableProductsResponse = getAvailableInsurances();

        List<UpdateProductsInShoppingBasketActionStatus> response = nonEssentialProductsHandler.getNonEssentialProductsActionStatus(nonEssentialProductsIdsToRemove, shoppingCartSummaryResponse, availableProductsResponse);

        assertEquals(response.size(), 0);
    }

    @Test
    public void getNonEssentialProductsActionStatusNullParams() {
        List<UpdateProductsInShoppingBasketActionStatus> response = nonEssentialProductsHandler.getNonEssentialProductsActionStatus(null, null, null);
        assertEquals(response.size(), 0);
    }

    @Test
    public void getNonEssentialProductsIdsToRemove() {
        List<ProductIdRequest> productsToRemove = Collections.singletonList(new ProductIdRequest(POLICY, ProductType.INSURANCE));
        AvailableProductsResponse availableProductsResponse = getAvailableInsurances();

        when(updateProductsRequest.getProductsToRemove()).thenReturn(productsToRemove);

        List<String> result = nonEssentialProductsHandler.getNonEssentialProductsIdsToRemove(updateProductsRequest, availableProductsResponse);

        assertEquals(result.size(), 1);
        assertEquals(result.get(0), ID);
    }

    @Test
    public void getNonEssentialProductsIdsToRemoveEmptyRequest() {
        when(updateProductsRequest.getProductsToRemove()).thenReturn(Collections.emptyList());
        AvailableProductsResponse availableProductsResponse = getAvailableInsurances();

        List<String> result = nonEssentialProductsHandler.getNonEssentialProductsIdsToRemove(updateProductsRequest, availableProductsResponse);

        assertEquals(result.size(), 0);
    }

    @Test
    public void getNonEssentialProductsIdsToAdd() {
        List<ProductIdRequest> productsToAdd = Collections.singletonList(new ProductIdRequest(POLICY, ProductType.INSURANCE));
        AvailableProductsResponse availableProductsResponse = getAvailableInsurances();

        when(updateProductsRequest.getProductsToAdd()).thenReturn(productsToAdd);

        List<String> result = nonEssentialProductsHandler.getNonEssentialProductsIdsToAdd(updateProductsRequest, availableProductsResponse);

        assertEquals(result.size(), 1);
        assertEquals(result.get(0), ID);
    }

    @Test
    public void getNonEssentialProductsIdsToAddEmptyRequest() {
        when(updateProductsRequest.getProductsToAdd()).thenReturn(Collections.emptyList());
        AvailableProductsResponse availableProductsResponse = getAvailableInsurances();

        List<String> result = nonEssentialProductsHandler.getNonEssentialProductsIdsToAdd(updateProductsRequest, availableProductsResponse);

        assertEquals(result.size(), 0);
    }

    private AvailableProductsResponse getAvailableInsurances() {
        AvailableProductsResponse availableProductsResponse = new AvailableProductsResponse();
        Insurance insurance = new Insurance();
        insurance.setPolicy(POLICY);
        when(insuranceOffer.getInsurances()).thenReturn(Collections.singletonList(insurance));
        when(insuranceOffer.getId()).thenReturn(ID);
        when(insurances.getInsuranceOffers()).thenReturn(Collections.singletonList(insuranceOffer));
        availableProductsResponse.setInsurances(insurances);
        return availableProductsResponse;
    }

}
