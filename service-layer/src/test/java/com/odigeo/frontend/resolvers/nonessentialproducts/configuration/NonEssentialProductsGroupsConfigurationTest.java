package com.odigeo.frontend.resolvers.nonessentialproducts.configuration;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class NonEssentialProductsGroupsConfigurationTest {

    NonEssentialProductsGroupsConfiguration nonEssentialProductsGroupsConfiguration;

    @BeforeMethod
    public void setUp() {
        ConfigurationEngine.init();
    }

    @Test
    public void testLoad() {
        nonEssentialProductsGroupsConfiguration = ConfigurationEngine.getInstance(NonEssentialProductsGroupsConfiguration.class);
        assertTrue(nonEssentialProductsGroupsConfiguration.isGuarantee("GUARANTEE"));
        assertTrue(nonEssentialProductsGroupsConfiguration.isRebooking("REBOOKING"));
    }
}
