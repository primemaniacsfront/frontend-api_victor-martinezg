package com.odigeo.frontend.resolvers.geolocation.suggestions;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeolocationSuggestionsResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.geolocation.suggestions.handlers.GeolocationSuggestionsHandler;
import com.odigeo.frontend.resolvers.geolocation.suggestions.models.GeolocationSuggestionsRequestDTO;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class GeolocationSuggestionsResolverTest {

    private static final String GEOLOCATION_SUGGESTIONS_QUERY = "geolocationSuggestions";

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private GeolocationSuggestionsHandler handler;
    @InjectMocks
    private GeolocationSuggestionsResolver resolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visit);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(GEOLOCATION_SUGGESTIONS_QUERY));
    }

    @Test
    public void testGetGeolocationSuggestions() {
        GeolocationSuggestionsRequest request = mock(GeolocationSuggestionsRequest.class);
        GeolocationSuggestionsRequestDTO requestDTO = mock(GeolocationSuggestionsRequestDTO.class);
        GeolocationSuggestionsResponse response = mock(GeolocationSuggestionsResponse.class);

        when(jsonUtils.fromDataFetching(env, GeolocationSuggestionsRequest.class)).thenReturn(request);
        when(handler.buildGeolocationSuggestionsRequest(request, visit)).thenReturn(requestDTO);
        when(handler.getGeolocationSuggestions(requestDTO)).thenReturn(response);

        assertSame(resolver.getGeolocationSuggestions(env), response);
    }
}
