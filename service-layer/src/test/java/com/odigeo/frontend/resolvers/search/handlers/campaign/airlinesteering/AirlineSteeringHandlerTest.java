package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering;

import com.google.inject.Guice;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.search.handlers.SearchDebugHandler;
import com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering.types.AirlineSteeringPosition;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.odigeo.frontend.monitoring.MeasureConstants.SEARCH_AIRLINE_STEERING_TOTAL_TIME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AirlineSteeringHandlerTest extends AirlineSteeringAbstractTest {

    @Mock
    private AirlineSteeringManager airlineSteeringManager;

    @Mock
    private VisitInformation visit;

    @Mock
    private MetricsHandler metricsHandler;

    @Mock
    private Logger logger;

    @Mock
    private ResolverContext context;

    @Mock
    private SearchDebugHandler debugHandler;

    private SearchResponseDTO searchResponseDTO;

    private AirlineSteeringHandler handler;

    static final String QATAR_AIRLINE = "QR";
    static final String AIREUROPA_AIRLINE = "UX";

    private static final AirlineSteeringPosition PARTITION_4 = AirlineSteeringPosition.HIGH;
    private static final AirlineSteeringPosition PARTITION_3 = AirlineSteeringPosition.MEDIUM;
    private static final AirlineSteeringPosition PARTITION_2 = AirlineSteeringPosition.LOW;
    private static final AirlineSteeringPosition PARTITION_1 = AirlineSteeringPosition.NONE;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new AirlineSteeringHandler();
        Guice.createInjector(handler -> {
            handler.bind(AirlineSteeringManager.class).toProvider(() -> airlineSteeringManager);
            handler.bind(Logger.class).toProvider(() -> logger);
            handler.bind(MetricsHandler.class).toProvider(() -> metricsHandler);
            handler.bind(SearchDebugHandler.class).toProvider(() -> debugHandler);
        }).injectMembers(handler);

        searchResponseDTO = new SearchResponseDTO();
        searchResponseDTO.setDefaultFeeType(FEE_TYPE);
    }

    @Test
    public void testNotActivated() {
        initializeTestConfiguration(QATAR_AIRLINE, PARTITION_1);

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("0", 0.99999, AIREUROPA_AIRLINE, 103));
        itineraries.add(searchItineraryDTOMock("1", 0.99999, QATAR_AIRLINE, 101));

        handler.boostingAirline(searchResponseDTO, context);

        for (int i = 0; i < itineraries.size(); i++) {
            assertEquals(BigDecimal.valueOf(0.99999), itineraries.get(i).getMeRating());
        }

    }

    @Test
    public void testSteeringWithRatingNull() {
        initializeTestConfiguration(QATAR_AIRLINE, PARTITION_2);
        when(airlineSteeringManager.getAirlines()).thenReturn(Collections.singletonList(QATAR_AIRLINE));

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("2", 0.88888, AIREUROPA_AIRLINE, 104));
        itineraries.add(searchItineraryDTOMock("1", 0.88888, QATAR_AIRLINE, 105));
        itineraries.add(searchItineraryDTOMock("0", 0.99999, AIREUROPA_AIRLINE, 103));
        itineraries.add(searchItineraryDTOMock("3", 0.99999, QATAR_AIRLINE, 101));
        itineraries.get(3).setMeRating(null);

        launchBoostingTest(itineraries);

    }

    @Test
    public void testSteeringOnePosition() {
        initializeTestConfiguration(QATAR_AIRLINE, PARTITION_2);
        when(airlineSteeringManager.getAirlines()).thenReturn(Collections.singletonList(QATAR_AIRLINE));

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("3", 0.88888, AIREUROPA_AIRLINE, 104));
        itineraries.add(searchItineraryDTOMock("1", 0.88888, QATAR_AIRLINE, 105));
        itineraries.add(searchItineraryDTOMock("0", 0.99999, AIREUROPA_AIRLINE, 103));
        itineraries.add(searchItineraryDTOMock("2", 0.77777, QATAR_AIRLINE, 104));

        launchBoostingTest(itineraries);


    }

    @Test
    public void testSteeringThreePosition() {
        initializeTestConfiguration(QATAR_AIRLINE, PARTITION_3);
        when(airlineSteeringManager.getAirlines()).thenReturn(Collections.singletonList(QATAR_AIRLINE));

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("2", 0.99999, AIREUROPA_AIRLINE, 103));
        itineraries.add(searchItineraryDTOMock("3", 0.88888, AIREUROPA_AIRLINE, 104));
        itineraries.add(searchItineraryDTOMock("0", 0.88888, QATAR_AIRLINE, 105));
        itineraries.add(searchItineraryDTOMock("1", 0.77777, QATAR_AIRLINE, 104));

        launchBoostingTest(itineraries);

    }

    @Test
    public void testSteeringFivePosition() {
        initializeTestConfiguration(QATAR_AIRLINE, PARTITION_4);
        when(airlineSteeringManager.getAirlines()).thenReturn(Collections.singletonList(QATAR_AIRLINE));

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("1", 0.99999, AIREUROPA_AIRLINE, 100));
        itineraries.add(searchItineraryDTOMock("2", 0.99999, AIREUROPA_AIRLINE, 103));
        itineraries.add(searchItineraryDTOMock("3", 0.88888, AIREUROPA_AIRLINE, 104));
        itineraries.add(searchItineraryDTOMock("4", 0.88888, AIREUROPA_AIRLINE, 105));
        itineraries.add(searchItineraryDTOMock("6", 0.88888, AIREUROPA_AIRLINE, 106));
        itineraries.add(searchItineraryDTOMock("0", 0.88888, QATAR_AIRLINE, 107));
        itineraries.add(searchItineraryDTOMock("7", 0.88888, AIREUROPA_AIRLINE, 108));
        itineraries.add(searchItineraryDTOMock("8", 0.77777, AIREUROPA_AIRLINE, 101));
        itineraries.add(searchItineraryDTOMock("9", 0.77777, AIREUROPA_AIRLINE, 102));
        itineraries.add(searchItineraryDTOMock("10", 0.77777, AIREUROPA_AIRLINE, 102));
        itineraries.add(searchItineraryDTOMock("5", 0.77777, QATAR_AIRLINE, 104));

        launchBoostingTest(itineraries);

    }

    @Test
    public void testSteeringOnePositionWithPriceOutPercentage() {
        initializeTestConfiguration(QATAR_AIRLINE, PARTITION_2);
        when(airlineSteeringManager.getAirlines()).thenReturn(Collections.singletonList(QATAR_AIRLINE));

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("1", 0.88888, AIREUROPA_AIRLINE, 104));
        itineraries.add(searchItineraryDTOMock("2", 0.88888, QATAR_AIRLINE, 150));
        itineraries.add(searchItineraryDTOMock("0", 0.99999, AIREUROPA_AIRLINE, 103));
        itineraries.add(searchItineraryDTOMock("3", 0.77777, QATAR_AIRLINE, 150));

        launchBoostingTest(itineraries);

    }

    @Test
    public void testSteeringItinerariesWithOutPrice() {
        initializeTestConfiguration(QATAR_AIRLINE, PARTITION_2);
        when(airlineSteeringManager.getAirlines()).thenReturn(Collections.singletonList(QATAR_AIRLINE));

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("0", 0.99999, AIREUROPA_AIRLINE, 104));
        itineraries.add(searchItineraryDTOMock("1", 0.99999, QATAR_AIRLINE, 103));
        itineraries.stream().forEach(i -> i.setFees(Collections.emptyList()));

        launchBoostingTest(itineraries);
        verify(logger, times(1)).warning(any(), anyString());

    }

    @Test
    public void testResponseWithoutActivatedAirlines() {
        initializeTestConfiguration(QATAR_AIRLINE, PARTITION_2);

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("0", 0.99999, AIREUROPA_AIRLINE, 104));
        searchResponseDTO.setItineraries(itineraries);
        handler.boostingAirline(searchResponseDTO, context);

        verify(metricsHandler, times(0)).startMetric(SEARCH_AIRLINE_STEERING_TOTAL_TIME);
        verify(metricsHandler, times(0)).stopMetric(SEARCH_AIRLINE_STEERING_TOTAL_TIME, visit);
        verify(debugHandler, times(0)).addQaModeMeRatingInfo(context, itineraries);
    }

    @Test
    public void testAddDebugInfo() {
        initializeTestConfiguration(QATAR_AIRLINE, PARTITION_2);
        when(airlineSteeringManager.getAirlines()).thenReturn(Collections.singletonList(QATAR_AIRLINE));

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("0", 0.99999, QATAR_AIRLINE, 104));
        launchBoostingTest(itineraries);

        verify(debugHandler).addQaModeMeRatingInfo(context, itineraries);
    }

    private void launchBoostingTest(List<SearchItineraryDTO> itineraries) {
        searchResponseDTO.setItineraries(itineraries);

        handler.boostingAirline(searchResponseDTO, context);

        Collections.sort(itineraries, new ItinerarySteeringComparator(FEE_TYPE));

        for (int i = 0; i < itineraries.size(); i++) {
            assertEquals(String.valueOf(i), itineraries.get(i).getId());
        }

        verify(metricsHandler, times(1)).startMetric(SEARCH_AIRLINE_STEERING_TOTAL_TIME);
        verify(metricsHandler, times(1)).stopMetric(SEARCH_AIRLINE_STEERING_TOTAL_TIME, visit);

    }

    private void initializeTestConfiguration(String airline, AirlineSteeringPosition boostingPosition) {
        when(context.getVisitInformation()).thenReturn(visit);
        when(airlineSteeringManager.getAirlineSteeringPosition(visit)).thenReturn(boostingPosition);
        when(airlineSteeringManager.getControlPercentagePrice()).thenReturn(BigDecimal.TEN);
    }
}
