package com.odigeo.frontend.resolvers.membershipoffer;

import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.membership.offer.api.MembershipOfferService;
import com.odigeo.membership.offer.api.exception.DataNotFoundException;
import com.odigeo.membership.offer.api.exception.InvalidParametersException;
import com.odigeo.membership.offer.api.exception.MembershipOfferServiceException;
import com.odigeo.membership.offer.api.request.MembershipOfferRequest;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MembershipOfferHandlerTest {
    private static final String OFFER_ID = "offerId";
    public static final VerificationMode ONCE = times(1);

    @Mock
    private MembershipOfferService membershipOfferService;
    @Mock
    private MembershipOfferRequest membershipOfferRequest;
    @Mock
    private MembershipSubscriptionOffer subscriptionOffer;
    @Mock
    private MembershipSubscriptionOffers subscriptionOffers;
    @Mock
    private MetricsHandler metricsHandler;
    @InjectMocks
    private MembershipOfferHandler membershipOfferHandler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetSubscriptionOffer() throws Exception {
        when(membershipOfferService.createMembershipSubscriptionOffer(eq(membershipOfferRequest)))
                .thenReturn(subscriptionOffer);

        assertEquals(membershipOfferHandler.getSubscriptionOffer(membershipOfferRequest), subscriptionOffer);
        verify(metricsHandler, ONCE).trackCounter(MeasureConstants.MOS_CREATE_OFFER);

    }

    @Test
    public void testGetSubscriptionOffers() throws Exception {
        when(membershipOfferService.createMembershipSubscriptionOffers(eq(membershipOfferRequest)))
                .thenReturn(subscriptionOffers);

        assertEquals(membershipOfferHandler.getSubscriptionOffers(membershipOfferRequest), subscriptionOffers);
        verify(metricsHandler, ONCE).trackCounter(MeasureConstants.MOS_CREATE_OFFERS);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetSubscriptionOfferThrowsInvalidParameter() throws Exception {
        when(membershipOfferService.createMembershipSubscriptionOffer(any(MembershipOfferRequest.class)))
                .thenThrow(InvalidParametersException.class);

        membershipOfferHandler.getSubscriptionOffer(membershipOfferRequest);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetSubscriptionOffersThrowsInvalidParameter() throws Exception {
        when(membershipOfferService.createMembershipSubscriptionOffers(any(MembershipOfferRequest.class)))
                .thenThrow(InvalidParametersException.class);

        membershipOfferHandler.getSubscriptionOffers(membershipOfferRequest);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetSubscriptionOfferThrowsMembershipException() throws Exception {
        when(membershipOfferService.createMembershipSubscriptionOffer(any(MembershipOfferRequest.class)))
                .thenThrow(MembershipOfferServiceException.class);

        membershipOfferHandler.getSubscriptionOffer(membershipOfferRequest);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetSubscriptionOffersThrowsMembershipException() throws Exception {
        when(membershipOfferService.createMembershipSubscriptionOffers(any(MembershipOfferRequest.class)))
                .thenThrow(MembershipOfferServiceException.class);

        membershipOfferHandler.getSubscriptionOffers(membershipOfferRequest);
    }

    @Test
    public void testGetSubscriptionOfferId() throws Exception {
        when(membershipOfferService.getMembershipSubscriptionOffer(eq(OFFER_ID))).thenReturn(subscriptionOffer);
        assertEquals(membershipOfferHandler.getSubscriptionOffer(OFFER_ID), subscriptionOffer);
        verify(metricsHandler, ONCE).trackCounter(MeasureConstants.MOS_GET_OFFER);
    }

    @Test
    public void testGetSubscriptionOfferIdThrowsMembershipException() throws Exception {
        when(membershipOfferService.getMembershipSubscriptionOffer(any()))
                .thenThrow(MembershipOfferServiceException.class);

        assertNull(membershipOfferHandler.getSubscriptionOffer(OFFER_ID));
    }

    @Test
    public void testGetSubscriptionOfferIdThrowsDataException() throws Exception {
        when(membershipOfferService.getMembershipSubscriptionOffer(any())).thenThrow(DataNotFoundException.class);
        assertNull(membershipOfferHandler.getSubscriptionOffer(OFFER_ID));
    }
}
