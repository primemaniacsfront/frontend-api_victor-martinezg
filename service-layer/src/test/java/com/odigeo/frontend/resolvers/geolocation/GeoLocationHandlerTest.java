package com.odigeo.frontend.resolvers.geolocation;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeoLocation;
import com.odigeo.frontend.commons.mappers.GeoCoordinatesMapper;
import com.odigeo.frontend.commons.util.GeoUtils;
import com.odigeo.frontend.resolvers.accommodation.commons.exceptions.CityNotFoundException;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoServiceException;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Coordinates;
import com.odigeo.geoapi.v4.responses.Country;
import com.odigeo.geoapi.v4.responses.GeoNode;
import com.odigeo.geoapi.v4.utils.LocalizedText;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class GeoLocationHandlerTest {

    @Mock
    private GeoService geoService;
    @Mock
    private GeoUtils geoUtils;
    @Mock
    private GeoNode geoNode;
    @Mock
    private GeoCoordinatesMapper geoCoordinatesMapper;
    @Mock
    private City city;
    @Mock
    private LocalizedText geonodeName;

    @InjectMocks
    private GeoLocationHandler testClass;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(geonodeName.getText(any(Locale.class), any(Locale.class))).thenReturn("GeonodeName");
    }


    @Test
    public void testComputeDistance() {
        Double inputDistance = 1.0;
        when(geoUtils.calculateDistance(any(), any())).thenReturn(inputDistance);
        when(geoNode.getCoordinates()).thenReturn(new Coordinates());
        Double distance = testClass.computeDistanceFromGeonode(geoNode, new GeoCoordinatesDTO());
        assertEquals(distance, inputDistance);
    }

    @Test
    public void testGetCityGeonode() throws GeoNodeNotFoundException, CityNotFoundException {
        when(geoService.getCityByGeonodeId(anyInt())).thenReturn(city);
        City cityGeonode = testClass.getCityGeonode(1, null);
        assertEquals(cityGeonode, city);
    }

    @Test
    public void testGetCityGeonodeWithIata() throws GeoNodeNotFoundException, CityNotFoundException {
        when(geoService.getCityByIataCode(anyString())).thenReturn(city);
        City cityGeonode = testClass.getCityGeonode(null, "BCN");
        assertEquals(cityGeonode, city);
    }

    @Test
    public void testRetrieveGeoLocation() throws GeoNodeNotFoundException, GeoServiceException {
        City city = buildCity();
        Country country = city.getCountry();
        when(geoService.getGeonode(anyInt())).thenReturn(city);
        GeoLocation geoLocation = testClass.retrieveGeoLocation(1, "es_ES");
        assertNotNull(geoLocation);
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.City cityResponse = geoLocation.getCity();
        assertNotNull(cityResponse);
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country countryResponse = geoLocation.getCountry();
        assertNotNull(countryResponse);

        assertEquals(new Integer(city.getGeoNodeId()), cityResponse.getId());
        assertEquals(country.getGeoNodeId(), countryResponse.getId());
    }

    private City buildCity() {
        City city = new City();
        city.setIataCode("BCN");
        city.setGeoNodeId(1);
        city.setName(geonodeName);
        city.setCountry(buildCountry());
        return city;
    }

    private Country buildCountry() {
        Country country = new Country();
        country.setName(geonodeName);
        country.setGeoNodeId(2);
        country.setCountryCode("ES");
        return country;
    }

}
