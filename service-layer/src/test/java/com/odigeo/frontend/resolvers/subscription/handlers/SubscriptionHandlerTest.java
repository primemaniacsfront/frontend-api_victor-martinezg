package com.odigeo.frontend.resolvers.subscription.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionFdoResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import com.odigeo.frontend.resolvers.search.handlers.alert.PointOfSaleMapper;
import com.odigeo.frontend.services.CrmSubscriptionService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SubscriptionHandlerTest {

    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private PointOfSaleMapper posWebsiteMapper;
    @Mock
    private CrmSubscriptionService crmSubscriptionService;
    @InjectMocks
    private SubscriptionHandler subscriptionHandler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testSubscribeToFdo() {
        String email = "test@gmail.com";
        String locale = "locale";
        Site site = mock(Site.class);
        Brand brand = mock(Brand.class);
        WebInterface webInterface = mock(WebInterface.class);
        SubscriptionFdoResponse fdoResponse = new SubscriptionFdoResponse(email);

        when(context.getVisitInformation()).thenReturn(visit);
        when(visit.getSite()).thenReturn(site);
        when(visit.getBrand()).thenReturn(brand);
        when(visit.getWebInterface()).thenReturn(webInterface);
        when(visit.getLocale()).thenReturn(locale);
        assertEquals(subscriptionHandler.subscribeToFdo(email, context).getEmail(), fdoResponse.getEmail());
    }
}
