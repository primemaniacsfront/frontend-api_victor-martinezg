package com.odigeo.frontend.resolvers.prime.pages;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPageName;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.services.UserDescriptionService;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MembershipManagerPageTest {
    private static final String RENEWAL_STATUS_DISABLED = "DISABLED";

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private SiteVariations siteVariations;
    @Mock
    private Membership membership;
    @Mock
    private UserDescriptionService userDescriptionService;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private ResolverContext context;

    @InjectMocks
    private MembershipManagerPage membershipManagerPage;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(SiteVariations.class).toProvider(() -> siteVariations);
            binder.bind(UserDescriptionService.class).toProvider(() -> userDescriptionService);
        });
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
        when(env.getContext()).thenReturn(context);
        when(graphQLContext.get(VisitInformation.class)).thenReturn(visitInformation);
        when(graphQLContext.get("Membership")).thenReturn(membership);
    }

    @Test
    public void testGetMembershipFunnelPageWhenRecapture() {
        when(siteVariations.isMembershipRecaptureM(eq(visitInformation))).thenReturn(Boolean.TRUE);
        when(membership.getAutoRenewalStatus()).thenReturn(RENEWAL_STATUS_DISABLED);
        when(userDescriptionService.isUserLogged(context)).thenReturn(Boolean.TRUE);
        MembershipPageName page = membershipManagerPage.getPage(env);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_PAGE_RECAPTURE);
        assertEquals(page, MembershipPageName.RENEWAL_RECAPTURE);
    }

    @Test
    public void testGetMembershipFunnelPageWhenRecaptureAutoApply() {
        when(siteVariations.isMembershipRecaptureM(eq(visitInformation))).thenReturn(Boolean.TRUE);
        when(membership.getAutoRenewalStatus()).thenReturn(RENEWAL_STATUS_DISABLED);
        when(membership.getIsAutoApply()).thenReturn(Boolean.TRUE);
        when(userDescriptionService.isUserLogged(context)).thenReturn(Boolean.FALSE);
        MembershipPageName page = membershipManagerPage.getPage(env);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_PAGE_RECAPTURE);
        assertEquals(page, MembershipPageName.RENEWAL_RECAPTURE);
    }

    @Test
    public void testGetMembershipFunnelDefault() {
        assertNull(membershipManagerPage.getPage(env));
    }
}
