package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.TripType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class MirTripTypeMapperTest {

    @Mock
    private SearchRequest searchRequestDTO;

    private MirTripTypeMapper mapper;

    @BeforeClass
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new MirTripTypeMapper();
    }

    @Test
    public void testMapOneWay() {
        when(searchRequestDTO.getTripType())
            .thenReturn(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType.ONE_WAY);
        assertEquals(mapper.map(searchRequestDTO), TripType.ONE_WAY);
    }

    @Test
    public void testMapOther() {
        when(searchRequestDTO.getTripType()).thenReturn(null);
        assertEquals(mapper.map(searchRequestDTO), TripType.ROUND_TRIP);
    }
}
