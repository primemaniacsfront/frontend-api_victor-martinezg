package com.odigeo.frontend.resolvers.checkout.handlers;

import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteractionId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteraction;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteractionIdInput;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteractionRequest;
import com.odigeo.frontend.services.checkout.UserPaymentInteractionService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class UserPaymentInteractionHandlerTest {

    private static final String UUID = "a93a9762-ff73-46cb-85e3-a2cd2dd4c7e9";
    @Mock
    private UserPaymentInteractionRequest request;
    @Mock
    private UserPaymentInteractionService paymentInteractionService;
    @Mock
    private UserPaymentInteraction userPaymentInteraction;
    @InjectMocks
    private UserPaymentInteractionHandler handler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(request.getUserPaymentInteractionId()).thenReturn(new UserPaymentInteractionIdInput(UUID));
        UserPaymentInteractionId userPaymentInteractionId = new UserPaymentInteractionId(request.getUserPaymentInteractionId().getId());
        when(paymentInteractionService.retrieveUserPaymentInteraction(userPaymentInteractionId)).thenReturn(userPaymentInteraction);
    }

    @Test
    public void testRetrieveUserPaymentInteraction() {
        UserPaymentInteraction userPaymentInteraction = handler.retrieveUserPaymentInteraction(request);
        assertNotNull(userPaymentInteraction);
    }
}
