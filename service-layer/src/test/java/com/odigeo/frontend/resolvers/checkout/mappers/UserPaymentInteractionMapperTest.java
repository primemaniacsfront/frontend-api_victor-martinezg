package com.odigeo.frontend.resolvers.checkout.mappers;

import com.edreamsodigeo.collections.userpaymentinteraction.contract.model.UserPaymentInteraction;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Param;
import com.odigeo.dapi.client.InteractionType;
import com.odigeo.dapi.client.UserInteractionNeededResponse;
import org.apache.commons.collections.map.HashedMap;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class UserPaymentInteractionMapperTest {

    private static final String KEY = "key";
    private static final String VALUE = "value";
    private static final String JS_SNIPPET = "snippet";

    @Mock
    private UserInteractionNeededResponse response;
    @Mock
    private UserInteractionNeededResponse.Parameters params;
    @Mock
    private UserInteractionNeededResponse.Parameters.Entry entry;
    @Mock
    private UserPaymentInteraction userPaymentInteraction;
    @Mock
    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserPaymentInteraction userPaymentInteractionContract;

    private Map<String, String> mapParameters = new HashedMap();
    private UserPaymentInteractionMapper testClass;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        testClass = new UserPaymentInteractionMapperImpl();
    }

    @Test
    public void testMapFromDapi() {
        assertNull(testClass.mapFromDapi(null));

        when(response.getJavascriptSnippet()).thenReturn(JS_SNIPPET);
        when(response.getInteractionType()).thenReturn(InteractionType.JAVASCRIPT);
        UserPaymentInteraction userPaymentInteraction = testClass.mapFromDapi(response);
        assertEquals(userPaymentInteraction.getJavascriptSnippet(), JS_SNIPPET);
        assertEquals(userPaymentInteraction.getInteractionType(), InteractionType.JAVASCRIPT.value());
    }

    @Test
    public void testMapParameters() {
        assertTrue(testClass.mapParameters(params).isEmpty());

        when(entry.getKey()).thenReturn(KEY);
        when(entry.getValue()).thenReturn(VALUE);
        when(params.getEntry()).thenReturn(Collections.singletonList(entry));
        Map<String, String> mapParams = testClass.mapParameters(params);
        assertEquals(mapParams.get(KEY), VALUE);
    }

    @Test
    public void testMapUserPaymentInteractionToContract() {
        assertNotNull(testClass.mapUserPaymentInteractionToContract(userPaymentInteraction));
    }

    @Test
    public void testMapParametersToContract() {
        mapParameters.put(KEY, VALUE);
        List<Param> params = testClass.mapParametersToContract(mapParameters);
        assertEquals(params.get(0).getValue(), mapParameters.get(KEY));
    }
}
