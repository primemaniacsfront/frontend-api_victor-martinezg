package com.odigeo.frontend.resolvers.prime.models;

import bean.test.BeanTest;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowGroup;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowInfo;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowTravel;

public class RetentionFlowInfoTest extends BeanTest<RetentionFlowInfo> {

    @Override
    public RetentionFlowInfo getBean() {
        RetentionFlowInfo bean = new RetentionFlowInfo();
        bean.setFlow(RetentionFlowGroup.BASIC_LONG_WITH_TRIP);
        bean.setTravel(new RetentionFlowTravel());

        return bean;
    }

}
