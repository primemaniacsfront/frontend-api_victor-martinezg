package com.odigeo.frontend.resolvers.shoppingcart.mappers;

import com.odigeo.dapi.client.BaggageApplicationLevel;
import com.odigeo.dapi.client.BaggageConditions;
import com.odigeo.dapi.client.BaggageDescriptor;
import com.odigeo.dapi.client.BaggageType;
import com.odigeo.dapi.client.ItineraryShoppingItem;
import com.odigeo.dapi.client.SegmentTypeIndex;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.TravellerInformationDescription;
import com.odigeo.dapi.client.TravellerType;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class ShoppingCartMapperTest {

    private static final long BOOKING_ID = 12L;

    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private ItineraryShoppingItem shoppingItem;
    @Mock
    private TravellerInformationDescription travellerInfo;
    @Mock
    private BaggageConditions baggageConditions;
    @Mock
    private BaggageDescriptor baggageDescriptor;
    @Mock
    private VisitInformation visit;

    private ShoppingCartMapper mapper;
    private ShoppingCartContext context;
    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCart model;

    @BeforeMethod
    public void init() {
        initMocks(this);
        mapper = new ShoppingCartMapperImpl();
        context = new ShoppingCartContext(visit);
    }

    @Test
    public void testShoppingCartContractToModel() {
        assertNull(mapper.shoppingCartContractToModel(null, context));

        when(shoppingCart.getBookingId()).thenReturn(BOOKING_ID);
        when(shoppingCart.getRequiredTravellerInformation()).thenReturn(null);
        model = mapper.shoppingCartContractToModel(shoppingCart, context);
        assertEquals(model.getBookingId(), BOOKING_ID);
        assertNull(model.getRequiredTravellerInformation());

        when(shoppingCart.getItineraryShoppingItem()).thenReturn(shoppingItem);
        when(shoppingCart.getRequiredTravellerInformation()).thenReturn(Collections.emptyList());
        model = mapper.shoppingCartContractToModel(shoppingCart, context);
    }

    @Test
    public void testShoppingCartTravellerTypes() {
        when(travellerInfo.getTravellerType()).thenReturn(TravellerType.INFANT);
        TravellerInformationDescription travellerChild = mock(TravellerInformationDescription.class);
        when(travellerChild.getTravellerType()).thenReturn(TravellerType.CHILD);
        when(shoppingCart.getRequiredTravellerInformation()).thenReturn(Arrays.asList(travellerInfo, travellerChild));
        model = mapper.shoppingCartContractToModel(shoppingCart, context);
    }

    @Test
    public void testShoppingCartBaggageLevel() {
        when(baggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(baggageDescriptor);
        when(baggageConditions.getBaggageApplicationLevel()).thenReturn(BaggageApplicationLevel.ITINERARY);

        BaggageConditions conditions = mock(BaggageConditions.class);
        when(conditions.getBaggageApplicationLevel()).thenReturn(BaggageApplicationLevel.SECTION);

        when(travellerInfo.getBaggageConditions()).thenReturn(Arrays.asList(baggageConditions, conditions));
        when(shoppingCart.getRequiredTravellerInformation()).thenReturn(Arrays.asList(travellerInfo));
        model = mapper.shoppingCartContractToModel(shoppingCart, context);

    }

    @Test
    public void testShoppingCartBaggageTypes() {
        Map<BaggageType, com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType> bagTypes = Stream.of(
                new AbstractMap.SimpleImmutableEntry<>(BaggageType.BAGGAGE,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType.BAGGAGE),
                new AbstractMap.SimpleImmutableEntry<>(BaggageType.BICYCLE,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType.BICYCLE),
                new AbstractMap.SimpleImmutableEntry<>(BaggageType.GOLF,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType.GOLF),
                new AbstractMap.SimpleImmutableEntry<>(BaggageType.SURF,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType.SURF),
                new AbstractMap.SimpleImmutableEntry<>(BaggageType.INSTRUMENT,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType.INSTRUMENT),
                new AbstractMap.SimpleImmutableEntry<>(BaggageType.SPORT_EQUIPMENT,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType.SPORT_EQUIPMENT),
                new AbstractMap.SimpleImmutableEntry<>(BaggageType.SKI,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType.SKI),
                new AbstractMap.SimpleImmutableEntry<>(BaggageType.SNOWBOARD,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageType.SNOWBOARD))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        when(baggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(baggageDescriptor);
        when(travellerInfo.getBaggageConditions()).thenReturn(Arrays.asList(baggageConditions));
        when(shoppingCart.getRequiredTravellerInformation()).thenReturn(Arrays.asList(travellerInfo));
    }

    @Test
    public void testShoppingCartBaggageSegment() {
        Map<SegmentTypeIndex, com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentTypeIndex> segmentTypes = Stream.of(
                new AbstractMap.SimpleImmutableEntry<>(SegmentTypeIndex.FIRST,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentTypeIndex.FIRST),
                new AbstractMap.SimpleImmutableEntry<>(SegmentTypeIndex.SECOND,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentTypeIndex.SECOND),
                new AbstractMap.SimpleImmutableEntry<>(SegmentTypeIndex.THIRD,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentTypeIndex.THIRD),
                new AbstractMap.SimpleImmutableEntry<>(SegmentTypeIndex.FOURTH,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentTypeIndex.FOURTH),
                new AbstractMap.SimpleImmutableEntry<>(SegmentTypeIndex.FIFTH,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentTypeIndex.FIFTH),
                new AbstractMap.SimpleImmutableEntry<>(SegmentTypeIndex.SIXTH,
                    com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentTypeIndex.SIXTH))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        when(travellerInfo.getBaggageConditions()).thenReturn(Arrays.asList(baggageConditions));
        when(shoppingCart.getRequiredTravellerInformation()).thenReturn(Arrays.asList(travellerInfo));
    }

}
