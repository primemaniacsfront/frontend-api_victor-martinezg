package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.DynPackSearchCriteria;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class AccommodationSearchCriteriaRetrieveHandlerTest {

    @Mock
    private SearchService searchService;
    @Mock
    private SearchResults searchResults;
    @Mock
    private AccommodationSearchCriteria accommodationSearchCriteria;

    @InjectMocks
    private AccommodationSearchCriteriaRetrieveHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetSearchCriteriaDynpack() {
        when(searchService.getSearchResult(anyLong(), isNull(), anyInt())).thenReturn(searchResults);
        DynPackSearchCriteria dynPackSearchCriteria = mock(DynPackSearchCriteria.class);
        when(searchResults.getSearchCriteria()).thenReturn(dynPackSearchCriteria);
        when(dynPackSearchCriteria.getAccommodationSearchCriteria()).thenReturn(accommodationSearchCriteria);
        assertSame(handler.getSearchCriteria(1L, 2), accommodationSearchCriteria);
    }

    @Test
    public void testGetSearchCriteria() {
        when(searchService.getSearchResult(anyLong(), isNull(), anyInt())).thenReturn(searchResults);
        AccommodationSearchCriteria searchCriteria = mock(AccommodationSearchCriteria.class);
        when(searchResults.getSearchCriteria()).thenReturn(searchCriteria);
        assertSame(handler.getSearchCriteria(1L, 2), searchCriteria);
    }

    @Test
    public void getSearchResultsBySearchId() {
        long searchId = 8L;
        String visitCode = "visitCode";

        when(searchService.getSearchResults(searchId, visitCode)).thenReturn(searchResults);
        assertEquals(handler.getSearchResultsBySearchId(searchId, visitCode), searchResults);
    }
}
