package com.odigeo.frontend.resolvers.search.handlers;

import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.math.NumberUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class TicketHandlerTest {

    private static final Integer NO_TICKETS_LEFT_INFORMATION = NumberUtils.INTEGER_MINUS_ONE;

    @Mock
    private LegDTO legDTO1;
    @Mock
    private LegDTO legDTO2;
    @Mock
    private SegmentDTO segmentDTO1;
    @Mock
    private SegmentDTO segmentDTO2;
    @Mock
    private SegmentDTO segmentDTO3;

    private TicketHandler handler;
    private List<LegDTO> itineraryLegs;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new TicketHandler();

        itineraryLegs = Arrays.asList(legDTO1, legDTO2);
        when(legDTO1.getSegments()).thenReturn(Arrays.asList(segmentDTO1, segmentDTO2));
        when(legDTO2.getSegments()).thenReturn(Collections.singletonList(segmentDTO3));
    }

    @Test
    public void testCalculateSeatsLeft() {
        initializeSegmentSeatInfo(1, 2, 5);
        assertEquals(handler.calculateSeatsLeft(itineraryLegs), new Integer(3));
    }

    @Test
    public void testCalculateSeatsLeftWithOneSegmentNull() {
        initializeSegmentSeatInfo(null, 2, 3);
        assertEquals(handler.calculateSeatsLeft(itineraryLegs), new Integer(2));
    }

    @Test
    public void testCalculateSeatsLeftWithOneLegNull() {
        initializeSegmentSeatInfo(null, null, 3);
        assertEquals(handler.calculateSeatsLeft(itineraryLegs), new Integer(3));
    }

    @Test
    public void testCalculateSeatsLeftAllNull() {
        initializeSegmentSeatInfo(null, null, null);
        assertEquals(handler.calculateSeatsLeft(itineraryLegs), NO_TICKETS_LEFT_INFORMATION);
    }

    private void initializeSegmentSeatInfo(Integer segment1, Integer segment2, Integer segment3) {
        when(segmentDTO1.getSeats()).thenReturn(segment1);
        when(segmentDTO2.getSeats()).thenReturn(segment2);
        when(segmentDTO3.getSeats()).thenReturn(segment3);
    }

}
