package com.odigeo.frontend.resolvers.prime.operations;

import com.odigeo.dapi.client.ModifyShoppingCartRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.resolvers.user.mappers.ModifyMembershipSubscriptionProductMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.openMocks;

public class AddMembershipSubscriptionActionTest {
    @Mock
    private ShoppingCartHandler shoppingCartHandler;
    @Mock
    private ModifyMembershipSubscriptionProductMapper modifyMembershipSubscriptionProductMapper;
    @Mock
    private ResolverContext context;
    @Mock
    private ModifyShoppingCartRequest modifyShoppingCartRequest;
    private ModifyMembershipSubscription modifySubscriptionProduct;
    @InjectMocks
    private AddMembershipSubscriptionAction addMembershipSubscriptionAction;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        modifySubscriptionProduct = ModifyMembershipSubscription.builder().build();
    }

    @Test
    public void testApply() {
        given(modifyMembershipSubscriptionProductMapper.toModifyShoppingCartRequest(eq(modifySubscriptionProduct)))
                .willReturn(modifyShoppingCartRequest);
        addMembershipSubscriptionAction.apply(modifySubscriptionProduct, context);
        verify(modifyMembershipSubscriptionProductMapper).toModifyShoppingCartRequest(eq(modifySubscriptionProduct));
        verify(shoppingCartHandler).addProductsToShoppingCart(eq(modifyShoppingCartRequest), eq(context));
    }
}
