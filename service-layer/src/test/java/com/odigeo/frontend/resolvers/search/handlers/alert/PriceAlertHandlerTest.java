package com.odigeo.frontend.resolvers.search.handlers.alert;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PriceAlertRequest;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.services.CrmSubscriptionService;
import com.odigeo.marketing.subscriptions.rest.v1.parameters.PriceAlertSubscription;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class PriceAlertHandlerTest {

    private static final String DEP = "DEP";
    private static final String ARR = "ARR";
    private static final LocalDate NOW = LocalDate.now();
    private static final String DEP_DATE = NOW.toString();
    private static final String ARR_DATE = NOW.plusDays(1).toString();
    private static final String TEST_MAIL = "test@test.com";

    private static final String CURRENCY = "EUR";
    private static final Site SITE = Site.CO;
    private static final Brand BRAND = Brand.ED;
    private static final String USER_IP = "127.0.0.1";
    private static final String POS = "pos.com";

    @Mock
    private CrmSubscriptionService crmSubscriptionService;
    @Mock
    private PointOfSaleMapper posWebsiteMapper;
    @Mock
    private PriceAlertRequest priceAlertRequestDTO;
    @Mock
    private VisitInformation visit;

    private PriceAlertHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new PriceAlertHandler();

        Guice.createInjector(binder -> {
            binder.bind(CrmSubscriptionService.class).toProvider(() -> crmSubscriptionService);
            binder.bind(PointOfSaleMapper.class).toProvider(() -> posWebsiteMapper);
        }).injectMembers(handler);
    }

    @Test
    public void testSubscribePriceAlerts() {
        ArgumentCaptor<PriceAlertSubscription> captor = ArgumentCaptor.forClass(PriceAlertSubscription.class);

        configureVisitInformation();
        configurePosMapper();
        configurePriceAlertRequest();

        handler.subscribePriceAlerts(priceAlertRequestDTO, visit);

        verify(crmSubscriptionService).subscribeToPriceAlert(captor.capture());
        assertPriceAlertSubscription(captor.getValue());

    }

    private void configureVisitInformation() {
        when(visit.getUserIp()).thenReturn(USER_IP);
        when(visit.getSite()).thenReturn(SITE);
        when(visit.getBrand()).thenReturn(BRAND);
        when(visit.getCurrency()).thenReturn(CURRENCY);
    }

    private void configurePosMapper() {
        when(posWebsiteMapper.map(visit)).thenReturn(POS);
    }

    private void configurePriceAlertRequest() {
        when(priceAlertRequestDTO.getDepartureIata()).thenReturn(DEP);
        when(priceAlertRequestDTO.getArrivalIata()).thenReturn(ARR);
        when(priceAlertRequestDTO.getDepartureDate()).thenReturn(DEP_DATE);
        when(priceAlertRequestDTO.getArrivalDate()).thenReturn(ARR_DATE);
        when(priceAlertRequestDTO.getPrice()).thenReturn(BigDecimal.TEN);
        when(priceAlertRequestDTO.getEmail()).thenReturn(TEST_MAIL);
        when(priceAlertRequestDTO.getNewsletterAccepted()).thenReturn(true);
    }

    private void assertPriceAlertSubscription(PriceAlertSubscription alertSubscription) {
        assertEquals(alertSubscription.getIataPair().getDepartureIata(), DEP);
        assertEquals(alertSubscription.getIataPair().getArrivalIata(), ARR);
        assertEquals(alertSubscription.getPrice(), BigDecimal.TEN);
        assertEquals(alertSubscription.getCurrency(), Currency.getInstance(CURRENCY));
        assertEquals(alertSubscription.getDepartureDate().toString(), DEP_DATE);
        assertEquals(alertSubscription.getArrivalDate().toString(), ARR_DATE);
        assertEquals(alertSubscription.getMarket(), SITE.name());
        assertEquals(alertSubscription.getBrand(), BRAND.name());
        assertEquals(alertSubscription.getEmail(), TEST_MAIL);
        assertTrue(alertSubscription.isSubscribed());
        assertEquals(alertSubscription.getSubscriptionKey(), TEST_MAIL + "|" + POS);
        assertEquals(alertSubscription.getIp(), USER_IP);
    }

}
