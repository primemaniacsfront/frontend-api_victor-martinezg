package com.odigeo.frontend.resolvers.prime;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.prime.handlers.PrimeInfoHandler;
import com.odigeo.frontend.resolvers.prime.handlers.RetentionFlowInfoHandler;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowInfo;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class PrimeResolverTest {
    private static final String PRIME_INFO_QUERY = "primeInfo";
    private static final String RETENTION_INFO_FLOW_QUERY = "retentionInfo";

    @Mock
    private PrimeInfoHandler primeInfoHandler;
    @Mock
    private RetentionFlowInfoHandler retentionFlowInfoHandler;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;

    @InjectMocks
    private PrimeResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visit);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(PRIME_INFO_QUERY));
        assertNotNull(queries.get(RETENTION_INFO_FLOW_QUERY));
    }

    @Test
    public void testPrimeInfoFetcher() {
        PrimeInfo response = mock(PrimeInfo.class);
        when(primeInfoHandler.buildPrimeInfo(context)).thenReturn(response);

        assertSame(resolver.primeInfoFetcher(env), response);
    }

    @Test
    public void testRetentionInfoFetcher() {
        RetentionFlowInfo response = mock(RetentionFlowInfo.class);
        when(retentionFlowInfoHandler.buildRetentionFlowInfo(env)).thenReturn(response);

        assertSame(resolver.retentionFlowInfoFetcher(env), response);
    }

}
