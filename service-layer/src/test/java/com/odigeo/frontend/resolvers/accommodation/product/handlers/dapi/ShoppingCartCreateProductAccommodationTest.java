package com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationBuyer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreateAccommodationProductResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Guest;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.dapi.client.PersonalInfoRequest;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ShoppingCartCreateProductAccommodationTest {

    @Mock
    private ShoppingCartHandler shoppingCartHandler;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private AccommodationGuestsHandler accommodationGuestsHandler;
    @Mock
    private VisitInformation visitInformation;

    @InjectMocks
    private ShoppingCartCreateProductAccommodation testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testOk() {
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = getShoppingCartSummaryResponse();
        when(resolverContext.getVisitInformation()).thenReturn(visitInformation);
        when(visitInformation.getVisitCode()).thenReturn("visitCode");
        when(shoppingCartHandler.addPersonalInfo(any(PersonalInfoRequest.class), any(ResolverContext.class))).thenReturn(shoppingCartSummaryResponse);
        Guest guest = createGuestRequest();
        AccommodationBuyer buyer = createBuyerRequest();
        List<Guest> guestsRequest = Collections.singletonList(guest);
        CreateAccommodationProductResponse productAccommodation = testClass.createProductAccommodation("123", guestsRequest, buyer, resolverContext);
        assertNotNull(productAccommodation);
        assertEquals(productAccommodation.getAccommodationProduct().getProductId(), "456");
        verify(accommodationGuestsHandler)
                .buildGuests(eq(guestsRequest), anyLong(), any(ResolverContext.class), anyString());
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testInvalidId() {
        when(resolverContext.getVisitInformation()).thenReturn(visitInformation);
        when(visitInformation.getVisitCode()).thenReturn("visitCode");
        Guest guest = createGuestRequest();
        AccommodationBuyer buyer = createBuyerRequest();
        List<Guest> guestsRequest = Collections.singletonList(guest);
        try {
            testClass.createProductAccommodation("abc", guestsRequest, buyer, resolverContext);
        } catch (Exception e) {
            verify(shoppingCartHandler, never()).addPersonalInfo(any(PersonalInfoRequest.class), any(ResolverContext.class));
            verify(accommodationGuestsHandler, never())
                    .buildGuests(eq(guestsRequest), anyLong(), any(ResolverContext.class), anyString());
            throw e;
        }
    }

    private ShoppingCartSummaryResponse getShoppingCartSummaryResponse() {
        ShoppingCartSummaryResponse shoppingCartSummaryResponse = new ShoppingCartSummaryResponse();
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setBookingId(123L);
        AccommodationShoppingItem accommodationShoppingItem = new AccommodationShoppingItem();
        accommodationShoppingItem.setId(456L);
        shoppingCart.setAccommodationShoppingItem(accommodationShoppingItem);
        shoppingCartSummaryResponse.setShoppingCart(shoppingCart);
        return shoppingCartSummaryResponse;
    }


    private AccommodationBuyer createBuyerRequest() {
        AccommodationBuyer buyerRequest = new AccommodationBuyer();

        buyerRequest.setName("Name");
        buyerRequest.setLastNames("LastNames");
        buyerRequest.setMail("test@edreamsodigeo.com");
        buyerRequest.setBuyerIdentificationTypeName("PASSPORT");
        buyerRequest.setIdentification("56435143524a");
        buyerRequest.setDayOfBirth(30);
        buyerRequest.setMonthOfBirth(8);
        buyerRequest.setYearOfBirth(1990);
        buyerRequest.setAddress("Adress");
        buyerRequest.setCityName("City");
        buyerRequest.setStateName("State");
        buyerRequest.setZipCode("08830");
        buyerRequest.setPhoneNumber("600111222");
        buyerRequest.setCountryPhoneNumber("ES");
        buyerRequest.setCountryCode("ES");

        return buyerRequest;
    }

    private Guest createGuestRequest() {
        Guest guest = new Guest();
        guest.setTravellerTypeName("ADULT");
        guest.setTitleName("MR");
        guest.setName("Name");
        guest.setFirstLastName("FLastName");
        guest.setSecondLastName("SLastName");
        guest.setMiddleName("MName");
        guest.setGender("MALE");
        guest.setNationalityCountryCode("ES");
        guest.setCountryCodeOfResidence("ES");
        guest.setIdentification("56435143524a");
        guest.setIdentificationTypeName("PASSPORT");
        guest.setIdentificationExpirationDay(10);
        guest.setIdentificationExpirationMonth(7);
        guest.setIdentificationExpirationYear(2030);
        guest.setIdentificationIssueCountryCode("ES");
        guest.setDayOfBirth(15);
        guest.setMonthOfBirth(7);
        guest.setYearOfBirth(1990);
        return guest;
    }

}
