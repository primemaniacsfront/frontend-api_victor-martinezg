package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.List;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class AbstractPricePolicyTest {

    private static final String CURRENCY_TEST = "GBP";

    private AbstractPricePolicy policy;

    @BeforeMethod
    public void init() {
        policy = spy(AbstractPricePolicy.class);
    }

    @Test
    public void testCalculateItineraryPrices() {
        BigDecimal passengers = new BigDecimal("2");
        BigDecimal price = BigDecimal.TEN;

        FareItinerary fareItinerary = mock(FareItinerary.class);
        SearchEngineContext seContext = mock(SearchEngineContext.class);

        Fee feeDTO = new Fee();
        feeDTO.setPrice(new Money());
        feeDTO.setType(new FeeType());
        feeDTO.getPrice().setAmount(price);

        when(seContext.getCurrency()).thenReturn(CURRENCY_TEST);
        when(policy.calculateFees(eq(seContext), eq(fareItinerary))).thenReturn(Collections.singletonList(feeDTO));

        List<Fee> itineraryFees = policy.calculateItineraryPrices(seContext, fareItinerary, passengers);
        Fee fee = itineraryFees.get(0);
        Money feePrice = fee.getPrice();

        assertEquals(itineraryFees.size(), 1);
        assertEquals(feePrice.getCurrency(), CURRENCY_TEST);
        assertEquals(feePrice.getAmount(), price.divide(passengers, 2, RoundingMode.HALF_UP));
    }

    @Test
    public void testCalculateDefaultFeeType() {
        String defaultId = "id";
        when(policy.calculateDefaultFeeTypeId()).thenReturn(defaultId);

        FeeType defaultFeeType = policy.calculateDefaultFeeType();
        assertEquals(defaultFeeType.getId(), defaultId);
        assertNull(defaultFeeType.getName());
    }

    @Test
    public void testCalculatePricePerPassenger() {
        BigDecimal amount = BigDecimal.valueOf(10);
        BigDecimal passengers = BigDecimal.valueOf(4);

        BigDecimal pricePax = policy.calculatePricePerPassenger(amount, passengers);

        assertEquals(pricePax, amount.divide(passengers, 2, RoundingMode.HALF_UP));
    }

    @Test
    public void testPopulatePricePerPassenger() {
        BigDecimal passengers = BigDecimal.valueOf(4);
        Fee fee = buildDefaultFee();

        policy.populatePricePerPassenger(fee, passengers);

        assertEquals(fee.getPrice().getAmount(), buildDefaultFee().getPrice().getAmount().divide(passengers, 2, RoundingMode.HALF_UP));
    }

    @Test
    public void testPopulateCurrency() {
        Fee fee = buildDefaultFee();
        policy.populateCurrency(fee, CURRENCY_TEST);
        assertEquals(fee.getPrice().getCurrency(), CURRENCY_TEST);
    }

    private Fee buildDefaultFee() {
        return new Fee(new Money(BigDecimal.TEN, CURRENCY_TEST), new FeeType());
    }

}
