package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentRequest;
import com.odigeo.frontend.commons.ModuleReleaseInfo;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.search.handlers.pricing.PriceHandler;
import com.odigeo.searchengine.v2.common.CabinClass;
import com.odigeo.searchengine.v2.requests.ExternalSelectionRequest;
import com.odigeo.searchengine.v2.requests.ItinerarySearchRequest;
import com.odigeo.searchengine.v2.requests.ItinerarySegmentRequest;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.requests.enums.SearchProductType;
import com.odigeo.searchengine.v2.searchresults.criteria.marketing.tracking.MarketingTrackingInfo;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class SearchRequestHandlerTest {

    private static final int MAX_SEARCH_RESULTS = 750;

    @Mock
    private ModuleReleaseInfo moduleReleaseInfo;
    @Mock
    private VisitInformation visit;
    @Mock
    private DateUtils dateUtils;
    @Mock
    private ResolverContext context;
    @Mock
    private DebugInfo debugInfo;
    @Mock
    private MarketingTrackingHandler marketingTrackingHandler;
    @Mock
    private CabinMapper cabinMapper;
    @Mock
    private SiteVariations siteVariations;
    @Mock
    private PriceHandler priceHandler;
    @Mock
    private PrimeHandler primeHandler;

    private SearchRequest searchRequestDTO;
    private ItineraryRequest itineraryDTO;
    private SegmentRequest segmentDTO;
    private LocationRequest departureDTO;
    private LocationRequest destinationDTO;

    @InjectMocks
    private SearchRequestHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(context.getVisitInformation()).thenReturn(visit);
        when(context.getDebugInfo()).thenReturn(debugInfo);

        searchRequestDTO = new SearchRequest();
        itineraryDTO = new ItineraryRequest();
        segmentDTO = new SegmentRequest();
        departureDTO = new LocationRequest();
        destinationDTO = new LocationRequest();

        searchRequestDTO.setItinerary(itineraryDTO);
        itineraryDTO.setNumChildren(0);
        itineraryDTO.setNumInfants(0);
        itineraryDTO.setNumAdults(0);
        itineraryDTO.setSegments(Collections.singletonList(segmentDTO));
        itineraryDTO.setResident(false);
        segmentDTO.setDeparture(departureDTO);
        segmentDTO.setDestination(destinationDTO);
    }

    @Test
    public void testBuildSearchRequest() {
        Integer buyPath = 1;
        String visitCode = "visit";
        String clientVersion = "client";

        MarketingTrackingInfo trackingInfo = mock(MarketingTrackingInfo.class);

        searchRequestDTO.setBuyPath(buyPath);
        when(visit.getVisitCode()).thenReturn(visitCode);
        when(moduleReleaseInfo.formatModuleInfo()).thenReturn(clientVersion);
        when(marketingTrackingHandler.createMktTrackingInfo(context)).thenReturn(trackingInfo);
        when(primeHandler.isPrimeMarket(context)).thenReturn(Boolean.FALSE);

        SearchMethodRequest request = handler.buildSearchRequest(searchRequestDTO, context);

        assertEquals(request.getBuypath(), buyPath);
        assertEquals(request.getVisitInformation(), visitCode);
        assertFalse(request.isQaMode());
        assertTrue(request.isEstimationFees());
        assertEquals(request.getClientVersion(), clientVersion);
        assertEquals(request.getMarketingTrackingInfo(), trackingInfo);
    }

    @Test
    public void testBuildItineraryRequest() {
        Integer numAdults = 3;
        Integer numInfants = 2;
        Integer numChildren = 1;
        String cabinClass = "cabinClass";
        CabinClass cabin = mock(CabinClass.class);

        itineraryDTO.setNumAdults(numAdults);
        itineraryDTO.setNumInfants(numInfants);
        itineraryDTO.setNumChildren(numChildren);
        itineraryDTO.setCabinClass(cabinClass);
        itineraryDTO.setResident(true);
        itineraryDTO.setMainAirportsOnly(true);

        when(cabinMapper.map(cabinClass)).thenReturn(cabin);

        SearchMethodRequest request = handler.buildSearchRequest(searchRequestDTO, context);
        ItinerarySearchRequest itinerary = (ItinerarySearchRequest) request.getSearchRequest();

        assertEquals(itinerary.getNumAdults(), numAdults.intValue());
        assertEquals(itinerary.getNumInfants(), numInfants.intValue());
        assertEquals(itinerary.getNumChildren(), numChildren.intValue());
        assertFalse(itinerary.isDirectFlightsOnly());
        assertSame(itinerary.getSearchMainProductType(), SearchProductType.FLIGHT);
        assertSame(itinerary.getCabinClass(), cabin);
        assertTrue(itinerary.isResident());
        assertTrue(itinerary.isMainAirportsOnly());
        assertFalse(itinerary.isDynpackSearch());
        assertEquals(itinerary.getMaxSize(), MAX_SEARCH_RESULTS);
        verify(priceHandler).populatePricePolicy(itinerary, itineraryDTO, Boolean.FALSE);
    }

    @Test
    public void residentIsSetToFalseIfAbActive() {
        Integer numAdults = 3;
        Integer numInfants = 2;
        Integer numChildren = 1;
        String cabinClass = "cabinClass";
        CabinClass cabin = mock(CabinClass.class);

        itineraryDTO.setNumAdults(numAdults);
        itineraryDTO.setNumInfants(numInfants);
        itineraryDTO.setNumChildren(numChildren);
        itineraryDTO.setCabinClass(cabinClass);
        itineraryDTO.setResident(true);
        itineraryDTO.setMainAirportsOnly(true);

        when(cabinMapper.map(cabinClass)).thenReturn(cabin);
        when(siteVariations.isResidentDiscountInPax(visit)).thenReturn(true);

        SearchMethodRequest request = handler.buildSearchRequest(searchRequestDTO, context);
        ItinerarySearchRequest itinerary = (ItinerarySearchRequest) request.getSearchRequest();

        assertFalse(itinerary.isResident());
    }

    @Test
    public void testBuildSegmentRequest() {
        String requestDate = "requestDate";
        Calendar convertedDate = Calendar.getInstance();
        String deparuteIata = "departureIata";
        String destinationIata = "destinationIata";
        Integer departureCode = 1;
        Integer destinationCode = 2;

        segmentDTO.setDate(requestDate);
        departureDTO.setIata(deparuteIata);
        departureDTO.setGeoNodeId(departureCode);
        destinationDTO.setIata(destinationIata);
        destinationDTO.setGeoNodeId(destinationCode);

        when(dateUtils.convertFromIsoToCalendar(requestDate)).thenReturn(convertedDate);

        SearchMethodRequest request = handler.buildSearchRequest(searchRequestDTO, context);
        ItinerarySearchRequest itinerary = (ItinerarySearchRequest) request.getSearchRequest();
        List<ItinerarySegmentRequest> segments = itinerary.getSegmentRequests();
        ItinerarySegmentRequest segment = segments.get(0);

        assertEquals(segments.size(), 1);
        assertEquals(segment.getDestination().getIataCode(), destinationIata);
        assertEquals(segment.getDeparture().getIataCode(), deparuteIata);
        assertEquals(segment.getDestination().getGeoNodeId(), destinationCode);
        assertEquals(segment.getDeparture().getGeoNodeId(), departureCode);
        assertEquals(segment.getDate(), convertedDate);
    }

    @Test
    public void testBuildExternalSelectionRequest() {
        Integer externalCollectionMethod = 1;
        String externalFareItinerary = "fareItinerary";
        Long externalSearchId = 2L;
        List<String> externalSegments = Collections.singletonList("s1");
        BigDecimal displayedPrice = new BigDecimal("1.2");
        String displayedCurrency = "displayCurrency";

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelectionRequest externalSelectionDTO = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelectionRequest();
        externalSelectionDTO.setCollectionMethodId(externalCollectionMethod);
        externalSelectionDTO.setFareItineraryKey(externalFareItinerary);
        externalSelectionDTO.setSearchId(externalSearchId);
        externalSelectionDTO.setSegmentKeys(externalSegments);
        externalSelectionDTO.setDisplayedPrice(displayedPrice);
        externalSelectionDTO.setDisplayedCurrency(displayedCurrency);
        itineraryDTO.setExternalSelection(externalSelectionDTO);

        SearchMethodRequest request = handler.buildSearchRequest(searchRequestDTO, context);
        ItinerarySearchRequest itinerary = (ItinerarySearchRequest) request.getSearchRequest();
        ExternalSelectionRequest externalSelection = itinerary.getExternalSelectionRequest();

        assertEquals(externalSelection.getCollectionMethodId(), externalCollectionMethod);
        assertEquals(externalSelection.getFareItineraryKey(), externalFareItinerary);
        assertEquals(externalSelection.getSearchId(), externalSearchId);
        assertEquals(externalSelection.getSegmentKeys(), externalSegments);
        assertEquals(externalSelection.getDisplayedPrice(), displayedPrice);
        assertEquals(externalSelection.getDisplayedCurrency(), displayedCurrency);
    }

    @Test
    public void testBuildExternalSelectionRequestEmpty() {
        SearchMethodRequest request = handler.buildSearchRequest(searchRequestDTO, context);
        ItinerarySearchRequest itinerary = (ItinerarySearchRequest) request.getSearchRequest();

        assertNull(itinerary.getExternalSelectionRequest());
    }

    @Test
    public void testBuildExternalSelectionRequestNoDisplayPrice() {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelectionRequest externalSelectionDTO = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelectionRequest();
        externalSelectionDTO.setSegmentKeys(Collections.emptyList());
        itineraryDTO.setExternalSelection(externalSelectionDTO);

        SearchMethodRequest request = handler.buildSearchRequest(searchRequestDTO, context);
        ItinerarySearchRequest itinerary = (ItinerarySearchRequest) request.getSearchRequest();
        assertNull(itinerary.getExternalSelectionRequest().getDisplayedPrice());
    }

}
