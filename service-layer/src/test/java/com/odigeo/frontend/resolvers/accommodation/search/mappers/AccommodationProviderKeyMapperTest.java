package com.odigeo.frontend.resolvers.accommodation.search.mappers;

import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AccommodationProviderKeyMapperTest {

    AccommodationProviderKeyMapper testClass;

    @Test
    public void testMap() {
        testClass = new AccommodationProviderKeyMapper();
        String hotelSupplierId = "123";
        String supplierId = "BC";
        AccommodationProviderKeyDTO accommodationProviderKeyDTO = testClass.map(new HotelSupplierKey(hotelSupplierId, supplierId));
        assertEquals(accommodationProviderKeyDTO.getAccommodationProviderId(), hotelSupplierId);
        assertEquals(accommodationProviderKeyDTO.getProviderId(), supplierId);

    }
}
