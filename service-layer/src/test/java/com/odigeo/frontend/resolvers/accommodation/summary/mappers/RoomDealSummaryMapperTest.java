package com.odigeo.frontend.resolvers.accommodation.summary.mappers;

import com.odigeo.frontend.resolvers.accommodation.summary.models.RoomDealSummaryDTO;
import com.odigeo.searchengine.v2.responses.accommodation.BoardType;
import com.odigeo.searchengine.v2.responses.accommodation.RoomDeal;
import com.odigeo.searchengine.v2.responses.accommodation.RoomSmokingPreference;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class RoomDealSummaryMapperTest {

    private RoomDealSummaryMapper mapper = new RoomDealSummaryMapper();

    @Test
    public void testMap() {
        RoomDeal roomDeal = new RoomDeal();
        roomDeal.setRoomId("roomID");
        roomDeal.setKey("roomKey");
        roomDeal.setDescription("description");
        roomDeal.getBedsDescriptions().add("bed description");
        roomDeal.setCancelPolicy("cancellation policy");
        roomDeal.setSmokingPreference(RoomSmokingPreference.NON_SMOKING);
        roomDeal.setProviderId("OC");
        roomDeal.setIsCancellationFree(true);
        roomDeal.setDepositRequired(true);
        roomDeal.setBoardType(BoardType.RO);

        RoomDealSummaryDTO dto = mapper.map(roomDeal);

        assertEquals(dto.getRoomId(), roomDeal.getRoomId());
        assertEquals(dto.getRoomKey(), roomDeal.getKey());
        assertEquals(dto.getDescription(), roomDeal.getDescription());
        assertEquals(dto.getBedsDescription(), roomDeal.getBedsDescriptions().get(0));
        assertEquals(dto.getCancelPolicy(), roomDeal.getCancelPolicy());
        assertEquals(dto.getSmokingPreference().value(), roomDeal.getSmokingPreference().value());
        assertEquals(dto.getProviderId(), roomDeal.getProviderId());
        assertEquals(dto.getCancellationFree(), roomDeal.isIsCancellationFree());
        assertEquals(dto.getDepositRequired(), roomDeal.isDepositRequired());
        assertEquals(dto.getBoardType().value(), roomDeal.getBoardType().value());
    }

    @Test
    public void testMapNoBedsDescriptionButRoomType() {
        RoomDeal roomDeal = new RoomDeal();
        roomDeal.setRoomId("roomID");
        roomDeal.setKey("roomKey");
        roomDeal.setDescription("description");
        roomDeal.setCancelPolicy("cancellation policy");
        roomDeal.setRoomType("room type");
        roomDeal.setSmokingPreference(RoomSmokingPreference.NON_SMOKING);
        roomDeal.setProviderId("OC");
        roomDeal.setIsCancellationFree(true);
        roomDeal.setDepositRequired(true);
        roomDeal.setBoardType(BoardType.RO);

        RoomDealSummaryDTO dto = mapper.map(roomDeal);

        assertEquals(dto.getRoomId(), roomDeal.getRoomId());
        assertEquals(dto.getRoomKey(), roomDeal.getKey());
        assertEquals(dto.getDescription(), roomDeal.getDescription());
        assertEquals(dto.getBedsDescription(), roomDeal.getRoomType());
        assertEquals(dto.getCancelPolicy(), roomDeal.getCancelPolicy());
        assertEquals(dto.getSmokingPreference().value(), roomDeal.getSmokingPreference().value());
        assertEquals(dto.getProviderId(), roomDeal.getProviderId());
        assertEquals(dto.getCancellationFree(), roomDeal.isIsCancellationFree());
        assertEquals(dto.getDepositRequired(), roomDeal.isDepositRequired());
        assertEquals(dto.getBoardType().value(), roomDeal.getBoardType().value());
    }

    @Test
    public void testMapNullValues() {
        RoomDealSummaryDTO dto = mapper.map(new RoomDeal());
        assertNull(dto.getRoomId());
        assertNull(dto.getRoomKey());
        assertNull(dto.getDescription());
        assertNull(dto.getBedsDescription());
        assertNull(dto.getCancelPolicy());
        assertNull(dto.getSmokingPreference());
        assertNull(dto.getProviderId());
        assertNull(dto.getCancellationFree());
        assertNull(dto.getDepositRequired());
        assertNull(dto.getBoardType());
    }

    @Test
    public void testMapWithPaymentAtDestinationTrueAndDepositNotRequired() {
        RoomDeal roomDeal = new RoomDeal();
        roomDeal.setRoomId("roomID");
        roomDeal.setKey("roomKey");
        roomDeal.setDescription("description");
        roomDeal.getBedsDescriptions().add("bed description");
        roomDeal.setCancelPolicy("cancellation policy");
        roomDeal.setSmokingPreference(RoomSmokingPreference.NON_SMOKING);
        roomDeal.setProviderId("OC");
        roomDeal.setIsCancellationFree(true);
        roomDeal.setDepositRequired(false);
        roomDeal.setBoardType(BoardType.RO);

        RoomDealSummaryDTO dto = mapper.map(roomDeal, true);

        assertEquals(dto.getRoomId(), roomDeal.getRoomId());
        assertEquals(dto.getRoomKey(), roomDeal.getKey());
        assertEquals(dto.getDescription(), roomDeal.getDescription());
        assertEquals(dto.getBedsDescription(), roomDeal.getBedsDescriptions().get(0));
        assertEquals(dto.getCancelPolicy(), roomDeal.getCancelPolicy());
        assertEquals(dto.getSmokingPreference().value(), roomDeal.getSmokingPreference().value());
        assertEquals(dto.getProviderId(), roomDeal.getProviderId());
        assertEquals(dto.getCancellationFree(), roomDeal.isIsCancellationFree());
        assertEquals(dto.getDepositRequired(), roomDeal.isDepositRequired());
        assertEquals(dto.getBoardType().value(), roomDeal.getBoardType().value());
        assertTrue(dto.getPaymentAtDestination());
    }

    @Test
    public void testMapWithPaymentAtDestinationFalseAndDepositIsRequired() {
        RoomDeal roomDeal = new RoomDeal();
        roomDeal.setRoomId("roomID");
        roomDeal.setKey("roomKey");
        roomDeal.setDescription("description");
        roomDeal.getBedsDescriptions().add("bed description");
        roomDeal.setCancelPolicy("cancellation policy");
        roomDeal.setSmokingPreference(RoomSmokingPreference.NON_SMOKING);
        roomDeal.setProviderId("OC");
        roomDeal.setIsCancellationFree(true);
        roomDeal.setDepositRequired(true);
        roomDeal.setBoardType(BoardType.RO);

        RoomDealSummaryDTO dto = mapper.map(roomDeal, true);

        assertEquals(dto.getRoomId(), roomDeal.getRoomId());
        assertEquals(dto.getRoomKey(), roomDeal.getKey());
        assertEquals(dto.getDescription(), roomDeal.getDescription());
        assertEquals(dto.getBedsDescription(), roomDeal.getBedsDescriptions().get(0));
        assertEquals(dto.getCancelPolicy(), roomDeal.getCancelPolicy());
        assertEquals(dto.getSmokingPreference().value(), roomDeal.getSmokingPreference().value());
        assertEquals(dto.getProviderId(), roomDeal.getProviderId());
        assertEquals(dto.getCancellationFree(), roomDeal.isIsCancellationFree());
        assertEquals(dto.getDepositRequired(), roomDeal.isDepositRequired());
        assertEquals(dto.getBoardType().value(), roomDeal.getBoardType().value());
        assertFalse(dto.getPaymentAtDestination());
    }

}
