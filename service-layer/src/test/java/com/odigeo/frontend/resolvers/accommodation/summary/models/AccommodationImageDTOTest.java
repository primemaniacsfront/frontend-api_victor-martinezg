package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationImageType;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;

import static org.mockito.Mockito.mock;

public class AccommodationImageDTOTest extends BeanTest<AccommodationImageDTO> {

    public AccommodationImageDTO getBean() {
        AccommodationImageDTO bean = new AccommodationImageDTO();
        bean.setUrl("url");
        bean.setThumbnailUrl("thumbUrl");
        bean.setQuality(mock(AccommodationImageQualityDTO.class));
        bean.setType(mock(AccommodationImageType.class));

        return bean;
    }


}
