package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.common.ItinerarySortCriteria;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.FareItineraryPriceCalculator;
import com.odigeo.searchengine.v2.responses.membership.MembershipPerks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class PrimePricePolicyTest {

    private static final String DISCOUNTED_FEE_TYPE_ID = "MEMBER_PRICE_POLICY_DISCOUNTED";
    private static final String UNDISCOUNTED_FEE_TYPE_ID = "MEMBER_PRICE_POLICY_UNDISCOUNTED";
    private static final String DISCOUNTED_PLUS_COUPON_FEE_TYPE_ID = "MEMBER_PRICE_POLICY_DISCOUNTED_PLUS_COUPON";

    private static final BigDecimal SORT_PRICE = BigDecimal.TEN;

    @Mock
    private SearchEngineContext seContext;
    @Mock
    private FareItinerary fareItinerary;
    @Mock
    private MembershipPerks membershipPerks;

    private PrimePricePolicy policy;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        policy = new PrimePricePolicy();

        FareItineraryPriceCalculator price = mock(FareItineraryPriceCalculator.class);
        when(fareItinerary.getPrice()).thenReturn(price);
        when(price.getSortPrice()).thenReturn(SORT_PRICE);
        when(seContext.getItinerarySortCriteria()).thenReturn(ItinerarySortCriteria.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE);
        when(fareItinerary.getMembershipPerks()).thenReturn(membershipPerks);
    }

    @Test
    public void testCalculateFees() {
        BigDecimal perksFee = BigDecimal.ONE.negate();
        when(membershipPerks.getFee()).thenReturn(perksFee);

        List<Fee> itineraryFees = policy.calculateFees(seContext, fareItinerary);

        assertEquals(itineraryFees.size(), 2);
        FeesTester.checkFee(itineraryFees, DISCOUNTED_FEE_TYPE_ID, SORT_PRICE, null, null);
        FeesTester.checkFee(itineraryFees, UNDISCOUNTED_FEE_TYPE_ID, SORT_PRICE.subtract(perksFee), null, null);
    }

    @Test
    public void testCalculateFeesWithMinimumPurchasablePriceSortCriteria() {
        when(seContext.getItinerarySortCriteria()).thenReturn(ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
        BigDecimal perksFee = BigDecimal.ONE.negate();
        when(membershipPerks.getFee()).thenReturn(perksFee);

        List<Fee> itineraryFees = policy.calculateFees(seContext, fareItinerary);

        assertEquals(itineraryFees.size(), 2);
        FeesTester.checkFee(itineraryFees, UNDISCOUNTED_FEE_TYPE_ID, SORT_PRICE, null, null);
        FeesTester.checkFee(itineraryFees, DISCOUNTED_FEE_TYPE_ID, SORT_PRICE.add(perksFee), null, null);
    }

    @Test
    public void testCalculateFeesWithoutPerks() {
        List<Fee> itineraryFees = policy.calculateFees(seContext, fareItinerary);

        assertEquals(itineraryFees.size(), 2);
        FeesTester.checkFee(itineraryFees, DISCOUNTED_FEE_TYPE_ID, SORT_PRICE, null, null);
        FeesTester.checkFee(itineraryFees, UNDISCOUNTED_FEE_TYPE_ID, SORT_PRICE, null, null);
    }

    @Test
    public void testCalculateDefaultFeeTypeId() {
        assertEquals(policy.calculateDefaultFeeTypeId(), DISCOUNTED_FEE_TYPE_ID);
    }

    @Test
    public void testCouponPrime() {
        BigDecimal perksFee = BigDecimal.ONE.negate();
        BigDecimal perksCoupon = BigDecimal.TEN.negate();
        when(membershipPerks.getFee()).thenReturn(perksFee);
        when(membershipPerks.getMembershipCoupon()).thenReturn(perksCoupon);

        List<Fee> itineraryFees = policy.calculateFees(seContext, fareItinerary);
        assertEquals(itineraryFees.size(), 3);

        FeesTester.checkFee(itineraryFees, UNDISCOUNTED_FEE_TYPE_ID, SORT_PRICE.subtract(perksFee).subtract(perksCoupon), null, null);
        FeesTester.checkFee(itineraryFees, DISCOUNTED_FEE_TYPE_ID, SORT_PRICE.subtract(perksCoupon), null, null);
        FeesTester.checkFee(itineraryFees, DISCOUNTED_PLUS_COUPON_FEE_TYPE_ID, SORT_PRICE, null, null);
    }
}
