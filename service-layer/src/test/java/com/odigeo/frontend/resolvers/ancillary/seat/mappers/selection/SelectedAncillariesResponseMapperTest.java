package com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedAncillariesResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedAncillaryProduct;
import com.odigeo.itineraryapi.v1.response.SelectAncillaryServiceResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SelectedAncillariesResponseMapperTest {

    @Mock
    private SelectedAncillaryProductMapper selectedAncillaryProductMapper;

    @Mock
    private SelectedAncillaryProduct selectedAncillaryProduct;

    @InjectMocks
    private SelectedAncillariesResponseMapper selectedAncillariesResponseMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(selectedAncillaryProductMapper.map(any())).thenReturn(Collections.singletonList(selectedAncillaryProduct));
        when(selectedAncillaryProductMapper.map(null)).thenReturn(Collections.emptyList());
    }

    @Test
    public void mapSuccessful() {
        SelectAncillaryServiceResponse inputSelectAncillaryServiceResponse = new SelectAncillaryServiceResponse();
        inputSelectAncillaryServiceResponse.setCode(null);
        inputSelectAncillaryServiceResponse.setMessage(null);
        inputSelectAncillaryServiceResponse.setSuccess(true);
        inputSelectAncillaryServiceResponse.setProductIds(Collections.singletonMap("FOO", "BAR"));

        SelectedAncillariesResponse outputSelectedAncillariesResponse =
                selectedAncillariesResponseMapper.map(inputSelectAncillaryServiceResponse);

        assertEquals(outputSelectedAncillariesResponse.getCode(), inputSelectAncillaryServiceResponse.getCode());
        assertEquals(outputSelectedAncillariesResponse.getMessage(), inputSelectAncillaryServiceResponse.getMessage());
        assertEquals(outputSelectedAncillariesResponse.getSuccess(), inputSelectAncillaryServiceResponse.isSuccess());
        assertEquals(outputSelectedAncillariesResponse.getProducts().size(), inputSelectAncillaryServiceResponse.getProductIds().size());
    }

    @Test
    public void mapUnsuccessful() {
        SelectAncillaryServiceResponse inputSelectAncillaryServiceResponse = new SelectAncillaryServiceResponse();
        inputSelectAncillaryServiceResponse.setCode("500");
        inputSelectAncillaryServiceResponse.setMessage("FOO");
        inputSelectAncillaryServiceResponse.setSuccess(false);
        inputSelectAncillaryServiceResponse.setProductIds(null);

        SelectedAncillariesResponse outputSelectedAncillariesResponse =
                selectedAncillariesResponseMapper.map(inputSelectAncillaryServiceResponse);

        assertEquals(outputSelectedAncillariesResponse.getCode(), inputSelectAncillaryServiceResponse.getCode());
        assertEquals(outputSelectedAncillariesResponse.getMessage(), inputSelectAncillaryServiceResponse.getMessage());
        assertEquals(outputSelectedAncillariesResponse.getSuccess(), inputSelectAncillaryServiceResponse.isSuccess());
        assertTrue(outputSelectedAncillariesResponse.getProducts().isEmpty());
    }
}

