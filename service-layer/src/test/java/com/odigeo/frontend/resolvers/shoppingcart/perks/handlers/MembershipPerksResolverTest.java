package com.odigeo.frontend.resolvers.shoppingcart.perks.handlers;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerks;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.shoppingcart.perks.handlers.MembershipPerksResolver.MEMBERSHIP_PERKS_FIELD;
import static com.odigeo.frontend.resolvers.shoppingcart.perks.handlers.MembershipPerksResolver.SHOPPING_CART_TYPE;
import static org.mockito.ArgumentMatchers.eq;
import static org.testng.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MembershipPerksResolverTest {

    @Mock
    private DataFetchingEnvironment dataFetchingEnvironment;
    @Mock
    private PerksHandler<MembershipPerks> membershipPerksPerksHandler;
    @Mock
    private GraphQLContext graphQLContext;
    @InjectMocks
    private MembershipPerksResolver membershipPerksResolver;

    @BeforeTest
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(dataFetchingEnvironment.getGraphQlContext()).thenReturn(graphQLContext);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder builder = RuntimeWiring.newRuntimeWiring();
        membershipPerksResolver.addToBuilder(builder);
        RuntimeWiring runtimeWiring = builder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(SHOPPING_CART_TYPE);
        assertNotNull(queries.get(MEMBERSHIP_PERKS_FIELD));
    }

    @Test
    public void testMembershipPerksDataFetcher() {
        membershipPerksResolver.membershipPerksDataFetcher(dataFetchingEnvironment);
        verify(membershipPerksPerksHandler).map(eq(graphQLContext));
    }
}
