package com.odigeo.frontend.resolvers.accommodation.search.handlers;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItinerarySelection;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MainProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RoomRequest;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.ModuleReleaseInfo;
import com.odigeo.frontend.commons.context.DebugInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.search.handlers.MarketingTrackingHandler;
import com.odigeo.searchengine.v2.requests.AccommodationSearchRequest;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static voldemort.utils.Utils.assertNotNull;

public class AccommodationSearchRequestHandlerTest {

    private AccommodationSearchRequestHandler testClass;

    @Mock
    private ModuleReleaseInfo moduleReleaseInfo;
    @Mock
    private DateUtils dateUtils;
    @Mock
    private MarketingTrackingHandler marketingTrackingHandler;

    @Mock
    private ResolverContext context;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        testClass = new AccommodationSearchRequestHandler();
        Guice.createInjector(binder -> {
            binder.bind(ModuleReleaseInfo.class).toProvider(() -> moduleReleaseInfo);
            binder.bind(DateUtils.class).toProvider(() -> dateUtils);
            binder.bind(MarketingTrackingHandler.class).toProvider(() -> marketingTrackingHandler);
        }).injectMembers(testClass);

    }

    private void setUpContext() {
        VisitInformation visitInformation = new VisitInformation();
        DebugInfo debugInfo = new DebugInfo();
        when(context.getVisitInformation()).thenReturn(visitInformation);
        when(context.getDebugInfo()).thenReturn(debugInfo);
        when(dateUtils.convertFromIsoToLocalDate(anyString())).thenCallRealMethod();
    }

    @Test
    public void testBuildRequest() {
        setUpContext();
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest request = buildAccommodationRequestDTO(false);
        request.setBuyPath(8);
        SearchMethodRequest searchMethodRequest = testClass.buildSearchRequest(request, context);

        assertNotNull(searchMethodRequest.getSearchRequest());
        assertTrue(searchMethodRequest.getSearchRequest() instanceof AccommodationSearchRequest);
        AccommodationSearchRequest searchRequest = (AccommodationSearchRequest) searchMethodRequest.getSearchRequest();
        assertNull(searchRequest.getMainProduct());
        assertEquals(searchRequest.getRoomRequests().size(), 1);
        assertEquals(searchMethodRequest.getBuypath(), request.getBuyPath());
    }

    @Test
    public void testBuildRequestWithMainProduct() {
        setUpContext();
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest request = buildAccommodationRequestDTO(true);
        SearchMethodRequest searchMethodRequest = testClass.buildSearchRequest(request, context);

        assertNotNull(searchMethodRequest.getSearchRequest());
        assertTrue(searchMethodRequest.getSearchRequest() instanceof AccommodationSearchRequest);
        AccommodationSearchRequest searchRequest = (AccommodationSearchRequest) searchMethodRequest.getSearchRequest();
        assertNotNull(searchRequest.getMainProduct());
        assertEquals(searchRequest.getRoomRequests().size(), 1);

    }

    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest buildAccommodationRequestDTO(boolean withMainProduct) {
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest accommodationRequestDTO = new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest();
        accommodationRequestDTO.setCheckInDate("2021-10-01");
        accommodationRequestDTO.setCheckOutDate("2021-10-08");
        LocationRequest locationRequestDTO = new LocationRequest();
        locationRequestDTO.setGeoNodeId(9646);
        locationRequestDTO.setIata("BCN");
        accommodationRequestDTO.setDestination(locationRequestDTO);
        RoomRequest accommodationRoomRequestDTO = new RoomRequest();
        accommodationRoomRequestDTO.setNumAdults(2);
        accommodationRequestDTO.setRoomRequests(Collections.singletonList(accommodationRoomRequestDTO));
        accommodationRequestDTO.setSearchType(AccommodationSearchType.STANDALONE);
        accommodationRequestDTO.setMembershipFee(false);
        if (withMainProduct) {
            accommodationRequestDTO.setMainProduct(buildMainProduct());
        }
        return accommodationRequestDTO;
    }

    private MainProduct buildMainProduct() {
        MainProduct selectionRequestDTO = new MainProduct();
        selectionRequestDTO.setSearchId(123L);
        ItinerarySelection itinerarySelectionRequestDTO = new ItinerarySelection();
        itinerarySelectionRequestDTO.setFareItineraryKey("0,1A");
        itinerarySelectionRequestDTO.setSegmentKeys(Arrays.asList("0", "0"));
        selectionRequestDTO.setItinerarySelection(itinerarySelectionRequestDTO);
        return selectionRequestDTO;
    }


}
