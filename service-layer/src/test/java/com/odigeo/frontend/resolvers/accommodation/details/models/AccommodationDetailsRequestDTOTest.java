package com.odigeo.frontend.resolvers.accommodation.details.models;

import bean.test.BeanTest;

public class AccommodationDetailsRequestDTOTest extends BeanTest<AccommodationDetailsRequestDTO> {

    @Override
    protected AccommodationDetailsRequestDTO getBean() {
        AccommodationDetailsRequestDTO bean = new AccommodationDetailsRequestDTO();
        bean.setAccommodationDealKey("accommodationDealKey");
        bean.setSearchId(1234L);
        return bean;
    }
}
