package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering;

import com.google.inject.Guice;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.configuration.AirlineSteeringConfiguration;
import com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering.types.AirlineSteeringPosition;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering.AirlineSteeringHandlerTest.QATAR_AIRLINE;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AirlineSteeringManagerTest {

    @Mock
    private SiteVariations siteVariations;

    @Mock
    private VisitInformation visit;

    @Mock
    private AirlineSteeringConfiguration airlinesSteeringConfiguration;

    private List<String> airlines = Collections.singletonList(QATAR_AIRLINE);

    private AirlineSteeringManager manager;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        manager = new AirlineSteeringManager();
        Guice.createInjector(manager -> {
            manager.bind(SiteVariations.class).toProvider(() -> siteVariations);
            manager.bind(AirlineSteeringConfiguration.class).toProvider(() -> airlinesSteeringConfiguration);
        }).injectMembers(manager);

        when(manager.getAirlines()).thenReturn(airlines);
    }

    @Test
    public void testGetAirlines() {
        assertEquals(manager.getAirlines(), airlines);
    }

    @Test
    public void testNullPartition() {
        launchTest(AirlineSteeringPosition.NONE, null);
    }

    @Test
    public void testPartitionOne() {
        launchTest(AirlineSteeringPosition.NONE, 1);
    }

    @Test
    public void testPartitionTwo() {
        launchTest(AirlineSteeringPosition.LOW, 2);
    }

    @Test
    public void testPartitionThree() {
        launchTest(AirlineSteeringPosition.MEDIUM, 3);
    }

    @Test
    public void testPartitionFour() {
        launchTest(AirlineSteeringPosition.HIGH, 4);
    }


    private void launchTest(AirlineSteeringPosition boostingPosition, Integer partition) {
        when(siteVariations.getAirlineSteeringPartition(visit)).thenReturn(partition);
        assertEquals(manager.getAirlineSteeringPosition(visit), boostingPosition);
    }
}
