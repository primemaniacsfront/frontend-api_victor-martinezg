package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsThemeType;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class NonEssentialProductsThemeTypeMapperTest {

    private static final String THEME = "TEMPLATE";

    private NonEssentialProductsThemeTypeMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new NonEssentialProductsThemeTypeMapper();
    }

    @Test
    public void map() {
        AncillaryConfiguration ancillaryConfiguration = new AncillaryConfiguration();
        NonEssentialProductsThemeType nonEssentialProductsThemeType = NonEssentialProductsThemeType.DEFAULT;
        ancillaryConfiguration.setTheme(nonEssentialProductsThemeType.name());

        assertEquals(mapper.map(ancillaryConfiguration), nonEssentialProductsThemeType);
    }

    @Test
    public void mapShouldReturnNullIf() {
        AncillaryConfiguration ancillaryConfiguration = new AncillaryConfiguration();
        ancillaryConfiguration.setTheme(THEME);

        assertNull(mapper.map(ancillaryConfiguration));
    }
}
