package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HotelXSellingHandlerTest {

    private static final Site ENABLED_SITE = Site.ES;
    private static final Site DISABLED_SITE = Site.UNKNOWN;
    private static final String DISABLED_CARRIER = "9W";
    private static final int DESKTOP_MAX_NIGHTS = 30;
    private static final int MOBILE_MAX_NIGHTS = 7;

    private static final String DEPARTURE_DATE = "departureDate";
    private static final String ARRIVAL_DATE = "arrivalDate";
    private static final LocalDate DEPARTURE_LOCAL_DATE = LocalDate.of(2020, 1, 1);

    @Mock
    private DateUtils dateUtils;
    @Mock
    private VisitInformation visit;
    @Mock
    private SearchRequest searchRequest;
    @Mock
    private SiteVariations siteVariations;
    @Mock
    private SearchItineraryDTO itinerary;

    private HotelXSellingHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new HotelXSellingHandler();

        Guice.createInjector(binder -> {
            binder.bind(DateUtils.class).toProvider(() -> dateUtils);
            binder.bind(SiteVariations.class).toProvider(() -> siteVariations);
        }).injectMembers(handler);

        ItineraryRequest itineraryRequest = mock(ItineraryRequest.class);
        SegmentRequest departureSegment = mock(SegmentRequest.class);
        SegmentRequest arrivalSegment = mock(SegmentRequest.class);
        LocalDate arrivalLocalDate = DEPARTURE_LOCAL_DATE.plus(DESKTOP_MAX_NIGHTS - 1, ChronoUnit.DAYS);

        when(visit.getDevice()).thenReturn(Device.DESKTOP);
        when(visit.getSite()).thenReturn(ENABLED_SITE);
        when(searchRequest.getItinerary()).thenReturn(itineraryRequest);
        when(itineraryRequest.getSegments()).thenReturn(Arrays.asList(departureSegment, arrivalSegment));
        when(departureSegment.getDate()).thenReturn(DEPARTURE_DATE);
        when(arrivalSegment.getDate()).thenReturn(ARRIVAL_DATE);
        when(dateUtils.convertFromIsoToLocalDate(eq(DEPARTURE_DATE))).thenReturn(DEPARTURE_LOCAL_DATE);
        when(dateUtils.convertFromIsoToLocalDate(eq(ARRIVAL_DATE))).thenReturn(arrivalLocalDate);

        when(siteVariations.isDptIpTouchPointEnabled(eq(visit))).thenReturn(true);
        when(searchRequest.getTripType()).thenReturn(TripType.ROUND_TRIP);
        when(itinerary.getSectionsCarrierIds()).thenReturn(Collections.emptySet());
    }

    @Test
    public void testPopulateIsEnabled() {
        handler.populateIsEnabled(itinerary, searchRequest, visit);
        verify(itinerary).setHotelXSellingEnabled(eq(true));
    }

    @Test
    public void testPopulateIsDisabledBySiteVariation() {
        when(siteVariations.isDptIpTouchPointEnabled(eq(visit))).thenReturn(false);

        handler.populateIsEnabled(itinerary, searchRequest, visit);
        verify(itinerary).setHotelXSellingEnabled(eq(false));
    }

    @Test
    public void testPopulateIsDisabledByTripType() {
        when(searchRequest.getTripType()).thenReturn(TripType.ONE_WAY);

        handler.populateIsEnabled(itinerary, searchRequest, visit);
        verify(itinerary).setHotelXSellingEnabled(eq(false));
    }

    @Test
    public void testPopulateIsDisabledBySite() {
        when(visit.getSite()).thenReturn(DISABLED_SITE);

        handler.populateIsEnabled(itinerary, searchRequest, visit);
        verify(itinerary).setHotelXSellingEnabled(eq(false));
    }

    @Test
    public void testPopulateIsDisabledByMaxNights() {
        LocalDate arrivalLocalDate = DEPARTURE_LOCAL_DATE.plus(DESKTOP_MAX_NIGHTS, ChronoUnit.DAYS);
        when(dateUtils.convertFromIsoToLocalDate(eq(ARRIVAL_DATE))).thenReturn(arrivalLocalDate);

        handler.populateIsEnabled(itinerary, searchRequest, visit);
        verify(itinerary).setHotelXSellingEnabled(eq(false));
    }

    @Test
    public void testPopulateIsDisabledByMaxNightsMobile() {
        when(visit.getDevice()).thenReturn(Device.MOBILE);
        LocalDate arrivalLocalDate = DEPARTURE_LOCAL_DATE.plus(MOBILE_MAX_NIGHTS, ChronoUnit.DAYS);
        when(dateUtils.convertFromIsoToLocalDate(eq(ARRIVAL_DATE))).thenReturn(arrivalLocalDate);

        handler.populateIsEnabled(itinerary, searchRequest, visit);
        verify(itinerary).setHotelXSellingEnabled(eq(false));
    }

    @Test
    public void testPopulateIsDisabledByCarrier() {
        when(itinerary.getSectionsCarrierIds()).thenReturn(Collections.singleton(DISABLED_CARRIER));

        handler.populateIsEnabled(itinerary, searchRequest, visit);
        verify(itinerary).setHotelXSellingEnabled(eq(false));
    }

}
