package com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts;

import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryPolicyConfiguration;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class NonEssentialProductContextBuilderTest {

    @Mock
    private ShoppingCartContext shoppingCartContext;

    @Mock
    private NonEssentialProductsConfiguration nonEssentialProductsConfiguration;

    @Mock
    private AncillaryPolicyConfiguration ancillaryPolicyConfiguration;

    @Mock
    private AncillaryConfiguration ancillaryConfiguration;

    @BeforeMethod
    public void init() {
        initMocks(this);
        try {
            when(ancillaryPolicyConfiguration.get(ArgumentMatchers.<String>any())).thenReturn(ancillaryConfiguration);
            when(nonEssentialProductsConfiguration.getConfiguration(any())).thenReturn(ancillaryPolicyConfiguration);
        } catch (IOException ignored) {
            fail();
        }
    }

    private int randomInt(int max) {
        return (int) Math.floor(Math.random() * max + 1);
    }

    @Test
    public void testBuild() {
        int pax = randomInt(4);
        int segments = randomInt(3);

        NonEssentialProductContext context = new NonEssentialProductContextBuilder()
                .withShoppingCartContext(shoppingCartContext)
                .withPassengers(pax)
                .withSegments(segments)
                .withNonEssentialProductsConfiguration(nonEssentialProductsConfiguration)
                .build();

        assertEquals(context.getPassengers(), pax);
        assertEquals(context.getSegments(), segments);
        assertEquals(context.getShoppingCartContext(), shoppingCartContext);
        assertEquals(context.getAncillaryConfiguration(""), ancillaryConfiguration);
    }
}
