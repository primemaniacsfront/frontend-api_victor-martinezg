package com.odigeo.frontend.resolvers.checkout.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckoutResponse;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.BookingResponseStatus;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.checkout.CheckoutFetcherResult;
import com.odigeo.frontend.resolvers.checkout.mappers.CheckoutMapper;
import com.odigeo.frontend.resolvers.shoppingcart.handlers.ShoppingCartHandler;
import com.odigeo.frontend.resolvers.shoppingcart.models.ConfirmRequest;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static voldemort.utils.Utils.assertNotNull;

public class CheckoutResponseHandlerTest {

    private static final String USER_PAYMENT_INT_ID = "679389cb-6c73-4c78-8035-5dd839dd710c";

    @Mock
    private BookingResponse bookingResponse;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private CheckoutResponse checkoutResponse;
    @Mock
    private CheckoutMapper checkoutMapper;
    @Mock
    private ConfirmRequest confirmRequest;
    @Mock
    private ShoppingCartHandler shoppingCartHandler;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private ResolverContext context;
    @Mock
    private ShoppingCartSummaryResponse shoppingCart;
    @Mock
    private CheckoutFetcherResult checkoutFetcherResult;

    @InjectMocks
    private CheckoutResponseHandler checkoutResponseHandler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(checkoutMapper.buildShoppingCartConfirmRequest(any(), any(), any())).thenReturn(confirmRequest);
        when(checkoutMapper.mapShoppingCartToCheckoutResponse(any(), any())).thenReturn(checkoutResponse);
        when(checkoutMapper.mapShoppingCartToCheckoutFetcherResult(any(), any(), any())).thenReturn(checkoutFetcherResult);
        when(shoppingCartHandler.confirm(any(), any())).thenReturn(bookingResponse);
        when(bookingResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(bookingResponse.getStatus()).thenReturn(BookingResponseStatus.BOOKING_CONFIRMED);
        when(checkoutFetcherResult.getData()).thenReturn(checkoutResponse);

    }

    @Test
    public void testBuildCheckoutResponse() {
        CheckoutFetcherResult checkoutFetcherResult = checkoutResponseHandler.buildCheckoutResponse(bookingResponse, graphQLContext, USER_PAYMENT_INT_ID);
        assertNotNull(checkoutFetcherResult.getData());
        assertNotNull(checkoutFetcherResult.getErrors());
    }
}
