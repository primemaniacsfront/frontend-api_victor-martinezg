package com.odigeo.frontend.resolvers.accommodation.search.models;

import bean.test.BeanTest;


public class AccommodationRoomSelectionRequestDTOTest extends BeanTest<AccommodationRoomSelectionRequestDTO> {

    @Override
    protected AccommodationRoomSelectionRequestDTO getBean() {
        AccommodationRoomSelectionRequestDTO bean = new AccommodationRoomSelectionRequestDTO();
        bean.setAccommodationDealKey("dealKey");
        bean.setRoomGroupDealKey("roomDealKey");
        return bean;
    }
}
