package com.odigeo.frontend.resolvers.itinerary.mappers.leg;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertNull;

public class LegMapperTest {

    private LegMapper mapper;
    private ShoppingCartContext context;
    @Mock
    private VisitInformation visit;

    @BeforeMethod
    public void init() {
        openMocks(this);
        mapper = new LegMapperImpl();
        context = new ShoppingCartContext(visit);
    }

    @Test
    public void testMapSegmentListContractToModel() {
        assertNull(mapper.mapSegmentListContractToModel(null, context));
    }

    @Test
    public void testItinerariesLegendToModel() {
        assertNull(mapper.itinerariesLegendToModel(null, context));
    }

}

