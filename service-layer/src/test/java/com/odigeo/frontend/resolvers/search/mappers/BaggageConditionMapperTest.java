package com.odigeo.frontend.resolvers.search.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageCondition;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.BaggageIncluded;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class BaggageConditionMapperTest {

    private BaggageConditionMapper mapper;

    @BeforeClass
    public void init() {
        mapper = new BaggageConditionMapper();
    }

    @Test
    public void testMapChecking() {
        assertEquals(mapper.map(BaggageCondition.CHECKIN_INCLUDED), BaggageIncluded.CHECK_IN);
    }

    @Test
    public void testMapCabin() {
        assertEquals(mapper.map(BaggageCondition.CABIN_INCLUDED), BaggageIncluded.CABIN);
    }

}
