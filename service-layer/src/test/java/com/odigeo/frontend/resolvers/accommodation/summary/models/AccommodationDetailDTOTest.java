package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;

import java.util.Collections;

public class AccommodationDetailDTOTest extends BeanTest<AccommodationDetailDTO> {

    public AccommodationDetailDTO getBean() {
        AccommodationDetailDTO bean = new AccommodationDetailDTO();
        bean.setAccommodationDealInformation(new AccommodationDealInformationDTO());
        bean.setStateProvince("stateProvince");
        bean.setCountry("country");
        bean.setPostalCode("postalCode");
        bean.setMapUrls(Collections.EMPTY_LIST);
        bean.setLocationDescription("locationDescription");
        bean.setCheckInPolicy("checkInPolicy");
        bean.setCheckOutPolicy("checkOutPolicy");
        bean.setHotelPolicy("hotelPolicy");
        bean.setCreditCardTypes(Collections.EMPTY_LIST);
        bean.setPaymentMethod("paymentMethod");
        bean.setPhoneNumber("phoneNumber");
        bean.setMail("mail");
        bean.setWeb("web");
        bean.setFax("fax");
        bean.setAccommodationImages(Collections.EMPTY_LIST);
        bean.setFacilityGroups(Collections.EMPTY_LIST);
        bean.setAccommodationRoomServices(Collections.EMPTY_LIST);
        bean.setNumberOfRooms("numberOfRooms");
        bean.setHotelType(AccommodationType.UNKNOWN);
        bean.setDistanceFromCityCenter(123.0);

        return bean;
    }
}
