package com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductProviderType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsResponse;
import com.odigeo.dapi.client.Insurance;
import com.odigeo.dapi.client.InsuranceShoppingItem;
import com.odigeo.dapi.client.Itinerary;
import com.odigeo.dapi.client.ItineraryShoppingItem;
import com.odigeo.dapi.client.OtherProductsShoppingItems;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.TravellerInformationDescription;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsGroupsConfiguration;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class NonEssentialProductsMapperTest {

    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private OtherProductsShoppingItems otherProductsShoppingItems;
    @Mock
    private TravellerInformationDescription dummyTravellerInformation;
    @Mock
    private Itinerary itinerary;
    @Mock
    private ItineraryShoppingItem itineraryShoppingItem;
    @Mock
    private VisitInformation visit;

    private NonEssentialProductsMapper mapper;
    private ShoppingCartContext context;

    private static final int SEGMENTS = 2;
    private static final int PAX = 3;

    private static final Site SITE = Site.GB;
    private static final Device DEVICE = Device.DESKTOP;

    private static final String CANXSELF_POLICY = "CANXSELF";
    private static final int CANXSELF_PERCEIVED_PRICE = 100;
    private static final int CANXSELF_TOTAL_PRICE = CANXSELF_PERCEIVED_PRICE * PAX * SEGMENTS;

    private static final String FLEXSELF_POLICY = "FLEXSELF";
    private static final int FLEXSELF_PERCEIVED_PRICE = 30;
    private static final int FLEXSELF_TOTAL_PRICE = FLEXSELF_PERCEIVED_PRICE * PAX * SEGMENTS;

    private static final String FLEX3RD_POLICY = "FLEXUK164";
    private static final int FLEX3RD_PERCEIVED_PRICE = 20;
    private static final int FLEX3RD_TOTAL_PRICE = FLEX3RD_PERCEIVED_PRICE * PAX * SEGMENTS;

    private static final String CANX3RD_POLICY = "CNXFHL154";
    private static final int CANX3RD_PERCEIVED_PRICE = 80;
    private static final int CANX3RD_TOTAL_PRICE = CANX3RD_PERCEIVED_PRICE * PAX * SEGMENTS;

    @BeforeMethod
    public void init() {
        initMocks(this);
        mapper = new NonEssentialProductsMapperImpl();
        context = new ShoppingCartContext(visit);
        ConfigurationEngine.init();
        NonEssentialProductsGroupsConfiguration config = ConfigurationEngine.getInstance(NonEssentialProductsGroupsConfiguration.class);
        config.setGuarantees(new HashSet<>(Arrays.asList(CANXSELF_POLICY, FLEXSELF_POLICY)));
        config.setRebookings(new HashSet<>(Arrays.asList(FLEXSELF_POLICY, FLEX3RD_POLICY)));
    }

    private static InsuranceShoppingItem buildInsuranceItem(String policy, String provider, BigDecimal totalPrice) {
        InsuranceShoppingItem item = new InsuranceShoppingItem();
        Insurance insurance = new Insurance();
        insurance.setPolicy(policy);
        insurance.setProvider(provider);
        item.setInsurance(insurance);
        item.setId("Mock: ".concat(policy));
        item.setTotalPrice(totalPrice);
        return item;
    }
    private static InsuranceShoppingItem buildInsuranceItem(String policy, int totalPrice) {
        return buildInsuranceItem(policy, NonEssentialProductProviderType.NOSUPP.toString(), new BigDecimal(totalPrice));
    }

    private static void assertListIncludes(List<NonEssentialProduct> list, String policy) {
        assertTrue(list.stream().anyMatch(p -> policy.equals(p.getOfferId())));
    }

    private static void assertEmptyList(List<NonEssentialProduct> list) {
        assertNotNull(list);
        assertEquals(list.size(), 0);
    }

    private static void assertNoProducts(NonEssentialProductsResponse response) {
        assertNotNull(response);
        assertEmptyList(response.getGuarantees());
        assertEmptyList(response.getInsurances());
        assertEmptyList(response.getAdditions());
        assertEmptyList(response.getFareRules());
        assertEmptyList(response.getInsurances());
        assertEmptyList(response.getServiceOptions());
    }

    private void setup(List<InsuranceShoppingItem> insuranceShoppingItems, int segments, int pax) {
        when(visit.getSite()).thenReturn(SITE);
        when(visit.getDevice()).thenReturn(DEVICE);

        when(otherProductsShoppingItems.getInsuranceShoppingItems()).thenReturn(insuranceShoppingItems);
        when(shoppingCart.getOtherProductsShoppingItemContainer()).thenReturn(otherProductsShoppingItems);

        List<Integer> itinerarySegments = IntStream.range(0, segments).boxed().collect(Collectors.toList());
        when(itinerary.getSegments()).thenReturn(itinerarySegments);
        when(itineraryShoppingItem.getItinerary()).thenReturn(itinerary);
        when(shoppingCart.getItineraryShoppingItem()).thenReturn(itineraryShoppingItem);

        when(shoppingCart.getRequiredTravellerInformation()).thenReturn(Collections.nCopies(pax, dummyTravellerInformation));
    }

    private List<InsuranceShoppingItem> plentyOfInsuranceShoppingItems() {
        InsuranceShoppingItem canxselfShoppingItem = buildInsuranceItem(CANXSELF_POLICY, CANXSELF_TOTAL_PRICE);
        InsuranceShoppingItem flexselfShoppingItem = buildInsuranceItem(FLEXSELF_POLICY, FLEXSELF_TOTAL_PRICE);
        InsuranceShoppingItem flex3rdShoppingItem = buildInsuranceItem(FLEX3RD_POLICY, FLEX3RD_TOTAL_PRICE);
        InsuranceShoppingItem canx3rdShoppingItem = buildInsuranceItem(CANX3RD_POLICY, CANX3RD_TOTAL_PRICE);

        return Arrays.asList(canxselfShoppingItem, flexselfShoppingItem, flex3rdShoppingItem, canx3rdShoppingItem);
    }

    @Test
    public void testOtherProductsToResponseNullShoppingCart() {
        assertNull(mapper.otherProductsToResponse(null, context));
    }

    @Test
    public void testOtherProductsToResponseNullOtherProductsShoppingItems() {
        when(shoppingCart.getOtherProductsShoppingItemContainer()).thenReturn(null);
        assertNull(mapper.otherProductsToResponse(shoppingCart, context));
    }

    @Test
    public void testOtherProductsToResponseZeroSegments() {
        setup(plentyOfInsuranceShoppingItems(), 0, PAX);

        assertNull(mapper.otherProductsToResponse(shoppingCart, context));
    }

    @Test
    public void testOtherProductsToResponseZeroPax() {
        setup(plentyOfInsuranceShoppingItems(), SEGMENTS, 0);

        assertNull(mapper.otherProductsToResponse(shoppingCart, context));
    }

    @Test
    public void testOtherProductsToResponseNoProducts() {
        setup(Collections.emptyList(), SEGMENTS, PAX);

        assertNoProducts(mapper.otherProductsToResponse(shoppingCart, context));
    }

    @Test
    public void testOtherProductsToResponseFilledShoppingCart() {
        setup(plentyOfInsuranceShoppingItems(), SEGMENTS, PAX);

        NonEssentialProductsResponse response = mapper.otherProductsToResponse(shoppingCart, context);

        List<NonEssentialProduct> guarantees = response.getGuarantees();
        List<NonEssentialProduct> rebooking = response.getRebooking();
        assertEquals(guarantees.size(), 2);
        assertListIncludes(guarantees, CANXSELF_POLICY);
        assertListIncludes(guarantees, FLEXSELF_POLICY);
        assertEquals(rebooking.size(), 2);
        assertListIncludes(rebooking, FLEXSELF_POLICY);
        assertListIncludes(rebooking, FLEX3RD_POLICY);
        NonEssentialProduct flexselfProduct = guarantees.stream().filter(p -> FLEXSELF_POLICY.equals(p.getOfferId())).findFirst().orElse(null);
        assertNotNull(flexselfProduct);
        assertEquals(flexselfProduct.getPrice().getValue().getAmount().compareTo(new BigDecimal(FLEXSELF_PERCEIVED_PRICE)), 0);
        assertEmptyList(response.getAdditions());
        assertEmptyList(response.getFareRules());
        assertEmptyList(response.getInsurances());
        assertEmptyList(response.getServiceOptions());
    }
}
