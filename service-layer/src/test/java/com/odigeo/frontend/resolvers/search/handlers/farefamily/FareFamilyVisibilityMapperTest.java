package com.odigeo.frontend.resolvers.search.handlers.farefamily;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FareFamilyVisibilityMapperTest {

    private static final String CABIN_BAG = "CABIN_BAG";
    private static final String MIDDLE_SEAT_KEPT_VACANT = "MIDDLE_SEAT_KEPT_VACANT";
    private static final String OTHER_SERVICES = "OTHER_SERVICES";

    private static final String ALWAYS = "Always";
    private static final String EXPANDED = "Expanded";

    private FareFamilyVisibilityMapper mapper = new FareFamilyVisibilityMapper();

    @Test
    public void testMapAlwaysValue() {
        assertEquals(mapper.map(CABIN_BAG), ALWAYS);
    }

    @Test
    public void testMapExtendedValue() {
        assertEquals(mapper.map(MIDDLE_SEAT_KEPT_VACANT), EXPANDED);
    }

    @Test
    public void testMapUnknownValue() {
        assertEquals(mapper.map(OTHER_SERVICES), StringUtils.EMPTY);
    }
}
