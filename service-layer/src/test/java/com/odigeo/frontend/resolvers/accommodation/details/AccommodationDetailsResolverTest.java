package com.odigeo.frontend.resolvers.accommodation.details;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationDetailsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationDetailsResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.details.handlers.AccommodationDetailsHandler;
import com.odigeo.frontend.resolvers.accommodation.details.mappers.AccommodationDetailResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.details.models.AccommodationDetailsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationSearchCriteriaRetrieveHandler;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.provider.AccommodationProviderSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.product.ProductCategorySearchResult;
import com.odigeo.searchengine.v2.searchresults.product.accommodation.AccommodationDealSearchResults;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.accommodation.details.AccommodationDetailsResolver.ACCOMMODATION;
import static com.odigeo.frontend.resolvers.accommodation.details.AccommodationDetailsResolver.ACCOMMODATION_DETAILS;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class AccommodationDetailsResolverTest {

    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private AccommodationSearchCriteriaRetrieveHandler searchCriteriaRetrieveHandler;
    @Mock
    private AccommodationDetailResponseMapper accommodationDetailResponseMapper;
    @Mock
    private AccommodationDetailsHandler accommodationDetailsHandler;
    @Mock
    private SearchResults searchResults;
    @Mock
    private Map<String, ProductCategorySearchResult> searchProducts;
    @Mock
    private AccommodationDealSearchResults productCategorySearchResult;
    @Mock
    private ContentKeyMapper contentKeyMapper;
    @Mock
    private AccommodationProviderKeyDTO accommodationProviderKey;

    @InjectMocks
    private AccommodationDetailsResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(resolverContext);
        when(env.getGraphQlContext()).thenReturn(context);
        when(resolverContext.getVisitInformation()).thenReturn(visit);
        when(contentKeyMapper.extractProviderKeyFromContentKey(anyString())).thenReturn(accommodationProviderKey);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(ACCOMMODATION_DETAILS));
    }

    @Test
    public void testAccommodationDetailsFetcher() {
        Integer destinationGeoNode = 8;
        AccommodationDetailsRequest requestParams = mock(AccommodationDetailsRequest.class);
        AccommodationDetailsResponseDTO responseDTO = mock(AccommodationDetailsResponseDTO.class);
        AccommodationDetailsResponse accommodationDetailsResponse = mock(AccommodationDetailsResponse.class);

        when(requestParams.getSearchId()).thenReturn(1234L);
        when(requestParams.getContentKey()).thenReturn("bc#1234");
        when(jsonUtils.fromDataFetching(env, AccommodationDetailsRequest.class)).thenReturn(requestParams);

        prepareAccommodationDetailsMock(requestParams, destinationGeoNode);

        when(accommodationDetailsHandler.getAccommodationDetails(accommodationProviderKey, visit, destinationGeoNode)).thenReturn(responseDTO);
        when(accommodationDetailResponseMapper.map(responseDTO)).thenReturn(accommodationDetailsResponse);

        assertSame(resolver.accommodationDetailsFetcher(env), accommodationDetailsResponse);
        verify(contentKeyMapper).extractProviderKeyFromContentKey(requestParams.getContentKey());
    }

    @Test
    public void testAccommodationDetailsFetcherNotFound() {
        Integer destinationGeoNode = 8;
        AccommodationDetailsRequest requestParams = mock(AccommodationDetailsRequest.class);
        AccommodationDetailsResponseDTO responseDTO = mock(AccommodationDetailsResponseDTO.class);

        when(requestParams.getSearchId()).thenReturn(1234L);
        when(requestParams.getContentKey()).thenReturn("ex#1234");
        when(jsonUtils.fromDataFetching(env, AccommodationDetailsRequest.class)).thenReturn(requestParams);


        prepareAccommodationDetailsMock(requestParams, destinationGeoNode);
        when(accommodationDetailsHandler.getAccommodationDetails(accommodationProviderKey, visit, destinationGeoNode)).thenReturn(responseDTO);
        when(accommodationDetailResponseMapper.map(isNull())).thenReturn(null);

        assertSame(resolver.accommodationDetailsFetcher(env), null);
        verify(contentKeyMapper).extractProviderKeyFromContentKey(requestParams.getContentKey());
    }

    private void prepareAccommodationDetailsMock(AccommodationDetailsRequest requestParams, Integer destinationGeoNode) {
        AccommodationSearchCriteria searchCriteria = mock(AccommodationSearchCriteria.class);
        AccommodationProviderSearchCriteria accommodationProviderSearchCriteria = mock(AccommodationProviderSearchCriteria.class);
        when(searchCriteria.getProviderSearchCriteria()).thenReturn(accommodationProviderSearchCriteria);
        when(accommodationProviderSearchCriteria.getDestinationGeoNodeId()).thenReturn(destinationGeoNode);
        when(searchResults.getSearchCriteria()).thenReturn(searchCriteria);
        when(searchCriteriaRetrieveHandler.getSearchResultsBySearchId(requestParams.getSearchId(), visit.getVisitCode())).thenReturn(searchResults);
        when(searchResults.getCategoryResultsMap()).thenReturn(searchProducts);
        when(searchProducts.get(ACCOMMODATION)).thenReturn(productCategorySearchResult);
    }
}
