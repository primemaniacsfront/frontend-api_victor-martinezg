package com.odigeo.frontend.resolvers.prime.models;

import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowGroup;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;

import static org.testng.Assert.assertNotEquals;

public class RetentionFlowGroupTest {

    @Test(dataProvider = "test1")
    public void testBuilder(MembershipType membershipType, SourceType sourceType, Integer monthsDuration, LocalDate membershipExpirationDate, Collection<?> upcomingTrips) {

        RetentionFlowGroup result = RetentionFlowGroup.builder()
                .freeTrial(membershipType, sourceType, monthsDuration)
                .daysFromRenew(membershipExpirationDate)
                .upcomingTrips(upcomingTrips).build();
        assertNotEquals(result, RetentionFlowGroup.DEFAULT);
    }

    @DataProvider(name = "test1")
    public static Object[][] primeNumbers() {
        return new Object[][]{
                {MembershipType.BASIC_FREE, SourceType.FUNNEL_BOOKING, 0, LocalDate.now(), Collections.emptyList()},
                {MembershipType.BASIC, SourceType.FUNNEL_BOOKING, 1, LocalDate.now(), Collections.singleton("")},
                {null, null, null, null, null},
                {MembershipType.BASIC, null, 0, LocalDate.now(), Collections.emptyList()},
                {MembershipType.BASIC_FREE, null, 1, LocalDate.now(), Collections.emptyList()}};
    }
}
