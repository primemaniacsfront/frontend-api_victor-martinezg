package com.odigeo.frontend.resolvers.search.handlers;

import com.odigeo.searchengine.v2.common.CabinClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertSame;

public class CabinMapperTest {

    private static final CabinClass CABIN_CLASS = CabinClass.BUSINESS;

    private CabinMapper mapper;

    @BeforeMethod
    public void init() {
        mapper = new CabinMapper();
    }

    @Test
    public void testMap() {
        assertSame(mapper.map(CABIN_CLASS.name()), CABIN_CLASS);
    }

    @Test
    public void testMapUndefinedCabinClass() {
        assertSame(mapper.map("undefined"), CabinClass.TOURIST);
    }

    @Test
    public void testMapNullCabinClass() {
        assertSame(mapper.map(null), CabinClass.TOURIST);
    }

}
