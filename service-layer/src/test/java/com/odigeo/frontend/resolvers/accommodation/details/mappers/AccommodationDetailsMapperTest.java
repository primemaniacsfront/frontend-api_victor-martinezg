package com.odigeo.frontend.resolvers.accommodation.details.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationDealInformationMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationImagesMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.mappers.AccommodationFacilitiesMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.FacilityGroupsDTO;
import com.odigeo.hcsapi.v9.beans.Hotel;
import com.odigeo.hcsapi.v9.beans.HotelImage;
import com.odigeo.hcsapi.v9.beans.HotelImageQuality;
import com.odigeo.hcsapi.v9.beans.HotelImageType;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.hcsapi.v9.beans.HotelType;
import com.odigeo.hcsapi.v9.beans.Legend;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class AccommodationDetailsMapperTest {

    @Mock
    private AccommodationImagesMapper accommodationImagesMapper;
    @Mock
    private AccommodationFacilitiesMapper accommodationFacilitiesMapper;
    @Mock
    private AccommodationDealInformationMapper accommodationDealInformationMapper;
    @Mock
    private Hotel hotel;
    @Mock
    private Legend legend;
    @Mock
    private HotelSummary hotelSummary;
    @Mock
    private HotelImage hotelImage;

    @InjectMocks
    AccommodationDetailsMapper accommodationDetailsMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(hotel.getPostalCode()).thenReturn("postalCode");
        when(hotel.getCountry()).thenReturn("country");
        when(hotel.getCheckIn()).thenReturn("checkIn");
        when(hotel.getCheckOut()).thenReturn("checkOut");
        when(hotel.getHotelPolicy()).thenReturn("hotelPolicy");
        when(hotel.getPaymentMethod()).thenReturn("paymentMethod");
        when(hotel.getNumberOfRooms()).thenReturn("8");

        when(hotelImage.getUrl()).thenReturn("url");
        when(hotelImage.getThumbnail()).thenReturn("thumbnail");
        when(hotelImage.getType()).thenReturn(HotelImageType.FOOD);
        when(hotelImage.getQuality()).thenReturn(HotelImageQuality.MEDIUM);
    }

    @Test
    public void testMapHotelNull() {
        assertNull(accommodationDetailsMapper.map(null, null).getAccommodationDetail());
    }

    @Test
    public void testMap() {
        AccommodationDealInformationDTO dealInformationDTO = new AccommodationDealInformationDTO();
        when(accommodationDealInformationMapper.mapHcs(hotel)).thenReturn(dealInformationDTO);
        AccommodationDetailDTO response = accommodationDetailsMapper.map(hotel, legend).getAccommodationDetail();
        assertSame(response.getPostalCode(), hotel.getPostalCode());
        assertSame(response.getCountry(), hotel.getCountry());
        assertSame(response.getCheckInPolicy(), hotel.getCheckIn());
        assertSame(response.getCheckOutPolicy(), hotel.getCheckOut());
        assertSame(response.getHotelPolicy(), hotel.getHotelPolicy());
        assertSame(response.getPaymentMethod(), hotel.getPaymentMethod());
        assertSame(response.getNumberOfRooms(), hotel.getNumberOfRooms());
        assertTrue(response.getAccommodationImages().isEmpty());
        assertNull(response.getHotelType());
        assertSame(response.getAccommodationDealInformation(), dealInformationDTO);
        assertTrue(response.getAccommodationImages().isEmpty());
    }

    @Test
    public void testMapWithHotelSummary() {
        when(hotel.getHotelSummary()).thenReturn(hotelSummary);

        AccommodationDetailDTO response = accommodationDetailsMapper.map(hotel, legend).getAccommodationDetail();
        assertNull(response.getHotelType());

        when(hotelSummary.getType()).thenReturn(HotelType.BED_AND_BREAKFAST);
        response = accommodationDetailsMapper.map(hotel, legend).getAccommodationDetail();
        assertSame(response.getHotelType(), AccommodationType.BED_AND_BREAKFAST);

    }

    @Test
    public void testMapWithImages() {
        when(hotel.getHotelImages()).thenReturn(Arrays.asList(hotelImage, hotelImage));
        AccommodationImageDTO accommodationImageDTO = new AccommodationImageDTO();
        when(accommodationImagesMapper.mapHcsImage(hotelImage)).thenReturn(accommodationImageDTO);
        AccommodationDetailDTO response = accommodationDetailsMapper.map(hotel, legend).getAccommodationDetail();
        assertSame(response.getAccommodationImages().size(), hotel.getHotelImages().size());
        assertSame(response.getAccommodationImages().get(0), accommodationImageDTO);
    }

    @Test
    public void testMapFacilitiesGroup() {
        List<String> facilities = Collections.singletonList("facility");
        when(hotel.getHotelSummary()).thenReturn(hotelSummary);
        when(hotelSummary.getHotelFacilities()).thenReturn(facilities);
        List<FacilityGroupsDTO> facilitiesGroup = new ArrayList<>();
        when(accommodationFacilitiesMapper.mapFacilityGroups(facilities, legend)).thenReturn(facilitiesGroup);
        AccommodationDetailDTO response = accommodationDetailsMapper.map(hotel, legend).getAccommodationDetail();
        assertSame(response.getFacilityGroups(), facilitiesGroup);
    }
}
