package com.odigeo.frontend.resolvers.accommodation.staticcontent;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationStaticContentRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationStaticContentResponse;
import com.odigeo.accommodation.scoring.service.ScoringServiceException;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationStaticContentHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.mappers.AccommodationStaticContentResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.model.AccommodationStaticContentResponseDTO;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class AccommodationStaticContentResolverTest {

    private static final String ACCOMMODATION_STATIC_CONTENT_QUERY = "accommodationStaticContent";

    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private AccommodationStaticContentResponseMapper accommodationStaticContentResponseMapper;
    @Mock
    private AccommodationStaticContentHandler accommodationStaticContentHandler;
    @Mock
    private ContentKeyMapper contentKeyMapper;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private MetricsHandler metricsHandler;

    @InjectMocks
    private AccommodationStaticContentResolver resolver;


    @BeforeMethod
    public void init() throws ScoringServiceException {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(resolverContext);
        when(env.getGraphQlContext()).thenReturn(context);
        when(resolverContext.getVisitInformation()).thenReturn(visit);
        when(context.get(MetricsHandler.class)).thenReturn(metricsHandler);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(ACCOMMODATION_STATIC_CONTENT_QUERY));
    }

    @Test
    public void testSearchAccommodationFetcher() {
        AccommodationStaticContentResponseDTO responseDTO = mock(AccommodationStaticContentResponseDTO.class);
        AccommodationStaticContentResponse response = mock(AccommodationStaticContentResponse.class);
        AccommodationStaticContentRequest requestParams = mock(AccommodationStaticContentRequest.class);
        when(requestParams.getContentKeys()).thenReturn(Collections.singletonList("BC#123"));

        AccommodationProviderKeyDTO accommodationProviderKeyDTO = mock(AccommodationProviderKeyDTO.class);
        when(jsonUtils.fromDataFetching(env, AccommodationStaticContentRequest.class)).thenReturn(requestParams);
        when(contentKeyMapper.extractProviderKeyFromContentKey(anyString())).thenReturn(accommodationProviderKeyDTO);
        when(accommodationStaticContentHandler.retrieveStaticContent(visit, Collections.singletonList(accommodationProviderKeyDTO))).thenReturn(responseDTO);
        when(accommodationStaticContentResponseMapper.mapModelToContract(responseDTO)).thenReturn(response);

        assertSame(resolver.accommodationStaticContentFetcher(env), response);
    }
}
