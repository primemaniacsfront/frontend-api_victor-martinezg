package com.odigeo.frontend.resolvers.search.models;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class TrackingInfoDTOTest {

    @Test
    public void testGetReturningUserDeviceIdWhenNotSetted() {
        TrackingInfoDTO trackingInfo = new TrackingInfoDTO();
        assertEquals(trackingInfo.getReturningUserDeviceId(), StringUtils.EMPTY);
    }
}
