package com.odigeo.frontend.resolvers.itinerary.mappers.section;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import org.testng.annotations.Test;

import java.time.ZonedDateTime;

import static org.testng.Assert.assertEquals;

public class TechnicalStopMapperTest {

    @Test
    public void testSetStopDuration() {
        TechnicalStop stop = new TechnicalStop();
        TechnicalStopMapper mapper = new TechnicalStopMapperImpl();
        ZonedDateTime time = ZonedDateTime.now();
        stop.setDepartureDate(time);
        stop.setArrivalDate(time.plusHours(2));
        mapper.setStopDuration(stop);
        assertEquals(stop.getStopDuration(), 120);
    }

}

