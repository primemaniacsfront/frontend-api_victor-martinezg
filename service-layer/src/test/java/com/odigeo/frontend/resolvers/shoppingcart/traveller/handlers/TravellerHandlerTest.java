package com.odigeo.frontend.resolvers.shoppingcart.traveller.handlers;

import com.odigeo.dapi.client.ItineraryShoppingItem;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.dapi.client.Traveller;
import com.odigeo.dapi.client.TravellerInformationDescription;
import com.odigeo.frontend.resolvers.shoppingcart.traveller.mappers.ShoppingTravellerMapper;
import graphql.GraphQLContext;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TravellerHandlerTest {

    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    private ShoppingTravellerMapper mapper;
    @Mock
    private TravellerInformationDescription travellerInformationDescription;
    @Mock
    private Traveller traveller;

    @InjectMocks
    private TravellerHandler handler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testMapTravellerInformationNull() {
        handler.mapTravellerInformation(graphQLContext);
        verify(mapper).travellerListInfoToModel(isNull());
    }

    @Test
    public void testMapTravellersNull() {
        handler.mapTravellers(graphQLContext);
        verify(mapper).travellerListContractToModel(isNull());
    }

    @Test
    public void testMapTravellerInformation() {
        when(graphQLContext.get(eq(ShoppingCartSummaryResponse.class))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        List<TravellerInformationDescription> travellerInformationDescriptionList = Collections.singletonList(travellerInformationDescription);
        when(shoppingCart.getRequiredTravellerInformation()).thenReturn(travellerInformationDescriptionList);
        handler.mapTravellerInformation(graphQLContext);
        verify(mapper).travellerListInfoToModel(eq(travellerInformationDescriptionList));
    }

    @Test
    public void testMapTravellers() {
        when(graphQLContext.get(eq(ShoppingCartSummaryResponse.class))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getItineraryShoppingItem()).thenReturn(new ItineraryShoppingItem());
        List<Traveller> travellers = Collections.singletonList(traveller);
        when(shoppingCart.getTravellers()).thenReturn(travellers);
        handler.mapTravellers(graphQLContext);
        verify(mapper).travellerListContractToModel(eq(travellers));
    }

    @Test
    public void testMapTravellersCleaner() {
        when(graphQLContext.get(eq(ShoppingCartSummaryResponse.class))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getItineraryShoppingItem()).thenReturn(null);
        Traveller wrongTraveller = mock(Traveller.class);
        when(wrongTraveller.getName()).thenReturn(null);
        when(traveller.getName()).thenReturn("name");
        List<Traveller> travellers = Arrays.asList(traveller, wrongTraveller);
        when(shoppingCart.getTravellers()).thenReturn(travellers);
        handler.mapTravellers(graphQLContext);
        verify(mapper).travellerListContractToModel(eq(Collections.singletonList(traveller)));
    }
}
