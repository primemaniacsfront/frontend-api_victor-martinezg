package com.odigeo.frontend.resolvers.geolocation.country;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Country;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetCountriesRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetCountriesResponse;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.geolocation.country.handlers.CountryHandler;
import com.odigeo.geoapi.v4.GeoServiceException;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.odigeo.frontend.resolvers.geolocation.country.CountryResolver.GET_COUNTRIES_QUERY;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class CountryResolverTest {

    public static final String LOCALE = "es_ES";
    @Mock
    private JsonUtils jsonUtils;

    @Mock
    private DataFetchingEnvironment env;

    @Mock
    private CountryHandler countryHandler;

    @InjectMocks
    private CountryResolver resolver;

    @BeforeMethod
    public void init() throws GeoServiceException {
        MockitoAnnotations.openMocks(this);
        setupCountryHandler();
    }

    @Test
    public void addToBuilderQueries() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(GET_COUNTRIES_QUERY));
    }

    @Test
    public void testGetCountries() {
        GetCountriesRequest getCountriesRequest = new GetCountriesRequest();
        getCountriesRequest.setLocale(LOCALE);
        when(jsonUtils.fromDataFetching(env, GetCountriesRequest.class)).thenReturn(getCountriesRequest);
    }

    private void setupCountryHandler() throws GeoServiceException {
        Country country = new Country();
        country.setId(12);
        country.setName("SPAIN");
        country.setPhonePrefix("+34");
        country.setCode("ES");

        List<Country> countries = new ArrayList<>();
        countries.add(country);
        GetCountriesResponse getCountriesResponse = new GetCountriesResponse();
        getCountriesResponse.setCountries(countries);
        when(countryHandler.getCountries(any())).thenReturn(getCountriesResponse);
    }
}
