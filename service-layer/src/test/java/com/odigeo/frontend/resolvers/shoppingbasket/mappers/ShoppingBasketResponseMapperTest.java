package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceProduct;
import com.odigeo.shoppingbasket.v2.model.Product;
import com.odigeo.shoppingbasket.v2.responses.ShoppingBasketResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ShoppingBasketResponseMapperTest {

    private static final long SHOPPING_BASKET_ID = 1L;
    private static final List<InsuranceProduct> INSURANCE_PRODUCTS = Collections.singletonList(new InsuranceProduct());

    @Mock
    private InsuranceProductsMapper insuranceProductsMapper;

    @InjectMocks
    private ShoppingBasketResponseMapper shoppingBasketMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(insuranceProductsMapper.map(any())).thenReturn(INSURANCE_PRODUCTS);
    }

    @Test
    public void map() {
        ShoppingBasketResponse shoppingBasketResponse = new ShoppingBasketResponse();
        shoppingBasketResponse.setShoppingBasket(getShoppingBasket());

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketResponse shoppingBasket = shoppingBasketMapper.map(shoppingBasketResponse);

        assertEquals(shoppingBasket.getShoppingBasketId(), String.valueOf(SHOPPING_BASKET_ID));
        assertEquals(shoppingBasket.getInsurances(), INSURANCE_PRODUCTS);
    }

    private com.odigeo.shoppingbasket.v2.model.ShoppingBasket getShoppingBasket() {
        com.odigeo.shoppingbasket.v2.model.ShoppingBasket shoppingBasket = new com.odigeo.shoppingbasket.v2.model.ShoppingBasket();

        shoppingBasket.setId(SHOPPING_BASKET_ID);
        shoppingBasket.setProducts(Collections.singletonList(new Product()));

        return shoppingBasket;
    }

}
