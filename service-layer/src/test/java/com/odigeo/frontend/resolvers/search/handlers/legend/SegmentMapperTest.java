package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.google.inject.Guice;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;
import com.odigeo.searchengine.v2.responses.itinerary.Segment;
import com.odigeo.searchengine.v2.responses.itinerary.SegmentResult;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SegmentMapperTest {

    private static final String SECTION_INDEX_SEPARATOR = "_";

    @Mock
    private SectionMapper sectionMapper;
    @Mock
    private TransportHandler transportHandler;

    private SegmentMapper segmentMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        segmentMapper = new SegmentMapper();

        Guice.createInjector(binder -> {
            binder.bind(SectionMapper.class).toProvider(() -> sectionMapper);
            binder.bind(TransportHandler.class).toProvider(() -> transportHandler);
        }).injectMembers(segmentMapper);
    }

    @Test
    public void testBuildSegmentMap() {
        SearchEngineContext seContext = mock(SearchEngineContext.class);
        SegmentResult segmentResult = mock(SegmentResult.class);
        SectionResult sectionResult = mock(SectionResult.class);
        Segment segment = mock(Segment.class);
        Carrier carrierDTO = mock(Carrier.class);
        SectionDTO sectionDTO = mock(SectionDTO.class);
        Set<TransportType> transportTypes = new HashSet<>(0);

        Integer segmentId = 1;
        Integer carrierId = 2;
        Integer sectionId = 3;
        Integer seats = 10;
        Long duration = 1L;

        when(seContext.getSegmentResults()).thenReturn(Collections.singletonList(segmentResult));
        when(seContext.getCarrierMap()).thenReturn(Collections.singletonMap(carrierId, carrierDTO));
        when(seContext.getSectionResultMap()).thenReturn(Collections.singletonMap(sectionId, sectionResult));

        when(segmentResult.getSegment()).thenReturn(segment);
        when(segmentResult.getId()).thenReturn(segmentId);
        when(segment.getCarrier()).thenReturn(carrierId);
        when(segment.getSeats()).thenReturn(seats);
        when(segment.getDuration()).thenReturn(duration);
        when(segment.getSections()).thenReturn(Collections.singletonList(sectionId));

        when(sectionMapper.buildSection(eq(sectionResult), eq(seContext))).thenReturn(sectionDTO);

        when(transportHandler.calculateSegmentTransportTypes(argThat(argument ->
            argument.size() == 1 && argument.get(0) == sectionDTO))).thenReturn(transportTypes);

        Map<Integer, SegmentDTO> segmentMap = segmentMapper.buildSegmentMap(seContext);
        SegmentDTO segmentDTO = segmentMap.get(segmentId);

        assertEquals(segmentMap.size(), 1);
        assertEquals(segmentDTO.getId(), segmentId);
        assertEquals(segmentDTO.getCarrier(), carrierDTO);
        assertEquals(segmentDTO.getDuration(), duration);
        assertEquals(segmentDTO.getSeats(), seats);
        assertEquals(segmentDTO.getTransportTypes(), transportTypes);

        assertEquals(segmentDTO.getSections().size(), 1);
        assertEquals(segmentDTO.getSections().get(0), sectionDTO);
        verify(sectionDTO).setId(eq(sectionId + SECTION_INDEX_SEPARATOR + 0));
    }

}
