package com.odigeo.frontend.resolvers.accommodation.search.mappers;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.google.inject.Guice;
import com.odigeo.accommodation.scoring.service.DeviceType;
import com.odigeo.accommodation.scoring.service.GetScoresRequest;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationScoringRequestDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationPriceCalculatorDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.frontend.services.GeoService;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Coordinates;
import com.odigeo.geoapi.v4.utils.LocalizedText;
import com.odigeo.searchengine.v2.requests.AccommodationRoomRequest;
import com.odigeo.searchengine.v2.requests.LocationRequest;
import com.odigeo.searchengine.v2.responses.accommodation.SearchType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AccommodationScoringRequestMapperTest {

    private AccommodationScoringRequestMapper testClass;

    @Mock
    private GeoService geoService;
    @Mock
    private VisitInformation visit;
    @Mock
    private City geoNode;
    @Mock
    private LocalizedText localizedText;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        testClass = new AccommodationScoringRequestMapper();

        Guice.createInjector(binder -> {
            binder.bind(GeoService.class).toProvider(() -> geoService);
        }).injectMembers(testClass);
    }

    @Test
    public void test() throws GeoNodeNotFoundException {
        Long searchId = 123L;
        Calendar checkIn = Calendar.getInstance();
        checkIn.set(2001, Calendar.JANUARY, 1);
        Calendar checkOut = Calendar.getInstance();
        checkOut.set(2001, Calendar.JANUARY, 2);
        List<AccommodationRoomRequest> roomRequests = Collections.singletonList(new AccommodationRoomRequest());
        SearchType searchType = SearchType.UPSELL;
        LocationRequest destination = new LocationRequest();
        destination.setGeoNodeId(123);
        destination.setIataCode("abc");
        AccommodationResponseDTO response = getAccommodationResponseDTO();
        when(visit.getLocale()).thenReturn("es_ES");
        when(visit.getCurrency()).thenReturn("EUR");
        when(visit.getSite()).thenReturn(Site.ES);
        when(visit.getDevice()).thenReturn(Device.DESKTOP);
        when(geoService.getCityByGeonodeId(anyInt())).thenReturn(geoNode);
        when(geoService.getCityByIataCode(anyString())).thenReturn(geoNode);
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(BigDecimal.ONE);
        coordinates.setLongitude(BigDecimal.ONE);
        when(geoNode.getCoordinates()).thenReturn(coordinates);
        when(geoNode.getName()).thenReturn(localizedText);
        when(localizedText.getText(any(Locale.class))).thenReturn("name");

        GetScoresRequest result = testClass.map(new AccommodationScoringRequestDTO(searchId, checkIn, checkOut, roomRequests, searchType, destination, response, visit));
        assertEquals(result.getSearchId(), searchId);
        assertEquals(result.getCheckInDate(), "2001-01-01");
        assertEquals(result.getCheckOutDate(), "2001-01-02");
        assertEquals(result.getDevice(), DeviceType.DESKTOP);
        assertEquals(result.getAccommodations().size(), 1);
        assertEquals(result.getWebsite(), "ES");
        assertEquals(result.getDestinationName(), "name");
    }

    private AccommodationResponseDTO getAccommodationResponseDTO() {
        AccommodationResponseDTO accommodationResponseDTO = new AccommodationResponseDTO();
        SearchAccommodationDTO searchAccommodationDTO = new SearchAccommodationDTO();
        searchAccommodationDTO.setDedupId(1);
        searchAccommodationDTO.setContentKey("a#b");
        searchAccommodationDTO.setProviderPrice(new Money(BigDecimal.ONE, "EUR"));
        AccommodationPriceCalculatorDTO accommodationPriceCalculatorDTO = new AccommodationPriceCalculatorDTO();
        accommodationPriceCalculatorDTO.setSortPrice(BigDecimal.ONE);
        searchAccommodationDTO.setPriceCalculator(accommodationPriceCalculatorDTO);
        accommodationResponseDTO.setAccommodations(Collections.singletonList(searchAccommodationDTO));
        return accommodationResponseDTO;
    }

}
