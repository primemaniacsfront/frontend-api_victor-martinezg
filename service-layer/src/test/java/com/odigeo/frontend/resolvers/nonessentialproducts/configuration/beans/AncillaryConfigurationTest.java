package com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans;

import bean.test.BeanTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AncillaryConfigurationTest extends BeanTest<AncillaryConfiguration> {

    private final String theme = "template";
    private final boolean mandatory = true;
    private final boolean rejectNagEnabled = false;
    private final boolean prechecked = true;
    private final boolean continuanceNagEnabled = false;
    private final boolean isModalActive = true;
    private final boolean showModalDualContract = false;
    private final boolean isAttachableFromModal = false;
    private final boolean showModalBenefits = false;
    private final boolean showModalExclusions = false;
    private final PricePerceptionType pricePerception = PricePerceptionType.PERPAX;
    private final boolean ipid = true;
    private final boolean highlighted = true;

    @Override
    public AncillaryConfiguration getBean() {
        final AncillaryConfiguration ancillaryConfiguration = new AncillaryConfiguration();
        ancillaryConfiguration.setTheme(theme);
        ancillaryConfiguration.setMandatory(mandatory);
        ancillaryConfiguration.setRejectNagEnabled(rejectNagEnabled);
        ancillaryConfiguration.setPrechecked(prechecked);
        ancillaryConfiguration.setContinuanceNagEnabled(continuanceNagEnabled);
        ancillaryConfiguration.setIsModalActive(isModalActive);
        ancillaryConfiguration.setShowModalDualContract(showModalDualContract);
        ancillaryConfiguration.setIsAttachableFromModal(isAttachableFromModal);
        ancillaryConfiguration.setShowModalBenefits(showModalBenefits);
        ancillaryConfiguration.setShowModalExclusions(showModalExclusions);
        ancillaryConfiguration.setPricePerception(pricePerception);
        ancillaryConfiguration.setIpid(ipid);
        ancillaryConfiguration.setHighlighted(highlighted);
        return ancillaryConfiguration;
    }

    @Test
    public void cloneMe() {
        AncillaryConfiguration bean = getBean();
        AncillaryConfiguration ancillaryConfiguration = bean.cloneMe();
        assertEquals(bean.getTheme(), ancillaryConfiguration.getTheme());
        assertEquals(bean.getMandatory(), ancillaryConfiguration.getMandatory());
        assertEquals(bean.isRejectNagEnabled(), ancillaryConfiguration.isRejectNagEnabled());
        assertEquals(bean.getPrechecked(), ancillaryConfiguration.getPrechecked());
        assertEquals(bean.isContinuanceNagEnabled(), ancillaryConfiguration.isContinuanceNagEnabled());
        assertEquals(bean.getIsModalActive(), ancillaryConfiguration.getIsModalActive());
        assertEquals(bean.getShowModalDualContract(), ancillaryConfiguration.getShowModalDualContract());
        assertEquals(bean.getIsAttachableFromModal(), ancillaryConfiguration.getIsAttachableFromModal());
        assertEquals(bean.getPricePerception(), ancillaryConfiguration.getPricePerception());
        assertEquals(bean.getIpid(), ancillaryConfiguration.getIpid());
        assertEquals(bean.getHighlighted(), ancillaryConfiguration.getHighlighted());
    }

}
