package com.odigeo.frontend.resolvers.accommodation.search.handlers;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationRequest;
import com.odigeo.accommodation.scoring.service.ScoringServiceException;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationLocationHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationSortingHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationStaticContentHandler;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.AccommodationSearchResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.searchengine.v2.requests.SearchMethodRequest;
import com.odigeo.searchengine.v2.responses.SearchResponse;
import graphql.GraphQLContext;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class AccommodationSearchHandlerTest {

    @Mock
    private AccommodationSearchRequestHandler accommodationSearchRequestHandler;
    @Mock
    private AccommodationSearchResponseHandler accommodationSearchResponseHandler;
    @Mock
    private SearchService searchService;
    @Mock
    private AccommodationSortingHandler accommodationSortingHandler;
    @Mock
    private AccommodationStaticContentHandler accommodationStaticContentHandler;
    @Mock
    private AccommodationMetadataHandler accommodationMetadataHandler;
    @Mock
    private AccommodationSearchReviewsHandler accommodationSearchReviewsHandler;
    @Mock
    private AccommodationSearchResponseMapper accommodationSearchResponseMapper;
    @Mock
    private AccommodationLocationHandler accommodationLocationHandler;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private DateUtils dateUtils;

    @InjectMocks
    private AccommodationSearchHandler testClass;


    @BeforeMethod
    public void init() throws ScoringServiceException {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testSearchAccommodationFetcher() {
        SearchResponse searchResponse = mock(SearchResponse.class);
        SearchMethodRequest searchRequest = mock(SearchMethodRequest.class);
        AccommodationSearchResponseDTO responseDTO = mock(AccommodationSearchResponseDTO.class);
        AccommodationSearchResponse response = mock(AccommodationSearchResponse.class);
        AccommodationSearchRequest requestParams = mock(AccommodationSearchRequest.class);
        LocationRequest locationRequest = mock(LocationRequest.class);

        when(requestParams.getDestination()).thenReturn(locationRequest);
        when(requestParams.getCheckInDate()).thenReturn("aDate");
        when(requestParams.getCheckOutDate()).thenReturn("aDate");
        when(dateUtils.daysBetweenIsoDates(anyString(), anyString())).thenReturn(1);
        when(locationRequest.getGeoNodeId()).thenReturn(123);

        when(accommodationSearchRequestHandler.buildSearchRequest(requestParams, resolverContext)).thenReturn(searchRequest);
        when(searchService.executeSearch(searchRequest)).thenReturn(searchResponse);
        when(accommodationSearchResponseHandler.buildSearchResponse(searchResponse, visit, 1)).thenReturn(responseDTO);
        when(accommodationSearchResponseMapper.mapModelToContract(responseDTO)).thenReturn(response);

        assertSame(testClass.searchAccommodation(requestParams, resolverContext, visit), response);
    }

}
