package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationImageQualityDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class AccommodationImageHandlerTest {

    private static final String URL_A = "http://www.example.com/image1.jpg";
    private static final String URL_B = "http://www.example.com/image2.jpg";
    private static final String URL_INVALID = "url";

    @Mock
    private Logger logger;
    @InjectMocks
    private AccommodationImageHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testRepeatedImagesWithMainImage() {
        AccommodationDetailDTO accommodationDetail = new AccommodationDetailDTO();
        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();

        AccommodationImageDTO imageA = new AccommodationImageDTO();
        imageA.setUrl(URL_A);
        imageA.setQuality(AccommodationImageQualityDTO.HIGH);
        accommodationDealInformationDTO.setMainAccommodationImage(imageA);
        accommodationDetail.setAccommodationDealInformation(accommodationDealInformationDTO);

        AccommodationImageDTO imageB = new AccommodationImageDTO();
        imageB.setUrl(URL_B);
        imageB.setQuality(AccommodationImageQualityDTO.HIGH);
        accommodationDetail.setAccommodationImages(new ArrayList<>());
        accommodationDetail.getAccommodationImages().add(imageA);
        accommodationDetail.getAccommodationImages().add(imageA);
        accommodationDetail.getAccommodationImages().add(imageB);
        accommodationDetail.getAccommodationImages().add(imageB);

        handler.removeDuplicatedImages(accommodationDetail);

        assertEquals(accommodationDetail.getAccommodationImages().size(), 2);
        assertEquals(accommodationDetail.getAccommodationImages().get(0), imageA);
        assertEquals(accommodationDetail.getAccommodationImages().get(1), imageB);
        assertEquals(accommodationDetail.getAccommodationDealInformation().getMainAccommodationImage(), imageA);
    }

    @Test
    public void testMainImageOneTwoQualities() {
        AccommodationDetailDTO accommodationDetail = new AccommodationDetailDTO();
        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();
        accommodationDetail.setAccommodationDealInformation(accommodationDealInformationDTO);

        AccommodationImageDTO imageA = new AccommodationImageDTO();
        imageA.setUrl(URL_A);
        imageA.setQuality(AccommodationImageQualityDTO.LOW);
        accommodationDealInformationDTO.setMainAccommodationImage(imageA);
        accommodationDetail.setAccommodationDealInformation(accommodationDealInformationDTO);

        AccommodationImageDTO imageBHigh = new AccommodationImageDTO();
        imageBHigh.setUrl(URL_B);
        imageBHigh.setQuality(AccommodationImageQualityDTO.HIGH);
        AccommodationImageDTO imageBUnknown = new AccommodationImageDTO();
        imageBUnknown.setUrl(URL_B);
        imageBUnknown.setQuality(AccommodationImageQualityDTO.HIGH);
        accommodationDetail.setAccommodationImages(new ArrayList<>());
        accommodationDetail.getAccommodationImages().add(imageBUnknown);
        accommodationDetail.getAccommodationImages().add(imageBHigh);

        handler.removeDuplicatedImages(accommodationDetail);

        assertEquals(accommodationDetail.getAccommodationImages().size(), 2);
        assertEquals(accommodationDetail.getAccommodationImages().get(0), imageA);
        assertEquals(accommodationDetail.getAccommodationImages().get(1).getUrl(), imageBHigh.getUrl());
        assertEquals(accommodationDetail.getAccommodationImages().get(1).getThumbnailUrl(), imageBHigh.getThumbnailUrl());
        assertEquals(accommodationDetail.getAccommodationImages().get(1).getQuality(), imageBHigh.getQuality());
        assertEquals(accommodationDetail.getAccommodationImages().get(1).getType(), imageBHigh.getType());
        assertEquals(accommodationDetail.getAccommodationDealInformation().getMainAccommodationImage(), imageA);
    }

    @Test
    public void testRepeatedImagesNoMainImage() {
        AccommodationDetailDTO accommodationDetail = new AccommodationDetailDTO();
        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();
        accommodationDetail.setAccommodationDealInformation(accommodationDealInformationDTO);

        AccommodationImageDTO imageA = new AccommodationImageDTO();
        imageA.setUrl(URL_A);
        imageA.setQuality(AccommodationImageQualityDTO.HIGH);
        AccommodationImageDTO imageB = new AccommodationImageDTO();
        imageB.setUrl(URL_B);
        imageB.setQuality(AccommodationImageQualityDTO.HIGH);
        accommodationDetail.setAccommodationImages(new ArrayList<>());
        accommodationDetail.getAccommodationImages().add(imageA);
        accommodationDetail.getAccommodationImages().add(imageA);
        accommodationDetail.getAccommodationImages().add(imageB);
        accommodationDetail.getAccommodationImages().add(imageB);

        handler.removeDuplicatedImages(accommodationDetail);

        assertEquals(accommodationDetail.getAccommodationImages().size(), 2);
        assertEquals(accommodationDetail.getAccommodationImages().get(0), imageA);
        assertEquals(accommodationDetail.getAccommodationImages().get(1), imageB);
        assertNull(accommodationDetail.getAccommodationDealInformation().getMainAccommodationImage());
    }

    @Test
    public void testWrongUrlImages() {
        AccommodationDetailDTO accommodationDetail = new AccommodationDetailDTO();
        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();
        accommodationDetail.setAccommodationDealInformation(accommodationDealInformationDTO);

        AccommodationImageDTO imageA = new AccommodationImageDTO();
        imageA.setUrl(URL_INVALID);
        imageA.setQuality(AccommodationImageQualityDTO.HIGH);
        AccommodationImageDTO imageB = new AccommodationImageDTO();
        imageB.setUrl(URL_INVALID);
        imageB.setQuality(AccommodationImageQualityDTO.HIGH);
        accommodationDetail.setAccommodationImages(new ArrayList<>());
        accommodationDetail.getAccommodationImages().add(imageB);

        handler.removeDuplicatedImages(accommodationDetail);

        assertEquals(accommodationDetail.getAccommodationImages().size(), 0);
        assertNull(accommodationDetail.getAccommodationDealInformation().getMainAccommodationImage());
    }

    @Test
    public void testNoImages() {
        AccommodationDetailDTO accommodationDetail = new AccommodationDetailDTO();
        accommodationDetail.setAccommodationImages(new ArrayList<>());

        handler.removeDuplicatedImages(accommodationDetail);

        assertTrue(accommodationDetail.getAccommodationImages().isEmpty());
        assertNull(accommodationDetail.getAccommodationDealInformation());
    }

}
