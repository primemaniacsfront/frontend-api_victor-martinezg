package com.odigeo.frontend.resolvers.accommodation.search.models;


import bean.test.BeanTest;

public class AccommodationSearchResponseDTOTest extends BeanTest<AccommodationSearchResponseDTO> {

    @Override
    protected AccommodationSearchResponseDTO getBean() {
        AccommodationSearchResponseDTO bean = new AccommodationSearchResponseDTO();
        bean.setAccommodationResponse(new AccommodationResponseDTO());
        bean.setSearchId(123L);
        return bean;
    }
}
