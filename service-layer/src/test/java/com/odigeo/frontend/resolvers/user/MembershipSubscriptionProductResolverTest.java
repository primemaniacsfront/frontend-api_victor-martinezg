package com.odigeo.frontend.resolvers.user;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GetMembershipProductRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateMembershipProductRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.prime.handlers.MembershipSubscriptionProductHandler;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import com.odigeo.frontend.resolvers.user.mappers.ModifyMembershipSubscriptionProductMapper;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.user.MembershipSubscriptionProductResolver.MEMBERSHIP_PRODUCT;
import static com.odigeo.frontend.resolvers.user.MembershipSubscriptionProductResolver.UPDATE_MEMBERSHIP_PRODUCT;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class MembershipSubscriptionProductResolverTest {
    private static final String QUERY = "Query";
    private static final String MUTATION = "Mutation";
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private MembershipSubscriptionProductHandler membershipSubscriptionProductHandler;
    @Mock
    private ModifyMembershipSubscriptionProductMapper modifyMembershipSubscriptionProductMapper;
    @Mock
    private ResolverContext context;
    @Mock
    private UpdateMembershipProductRequest updateMembershipProductRequest;
    @Mock
    private GetMembershipProductRequest getMembershipProductRequest;
    @Mock
    private ModifyMembershipSubscription modifyMembershipSubscription;
    @Mock
    private ShoppingInfo shoppingInfo;
    @InjectMocks
    private MembershipSubscriptionProductResolver membershipSubscriptionProductResolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        membershipSubscriptionProductResolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(QUERY);
        Map<String, DataFetcher> mutations = runtimeWiring.getDataFetchers().get(MUTATION);

        assertNotNull(queries.get(MEMBERSHIP_PRODUCT));
        assertNotNull(mutations.get(UPDATE_MEMBERSHIP_PRODUCT));
    }

    @Test
    public void testGetMembershipDataFetcher() {
        given(jsonUtils.fromDataFetching(eq(env), eq(GetMembershipProductRequest.class)))
                .willReturn(getMembershipProductRequest);
        given(getMembershipProductRequest.getShoppingInfo())
                .willReturn(shoppingInfo);
        membershipSubscriptionProductResolver.membershipSubscriptionProductDataFetcher(env);
        verify(membershipSubscriptionProductHandler).getSubscriptionFromShoppingCart(eq(shoppingInfo), eq(context));
    }

    @Test
    public void testUpdateMembershipDataFetcher() {
        given(jsonUtils.fromDataFetching(eq(env), eq(UpdateMembershipProductRequest.class)))
                .willReturn(updateMembershipProductRequest);
        given(modifyMembershipSubscriptionProductMapper.fromUpdateMembershipRequest(updateMembershipProductRequest))
                .willReturn(modifyMembershipSubscription);
        membershipSubscriptionProductResolver.updateMembershipProductResolver(env);
        verify(membershipSubscriptionProductHandler).updateMembershipSubscription(eq(modifyMembershipSubscription), eq(context));
    }
}
