package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.Segment;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.section.FlightSection;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class FootprintSegmentMapperTest {

    @Mock
    private FootprintSectionMapper sectionMapper;

    @InjectMocks
    private FootprintSegmentMapper mapper;

    private static final Integer SEGMENT_ID = 1;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(sectionMapper.map(any(), any())).thenReturn(new FlightSection());
    }

    @Test
    public void testMap() {
        Segment segment = mapper.map(getSegmentDTOMock());
        assertNotNull(segment);
        assertNotNull(segment.getSections());
        assertEquals(segment.getSections().size(), 1);
        assertEquals(segment.getSegmentIndex(), SEGMENT_ID);
    }

    private SegmentDTO getSegmentDTOMock() {
        SegmentDTO segmentDTO = new SegmentDTO();
        segmentDTO.setId(SEGMENT_ID);
        segmentDTO.setSections(Collections.singletonList(new SectionDTO()));
        return segmentDTO;
    }

}
