package com.odigeo.frontend.resolvers.prime.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.google.inject.Guice;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowGroup;
import com.odigeo.frontend.resolvers.prime.models.retention.RetentionFlowInfo;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.membership.MemberUserArea;
import com.odigeo.membership.response.FutureFlight;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class RetentionFlowInfoHandlerTest {

    private static final String BASIC = "BASIC";
    private static final String FUNNEL_BOOKING = "FUNNEL_BOOKING";
    private static final long MEMBERSHIP_ID = 1234L;
    @Mock
    private MembershipHandler membershipHandler;
    @Mock
    private MemberUserArea memberUserArea;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private Membership membership;
    private RetentionFlowInfoHandler handler;
    private List<FutureFlight> flights;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new RetentionFlowInfoHandler();
        flights = new ArrayList<>();
        Guice.createInjector(binder -> {
            binder.bind(MembershipHandler.class).toProvider(() -> membershipHandler);
            binder.bind(MemberUserArea.class).toProvider(() -> memberUserArea);
            binder.bind(MetricsHandler.class).toProvider(() -> metricsHandler);
        }).injectMembers(handler);

        when(membershipHandler.getMembershipFromLoggedUser(env)).thenReturn(membership);
        when(membership.getId()).thenReturn(MEMBERSHIP_ID);
        when(memberUserArea.getFutureFlights(MEMBERSHIP_ID)).thenReturn(flights);
    }

    public FutureFlight setupFlight() {
        FutureFlight flight = mock(FutureFlight.class);
        String departureDate = "dd/mm/yyyy";
        String destination = "mock";

        when(flight.getDepartureDate()).thenReturn(departureDate);
        when(flight.getDestination()).thenReturn(destination);

        return flight;
    }

    @Test
    public void testTracking() {
        handler.getFutureFlights(MEMBERSHIP_ID);
        verify(metricsHandler, times(1)).trackCounter(MeasureConstants.MEMBERSHIP_USER_AREA_GET_FUTURE_FLIGHTS);
    }

    @Test
    public void testBuildRetentionFlowInfoForBasicWithTrip() {
        String expirationDate = LocalDate.now().plusDays(276).toString();
        FutureFlight flight = setupFlight();
        flights.add(flight);

        when(membership.getExpirationDate()).thenReturn(expirationDate);

        RetentionFlowInfo retentionFlowInfo = handler.buildRetentionFlowInfo(env);
        assertEquals(retentionFlowInfo.getFlow(), RetentionFlowGroup.BASIC_LONG_WITH_TRIP);
        assertEquals(retentionFlowInfo.getTravel().getArrivalCity(), flight.getDestination());
        assertEquals(retentionFlowInfo.getTravel().getDate(), flight.getDepartureDate());
    }

    @Test
    public void testBuildRetentionFlowInfoForBasicNoTrip() {
        String expirationDate = LocalDate.now().plusDays(276).toString();

        when(membership.getExpirationDate()).thenReturn(expirationDate);

        RetentionFlowInfo retentionFlowInfo = handler.buildRetentionFlowInfo(env);
        assertEquals(retentionFlowInfo.getFlow(), RetentionFlowGroup.BASIC_LONG_NO_TRIP);
        assertNull(retentionFlowInfo.getTravel());
    }

    @Test
    public void testBuildRetentionFlowInfoForBasicShortNoTrip() {
        String expirationDate = LocalDate.now().plusDays(270).toString();

        when(membership.getExpirationDate()).thenReturn(expirationDate);

        RetentionFlowInfo retentionFlowInfo = handler.buildRetentionFlowInfo(env);
        assertEquals(retentionFlowInfo.getFlow(), RetentionFlowGroup.BASIC_SHORT_NO_TRIP);
        assertNull(retentionFlowInfo.getTravel());
    }

    @Test
    public void testBuildRetentionFlowInfoForBasicShortTrip() {
        String expirationDate = LocalDate.now().plusDays(270).toString();
        FutureFlight flight = setupFlight();
        flights.add(flight);

        when(membership.getExpirationDate()).thenReturn(expirationDate);

        RetentionFlowInfo retentionFlowInfo = handler.buildRetentionFlowInfo(env);
        assertEquals(retentionFlowInfo.getFlow(), RetentionFlowGroup.BASIC_SHORT_WITH_TRIP);
        assertEquals(retentionFlowInfo.getTravel().getArrivalCity(), flight.getDestination());
        assertEquals(retentionFlowInfo.getTravel().getDate(), flight.getDepartureDate());
    }

    @Test
    public void testBuildRetentionFlowInfoForFreeTrialWithTrip() {
        String expirationDate = LocalDate.now().plusDays(276).toString();
        FutureFlight flight = setupFlight();
        flights.add(flight);

        when(membership.getExpirationDate()).thenReturn(expirationDate);
        when(membership.getType()).thenReturn(BASIC);
        when(membership.getSourceType()).thenReturn(FUNNEL_BOOKING);
        when(membership.getMonthsDuration()).thenReturn(1);

        RetentionFlowInfo retentionFlowInfo = handler.buildRetentionFlowInfo(env);
        assertEquals(retentionFlowInfo.getFlow(), RetentionFlowGroup.FREE_TRIAL_WITH_TRIP);
        assertEquals(retentionFlowInfo.getTravel().getArrivalCity(), flight.getDestination());
        assertEquals(retentionFlowInfo.getTravel().getDate(), flight.getDepartureDate());
    }

    @Test
    public void testBuildRetentionFlowInfoNotForFreeTrialWithTrip() {
        String expirationDate = LocalDate.now().plusDays(276).toString();
        FutureFlight flight = setupFlight();
        flights.add(flight);

        when(membership.getExpirationDate()).thenReturn(expirationDate);
        when(membership.getType()).thenReturn(BASIC);
        when(membership.getSourceType()).thenReturn(FUNNEL_BOOKING);
        when(membership.getMonthsDuration()).thenReturn(3);

        RetentionFlowInfo retentionFlowInfo = handler.buildRetentionFlowInfo(env);
        assertEquals(retentionFlowInfo.getFlow(), RetentionFlowGroup.BASIC_LONG_WITH_TRIP);
        assertEquals(retentionFlowInfo.getTravel().getArrivalCity(), flight.getDestination());
        assertEquals(retentionFlowInfo.getTravel().getDate(), flight.getDepartureDate());
    }
}
