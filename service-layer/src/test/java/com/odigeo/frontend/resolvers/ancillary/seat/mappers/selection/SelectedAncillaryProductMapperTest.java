package com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AncillaryProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectedAncillaryProduct;
import com.odigeo.frontend.commons.Logger;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SelectedAncillaryProductMapperTest {

    private static final String PRODUCT_ID = "123456";
    private static final String PRODUCT_TYPE_SEAT = "SEAT";
    private static final String PRODUCT_TYPE_UNKNOWN = "FOO";

    @Mock
    private Logger logger;

    @InjectMocks
    private SelectedAncillaryProductMapper selectedAncillaryProductMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void map() {
        String productId = PRODUCT_ID;
        String productType = PRODUCT_TYPE_SEAT;
        Map<String, String> inputProducts = new HashMap<>();
        inputProducts.put(productType, productId);

        List<SelectedAncillaryProduct> selectedAncillaryProducts = selectedAncillaryProductMapper.map(inputProducts);
        SelectedAncillaryProduct mappedProduct = selectedAncillaryProducts.get(0);

        assertEquals(selectedAncillaryProducts.size(), inputProducts.size());
        assertEquals(mappedProduct.getId(), productId);
        assertEquals(mappedProduct.getType(), AncillaryProductType.valueOf(productType));
    }

    @Test
    public void mapUnknownType() {
        String productId = PRODUCT_ID;
        String productType = PRODUCT_TYPE_UNKNOWN;
        Map<String, String> inputProducts = new HashMap<>();
        inputProducts.put(productType, productId);

        List<SelectedAncillaryProduct> selectedAncillaryProducts = selectedAncillaryProductMapper.map(inputProducts);

        assertTrue(selectedAncillaryProducts.isEmpty());
    }

    @Test
    public void mapNullInput() {
        List<SelectedAncillaryProduct> selectedAncillaryProducts = selectedAncillaryProductMapper.map(null);

        assertTrue(selectedAncillaryProducts.isEmpty());
    }
}

