package com.odigeo.frontend.resolvers.itinerary.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripType;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.odigeo.dapi.client.Carrier;
import com.odigeo.dapi.client.FreeCancellation;
import com.odigeo.dapi.client.ItinerariesLegend;
import com.odigeo.dapi.client.Itinerary;
import com.odigeo.dapi.client.Location;
import com.odigeo.dapi.client.Section;
import com.odigeo.dapi.client.SectionResult;
import com.odigeo.dapi.client.Segment;
import com.odigeo.dapi.client.SegmentResult;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.itinerary.configuration.FreeRebookingConfiguration;
import com.odigeo.frontend.resolvers.search.handlers.campaign.CampaignConfigHandler;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class ItineraryMapperTest {

    @Mock
    private Itinerary itinerary;
    @Mock
    private ItinerariesLegend legend;
    @Mock
    private SegmentResult segmentResult1;
    @Mock
    private SegmentResult segmentResult2;
    @Mock
    private Segment segment1;
    @Mock
    private Segment segment2;
    @Mock
    private SectionResult sectionResult1;
    @Mock
    private SectionResult sectionResult2;
    @Mock
    private Section section1;
    @Mock
    private Section section2;
    @Mock
    private Location loc1;
    @Mock
    private Location loc2;
    @Mock
    private Location loc3;
    @Mock
    private Carrier carrier;
    @Mock
    private FreeRebookingConfiguration freeRebookingConfiguration;
    @Mock
    private CampaignConfigHandler campaignConfigHandler;
    @Mock
    private VisitInformation visit;

    private ItineraryMapper mapper;
    private ShoppingCartContext context;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(visit.getSite()).thenReturn(Site.ES);
        Injector injector = Guice.createInjector(binder -> {
            binder.bind(FreeRebookingConfiguration.class).toProvider(() -> freeRebookingConfiguration);
            binder.bind(CampaignConfigHandler.class).toProvider(() -> campaignConfigHandler);
        });
        mapper = new ItineraryMapperImpl();
        context = new ShoppingCartContext(visit);
        injector.injectMembers(mapper);
    }

    @Test
    public void testItineraryContractToModel() {
        assertNull(mapper.itineraryContractToModel(null, context));
        assertNotNull(mapper.itineraryContractToModel(itinerary, context));
    }

    @Test
    public void testHasFreeBooking() {
        setItineraryOneWay();
        when(freeRebookingConfiguration.getSites()).thenReturn(Collections.singletonList("ES"));
        when(freeRebookingConfiguration.getCarriers()).thenReturn(Collections.singletonList("UX"));
        when(freeRebookingConfiguration.getMaxDateFormatted()).thenReturn(LocalDate.of(2024, 9, 30));
        assertTrue(mapper.itineraryContractToModel(itinerary, context).getHasFreeRebooking());
    }

    @Test
    public void testItineraryTypeOW() {
        setItineraryOneWay();
        assertEquals(mapper.itineraryContractToModel(itinerary, context).getTripType(), TripType.ONE_WAY);
    }

    @Test
    public void testItineraryWithFreeCancellation() {
        setItineraryOneWay();
        FreeCancellation freeCancellation = mock(FreeCancellation.class);
        Calendar calendar = Calendar.getInstance();
        when(freeCancellation.getLimitTime()).thenReturn(calendar);
        when(itinerary.getFreeCancellation()).thenReturn(freeCancellation);

        assertEquals(mapper.itineraryContractToModel(itinerary, context).getFreeCancellationLimit().getLimitTime(), calendar.getTimeInMillis());
    }

    @Test
    public void testItineraryTypeRT() {
        setItineraryRT();
        mapper.itineraryContractToModel(itinerary, context);
        assertEquals(mapper.itineraryContractToModel(itinerary, context).getTripType(), TripType.ROUND_TRIP);
    }

    @Test
    public void testItineraryTypeMD() {
        setItineraryMD();
        mapper.itineraryContractToModel(itinerary, context);
        assertEquals(mapper.itineraryContractToModel(itinerary, context).getTripType(), TripType.MULTIPLE_DESTINATIONS);
    }

    private void setItineraryOneWay() {
        List<Integer> segments = Collections.singletonList(0);
        when(itinerary.getSegments()).thenReturn(segments);
        when(itinerary.getLegend()).thenReturn(legend);
        List<SegmentResult> segmentResults = Collections.singletonList(segmentResult1);
        when(legend.getSegmentResults()).thenReturn(segmentResults);
        when(segmentResult1.getSegment()).thenReturn(segment1);
        when(segmentResult1.getId()).thenReturn(0);
        List<Integer> sections = Collections.singletonList(0);
        when(segment1.getSections()).thenReturn(sections);
        List<SectionResult> sectionResults = Collections.singletonList(sectionResult1);
        when(legend.getSectionResults()).thenReturn(sectionResults);
        when(sectionResult1.getId()).thenReturn(0);
        when(sectionResult1.getSection()).thenReturn(section1);
        when(section1.getDepartureDate()).thenReturn(Calendar.getInstance());
        when(section1.getFrom()).thenReturn(0);
        when(section1.getTo()).thenReturn(1);
        when(section1.getCarrier()).thenReturn(0);
        when(section1.getId()).thenReturn("1234");
        List<Carrier> carriers = Collections.singletonList(carrier);
        when(carrier.getId()).thenReturn(0);
        when(carrier.getCode()).thenReturn("UX");
        when(legend.getCarriers()).thenReturn(carriers);
        List<Location> locations = Arrays.asList(loc1, loc2);
        when(legend.getLocations()).thenReturn(locations);
        when(loc1.getGeoNodeId()).thenReturn(0);
        when(loc2.getGeoNodeId()).thenReturn(1);
        when(loc1.getCityIataCode()).thenReturn("BCN");
        when(loc2.getCityIataCode()).thenReturn("MAD");
    }

    private void setItineraryRT() {
        setCommonRTandMulti();
        when(section1.getFrom()).thenReturn(0);
        when(section1.getTo()).thenReturn(1);
        when(section2.getFrom()).thenReturn(1);
        when(section2.getTo()).thenReturn(0);
    }

    private void setItineraryMD() {
        setCommonRTandMulti();
        when(section1.getFrom()).thenReturn(0);
        when(section1.getTo()).thenReturn(1);
        when(section2.getFrom()).thenReturn(2);
        when(section2.getTo()).thenReturn(0);
    }

    private void setCommonRTandMulti() {
        List<Integer> segments = Arrays.asList(0, 1);
        when(itinerary.getSegments()).thenReturn(segments);
        when(itinerary.getLegend()).thenReturn(legend);
        List<SegmentResult> segmentResults = Arrays.asList(segmentResult1, segmentResult2);
        when(legend.getSegmentResults()).thenReturn(segmentResults);
        when(segmentResult1.getSegment()).thenReturn(segment1);
        when(segmentResult1.getId()).thenReturn(0);
        when(segmentResult2.getSegment()).thenReturn(segment2);
        when(segmentResult2.getId()).thenReturn(1);
        List<Integer> sections1 = Collections.singletonList(0);
        when(segment1.getSections()).thenReturn(sections1);
        List<Integer> sections2 = Collections.singletonList(1);
        when(segment2.getSections()).thenReturn(sections2);
        List<SectionResult> sectionResults = Arrays.asList(sectionResult1, sectionResult2);
        when(legend.getSectionResults()).thenReturn(sectionResults);
        when(sectionResult1.getId()).thenReturn(0);
        when(sectionResult1.getSection()).thenReturn(section1);
        when(sectionResult2.getId()).thenReturn(1);
        when(sectionResult2.getSection()).thenReturn(section2);
        when(section1.getCarrier()).thenReturn(0);
        when(section1.getId()).thenReturn("1234");
        when(section2.getCarrier()).thenReturn(0);
        when(section2.getId()).thenReturn("1235");
        List<Carrier> carriers = Collections.singletonList(carrier);
        when(carrier.getId()).thenReturn(0);
        when(carrier.getCode()).thenReturn("VY");
        when(legend.getCarriers()).thenReturn(carriers);
        List<Location> locations = Arrays.asList(loc1, loc2);
        when(legend.getLocations()).thenReturn(locations);
        when(loc1.getGeoNodeId()).thenReturn(0);
        when(loc2.getGeoNodeId()).thenReturn(1);
        when(loc3.getGeoNodeId()).thenReturn(2);
        when(loc1.getCityIataCode()).thenReturn("BCN");
        when(loc2.getCityIataCode()).thenReturn("MAD");
        when(loc3.getCityIataCode()).thenReturn("PMI");
    }
}

