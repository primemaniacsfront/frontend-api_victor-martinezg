package com.odigeo.frontend.resolvers.checkout.builder;

import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceName;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.execution.ResultPath;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertNotNull;

public class CheckoutErrorBuilderTest {

    private static final ErrorType ERROR_TYPE = mock(ErrorType.class);
    private static final ResultPath PATH = ResultPath.rootPath();
    private static final Map<String, Object> EXTENSIONS = Collections.EMPTY_MAP;
    private static final ErrorCodes ERROR_CODES = ErrorCodes.VALIDATION;
    private static final ServiceName SERVICE_NAME = ServiceName.FRONTEND_API;
    private static final String MESSAGE = "message";

    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void testBuild() {
        GraphQLError graphQLError = CheckoutErrorBuilder.newError()
                .errorType(ERROR_TYPE)
                .path(PATH)
                .extensions(EXTENSIONS)
                .message(MESSAGE).build();
        assertNotNull(graphQLError);
    }

    @Test
    public void testAlternativesOptionsBuild() {
        GraphQLError graphQLError = CheckoutErrorBuilder.newError()
                .errorType(ERROR_TYPE)
                .path(Collections.EMPTY_LIST)
                .extensions(ERROR_CODES, SERVICE_NAME)
                .message(MESSAGE).build();
        assertNotNull(graphQLError);
    }
}
