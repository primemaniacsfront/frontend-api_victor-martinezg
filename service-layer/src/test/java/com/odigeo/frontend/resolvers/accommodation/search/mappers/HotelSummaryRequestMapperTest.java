package com.odigeo.frontend.resolvers.accommodation.search.mappers;


import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;
import com.odigeo.hcsapi.v9.beans.SendPackage;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class HotelSummaryRequestMapperTest {

    private final HotelSummaryRequestMapper testClass = new HotelSummaryRequestMapper();

    @Test
    public void test() {
        List<AccommodationProviderKeyDTO> accommodationProviderKeys = Collections.singletonList(new AccommodationProviderKeyDTO("providerId", "accommodationProviderId"));
        SendPackage result = testClass.map(accommodationProviderKeys);
        assertNotNull(result.getHotelSupplierKeys());
        assertEquals(result.getHotelSupplierKeys().size(), 1);
        HotelSupplierKey hotelSupplierKey = result.getHotelSupplierKeys().get(0);
        assertEquals(hotelSupplierKey.getHotelSupplierId(), "accommodationProviderId");
        assertEquals(hotelSupplierKey.getSupplierCode(), "providerId");
    }


}
