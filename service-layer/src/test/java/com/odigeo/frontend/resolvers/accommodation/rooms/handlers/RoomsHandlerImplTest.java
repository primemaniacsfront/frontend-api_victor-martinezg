package com.odigeo.frontend.resolvers.accommodation.rooms.handlers;


import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationDataHandler;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;

public class RoomsHandlerImplTest {


    @Mock
    private AccommodationDataHandler accommodationDataHandler;
    @Mock
    private VisitInformation visit;

    @InjectMocks
    private RoomsHandlerImpl roomsHandler;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testRoomDetails() {
        String providerId = "123";
        String providerHotelId = "BC";
        String roomId = "1";
        roomsHandler.getStaticRoomDetails(providerId, providerHotelId, visit, roomId);
        verify(accommodationDataHandler).getStaticRoomDetails(providerId, providerHotelId, visit, roomId);
    }

    @Test
    public void testRoomAvailability() {
        long searchId = 123L;
        long accommodationDealKey = 456L;
        roomsHandler.getRoomAvailability(searchId, accommodationDealKey, visit);
        verify(accommodationDataHandler).getRoomAvailability(searchId, accommodationDealKey, visit);
    }

}
