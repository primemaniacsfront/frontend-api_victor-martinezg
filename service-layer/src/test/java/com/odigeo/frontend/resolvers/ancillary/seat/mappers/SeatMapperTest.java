package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Seat;
import com.odigeo.itineraryapi.v1.response.ProviderSeatMapPreferencesDescriptorItem;
import com.odigeo.itineraryapi.v1.response.SeatLocation;
import com.odigeo.itineraryapi.v1.response.SeatType;
import com.odigeo.itineraryapi.v1.response.Money;
import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SeatMapperTest {
    @Mock
    private Money money;
    @Mock
    private MoneyMapper moneyMapper;
    @Mock
    private SeatCharacteristicsMapper seatCharacteristicsMapper;

    @InjectMocks
    private SeatMapper seatMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(moneyMapper.map(any(com.odigeo.itineraryapi.v1.response.Money.class))).thenReturn(null);
        when(seatCharacteristicsMapper.map(anyList())).thenReturn(null);
    }

    @Test
    public void mapValidProviderSeatList() {
        when(money.getAmount()).thenReturn(new BigDecimal(100));
        List<ProviderSeatMapPreferencesDescriptorItem> providerSeatList = getInput();
        List<Seat> mappedSeatList = seatMapper.map(providerSeatList);
        List<Seat> expectedMappedSeatList = getOutput();
        Seat expectedMappedSeat = expectedMappedSeatList.get(0);
        Seat mappedSeat = mappedSeatList.get(0);

        assertEquals(providerSeatList.size(), mappedSeatList.size(), expectedMappedSeatList.size());
        assertEquals(mappedSeat.getSeatMapRow(), expectedMappedSeat.getSeatMapRow());
        assertEquals(mappedSeat.getRow(), expectedMappedSeat.getRow());
        assertEquals(mappedSeat.getColumn(), expectedMappedSeat.getColumn());
        assertEquals(mappedSeat.getFloor(), expectedMappedSeat.getFloor());
    }

    @Test
    public void mapEmptyProviderSeatList() {
        List<ProviderSeatMapPreferencesDescriptorItem> providerSeatList = new ArrayList<>();
        List<Seat> mappedSeatList = seatMapper.map(providerSeatList);

        assertEquals(providerSeatList.size(), mappedSeatList.size());
    }

    private List<ProviderSeatMapPreferencesDescriptorItem> getInput() {
        List<ProviderSeatMapPreferencesDescriptorItem> inputSeatList = new ArrayList<>();
        ProviderSeatMapPreferencesDescriptorItem mockSeat = new ProviderSeatMapPreferencesDescriptorItem();

        SeatLocation mockSeatLocation = new SeatLocation();

        mockSeatLocation.setSeatMapRow(0);
        mockSeatLocation.setSeatRow(8);
        mockSeatLocation.setColumn("A");
        mockSeatLocation.setFloor(0);

        mockSeat.setCharacteristics(new ArrayList<>());
        mockSeat.setSeatType(new SeatType());
        mockSeat.setSeatLocation(mockSeatLocation);
        mockSeat.setDebugInfo(StringUtils.EMPTY);
        mockSeat.setProviderPrice(money);
        mockSeat.setTotalPrice(money);

        inputSeatList.add(mockSeat);

        return inputSeatList;
    }

    private List<Seat> getOutput() {
        List<Seat> outputSeatList = new ArrayList<Seat>();
        Seat outputSeat = new Seat();

        outputSeat.setFloor(0);
        outputSeat.setRow(8);
        outputSeat.setSeatMapRow(0);
        outputSeat.setColumn("A");
        outputSeat.setType(null);
        outputSeat.setPrice(new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money());
        outputSeat.setFee(new com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money());
        outputSeat.setCharacteristics(new ArrayList<>());

        outputSeatList.add(outputSeat);
        return outputSeatList;
    }
}
