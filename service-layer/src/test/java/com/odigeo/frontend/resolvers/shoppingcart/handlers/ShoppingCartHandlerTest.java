package com.odigeo.frontend.resolvers.shoppingcart.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductIdRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketActionStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateProductsInShoppingBasketRequest;
import com.google.inject.Guice;
import com.odigeo.dapi.client.AvailableProductsResponse;
import com.odigeo.dapi.client.BookingResponse;
import com.odigeo.dapi.client.Buyer;
import com.odigeo.dapi.client.BuyerIdentificationType;
import com.odigeo.dapi.client.CabinClass;
import com.odigeo.dapi.client.Carrier;
import com.odigeo.dapi.client.FreeCancellation;
import com.odigeo.dapi.client.Itineraries;
import com.odigeo.dapi.client.ItinerariesLegend;
import com.odigeo.dapi.client.Itinerary;
import com.odigeo.dapi.client.ItineraryProvider;
import com.odigeo.dapi.client.ItineraryShoppingItem;
import com.odigeo.dapi.client.Location;
import com.odigeo.dapi.client.PersonalInfoRequest;
import com.odigeo.dapi.client.ResumeDataRequest;
import com.odigeo.dapi.client.Section;
import com.odigeo.dapi.client.SectionResult;
import com.odigeo.dapi.client.Segment;
import com.odigeo.dapi.client.SegmentResult;
import com.odigeo.dapi.client.SelectionRequests;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.dapi.client.TechnicalStop;
import com.odigeo.dapi.client.TestConfigurationSelectionRequest;
import com.odigeo.dapi.client.TravellerInformationDescription;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.checkout.mappers.CheckoutMapper;
import com.odigeo.frontend.resolvers.checkout.mappers.CheckoutMapperImpl;
import com.odigeo.frontend.resolvers.nonessentialproducts.handlers.NonEssentialProductsHandler;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartMapper;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartMapperImpl;
import com.odigeo.frontend.resolvers.shoppingcart.models.ConfirmRequest;
import com.odigeo.frontend.services.DapiService;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class ShoppingCartHandlerTest {

    private static final long BOOKING_ID = 12L;
    private static final Integer SEATS = 2;
    private static final long DURATION = 10L;
    private static final int CARRIER_ID = 0;
    private static final String CARRIER_CODE = "code";
    private static final String CARRIER_NAME = "name";
    private static final String CITY_IATA_CODE = "MAD";
    private static final String TERMINAL = "T";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneOffset.UTC);
    private static final String PROVIDER_CODE = "1A";
    private static final BigDecimal TOTAL_PRICE = BigDecimal.TEN;
    private static final BigDecimal BAGGAGE_FEE = BigDecimal.ONE;
    private static final BigDecimal COLLECTION_FEE = BigDecimal.TEN;
    private static final String ADDRESS = "address";
    private static final String CITY = "city";
    private static final String LAST_NAME = "lastName";
    private static final String COUNTRY = "country";
    private static final String NAME = "name";
    private static final String ZIP_CODE = "zip";
    private static final String EMAIL = "email";
    private static final String NEIGHBORHOOD = "neighborhood";
    private static final String POLICY = "POLICY";
    private static final String POLICY2 = "POLICY2";

    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private ItineraryShoppingItem shoppingItem;
    @Mock
    private Itinerary itinerary;
    @Mock
    private ItinerariesLegend legend;
    @Mock
    private Carrier carrier;
    @Mock
    private FreeCancellation freeCancellation;
    @Mock
    private Location location;
    @Mock
    private ItineraryProvider provider;
    @Mock
    private Itineraries itineraries;
    @Mock
    private TravellerInformationDescription travellerInfo;
    @Mock
    private Buyer buyer;
    @Mock
    private ConfirmRequest confirmRequest;
    @Mock
    private BookingResponse response;
    @Mock
    private TestConfigurationSelectionRequest testConfigurationSelectionRequest;
    @Mock
    private ResumeDataRequest resumeDataRequest;
    @Mock
    private ResolverContext context;
    @Mock
    private DapiService dapiService;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private NonEssentialProductsHandler nonEssentialProductsHandler;
    @Mock
    private AvailableProductsResponse availableProductsResponse;

    private ShoppingCartMapper mapper;
    private CheckoutMapper checkoutMapper;
    private ShoppingCartHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new ShoppingCartHandler();
        mapper = new ShoppingCartMapperImpl();
        checkoutMapper = new CheckoutMapperImpl();

        Guice.createInjector(binder -> {
            binder.bind(ShoppingCartMapper.class).toProvider(() -> mapper);
            binder.bind(CheckoutMapper.class).toProvider(() -> checkoutMapper);
            binder.bind(DapiService.class).toProvider(() -> dapiService);
            binder.bind(NonEssentialProductsHandler.class).toProvider(() -> nonEssentialProductsHandler);
        }).injectMembers(handler);
    }

    @Test
    public void testBuildShoppingCartResponse() {
        configShoppingCart();
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCart model = handler.buildShoppingCart(shoppingCart, visitInformation);
        checkShoppingCart(model);
    }

    @Test
    public void testCreateShoppingCart() {
        SelectionRequests selectionRequest = new SelectionRequests();
        when(dapiService.createShoppingCart(any(), any(), any())).thenReturn(new ShoppingCartSummaryResponse());
        ShoppingCartSummaryResponse shoppingCart = handler.createShoppingCart(selectionRequest, context, testConfigurationSelectionRequest);
        assertNotNull(shoppingCart);
        verify(dapiService).createShoppingCart(any(), any(), any());
    }

    @Test
    public void testAddPersonalInfoToShoppingCart() {
        PersonalInfoRequest personalInfoRequest = new PersonalInfoRequest();
        when(dapiService.addPersonalInfoToShoppingCart(any(), any())).thenReturn(new ShoppingCartSummaryResponse());
        ShoppingCartSummaryResponse shoppingCart = handler.addPersonalInfo(personalInfoRequest, context);
        assertNotNull(shoppingCart);
        verify(dapiService).addPersonalInfoToShoppingCart(any(), any());

    }

    @Test
    public void testConfirmBooking() {
        when(dapiService.confirmBooking(any(), anyLong(), any(), any(), any(), any())).thenReturn(response);
        BookingResponse responseReturned = handler.confirm(confirmRequest, context);
        assertEquals(response, responseReturned);
        verify(dapiService).confirmBooking(any(), anyLong(), any(), any(), any(), any());
    }

    @Test
    public void testResumeBooking() {
        when(dapiService.resumeBooking(anyLong(), any(), any())).thenReturn(response);
        BookingResponse responseReturned = handler.resume(BOOKING_ID, resumeDataRequest, context);
        assertEquals(response, responseReturned);
        verify(dapiService, times(1)).resumeBooking(BOOKING_ID, resumeDataRequest, context);
    }

    @Test
    public void getAvailableProductsFromDapi() {
        when(dapiService.getAvailableProducts(any(), any())).thenReturn(availableProductsResponse);
        AvailableProductsResponse response = handler.getAvailableProductsFromDapi(BOOKING_ID, context);
        assertEquals(availableProductsResponse, response);
        verify(dapiService, times(1)).getAvailableProducts(BOOKING_ID, context);
    }

    @Test
    public void removeProductsHandler() {
        UpdateProductsInShoppingBasketRequest request = new UpdateProductsInShoppingBasketRequest();
        request.setProductsToRemove(Collections.singletonList(new ProductIdRequest(POLICY, ProductType.INSURANCE)));
        request.setProductsToAdd(Collections.singletonList(new ProductIdRequest(POLICY2, ProductType.INSURANCE)));
        request.setShoppingId(String.valueOf(BOOKING_ID));

        ProductId productId = new ProductId(POLICY, ProductType.INSURANCE);
        List<UpdateProductsInShoppingBasketActionStatus> updateProductsInShoppingBasketActionStatuses =
                Collections.singletonList(new UpdateProductsInShoppingBasketActionStatus(productId, true));

        when(nonEssentialProductsHandler.getNonEssentialProductsIdsToRemove(request, availableProductsResponse)).thenReturn(Collections.singletonList(POLICY));
        when(dapiService.removeFromShoppingCart(any(), any())).thenReturn(new ShoppingCartSummaryResponse());
        when(nonEssentialProductsHandler.getNonEssentialProductsActionStatus(any(), any(), any()))
                .thenReturn(updateProductsInShoppingBasketActionStatuses);

        List<UpdateProductsInShoppingBasketActionStatus> response = handler.removeProducts(request, availableProductsResponse, context);
        assertNotNull(response);
        assertEquals(response.size(), 1);
        assertEquals(response.get(0).getProductId().getId(), POLICY);
        assertEquals(ProductType.INSURANCE, response.get(0).getProductId().getType());
        assertTrue(response.get(0).getSuccess());
    }

    @Test
    public void removeProductsHandlerNoProductsInRequest() {
        UpdateProductsInShoppingBasketRequest request = new UpdateProductsInShoppingBasketRequest();
        request.setProductsToAdd(Collections.singletonList(new ProductIdRequest(POLICY2, ProductType.INSURANCE)));
        request.setShoppingId(String.valueOf(BOOKING_ID));
        when(nonEssentialProductsHandler.getNonEssentialProductsIdsToRemove(request, availableProductsResponse)).thenReturn(Collections.emptyList());
        when(dapiService.removeFromShoppingCart(any(), any())).thenReturn(new ShoppingCartSummaryResponse());
        when(nonEssentialProductsHandler.getNonEssentialProductsActionStatus(any(), any(), any()))
                .thenReturn(Collections.emptyList());

        List<UpdateProductsInShoppingBasketActionStatus> response = handler.removeProducts(request, availableProductsResponse, context);
        assertNotNull(response);
        assertTrue(response.isEmpty());
    }

    @Test
    public void addProductsHandler() {
        UpdateProductsInShoppingBasketRequest request = new UpdateProductsInShoppingBasketRequest();
        request.setProductsToAdd(Collections.singletonList(new ProductIdRequest(POLICY, ProductType.INSURANCE)));
        request.setProductsToRemove(Collections.singletonList(new ProductIdRequest(POLICY2, ProductType.INSURANCE)));
        request.setShoppingId(String.valueOf(BOOKING_ID));
        ProductId productId = new ProductId(POLICY, ProductType.INSURANCE);
        List<UpdateProductsInShoppingBasketActionStatus> updateProductsInShoppingBasketActionStatuses =
                Collections.singletonList(new UpdateProductsInShoppingBasketActionStatus(productId, true));

        when(nonEssentialProductsHandler.getNonEssentialProductsIdsToAdd(request, availableProductsResponse)).thenReturn(Collections.singletonList(POLICY));
        when(dapiService.addToShoppingCart(any(), any())).thenReturn(new ShoppingCartSummaryResponse());
        when(nonEssentialProductsHandler.getNonEssentialProductsActionStatus(any(), any(), any()))
                .thenReturn(updateProductsInShoppingBasketActionStatuses);

        List<UpdateProductsInShoppingBasketActionStatus> response = handler.addProducts(request, availableProductsResponse, context);

        assertNotNull(response);
        assertEquals(response.size(), 1);
        assertEquals(response.get(0).getProductId().getId(), POLICY);
        assertEquals(ProductType.INSURANCE, response.get(0).getProductId().getType());
        assertTrue(response.get(0).getSuccess());
    }

    @Test
    public void addProductsHandlerNoProductsInRequest() {
        UpdateProductsInShoppingBasketRequest request = new UpdateProductsInShoppingBasketRequest();
        request.setProductsToRemove(Collections.singletonList(new ProductIdRequest(POLICY2, ProductType.INSURANCE)));
        request.setShoppingId(String.valueOf(BOOKING_ID));
        when(nonEssentialProductsHandler.getNonEssentialProductsIdsToAdd(request, availableProductsResponse)).thenReturn(Collections.emptyList());
        when(dapiService.addToShoppingCart(any(), any())).thenReturn(new ShoppingCartSummaryResponse());
        when(nonEssentialProductsHandler.getNonEssentialProductsActionStatus(any(), any(), any()))
                .thenReturn(Collections.emptyList());

        List<UpdateProductsInShoppingBasketActionStatus> response = handler.addProducts(request, availableProductsResponse, context);

        assertNotNull(response);
        assertTrue(response.isEmpty());
    }

    private void configLocation(int id, String cityIataCode) {
        when(location.getGeoNodeId()).thenReturn(id);
        when(location.getCityIataCode()).thenReturn(cityIataCode);
        when(location.getType()).thenReturn("Airport");
    }

    private void configCarrier(int id, String name, String code) {
        when(carrier.getId()).thenReturn(id);
        when(carrier.getCode()).thenReturn(code);
        when(carrier.getName()).thenReturn(name);
    }

    private List<SectionResult> configSection(List<Integer> sectionIds, int departure, int destination) {
        TechnicalStop stop = mock(TechnicalStop.class);
        when(stop.getLocation()).thenReturn(0);

        return sectionIds.stream().map(id -> {
            SectionResult result = mock(SectionResult.class);
            Section section = mock(Section.class);
            when(section.getFrom()).thenReturn(departure);
            when(section.getTo()).thenReturn(destination);
            when(section.getTechnicalStops()).thenReturn(Arrays.asList(stop));
            when(section.getId()).thenReturn(String.valueOf(id));
            when(section.getCarrier()).thenReturn(CARRIER_ID);
            when(section.getBaggageAllowanceQuantity()).thenReturn(0);
            when(section.getDuration()).thenReturn(1L);
            when(section.getCabinClass()).thenReturn(CabinClass.TOURIST);
            when(section.getArrivalTerminal()).thenReturn(TERMINAL);
            when(section.getDepartureTerminal()).thenReturn(TERMINAL);
            when(section.getDepartureDate()).thenReturn(Calendar.getInstance());
            when(section.getArrivalDate()).thenReturn(Calendar.getInstance());
            when(result.getSection()).thenReturn(section);
            when(result.getId()).thenReturn(id);
            return result;
        }).collect(Collectors.toList());
    }

    private List<SegmentResult> configSegment(List<Integer> segmentIds, List<Integer> sectionIds, int carrierId, long duration, Integer seats) {
        return segmentIds.stream().map(id -> {
            SegmentResult segmentResult = mock(SegmentResult.class);
            Segment segment = mock(Segment.class);
            when(segment.getCarrier()).thenReturn(carrierId);
            when(segment.getDuration()).thenReturn(duration);
            when(segment.getSeats()).thenReturn(seats);
            when(segment.getSections()).thenReturn(sectionIds);
            when(segmentResult.getId()).thenReturn(id);
            when(segmentResult.getSegment()).thenReturn(segment);
            return segmentResult;
        }).collect(Collectors.toList());
    }

    private void configItinerariesLegend(List<Carrier> carriers, List<SectionResult> sectionResults, List<Integer> sectionIds, List<SegmentResult> segmentResults, List<Location> locations) {
        when(legend.getCarriers()).thenReturn(carriers);
        when(legend.getSectionResults()).thenReturn(sectionResults);
        when(legend.getSegmentResults()).thenReturn(segmentResults);
        when(legend.getLocations()).thenReturn(locations);
        when(provider.getProvider()).thenReturn(PROVIDER_CODE);
        when(provider.getSections()).thenReturn(sectionIds);
        when(itineraries.getSimples()).thenReturn(Collections.singletonList(provider));
        when(legend.getItineraries()).thenReturn(itineraries);
    }

    private void configItinerary() {
        Calendar calendar = Calendar.getInstance();
        when(freeCancellation.getLimitTime()).thenReturn(calendar);
        when(itinerary.getFreeCancellation()).thenReturn(freeCancellation);
        when(itinerary.getLegend()).thenReturn(legend);
    }

    private void configShoppingCartItinerary() {
        configCarrier(CARRIER_ID, CARRIER_NAME, CARRIER_CODE);
        configLocation(0, CITY_IATA_CODE);
        List<Integer> segmentIds = Arrays.asList(0);
        List<Integer> sectionIds = Arrays.asList(0, 1);
        List<SectionResult> sectionResults = configSection(sectionIds, 0, 0);
        List<SegmentResult> segmentResults = configSegment(segmentIds, sectionIds, CARRIER_ID, DURATION, SEATS);
        List<Carrier> carriers = Arrays.asList(carrier);
        List<Location> locations = Arrays.asList(location);
        configItinerariesLegend(carriers, sectionResults, sectionIds, segmentResults, locations);
        configItinerary();
        when(shoppingItem.getItinerary()).thenReturn(itinerary);
    }

    private void configBuyer() {
        when(buyer.getAddress()).thenReturn(ADDRESS);
        when(buyer.getBuyerIdentificationType()).thenReturn(BuyerIdentificationType.PASSPORT);
        when(buyer.getCityName()).thenReturn(CITY);
        when(buyer.getCountry()).thenReturn(COUNTRY);
        when(buyer.getLastNames()).thenReturn(LAST_NAME);
        when(buyer.getName()).thenReturn(NAME);
        when(buyer.getDateOfBirth()).thenReturn(Calendar.getInstance());
        when(buyer.getZipCode()).thenReturn(ZIP_CODE);
        when(buyer.getMail()).thenReturn(EMAIL);
        when(buyer.getNeighborhood()).thenReturn(NEIGHBORHOOD);
        when(shoppingCart.getBuyer()).thenReturn(buyer);
    }

    private void configShoppingCart() {
        configShoppingCartItinerary();
        when(shoppingCart.getItineraryShoppingItem()).thenReturn(shoppingItem);
        configBuyer();
        when(shoppingCart.getRequiredTravellerInformation()).thenReturn(Arrays.asList(travellerInfo));
        when(shoppingCart.getBookingId()).thenReturn(BOOKING_ID);
        when(shoppingCart.getTotalPrice()).thenReturn(TOTAL_PRICE);
        when(shoppingCart.getBaggageTotalFee()).thenReturn(BAGGAGE_FEE);
        when(shoppingCart.getCollectionTotalFee()).thenReturn(COLLECTION_FEE);
    }

    private void checkShoppingCart(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingCart model) {
        assertEquals(model.getBookingId(), BOOKING_ID);
        assertEquals(model.getTotalPrice(), TOTAL_PRICE);
        assertEquals(model.getBaggageTotalFee(), BAGGAGE_FEE);
        assertEquals(model.getCollectionTotalFee(), COLLECTION_FEE);
    }
}
