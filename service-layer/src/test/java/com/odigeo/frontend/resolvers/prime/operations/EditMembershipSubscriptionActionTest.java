package com.odigeo.frontend.resolvers.prime.operations;

import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.prime.models.ModifyMembershipSubscription;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.MockitoAnnotations.openMocks;


public class EditMembershipSubscriptionActionTest {
    private ModifyMembershipSubscription modifyMembershipSubscription;
    @Mock
    private RemoveMembershipSubscriptionAction removeMembershipSubscriptionAction;
    @Mock
    private AddMembershipSubscriptionAction addMembershipSubscriptionAction;
    @Mock
    private ResolverContext context;
    @InjectMocks
    private EditMembershipSubscriptionAction editMembershipSubscriptionAction;


    @BeforeMethod
    public void setUp() {
        openMocks(this);
        modifyMembershipSubscription = ModifyMembershipSubscription.builder().build();
    }

    @Test
    public void testApply() {
        editMembershipSubscriptionAction.apply(modifyMembershipSubscription, context);
        InOrder inOrder = Mockito.inOrder(removeMembershipSubscriptionAction, addMembershipSubscriptionAction);
        inOrder.verify(removeMembershipSubscriptionAction).apply(eq(modifyMembershipSubscription), eq(context));
        inOrder.verify(addMembershipSubscriptionAction).apply(eq(modifyMembershipSubscription), eq(context));
        inOrder.verifyNoMoreInteractions();
    }
}
