package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.criteria.SearchCriteria;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.Itinerary;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.request.ItineraryFootprintCalculatorRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ItineraryFootprintCalculatorRequestMapperTest {

    @Mock
    private CarbonFootprintSearchCriteriaHandler searchCriteriaHandler;

    @Mock
    private FootprintItineraryMapper footprintItineraryMapper;

    @InjectMocks
    private ItineraryFootprintCalculatorRequestMapper mapper;

    @Mock
    private SearchRequest searchRequest;
    @Mock
    private SearchResponseDTO searchResponse;
    @Mock
    private ResolverContext context;

    private static final SearchCriteria SEARCH_CRITERIA = new SearchCriteria();

    private static final List<Itinerary> ITINERARY_LIST = Collections.emptyList();

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(footprintItineraryMapper.map(any())).thenReturn(ITINERARY_LIST);
        when(searchCriteriaHandler.buildSearchCriteria(any(), any(), any())).thenReturn(SEARCH_CRITERIA);
    }

    @Test
    public void testMap() {

        ItineraryFootprintCalculatorRequest itineraryRequest = mapper.map(searchRequest, searchResponse, context);
        assertNotNull(itineraryRequest);
        assertEquals(itineraryRequest.getSearchCriteria(), SEARCH_CRITERIA);
        assertEquals(itineraryRequest.getItineraries(), ITINERARY_LIST);

    }

}
