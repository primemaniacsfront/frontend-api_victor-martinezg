package com.odigeo.frontend.resolvers.itinerary.mappers.segment;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.ZonedDateTime;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class StopMapperTest {

    @Mock
    private Section section;
    @Mock
    private TechnicalStop techStop;

    private Segment segment;
    private StopMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new StopMapperImpl();
        segment = new Segment();
    }

    @Test
    public void testCalculateStops() {
        when(section.getTechnicalStops()).thenReturn(Arrays.asList(techStop));
        when(section.getDepartureDate()).thenReturn(ZonedDateTime.now());
        when(section.getArrivalDate()).thenReturn(ZonedDateTime.now());
        segment.setSections(Arrays.asList(section));
        mapper.calculateStops(segment);
        assertEquals(segment.getStops(), Integer.valueOf(0));
        assertEquals(segment.getTechnicalStops(), Integer.valueOf(1));
        assertEquals(segment.getAllStops(), Integer.valueOf(1));

        segment.setSections(Arrays.asList(section, section));
        mapper.calculateStops(segment);
        assertEquals(segment.getStops(), Integer.valueOf(1));
        assertEquals(segment.getTechnicalStops(), Integer.valueOf(2));
        assertEquals(segment.getAllStops(), Integer.valueOf(3));
    }

}

