package com.odigeo.frontend.resolvers.prime.handlers;

import com.odigeo.membership.request.search.MembershipSearchRequest;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.frontend.resolvers.prime.handlers.MembershipSearchRequestHandler.NEWEST_FIRST;
import static com.odigeo.frontend.resolvers.prime.handlers.MembershipSearchRequestHandler.PAGINATION_ONE;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MembershipSearchRequestHandlerTest {

    private static final String WEBSITE_CODE = "ES";
    private static final String EMAIL = "buyer@edreams.com";
    private static final Long USER_ID = 1234L;
    @InjectMocks
    private MembershipSearchRequestHandler handler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testBuilderWithEmail() {
        MembershipSearchRequest request = handler.build(EMAIL, WEBSITE_CODE);
        assertEquals(request.getEmail(), EMAIL);
        assertCommonParameters(request);
    }

    private void assertCommonParameters(MembershipSearchRequest request) {
        assertEquals(request.getWebsite(), WEBSITE_CODE);
        assertEquals(request.getSorting(), NEWEST_FIRST);
        assertEquals(request.getPagination(), PAGINATION_ONE);
        assertTrue(request.isWithMemberAccount());
    }

    @Test
    public void testBuilderWithUserId() {
        MembershipSearchRequest request = handler.build(USER_ID, WEBSITE_CODE);
        assertEquals(request.getMemberAccountSearchRequest().getUserId(), USER_ID);
        assertCommonParameters(request);
    }
}
