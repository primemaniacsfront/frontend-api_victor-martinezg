package com.odigeo.frontend.resolvers.shoppingcart.traveller.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TravellerAgeType;
import com.odigeo.dapi.client.Traveller;
import com.odigeo.dapi.client.TravellerType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class PersonalInfoMapperTest {

    @Mock
    private Traveller traveller;

    private PersonalInfoMapper mapper;
    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PersonalInfo personalInfoModel;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new PersonalInfoMapperImpl();
    }

    @Test
    public void testPersonalInfoContractToModel() {
        assertNull(mapper.personalInfoContractToModel(null));

        Map<TravellerType, TravellerAgeType> types = Stream.of(
                new AbstractMap.SimpleImmutableEntry<>(TravellerType.ADULT, TravellerAgeType.ADULT),
                new AbstractMap.SimpleImmutableEntry<>(TravellerType.INFANT, TravellerAgeType.INFANT),
                new AbstractMap.SimpleImmutableEntry<>(TravellerType.CHILD, TravellerAgeType.CHILD))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        types.forEach((k, v) -> {
            when(traveller.getTravellerType()).thenReturn(k);
            personalInfoModel = mapper.personalInfoContractToModel(traveller);
            assertEquals(personalInfoModel.getAgeType(), v);
        });
    }
}

