package com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Guest;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.dapi.client.RoomGroupDeal;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationDetailRetrieveHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationSearchCriteriaRetrieveHandler;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.accommodation.RoomCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.provider.AccommodationProviderSearchCriteria;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class AccommodationGuestsHandlerTest {

    private static final long DEAL_ID = 876L;

    @Mock
    private AccommodationDetailRetrieveHandler accommodationDetailRetrieveHandler;
    @Mock
    private AccommodationSearchCriteriaRetrieveHandler accommodationSearchCriteriaRetrieveHandler;

    @InjectMocks
    private AccommodationGuestsHandler accommodationGuestsHandler;

    private List<Guest> mainGuests;
    @Mock
    private Guest guest;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    ShoppingCart shoppingCart;
    @Mock
    AccommodationShoppingItem accommodationShoppingItem;
    @Mock
    RoomGroupDeal groupDeal;
    @Mock
    SearchResults searchResults;
    @Mock
    AccommodationProviderSearchCriteria accommodationProviderSearchCriteria;
    @Mock
    RoomCriteria roomCriteria;
    @Mock
    AccommodationSearchCriteria accommodationSearchCriteria;


    @BeforeMethod
    public void setUp() {
        openMocks(this);
        mainGuests = Collections.singletonList(guest);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getAccommodationShoppingItem()).thenReturn(accommodationShoppingItem);
        when(accommodationShoppingItem.getRoomGroupDeal()).thenReturn(groupDeal);
        when(guest.getName()).thenReturn("getName");
        when(guest.getFirstLastName()).thenReturn("getFirstLastName");
        when(guest.getMiddleName()).thenReturn("getMiddleName");
        when(guest.getTitleName()).thenReturn("getTitleName");
        when(guest.getYearOfBirth()).thenReturn(30);
    }

    @Test
    public void buildGuestsNullShoppingCart() {
        try {
            when(accommodationDetailRetrieveHandler.getShoppingCart(anyLong(), eq(resolverContext))).thenReturn(null);
            accommodationGuestsHandler.buildGuests(mainGuests, DEAL_ID, resolverContext, "visitCode");
        } catch (ServiceException e) {
            assertTrue(e.getMessage().contains("Error calculating number of guest"));
        }
    }

    @Test
    public void buildGuestsNullSearchResults() {
        when(groupDeal.getSearchId()).thenReturn(1234L);
        when(accommodationDetailRetrieveHandler.getShoppingCart(anyLong(), eq(resolverContext))).thenReturn(shoppingCartSummaryResponse);
        when(accommodationSearchCriteriaRetrieveHandler.getSearchResultsBySearchId(anyLong(), anyString())).thenReturn(null);
        try {
            accommodationGuestsHandler.buildGuests(mainGuests, DEAL_ID, resolverContext, "visitCode");
        } catch (
            ServiceException e) {
            assertTrue(e.getMessage().contains("Error calculating number of guest"));
        }
    }

    @Test
    public void buildGuestsTest() {
        prepareMockToCalculateNumberOfGuest();
        when(accommodationProviderSearchCriteria.getRoomCriterias()).thenReturn(Collections.singletonList(roomCriteria));
        when(guest.getName()).thenReturn("name");
        when(guest.getFirstLastName()).thenReturn("getFirstLastName");
        when(roomCriteria.getNumberOfAdults()).thenReturn(2);

        List<Guest> results = accommodationGuestsHandler.buildGuests(mainGuests, DEAL_ID, resolverContext, "visitCode");
        assertTrue(results.size() == 2);
        assertGuest(results.get(0), guest);
        assertGuest(results.get(1), guest);
    }

    @Test
    public void buildGuestsWrongNumberMainGuests() {
        prepareMockToCalculateNumberOfGuest();
        when(accommodationProviderSearchCriteria.getRoomCriterias()).thenReturn(Arrays.asList(roomCriteria, roomCriteria));
        when(roomCriteria.getNumberOfAdults()).thenReturn(2);
        try {
            accommodationGuestsHandler.buildGuests(mainGuests, DEAL_ID, resolverContext, "visitCode");
        } catch (ServiceException e) {
            assertTrue(e.getMessage().contains("Number of guest is not correct"));
        }
    }

    @Test
    public void buildGuestsMultipleRooms() {
        prepareMockToCalculateNumberOfGuest();
        when(accommodationProviderSearchCriteria.getRoomCriterias()).thenReturn(Arrays.asList(roomCriteria, roomCriteria));
        when(roomCriteria.getNumberOfAdults()).thenReturn(2);
        Guest secondRoom = mock(Guest.class);
        mainGuests = Arrays.asList(guest, secondRoom);
        when(secondRoom.getName()).thenReturn("secondgetName");
        when(secondRoom.getFirstLastName()).thenReturn("secondgetFirstLastName");
        when(secondRoom.getMiddleName()).thenReturn("secondgetMiddleName");
        when(secondRoom.getTitleName()).thenReturn("secondgetTitleName");
        when(secondRoom.getYearOfBirth()).thenReturn(30);
        List<Guest> results = accommodationGuestsHandler.buildGuests(mainGuests, DEAL_ID, resolverContext, "visitCode");
        assertGuest(results.get(0), guest);
        assertGuest(results.get(1), guest);
        assertGuest(results.get(2), secondRoom);
        assertGuest(results.get(3), secondRoom);

    }

    private void prepareMockToCalculateNumberOfGuest() {
        when(groupDeal.getSearchId()).thenReturn(1234L);
        when(accommodationDetailRetrieveHandler.getShoppingCart(anyLong(), eq(resolverContext))).thenReturn(shoppingCartSummaryResponse);
        when(accommodationSearchCriteriaRetrieveHandler.getSearchResultsBySearchId(anyLong(), anyString())).thenReturn(searchResults);
        when(searchResults.getSearchCriteria()).thenReturn(accommodationSearchCriteria);
        when(accommodationSearchCriteria.getProviderSearchCriteria()).thenReturn(accommodationProviderSearchCriteria);
    }

    private void assertGuest(Guest result, Guest expected) {
        assertEquals(result.getName(), expected.getName());
        assertEquals(result.getFirstLastName(), expected.getFirstLastName());
        assertEquals(result.getSecondLastName(), expected.getSecondLastName());
        assertEquals(result.getTitleName(), expected.getTitleName());
        assertEquals(result.getYearOfBirth(), expected.getYearOfBirth());
    }
}
