package com.odigeo.frontend.resolvers.shoppingcart.buyer.handlers;

import com.odigeo.dapi.client.BuyerInformationDescription;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.resolvers.shoppingcart.buyer.mappers.BuyerMapper;
import graphql.GraphQLContext;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertNull;

public class BuyerHandlerTest {

    @Mock
    private BuyerMapper buyerMapper;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private com.odigeo.dapi.client.Buyer buyer;
    @Mock
    private BuyerInformationDescription buyerInformationDescription;

    @InjectMocks
    private BuyerHandler handler;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testMapNull() {
        assertNull(handler.map(graphQLContext));
        verify(shoppingCart, never()).getBuyer();
        verify(buyerMapper).map(isNull());
    }

    @Test
    public void testMap() {
        when(graphQLContext.get(eq(ShoppingCartSummaryResponse.class))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getBuyer()).thenReturn(buyer);
        handler.map(graphQLContext);
        verify(buyerMapper).map(eq(buyer));
    }

    @Test
    public void testMapBuyerInfoNull() {
        assertNull(handler.mapBuyerInfo(graphQLContext));
        verify(shoppingCart, never()).getRequiredBuyerInformation();
        verify(buyerMapper).mapBuyerInformationDescription(isNull());
    }

    @Test
    public void testMapBuyerInfo() {
        when(graphQLContext.get(eq(ShoppingCartSummaryResponse.class))).thenReturn(shoppingCartSummaryResponse);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getRequiredBuyerInformation()).thenReturn(buyerInformationDescription);
        handler.mapBuyerInfo(graphQLContext);
        verify(buyerMapper).mapBuyerInformationDescription(buyerInformationDescription);
    }
}
