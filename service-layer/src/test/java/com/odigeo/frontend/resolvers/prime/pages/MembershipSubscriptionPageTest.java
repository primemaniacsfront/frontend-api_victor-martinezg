package com.odigeo.frontend.resolvers.prime.pages;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPageName;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPagesRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.commons.util.ShoppingInfoUtils;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.frontend.resolvers.prime.pages.MembershipSubscriptionPage.MULTI_OFFER_PARTITION;
import static com.odigeo.frontend.resolvers.prime.pages.MembershipSubscriptionPage.SINGLE_OFFER_PARTITION;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MembershipSubscriptionPageTest {
    private static final long SHOPPING_ID = 12345L;
    private static final int A_PARTITION = 1;

    @Mock
    private VisitInformation visitInformation;
    @Mock
    private SiteVariations siteVariations;
    @Mock
    private MembershipHandler membershipHandler;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private MembershipPagesRequest membershipPagesRequest;
    @Mock
    private ShoppingInfo shoppingInfo;
    @Mock
    private ShoppingInfoUtils shoppingInfoUtils;
    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private JsonUtils jsonUtils;

    @InjectMocks
    private MembershipSubscriptionPage membershipSubscriptionPage;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Guice.createInjector(binder -> {
            binder.bind(SiteVariations.class).toProvider(() -> siteVariations);
            binder.bind(MembershipHandler.class).toProvider(() -> membershipHandler);
            binder.bind(ShoppingInfoUtils.class).toProvider(() -> shoppingInfoUtils);
        });
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
        when(jsonUtils.fromDataFetching(eq(env), eq(MembershipPagesRequest.class))).thenReturn(membershipPagesRequest);
        when(graphQLContext.get(VisitInformation.class)).thenReturn(visitInformation);
        when(membershipPagesRequest.getShoppingInfo()).thenReturn(shoppingInfo);
    }

    @Test
    public void testGetSubscriptionTypeFunnelPageWhenTiers() {
        when(siteVariations.getMembershipSubscriptionTiers(eq(visitInformation))).thenReturn(MULTI_OFFER_PARTITION);
        MembershipPageName page = membershipSubscriptionPage.getSubscriptionTypeFunnelPage(visitInformation);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_PAGE_SUBSCRIPTION_TIERS);
        assertEquals(page, MembershipPageName.SUBSCRIPTION_MULTIPLE_OFFERS);
    }

    @Test
    public void testGetSubscriptionTypeFunnelPageWhenSingle() {
        when(siteVariations.getMembershipSubscriptionTiers(eq(visitInformation))).thenReturn(SINGLE_OFFER_PARTITION);
        MembershipPageName page = membershipSubscriptionPage.getSubscriptionTypeFunnelPage(visitInformation);
        assertEquals(page, MembershipPageName.SUBSCRIPTION_SINGLE_OFFER);
    }

    @Test
    public void testGetSubscriptionTypeFunnelPageWhenABOff() {
        when(siteVariations.getMembershipSubscriptionTiers(eq(visitInformation))).thenReturn(A_PARTITION);
        MembershipPageName page = membershipSubscriptionPage.getSubscriptionTypeFunnelPage(visitInformation);
        assertNull(page);
    }

    @Test
    public void testGetSubscriptionFunnelPageWhenFTL() {
        when(siteVariations.getMembershipSubscriptionTiers(eq(visitInformation))).thenReturn(SINGLE_OFFER_PARTITION);
        when(shoppingInfo.getShoppingId()).thenReturn(String.valueOf(SHOPPING_ID));
        when(shoppingInfo.getShoppingType()).thenReturn(ShoppingType.CART);
        when(membershipHandler.isEligibleForFreeTrial(eq(shoppingInfo), any())).thenReturn(Boolean.FALSE);
        MembershipPageName page = membershipSubscriptionPage.getPage(env);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_PAGE_SUBSCRIPTION_FTL);
        assertEquals(page, MembershipPageName.SUBSCRIPTION_FTL);
    }

    @Test
    public void testGetSubscriptionFunnelPageWhenFTLFails() {
        when(siteVariations.getMembershipSubscriptionTiers(eq(visitInformation))).thenReturn(SINGLE_OFFER_PARTITION);
        when(shoppingInfo.getShoppingId()).thenReturn(String.valueOf(SHOPPING_ID));
        when(shoppingInfo.getShoppingType()).thenReturn(ShoppingType.CART);
        when(membershipHandler.isEligibleForFreeTrial(eq(shoppingInfo), any())).thenThrow(new ServiceException("ERROR", ServiceName.FRONTEND_API));
        MembershipPageName page = membershipSubscriptionPage.getPage(env);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_FREE_TRIAL_ABUSER_FAILED);
        assertEquals(page, MembershipPageName.SUBSCRIPTION_SINGLE_OFFER);
    }

    @Test
    public void testGetSubscriptionFunnelPageWhenNotFTLSingle() {
        when(siteVariations.getMembershipSubscriptionTiers(eq(visitInformation))).thenReturn(SINGLE_OFFER_PARTITION);
        when(shoppingInfo.getShoppingId()).thenReturn(String.valueOf(SHOPPING_ID));
        when(shoppingInfo.getShoppingType()).thenReturn(ShoppingType.CART);
        when(membershipHandler.isEligibleForFreeTrial(eq(shoppingInfo), any())).thenReturn(Boolean.TRUE);
        MembershipPageName page = membershipSubscriptionPage.getPage(env);
        verify(metricsHandler).trackCounter(MeasureConstants.MEMBERSHIP_PAGE_SUBSCRIPTION);
        assertEquals(page, MembershipPageName.SUBSCRIPTION_SINGLE_OFFER);
    }

    @Test
    public void testGetSubscriptionFunnelPageWhenNotFTLMulti() {
        when(siteVariations.getMembershipSubscriptionTiers(eq(visitInformation))).thenReturn(MULTI_OFFER_PARTITION);
        when(shoppingInfo.getShoppingId()).thenReturn(String.valueOf(SHOPPING_ID));
        when(shoppingInfo.getShoppingType()).thenReturn(ShoppingType.CART);
        when(membershipHandler.isEligibleForFreeTrial(eq(shoppingInfo), any())).thenReturn(Boolean.TRUE);
        MembershipPageName page = membershipSubscriptionPage.getPage(env);
        assertEquals(page, MembershipPageName.SUBSCRIPTION_MULTIPLE_OFFERS);
    }
}
