package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.Segment;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.product.segment.SegmentResult;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class FootprintSegmentResultMapperTest {

    @Mock
    private FootprintSegmentMapper segmentMapper;

    @InjectMocks
    private FootprintSegmentResultMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(segmentMapper.map(any())).thenReturn(new Segment());
    }

    @Test
    public void testMap() {
        SegmentResult segmentResult = mapper.map(getLegDTOMock());
        assertNotNull(segmentResult);
        assertNotNull(segmentResult.getSegments());
        assertEquals(segmentResult.getSegments().size(), 1);
    }

    private LegDTO getLegDTOMock() {
        LegDTO legDTO = new LegDTO();
        legDTO.setSegmentKeys(Collections.singletonList(1));
        legDTO.setSegments(Collections.singletonList(new SegmentDTO()));
        return legDTO;
    }


}
