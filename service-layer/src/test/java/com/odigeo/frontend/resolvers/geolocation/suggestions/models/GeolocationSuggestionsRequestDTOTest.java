package com.odigeo.frontend.resolvers.geolocation.suggestions.models;

import bean.test.BeanTest;
import com.odigeo.geoapi.v4.request.ProductType;

public class GeolocationSuggestionsRequestDTOTest extends BeanTest<GeolocationSuggestionsRequestDTO> {

    @Override
    protected GeolocationSuggestionsRequestDTO getBean() {
        GeolocationSuggestionsRequestDTO bean = new GeolocationSuggestionsRequestDTO();
        bean.setSearchWord("bar");
        bean.setLocale("es_ES");
        bean.setProductType(ProductType.FLIGHT);
        bean.setWebsite("ES");
        bean.setInputType("DEPARTURE");
        bean.setIncludeMultiLanguage(Boolean.TRUE);
        bean.setIncludeRelatedLocations(Boolean.TRUE);
        bean.setIncludeSearchInAllWords(Boolean.TRUE);
        bean.setIncludeCountry(Boolean.TRUE);
        bean.setIncludeRegion(Boolean.TRUE);
        bean.setIncludeNearestLocations(Boolean.TRUE);
        bean.setIncludeCitiesOnly(Boolean.TRUE);
        return bean;
    }
}
