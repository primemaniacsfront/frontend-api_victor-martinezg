package com.odigeo.frontend.resolvers.shoppingcart.models;

import bean.test.BeanTest;
import com.odigeo.dapi.client.FraudInfoRequest;
import com.odigeo.dapi.client.InvoiceRequest;
import com.odigeo.dapi.client.PaymentDataRequest;
import org.apache.commons.lang3.StringUtils;

public class ConfirmRequestTest extends BeanTest<ConfirmRequest> {

    @Override
    protected ConfirmRequest getBean() {
        ConfirmRequest bean = new ConfirmRequest();
        bean.setBookingId(1L);
        bean.setClientBookingReferencesId(StringUtils.EMPTY);
        bean.setPaymentData(new PaymentDataRequest());
        bean.setInvoice(new InvoiceRequest());
        bean.setFraudInfo(new FraudInfoRequest());
        return bean;
    }

}
