package com.odigeo.frontend.resolvers.search.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.odigeo.searchengine.v2.responses.gis.Location;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class LocationMapperTest {

    private static final String AIRPORT = "Airport";
    private static final String TRAIN_STATION = "Train Station";

    @Mock
    private Location location;

    private LocationMapper mapper;

    @BeforeClass
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new LocationMapper();
    }

    @Test
    public void testMap() {
        Stream.of(
            new ImmutablePair<>(AIRPORT, LocationType.AIRPORT),
            new ImmutablePair<>(TRAIN_STATION, LocationType.TRAIN_STATION),
            new ImmutablePair<>("unknown location", LocationType.OTHER),
            new ImmutablePair<String, LocationType>(null, LocationType.OTHER)
        ).forEach(pair -> {
            when(location.getType()).thenReturn(pair.left);
            assertEquals(mapper.map(location), pair.right);
        });
    }

}
