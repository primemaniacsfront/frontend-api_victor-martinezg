package com.odigeo.frontend.resolvers.search.handlers.mir;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.handlers.SearchDebugHandler;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.frontend.resolvers.search.models.TrackingInfoDTO;
import com.odigeo.frontend.services.MirService;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.Itinerary;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.ItineraryRatingResult;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.SearchCriteria;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.response.ItineraryRatingResponse;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MirHandlerTest {

    private static final String USER_DEVICE_ID = "USER_DEVICE_ID";
    private static final BigDecimal ONE = BigDecimal.ONE;

    @Mock
    private MirSearchCriteriaHandler mirSearchCriteriaHandler;
    @Mock
    private MirItineraryHandler mirItineraryHandler;
    @Mock
    private MirService mirService;
    @Mock
    private SearchRequest searchRequest;
    @Mock
    private SearchResponseDTO searchResponse;
    @Mock
    private VisitInformation visit;
    @Mock
    private ItineraryRatingResponse ratingResponse;
    @Mock
    private SearchDebugHandler debugHandler;
    @Mock
    private ResolverContext context;
    @Mock
    private SearchItineraryDTO itinerary;
    @Spy
    private TrackingInfoDTO trackingInfo;

    private MirHandler handler;

    private List<SearchItineraryDTO> itineraries;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new MirHandler();

        Guice.createInjector(binder -> {
            binder.bind(MirSearchCriteriaHandler.class).toProvider(() -> mirSearchCriteriaHandler);
            binder.bind(MirItineraryHandler.class).toProvider(() -> mirItineraryHandler);
            binder.bind(MirService.class).toProvider(() -> mirService);
            binder.bind(SearchDebugHandler.class).toProvider(() -> debugHandler);
        }).injectMembers(handler);

        when(context.getVisitInformation()).thenReturn(visit);
    }

    @Test
    public void testAddRating() {
        initMocks();

        handler.addRatingsAndTrackingInfo(searchRequest, searchResponse, context);

        verify(itinerary).setMeRating(ONE);
    }

    @Test
    public void testAddReturningUserDeviceIdWhenReturned() {
        initMocks();
        givenUserDeviceIdIsReturned();

        handler.addRatingsAndTrackingInfo(searchRequest, searchResponse, context);

        assertEquals(trackingInfo.getReturningUserDeviceId(), USER_DEVICE_ID);
    }

    @Test
    public void testAddReturningUserDeviceIdWhenNotReturned() {
        initMocks();

        handler.addRatingsAndTrackingInfo(searchRequest, searchResponse, context);

        assertEquals(trackingInfo.getReturningUserDeviceId(), StringUtils.EMPTY);

    }

    @Test
    public void testAddDebugInfo() {
        initMocks();

        handler.addRatingsAndTrackingInfo(searchRequest, searchResponse, context);

        verify(debugHandler).addQaModeMeRatingInfo(context, itineraries);
    }

    private void initMocks() {
        String visitCode = "visit";
        SearchCriteria searchCriteria = mock(SearchCriteria.class);
        ItineraryRatingResult ratingResult = mock(ItineraryRatingResult.class);
        List<Itinerary> ratingItineraries = Collections.emptyList();
        itineraries = Collections.singletonList(itinerary);

        when(mirSearchCriteriaHandler.buildRatingSearchCriteria(searchRequest, searchResponse, visit))
                .thenReturn(searchCriteria);
        when(mirItineraryHandler.buildRatingItineraries(searchResponse)).thenReturn(ratingItineraries);
        when(visit.getVisitCode()).thenReturn(visitCode);

        when(mirService.getRatings(argThat(ratingRequest ->
                ratingRequest.getVisitInformation().equals(visitCode)
                        && ratingRequest.getSearchCriteria() == searchCriteria
                        && ratingRequest.getItineraries() == ratingItineraries
        ))).thenReturn(ratingResponse);

        when(searchResponse.getTrackingInfo()).thenReturn(trackingInfo);
        when(searchResponse.getItineraries()).thenReturn(itineraries);
        when(ratingResponse.getItineraryRatingResults()).thenReturn(Collections.singletonList(ratingResult));
        when(ratingResult.getRating()).thenReturn(ONE);
    }

    private void givenUserDeviceIdIsReturned() {
        when(ratingResponse.getUserDeviceId()).thenReturn(USER_DEVICE_ID);
    }
}
