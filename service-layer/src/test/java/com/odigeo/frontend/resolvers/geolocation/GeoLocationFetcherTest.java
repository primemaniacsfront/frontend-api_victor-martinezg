package com.odigeo.frontend.resolvers.geolocation;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeoLocation;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.geoapi.v4.GeoNodeNotFoundException;
import com.odigeo.geoapi.v4.GeoServiceException;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class GeoLocationFetcherTest {


    private static final String ES_ES = "es_ES";

    @Mock
    private DataFetchingEnvironment environment;
    @Mock
    private GraphQLContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private GeoLocationHandler geoLocationHandler;
    @Mock
    private JsonUtils jsonUtils;

    @InjectMocks
    private GeoLocationFetcher testClass;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(environment.getGraphQlContext()).thenReturn(context);
        when(context.get(VisitInformation.class)).thenReturn(visit);
        when(visit.getLocale()).thenReturn(ES_ES);
    }

    @Test
    public void testFromQueryResolver() throws GeoNodeNotFoundException, GeoServiceException {
        setupQueryResolver();
        when(geoLocationHandler.retrieveGeoLocation(anyInt(), anyString())).thenReturn(new GeoLocation());
        assertNotNull(testClass.getGeoLocation(environment));
    }

    @Test
    public void testFromTypeResolver() throws GeoNodeNotFoundException, GeoServiceException {
        setupTypeResolver();
        when(geoLocationHandler.retrieveGeoLocation(anyInt(), anyString())).thenReturn(new GeoLocation());
        assertNotNull(testClass.getGeoLocation(environment));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testNotFound() throws GeoNodeNotFoundException, GeoServiceException {
        setupQueryResolver();
        when(geoLocationHandler.retrieveGeoLocation(anyInt(), anyString())).thenThrow(new GeoNodeNotFoundException("Not found"));
        testClass.getGeoLocation(environment);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGeoServiceException() throws GeoNodeNotFoundException, GeoServiceException {
        setupQueryResolver();
        when(geoLocationHandler.retrieveGeoLocation(anyInt(), anyString())).thenThrow(new GeoServiceException("Not found"));
        testClass.getGeoLocation(environment);
    }

    private void setupTypeResolver() {
        Map<String, Object> localContext = new HashMap<>();
        localContext.put(GeoLocationFetcher.LOCALE, ES_ES);
        localContext.put(GeoLocationFetcher.GEO_NODE_ID, 1);
        when(environment.getLocalContext()).thenReturn(localContext);
        when(environment.getArguments()).thenReturn(null);
    }

    private void setupQueryResolver() {
        when(environment.getArguments()).thenReturn(new HashMap<>());
        when(jsonUtils.fromDataFetching(environment)).thenReturn(1);
    }

}
