package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceUrlType;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class InsuranceUrlTypeMapperTest {

    private static final String NOT_FOUND_TYPE = "NOT_FOUND_TYPE";
    private static final InsuranceUrlTypeMapper INSURANCE_URL_TYPE_MAPPER = new InsuranceUrlTypeMapper();

    @Test
    public void found() {
        InsuranceUrlType insuranceUrlType = InsuranceUrlType.EXTENDED;

        assertEquals(INSURANCE_URL_TYPE_MAPPER.map(insuranceUrlType.name()), insuranceUrlType);
    }

    @Test
    public void notFound() {
        assertEquals(INSURANCE_URL_TYPE_MAPPER.map(NOT_FOUND_TYPE), InsuranceUrlType.SIMPLE);
    }

}
