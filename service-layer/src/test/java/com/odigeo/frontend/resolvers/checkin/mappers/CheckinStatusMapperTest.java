package com.odigeo.frontend.resolvers.checkin.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Airport;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BoardingPassStatus;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinSection;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.commons.util.GeoUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.time.ZoneId;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNull;

public class CheckinStatusMapperTest {

    private static final String PATH_TO_JSON = "com/odigeo/frontend/resolvers/checkin/mapper/checkinStatusResponse.json";

    @Mock
    private GeoUtils geoUtils;
    @Spy
    private DateUtils dateUtils;
    @InjectMocks
    private CheckinStatusMapper mapper;

    private com.odigeo.checkin.api.v1.model.response.CheckinStatus statusResponse;

    @BeforeMethod
    public void init() throws IOException {
        MockitoAnnotations.openMocks(this);
        InputStream statusJson = this.getClass().getClassLoader().getResourceAsStream(PATH_TO_JSON);
        statusResponse = new ObjectMapper().readValue(statusJson, com.odigeo.checkin.api.v1.model.response.CheckinStatus.class);
        when(geoUtils.getZoneIdFromAirportIata(any())).thenReturn(ZoneId.of("UTC"));
    }

    @Test
    public void testMapOk() {
        CheckinStatus status = mapper.map(this.statusResponse);
        assertEquals(status.getSections().size(), 1);

        CheckinSection section = status.getSections().get(0);
        assertEquals(section.getPassengerName(), "Maverick Agostini");
        assertEquals(section.getStatus(), BoardingPassStatus.SUSPENDED);
        assertEquals(section.getBoardingPass().getId(), "4386d16cc76f44ab8ff38786639ae17c");
        assertNull(section.getBoardingPass().getUrl());
        assertEquals(section.getBoardingPass().getId(), "4386d16cc76f44ab8ff38786639ae17c");
        assertEquals(section.getBoardingPass().getFlightCode(), "QR138");
        assertEquals(section.getBoardingPass().getCarrierName(), "Qatar Airways");

        Airport departure = section.getBoardingPass().getDeparture();
        assertEquals(departure.getIata(), "BCN");
        assertEquals(departure.getName(), "Barcelona El Prat Airport");
        assertEquals(departure.getDatetime(), "2022-03-23T08:20:00Z");
        assertEquals(departure.getBoardingTime(), "1:20:00");

        Airport arrival = section.getBoardingPass().getArrival();
        assertEquals(arrival.getIata(), "DOH");
        assertEquals(arrival.getName(), "Doha International Airport");
        assertNull(arrival.getBoardingTime());
    }
}
