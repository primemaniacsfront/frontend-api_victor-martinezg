package com.odigeo.frontend.resolvers.shoppingcart.mappers.carrier;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertNull;

public class CarrierMapperTest {

    private CarrierMapper mapper;
    private ShoppingCartContext context;
    @Mock
    private VisitInformation visit;

    @BeforeMethod
    public void init() {
        openMocks(this);
        mapper = new CarrierMapperImpl();
        context = new ShoppingCartContext(visit);
    }

    @Test
    public void testCarrierContractToModel() {
        assertNull(mapper.carrierContractToModel(null));
        assertNull(mapper.carrierIdContractToModel(null, context));
    }

}
