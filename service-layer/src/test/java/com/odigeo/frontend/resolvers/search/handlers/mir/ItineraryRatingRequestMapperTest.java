package com.odigeo.frontend.resolvers.search.handlers.mir;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.model.Itinerary;
import com.odigeo.machinelearning.itineraryrating.api.v2.contract.request.ItineraryRatingRequest;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ItineraryRatingRequestMapperTest {

    @Mock
    private MirSearchCriteriaHandler mirSearchCriteriaHandler;
    @Mock
    private MirItineraryHandler mirItineraryHandler;

    @Mock
    private SearchRequest searchRequest;
    @Mock
    private SearchResponseDTO searchResponse;
    @Mock
    private ResolverContext context;

    @Mock
    private VisitInformation visit;

    private ItineraryRatingRequestMapper mapper;

    private static final String VISIT_CODE = "VISIT_CODE";
    private static final List<Itinerary> LIST_ITINERARY = Collections.emptyList();

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new ItineraryRatingRequestMapper();

        Guice.createInjector(binder -> {
            binder.bind(MirSearchCriteriaHandler.class).toProvider(() -> mirSearchCriteriaHandler);
            binder.bind(MirItineraryHandler.class).toProvider(() -> mirItineraryHandler);
        }).injectMembers(mapper);

        when(context.getVisitInformation()).thenReturn(visit);
        when(visit.getVisitCode()).thenReturn(VISIT_CODE);
        when(mirItineraryHandler.buildRatingItineraries(any())).thenReturn(LIST_ITINERARY);
    }

    @Test
    public void testMap() {

        ItineraryRatingRequest itineraryRequest = mapper.map(searchRequest, searchResponse, context);
        assertNotNull(itineraryRequest);
        assertEquals(itineraryRequest.getVisitInformation(), VISIT_CODE);
        assertEquals(itineraryRequest.getItineraries(), LIST_ITINERARY);


    }
}
