package com.odigeo.frontend.resolvers.payment.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CreditCardBinDetails;
import com.odigeo.collectionmethod.v3.CreditCardType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CreditCardBinDetailsMapperTest {

    private static final String BIN = "1234567890";
    private static final String COUNTRY_CODE = "US";
    private static final String LEVEL = "C";
    private static final String CARD_CODE = "DC";

    @Mock
    private com.odigeo.collectionmethod.v3.CreditCardBinDetails creditCardBinDetails;
    @Mock
    private CreditCardType creditCardType;

    private CreditCardBinDetailsMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new CreditCardBinDetailsMapperImpl();
    }

    @Test
    public void testCreditCardBinDetailsContractToModel() {
        assertNotNull(mapper.creditCardBinDetailsContractToModel(null));

        when(creditCardBinDetails.getCreditCardBin()).thenReturn(BIN);
        when(creditCardBinDetails.getCreditCardCountryCode()).thenReturn(COUNTRY_CODE);
        when(creditCardBinDetails.getCreditCardLevel()).thenReturn(LEVEL);
        when(creditCardType.getCode()).thenReturn(CARD_CODE);
        when(creditCardBinDetails.getCreditCardType()).thenReturn(creditCardType);

        CreditCardBinDetails details = mapper.creditCardBinDetailsContractToModel(creditCardBinDetails);

        assertEquals(details.getCreditCardBin(), BIN);
        assertEquals(details.getCreditCardCountryCode(), COUNTRY_CODE);
        assertEquals(details.getCreditCardLevel(), LEVEL);
        assertEquals(details.getCreditCardTypeCode(), CARD_CODE);
    }

}
