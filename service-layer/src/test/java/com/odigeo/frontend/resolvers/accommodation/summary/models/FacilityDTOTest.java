package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;

public class FacilityDTOTest extends BeanTest<FacilityDTO> {

    @Override
    protected FacilityDTO getBean() {
        FacilityDTO bean = new FacilityDTO();
        bean.setCode("CODE");
        bean.setDescription("DESCRIPTION");
        return bean;
    }
}
