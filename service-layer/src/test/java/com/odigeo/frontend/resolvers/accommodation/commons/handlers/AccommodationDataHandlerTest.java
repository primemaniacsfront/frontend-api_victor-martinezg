package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.TestDimensionPartitionsMapper;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.HotelSummaryRequestMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.HotelDetailsRequestDTO;
import com.odigeo.frontend.services.AccommodationStaticContentService;
import com.odigeo.hcsapi.v9.beans.RoomsDetailRequest;
import com.odigeo.hcsapi.v9.beans.SendPackage;
import com.odigeo.searchengine.v2.SearchEngineServiceRest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccommodationDataHandlerTest {


    @Mock
    private SearchEngineServiceRest searchEngineService;
    @Mock
    private AccommodationStaticContentService accommodationStaticContentService;
    @Mock
    private TestDimensionPartitionsMapper testDimensionPartitionsMapper;
    @Mock
    private HotelSummaryRequestMapper hotelSummaryRequestMapper;
    @Mock
    private VisitInformation visit;

    @InjectMocks
    private AccommodationDataHandler accommodationDataHandler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

    }

    @Test
    public void testRoomDetails() {
        String providerId = "123";
        String providerHotelId = "BC";
        String roomId = "1";
        accommodationDataHandler.getStaticRoomDetails(providerId, providerHotelId, visit, roomId);
        verify(accommodationStaticContentService).getRoomsDetail(any(RoomsDetailRequest.class));
    }

    @Test
    public void testRoomAvailability() {
        String visitCode = "visitCode";
        long searchId = 123L;
        long accommodationDealKey = 456L;
        when(visit.getVisitCode()).thenReturn(visitCode);
        accommodationDataHandler.getRoomAvailability(searchId, accommodationDealKey, visit);
        verify(searchEngineService).searchRoomAvailability(searchId, accommodationDealKey, visitCode);
    }

    @Test
    public void testAccommodationSummary() {
        SendPackage sendP = new SendPackage();
        when(hotelSummaryRequestMapper.map(anyList())).thenReturn(sendP);
        HotelDetailsRequestDTO requestDTO = new HotelDetailsRequestDTO();
        requestDTO.setProviderKeys(Collections.emptyList());
        accommodationDataHandler.getAccommodationSummary(requestDTO, visit);
        verify(accommodationStaticContentService).getHotelsSummary(sendP);
    }

    @Test
    public void getAccommodationDetails() {
        SendPackage sendP = new SendPackage();
        when(hotelSummaryRequestMapper.map(anyList())).thenReturn(sendP);
        HotelDetailsRequestDTO requestDTO = new HotelDetailsRequestDTO();
        requestDTO.setProviderKeys(Collections.emptyList());
        accommodationDataHandler.getAccommodationDetails(requestDTO, visit);
        verify(accommodationStaticContentService).getHotelsDetail(sendP);
    }

}
