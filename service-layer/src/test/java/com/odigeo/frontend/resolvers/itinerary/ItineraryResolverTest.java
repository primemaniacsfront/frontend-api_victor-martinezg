package com.odigeo.frontend.resolvers.itinerary;

import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.itinerary.handlers.ItineraryHandler;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.itinerary.ItineraryResolver.ITINERARY_FIELD;
import static com.odigeo.frontend.resolvers.itinerary.ItineraryResolver.SHOPPING_CART_TYPE;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class ItineraryResolverTest {

    @Mock
    private DataFetchingEnvironment environment;
    @Mock
    private ItineraryHandler handler;
    @Mock
    private ResolverContext resolverContext;

    @InjectMocks
    private ItineraryResolver resolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(environment.getContext()).thenReturn(resolverContext);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(SHOPPING_CART_TYPE);

        assertNotNull(queries.get(ITINERARY_FIELD));
    }

    @Test
    public void testItineraryDataFetcher() {
        resolver.itineraryDataFetcher(environment);
        verify(handler).map(eq(environment));
    }
}
