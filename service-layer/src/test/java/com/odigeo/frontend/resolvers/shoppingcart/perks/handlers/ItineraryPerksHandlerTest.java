package com.odigeo.frontend.resolvers.shoppingcart.perks.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Perks;
import com.odigeo.dapi.client.MembershipPerks;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import graphql.GraphQLContext;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ItineraryPerksHandlerTest {
    public static final BigDecimal FEE_NOT_APPLIED = BigDecimal.TEN;
    public static final BigDecimal FEE_APPLIED = BigDecimal.ONE;
    public static final String CURRENCY = "EUR";
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private GraphQLContext context;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private MembershipPerks membershipPerksApplied;
    @Mock
    private MembershipPerks membershipPerksNotApplied;

    @InjectMocks
    private ItineraryPerksHandler itineraryPerksHandler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(context.get(VisitInformation.class)).thenReturn(visitInformation);
        when(context.get(ShoppingCartSummaryResponse.class)).thenReturn(shoppingCartSummaryResponse);

    }

    @Test
    public void testSCWithDiscountApplied() {
        setAppliedPerks();
        assertEquals(itineraryPerksHandler.getPerksIfDiscountApplied(context), FEE_APPLIED);
    }

    private void setAppliedPerks() {
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getMembershipPerks()).thenReturn(membershipPerksApplied);
        when(membershipPerksApplied.getFee()).thenReturn(FEE_APPLIED);
    }

    @Test
    public void testSCWithDiscountNotApplied() {
        setNonAppliedPerks();
        assertEquals(itineraryPerksHandler.getPerksIfDiscountNotApplied(context), FEE_NOT_APPLIED);
    }

    private void setNonAppliedPerks() {
        when(shoppingCartSummaryResponse.getMembershipPerks()).thenReturn(membershipPerksNotApplied);
        when(membershipPerksNotApplied.getFee()).thenReturn(FEE_NOT_APPLIED);
    }

    @Test
    public void testMapPerksInWrongCaseWhereBothPerksAreAvailable() {
        setAppliedPerks();
        setNonAppliedPerks();
        when(visitInformation.getCurrency()).thenReturn(CURRENCY);
        Perks perks = itineraryPerksHandler.map(context);
        assertEquals(perks.getPrimeDiscount().getAmount(), FEE_APPLIED);
        assertEquals(perks.getPrimeDiscount().getCurrency(), CURRENCY);
    }

    @Test
    public void testMapPerksWhenApplied() {
        setAppliedPerks();
        when(visitInformation.getCurrency()).thenReturn(CURRENCY);
        Perks perks = itineraryPerksHandler.map(context);
        assertEquals(perks.getPrimeDiscount().getAmount(), FEE_APPLIED);
        assertEquals(perks.getPrimeDiscount().getCurrency(), CURRENCY);
    }

    @Test
    public void testMapPerksWhenNotApplied() {
        setNonAppliedPerks();
        when(visitInformation.getCurrency()).thenReturn(CURRENCY);
        Perks perks = itineraryPerksHandler.map(context);
        assertEquals(perks.getPrimeDiscount().getAmount(), FEE_NOT_APPLIED);
        assertEquals(perks.getPrimeDiscount().getCurrency(), CURRENCY);
    }
}
