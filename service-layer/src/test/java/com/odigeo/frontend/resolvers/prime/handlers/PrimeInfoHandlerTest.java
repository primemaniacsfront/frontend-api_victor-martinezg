package com.odigeo.frontend.resolvers.prime.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeInfo;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.prime.models.MembershipStatus;
import com.odigeo.frontend.resolvers.user.handlers.MembershipHandler;
import com.odigeo.frontend.services.PrimeService;
import com.odigeo.frontend.services.UserDescriptionService;
import com.odigeo.userprofiles.api.v2.model.User;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PrimeInfoHandlerTest {

    private static final String PRIME_USER_COOKIE = "name%40domain.com%3BSebastian%3Btrue%3Bmail";
    private static final String NON_PRIME_USER_COOKIE = "name%40domain.com%3BSebastian%3Bfalse%3Bmail";
    private static final String INCORRECT_PRIME_USER_COOKIE = "incorrectCookie";
    @Mock
    private PrimeService primeService;
    @Mock
    private MembershipHandler membershipHandler;
    @Mock
    private UserDescriptionService userService;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private SiteVariations siteVariations;
    @Mock
    private Membership membership;
    @Mock
    private User user;
    private PrimeInfoHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new PrimeInfoHandler();

        Guice.createInjector(binder -> {
            binder.bind(PrimeService.class).toProvider(() -> primeService);
            binder.bind(MembershipHandler.class).toProvider(() -> membershipHandler);
            binder.bind(UserDescriptionService.class).toProvider(() -> userService);
            binder.bind(SiteVariations.class).toProvider(() -> siteVariations);
        }).injectMembers(handler);

        when(context.getVisitInformation()).thenReturn(visit);
    }

    @Test
    public void testBuildPrimeInfo() {
        when(membership.getStatus()).thenReturn(MembershipStatus.ACTIVATED.name());
        when(primeService.isPrimeSite(visit)).thenReturn(true);
        when(userService.getUserByToken(context)).thenReturn(user);
        when(membershipHandler.getMembership(user, visit)).thenReturn(membership);

        PrimeInfo primeInfo = handler.buildPrimeInfo(context);
        assertTrue(primeInfo.getIsPrimeSite());
        assertTrue(primeInfo.getIsPrimeUser());
        assertFalse(primeInfo.getIsPrimeUserFromCookie());
    }

    @Test
    public void testBuildPrimeInfoUserNotLogged() {
        assertFalse(handler.buildPrimeInfo(context).getIsPrimeUser());
    }

    @Test
    public void testBuildPrimeInfoUserNotPrime() {
        when(userService.getUserByToken(context)).thenReturn(mock(User.class));
        assertFalse(handler.buildPrimeInfo(context).getIsPrimeUser());
    }

    @Test
    public void testBuildPrimeInfoCookieTestActive() {
        when(userService.getUserByToken(context)).thenReturn(mock(User.class));
        when(context.getRequestInfo()).thenReturn(mock(RequestInfo.class));
        when(siteVariations.isPrimeTestEnabled(visit)).thenReturn(true);
        when(siteVariations.isPrimeUserFromCookieEnabled(visit)).thenReturn(true);
        assertFalse(handler.buildPrimeInfo(context).getIsPrimeUserFromCookie());
    }

    @Test
    public void testBuildPrimeInfoCookieTestNotActive() {
        when(userService.getUserByToken(context)).thenReturn(mock(User.class));
        when(context.getRequestInfo()).thenReturn(mock(RequestInfo.class));
        when(siteVariations.isPrimeTestEnabled(visit)).thenReturn(true);
        when(siteVariations.isPrimeUserFromCookieEnabled(visit)).thenReturn(false);
        assertFalse(handler.buildPrimeInfo(context).getIsPrimeUserFromCookie());
    }

    @Test
    public void testBuildPrimeInfoCookieFalse() {
        when(userService.getUserByToken(context)).thenReturn(mock(User.class));
        when(context.getRequestInfo()).thenReturn(mock(RequestInfo.class));
        when(siteVariations.isPrimeTestEnabled(visit)).thenReturn(true);
        when(siteVariations.isPrimeUserFromCookieEnabled(visit)).thenReturn(true);
        when(context.getRequestInfo().getCookieValue(SiteCookies.SSO_REMEMBER_ME)).thenReturn(NON_PRIME_USER_COOKIE);
        assertFalse(handler.buildPrimeInfo(context).getIsPrimeUserFromCookie());
    }

    @Test
    public void testBuildPrimeInfoCookieTrue() {
        when(userService.getUserByToken(context)).thenReturn(mock(User.class));
        when(context.getRequestInfo()).thenReturn(mock(RequestInfo.class));
        when(siteVariations.isPrimeTestEnabled(visit)).thenReturn(true);
        when(siteVariations.isPrimeUserFromCookieEnabled(visit)).thenReturn(true);
        when(context.getRequestInfo().getCookieValue(SiteCookies.SSO_REMEMBER_ME)).thenReturn(PRIME_USER_COOKIE);
        assertTrue(handler.buildPrimeInfo(context).getIsPrimeUserFromCookie());
    }

    @Test
    public void testBuildPrimeInfoCookieIncorrect() {
        when(userService.getUserByToken(context)).thenReturn(mock(User.class));
        when(context.getRequestInfo()).thenReturn(mock(RequestInfo.class));
        when(siteVariations.isPrimeTestEnabled(visit)).thenReturn(true);
        when(siteVariations.isPrimeUserFromCookieEnabled(visit)).thenReturn(true);
        when(context.getRequestInfo().getCookieValue(SiteCookies.SSO_REMEMBER_ME)).thenReturn(INCORRECT_PRIME_USER_COOKIE);
        assertFalse(handler.buildPrimeInfo(context).getIsPrimeUserFromCookie());
    }

    @Test
    public void testBuildPrimeInfoCookieEmpty() {
        when(userService.getUserByToken(context)).thenReturn(mock(User.class));
        when(context.getRequestInfo()).thenReturn(mock(RequestInfo.class));
        when(siteVariations.isPrimeTestEnabled(visit)).thenReturn(true);
        when(siteVariations.isPrimeUserFromCookieEnabled(visit)).thenReturn(true);
        when(context.getRequestInfo().getCookieValue(SiteCookies.SSO_REMEMBER_ME)).thenReturn("");
        assertFalse(handler.buildPrimeInfo(context).getIsPrimeUserFromCookie());
    }

    @Test
    public void testBuildPrimeInfoCookieNull() {
        when(userService.getUserByToken(context)).thenReturn(mock(User.class));
        when(context.getRequestInfo()).thenReturn(mock(RequestInfo.class));
        when(siteVariations.isPrimeTestEnabled(visit)).thenReturn(true);
        when(siteVariations.isPrimeUserFromCookieEnabled(visit)).thenReturn(true);
        when(context.getRequestInfo().getCookieValue(SiteCookies.SSO_REMEMBER_ME)).thenReturn(null);
        assertFalse(handler.buildPrimeInfo(context).getIsPrimeUserFromCookie());
    }
}
