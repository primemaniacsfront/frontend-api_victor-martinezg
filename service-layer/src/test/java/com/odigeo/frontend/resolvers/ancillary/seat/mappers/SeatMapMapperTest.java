package com.odigeo.frontend.resolvers.ancillary.seat.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SeatMap;
import com.odigeo.itineraryapi.v1.response.SeatMapPreferencesDescriptorItem;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SeatMapMapperTest {

    @Mock
    private CabinsMapper cabinsMapper;

    @InjectMocks
    private SeatMapMapper seatMapMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(cabinsMapper.map(any())).thenReturn(new ArrayList<>());
    }

    @Test
    public void mapValidSeatMapList() {
        List<SeatMapPreferencesDescriptorItem> providerSeatMapList = getInput();
        List<SeatMap> mappedSeatMapList = seatMapMapper.map(providerSeatMapList);
        List<SeatMap> expectedSeatMapList = getOutput();
        SeatMap expectedSeatMap = expectedSeatMapList.get(0);
        SeatMap mappedSeatMap = mappedSeatMapList.get(0);

        assertEquals(providerSeatMapList.size(), mappedSeatMapList.size(), expectedSeatMapList.size());
        assertEquals(mappedSeatMap.getCabins(), expectedSeatMap.getCabins());
        assertEquals(mappedSeatMap.getSection(), expectedSeatMap.getSection());
    }

    @Test
    public void mapEmptySeatMapList() {
        List<SeatMapPreferencesDescriptorItem> providerSeatMapList = new ArrayList<>();
        List<SeatMap> mappedSeatMapList = seatMapMapper.map(providerSeatMapList);
        assertEquals(providerSeatMapList.size(), mappedSeatMapList.size());
    }

    private List<SeatMapPreferencesDescriptorItem> getInput() {
        List<SeatMapPreferencesDescriptorItem> inputSeatMapList = new ArrayList<>();
        SeatMapPreferencesDescriptorItem mockSeatMap = new SeatMapPreferencesDescriptorItem();

        mockSeatMap.setCabin(new ArrayList<>());
        mockSeatMap.setSection(0);

        inputSeatMapList.add(mockSeatMap);
        return inputSeatMapList;
    }

    private List<SeatMap> getOutput() {
        SeatMap outputSeatMap = new SeatMap();
        outputSeatMap.setCabins(new ArrayList<>());
        outputSeatMap.setSection(0);
        return Collections.singletonList(outputSeatMap);
    }
}
