package com.odigeo.frontend.resolvers.accommodation.search.models;

import bean.test.BeanTest;

import java.util.Collections;


public class ItinerarySelectionRequestDTOTest extends BeanTest<ItinerarySelectionRequestDTO> {

    @Override
    protected ItinerarySelectionRequestDTO getBean() {
        ItinerarySelectionRequestDTO bean = new ItinerarySelectionRequestDTO();
        bean.setFareItineraryKey("fareItineraryKey");
        bean.setFareFamilyCode("fareFamilycode");
        bean.setSegmentKeys(Collections.emptyList());
        return bean;
    }
}
