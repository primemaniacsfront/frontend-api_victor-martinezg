package com.odigeo.frontend.resolvers.deals.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.DateRange;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Deal;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.DealsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NumberRange;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.exception.ErrorCodes;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.exception.ServiceName;
import com.odigeo.frontend.commons.util.BigDecimalUtils;
import com.odigeo.frontend.resolvers.deals.configuration.DealsConfiguration;
import com.odigeo.frontend.resolvers.deals.mappers.ApparentPricesMapper;
import com.odigeo.frontend.resolvers.deals.mappers.CityMapper;
import com.odigeo.frontend.resolvers.deals.mappers.DateRangeMapper;
import com.odigeo.frontend.resolvers.deals.mappers.DateRangeMapperImpl;
import com.odigeo.frontend.resolvers.deals.mappers.DealsRoutePriceMapper;
import com.odigeo.frontend.resolvers.deals.mappers.MoneyMapper;
import com.odigeo.frontend.resolvers.deals.mappers.MoneyMapperImpl;
import com.odigeo.frontend.services.SearchPriceService;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.utils.LocalizedText;
import com.odigeo.marketing.search.price.v1.requests.IataPair;
import com.odigeo.marketing.search.price.v1.requests.PriceType;
import com.odigeo.marketing.search.price.v1.requests.SearchPricesRequest;
import com.odigeo.marketing.search.price.v1.responses.RoutePrice;
import com.odigeo.marketing.search.price.v1.responses.SearchPriceResponse;
import com.odigeo.marketing.search.price.v1.util.Money;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.collections.Sets;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.collections.Maps;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class DealsHandlerTest {

    private static final Integer MAX_POPULAR_DESTINATIONS = 50;
    private static final String AGGREGATE_BY = "AGGREGATE_BY";
    private static final String ROUND_TRIP = "ROUND_TRIP";
    private static final Double MAX_DISTANCE_KM = 3000D;
    private static final Integer MAX_AGE_DAYS = 1;
    private static final Integer SIZE = 60;

    private static final LocalDate NOW = LocalDate.now();
    private static final LocalDate DATE_DEP = NOW.plusWeeks(1);
    private static final LocalDate DATE_ARR = DATE_DEP.plusDays(2);

    private static final String IATA_DEP = "DEP";
    private static final int ID_DEP = 1;

    private static final String IATA_ARR = "ARR";
    private static final String NAME_ARR = "Destination";
    private static final int ID_ARR = 2;

    private static final Money EUR_100 = new Money.Builder().amount(BigDecimal.valueOf(100.00)).currency("EUR").build();
    private static final Money EUR_80 = new Money.Builder().amount(BigDecimal.valueOf(80.00)).currency("EUR").build();
    private static final double DIFFERENCE_DISCOUNT = 20.0;

    private final Map<String, Money> apparentPrices = Stream.of(
            new AbstractMap.SimpleEntry<>(PriceType.MINIMUM_PURCHASABLE_PRICE.name(), EUR_100),
            new AbstractMap.SimpleEntry<>(PriceType.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE.name(), EUR_80)
    ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

    @Mock
    private City departure;
    @Mock
    private City destination;
    @Mock
    private LocalizedText destinationName;
    @Mock
    private DealsRequest request;
    @Mock
    private NumberRange durationDays;
    @Mock
    private VisitInformation visitInformation;

    @Mock
    private DealsConfiguration dealsConfiguration;
    @Mock
    private DealsHelper dealsHelper;
    @Mock
    private SearchPriceService searchPriceService;

    private final DateRangeMapper dateRangeMapper = new DateRangeMapperImpl();
    private final DealsRoutePriceMapper dealsRoutePriceMapper = new DealsRoutePriceMapper();
    private final CityMapper cityMapper = new CityMapper();
    private final ApparentPricesMapper apparentPricesMapper = new ApparentPricesMapper();
    private final MoneyMapper moneyMapper = new MoneyMapperImpl();
    private final BigDecimalUtils bigDecimalUtils = new BigDecimalUtils();

    private final DealsHandler handler = new DealsHandler();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Injector pricesMapperInjector = Guice.createInjector(binder -> {
            binder.bind(MoneyMapper.class).toProvider(() -> moneyMapper);
        });
        pricesMapperInjector.injectMembers(apparentPricesMapper);

        Injector routeMapperInjector = Guice.createInjector(binder -> {
            binder.bind(CityMapper.class).toProvider(() -> cityMapper);
            binder.bind(ApparentPricesMapper.class).toProvider(() -> apparentPricesMapper);
            binder.bind(BigDecimalUtils.class).toProvider(() -> bigDecimalUtils);
        });
        routeMapperInjector.injectMembers(dealsRoutePriceMapper);

        Injector injector = Guice.createInjector(binder -> {
            binder.bind(DealsConfiguration.class).toProvider(() -> dealsConfiguration);
            binder.bind(DealsHelper.class).toProvider(() -> dealsHelper);
            binder.bind(SearchPriceService.class).toProvider(() -> searchPriceService);
            binder.bind(DateRangeMapper.class).toProvider(() -> dateRangeMapper);
            binder.bind(DealsRoutePriceMapper.class).toProvider(() -> dealsRoutePriceMapper);
        });
        injector.injectMembers(handler);

        when(dealsConfiguration.getMaxPopularDestinations()).thenReturn(MAX_POPULAR_DESTINATIONS);
        when(dealsConfiguration.getAggregateBy()).thenReturn(AGGREGATE_BY);
        when(dealsConfiguration.getMaxDistanceInKm()).thenReturn(MAX_DISTANCE_KM);
        when(dealsConfiguration.getMaxSearchAgeDays()).thenReturn(MAX_AGE_DAYS);
        when(dealsConfiguration.getTripType()).thenReturn(ROUND_TRIP);
        when(dealsConfiguration.getPriceType()).thenReturn(PriceType.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE.name());
        when(dealsConfiguration.getSize()).thenReturn(SIZE);

        when(visitInformation.getSite()).thenReturn(Site.GB);
        when(visitInformation.getLocale()).thenReturn(Locale.ENGLISH.getLanguage());
        when(request.getDeparture()).thenReturn(ID_DEP);

        DateRange depDateRange = mock(DateRange.class);
        when(depDateRange.getStartDate()).thenReturn(DATE_DEP.toString());
        when(depDateRange.getEndDate()).thenReturn(DATE_DEP.toString());
        when(request.getDepartureDateRange()).thenReturn(depDateRange);

        DateRange arrDateRange = mock(DateRange.class);
        when(arrDateRange.getStartDate()).thenReturn(DATE_ARR.toString());
        when(arrDateRange.getEndDate()).thenReturn(DATE_ARR.toString());
        when(request.getArrivalDateRange()).thenReturn(arrDateRange);

        when(departure.getGeoNodeId()).thenReturn(ID_DEP);
        when(departure.getIataCode()).thenReturn(IATA_DEP);
        when(departure.getCity()).thenReturn(departure);

        when(destination.getGeoNodeId()).thenReturn(ID_ARR);
        when(destination.getIataCode()).thenReturn(IATA_ARR);
        when(destination.getCity()).thenReturn(destination);
        when(destinationName.getText(Locale.ENGLISH)).thenReturn(NAME_ARR);
        when(destination.getName()).thenReturn(destinationName);

        when(dealsHelper.hasValidIata(departure)).thenReturn(true);
    }

    @Test
    public void testGetDeals() {
        Map<String, City> destinations = Maps.newHashMap();
        destinations.put(IATA_ARR, destination);
        IataPair iataPair = new IataPair.Builder().departureIata(IATA_DEP).arrivalIata(IATA_ARR).build();
        RoutePrice routePrice = new RoutePrice.Builder()
            .withOutboundDate(DATE_DEP)
            .withInboundDate(DATE_ARR)
            .withApparentPricesByPax(apparentPrices)
            .withInboundCityIataCode(IATA_ARR)
            .build();
        SearchPriceResponse response = new SearchPriceResponse.Builder().routePriceList(Arrays.asList(routePrice)).build();

        when(dealsHelper.getCity(ID_DEP)).thenReturn(departure);
        when(dealsHelper.getPopularDestinationCities(departure, visitInformation.getSite())).thenReturn(destinations);
        when(dealsHelper.getRoutesIataPairs(departure, destinations.values())).thenReturn(Sets.newSet(iataPair));
        when(searchPriceService.searchRoutePrices(any(SearchPricesRequest.class))).thenReturn(response);

        List<Deal> deals = handler.getDeals(request, visitInformation);
        Deal deal = deals.get(0);

        assertEquals(deal.getDepartureDate(), DATE_DEP);
        assertEquals(deal.getArrivalDate(), DATE_ARR);
        assertEquals(deal.getDiscount(), DIFFERENCE_DISCOUNT);
        deal.getApparentPricesByPax()
            .stream()
            .forEach(apparentPrice -> {
                assertEquals(apparentPrice.getPrice().getAmount(), apparentPrices.get(apparentPrice.getId()).getAmount());
                assertEquals(apparentPrice.getPrice().getCurrency(), apparentPrices.get(apparentPrice.getId()).getCurrency());
            });

        verify(dealsHelper).getCity(ID_DEP);
        verify(searchPriceService).searchRoutePrices(any(SearchPricesRequest.class));
    }

    @Test
    public void testGetDealsWithDurationFilter() {
        Map<String, City> destinations = Maps.newHashMap();
        destinations.put(IATA_ARR, destination);
        IataPair iataPair = new IataPair.Builder().departureIata(IATA_DEP).arrivalIata(IATA_ARR).build();
        RoutePrice routePrice = new RoutePrice.Builder()
                .withOutboundDate(DATE_DEP)
                .withInboundDate(DATE_ARR)
                .withApparentPricesByPax(apparentPrices)
                .withInboundCityIataCode(IATA_ARR)
                .build();
        SearchPriceResponse response = new SearchPriceResponse.Builder().routePriceList(Arrays.asList(routePrice)).build();

        when(durationDays.getStart()).thenReturn(3);
        when(durationDays.getEnd()).thenReturn(4);
        when(request.getDurationDays()).thenReturn(durationDays);
        when(dealsHelper.getCity(ID_DEP)).thenReturn(departure);
        when(dealsHelper.getPopularDestinationCities(departure, visitInformation.getSite())).thenReturn(destinations);
        when(dealsHelper.getRoutesIataPairs(departure, destinations.values())).thenReturn(Sets.newSet(iataPair));
        when(searchPriceService.searchRoutePrices(any(SearchPricesRequest.class))).thenReturn(response);

        List<Deal> deals = handler.getDeals(request, visitInformation);

        assertEquals(deals.size(), 0);
        verify(dealsHelper).getCity(ID_DEP);
        verify(searchPriceService).searchRoutePrices(any(SearchPricesRequest.class));
    }

    @Test
    public void testGetDealsNullSearchPrice() {
        Map<String, City> destinations = Maps.newHashMap();
        destinations.put(IATA_ARR, destination);
        IataPair iataPair = new IataPair.Builder().departureIata(IATA_DEP).arrivalIata(IATA_ARR).build();

        when(dealsHelper.getCity(ID_DEP)).thenReturn(departure);
        when(dealsHelper.getPopularDestinationCities(departure, visitInformation.getSite())).thenReturn(destinations);
        when(dealsHelper.getRoutesIataPairs(departure, destinations.values())).thenReturn(Sets.newSet(iataPair));
        when(searchPriceService.searchRoutePrices(any(SearchPricesRequest.class))).thenReturn(null);

        List<Deal> deals = handler.getDeals(request, visitInformation);

        assertEquals(deals.size(), 0);
        verify(dealsHelper).getCity(ID_DEP);
        verify(searchPriceService).searchRoutePrices(any(SearchPricesRequest.class));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetDealsExceptionSearchPrice() {
        Map<String, City> destinations = Maps.newHashMap();
        destinations.put(IATA_ARR, destination);
        IataPair iataPair = new IataPair.Builder().departureIata(IATA_DEP).arrivalIata(IATA_ARR).build();

        when(dealsHelper.getCity(ID_DEP)).thenReturn(departure);
        when(dealsHelper.getPopularDestinationCities(departure, visitInformation.getSite())).thenReturn(destinations);
        when(dealsHelper.getRoutesIataPairs(departure, destinations.values())).thenReturn(Sets.newSet(iataPair));
        when(searchPriceService.searchRoutePrices(any(SearchPricesRequest.class))).thenThrow(new ServiceException(ErrorCodes.INTERNAL_GENERIC.getDescription(), ServiceName.FRONTEND_API));

        handler.getDeals(request, visitInformation);

        verify(dealsHelper).getCity(ID_DEP);
        verify(searchPriceService).searchRoutePrices(any(SearchPricesRequest.class));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetDealsInvalidDeparture() {
        when(dealsHelper.getCity(ID_DEP)).thenThrow(new ServiceException(ErrorCodes.INTERNAL_GENERIC.getDescription(), ServiceName.FRONTEND_API));

        handler.getDeals(request, visitInformation);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testGetDealsInvalidDepartureIataNull() {
        when(departure.getIataCode()).thenReturn(null);
        when(dealsHelper.getCity(ID_DEP)).thenReturn(departure);
        when(dealsHelper.hasValidIata(departure)).thenReturn(false);

        handler.getDeals(request, visitInformation);
    }

}
