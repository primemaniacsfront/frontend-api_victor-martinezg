package com.odigeo.frontend.resolvers.itinerary.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Itinerary;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class TransportTypeMapperTest {

    @Mock
    private Segment segment;
    @Mock
    private Location departure;
    @Mock
    private Location destination;
    @Mock
    private Itinerary itinerary;

    private TransportTypeMapper mapper;
    private Section section;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new TransportTypeMapperImpl();
        section = new Section();
    }

    @Test
    public void testSetTransportTypeSegment() {
        when(segment.getSections()).thenReturn(Collections.emptyList());
        mapper.setTransportTypeSegment(segment);
        assertTrue(segment.getTransportTypes().isEmpty());
    }

    @Test
    public void testSetTransportTypeSection() {
        when(departure.getLocationType()).thenReturn(LocationType.AIRPORT);
        section.setDeparture(departure);
        when(destination.getLocationType()).thenReturn(LocationType.AIRPORT);
        section.setDestination(destination);

        when(departure.getLocationType()).thenReturn(LocationType.TRAIN_STATION);
        mapper.setTransportTypeSection(section);
        assertSame(section.getTransportType(), TransportType.TRAIN);

        when(destination.getLocationType()).thenReturn(LocationType.TRAIN_STATION);
        section.setDestination(destination);
        assertSame(section.getTransportType(), TransportType.TRAIN);

        when(departure.getLocationType()).thenReturn(LocationType.AIRPORT);
        mapper.setTransportTypeSection(section);
        assertSame(section.getTransportType(), TransportType.TRAIN);
    }

    @Test
    public void testSetTransportTypeItinerary() {
        when(itinerary.getLegs()).thenReturn(Collections.emptyList());
        mapper.setTransportTypeItinerary(itinerary);
        assertTrue(itinerary.getTransportTypes().isEmpty());
    }

}
