package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ProductIdMapperTest {

    private static final String ID = "ID";
    private static final ProductType PRODUCT_TYPE = ProductType.INSURANCE;

    @Test
    public void map() {
        ProductIdMapper productIdMapper = new ProductIdMapper();
        ProductId productId = productIdMapper.map(getProductId(), PRODUCT_TYPE);
        assertEquals(productId.getId(), ID);
        assertEquals(productId.getType(), PRODUCT_TYPE);
    }

    private com.edreamsodigeo.insuranceproduct.api.last.response.ProductId getProductId() {
        com.edreamsodigeo.insuranceproduct.api.last.response.ProductId productId = new com.edreamsodigeo.insuranceproduct.api.last.response.ProductId();
        productId.setId(ID);
        return productId;
    }

}
