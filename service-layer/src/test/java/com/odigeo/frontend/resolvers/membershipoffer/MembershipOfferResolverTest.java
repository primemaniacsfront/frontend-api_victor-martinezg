package com.odigeo.frontend.resolvers.membershipoffer;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionResponse;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.prime.handlers.SubscriptionRequestHandler;
import com.odigeo.frontend.resolvers.prime.handlers.SubscriptionResponseHandler;
import com.odigeo.membership.offer.api.request.MembershipOfferRequest;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffers;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.odigeo.frontend.resolvers.membershipoffer.MembershipOfferResolver.QUERY;
import static com.odigeo.frontend.resolvers.membershipoffer.MembershipOfferResolver.SUBSCRIPTION_OFFER;
import static com.odigeo.frontend.resolvers.membershipoffer.MembershipOfferResolver.SUBSCRIPTION_OFFER_BY_ID;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class MembershipOfferResolverTest {
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private GraphQLContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private SubscriptionRequestHandler subscriptionRequestHandler;
    @Mock
    private SubscriptionResponseHandler subscriptionResponseHandler;
    @Mock
    private MembershipOfferHandler membershipOfferHandler;
    @Mock
    private MembershipSubscriptionOffers membershipSubscriptionOffers;
    @Mock
    private JsonUtils jsonUtils;
    @InjectMocks
    private MembershipOfferResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(env.getGraphQlContext()).thenReturn(context);
        when(context.get(VisitInformation.class)).thenReturn(visit);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(QUERY);

        assertNotNull(queries.get(SUBSCRIPTION_OFFER));
        assertNotNull(queries.get(SUBSCRIPTION_OFFER_BY_ID));
    }

    @Test
    public void testSubscriptionFetcher() {
        MembershipOfferRequest offerRequest = mock(MembershipOfferRequest.class);
        MembershipSubscriptionOffer subscriptionOffer = mock(MembershipSubscriptionOffer.class);
        SubscriptionResponse responseDTO = mock(SubscriptionResponse.class);

        when(subscriptionRequestHandler.buildSubscriptionRequest(visit)).thenReturn(offerRequest);
        when(membershipOfferHandler.getSubscriptionOffer(offerRequest)).thenReturn(subscriptionOffer);
        when(subscriptionResponseHandler.buildSubscriptionResponse(subscriptionOffer)).thenReturn(responseDTO);

        assertSame(resolver.subscriptionOfferFetcher(env), responseDTO);
    }

    @Test
    public void testSubscriptionOffersFetcher() {
        MembershipOfferRequest offerRequest = mock(MembershipOfferRequest.class);
        SubscriptionResponse subscription1 = mock(SubscriptionResponse.class);
        SubscriptionResponse subscription2 = mock(SubscriptionResponse.class);
        List<SubscriptionResponse> responseDTO = Arrays.asList(subscription1, subscription2);

        when(subscriptionRequestHandler.buildSubscriptionRequest(visit)).thenReturn(offerRequest);
        when(membershipOfferHandler.getSubscriptionOffers(offerRequest)).thenReturn(membershipSubscriptionOffers);
        when(subscriptionResponseHandler.buildSubscriptionOffersResponse(membershipSubscriptionOffers)).thenReturn(responseDTO);

        assertSame(resolver.subscriptionOffersFetcher(env), responseDTO);
    }

    @Test
    public void testSubscriptionByIdFetcher() {
        String offerId = "offerId";
        MembershipSubscriptionOffer subscriptionOffer = mock(MembershipSubscriptionOffer.class);
        SubscriptionResponse responseDTO = mock(SubscriptionResponse.class);

        when(jsonUtils.fromDataFetching(env)).thenReturn(offerId);
        when(membershipOfferHandler.getSubscriptionOffer(offerId)).thenReturn(subscriptionOffer);
        when(subscriptionResponseHandler.buildSubscriptionResponse(subscriptionOffer)).thenReturn(responseDTO);

        assertSame(resolver.subscriptionOfferByIdFetcher(env), responseDTO);
    }

}
