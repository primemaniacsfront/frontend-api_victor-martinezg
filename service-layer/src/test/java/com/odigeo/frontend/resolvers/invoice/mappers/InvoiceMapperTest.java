package com.odigeo.frontend.resolvers.invoice.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InvoiceRequest;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class InvoiceMapperTest {

    @Mock
    private InvoiceRequest invoiceRequest;

    private InvoiceMapper mapper;

    @BeforeMethod
    public void init() {
        mapper = new InvoiceMapperImpl();
    }

    @Test
    public void testBuildShoppingCartInvoiceRequest() {
        assertNotNull(mapper.buildShoppingCartInvoiceRequest(null));

        com.odigeo.dapi.client.InvoiceRequest request = mapper.buildShoppingCartInvoiceRequest(invoiceRequest);
        assertNotNull(request);
    }

}
