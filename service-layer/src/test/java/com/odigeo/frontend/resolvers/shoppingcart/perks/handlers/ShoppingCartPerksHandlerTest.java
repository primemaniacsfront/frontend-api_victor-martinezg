package com.odigeo.frontend.resolvers.shoppingcart.perks.handlers;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerk;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerksType;
import com.odigeo.dapi.client.MembershipPerks;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import graphql.GraphQLContext;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

public class ShoppingCartPerksHandlerTest {

    private static final String CURRENCY = "EUR";
    private static final BigDecimal DISCOUNT = new BigDecimal(100);

    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryResponse;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private MembershipPerks membershipPerks;
    @Mock
    private ShoppingCart shoppingCart;
    private ShoppingCartPerksHandler shoppingCartPerksHandler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        shoppingCartPerksHandler = new ShoppingCartPerksHandler();
        when(membershipPerks.getFee()).thenReturn(DISCOUNT);
        initializeShoppingCartSummaryResponse();
    }

    @Test
    public void testMappingFlightPerksWhenTheDiscountIsNotApplied() {
        givenTheDiscountIsNotApplied();
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerks mappedPerks =
                shoppingCartPerksHandler.map(graphQLContext);
        MembershipPerk flightPerk = thenThePerksHaveBeenMappedProperly(mappedPerks);
        assertFalse(flightPerk.getIsApplied());
    }

    @Test
    public void testMappingFlightPerksWhenTheDiscountIsApplied() {
        givenTheDiscountIsApplied();
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerks mappedPerks =
                shoppingCartPerksHandler.map(graphQLContext);
        MembershipPerk flightPerk = thenThePerksHaveBeenMappedProperly(mappedPerks);
        assertTrue(flightPerk.getIsApplied());
    }

    private MembershipPerk thenThePerksHaveBeenMappedProperly(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipPerks mappedPerks) {
        MembershipPerk flightPerk = mappedPerks.getPerks().stream()
                .filter(perk -> MembershipPerksType.ITINERARY_PERKS.equals(perk.getPerkType()))
                .findFirst()
                .get();

        assertEquals(flightPerk.getDiscount().getCurrency(), CURRENCY);
        assertEquals(flightPerk.getDiscount().getAmount(), DISCOUNT);
        assertEquals(mappedPerks.getFee(), DISCOUNT);
        return flightPerk;
    }

    private void initializeShoppingCartSummaryResponse() {
        when(graphQLContext.get(ShoppingCartSummaryResponse.class)).thenReturn(shoppingCartSummaryResponse);
        when(graphQLContext.get(VisitInformation.class)).thenReturn(visitInformation);
        when(shoppingCartSummaryResponse.getShoppingCart()).thenReturn(shoppingCart);
        when(visitInformation.getCurrency()).thenReturn(CURRENCY);
    }

    private void givenTheDiscountIsNotApplied() {
        when(shoppingCartSummaryResponse.getMembershipPerks()).thenReturn(membershipPerks);
    }

    private void givenTheDiscountIsApplied() {
        when(shoppingCart.getMembershipPerks()).thenReturn(membershipPerks);
    }

}
