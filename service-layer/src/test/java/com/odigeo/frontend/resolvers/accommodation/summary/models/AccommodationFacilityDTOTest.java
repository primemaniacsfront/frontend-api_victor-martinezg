package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;

public class AccommodationFacilityDTOTest extends BeanTest<AccommodationFacilityDTO> {

    public AccommodationFacilityDTO getBean() {
        AccommodationFacilityDTO bean = new AccommodationFacilityDTO();
        bean.setCode("code");
        bean.setDescription("description");
        bean.setFacilityGroup(new AccommodationFacilityGroupDTO());

        return bean;
    }
}
