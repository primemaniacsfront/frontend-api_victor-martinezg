package com.odigeo.frontend.resolvers.prime.handlers;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.context.visit.types.WebInterface;
import com.odigeo.membership.offer.api.request.MembershipOfferRequest;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class SubscriptionRequestHandlerTest {

    @Test
    public void testBuildSubscriptionRequest() {
        SubscriptionRequestHandler handler = new SubscriptionRequestHandler();

        VisitInformation visit = mock(VisitInformation.class);
        Site website = Site.ES;
        WebInterface websiteInterface = WebInterface.ONE_FRONT_DESKTOP;
        String mktPortal = "marketingCode";
        Map<String, Integer> variations = new HashMap<>(0);

        when(visit.getSite()).thenReturn(website);
        when(visit.getWebInterface()).thenReturn(websiteInterface);
        when(visit.getMktPortal()).thenReturn(mktPortal);
        when(visit.getDimensionPartitionMap()).thenReturn(variations);

        MembershipOfferRequest offerRequest = handler.buildSubscriptionRequest(visit);

        assertEquals(offerRequest.getWebsiteCode(), website.name());
        assertEquals(offerRequest.getAnInterface(), websiteInterface.name());
        assertEquals(offerRequest.getMarketingPortal(), mktPortal);
        assertEquals(offerRequest.getTestTokenSet().getDimensionPartitionNumberMap(), variations);
        assertFalse(offerRequest.isExcludeFree());
    }

}
