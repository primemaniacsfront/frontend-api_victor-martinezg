package com.odigeo.frontend.resolvers.accommodation.rooms;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRoomAvailabilityRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRoomAvailabilityResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.accommodation.rooms.handlers.AccommodationRoomsResponseHandler;
import com.odigeo.frontend.resolvers.accommodation.rooms.handlers.RoomsHandler;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.SearchRoomAvailabilityResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.rooms.mappers.StaticRoomDetailsMapper;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.AccommodationRoomsResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.rooms.models.RoomDealDTO;
import com.odigeo.hcsapi.v9.beans.RoomsDetailResponse;
import com.odigeo.searchengine.v2.responses.accommodation.SearchRoomResponse;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DataFetchingFieldSelectionSet;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class SearchRoomAvailabilityResolverTest {

    private static final String ROOM_AVAILABILITY_QUERY = "searchRoomAvailability";

    @Mock
    private AccommodationRoomsResponseHandler accommodationRoomsResponseHandler;
    @Mock
    private SearchRoomAvailabilityResponseMapper accommodationRoomsResponseMapper;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;
    @Mock
    private RoomsHandler roomsHandler;
    @Mock
    private DataFetchingFieldSelectionSet dataFetchingFieldSelectionSet;
    @Mock
    private StaticRoomDetailsMapper staticRoomDetailsMapper;


    @InjectMocks
    private SearchRoomAvailabilityResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visit);
        when(visit.getCurrency()).thenReturn("EUR");
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(ROOM_AVAILABILITY_QUERY));
    }

    @Test
    public void testSearchFetcher() {
        SearchRoomAvailabilityResponse response = mock(SearchRoomAvailabilityResponse.class);
        SearchRoomAvailabilityRequest requestParams = mock(SearchRoomAvailabilityRequest.class);
        SearchRoomResponse roomAvailability = mock(SearchRoomResponse.class);

        when(visit.getCurrency()).thenReturn("EUR");
        AccommodationRoomsResponseDTO accommodationRoomsResponseDTO = mock(AccommodationRoomsResponseDTO.class);
        when(env.getSelectionSet()).thenReturn(dataFetchingFieldSelectionSet);
        when(dataFetchingFieldSelectionSet.contains("rooms/roomDetails")).thenReturn(false);

        when(jsonUtils.fromDataFetching(env, SearchRoomAvailabilityRequest.class)).thenReturn(requestParams);
        when(roomsHandler.getRoomAvailability(requestParams.getSearchId(), requestParams.getAccommodationDealKey(), visit)).thenReturn(roomAvailability);

        when(accommodationRoomsResponseHandler.buildAccommodationRoomResponse(roomAvailability, visit))
                .thenReturn(accommodationRoomsResponseDTO);
        when(accommodationRoomsResponseMapper.mapModelToContract(accommodationRoomsResponseDTO)).thenReturn(response);

        assertSame(resolver.accommodationRoomsFetcher(env), response);
        verify(roomsHandler).getRoomAvailability(requestParams.getSearchId(), requestParams.getAccommodationDealKey(), visit);

        verify(accommodationRoomsResponseHandler).buildAccommodationRoomResponse(roomAvailability, visit);
        verify(accommodationRoomsResponseMapper).mapModelToContract(accommodationRoomsResponseDTO);
    }

    @Test
    public void testSearchFetcherWithDetails() {
        SearchRoomAvailabilityResponse response = mock(SearchRoomAvailabilityResponse.class);
        SearchRoomAvailabilityRequest requestParams = mock(SearchRoomAvailabilityRequest.class);
        SearchRoomResponse roomAvailability = mock(SearchRoomResponse.class);
        RoomsDetailResponse roomDetails = mock(RoomsDetailResponse.class);
        AccommodationRoomsResponseDTO accommodationRoomsResponseDTO = mock(AccommodationRoomsResponseDTO.class);
        when(env.getSelectionSet()).thenReturn(dataFetchingFieldSelectionSet);
        when(dataFetchingFieldSelectionSet.contains("rooms/details")).thenReturn(true);
        when(jsonUtils.fromDataFetching(env, SearchRoomAvailabilityRequest.class)).thenReturn(requestParams);
        when(roomsHandler.getRoomAvailability(anyLong(), anyLong(), any(VisitInformation.class))).thenReturn(roomAvailability);
        when(roomsHandler.getStaticRoomDetails(anyString(), anyString(), any(VisitInformation.class), anyString())).thenReturn(roomDetails);
        when(accommodationRoomsResponseHandler.buildAccommodationRoomResponse(roomAvailability, visit))
                .thenReturn(accommodationRoomsResponseDTO);
        when(accommodationRoomsResponseMapper.mapModelToContract(accommodationRoomsResponseDTO)).thenReturn(response);
        when(accommodationRoomsResponseDTO.getRooms()).thenReturn(Collections.singletonList(getRoomDealDTO()));

        assertSame(resolver.accommodationRoomsFetcher(env), response);
        verify(roomsHandler).getRoomAvailability(anyLong(), anyLong(), any(VisitInformation.class));
        verify(roomsHandler).getStaticRoomDetails(anyString(), anyString(), any(VisitInformation.class), anyString());
        verify(accommodationRoomsResponseHandler).buildAccommodationRoomResponse(roomAvailability, visit);
        verify(accommodationRoomsResponseMapper).mapModelToContract(accommodationRoomsResponseDTO);
    }

    private RoomDealDTO getRoomDealDTO() {
        RoomDealDTO roomDealDTO = new RoomDealDTO();
        roomDealDTO.setProviderRoomId("123");
        roomDealDTO.setProviderId("123");
        roomDealDTO.setProviderHotelId("123");
        return roomDealDTO;
    }

}
