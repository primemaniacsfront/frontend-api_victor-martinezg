package com.odigeo.frontend.resolvers.prime;

import bean.test.BeanTest;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.prime.models.PrimeMarket;
import com.odigeo.frontend.resolvers.prime.models.PrimeMarketStatus;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.HashSet;

import static org.testng.Assert.assertEquals;

public class PrimeMarketsConfigurationTest extends BeanTest<PrimeMarketsConfiguration> {
    @Override
    protected PrimeMarketsConfiguration getBean() {
        return getPrimeMarketsConfiguration();
    }

    @Test
    public void testGetPrimeMarket() {
        PrimeMarketsConfiguration primeMarketsConfiguration = getPrimeMarketsConfiguration();
        PrimeMarket primeMarket = primeMarketsConfiguration.getPrimeMarket(Site.ES);
        assertEquals(PrimeMarketStatus.ROLLOUT, primeMarket.getStatus());
    }

    private PrimeMarketsConfiguration getPrimeMarketsConfiguration() {
        PrimeMarketsConfiguration bean = new PrimeMarketsConfiguration();
        PrimeMarket primeMarket = new PrimeMarket();
        primeMarket.setStatus(PrimeMarketStatus.ROLLOUT);
        primeMarket.setExcludedMktPortals(new HashSet<>());
        bean.setPrimeMarkets(new HashMap<Site, PrimeMarket>() { { put(Site.ES, primeMarket); } });
        return bean;
    }
}
