package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentRequest;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Set;

import static com.odigeo.frontend.commons.context.visit.types.Site.ES;
import static com.odigeo.frontend.commons.context.visit.types.Site.UNKNOWN;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class RebookingHandlerTest {

    private static final LocalDate MAX_DATE = LocalDate.of(2021, 9, 30);
    private static final String REBOOKING_CARRIER_ID = "BA";
    private static final String STEERING_RELIABLE_CARRIER_ID = "0B";
    private static final String STEERING_REFUND_CARRIER_ID = "4O";
    private static final String UNKNOWN_CARRIER_ID = "UNKNOWN";
    private static final Site REBOOKING_SITE = ES;
    private static final Site NOT_REBOOKING_SITE = UNKNOWN;

    @Mock
    private DateUtils dateUtils;
    @Mock
    private SearchRequest searchRequest;
    @Mock
    private VisitInformation visit;
    @Mock
    private LocalDate requestDate;
    @Mock
    private SearchItineraryDTO itineraryDTO;

    @InjectMocks
    private RebookingHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testIsRebookingAvailable() {
        initializeRebooking();
        assertTrue(handler.isRebookingAvailable(searchRequest, visit));
    }

    @Test
    public void testIsNotRebookingAvailableBySite() {
        initializeRebooking();
        when(visit.getSite()).thenReturn(NOT_REBOOKING_SITE);

        assertFalse(handler.isRebookingAvailable(searchRequest, visit));
    }

    @Test
    public void testIsNotRebookingAvailableByDates() {
        initializeRebooking();
        when(dateUtils.isBeforeOrEquals(requestDate, MAX_DATE)).thenReturn(false);

        assertFalse(handler.isRebookingAvailable(searchRequest, visit));
    }

    @Test
    public void testGetCarriersWithRebooking() {
        initializeRebooking();

        Set<String> sites = handler.getCarriersWithRebooking(searchRequest, visit);
        assertTrue(sites.contains(REBOOKING_CARRIER_ID));
        assertFalse(sites.contains(UNKNOWN_CARRIER_ID));
    }

    @Test
    public void testGetCarriersWithRebookingEmpty() {
        initializeRebooking();
        when(visit.getSite()).thenReturn(NOT_REBOOKING_SITE);

        assertTrue(handler.getCarriersWithRebooking(searchRequest, visit).isEmpty());
    }

    @Test
    public void testPopulateFreeRebooking() {
        initializeRebooking();
        initializeCarriers(REBOOKING_CARRIER_ID);

        handler.populateHasFreeRebooking(itineraryDTO, searchRequest, visit);
        verify(itineraryDTO).setHasFreeRebooking(true);
    }

    @Test
    public void testPopulateFreeRebookingNotAvailable() {
        initializeRebooking();
        when(visit.getSite()).thenReturn(NOT_REBOOKING_SITE);

        handler.populateHasFreeRebooking(itineraryDTO, searchRequest, visit);
        verify(itineraryDTO).setHasFreeRebooking(false);
    }

    @Test
    public void testPopulateFreeRebookingNoCarriers() {
        initializeRebooking();
        initializeCarriers(UNKNOWN_CARRIER_ID);

        handler.populateHasFreeRebooking(itineraryDTO, searchRequest, visit);
        verify(itineraryDTO).setHasFreeRebooking(false);
    }

    @Test
    public void testPopulateHasReliableCarriers() {
        initializeCarriers(STEERING_RELIABLE_CARRIER_ID);

        handler.populateHasReliableCarriers(itineraryDTO);
        verify(itineraryDTO).setHasReliableCarriers(true);
    }

    @Test
    public void testPopulateNotHasReliableCarriers() {
        initializeCarriers(UNKNOWN_CARRIER_ID);

        handler.populateHasReliableCarriers(itineraryDTO);
        verify(itineraryDTO).setHasReliableCarriers(false);
    }

    @Test
    public void testPopulateHasRefundCarriers() {
        initializeCarriers(STEERING_REFUND_CARRIER_ID);

        handler.populateHasRefundCarriers(itineraryDTO);
        verify(itineraryDTO).setHasRefundCarriers(true);
    }

    @Test
    public void testPopulateNotHasRefundCarriers() {
        initializeCarriers(UNKNOWN_CARRIER_ID);

        handler.populateHasRefundCarriers(itineraryDTO);
        verify(itineraryDTO).setHasRefundCarriers(false);
    }

    private void initializeRebooking() {
        ItineraryRequest itinerary = mock(ItineraryRequest.class);
        SegmentRequest segment = mock(SegmentRequest.class);
        String isoDate = "isoDate";

        when(searchRequest.getItinerary()).thenReturn(itinerary);
        when(itinerary.getSegments()).thenReturn(Collections.singletonList(segment));
        when(segment.getDate()).thenReturn(isoDate);
        when(dateUtils.convertFromIsoToLocalDate(isoDate)).thenReturn(requestDate);
        when(visit.getSite()).thenReturn(REBOOKING_SITE);
        when(dateUtils.isBeforeOrEquals(requestDate, MAX_DATE)).thenReturn(true);
    }

    private void initializeCarriers(String carrierId) {
        when(itineraryDTO.getSegmentsCarrierIds()).thenReturn(Collections.singleton(carrierId));
    }

}
