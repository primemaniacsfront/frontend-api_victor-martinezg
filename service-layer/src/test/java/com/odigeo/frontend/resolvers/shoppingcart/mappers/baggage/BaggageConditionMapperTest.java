package com.odigeo.frontend.resolvers.shoppingcart.mappers.baggage;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageCondition;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Location;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Segment;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class BaggageConditionMapperTest {

    @Mock
    private Carrier carrier;
    @Mock
    private Section section;
    @Mock
    private Location location;

    private Segment segment;
    private BaggageConditionMapper mapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new BaggageConditionMapperImpl();
    }

    @Test
    public void testSetBaggageConditionSegment() {
        segment = new Segment();
        when(carrier.getId()).thenReturn("code");
        when(location.getLocationType()).thenReturn(LocationType.AIRPORT);
        when(section.getDestination()).thenReturn(location);
        when(section.getDeparture()).thenReturn(location);
        when(section.getCarrier()).thenReturn(carrier);
        when(section.getBaggageAllowance()).thenReturn(0);
        segment.setSections(Arrays.asList(section));
        mapper.setBaggageConditionSegment(segment);
        assertEquals(segment.getBaggageCondition(), BaggageCondition.CABIN_INCLUDED);

        when(location.getLocationType()).thenReturn(LocationType.TRAIN_STATION);
        mapper.setBaggageConditionSegment(segment);
        assertEquals(segment.getBaggageCondition(), BaggageCondition.CHECKIN_INCLUDED);

        when(section.getBaggageAllowance()).thenReturn(1);
        mapper.setBaggageConditionSegment(segment);
        assertEquals(segment.getBaggageCondition(), BaggageCondition.CHECKIN_INCLUDED);
    }

}
