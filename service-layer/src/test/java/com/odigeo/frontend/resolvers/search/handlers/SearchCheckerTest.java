package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SegmentRequest;
import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.SiteHeaders;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.exception.ServiceException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;

public class SearchCheckerTest {

    private static final Integer NUM_ADULTS = 1;
    private static final LocalDate CURRENT_DATE = LocalDate.now();

    @Mock
    private SearchRequest searchRequest;
    @Mock
    private ItineraryRequest itineraryRequest;
    @Mock
    private SegmentRequest departureSegment;
    @Mock
    private SegmentRequest destinationSegment;
    @Mock
    private LocationRequest departure;
    @Mock
    private LocationRequest destination;
    @Mock
    private ResolverContext context;
    @Mock
    private RequestInfo requestInfo;

    private SearchChecker checker;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        checker = new SearchChecker();

        when(context.getRequestInfo()).thenReturn(requestInfo);
        when(requestInfo.getHeaderOrCookie(SiteHeaders.OF_JSESSIONID, SiteCookies.OF_JSESSIONID)).thenReturn("id");

        when(searchRequest.getItinerary()).thenReturn(itineraryRequest);
        when(itineraryRequest.getSegments()).thenReturn(Arrays.asList(departureSegment, destinationSegment));
        when(itineraryRequest.getNumAdults()).thenReturn(NUM_ADULTS);
        when(itineraryRequest.getNumChildren()).thenReturn(0);
        when(itineraryRequest.getNumInfants()).thenReturn(0);

        String currentDate = CURRENT_DATE.format(DateTimeFormatter.ISO_LOCAL_DATE);

        when(departureSegment.getDeparture()).thenReturn(departure);
        when(departureSegment.getDestination()).thenReturn(destination);
        when(departureSegment.getDate()).thenReturn(currentDate);

        when(destinationSegment.getDeparture()).thenReturn(destination);
        when(destinationSegment.getDestination()).thenReturn(departure);
        when(destinationSegment.getDate()).thenReturn(currentDate);

        when(departure.getIata()).thenReturn("departureIata");
        when(destination.getGeoNodeId()).thenReturn(0);
    }

    @Test
    public void testChecker() {
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testInvalidJsession() {
        when(requestInfo.getHeaderOrCookie(SiteHeaders.OF_JSESSIONID, SiteCookies.OF_JSESSIONID)).thenReturn(null);
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testInvalidNumAdults() {
        when(itineraryRequest.getNumAdults()).thenReturn(0);
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testInvalidNumChildren() {
        when(itineraryRequest.getNumChildren()).thenReturn(-1);
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testInvalidNumInfants() {
        when(itineraryRequest.getNumInfants()).thenReturn(-1);
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testTooManyPassengers() {
        when(itineraryRequest.getNumAdults()).thenReturn(NUM_ADULTS + 9);
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testTooManyChildren() {
        when(itineraryRequest.getNumChildren()).thenReturn(NUM_ADULTS * 2 + 1);
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testTooManyInfants() {
        when(itineraryRequest.getNumInfants()).thenReturn(NUM_ADULTS + 1);
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testInvalidLocation() {
        when(departure.getIata()).thenReturn(null);
        when(destination.getGeoNodeId()).thenReturn(null);
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testInvalidDateFormat() {
        when(departureSegment.getDate()).thenReturn("2020-01-");
        checker.check(searchRequest, context);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testInvalidDateDifferentSegments() {
        LocalDate departureDate = CURRENT_DATE.plus(1, ChronoUnit.DAYS);
        when(departureSegment.getDate()).thenReturn(departureDate.format(DateTimeFormatter.ISO_LOCAL_DATE));

        checker.check(searchRequest, context);
    }

}
