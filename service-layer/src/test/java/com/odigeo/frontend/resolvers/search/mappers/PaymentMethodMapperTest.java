package com.odigeo.frontend.resolvers.search.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertSame;

public class PaymentMethodMapperTest {

    @Test
    public void testMap() {
        PaymentMethodMapper mapper = new PaymentMethodMapper();
        PaymentMethod method = PaymentMethod.CREDITCARD;

        assertSame(mapper.map(method.name()), method);
    }

}
