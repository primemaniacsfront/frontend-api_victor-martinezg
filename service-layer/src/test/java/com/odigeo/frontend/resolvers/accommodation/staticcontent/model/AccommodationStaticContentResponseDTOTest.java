package com.odigeo.frontend.resolvers.accommodation.staticcontent.model;

import bean.test.BeanTest;

import java.util.Collections;

public class AccommodationStaticContentResponseDTOTest extends BeanTest<AccommodationStaticContentResponseDTO> {

    @Override
    protected AccommodationStaticContentResponseDTO getBean() {
        AccommodationStaticContentResponseDTO bean = new AccommodationStaticContentResponseDTO();
        bean.setAccommodationsStaticContent(Collections.emptyList());
        return bean;
    }
}
