package com.odigeo.frontend.resolvers.shoppingcart.collectionmethods;

import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.shoppingcart.collectionmethods.handlers.CollectionMethodsHandler;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.shoppingcart.collectionmethods.CollectionMethodsResolver.COLLECTION_METHODS;
import static com.odigeo.frontend.resolvers.shoppingcart.collectionmethods.CollectionMethodsResolver.SHOPPING_CART_TYPE;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class CollectionMethodsResolverTest {

    @Mock
    private DataFetchingEnvironment environment;
    @Mock
    private CollectionMethodsHandler handler;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext graphQLContext;

    @InjectMocks
    private CollectionMethodsResolver resolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(environment.getContext()).thenReturn(resolverContext);
        when(environment.getGraphQlContext()).thenReturn(graphQLContext);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(SHOPPING_CART_TYPE);

        assertNotNull(queries.get(COLLECTION_METHODS));
    }

    @Test
    public void testAvailableCollectionMethod() {
        resolver.availableCollectionMethod(environment);
        verify(handler).map(graphQLContext);
    }
}
