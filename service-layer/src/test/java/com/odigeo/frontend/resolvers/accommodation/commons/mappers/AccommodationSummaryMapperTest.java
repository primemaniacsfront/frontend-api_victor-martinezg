package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.odigeo.dapi.client.AccommodationCategory;
import com.odigeo.dapi.client.AccommodationDealInformation;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.dapi.client.AccommodationType;
import com.odigeo.dapi.client.BoardType;
import com.odigeo.dapi.client.GeoCoordinates;
import com.odigeo.dapi.client.RoomDeal;
import com.odigeo.dapi.client.RoomGroupDeal;
import com.odigeo.dapi.client.RoomSmokingPreference;
import com.odigeo.dapi.client.ThreeValuedLogic;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.BoardTypeDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.RoomDealSummaryDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.RoomSmokingPreferenceDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.responses.Coordinates;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

public class AccommodationSummaryMapperTest {
    private static final double DISTANCE_TO_CENTER = 15D;
    @Mock
    private AccommodationShoppingItem accommodationShoppingItem;
    @Mock
    private AccommodationReview accommodationReview;
    @Mock
    private City city;
    @Mock
    private RoomDeal roomDeal;
    @Mock
    private GeoCoordinates geoCoordinates;
    @Mock
    private GeoLocationHandler geolocationHandler;
    private AccommodationSummaryContext accommodationSummaryContext;

    @InjectMocks
    private AccommodationSummaryMapper mapper = new AccommodationSummaryMapperImpl();

    @Mock
    private AccommodationDealInformation accommodationDealInformation;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        accommodationSummaryContext = new AccommodationSummaryContext(city);
        accommodationSummaryContext.fillPaymentAtDestination(accommodationShoppingItem);
        when(city.getCoordinates()).thenReturn(new Coordinates());
        when(geolocationHandler.computeDistanceFromGeonode(eq(city), any(GeoCoordinatesDTO.class))).thenReturn(DISTANCE_TO_CENTER);
        when(accommodationShoppingItem.getAccommodationDealInformation()).thenReturn(accommodationDealInformation);
    }

    @Test
    public void testMap() {
        prepareFullRs();
        AccommodationSummaryResponseDTO accommodationSummaryResponseDTO = mapper.map(accommodationShoppingItem, accommodationReview, accommodationSummaryContext);

        AccommodationDealInformationDTO response = accommodationSummaryResponseDTO.getAccommodationDetail().getAccommodationDealInformation();
        assertEquals(response.getAddress(), accommodationDealInformation.getAddress());
        assertEquals(response.getCancellationPolicyDescription(), accommodationDealInformation.getCancellationPolicyDescription());
        assertEquals(response.getCategory(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory.STARS_3);
        assertEquals(response.getCityName(), accommodationDealInformation.getCityName());
        assertEquals(response.getLocation(), accommodationDealInformation.getLocation());
        assertEquals(response.getName(), accommodationDealInformation.getName());
        assertEquals(response.getType(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType.GUEST_HOUSE);

        assertEquals(accommodationSummaryResponseDTO.getAccommodationDetail().getDistanceFromCityCenter(), DISTANCE_TO_CENTER);
        assertEquals(accommodationSummaryResponseDTO.getAccommodationDetail().getHotelType(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType.GUEST_HOUSE);
        assertEquals(accommodationSummaryResponseDTO.getAccommodationReview(), accommodationReview);

        assertRoom(accommodationSummaryResponseDTO.getRoomDeals().get(0));

    }

    private void assertRoom(RoomDealSummaryDTO roomDealSummary) {
        assertEquals(roomDealSummary.getBoardType(), BoardTypeDTO.SC);
        assertEquals(roomDealSummary.getDescription(), roomDeal.getDescription());
        assertEquals(roomDealSummary.getCancellationFree(), roomDeal.isIsCancellationFree());
        assertEquals(roomDealSummary.getPaymentAtDestination().booleanValue(), accommodationSummaryContext.isPaymentAtDestination());
        assertEquals(roomDealSummary.getRoomKey(), roomDeal.getKey());
        assertEquals(roomDealSummary.getSmokingPreference(), RoomSmokingPreferenceDTO.NON_SMOKING);
        assertEquals(roomDealSummary.getCancelPolicy(), roomDeal.getCancelPolicy());
        assertEquals(roomDealSummary.getProviderId(), roomDeal.getProviderId());
        assertEquals(roomDealSummary.getDepositRequired(), roomDeal.isDepositRequired());
        assertEquals(roomDealSummary.getBedsDescription(), roomDeal.getBedsDescriptions().get(0));
    }

    private void prepareFullRs() {
        when(accommodationDealInformation.getName()).thenReturn("name");
        when(accommodationDealInformation.getAddress()).thenReturn("address");
        when(accommodationDealInformation.getCityName()).thenReturn("cityName");
        when(accommodationDealInformation.getDescription()).thenReturn("description");
        when(accommodationDealInformation.getLocation()).thenReturn("location");
        when(accommodationDealInformation.getChain()).thenReturn("chain");
        when(accommodationDealInformation.getType()).thenReturn(AccommodationType.GUEST_HOUSE);
        when(accommodationDealInformation.getCancellationPolicyDescription()).thenReturn("cancellationPolicyDescription");
        when(accommodationDealInformation.getCoordinates()).thenReturn(geoCoordinates);
        when(geoCoordinates.getLatitude()).thenReturn(new BigDecimal(8L));
        when(geoCoordinates.getLongitude()).thenReturn(new BigDecimal(88L));
        when(accommodationDealInformation.getCategory()).thenReturn(AccommodationCategory.STARS_3);
        when(accommodationDealInformation.getCancellationFree()).thenReturn(ThreeValuedLogic.TRUE);
        when(accommodationDealInformation.isPaymentAtDestination()).thenReturn(true);

        when(accommodationShoppingItem.getRoomDealLegend()).thenReturn(Arrays.asList(roomDeal));
        RoomGroupDeal groupDeal = mock(RoomGroupDeal.class);
        when(accommodationShoppingItem.getRoomGroupDeal()).thenReturn(groupDeal);
        when(groupDeal.getRoomsKeys()).thenReturn(Collections.singletonList("key"));
        roomDealMock();
    }

    private void roomDealMock() {
        when(roomDeal.isIsCancellationFree()).thenReturn(true);
        when(roomDeal.getDescription()).thenReturn("roomDescription");
        when(roomDeal.getKey()).thenReturn("key");
        when(roomDeal.getCancelPolicy()).thenReturn("cancelPolicy");
        when(roomDeal.getSmokingPreference()).thenReturn(RoomSmokingPreference.NON_SMOKING);
        when(roomDeal.getProviderId()).thenReturn("providerId");
        when(roomDeal.isDepositRequired()).thenReturn(false);
        when(roomDeal.getBoardType()).thenReturn(BoardType.SC);
        when(roomDeal.getBedsDescriptions()).thenReturn(Arrays.asList("bedDescription"));
    }

    @Test
    public void testMapRoomDeal() {
        roomDealMock();
        when(accommodationDealInformation.isPaymentAtDestination()).thenReturn(true);
        assertRoom(mapper.mapRoomDeal(roomDeal, accommodationSummaryContext));
    }

    @Test
    public void testMapRoomsDeal() {
        prepareFullRs();
        assertRoom(mapper.mapRoomsDeal(accommodationShoppingItem, accommodationSummaryContext).get(0));
    }

    @Test
    public void testMapDuplicatedRoomsDeal() {
        prepareFullRs();
        RoomGroupDeal groupDeal = mock(RoomGroupDeal.class);
        when(accommodationShoppingItem.getRoomGroupDeal()).thenReturn(groupDeal);
        when(groupDeal.getRoomsKeys()).thenReturn(Arrays.asList("key", "key"));
        assertRoom(mapper.mapRoomsDeal(accommodationShoppingItem, accommodationSummaryContext).get(0));
        assertRoom(mapper.mapRoomsDeal(accommodationShoppingItem, accommodationSummaryContext).get(1));
    }
}
