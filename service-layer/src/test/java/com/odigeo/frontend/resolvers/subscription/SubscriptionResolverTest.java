package com.odigeo.frontend.resolvers.subscription;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionFdoResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.subscription.handlers.SubscriptionHandler;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class SubscriptionResolverTest {

    private static final String SUBSCRIPTION_TO_FDO_MUTATION = "subscribeToFdo";
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private SubscriptionHandler subscriptionHandler;
    @InjectMocks
    private SubscriptionResolver resolver;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Mutation");

        assertNotNull(queries.get(SUBSCRIPTION_TO_FDO_MUTATION));
    }

    @Test
    public void testSubscribeToFdoDataFetcher() {
        String email = "test@gmail.com";
        SubscriptionFdoResponse fdoResponse = new SubscriptionFdoResponse(email);
        when(jsonUtils.fromDataFetching(env)).thenReturn(email);
        when(subscriptionHandler.subscribeToFdo(email, context)).thenReturn(fdoResponse);
        assertEquals(resolver.subscribeToFdoDataFetcher(env), fdoResponse);
    }
}
