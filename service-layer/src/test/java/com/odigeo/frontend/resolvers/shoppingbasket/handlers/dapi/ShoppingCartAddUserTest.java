package com.odigeo.frontend.resolvers.shoppingbasket.handlers.dapi;

import com.odigeo.dapi.client.RegisterUserLoginRequest;
import com.odigeo.dapi.client.RegisterUserLoginResult;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.services.DapiService;
import com.odigeo.userprofiles.api.v2.model.User;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ShoppingCartAddUserTest {

    @Mock
    private DapiService dapiService;
    @Mock
    private User user;
    @Mock
    private ResolverContext resolverContext;


    @InjectMocks
    private ShoppingCartAddUser shoppingCartAddUser;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testRegisterOk() {
        when(dapiService.registerUserLogin(any(RegisterUserLoginRequest.class), any(ResolverContext.class))).thenReturn(new RegisterUserLoginResult());
        Boolean result = shoppingCartAddUser.addUser(user, resolverContext);
        assertTrue(result);
    }

    @Test
    public void testRegisterKO() {
        when(dapiService.registerUserLogin(any(RegisterUserLoginRequest.class), any(ResolverContext.class))).thenReturn(null);
        Boolean result = shoppingCartAddUser.addUser(user, resolverContext);
        assertFalse(result);
    }
}
