package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingBasketId;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ShoppingBasketIdMapperTest {

    @Test
    public void mapOk() {
        ShoppingBasketIdMapper shoppingBasketIdMapper = new ShoppingBasketIdMapper();
        com.edreamsodigeo.insuranceproduct.api.last.request.ShoppingBasketId basketId = shoppingBasketIdMapper.map(new ShoppingBasketId(60L));
        assertEquals(basketId.getId().longValue(), 60L);
    }

}
