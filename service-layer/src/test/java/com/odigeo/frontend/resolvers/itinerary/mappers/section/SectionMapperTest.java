package com.odigeo.frontend.resolvers.itinerary.mappers.section;

import com.odigeo.dapi.client.Section;
import com.odigeo.dapi.client.SectionResult;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class SectionMapperTest {

    private SectionMapper mapper;

    @Mock
    private ShoppingCartContext context;
    @Mock
    private SectionResult sectionResult;
    @Mock
    private Section section;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new SectionMapperImpl();
    }

    @Test
    public void testSectionIdsContractToModel() {
        assertNull(mapper.sectionIdsContractToModel(null, context));
    }

    @Test
    public void testSectionIdContractToModel() {
        assertNull(mapper.sectionIdContractToModel(null, context));
    }

    @Test
    public void testSectionContractToModel() {
        assertNull(mapper.sectionContractToModel(null, null, context));
        assertNotNull(mapper.sectionContractToModel(sectionResult, StringUtils.EMPTY, context));
        assertNotNull(mapper.sectionContractToModel(null, StringUtils.EMPTY, context));
        assertNotNull(mapper.sectionContractToModel(sectionResult, null, context));

        Calendar calendar = Calendar.getInstance();
        when(section.getDepartureDate()).thenReturn(calendar);
        when(sectionResult.getSection()).thenReturn(section);
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Section model = mapper.sectionContractToModel(sectionResult, null, context);
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneOffset.UTC);
        String calendarFormatted = formatter.format(calendar.getTime().toInstant());
        assertEquals(model.getDepartureDate().format(formatter), calendarFormatted);
    }
}
