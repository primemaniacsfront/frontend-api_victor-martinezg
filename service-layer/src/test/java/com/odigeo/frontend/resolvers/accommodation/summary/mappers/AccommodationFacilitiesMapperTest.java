package com.odigeo.frontend.resolvers.accommodation.summary.mappers;

import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationFacilityDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.FacilityGroupsDTO;
import com.odigeo.hcsapi.v9.beans.FacilityInfo;
import com.odigeo.hcsapi.v9.beans.Legend;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationFacility;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationFacilityGroup;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class AccommodationFacilitiesMapperTest {

    private static final String GROUP_KEY = "GK";
    private static final String GROUP_VALUE = "GV";
    private static final String FACILITY_CODE = "FC";
    private static final String FACILITY_DES = "FD";
    @Mock
    private FacilityInfo facilityInfo;
    @Mock
    private Map<String, FacilityInfo> facilityLegend;
    @Mock
    private Legend legend;
    @Mock
    private AccommodationFacility accommodationFacility;
    @InjectMocks
    private AccommodationFacilitiesMapper mapper;
    private Map<String, String> facilitiesGroupLegend;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(facilityInfo.getGroupCode()).thenReturn(GROUP_KEY);
        when(facilityInfo.getText()).thenReturn(FACILITY_DES);
        when(legend.getFacilityLegend()).thenReturn(facilityLegend);
        when(facilityLegend.get(FACILITY_CODE)).thenReturn(facilityInfo);
        facilitiesGroupLegend = new HashMap<>();
        facilitiesGroupLegend.put(GROUP_KEY, GROUP_VALUE);
        when(legend.getFacilitiesGroupLegend()).thenReturn(facilitiesGroupLegend);
    }

    @Test
    public void testMapHcsFacilityNull() {
        AccommodationFacilityDTO dto = mapper.mapHcsFacility("", legend);
        assertNull(dto);
    }

    @Test
    public void testMapHcsFacilityAccommodationWithoutDescription() {
        String facilityCode = "facilityCode";
        when(facilityLegend.get(facilityCode)).thenReturn(facilityInfo);
        when(facilityInfo.getGroupCode()).thenReturn("other");
        AccommodationFacilityDTO dto = mapper.mapHcsFacility(facilityCode, legend);
        assertSame(dto.getCode(), facilityCode);
        assertSame(dto.getDescription(), facilityInfo.getText());
        assertSame(dto.getFacilityGroup().getCode(), facilityInfo.getGroupCode());
        assertNull(dto.getFacilityGroup().getDescription());
    }

    @Test
    public void testMapHcsFacilityWhenWasNotFoundFacilityCode() {
        List<FacilityGroupsDTO> dto = mapper.mapFacilityGroups(Collections.singletonList("FAKE"), legend);
        assertTrue(dto.isEmpty());
    }

    @Test
    public void testMapHcsFacilityWithoutDescription() {
        when(facilityInfo.getText()).thenReturn(null);
        assertTrue(mapper.mapFacilityGroups(Collections.singletonList(FACILITY_CODE), legend).isEmpty());
    }

    @Test
    public void testMapHcsFacilityWhenFacilityHasWrongGroup() {
        when(facilityInfo.getGroupCode()).thenReturn("FAKE");
        assertTrue(mapper.mapFacilityGroups(Collections.singletonList(FACILITY_CODE), legend).isEmpty());
    }

    @Test
    public void testMapHcsFacilityWithDescription() {
        List<FacilityGroupsDTO> dto = mapper.mapFacilityGroups(Collections.singletonList(FACILITY_CODE), legend);
        assertSame(dto.get(0).getFacilities().get(0).getCode(), FACILITY_CODE);
        assertSame(dto.get(0).getFacilities().get(0).getDescription(), facilityInfo.getText());
        assertSame(dto.get(0).getCode(), facilityInfo.getGroupCode());
        assertSame(dto.get(0).getDescription(), GROUP_VALUE);
    }

    @Test
    public void testMapHcsFacilityGroupsEmpty() {
        assertTrue(mapper.mapFacilityGroups(null).isEmpty());
        assertTrue(mapper.mapFacilityGroups(new ArrayList<>()).isEmpty());
    }

    @Test
    public void testMapHcsFacilityGroupsWithoutGroup() {
        when(accommodationFacility.getFacilityGroup()).thenReturn(null);
        when(accommodationFacility.getCode()).thenReturn(FACILITY_CODE);
        when(accommodationFacility.getDescription()).thenReturn(FACILITY_DES);
        assertTrue(mapper.mapFacilityGroups(Collections.singletonList(accommodationFacility)).isEmpty());
    }

    @Test
    public void testMapHcsFacilityGroups() {
        AccommodationFacilityGroup accommodationFacilityGroup = mock(AccommodationFacilityGroup.class);
        when(accommodationFacilityGroup.getCode()).thenReturn(GROUP_KEY);
        when(accommodationFacilityGroup.getDescription()).thenReturn(GROUP_VALUE);

        String facilityCode = "code2";
        String facilityDes = "des2";
        when(accommodationFacility.getFacilityGroup()).thenReturn(accommodationFacilityGroup);
        when(accommodationFacility.getCode()).thenReturn(FACILITY_CODE);
        when(accommodationFacility.getDescription()).thenReturn(FACILITY_DES);

        AccommodationFacility accommodationFacilityTwo = mock(AccommodationFacility.class);
        when(accommodationFacilityTwo.getCode()).thenReturn(facilityCode);
        when(accommodationFacilityTwo.getDescription()).thenReturn(facilityDes);
        when(accommodationFacilityTwo.getFacilityGroup()).thenReturn(accommodationFacilityGroup);


        List<FacilityGroupsDTO> dto = mapper.mapFacilityGroups(Arrays.asList(accommodationFacility, accommodationFacilityTwo));
        assertEquals(dto.size(), 1);
        assertEquals(dto.get(0).getCode(), GROUP_KEY);
        assertEquals(dto.get(0).getDescription(), GROUP_VALUE);
        assertEquals(dto.get(0).getFacilities().size(), 2);
        assertEquals(dto.get(0).getFacilities().get(0).getCode(), FACILITY_CODE);
        assertEquals(dto.get(0).getFacilities().get(0).getDescription(), FACILITY_DES);
        assertEquals(dto.get(0).getFacilities().get(1).getCode(), facilityCode);
        assertEquals(dto.get(0).getFacilities().get(1).getDescription(), facilityDes);
    }

}
