package com.odigeo.frontend.resolvers.search.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsurancePolicy;
import com.odigeo.searchengine.v2.responses.insurance.Insurance;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.testng.annotations.Test;

import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class InsurancePolicyMapperTest {

    private static final String EXTERNAL_INSURANCE = "NEWHUB162";
    private static final String ODIGEO_GUARANTEE = "VINGSELF";

    @Test
    public void testMap() {
        InsurancePolicyMapper mapper = new InsurancePolicyMapper();
        Insurance insurance = mock(Insurance.class);

        Stream.of(
            new ImmutablePair<>(EXTERNAL_INSURANCE, InsurancePolicy.EXTERNAL_INSURANCE),
            new ImmutablePair<>(ODIGEO_GUARANTEE, InsurancePolicy.ODIGEO_GUARANTEE),
            new ImmutablePair<>("unknow policy", InsurancePolicy.OTHER),
            new ImmutablePair<String, InsurancePolicy>(null, InsurancePolicy.OTHER)
        ).forEach(pair -> {
            when(insurance.getPolicy()).thenReturn(pair.getLeft());
            assertSame(mapper.map(insurance), pair.getRight());
        });
    }

}
