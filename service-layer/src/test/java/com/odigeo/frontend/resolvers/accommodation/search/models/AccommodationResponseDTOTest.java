package com.odigeo.frontend.resolvers.accommodation.search.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchMetadata;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.GeoLocation;

import java.util.Collections;


public class AccommodationResponseDTOTest extends BeanTest<AccommodationResponseDTO> {

    @Override
    protected AccommodationResponseDTO getBean() {
        AccommodationResponseDTO bean = new AccommodationResponseDTO();
        bean.setAccommodations(Collections.emptyList());
        bean.setCurrency("EUR");
        bean.setMetadata(new AccommodationSearchMetadata());
        bean.setLocation(new GeoLocation());
        return bean;
    }
}
