package com.odigeo.frontend.resolvers.search.handlers.farefamily;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamilyRequest;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.SiteVariations;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.search.handlers.pricing.AbstractPricePolicy;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.services.ItineraryService;
import com.odigeo.frontend.services.SearchService;
import com.odigeo.itineraryapi.v1.response.FareFamily;
import com.odigeo.itineraryapi.v1.response.FareFamilyPerk;
import com.odigeo.itineraryapi.v1.response.RetrieveFareFamiliesResponse;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import org.apache.commons.lang3.math.NumberUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class FareFamilyHandlerTest {

    private static final Long SEARCH_ID = NumberUtils.LONG_ONE;
    private static final String FARE_ITIN_KEY = "1,1A";
    private static final Integer FARE_ITIN_KEY_PARSED = 1;
    private static final List<String> SEGMENT_KEYS = Arrays.asList("0", "1");
    private static final String SEGMENT_KEYS_PARSED = "0 1";
    private static final Integer PASSENGERS = 2;
    private static final BigDecimal ITINERARY_PRICE = BigDecimal.valueOf(5).setScale(2);

    private static final String ENABLED_CARRIER = "LH";
    private static final String DISABLED_CARRIER = "FR";

    private static final String CABIN_BAG = "CABIN_BAG";
    private static final String ALWAYS = "Always";

    @Mock
    private SiteVariations siteVariations;
    @Mock
    private FareFamilyVisibilityMapper fareFamilyVisibilityMapper;
    @Mock
    private ItineraryService itineraryService;
    @Mock
    private SearchService searchService;
    @Mock
    private AbstractPricePolicy abstractPricePolicy;
    @Mock
    private SearchResultPriceMapper searchResultPriceMapper;
    @Mock
    private FareFamilyRequest fareFamilyRequest;
    @Mock
    private VisitInformation visit;
    @Mock
    private SearchItineraryDTO itinerary;
    @Mock
    private SearchResults searchResults;

    private FareFamilyHandler handler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        handler = new FareFamilyHandler();

        Guice.createInjector(binder -> {
            binder.bind(SiteVariations.class).toProvider(() -> siteVariations);
            binder.bind(FareFamilyVisibilityMapper.class).toProvider(() -> fareFamilyVisibilityMapper);
            binder.bind(ItineraryService.class).toProvider(() -> itineraryService);
            binder.bind(SearchService.class).toProvider(() -> searchService);
            binder.bind(AbstractPricePolicy.class).toProvider(() -> abstractPricePolicy);
            binder.bind(SearchResultPriceMapper.class).toProvider(() -> searchResultPriceMapper);
        }).injectMembers(handler);

        when(fareFamilyRequest.getSearchId()).thenReturn(SEARCH_ID);
        when(fareFamilyRequest.getFareItineraryKey()).thenReturn(FARE_ITIN_KEY);
        when(fareFamilyRequest.getSegmentKeys()).thenReturn(SEGMENT_KEYS);
        when(fareFamilyRequest.getPassengers()).thenReturn(PASSENGERS);

        when(fareFamilyVisibilityMapper.map(eq(CABIN_BAG))).thenReturn(ALWAYS);
    }

    @Test
    public void testPopulateIsAvailable() {
        givenIsFareFamilyEnabled();
        when(itinerary.getSegmentsCarrierIds()).thenReturn(Collections.singleton(ENABLED_CARRIER));

        handler.populateIsAvailable(itinerary, visit);
        verify(itinerary).setIsFareUpgradeAvailable(eq(true));
    }

    @Test
    public void testPopulateIsNotAvailable() {
        givenIsFareFamilyEnabled();
        when(itinerary.getSegmentsCarrierIds()).thenReturn(Collections.singleton(DISABLED_CARRIER));

        handler.populateIsAvailable(itinerary, visit);
        verify(itinerary).setIsFareUpgradeAvailable(eq(false));
    }

    @Test
    public void testPopulateIsNotAvailableByTest() {
        when(itinerary.getSegmentsCarrierIds()).thenReturn(Collections.singleton(ENABLED_CARRIER));

        handler.populateIsAvailable(itinerary, visit);
        verify(itinerary).setIsFareUpgradeAvailable(eq(false));
    }

    @Test
    public void testNotRetrieveFareFamiliesIfTestDisabled() {
        configureFareFamilyRequestCarrierIds(ENABLED_CARRIER);
        configureItineraryServiceFareFamilies();
        configureItineraryPrice();
        List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily> result = handler.retrieveFareFamilies(fareFamilyRequest, visit);
        assertTrue(result.isEmpty());
    }

    @Test
    public void testNotRetrieveFareFamiliesIfCarriersNotInScope() {
        givenIsFareFamilyEnabled();
        configureFareFamilyRequestCarrierIds(DISABLED_CARRIER);
        configureItineraryServiceFareFamilies();
        configureItineraryPrice();
        List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily> result = handler.retrieveFareFamilies(fareFamilyRequest, visit);
        assertTrue(result.isEmpty());
    }

    @Test
    public void testNotRetrieveFareFamiliesIfNullItineraryPrice() {
        givenIsFareFamilyEnabled();
        configureFareFamilyRequestCarrierIds(ENABLED_CARRIER);
        configureItineraryServiceFareFamilies();
        List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily> result = handler.retrieveFareFamilies(fareFamilyRequest, visit);
        assertTrue(result.isEmpty());
    }

    @Test
    public void testRetrieveFareFamilies() {
        givenIsFareFamilyEnabled();
        configureFareFamilyRequestCarrierIds(ENABLED_CARRIER);
        configureItineraryServiceFareFamilies();
        configureItineraryPrice();
        List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily> result = handler.retrieveFareFamilies(fareFamilyRequest, visit);
        assertFareFamily(result.get(0));
    }

    @Test
    public void testRetrieveFareFamiliesNullEmpty() {
        givenIsFareFamilyEnabled();
        configureFareFamilyRequestCarrierIds(ENABLED_CARRIER);
        configureItineraryPrice();
        List<com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily> result = handler.retrieveFareFamilies(fareFamilyRequest, visit);
        assertTrue(result.isEmpty());
    }

    private void configureFareFamilyRequestCarrierIds(String... carrierIds) {
        when(fareFamilyRequest.getCarrierIds()).thenReturn(Arrays.asList(carrierIds));
    }

    private void givenIsFareFamilyEnabled() {
        when(siteVariations.isFareFamilyTestEnabled(eq(visit))).thenReturn(true);
    }

    private void configureItineraryServiceFareFamilies() {
        when(itineraryService.retrieveFareFamilies(eq(SEARCH_ID), eq(FARE_ITIN_KEY_PARSED), eq(SEGMENT_KEYS_PARSED)))
                .thenReturn(getFareFamiliesResponse());
    }

    private void configureItineraryPrice() {
        when(searchService.getSearchResult(eq(SEARCH_ID), eq(FARE_ITIN_KEY_PARSED))).thenReturn(searchResults);
        when(searchResultPriceMapper.map(eq(searchResults))).thenReturn(BigDecimal.TEN);
        when(abstractPricePolicy.calculatePricePerPassenger(eq(BigDecimal.TEN), eq(BigDecimal.valueOf(PASSENGERS)))).thenReturn(ITINERARY_PRICE);
    }

    private RetrieveFareFamiliesResponse getFareFamiliesResponse() {
        RetrieveFareFamiliesResponse retrieveFareFamiliesResponse = new RetrieveFareFamiliesResponse();
        retrieveFareFamiliesResponse.setFareFamilies(getFareFamilies());

        return retrieveFareFamiliesResponse;
    }

    private List<FareFamily> getFareFamilies() {
        FareFamilyPerk fareFamilyPerk = new FareFamilyPerk();
        fareFamilyPerk.setDescription("DESCRIPTION");
        fareFamilyPerk.setAvailability("AVAILABILITY");
        fareFamilyPerk.setFareUpgradePerkType(CABIN_BAG);
        fareFamilyPerk.setFareUpgradePerkTypeGroup("GROUP");
        fareFamilyPerk.setUnits(1);
        fareFamilyPerk.setWeight(1);

        FareFamily fareFamily = new FareFamily();
        fareFamily.setName("NAME");
        fareFamily.setCabinClass("CABIN_CLASS");
        fareFamily.setPrice(BigDecimal.ONE);
        fareFamily.setCurrency("CUR");
        fareFamily.setProviderItineraryIndex(0);
        fareFamily.setFareFamilyPerks(Collections.singletonList(fareFamilyPerk));

        return Collections.singletonList(fareFamily);
    }

    private void assertFareFamily(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamily fareFamilyDTO) {
        FareFamily fareFamily = getFareFamilies().get(0);
        assertEquals(fareFamilyDTO.getName(), fareFamily.getName());
        assertEquals(fareFamilyDTO.getCabinClass(), fareFamily.getCabinClass());
        assertEquals(fareFamilyDTO.getProviderItineraryIndex(), fareFamily.getProviderItineraryIndex());
        assertEquals(fareFamilyDTO.getPrice().getAmount(), fareFamily.getPrice());
        assertEquals(fareFamilyDTO.getPrice().getCurrency(), fareFamily.getCurrency());
        assertEquals(fareFamilyDTO.getTotalPrice().getAmount(), fareFamily.getPrice().add(ITINERARY_PRICE));
        assertEquals(fareFamilyDTO.getTotalPrice().getCurrency(), fareFamily.getCurrency());
        assertFareFamilyPerk(fareFamilyDTO.getFareFamilyPerks().stream().findFirst().get());
    }

    private void assertFareFamilyPerk(com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FareFamilyPerk fareFamilyPerkDTO) {
        FareFamilyPerk fareFamilyPerk = getFareFamilies().get(0).getFareFamilyPerks().get(0);
        assertEquals(fareFamilyPerkDTO.getDescription(), fareFamilyPerk.getDescription());
        assertEquals(fareFamilyPerkDTO.getAvailability(), fareFamilyPerk.getAvailability());
        assertEquals(fareFamilyPerkDTO.getFareUpgradePerkType(), fareFamilyPerk.getFareUpgradePerkType());
        assertEquals(fareFamilyPerkDTO.getFareUpgradePerkTypeGroup(), fareFamilyPerk.getFareUpgradePerkTypeGroup());
        assertEquals(fareFamilyPerkDTO.getUnits(), fareFamilyPerk.getUnits());
        assertEquals(fareFamilyPerkDTO.getWeight(), fareFamilyPerk.getWeight());
        assertEquals(fareFamilyPerkDTO.getVisibility(), ALWAYS);
    }

}
