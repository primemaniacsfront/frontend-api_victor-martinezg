package com.odigeo.frontend.resolvers.trip.bookingimportantmessage;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Cancellation;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CancellationMessage;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Message;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MessageFilter;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingImportantMessageFilterTest {

    private static final Long FILTER_VALUE = 1234L;

    @Test
    public void filterOKWithNoFilter() {
        Message message = getMessage(FILTER_VALUE);
        assertTrue(new BookingImportantMessageFilter().filter(message, null).isPresent());
    }

    @Test
    public void filterOKWithFilter() {
        MessageFilter messageFilter = new MessageFilter(FILTER_VALUE);
        Message message = getMessage(FILTER_VALUE);
        assertTrue(new BookingImportantMessageFilter().filter(message, messageFilter).isPresent());
    }

    @Test
    public void filterKOWithFilter() {
        MessageFilter messageFilter = new MessageFilter(FILTER_VALUE);
        Message message = getMessage(4321L);
        assertFalse(new BookingImportantMessageFilter().filter(message, messageFilter).isPresent());
    }

    private Message getMessage(Long providerBookingItemId) {
        CancellationMessage message = new CancellationMessage();
        Cancellation cancellation = new Cancellation();
        cancellation.setProviderBookingItemId(providerBookingItemId);
        message.setCancellations(Collections.singletonList(cancellation));
        return message;
    }

}
