package com.odigeo.frontend.resolvers.accommodation.summary;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSummaryRequest;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationReviewHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationDetailResponseHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationDetailRetrieveHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationSearchCriteriaRetrieveHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationSummaryShoppingCartHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.handlers.RoomDealSummaryHandler;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.RoomDealSummaryDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetailResponse;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.provider.AccommodationProviderSearchCriteria;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.odigeo.frontend.resolvers.accommodation.summary.AccommodationSummaryResolver.REVIEW_PROVIDER_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class AccommodationSummaryResolverTest {

    private static final String SEARCH_QUERY = "accommodationSummary";
    private static final long BOOKING_ID = 88L;

    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private AccommodationDetailRetrieveHandler accommodationDetailRetrieveHandler;
    @Mock
    private AccommodationDetailResponseHandler accommodationDetailResponseHandler;
    @Mock
    private AccommodationSearchCriteriaRetrieveHandler accommodationSearchCriteriaRetrieveHandler;
    @Mock
    private RoomDealSummaryHandler roomDealSummaryHandler;
    @Mock
    private AccommodationReviewHandler accommodationReviewHandler;
    @Mock
    private GeoLocationHandler geoLocationHandler;
    @Mock
    private AccommodationSummaryShoppingCartHandler accommodationSummaryShoppingCartHandler;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;

    @InjectMocks
    private AccommodationSummaryResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visit);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(SEARCH_QUERY));
    }

    @Test
    public void testRoomDetailsFetcher() {
        AccommodationSummaryRequest request = new AccommodationSummaryRequest();
        request.setDedupId(123);
        request.setAccommodationDealKey("456");
        AccommodationDetailResponse accommodationDetailResponse = mock(AccommodationDetailResponse.class);
        AccommodationSummaryResponseDTO expected = new AccommodationSummaryResponseDTO();
        prepareCoordinates(expected);
        List<RoomDealSummaryDTO> roomDeals = Arrays.asList(mock(RoomDealSummaryDTO.class));
        AccommodationReview accommodationReviewDTO = mock(AccommodationReview.class);

        when(jsonUtils.fromDataFetching(env, AccommodationSummaryRequest.class)).thenReturn(request);
        when(accommodationDetailRetrieveHandler.getAccommodationDetails(request, visit)).thenReturn(accommodationDetailResponse);
        when(accommodationDetailResponseHandler.getAccommodationDetails(accommodationDetailResponse)).thenReturn(expected);
        when(roomDealSummaryHandler.getRoomsDealsByRoomKey(request, expected.getAccommodationDetail(), visit)).thenReturn(roomDeals);
        when(accommodationReviewHandler.getSingleReviewByProvider(eq(123), eq(REVIEW_PROVIDER_ID))).thenReturn(accommodationReviewDTO);
        AccommodationSearchCriteria accommodationSearchCriteria = prepareSearchCriteria();
        when(geoLocationHandler.computeDistanceFromGeonode(any(), any(GeoCoordinatesDTO.class))).thenReturn(1.0);
        when(accommodationSearchCriteriaRetrieveHandler.getSearchCriteria(anyLong(), anyInt())).thenReturn(accommodationSearchCriteria);

        AccommodationSummaryResponseDTO result = resolver.accommodationSummaryFetcher(env);
        assertSame(result, expected);
        assertSame(result.getAccommodationDetail(), expected.getAccommodationDetail());
        assertSame(result.getRoomDeals(), roomDeals);
        assertSame(result.getAccommodationReview(), accommodationReviewDTO);
    }

    @Test
    public void testAccommodationSummaryFetcherByBookingId() {
        AccommodationSummaryRequest request = new AccommodationSummaryRequest();
        request.setBookingId(BOOKING_ID);
        when(jsonUtils.fromDataFetching(env, AccommodationSummaryRequest.class)).thenReturn(request);
        AccommodationSummaryResponseDTO expected = mock(AccommodationSummaryResponseDTO.class);
        when(accommodationSummaryShoppingCartHandler.accommodationSummaryFromShopping(eq(request), eq(env))).thenReturn(expected);

        AccommodationSummaryResponseDTO result = resolver.accommodationSummaryFetcher(env);
        assertEquals(result, expected);
        verify(accommodationSummaryShoppingCartHandler).accommodationSummaryFromShopping(eq(request), eq(env));
    }

    private AccommodationSearchCriteria prepareSearchCriteria() {
        AccommodationSearchCriteria accommodationSearchCriteria = new AccommodationSearchCriteria();
        AccommodationProviderSearchCriteria accommodationProviderSearchCriteria = new AccommodationProviderSearchCriteria();
        accommodationProviderSearchCriteria.setDestinationGeoNodeId(123);
        accommodationSearchCriteria.setProviderSearchCriteria(accommodationProviderSearchCriteria);
        return accommodationSearchCriteria;
    }

    private void prepareCoordinates(AccommodationSummaryResponseDTO expected) {
        AccommodationDetailDTO accommodationDetailDTO = new AccommodationDetailDTO();
        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();
        GeoCoordinatesDTO geoCoordinatesDTO = new GeoCoordinatesDTO();
        geoCoordinatesDTO.setLongitude(BigDecimal.TEN);
        geoCoordinatesDTO.setLatitude(BigDecimal.TEN);
        accommodationDealInformationDTO.setCoordinates(geoCoordinatesDTO);
        accommodationDetailDTO.setAccommodationDealInformation(accommodationDealInformationDTO);
        expected.setAccommodationDetail(accommodationDetailDTO);
    }

}
