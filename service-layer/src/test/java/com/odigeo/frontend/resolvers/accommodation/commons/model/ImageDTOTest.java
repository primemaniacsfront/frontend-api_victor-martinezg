package com.odigeo.frontend.resolvers.accommodation.commons.model;

import bean.test.BeanTest;

public class ImageDTOTest extends BeanTest<ImageDTO> {

    @Override
    protected ImageDTO getBean() {
        ImageDTO bean = new ImageDTO();
        bean.setThumbnailUrl("TURL");
        bean.setUrl("URL");
        return bean;
    }
}
