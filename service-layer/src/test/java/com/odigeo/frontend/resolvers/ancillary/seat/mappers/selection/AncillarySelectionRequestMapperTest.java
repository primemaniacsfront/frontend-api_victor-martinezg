package com.odigeo.frontend.resolvers.ancillary.seat.mappers.selection;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AncillariesSelectionRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PassengerSeatsSelectionRequest;
import com.odigeo.itineraryapi.v1.request.AncillarySelectionRequest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class AncillarySelectionRequestMapperTest {

    @Mock
    private SeatMapSelectionRequestMapper seatMapSelectionRequestMapper;

    @InjectMocks
    private AncillarySelectionRequestMapper ancillarySelectionRequestMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(seatMapSelectionRequestMapper.map(any())).thenReturn(new ArrayList<>());
    }

    @Test
    public void map() {
        AncillariesSelectionRequest inputAncillariesSelectionRequest = new AncillariesSelectionRequest();
        List<PassengerSeatsSelectionRequest> inputSeats = new ArrayList<>();
        PassengerSeatsSelectionRequest inputSeat = new PassengerSeatsSelectionRequest();
        inputSeat.setPassenger(1);
        inputSeat.setSelection(new ArrayList<>());
        inputSeats.add(inputSeat);
        inputAncillariesSelectionRequest.setSeats(inputSeats);

        List<AncillarySelectionRequest> outputAncillarySelectionRequestList = ancillarySelectionRequestMapper.map(inputAncillariesSelectionRequest);
        AncillarySelectionRequest outputAncillarySelectionRequest = outputAncillarySelectionRequestList.get(0);

        assertEquals(outputAncillarySelectionRequestList.size(), inputSeats.size());
        assertEquals(outputAncillarySelectionRequest.getTravellerNumber().intValue(), inputSeat.getPassenger());
    }

    @Test
    public void mapNullSeatsInput() {
        List<AncillarySelectionRequest> outputAncillarySelectionRequestList = ancillarySelectionRequestMapper.map(new AncillariesSelectionRequest());

        assertTrue(outputAncillarySelectionRequestList.isEmpty());
    }
}

