package com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans;

import org.testng.annotations.Test;

import java.util.HashMap;

import static org.testng.Assert.assertEquals;

public class AncillaryPolicyConfigurationTest {
    private static final String PAGE_NAME = "default";
    private static final AncillaryConfiguration ANCILLARY_CONFIGURATION = (new AncillaryConfigurationTest()).getBean();

    public AncillaryPolicyConfiguration getBean() {
        AncillaryPolicyConfiguration bean = new AncillaryPolicyConfiguration();
        bean.put(PAGE_NAME, ANCILLARY_CONFIGURATION);
        return bean;
    }

    @Test
    public void test() {
        HashMap<String, AncillaryConfiguration> bean = getBean();
        assertEquals(bean.get(PAGE_NAME), ANCILLARY_CONFIGURATION);
    }
}
