package com.odigeo.frontend.resolvers.deals.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ApparentPrice;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.odigeo.marketing.search.price.v1.requests.PriceType;
import com.odigeo.marketing.search.price.v1.util.Money;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.testng.Assert.assertEquals;

public class ApparentPricesMapperTest {

    private static final Money EUR_100 = new Money.Builder().amount(BigDecimal.valueOf(100.00)).currency("EUR").build();
    private static final Money EUR_80 = new Money.Builder().amount(BigDecimal.valueOf(80.00)).currency("EUR").build();

    private final Map<String, Money> apparentPrices = Stream.of(
        new SimpleEntry<>(PriceType.MINIMUM_PURCHASABLE_PRICE.name(), EUR_100),
        new SimpleEntry<>(PriceType.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE.name(), EUR_80)
    ).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));

    private MoneyMapper moneyMapper = new MoneyMapperImpl();

    private ApparentPricesMapper mapper = new ApparentPricesMapper();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        Injector injector = Guice.createInjector(binder -> {
            binder.bind(MoneyMapper.class).toInstance(moneyMapper);
        });
        injector.injectMembers(mapper);
    }

    @Test
    public void testMap() {
        List<ApparentPrice> result = mapper.map(apparentPrices);

        result
            .stream()
            .forEach(apparentPrice -> {
                assertEquals(apparentPrice.getPrice().getAmount(), apparentPrices.get(apparentPrice.getId()).getAmount());
                assertEquals(apparentPrice.getPrice().getCurrency(), apparentPrices.get(apparentPrice.getId()).getCurrency());
            });
    }

    @Test
    public void testMapEmpty() {
        assertEquals(mapper.map(Collections.emptyMap()), Collections.emptyList());
    }
}
