package com.odigeo.frontend.resolvers.search.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageCondition;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.google.inject.Guice;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.resolvers.search.configuration.BaggageConfiguration;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.baggage.BaggageConditions;
import com.odigeo.searchengine.v2.responses.baggage.BaggageDescriptor;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BaggageHandlerTest {

    private static final String RENFE_CODE = "RNF";
    private static final String AIRLINE_WITH_SMALL_BAG = "FR";

    @Mock
    private BaggageConfiguration baggageConfiguration;

    @Mock
    private FareItinerary itinerary;
    @Mock
    private LegDTO leg;
    @Mock
    private SegmentDTO segment;
    @Mock
    private SectionDTO section;
    @Mock
    private Carrier sectionCarrier;
    @Mock
    private Carrier segmentCarrier;
    @Mock
    private BaggageDescriptor baggageDescriptor;
    @Mock
    private SearchEngineContext seContext;

    private Map<Integer, SegmentDTO> segments;
    private List<LegDTO> legs;

    private BaggageHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new BaggageHandler();
        Guice.createInjector(handler -> {
            handler.bind(BaggageConfiguration.class).toProvider(() -> baggageConfiguration);
        }).injectMembers(handler);

        when(leg.getSegments()).thenReturn(Collections.singletonList(segment));
        when(segment.getSections()).thenReturn(Collections.singletonList(section));
        when(segment.getCarrier()).thenReturn(segmentCarrier);
        when(section.getCarrier()).thenReturn(sectionCarrier);

        Integer baggageFees = 1;
        BaggageConditions baggageConditions = mock(BaggageConditions.class);

        when(itinerary.getBaggageFees()).thenReturn(baggageFees);
        when(baggageConditions.getBaggageDescriptorIncludedInPrice()).thenReturn(baggageDescriptor);

        when(seContext.getBaggageMap()).thenReturn(Collections.singletonMap(baggageFees, baggageConditions));

        legs = Collections.singletonList(leg);
        segments = Collections.singletonMap(0, segment);

        when(baggageConfiguration.getRenfeCode()).thenReturn(RENFE_CODE);
        when(baggageConfiguration.getAirlinesWithSmallBagInCabin()).thenReturn(Collections.singletonList(AIRLINE_WITH_SMALL_BAG));
    }

    @Test
    public void testPopulateItineraryWithPieces() {
        when(baggageDescriptor.getPieces()).thenReturn(10);

        handler.populateItineraryBaggageConditions(seContext, itinerary, legs);
        verify(segment).setBaggageCondition(BaggageCondition.CHECKIN_INCLUDED);
    }

    @Test
    public void testPopulateItineraryWithKilos() {
        when(baggageDescriptor.getKilos()).thenReturn(10);

        handler.populateItineraryBaggageConditions(seContext, itinerary, legs);
        verify(segment).setBaggageCondition(BaggageCondition.CHECKIN_INCLUDED);
    }

    @Test
    public void testPopulateItineraryWithoutPiecesAndKilos() {
        handler.populateItineraryBaggageConditions(seContext, itinerary, legs);
        verify(segment, never()).setBaggageCondition(any());
    }

    @Test
    public void testPopulateItineraryNotInEstimationFeesMap() {
        when(seContext.getBaggageMap()).thenReturn(Collections.emptyMap());

        handler.populateItineraryBaggageConditions(seContext, itinerary, legs);
        verify(segment, never()).setBaggageCondition(any());
    }

    @Test
    public void testPopulateSegmentWithAllowanceQuantity() {
        when(section.getBaggageAllowance()).thenReturn(1);

        handler.populateSegmentBaggageConditions(segments);
        verify(segment).setBaggageCondition(BaggageCondition.CHECKIN_INCLUDED);
    }

    @Test
    public void testPopulateSegmentWithCarrier() {
        when(sectionCarrier.getId()).thenReturn(RENFE_CODE);

        handler.populateSegmentBaggageConditions(segments);
        verify(segment).setBaggageCondition(BaggageCondition.CHECKIN_INCLUDED);
    }

    @Test
    public void testPopulateSegmentWithSpecialCarrier() {
        when(segmentCarrier.getId()).thenReturn(AIRLINE_WITH_SMALL_BAG);

        handler.populateSegmentBaggageConditions(segments);
        verify(segment).setBaggageCondition(BaggageCondition.BASED_ON_AIRLINE);
    }

    @Test
    public void testPopulateSegmentWithCabinIncluded() {
        when(segmentCarrier.getId()).thenReturn("code");

        handler.populateSegmentBaggageConditions(segments);
        verify(segment).setBaggageCondition(BaggageCondition.CABIN_INCLUDED);
    }

    @Test
    public void testPopulateSegmentSectionWithBaggageCondition() {
        when(segment.getBaggageCondition()).thenReturn(mock(BaggageCondition.class));

        handler.populateSegmentBaggageConditions(segments);
        verify(segment, never()).setBaggageCondition(any());
    }

}
