package com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationShoppingItem;
import com.odigeo.dapi.client.AccommodationCategory;
import com.odigeo.dapi.client.AccommodationDealInformation;
import com.odigeo.dapi.client.AccommodationType;
import com.odigeo.dapi.client.GeoCoordinates;
import com.odigeo.dapi.client.RoomDeal;
import com.odigeo.dapi.client.RoomGroupDeal;
import com.odigeo.dapi.client.ThreeValuedLogic;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class AccommodationShoppingItemMapperTest {

    private static final Long ACCOMMODATION_ID = 8L;

    @Mock
    private com.odigeo.dapi.client.AccommodationShoppingItem shoppingItem;
    @Mock
    private AccommodationDealInformation accommodationDealInformation;
    @Mock
    private RoomGroupDeal roomGroupDeal;
    @Mock
    private RoomDeal roomDeal;
    @Mock
    private GeoCoordinates geoCoordinates;

    private AccommodationShoppingItemMapper mapper;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mapper = new AccommodationShoppingItemMapperImpl();
        when(shoppingItem.getId()).thenReturn(ACCOMMODATION_ID);
        when(shoppingItem.getPrice()).thenReturn(BigDecimal.TEN);
    }

    @Test
    public void testMapNull() {
        assertNull(mapper.map(null));
    }

    @Test
    public void testMap() {
        AccommodationShoppingItem accommodationShoppingItem = mapper.map(shoppingItem);
        assertTrue(accommodationShoppingItem.getId() == ACCOMMODATION_ID);
        assertEquals(accommodationShoppingItem.getPrice(), BigDecimal.TEN);
    }

    @Test
    public void testMapInformation() {
        when(shoppingItem.getAccommodationDealInformation()).thenReturn(accommodationDealInformation);
        when(accommodationDealInformation.getName()).thenReturn("name");
        when(accommodationDealInformation.getAddress()).thenReturn("address");
        when(accommodationDealInformation.getCityName()).thenReturn("cityName");
        when(accommodationDealInformation.getDescription()).thenReturn("description");
        when(accommodationDealInformation.getLocation()).thenReturn("location");
        when(accommodationDealInformation.getChain()).thenReturn("chain");
        when(accommodationDealInformation.getType()).thenReturn(AccommodationType.APARTAMENT);
        when(accommodationDealInformation.getCancellationPolicyDescription()).thenReturn("getCancellationPolicyDescription");
        when(accommodationDealInformation.getCoordinates()).thenReturn(geoCoordinates);
        when(geoCoordinates.getLatitude()).thenReturn(BigDecimal.ONE);
        when(geoCoordinates.getLongitude()).thenReturn(BigDecimal.TEN);
        when(accommodationDealInformation.getCategory()).thenReturn(AccommodationCategory.STARS_1);
        when(accommodationDealInformation.getCancellationFree()).thenReturn(ThreeValuedLogic.FALSE);
        when(accommodationDealInformation.isPaymentAtDestination()).thenReturn(true);

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationDealInformation dealInformation = mapper.map(shoppingItem).getAccommodationDealInformation();
        assertEquals(dealInformation.getName(), accommodationDealInformation.getName());
        assertEquals(dealInformation.getAddress(), accommodationDealInformation.getAddress());
        assertEquals(dealInformation.getCityName(), accommodationDealInformation.getCityName());
        assertEquals(dealInformation.getDescription(), accommodationDealInformation.getDescription());
        assertEquals(dealInformation.getLocation(), accommodationDealInformation.getLocation());
        assertEquals(dealInformation.getChain(), accommodationDealInformation.getChain());
        assertEquals(dealInformation.getType(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType.APARTAMENT);
        assertEquals(dealInformation.getCancellationPolicyDescription(), accommodationDealInformation.getCancellationPolicyDescription());
        assertEquals(dealInformation.getCoordinates().getLatitude(), accommodationDealInformation.getCoordinates().getLatitude());
        assertEquals(dealInformation.getCoordinates().getLongitude(), accommodationDealInformation.getCoordinates().getLongitude());
        assertEquals(dealInformation.getCategory(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory.STARS_1);
        assertEquals(dealInformation.getCancellationFree().toString(), accommodationDealInformation.getCancellationFree().toString());
        assertTrue(dealInformation.getPaymentAtDestination());
    }

    @Test
    public void testMapRoomGroupDeal() {
        when(shoppingItem.getRoomGroupDeal()).thenReturn(roomGroupDeal);
        when(roomGroupDeal.getKey()).thenReturn("key");
        when(roomGroupDeal.getRoomsKeys()).thenReturn(Collections.singletonList("roomsKeys"));
        when(roomGroupDeal.getSearchId()).thenReturn(88L);

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RoomGroupDeal result = mapper.map(shoppingItem).getRoomGroupDeal();
        assertEquals(result.getKey(), roomGroupDeal.getKey());
        assertEquals(result.getRoomsKeys(), roomGroupDeal.getRoomsKeys());
        assertEquals(result.getSearchId(), roomGroupDeal.getSearchId());
    }

    @Test
    public void testMapRoomDealLegend() {
        when(shoppingItem.getRoomDealLegend()).thenReturn(Collections.singletonList(roomDeal));
        when(roomDeal.getKey()).thenReturn("key");
        when(roomDeal.getProviderId()).thenReturn("providerID");

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RoomDeal result = mapper.map(shoppingItem).getRoomDealLegend().get(0);
        assertEquals(result.getKey(), roomDeal.getKey());
        assertEquals(result.getProviderId(), roomDeal.getProviderId());
    }
}
