package com.odigeo.frontend.resolvers.accommodation.commons.exceptions;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CityNotFoundExceptionTest {

    @Test
    public void testException() {
        Exception throwable = new Exception();
        CityNotFoundException cityNotFoundException = new CityNotFoundException(throwable);
        assertEquals(cityNotFoundException.getCause(), throwable);
    }
}
