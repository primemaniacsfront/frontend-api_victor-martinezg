package com.odigeo.frontend.resolvers.search.handlers.campaign;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayConfig;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PrimeDayFareType;
import com.google.common.collect.Sets;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.search.configuration.PrimeDayMarketsConfiguration;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class PrimeDayHandlerTest {

    @Mock
    private VisitInformation visit;

    @Mock
    private PrimeDayMarketsConfiguration marketsConfiguretion;

    private PrimeDayHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        configureParameters();
    }

    private void configureParameters() {
        doReturn(initMarketsCarriersTest()).when(marketsConfiguretion).getMarketsCarriers();

        handler = new PrimeDayHandler(marketsConfiguretion);
    }

    @Test
    public void testPopulateIsPrimeDayFare() {
        check(Site.ES, true, "AM");
    }

    @Test
    public void testPopulateIsPrimeDayFareNullSite() {
        check(null, false, "AM");
    }

    @Test
    public void testPopulateIsPrimeDayFareInvalidSite() {
        check(Site.UNKNOWN, false, "AM");
    }

    @Test
    public void testPopulateIsPrimeDayFareDoesNotContainAll() {
        check(Site.ES, false, "AM", "U2");
    }

    @Test
    public void testPopulatePrimeDayGoldFareType() {
        check(Site.ES, PrimeDayFareType.GOLD, "AM");
    }

    @Test
    public void testPopulatePrimeDaySilverFareType() {
        check(Site.ES, PrimeDayFareType.SILVER, "EY");
    }

    @Test
    public void testPopulatePrimeDayBronzeFareType() {
        check(Site.ES, PrimeDayFareType.BRONZE, "LX");
    }

    @Test
    public void testPopulatePrimeDayMultiplesGoldFareType() {
        check(Site.ES, PrimeDayFareType.GOLD, "AM", "SN", "LX");
    }

    @Test
    public void testPopulatePrimeDayMultiplesSilverFareType() {
        check(Site.ES, PrimeDayFareType.SILVER, "SN", "LX");
    }

    @Test
    public void testPopulatePrimeDayMultiplesBronzeFareType() {
        check(Site.ES, PrimeDayFareType.BRONZE, "FI", "LX");
    }

    @Test
    public void testPopulatePrimeDayMultiplesNullFareType() {
        check(Site.ES, false, "FI", "QF");
    }

    private void initializeSite(Site site) {
        when(visit.getSite()).thenReturn(site);
    }

    private Set<String> buildCarriers(String... carrierIds) {
        return Sets.newHashSet(carrierIds);
    }

    private void check(Site site, boolean isPrimeDayFare, String... carrierIds) {
        PrimeDayConfig primeDayConfig = launchTest(site, carrierIds);
        assertEquals(primeDayConfig.getIsPrimeDayFare(), isPrimeDayFare);
        if (!isPrimeDayFare) {
            assertEquals(primeDayConfig.getPrimeDayFareType(), null);
        }
    }

    private void check(Site site, PrimeDayFareType primeDayFareType, String... carrierIds) {
        PrimeDayConfig primeDayConfig = launchTest(site, carrierIds);
        assertEquals(primeDayConfig.getIsPrimeDayFare(), true);
        assertEquals(primeDayConfig.getPrimeDayFareType(), primeDayFareType);
    }

    private PrimeDayConfig launchTest(Site site, String... carrierIds) {
        initializeSite(site);

        return handler.populatePrimeDayConfig(buildCarriers(carrierIds), visit);
    }

    private Map<Site, Map<String, PrimeDayFareType>> initMarketsCarriersTest() {
        Map<String, PrimeDayFareType> spainSite = new HashMap<>();
        spainSite.put("AM", PrimeDayFareType.GOLD);
        spainSite.put("EY", PrimeDayFareType.SILVER);
        spainSite.put("LX", PrimeDayFareType.BRONZE);
        spainSite.put("FI", PrimeDayFareType.BRONZE);
        spainSite.put("SN", PrimeDayFareType.SILVER);

        Map<Site, Map<String, PrimeDayFareType>> marketsCarriers = new HashMap<>();
        marketsCarriers.put(Site.ES, spainSite);
        return marketsCarriers;
    }
}
