package com.odigeo.frontend.resolvers.variations;

import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.services.TestTokenService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import java.util.List;
import java.util.Map;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class VariationsResolverTest {

    private static final String VARIATIONS_QUERY = "variations";

    @Mock
    private TestTokenService testTokenService;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private VisitInformation visit;

    @InjectMocks
    private VariationsResolver resolver;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        ResolverContext context = mock(ResolverContext.class);
        when(context.getVisitInformation()).thenReturn(visit);
        when(env.getContext()).thenReturn(context);
    }

    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get("Query");

        assertNotNull(queries.get(VARIATIONS_QUERY));
    }

    @Test
    public void testVariationsDataFetcher() {
        List<String> aliases = mock(List.class);
        List<Integer> variations = mock(List.class);

        when(jsonUtils.fromDataFetching(env, "aliases")).thenReturn(aliases);
        when(testTokenService.findMultiple(aliases, visit)).thenReturn(variations);

        assertSame(resolver.variationsDataFetcher(env), variations);
    }

}
