package com.odigeo.frontend.resolvers.insurance.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceOfferId;
import com.odigeo.insurance.api.last.request.OfferId;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class InsuranceOfferIdMapperTest {

    private static final String ID = "ID";
    private static final String VERSION = "VERSION";

    @Test
    public void map() {
        InsuranceOfferIdMapper insuranceOfferIdMapper = new InsuranceOfferIdMapper();

        InsuranceOfferId insuranceOfferId = insuranceOfferIdMapper.map(getOfferId());

        assertEquals(insuranceOfferId.getId(), ID);
        assertEquals(insuranceOfferId.getVersion(), VERSION);
    }

    private OfferId getOfferId() {
        OfferId offerId = new OfferId();
        offerId.setId(ID);
        offerId.setVersion(VERSION);
        return offerId;
    }
}
