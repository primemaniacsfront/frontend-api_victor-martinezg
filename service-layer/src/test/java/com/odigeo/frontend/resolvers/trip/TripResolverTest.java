package com.odigeo.frontend.resolvers.trip;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Trip;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TripResponse;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.trip.bookingimportantmessage.BookingImportantMessageHandler;
import com.odigeo.frontend.resolvers.trip.handlers.TripHandler;
import graphql.execution.DataFetcherResult;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.openMocks;
import static voldemort.utils.Utils.assertNotNull;

public class TripResolverTest {
    private static final String BOOKING_ID_FIELD = "bookingId";
    private static final String EMAIL_FIELD = "email";
    private static final long BOOKING_ID = 1234567890L;
    private static final String EMAIL = "test@edreamsodigeo.com";
    @Mock
    private BookingImportantMessageHandler bookingImportantMessageHandler;
    @Mock
    private TripHandler tripHandler;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visitInformation;
    private TripResolver tripResolver;

    @BeforeMethod
    private void setUpMethod() {
        openMocks(this);
        tripResolver = new TripResolver(bookingImportantMessageHandler, tripHandler, new JsonUtils());
    }

    @Test
    public void addToBuildersOK() {
        RuntimeWiring.Builder builder = spy(RuntimeWiring.newRuntimeWiring());
        tripResolver.addToBuilder(builder);
        verify(builder, times(3)).type(any(), any());
    }

    @Test
    public void getTripResponseOk() {
        when(env.getArguments()).thenReturn(Collections.emptyMap());
        DataFetcherResult<TripResponse> response = tripResolver.getTripResponse(env);
        assertNotNull(response.getData());
    }

    @Test
    public void getTripOK() {
        Map<String, Object> map = new HashMap<>();
        map.put(BOOKING_ID_FIELD, BOOKING_ID);
        map.put(EMAIL_FIELD, EMAIL);
        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visitInformation);
        when(env.getLocalContext()).thenReturn(map);
        when(tripHandler.getTrip(any(), any(), any())).thenReturn(mock(Trip.class));
        DataFetcherResult<Trip> response = tripResolver.getTrip(env);
        assertNotNull(response.getData());
    }

    @Test
    public void getCancellationMessagesOK() {
        Map<String, Object> map = new HashMap<String, Object>() {
            {
                put("email", "test@edreamsodigeo.com");
                put("bookingId", 1234L);
            }
        };
        when(env.getLocalContext()).thenReturn(map);
        assertNotNull(tripResolver.getCancellationMessages(env));
        verify(bookingImportantMessageHandler).getAllMessagesForBookingIdAndEmail(anyLong(), any(), any());
    }

    @Test
    public void getCancellationMessagesByTokenOK() {
        when(env.getLocalContext()).thenReturn(Collections.singletonMap("token", "test_token"));
        assertNotNull(tripResolver.getCancellationMessages(env));
        verify(bookingImportantMessageHandler).getAllMessagesForToken(any(), any());
    }

}
