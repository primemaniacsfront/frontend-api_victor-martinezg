package com.odigeo.frontend.resolvers.user.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipTokenResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingInfo;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateUserMembershipAction;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UpdateUserMembershipRequest;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.prime.operations.MembershipShoppingCartOperations;
import com.odigeo.frontend.resolvers.prime.operations.MembershipShoppingOperationsFactory;
import com.odigeo.frontend.resolvers.user.mappers.MembershipMapper;
import com.odigeo.frontend.resolvers.user.mappers.MembershipMapperImpl;
import com.odigeo.frontend.services.membership.MembershipServiceHandler;
import com.odigeo.frontend.services.UserDescriptionService;


import com.odigeo.frontend.services.membership.MembershipSearchHandler;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.userprofiles.api.v2.model.User;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipHandlerTest {

    private static final String ACTIVATION_DATE = "01-01-2021";
    private static final String ENABLED_AUTO_RENEWAL_STATUS = "ENABLED";
    private static final String EXPIRATION_DATE = "01-01-2022";
    private static final long MEMBERSHIP_ID = 123L;
    private static final String BASIC_MEMBERSHIP_TYPE = "BASIC";
    private static final int TWELVE_MONTHS_DURATION = 12;
    private static final String ONLINE_SOURCE_TYPE = "ONLINE";
    private static final String ACTIVE_STATUS = "ACTIVE";
    private static final long USER_ID = 1L;
    private static final String VALID_TOKEN_REQUEST = "abcde";
    private static final String VALID_TOKEN_RESPONSE = "name=Rodrigo&email=rodrigo@diazdevivar.com";
    private static final String VALID_TOKEN_RESPONSE_ONE_PARAM = "name=Rodrigo";
    private static final String TOKEN_NAME = "Rodrigo";
    private static final String TOKEN_EMAIL = "rodrigo@diazdevivar.com";
    private static final String EMPTY_VALUE = "";
    private static final String SHOPPING_ID = "1234";
    private static final String EMAIL = "buyer@edreams.com";

    @InjectMocks
    private MembershipHandler handler;
    @Mock
    private UserDescriptionService userService;
    @Mock
    private ResolverContext context;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private Membership membership;
    @Mock
    private MembershipResponse membershipResponse;
    @Mock
    private MembershipMapper mapper;
    @Mock
    private MembershipServiceHandler membershipServiceHandler;
    @Mock
    private VisitInformation visit;
    @Mock
    private UpdateUserMembershipRequest updateUserMembershipRequest;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private MembershipShoppingOperationsFactory membershipShoppingOperationsFactory;
    @Mock
    private MembershipShoppingCartOperations membershipShoppingCartOperations;
    @Mock
    private ShoppingInfo shoppingInfo;
    @Mock
    private MembershipSearchHandler membershipSearchHandler;
    @Mock
    private FreeTrialCandidateRequest freeTrialCandidateRequest;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new MembershipMapperImpl();
        Guice.createInjector(binder -> {
            binder.bind(MembershipMapper.class).toProvider(() -> mapper);
            binder.bind(UserDescriptionService.class).toProvider(() -> userService);
            binder.bind(MembershipServiceHandler.class).toProvider(() -> membershipServiceHandler);
            binder.bind(MembershipSearchHandler.class).toProvider(() -> membershipSearchHandler);
            binder.bind(JsonUtils.class).toProvider(() -> jsonUtils);
            binder.bind(MembershipShoppingOperationsFactory.class).toProvider(() -> membershipShoppingOperationsFactory);
            binder.bind(MembershipShoppingCartOperations.class).toProvider(() -> membershipShoppingCartOperations);
        }).injectMembers(handler);
        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visit);
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
        when(graphQLContext.get(VisitInformation.class)).thenReturn(visit);
        when(jsonUtils.fromDataFetching(env, UpdateUserMembershipRequest.class)).thenReturn(updateUserMembershipRequest);
        when(membershipShoppingOperationsFactory.getMembershipShoppingOperations(ShoppingType.CART)).thenReturn(membershipShoppingCartOperations);
    }

    private void setUserToken() {
        User user = new User();
        user.setId(USER_ID);
        User userContract = new User();
        userContract.setId(USER_ID);
        when(context.getVisitInformation()).thenReturn(visit);
        when(userService.getUserByToken(context)).thenReturn(user);
        when(userService.getUserByToken(context)).thenReturn(userContract);
    }

    @Test
    public void testGetUserInfo() {
        setUserToken();
        when(membershipResponse.getActivationDate()).thenReturn(ACTIVATION_DATE);
        when(membershipResponse.getAutoRenewal()).thenReturn(ENABLED_AUTO_RENEWAL_STATUS);
        when(membershipResponse.getExpirationDate()).thenReturn(EXPIRATION_DATE);
        when(membershipResponse.getMemberAccountId()).thenReturn(MEMBERSHIP_ID);
        when(membershipResponse.getId()).thenReturn(MEMBERSHIP_ID);
        when(membershipResponse.getMembershipType()).thenReturn(BASIC_MEMBERSHIP_TYPE);
        when(membershipResponse.getMonthsDuration()).thenReturn(TWELVE_MONTHS_DURATION);
        when(membershipResponse.getSourceType()).thenReturn(ONLINE_SOURCE_TYPE);
        when(membershipResponse.getStatus()).thenReturn(ACTIVE_STATUS);
        when(membershipSearchHandler.getCurrentMembership(eq(USER_ID), anyString())).thenReturn(membershipResponse);
        Membership membershipDescription = handler.getMembershipFromLoggedUser(env);
        assertEquals(membershipDescription.getActivationDate(), ACTIVATION_DATE);
        assertEquals(membershipDescription.getAutoRenewalStatus(), ENABLED_AUTO_RENEWAL_STATUS);
        assertEquals(membershipDescription.getExpirationDate(), EXPIRATION_DATE);
        assertEquals(membershipDescription.getMemberAccountId(), MEMBERSHIP_ID);
        assertEquals(membershipDescription.getId(), MEMBERSHIP_ID);
        assertEquals(membershipDescription.getType(), BASIC_MEMBERSHIP_TYPE);
        assertEquals(membershipDescription.getMonthsDuration(), TWELVE_MONTHS_DURATION);
        assertEquals(membershipDescription.getSourceType(), ONLINE_SOURCE_TYPE);
        assertEquals(membershipDescription.getStatus(), ACTIVE_STATUS);
    }

    @Test
    public void testGetUserInfoInvalidUser() {
        User userContract = new User();
        userContract.setId(USER_ID);
        when(userService.getUserByToken(context)).thenReturn(userContract);
        assertNull(handler.getMembershipFromLoggedUser(env));
    }

    private void setPrimeUserLogged() {
        when(userService.isUserLogged(context)).thenReturn(Boolean.TRUE);
        setUserToken();
        when(membershipSearchHandler.getCurrentMembership(anyLong(), anyString())).thenReturn(membershipResponse);
        when(membershipResponse.getId()).thenReturn(MEMBERSHIP_ID);
        when(membership.getId()).thenReturn(MEMBERSHIP_ID);
    }

    private void setPrimeUserNotLogged() {
        when(userService.isUserLogged(context)).thenReturn(Boolean.FALSE);
        when(membershipSearchHandler.getCurrentMembership(anyString(), any())).thenReturn(membershipResponse);
        when(membershipResponse.getId()).thenReturn(MEMBERSHIP_ID);
        when(membership.getId()).thenReturn(MEMBERSHIP_ID);
        when(membershipShoppingCartOperations.getBuyerEmail(Long.parseLong(SHOPPING_ID), context)).thenReturn(EMAIL);
    }

    @Test
    public void testUpdateMembershipDataFetcherToActivateRenewalStatus() {
        setPrimeUserLogged();
        when(updateUserMembershipRequest.getAction()).thenReturn(UpdateUserMembershipAction.ACTIVATE_RENEWAL_STATUS);
        handler.updateMembershipData(env);
        verify(membershipServiceHandler).enableAutoRenewal(membership.getId());
    }

    @Test
    public void testUpdateMembershipDataFetcherToActivateRenewalStatusNonLogged() {
        setPrimeUserNotLogged();
        when(updateUserMembershipRequest.getAction()).thenReturn(UpdateUserMembershipAction.ACTIVATE_RENEWAL_STATUS);
        when(updateUserMembershipRequest.getShoppingInfo()).thenReturn(shoppingInfo);
        when(shoppingInfo.getShoppingId()).thenReturn(SHOPPING_ID);
        when(shoppingInfo.getShoppingType()).thenReturn(ShoppingType.CART);
        when(membershipShoppingCartOperations.getBuyerEmail(Long.parseLong(SHOPPING_ID), context)).thenReturn(EMAIL);
        when(membershipShoppingCartOperations.isPrimeUserDiscountApplied(Long.parseLong(SHOPPING_ID), context)).thenReturn(Boolean.TRUE);
        when(userService.isUserLogged(context)).thenReturn(Boolean.FALSE);
        handler.updateMembershipData(env);
        verify(membershipServiceHandler).enableAutoRenewal(membership.getId());
    }

    @Test
    public void testUpdateMembershipDataFetcherToDeactivateRenewalStatusNonLogged() {
        setPrimeUserNotLogged();
        when(updateUserMembershipRequest.getAction()).thenReturn(UpdateUserMembershipAction.DEACTIVATE_RENEWAL_STATUS);
        handler.updateMembershipData(env);
        verify(membershipServiceHandler, never()).disableAutoRenewal(membership.getId());
    }

    @Test
    public void testUpdateMembershipDataFetcherToDisableRenewalStatus() {
        setPrimeUserLogged();
        when(updateUserMembershipRequest.getAction()).thenReturn(UpdateUserMembershipAction.DEACTIVATE_RENEWAL_STATUS);
        handler.updateMembershipData(env);
        verify(membershipServiceHandler).disableAutoRenewal(membership.getId());
    }

    @Test
    public void testUpdateMembershipDataFetcherToSetRemindMeLater() {
        setPrimeUserLogged();
        when(updateUserMembershipRequest.getAction()).thenReturn(UpdateUserMembershipAction.SET_REMIND_ME_LATER);
        handler.updateMembershipData(env);
        verify(membershipServiceHandler).setRemindMeLater(membership.getId());
    }


    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = "Invalid membership operation UNKNOWN")
    public void testUpdateMembershipDataFetcherWhenUnknownAction() {
        setPrimeUserLogged();
        when(updateUserMembershipRequest.getAction()).thenReturn(null);
        handler.updateMembershipData(env);
    }

    @Test
    public void testGetTokenInfo() {
        MembershipTokenResponse tokenResponse = setGetTokenEnvironment(VALID_TOKEN_REQUEST, VALID_TOKEN_RESPONSE);
        assertEquals(tokenResponse.getName(), TOKEN_NAME);
        assertEquals(tokenResponse.getEmail(), TOKEN_EMAIL);
    }

    @Test
    public void testGetTokenInfoWithOneParameter() {
        MembershipTokenResponse tokenResponse = setGetTokenEnvironment(VALID_TOKEN_REQUEST, VALID_TOKEN_RESPONSE_ONE_PARAM);
        assertEquals(tokenResponse.getName(), TOKEN_NAME);
        assertNull(tokenResponse.getEmail());
    }

    @Test
    public void testGetTokenInfoInvalid() {
        MembershipTokenResponse tokenResponse = setGetTokenEnvironment(EMPTY_VALUE, EMPTY_VALUE);
        assertNull(tokenResponse.getName());
        assertNull(tokenResponse.getEmail());
    }

    private MembershipTokenResponse setGetTokenEnvironment(String tokenRequest, String tokenResponse) {
        when(membershipServiceHandler.decryptToken(anyString())).thenReturn(tokenResponse);
        return handler.getTokenInfo(tokenRequest);
    }

    @Test
    public void testIsEligibleForFreeTrial() {
        when(shoppingInfo.getShoppingType()).thenReturn(ShoppingType.CART);
        when(shoppingInfo.getShoppingId()).thenReturn(SHOPPING_ID);
        when(membershipShoppingCartOperations.buildFreeTrialCandidateRequest(Long.parseLong(SHOPPING_ID), context)).thenReturn(freeTrialCandidateRequest);
        when(membershipServiceHandler.isEligibleForFreeTrial(freeTrialCandidateRequest)).thenReturn(Boolean.TRUE);
        assertTrue(handler.isEligibleForFreeTrial(shoppingInfo, context));
    }
}
