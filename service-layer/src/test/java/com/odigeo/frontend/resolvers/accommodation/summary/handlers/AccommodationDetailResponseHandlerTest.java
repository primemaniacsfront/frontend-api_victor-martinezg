package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationDetailMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetail;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetailResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class AccommodationDetailResponseHandlerTest {

    @Mock
    private AccommodationDetailMapper accommodationDetailMapper;
    @Mock
    private AccommodationImageHandler accommodationImageHandler;
    @InjectMocks
    private AccommodationDetailResponseHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAccommodationDetails() {
        AccommodationDetailResponse accommodationDetailResponse = new AccommodationDetailResponse();
        AccommodationDetail accommodationDetail = new AccommodationDetail();
        accommodationDetailResponse.setAccommodationDetail(accommodationDetail);
        AccommodationDetailDTO accommodationDetailDTO = new AccommodationDetailDTO();
        when(accommodationDetailMapper.map(accommodationDetail)).thenReturn(accommodationDetailDTO);

        AccommodationSummaryResponseDTO response = handler.getAccommodationDetails(accommodationDetailResponse);

        verify(accommodationImageHandler).removeDuplicatedImages(accommodationDetailDTO);
        assertEquals(response.getAccommodationDetail(), accommodationDetailDTO);
    }

    @Test
    public void testGetAccommodationDetailsNull() {
        AccommodationDetailResponse accommodationDetailResponse = new AccommodationDetailResponse();
        AccommodationDetail accommodationDetail = new AccommodationDetail();
        accommodationDetailResponse.setAccommodationDetail(accommodationDetail);

        AccommodationSummaryResponseDTO response = handler.getAccommodationDetails(accommodationDetailResponse);

        assertNull(response.getAccommodationDetail());
    }

}
