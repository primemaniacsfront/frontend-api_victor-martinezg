package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.criteria.SearchCriteria;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.criteria.TripType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SearchRequest;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Brand;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CarbonFootprintSearchCriteriaHandlerTest {

    private static final String GLOBAL_WORD = "GLOBAL";

    private static final Brand BRAND = Brand.ED;
    private static final Site SITE = Site.ES;

    @Mock
    private CarbonFootprintTripTypeMapper mirTripTypeMapper;
    @Mock
    private VisitInformation visit;
    @Mock
    private SearchRequest searchRequestDTO;
    @Mock
    private SearchResponseDTO searchResponseDTO;
    @Mock
    private ItineraryRequest itineraryRequestDTO;
    @Mock
    private SectionDTO sectionDTO;

    private CarbonFootprintSearchCriteriaHandler builder;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        builder = new CarbonFootprintSearchCriteriaHandler();

        Guice.createInjector(binder -> {
            binder.bind(CarbonFootprintTripTypeMapper.class).toProvider(() -> mirTripTypeMapper);
        }).injectMembers(builder);

        when(visit.getSite()).thenReturn(SITE);
        when(visit.getBrand()).thenReturn(BRAND);

        SearchItineraryDTO itineraryDTO = mock(SearchItineraryDTO.class);
        LegDTO legDTO = mock(LegDTO.class);
        SegmentDTO segmentDTO = mock(SegmentDTO.class);

        when(searchRequestDTO.getItinerary()).thenReturn(itineraryRequestDTO);
        when(searchResponseDTO.getItineraries()).thenReturn(Collections.singletonList(itineraryDTO));
        when(itineraryDTO.getLegs()).thenReturn(Collections.singletonList(legDTO));
        when(legDTO.getSegments()).thenReturn(Collections.singletonList(segmentDTO));
        when(segmentDTO.getSections()).thenReturn(Collections.singletonList(sectionDTO));
    }

    @Test
    public void testBuildRatingSearchCriteria() {
        Long visitId = 1L;
        Long searchId = 2L;
        Integer numAdults = 1;
        Integer numChildren = 2;
        String defaultCountry = "country";

        TripType tripType = mock(TripType.class);
        ZonedDateTime departureDate = mock(ZonedDateTime.class);

        when(visit.getVisitId()).thenReturn(visitId);
        when(visit.getWebsiteDefaultCountry()).thenReturn(defaultCountry);
        when(searchResponseDTO.getSearchId()).thenReturn(searchId);
        when(itineraryRequestDTO.getNumAdults()).thenReturn(numAdults);
        when(itineraryRequestDTO.getNumChildren()).thenReturn(numChildren);
        when(sectionDTO.getDepartureDate()).thenReturn(departureDate);
        when(mirTripTypeMapper.map(searchRequestDTO)).thenReturn(tripType);

        SearchCriteria searchCriteria = builder.buildSearchCriteria(searchRequestDTO, searchResponseDTO, visit);

        assertEquals(searchCriteria.getSearchId(), searchId);
        assertEquals(searchCriteria.getNumberOfAdults(), numAdults);
        assertEquals(searchCriteria.getNumberOfChildren(), numChildren);
        assertEquals(searchCriteria.getTripType(), tripType);
        assertEquals(searchCriteria.getBrand(), BRAND.name());
        assertEquals(searchCriteria.getMarket(), defaultCountry);
        assertEquals(searchCriteria.getSearchDateUTC().toLocalDate(), LocalDate.now());
    }

    @Test
    public void testBuildRatingSearchCriteriaWithGlobalMarket() {
        when(visit.getSite()).thenReturn(Site.GB);

        SearchCriteria searchCriteria = builder.buildSearchCriteria(searchRequestDTO, searchResponseDTO, visit);
        assertEquals(searchCriteria.getMarket(), GLOBAL_WORD);
    }

}
