package com.odigeo.frontend.resolvers.search.handlers.campaign;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AirlineCampaignConfig;
import com.google.common.collect.Sets;
import com.odigeo.frontend.resolvers.search.configuration.AirlineCampaignConfiguration;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.doReturn;
import static org.testng.Assert.assertEquals;

public class AirlineCampaignHandlerTest {

    @Mock
    private AirlineCampaignConfiguration airlinesConfiguration;

    private AirlineCampaignHandler handler;

    private static final String QATAR_AIRLINE = "QR";
    private static final String VUELING_AIRLINE = "VY";

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testHasAirlineCampaignTrue() {
        initializeTestConfiguration(initAirlinesTest(QATAR_AIRLINE));
        AirlineCampaignConfig airlineCampaignConfig = handler.populateCampaignConfig(getItineraryCarriers(QATAR_AIRLINE));
        assertEquals(airlineCampaignConfig.getHasAirlineCampaign(), true);
    }

    @Test
    public void testHasAirlineCampaignFalse() {
        initializeTestConfiguration(initAirlinesTest(QATAR_AIRLINE));
        AirlineCampaignConfig airlineCampaignConfig = handler.populateCampaignConfig(getItineraryCarriers(VUELING_AIRLINE));
        assertEquals(airlineCampaignConfig.getHasAirlineCampaign(), false);
    }

    @Test
    public void testTwoCarriersHasAirlineCampaignTrue() {
        initializeTestConfiguration(initAirlinesTest(VUELING_AIRLINE));
        AirlineCampaignConfig airlineCampaignConfig = handler.populateCampaignConfig(getItineraryCarriers(QATAR_AIRLINE, VUELING_AIRLINE));
        assertEquals(airlineCampaignConfig.getHasAirlineCampaign(), true);
    }

    @Test
    public void testHasTwoAirlineCampaignTrue() {
        initializeTestConfiguration(initAirlinesTest(QATAR_AIRLINE, VUELING_AIRLINE));
        AirlineCampaignConfig airlineCampaignConfig = handler.populateCampaignConfig(getItineraryCarriers(VUELING_AIRLINE));
        assertEquals(airlineCampaignConfig.getHasAirlineCampaign(), true);
    }

    @Test
    public void testWhenAirlinesListIsEmpty() {
        initializeTestConfiguration(Collections.emptyList());
        AirlineCampaignConfig airlineCampaignConfig = handler.populateCampaignConfig(getItineraryCarriers(VUELING_AIRLINE));
        assertEquals(airlineCampaignConfig.getHasAirlineCampaign(), false);
    }

    @Test
    public void testWhenAirlinesListIsNull() {
        initializeTestConfiguration(null);
        AirlineCampaignConfig airlineCampaignConfig = handler.populateCampaignConfig(getItineraryCarriers(VUELING_AIRLINE));
        assertEquals(airlineCampaignConfig.getHasAirlineCampaign(), false);
    }

    private List<String> initAirlinesTest(String... airlines) {
        return Arrays.asList(airlines);
    }

    private Set<String> getItineraryCarriers(String... carrierIds) {
        return Sets.newHashSet(carrierIds);
    }

    private void initializeTestConfiguration(List<String> airlines) {
        doReturn(airlines).when(airlinesConfiguration).getAirlines();
        handler = new AirlineCampaignHandler(airlinesConfiguration);
    }
}
