package com.odigeo.frontend.resolvers.accommodation.summary.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSummaryRequest;
import com.odigeo.dapi.client.AccommodationShoppingItem;
import com.odigeo.dapi.client.AccommodationStaticInformation;
import com.odigeo.dapi.client.RoomGroupDeal;
import com.odigeo.dapi.client.ShoppingCart;
import com.odigeo.dapi.client.ShoppingCartSummaryResponse;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.exceptions.CityNotFoundException;
import com.odigeo.frontend.resolvers.accommodation.commons.handlers.AccommodationReviewHandler;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationSummaryContext;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationSummaryMapper;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationSummaryResponseDTO;
import com.odigeo.frontend.resolvers.geolocation.GeoLocationHandler;
import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.searchengine.v2.searchresults.SearchResults;
import com.odigeo.searchengine.v2.searchresults.criteria.AccommodationSearchCriteria;
import com.odigeo.searchengine.v2.searchresults.criteria.provider.AccommodationProviderSearchCriteria;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.frontend.resolvers.accommodation.summary.handlers.AccommodationSummaryHandler.REVIEW_PROVIDER_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.AssertJUnit.assertEquals;

public class AccommodationSummaryShoppingCartHandlerTest {

    private static final String VISIT_CODE = "visitCode";
    private static final int DEDUP_ID = 8;
    private static final int GEO_NODE_ID = 888;
    private static final long SEARCH_ID = 8L;
    private static final long BOOKING_ID = 8888L;
    @Mock
    private AccommodationSearchCriteriaRetrieveHandler accommodationSearchCriteriaRetrieveHandler;
    @Mock
    private GeoLocationHandler geolocationHandler;
    @Mock
    private Logger logger;
    @Mock
    private ShoppingCartSummaryResponse shoppingCartSummaryRS;
    @Mock
    private ShoppingCart shoppingCart;
    @Mock
    private AccommodationShoppingItem accommodationShoppingItem;
    @Mock
    private RoomGroupDeal roomGroupDeal;
    @Mock
    private SearchResults searchResults;
    @Mock
    private AccommodationSearchCriteria accommodationSearchCriteria;
    @Mock
    private AccommodationProviderSearchCriteria accommodationProviderSearchCriteria;
    @Mock
    private City city;
    @Mock
    private CityNotFoundException cityNotFoundException;
    @Mock
    private AccommodationDetailRetrieveHandler accommodationDetailRetrieveHandler;
    @Mock
    private AccommodationSummaryRequest request;
    @Mock
    private ResolverContext context;
    @Mock
    private AccommodationReviewHandler accommodationReviewHandler;
    @Mock
    private AccommodationSummaryMapper accommodationSummaryMapper;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private VisitInformation visit;
    @Mock
    private AccommodationSummaryResponseDTO expectedRs;
    @Mock
    private AccommodationStaticInformation accommodationStaticInformation;
    @Mock
    private AccommodationReview accommodationReview;

    @InjectMocks
    private AccommodationSummaryShoppingCartHandler handler;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        when(env.getContext()).thenReturn(context);
        when(context.getVisitInformation()).thenReturn(visit);
        when(shoppingCartSummaryRS.getShoppingCart()).thenReturn(shoppingCart);
        when(shoppingCart.getAccommodationShoppingItem()).thenReturn(accommodationShoppingItem);

        when(accommodationDetailRetrieveHandler.getShoppingCart(eq(BOOKING_ID), eq(context))).thenReturn(shoppingCartSummaryRS);
        when(accommodationStaticInformation.getDedupId()).thenReturn(DEDUP_ID);
        when(accommodationShoppingItem.getAccommodationStaticInformation()).thenReturn(accommodationStaticInformation);

        when(accommodationReviewHandler.getSingleReviewByProvider(eq(DEDUP_ID), eq(REVIEW_PROVIDER_ID))).thenReturn(accommodationReview);
        when(accommodationSummaryMapper.map(eq(accommodationShoppingItem), eq(accommodationReview), any(AccommodationSummaryContext.class))).thenReturn(expectedRs);
        when(request.getBookingId()).thenReturn(BOOKING_ID);
    }

    @Test
    public void testAccommodationSummaryFromShopping() {
        assertEquals(handler.accommodationSummaryFromShopping(request, env), expectedRs);
        verify(accommodationDetailRetrieveHandler).getShoppingCart(eq(BOOKING_ID), eq(context));
        verify(accommodationReviewHandler).getSingleReviewByProvider(eq(DEDUP_ID), eq(REVIEW_PROVIDER_ID));
    }

    @Test
    public void testGetDestinationCityNull() throws CityNotFoundException {
        handler.accommodationSummaryFromShopping(request, env);
        verify(geolocationHandler, never()).getCityGeonode(isNull(), isNull());
    }

    @Test
    public void testGetDestinationCitySearchNull() {
        when(visit.getVisitCode()).thenReturn(VISIT_CODE);
        when(accommodationShoppingItem.getRoomGroupDeal()).thenReturn(roomGroupDeal);
        when(roomGroupDeal.getSearchId()).thenReturn(SEARCH_ID);
        when(accommodationSearchCriteriaRetrieveHandler.getSearchResultsBySearchId(eq(SEARCH_ID), eq(VISIT_CODE))).thenReturn(searchResults);
        handler.accommodationSummaryFromShopping(request, env);
        verify(searchResults).getSearchCriteria();
        verify(accommodationSearchCriteria, never()).getProviderSearchCriteria();
    }

    @Test
    public void testGetDestinationCitySearch() throws CityNotFoundException {
        when(visit.getVisitCode()).thenReturn(VISIT_CODE);
        when(accommodationShoppingItem.getRoomGroupDeal()).thenReturn(roomGroupDeal);
        when(roomGroupDeal.getSearchId()).thenReturn(SEARCH_ID);
        when(accommodationSearchCriteriaRetrieveHandler.getSearchResultsBySearchId(eq(SEARCH_ID), eq(VISIT_CODE))).thenReturn(searchResults);
        when(searchResults.getSearchCriteria()).thenReturn(accommodationSearchCriteria);
        when(accommodationSearchCriteria.getProviderSearchCriteria()).thenReturn(accommodationProviderSearchCriteria);
        when(accommodationProviderSearchCriteria.getDestinationGeoNodeId()).thenReturn(GEO_NODE_ID);
        when(geolocationHandler.getCityGeonode(eq(GEO_NODE_ID), isNull())).thenReturn(city);

        handler.accommodationSummaryFromShopping(request, env);
        verify(searchResults).getSearchCriteria();
        verify(accommodationSearchCriteria).getProviderSearchCriteria();
        verify(accommodationProviderSearchCriteria).getDestinationGeoNodeId();
    }

    @Test
    public void testGetDestinationCitySearchCityException() throws CityNotFoundException {
        when(visit.getVisitCode()).thenReturn(VISIT_CODE);
        when(accommodationShoppingItem.getRoomGroupDeal()).thenReturn(roomGroupDeal);
        when(roomGroupDeal.getSearchId()).thenReturn(SEARCH_ID);
        when(accommodationSearchCriteriaRetrieveHandler.getSearchResultsBySearchId(eq(SEARCH_ID), eq(VISIT_CODE))).thenReturn(searchResults);
        when(searchResults.getSearchCriteria()).thenReturn(accommodationSearchCriteria);
        when(accommodationSearchCriteria.getProviderSearchCriteria()).thenReturn(accommodationProviderSearchCriteria);
        when(accommodationProviderSearchCriteria.getDestinationGeoNodeId()).thenReturn(GEO_NODE_ID);
        when(geolocationHandler.getCityGeonode(eq(GEO_NODE_ID), isNull())).thenThrow(cityNotFoundException);

        handler.accommodationSummaryFromShopping(request, env);
        verify(logger).warning(eq(AccommodationSummaryShoppingCartHandler.class), anyString(), eq(cityNotFoundException));
    }
}
