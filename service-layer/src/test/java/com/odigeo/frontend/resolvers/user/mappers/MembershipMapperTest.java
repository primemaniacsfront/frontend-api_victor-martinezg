package com.odigeo.frontend.resolvers.user.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.MembershipDurationTimeUnit;
import com.odigeo.frontend.resolvers.prime.models.MembershipStatus;
import com.odigeo.frontend.resolvers.prime.models.MembershipType;
import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import org.testng.annotations.Test;

import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MembershipMapperTest {

    public static final String ACTIVATION_DATE = "01-01-2021";
    public static final String AUTO_RENEWAL = "true";
    public static final String EXPIRATION_DATE = "01-01-2022";
    public static final long ID = 1234L;
    public static final int DURATION = 1;
    public static final String MONTHS = "MONTHS";
    public static final String SOURCE = "source";
    public static final String MEMBERSHIP_TYPE = MembershipType.BASIC.name();
    public static final String STATUS = MembershipStatus.ACTIVATED.name();
    public static final String NAME = "Anakin";
    public static final String LAST_NAMES = "Skywalker Vader";
    public static final BigDecimal RENEWAL_PRICE = BigDecimal.TEN;
    public static final String CURRENCY = "EUR";
    public static final BigDecimal TOTAL_PRICE = BigDecimal.ZERO;
    private MembershipMapper mapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mapper = new MembershipMapperImpl();
    }

    @Test
    public void testMembershipContractToModel() {
        MembershipResponse membershipResponse = new MembershipResponse();
        MemberAccountResponse memberAccountResponse = new MemberAccountResponse();
        memberAccountResponse.setName(NAME);
        memberAccountResponse.setLastNames(LAST_NAMES);
        membershipResponse.setMemberAccount(memberAccountResponse);
        membershipResponse.setActivationDate(ACTIVATION_DATE);
        membershipResponse.setAutoRenewal(AUTO_RENEWAL);
        membershipResponse.setExpirationDate(EXPIRATION_DATE);
        membershipResponse.setId(ID);
        membershipResponse.setMembershipType(MEMBERSHIP_TYPE);
        membershipResponse.setDuration(DURATION);
        membershipResponse.setDurationTimeUnit(MONTHS);
        membershipResponse.setSourceType(SOURCE);
        membershipResponse.setStatus(STATUS);
        membershipResponse.setTotalPrice(TOTAL_PRICE);
        membershipResponse.setRenewalPrice(RENEWAL_PRICE);
        membershipResponse.setCurrencyCode(CURRENCY);
        Membership membershipModel = mapper.membershipContractToModel(membershipResponse);
        assertEquals(membershipModel.getStatus(), STATUS);
        assertEquals(membershipModel.getActivationDate(), ACTIVATION_DATE);
        assertEquals(membershipModel.getId(), ID);
        assertEquals(membershipModel.getExpirationDate(), EXPIRATION_DATE);
        assertEquals(membershipModel.getDuration().getAmount(), DURATION);
        assertEquals(membershipModel.getDuration().getTimeUnit(), MembershipDurationTimeUnit.MONTHS);
        assertEquals(membershipModel.getAutoRenewalStatus(), AUTO_RENEWAL);
        assertEquals(membershipModel.getSourceType(), SOURCE);
        assertEquals(membershipModel.getType(), MEMBERSHIP_TYPE);
        assertEquals(membershipModel.getName(), NAME);
        assertEquals(membershipModel.getLastNames(), LAST_NAMES);
        assertEquals(membershipModel.getRenewalPrice().getAmount(), RENEWAL_PRICE);
        assertEquals(membershipModel.getRenewalPrice().getCurrency(), CURRENCY);
        assertEquals(membershipModel.getTotalPrice().getAmount(), TOTAL_PRICE);
        assertEquals(membershipModel.getTotalPrice().getCurrency(), CURRENCY);
        assertTrue(membershipModel.getIsFreeTrial());
    }
}
