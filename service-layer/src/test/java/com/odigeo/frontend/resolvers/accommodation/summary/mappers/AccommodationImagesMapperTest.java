package com.odigeo.frontend.resolvers.accommodation.summary.mappers;

import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationImagesMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationImage;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationImageQuality;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationImageType;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class AccommodationImagesMapperTest {

    private AccommodationImagesMapper mapper = new AccommodationImagesMapper();

    @Test
    public void testMap() {
        AccommodationImage accommodationImage = new AccommodationImage();
        accommodationImage.setUrl("url");
        accommodationImage.setThumbnailUrl("thumbnail url");
        accommodationImage.setQuality(AccommodationImageQuality.HIGH);
        accommodationImage.setType(AccommodationImageType.HOTEL);

        AccommodationImageDTO dto = mapper.map(accommodationImage);

        assertEquals(dto.getUrl(), accommodationImage.getUrl());
        assertEquals(dto.getThumbnailUrl(), accommodationImage.getThumbnailUrl());
        assertEquals(dto.getQuality().value(), accommodationImage.getQuality().value());
        assertEquals(dto.getType().name(), accommodationImage.getType().value());
    }

    @Test
    public void testMapNullValues() {
        AccommodationImageDTO dto = mapper.map(new AccommodationImage());

        assertNull(dto.getUrl());
        assertNull(dto.getThumbnailUrl());
        assertNull(dto.getQuality());
        assertNull(dto.getType());
    }

}
