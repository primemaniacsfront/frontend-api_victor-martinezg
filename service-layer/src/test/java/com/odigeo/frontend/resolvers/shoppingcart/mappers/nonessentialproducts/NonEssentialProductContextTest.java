package com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryPolicyConfiguration;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class NonEssentialProductContextTest {

    private NonEssentialProductContext context;

    @Mock
    private NonEssentialProductsConfiguration nonEssentialProductsConfiguration;

    @Mock
    private AncillaryPolicyConfiguration ancillaryPolicyConfiguration;

    @Mock
    private AncillaryConfiguration ancillaryConfiguration;

    @Mock
    private VisitInformation visit;

    @Mock
    private ShoppingCartContext shoppingCartContext;

    private static final Site SITE = Site.GB;
    private static final Device DEVICE = Device.DESKTOP;
    private static final String CANXSELF_POLICY = "CANXSELF";

    @BeforeMethod
    public void init() {
        initMocks(this);
        context = new NonEssentialProductContextBuilder()
                .withNonEssentialProductsConfiguration(nonEssentialProductsConfiguration)
                .withShoppingCartContext(shoppingCartContext)
                .build();
    }

    private void setup() {
        when(shoppingCartContext.getVisit()).thenReturn(visit);
        when(visit.getSite()).thenReturn(SITE);
        when(visit.getDevice()).thenReturn(DEVICE);

        try {
            when(nonEssentialProductsConfiguration.getConfiguration(visit)).thenReturn(ancillaryPolicyConfiguration);
        } catch (IOException ignored) {
            fail();
        }
        when(ancillaryPolicyConfiguration.get(ArgumentMatchers.<String>any())).thenReturn(ancillaryConfiguration);
        when(ancillaryPolicyConfiguration.get(null)).thenReturn(null);
    }

    @Test
    public void testGetAncillaryConfiguration() {
        setup();
        assertEquals(context.getAncillaryConfiguration(CANXSELF_POLICY), ancillaryConfiguration);
    }

    @Test
    public void testGetAncillaryConfigurationNull() {
        setup();
        assertNull(context.getAncillaryConfiguration(null));
    }
}
