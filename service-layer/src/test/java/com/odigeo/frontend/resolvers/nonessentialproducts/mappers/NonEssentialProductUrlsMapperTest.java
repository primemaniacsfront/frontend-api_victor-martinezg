package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrl;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrlType;
import com.odigeo.dapi.client.Insurance;
import com.odigeo.dapi.client.InsuranceUrl;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import org.apache.commons.collections4.CollectionUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class NonEssentialProductUrlsMapperTest {

    @Mock
    private NonEssentialProductUrlMapper nonEssentialProductUrlMapper;
    @Mock
    private AncillaryConfiguration ancillaryConfiguration;
    @Mock
    private Insurance insurance;
    @InjectMocks
    private NonEssentialProductUrlsMapper mapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void mapShouldNotReturnElementsWhenInsuranceAndIpidAreNull() {
        when(ancillaryConfiguration.getIpid()).thenReturn(null);

        List<NonEssentialProductUrl> nonEssentialProductUrls = mapper.map(null, ancillaryConfiguration);

        assertNotNull(nonEssentialProductUrls);
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductUrls));
    }

    @Test
    public void mapShouldNotReturnElementsWhenInsuranceHasNullUrlsAndIpidIsFalse() {
        when(ancillaryConfiguration.getIpid()).thenReturn(Boolean.FALSE);
        when(insurance.getConditionsUrls()).thenReturn(null);

        List<NonEssentialProductUrl> nonEssentialProductUrls = mapper.map(insurance, ancillaryConfiguration);

        assertNotNull(nonEssentialProductUrls);
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductUrls));
    }

    @Test
    public void mapShouldNotReturnElementsWhenInsuranceHasEmptyUrlsAndIpidIsFalse() {
        when(ancillaryConfiguration.getIpid()).thenReturn(Boolean.FALSE);
        when(insurance.getConditionsUrls()).thenReturn(Collections.emptyList());

        List<NonEssentialProductUrl> nonEssentialProductUrls = mapper.map(insurance, ancillaryConfiguration);

        assertNotNull(nonEssentialProductUrls);
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductUrls));
    }

    @Test
    public void mapShouldReturnIpidElementWhenIpidIsTrue() {
        when(ancillaryConfiguration.getIpid()).thenReturn(Boolean.TRUE);
        when(insurance.getConditionsUrls()).thenReturn(Collections.emptyList());

        List<NonEssentialProductUrl> nonEssentialProductUrls = mapper.map(insurance, ancillaryConfiguration);

        assertTrue(CollectionUtils.isNotEmpty(nonEssentialProductUrls));
        assertEquals(nonEssentialProductUrls.get(0).getUrlType(), NonEssentialProductUrlType.IPID);
    }

    @Test
    public void mapShouldReturnElementsWhenInsuranceHasUrls() {
        when(ancillaryConfiguration.getIpid()).thenReturn(Boolean.TRUE);
        InsuranceUrl insuranceUrl = new InsuranceUrl();
        when(insurance.getConditionsUrls()).thenReturn(Arrays.asList(insuranceUrl));
        NonEssentialProductUrl nonEssentialProductUrl = new NonEssentialProductUrl();
        when(nonEssentialProductUrlMapper.map(insuranceUrl)).thenReturn(nonEssentialProductUrl);

        List<NonEssentialProductUrl> nonEssentialProductUrls = mapper.map(insurance, ancillaryConfiguration);

        assertTrue(CollectionUtils.isNotEmpty(nonEssentialProductUrls));
        assertEquals(nonEssentialProductUrls.get(0), nonEssentialProductUrl);
    }

}
