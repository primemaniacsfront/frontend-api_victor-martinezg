package com.odigeo.frontend.resolvers.nonessentialproducts.configuration;

import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryPolicyConfiguration;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class NonEssentialProductsConfigurationTest {

    private static final String CNXFLT_151 = "CNXFLT151";

    @Mock
    VisitInformation visitInformation;

    NonEssentialProductsConfiguration nonEssentialProductsConfiguration;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        nonEssentialProductsConfiguration = new NonEssentialProductsConfiguration();
    }

    @Test
    public void getConfigurationWithUnknownSiteShouldReturnDefault() throws IOException {
        when(visitInformation.getSite()).thenReturn(Site.UNKNOWN);
        when(visitInformation.getDevice()).thenReturn(Device.MOBILE);

        AncillaryPolicyConfiguration configuration = nonEssentialProductsConfiguration.getConfiguration(visitInformation);

        assertEquals(configuration.get(CNXFLT_151).getPricePerception().toString(), "PERPAX");
    }

    @Test
    public void getConfigurationWithUnknownDeviceShouldReturnNull() throws IOException {
        when(visitInformation.getSite()).thenReturn(Site.ES);
        when(visitInformation.getDevice()).thenReturn(Device.TABLET);

        AncillaryPolicyConfiguration configuration = nonEssentialProductsConfiguration.getConfiguration(visitInformation);

        assertNull(configuration);
    }

    @Test
    public void getConfigurationWithSiteShouldOverwriteDefault() throws IOException {
        when(visitInformation.getSite()).thenReturn(Site.ES);
        when(visitInformation.getDevice()).thenReturn(Device.DESKTOP);

        AncillaryPolicyConfiguration configuration = nonEssentialProductsConfiguration.getConfiguration(visitInformation);

        assertEquals(configuration.get(CNXFLT_151).getPricePerception().toString(), "PERSEGMENT");
    }
}
