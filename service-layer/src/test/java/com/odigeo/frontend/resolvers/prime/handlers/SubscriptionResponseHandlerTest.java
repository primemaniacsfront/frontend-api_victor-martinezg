package com.odigeo.frontend.resolvers.prime.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionOfferPeriod;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionOfferTimeUnit;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionOfferType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SubscriptionResponse;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffers;
import com.odigeo.membership.offer.api.response.TimeUnit;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class SubscriptionResponseHandlerTest {

    private static final BigDecimal RENEWAL_PRICE = BigDecimal.TEN;
    private static final int DURATION = 1;
    private static final int RENEWAL_DURATION = 12;
    private static final int ONE = 1;
    private static final BigDecimal PRICE = BigDecimal.ZERO;
    private static final String OFFER_ID = "offerId";
    private static final long PRODUCT_ID = 1L;
    private static final String CURRENCY = "currency";
    private static final String INVALID_TYPE = "NEW_PLUS";

    @Mock
    MembershipSubscriptionOffers membershipSubscriptionOffers;
    @Mock
    MembershipSubscriptionOffer membershipSubscriptionOffer;
    private SubscriptionResponseHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new SubscriptionResponseHandler();
    }

    @Test
    public void testBuildSubscriptionResponse() {
        MembershipSubscriptionOffer subscriptionOffer = buildOffer(SubscriptionOfferType.BASIC);
        SubscriptionResponse responseDTO = handler.buildSubscriptionResponse(subscriptionOffer);
        assertOfferWithDefaultValues(responseDTO, SubscriptionOfferType.BASIC);
    }

    private void assertOfferWithDefaultValues(SubscriptionResponse responseDTO, SubscriptionOfferType type) {
        assertEquals(responseDTO.getOfferId(), OFFER_ID);
        assertEquals(responseDTO.getProductId(), PRODUCT_ID);
        assertEquals(responseDTO.getFee().getCurrency(), CURRENCY);
        assertEquals(responseDTO.getFee().getAmount(), PRICE);
        assertEquals(responseDTO.getPrice().getAmount(), PRICE);
        assertEquals(responseDTO.getPrice().getCurrency(), CURRENCY);
        assertEquals(responseDTO.getRenewalPrice().getAmount(), RENEWAL_PRICE);
        assertEquals(responseDTO.getPrice().getCurrency(), CURRENCY);
        assertEquals(responseDTO.getDuration().getAmount(), DURATION);
        assertEquals(responseDTO.getDuration().getTimeUnit(), SubscriptionOfferTimeUnit.DAYS);
        assertEquals(responseDTO.getRenewalDuration().getAmount(), RENEWAL_DURATION);
        assertEquals(responseDTO.getRenewalDuration().getTimeUnit(), SubscriptionOfferTimeUnit.MONTHS);
        assertEquals(responseDTO.getSubscriptionPeriod(), SubscriptionOfferPeriod.FREE_TRIAL);
        assertEquals(responseDTO.getType(), type);
    }

    private MembershipSubscriptionOffer buildOffer(SubscriptionOfferType type) {
        return MembershipSubscriptionOffer.builder()
                .setPrice(PRICE)
                .setRenewalPrice(RENEWAL_PRICE)
                .setOfferId(OFFER_ID)
                .setMembershipType(type.name())
                .setCurrencyCode(CURRENCY)
                .setProductId(PRODUCT_ID)
                .setDuration(DURATION)
                .setRenewalDuration(RENEWAL_DURATION)
                .setDurationTimeUnit(TimeUnit.DAYS)
                .build();
    }

    @Test
    public void testBuildSubscriptionOffersResponse() {
        MembershipSubscriptionOffer basicSubscriptionOffer = buildOffer(SubscriptionOfferType.BASIC);
        MembershipSubscriptionOffer plusSubscriptionOffer = buildOffer(SubscriptionOfferType.PLUS);
        when(membershipSubscriptionOffers.getMembershipSubscriptionOffers()).thenReturn(Arrays.asList(basicSubscriptionOffer, plusSubscriptionOffer));
        List<SubscriptionResponse> offersList = handler.buildSubscriptionOffersResponse(membershipSubscriptionOffers);
        assertOfferWithDefaultValues(offersList.get(0), SubscriptionOfferType.BASIC);
        assertOfferWithDefaultValues(offersList.get(1), SubscriptionOfferType.PLUS);
    }

    @Test
    public void testBuildSubscriptionResponseWithoutOffer() {
        assertNull(handler.buildSubscriptionResponse(null));
    }

    @Test
    public void testGetDurationOrDefault() {
        assertEquals(handler.getDurationOrDefault(null), ONE);
    }

    @Test
    public void testUnknownSubscriptionType() {
        when(membershipSubscriptionOffer.getMembershipType()).thenReturn(INVALID_TYPE);
        assertEquals(handler.getMembershipType(membershipSubscriptionOffer), SubscriptionOfferType.BASIC);
    }
}
