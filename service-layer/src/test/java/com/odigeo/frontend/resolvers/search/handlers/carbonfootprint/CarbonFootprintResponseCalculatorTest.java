package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.ItineraryCo2FootprintResult;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.response.ItineraryFootprintCalculatorResponse;
import com.google.inject.Guice;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static voldemort.utils.Utils.assertNotNull;

public class CarbonFootprintResponseCalculatorTest {

    @Mock
    private CarbonFootprintHelper carbonFootprintHelper;

    private CarbonFootprintResponseCalculator calculator;

    private static final BigDecimal TOTAL_CO2 = BigDecimal.TEN;
    private static final BigDecimal TOTAL_CO2E = BigDecimal.ONE;
    private static final Integer ITINERARY_INDEX = Integer.MAX_VALUE;
    private static final int ECO_PERCENTAGE = 10;

    @BeforeMethod
    public void init() {

        MockitoAnnotations.openMocks(this);

        calculator = new CarbonFootprintResponseCalculator();
        Guice.createInjector(binder -> {
            binder.bind(CarbonFootprintHelper.class).toProvider(() -> carbonFootprintHelper);
        }).injectMembers(calculator);

        when(carbonFootprintHelper.getSearchKilosAverage(any())).thenReturn(BigDecimal.valueOf(100));
        when(carbonFootprintHelper.calculatePercentageEco(any(), any())).thenReturn(ECO_PERCENTAGE);
    }

    @Test
    public void testWhenNull() {
        assertNotNull(calculator.calculateFootprintMap(null));
    }

    @Test
    public void testWhenOneResult() {
        ItineraryFootprintCalculatorResponse response = new ItineraryFootprintCalculatorResponse();
        response.setItineraryCo2FootprintResults(new ArrayList<>());
        response.getItineraryCo2FootprintResults().add(getItineraryCo2FootprintResult());

        CarbonFootprintResponse footprintMap = calculator.calculateFootprintMap(response);
        assertEquals(footprintMap.getItineraries().size(), 1);
        assertEquals(footprintMap.getItineraries().size(), 1);
        assertEquals(footprintMap.getItineraries().get(ITINERARY_INDEX).getTotalCo2eKilos(), TOTAL_CO2E);
        assertEquals(footprintMap.getItineraries().get(ITINERARY_INDEX).getTotalCo2Kilos(), TOTAL_CO2);
        assertEquals(footprintMap.getItineraries().get(ITINERARY_INDEX).getEcoPercentageThanAverage(), ECO_PERCENTAGE);
        assertTrue(footprintMap.getItineraries().get(ITINERARY_INDEX).getIsEco());
    }

    private ItineraryCo2FootprintResult getItineraryCo2FootprintResult() {
        ItineraryCo2FootprintResult itinerary = new ItineraryCo2FootprintResult();
        itinerary.setKilosCo2(TOTAL_CO2);
        itinerary.setKilosCo2e(TOTAL_CO2E);
        itinerary.setItineraryIndex(ITINERARY_INDEX);
        return itinerary;
    }
}
