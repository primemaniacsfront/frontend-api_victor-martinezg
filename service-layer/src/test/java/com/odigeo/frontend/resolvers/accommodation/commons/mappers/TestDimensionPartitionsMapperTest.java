package com.odigeo.frontend.resolvers.accommodation.commons.mappers;

import com.odigeo.hcsapi.v9.beans.TestDimensionPartition;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestDimensionPartitionsMapperTest {

    private TestDimensionPartitionsMapper mapper = new TestDimensionPartitionsMapper();

    @Test
    public void mapSingle() {
        TestDimensionPartition testDimensionPartition = mapper.map("dimension", 1);

        Assert.assertEquals(testDimensionPartition.getDimension(), "dimension");
        Assert.assertEquals(testDimensionPartition.getPartition().intValue(), 1);
    }

    @Test
    public void mapMultiple() {
        Map<String, Integer> testDimensionPartitions = new HashMap<>();
        testDimensionPartitions.put("dimension", 1);
        List<TestDimensionPartition> result = mapper.map(testDimensionPartitions);

        Assert.assertEquals(result.size(), 1);
        Assert.assertEquals(result.get(0).getDimension(), "dimension");
        Assert.assertEquals(result.get(0).getPartition().intValue(), 1);
    }

    @Test
    public void mapMultipleEmpty() {
        Map<String, Integer> testDimensionPartitions = new HashMap<>();
        List<TestDimensionPartition> result = mapper.map(testDimensionPartitions);

        Assert.assertEquals(result.size(), 0);
    }

    @Test
    public void mapMultipleNull() {
        Map<String, Integer> testDimensionPartitions = null;
        List<TestDimensionPartition> result = mapper.map(testDimensionPartitions);

        Assert.assertEquals(result.size(), 0);
    }

}
