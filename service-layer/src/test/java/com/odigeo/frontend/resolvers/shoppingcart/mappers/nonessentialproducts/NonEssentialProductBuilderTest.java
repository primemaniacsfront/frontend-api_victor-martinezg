package com.odigeo.frontend.resolvers.shoppingcart.mappers.nonessentialproducts;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductProviderType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductUrl;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Price;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.context.visit.types.Device;
import com.odigeo.frontend.commons.context.visit.types.Site;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryPolicyConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.PricePerceptionType;
import com.odigeo.frontend.resolvers.shoppingcart.mappers.ShoppingCartContext;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class NonEssentialProductBuilderTest {

    @Mock
    NonEssentialProductContext nonEssentialProductContext;

    @Mock
    private NonEssentialProductsConfiguration nonEssentialProductsConfiguration;

    @Mock
    private AncillaryPolicyConfiguration ancillaryPolicyConfiguration;

    @Mock
    private AncillaryConfiguration ancillaryConfiguration;

    @Mock
    private VisitInformation visit;

    @Mock
    private ShoppingCartContext shoppingCartContext;

    private static final String POLICY = "POLICY";
    private static final Site SITE = Site.GB;
    private static final Device DEVICE = Device.DESKTOP;

    @BeforeMethod
    public void init() {
        initMocks(this);
        try {
            when(ancillaryConfiguration.getPricePerception()).thenReturn(PricePerceptionType.PERPLAN);
            when(ancillaryPolicyConfiguration.get(ArgumentMatchers.<String>any())).thenReturn(ancillaryConfiguration);
            when(nonEssentialProductsConfiguration.getConfiguration(any())).thenReturn(ancillaryPolicyConfiguration);
            when(visit.getSite()).thenReturn(SITE);
            when(visit.getDevice()).thenReturn(DEVICE);
            when(shoppingCartContext.getVisit()).thenReturn(visit);
            when(nonEssentialProductContext.getShoppingCartContext()).thenReturn(shoppingCartContext);
            when(nonEssentialProductContext.getAncillaryConfiguration(any())).thenReturn(ancillaryConfiguration);
        } catch (IOException ignored) {
            fail();
        }
    }

    @Test
    public void testBuild() {
        NonEssentialProductProviderType provider = NonEssentialProductProviderType.NOSUPP;
        BigDecimal totalPrice = BigDecimal.valueOf(Math.random() * 300.0);
        List<NonEssentialProductUrl> urls = Collections.emptyList();

        NonEssentialProduct product = new NonEssentialProductBuilder()
                .withContext(nonEssentialProductContext)
                .withProvider(provider)
                .withTotalPrice(totalPrice)
                .withPolicy(POLICY)
                .withUrls(urls)
                .build();

        assertEquals(product.getOfferId(), POLICY);
        assertEquals(product.getProvider(), provider);
        assertEquals(product.getUrls(), urls);
        Price price = product.getPrice();
        assertEquals(price.getPerception().getType(), com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PricePerceptionType.PERPLAN);
        assertEquals(price.getValue().getAmount(), totalPrice);
    }
}
