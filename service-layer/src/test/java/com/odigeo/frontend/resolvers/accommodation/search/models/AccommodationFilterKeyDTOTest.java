package com.odigeo.frontend.resolvers.accommodation.search.models;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.testng.annotations.Test;

public class AccommodationFilterKeyDTOTest extends BeanTest<AccommodationFilterKeyDTO> {

    @Override
    protected AccommodationFilterKeyDTO getBean() {
        return new AccommodationFilterKeyDTO("TEST_TYPE", "TEST_NAME");
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(AccommodationFilterKeyDTO.class)
                .withNonnullFields("filterType", "filterName")
                .usingGetClass()
                .verify();
    }
}
