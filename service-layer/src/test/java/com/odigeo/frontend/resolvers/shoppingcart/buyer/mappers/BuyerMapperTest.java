package com.odigeo.frontend.resolvers.shoppingcart.buyer.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BuyerIdentificationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RequiredFieldType;
import com.odigeo.dapi.client.Buyer;
import com.odigeo.dapi.client.Phone;
import com.odigeo.dapi.client.RequiredField;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class BuyerMapperTest {

    private BuyerMapper mapper;
    private com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Buyer buyerModel;

    @Mock
    private Buyer buyer;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new BuyerMapperImpl();
    }

    @Test
    public void testBuyerContractToModel() {
        assertNull(mapper.map(null));
    }

    @Test
    public void testBuyerIdentificationTypes() {
        Map<com.odigeo.dapi.client.BuyerIdentificationType, BuyerIdentificationType> idTypes = Stream.of(
                new AbstractMap.SimpleImmutableEntry<>(com.odigeo.dapi.client.BuyerIdentificationType.PASSPORT, BuyerIdentificationType.PASSPORT),
                new AbstractMap.SimpleImmutableEntry<>(com.odigeo.dapi.client.BuyerIdentificationType.NIE, BuyerIdentificationType.NIE),
                new AbstractMap.SimpleImmutableEntry<>(com.odigeo.dapi.client.BuyerIdentificationType.NIF, BuyerIdentificationType.NIF),
                new AbstractMap.SimpleImmutableEntry<>(com.odigeo.dapi.client.BuyerIdentificationType.NATIONAL_ID_CARD,
                    BuyerIdentificationType.NATIONAL_ID_CARD),
                new AbstractMap.SimpleImmutableEntry<>(com.odigeo.dapi.client.BuyerIdentificationType.BIRTH_DATE, BuyerIdentificationType.BIRTH_DATE))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        idTypes.forEach((k, v) -> {
            when(buyer.getBuyerIdentificationType()).thenReturn(k);
            buyerModel = mapper.map(buyer);
            assertEquals(buyerModel.getBuyerIdentificationType(), v);
        });
    }

    @Test
    public void testMapBuyer() {
        com.odigeo.dapi.client.Buyer buyerDAPI = createBuyer();
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Buyer buyerBean = mapper.map(buyerDAPI);
        assertEquals(buyerBean.getAddress(), buyerDAPI.getAddress());
        assertEquals(buyerBean.getCityName(), buyerDAPI.getCityName());
        assertEquals(buyerBean.getCountry(), buyerDAPI.getCountry());
        assertEquals(buyerBean.getIdentification(), buyerDAPI.getIdentification());
        assertEquals(buyerBean.getLastNames(), buyerDAPI.getLastNames());
        assertEquals(buyerBean.getName(), buyerDAPI.getName());
        assertEquals(buyerBean.getMail(), buyerDAPI.getMail());
        assertEquals(buyerBean.getStateName(), buyerDAPI.getStateName());
        assertEquals(buyerBean.getZipCode(), buyerDAPI.getZipCode());
        assertEquals(buyerBean.getCpf(), buyerDAPI.getCpf());
    }

    private com.odigeo.dapi.client.Buyer createBuyer() {
        com.odigeo.dapi.client.Buyer buyerDTO = new com.odigeo.dapi.client.Buyer();
        buyerDTO.setAddress("Address");
        buyerDTO.setAlternativePhoneNumber(new Phone());
        buyerDTO.setCityName("Spain");
        buyerDTO.setCountry("ES");
        buyerDTO.setIdentification("123456A");
        buyerDTO.setLastNames("LastNames");
        buyerDTO.setName("Name");
        buyerDTO.setMail("test@odigeo.com");
        buyerDTO.setStateName("stateName");
        buyerDTO.setPhoneNumber(new Phone());
        buyerDTO.setZipCode("28800");
        buyerDTO.setCpf("123");
        return buyerDTO;
    }

    @Test
    public void testMapBuyerInformationDescription() {
        com.odigeo.dapi.client.BuyerInformationDescription buyerInformationDescription = createBuyerInfo();
        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BuyerInformationDescription result = mapper.mapBuyerInformationDescription(buyerInformationDescription);
        assertEquals(result.getIdentificationTypes().get(0), BuyerIdentificationType.NIF);
        assertEquals(result.getNeedsName(), RequiredFieldType.MANDATORY);
        assertEquals(result.getNeedsLastNames(), RequiredFieldType.NOT_REQUIRED);
        assertEquals(result.getNeedsMail(), RequiredFieldType.CONDITIONAL_REQUIRED);
        assertEquals(result.getNeedsIdentification(), RequiredFieldType.CONDITIONAL_REQUIRED);
        assertEquals(result.getNeedsDateOfBirth(), RequiredFieldType.NOT_REQUIRED);
        assertEquals(result.getNeedsCpf(), RequiredFieldType.CONDITIONAL_REQUIRED);
        assertEquals(result.getNeedsAddress(), RequiredFieldType.MANDATORY);
        assertEquals(result.getNeedsCityName(), RequiredFieldType.CONDITIONAL_REQUIRED);
        assertEquals(result.getNeedsNeighborhood(), RequiredFieldType.NOT_REQUIRED);
        assertEquals(result.getNeedsStateName(), RequiredFieldType.MANDATORY);
        assertEquals(result.getNeedsZipCode(), RequiredFieldType.NOT_REQUIRED);
        assertEquals(result.getNeedsPhoneNumber(), RequiredFieldType.MANDATORY);
        assertEquals(result.getNeedsAlternativePhoneNumber(), RequiredFieldType.CONDITIONAL_REQUIRED);
        assertEquals(result.getNeedsCountryCode(), RequiredFieldType.NOT_REQUIRED);
    }

    private com.odigeo.dapi.client.BuyerInformationDescription createBuyerInfo() {
        com.odigeo.dapi.client.BuyerInformationDescription buyerInformationDescription = mock(com.odigeo.dapi.client.BuyerInformationDescription.class);
        when(buyerInformationDescription.getIdentificationTypes()).thenReturn(Collections.singletonList(com.odigeo.dapi.client.BuyerIdentificationType.NIF));
        when(buyerInformationDescription.getNeedsName()).thenReturn(RequiredField.MANDATORY);
        when(buyerInformationDescription.getNeedsLastNames()).thenReturn(RequiredField.NOT_REQUIRED);
        when(buyerInformationDescription.getNeedsMail()).thenReturn(RequiredField.CONDITIONAL_REQUIRED);
        when(buyerInformationDescription.getNeedsIdentification()).thenReturn(RequiredField.CONDITIONAL_REQUIRED);
        when(buyerInformationDescription.getNeedsDateOfBirth()).thenReturn(RequiredField.NOT_REQUIRED);
        when(buyerInformationDescription.getNeedsCpf()).thenReturn(RequiredField.CONDITIONAL_REQUIRED);
        when(buyerInformationDescription.getNeedsAddress()).thenReturn(RequiredField.MANDATORY);
        when(buyerInformationDescription.getNeedsCityName()).thenReturn(RequiredField.CONDITIONAL_REQUIRED);
        when(buyerInformationDescription.getNeedsNeighborhood()).thenReturn(RequiredField.NOT_REQUIRED);
        when(buyerInformationDescription.getNeedsStateName()).thenReturn(RequiredField.MANDATORY);
        when(buyerInformationDescription.getNeedsZipCode()).thenReturn(RequiredField.NOT_REQUIRED);
        when(buyerInformationDescription.getNeedsPhoneNumber()).thenReturn(RequiredField.MANDATORY);
        when(buyerInformationDescription.getNeedsAlternativePhoneNumber()).thenReturn(RequiredField.CONDITIONAL_REQUIRED);
        when(buyerInformationDescription.getNeedsCountryCode()).thenReturn(RequiredField.NOT_REQUIRED);
        return buyerInformationDescription;
    }

}

