package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.util.DateUtils;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.location.LocationDTO;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.common.CabinClass;
import com.odigeo.searchengine.v2.responses.itinerary.Section;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.ZonedDateTime;
import java.util.AbstractMap;
import java.util.Calendar;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class SectionMapperTest {

    @Mock
    private DateUtils dateUtils;
    @Mock
    private TransportHandler transportHandler;

    private SectionMapper sectionMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        sectionMapper = new SectionMapper();

        Guice.createInjector(binder -> {
            binder.bind(DateUtils.class).toProvider(() -> dateUtils);
            binder.bind(TransportHandler.class).toProvider(() -> transportHandler);
        }).injectMembers(sectionMapper);
    }

    @Test
    public void testBuildSection() {
        SearchEngineContext seContext = mock(SearchEngineContext.class);
        SectionResult sectionResult = mock(SectionResult.class);
        Section section = mock(Section.class);
        Carrier carrier = mock(Carrier.class);
        Carrier operatingCarrier = mock(Carrier.class);
        LocationDTO departure = mock(LocationDTO.class);
        LocationDTO destination = mock(LocationDTO.class);
        TechnicalStop techStop = mock(TechnicalStop.class);
        Calendar departureDate = mock(Calendar.class);
        Calendar arrivalDate = mock(Calendar.class);
        ZonedDateTime responseDepartureDate = mock(ZonedDateTime.class);
        ZonedDateTime responseArrivalDate = mock(ZonedDateTime.class);
        CabinClass cabinClass = mock(CabinClass.class);
        TransportType transportType = mock(TransportType.class);

        Integer sectionResultId = 1;
        Integer carrierId = 2;
        Integer operatingCarrierId = 3;
        Integer departureId = 4;
        Integer destinationId = 5;
        Integer baggageAllowance = 10;
        String sectionId = "sectionId";
        String departureTerminal = "departureTerminal";
        String arrivalTerminal = "arrivalTerminal";
        String vehicleModel = "model";
        String flightCode = "flightCode";
        Long duration = Long.valueOf(120);

        when(seContext.getCarrierMap()).thenReturn(Stream.of(
            new AbstractMap.SimpleEntry<>(carrierId, carrier),
            new AbstractMap.SimpleEntry<>(operatingCarrierId, operatingCarrier)
        ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue)));
        when(seContext.getLocationMap()).thenReturn(Stream.of(
            new AbstractMap.SimpleEntry<>(departureId, departure),
            new AbstractMap.SimpleEntry<>(destinationId, destination)
        ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue)));
        when(seContext.getTechStopsMap())
            .thenReturn(Collections.singletonMap(sectionResultId, Collections.singletonList(techStop)));

        when(sectionResult.getId()).thenReturn(sectionResultId);
        when(sectionResult.getSection()).thenReturn(section);
        when(section.getId()).thenReturn(sectionId);
        when(section.getDepartureDate()).thenReturn(departureDate);
        when(section.getArrivalDate()).thenReturn(arrivalDate);
        when(section.getFrom()).thenReturn(departureId);
        when(section.getTo()).thenReturn(destinationId);
        when(section.getCarrier()).thenReturn(carrierId);
        when(section.getOperatingCarrier()).thenReturn(operatingCarrierId);
        when(section.getCabinClass()).thenReturn(cabinClass);
        when(section.getDepartureTerminal()).thenReturn(departureTerminal);
        when(section.getArrivalTerminal()).thenReturn(arrivalTerminal);
        when(section.getBaggageAllowanceQuantity()).thenReturn(baggageAllowance);
        when(section.getVehicleModel()).thenReturn(vehicleModel);
        when(section.getFlightCode()).thenReturn(flightCode);
        when(section.getDuration()).thenReturn(duration);

        when(dateUtils.convertFromCalendar(eq(departureDate))).thenReturn(responseDepartureDate);
        when(dateUtils.convertFromCalendar(eq(arrivalDate))).thenReturn(responseArrivalDate);

        when(transportHandler.calculateSectionTransportType(eq(departure), eq(destination)))
            .thenReturn(transportType);

        SectionDTO sectionDTO = sectionMapper.buildSection(sectionResult, seContext);

        assertEquals(sectionDTO.getKey(), sectionId);
        assertEquals(sectionDTO.getDepartureDate(), responseDepartureDate);
        assertEquals(sectionDTO.getArrivalDate(), responseArrivalDate);
        assertEquals(sectionDTO.getDeparture(), departure);
        assertEquals(sectionDTO.getDestination(), destination);
        assertEquals(sectionDTO.getCarrier(), carrier);
        assertEquals(sectionDTO.getOperatingCarrier(), operatingCarrier);
        assertEquals(sectionDTO.getCabinClass(), cabinClass);
        assertEquals(sectionDTO.getFlightCode(), flightCode);
        assertEquals(sectionDTO.getArrivalTerminal(), arrivalTerminal);
        assertEquals(sectionDTO.getDepartureTerminal(), departureTerminal);
        assertEquals(sectionDTO.getBaggageAllowance(), baggageAllowance);
        assertEquals(sectionDTO.getVehicleModel(), vehicleModel);
        assertEquals(sectionDTO.getTransportType(), transportType);
        assertEquals(sectionDTO.getDuration(), duration);
    }

}
