package com.odigeo.frontend.resolvers.itinerary.configuration;

import bean.test.BeanTest;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Arrays;

import static org.testng.AssertJUnit.assertEquals;

public class FreeRebookingConfigurationTest extends BeanTest<FreeRebookingConfiguration> {

    @Override
    protected FreeRebookingConfiguration getBean() {
        FreeRebookingConfiguration bean = new FreeRebookingConfiguration();
        bean.setSites(Arrays.asList("sites"));
        bean.setCarriers(Arrays.asList("carriers"));
        bean.setMaxDateFormat("dd/MM/yyyy");
        bean.setMaxDate("30/09/2021");
        return bean;
    }

    @Test
    public void checkMaxDateFormatted() {
        FreeRebookingConfiguration bean = getBean();
        assertEquals(bean.getMaxDateFormatted(), LocalDate.of(2021, 9, 30));
    }
}
