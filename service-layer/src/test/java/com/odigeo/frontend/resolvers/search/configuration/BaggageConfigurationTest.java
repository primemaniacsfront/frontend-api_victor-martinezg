package com.odigeo.frontend.resolvers.search.configuration;

import bean.test.BeanTest;

import java.util.Collections;

public class BaggageConfigurationTest extends BeanTest<BaggageConfiguration> {

    @Override
    protected BaggageConfiguration getBean() {
        BaggageConfiguration bean = new BaggageConfiguration();
        bean.setAirlinesWithSmallBagInCabin(Collections.emptyList());
        return bean;
    }

}
