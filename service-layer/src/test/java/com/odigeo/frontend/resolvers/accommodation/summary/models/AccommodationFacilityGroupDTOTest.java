package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;

public class AccommodationFacilityGroupDTOTest extends BeanTest<AccommodationFacilityGroupDTO> {

    public AccommodationFacilityGroupDTO getBean() {
        AccommodationFacilityGroupDTO bean = new AccommodationFacilityGroupDTO();
        bean.setCode("code");
        bean.setDescription("description");

        return bean;
    }
}
