package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Perks;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import com.odigeo.searchengine.v2.responses.membership.MembershipPerks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class ItineraryPerksHandlerTest {

    private static final String CURRENCY_CODE = "USD";
    private static final int NUM_PASSENGERS = 3;
    @Mock
    private SearchEngineContext seContext;
    @Mock
    private FareItinerary fareItinerary;
    private MembershipPerks membershipPerks;
    private PerksHandler perksHandler;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        when(seContext.getCurrency()).thenReturn(CURRENCY_CODE);
        membershipPerks = new MembershipPerks();
        perksHandler = new PerksHandler();
    }

    @DataProvider(name = "fareItineraryMembershipPerks")
    public Object[][] perksProvider() {
        return new Object[][]{
                {BigDecimal.TEN, BigDecimal.ONE},
                {BigDecimal.TEN, null},
                {null, BigDecimal.ONE},
                {null, null}
        };
    }

    @Test(dataProvider = "fareItineraryMembershipPerks")
    public void testPerksFromItineraryToDTO(BigDecimal discount, BigDecimal coupon) {
        membershipPerks.setFee(discount);
        membershipPerks.setMembershipCoupon(coupon);
        when(fareItinerary.getMembershipPerks()).thenReturn(membershipPerks);
        Perks perksDTO = perksHandler.populatePerks(seContext, fareItinerary, NUM_PASSENGERS);
        checkCoupons(perksDTO);
        checkDiscounts(perksDTO);
        checkCurrency(perksDTO);
    }

    private void checkCoupons(Perks perksDTO) {
        if (Objects.nonNull(membershipPerks.getMembershipCoupon())) {
            assertEquals(perksDTO.getExtraPrimeCoupon().getAmount(), membershipPerks.getMembershipCoupon());
            assertEquals(perksDTO.getExtraPrimeCouponPerPassenger().getAmount(),
                    membershipPerks.getMembershipCoupon().divide(new BigDecimal(NUM_PASSENGERS), 2, RoundingMode.HALF_UP));
        } else {
            assertNull(perksDTO.getExtraPrimeCoupon());
            assertNull(perksDTO.getExtraPrimeCouponPerPassenger());
        }
    }

    private void checkDiscounts(Perks perksDTO) {
        if (Objects.nonNull(membershipPerks.getFee())) {
            assertEquals(perksDTO.getPrimeDiscount().getAmount(), membershipPerks.getFee());
            assertEquals(perksDTO.getPrimeDiscountPerPassenger().getAmount(),
                    membershipPerks.getFee().divide(new BigDecimal(NUM_PASSENGERS), 2, RoundingMode.HALF_UP));
        } else {
            assertNull(perksDTO.getPrimeDiscount());
            assertNull(perksDTO.getPrimeDiscountPerPassenger());
        }
    }

    private void checkCurrency(Perks perksDTO) {
        if (Objects.nonNull(membershipPerks.getMembershipCoupon())) {
            assertEquals(perksDTO.getExtraPrimeCoupon().getCurrency(), seContext.getCurrency());
            assertEquals(perksDTO.getExtraPrimeCouponPerPassenger().getCurrency(), seContext.getCurrency());
        }
        if (Objects.nonNull(membershipPerks.getFee())) {
            assertEquals(perksDTO.getPrimeDiscount().getCurrency(), seContext.getCurrency());
            assertEquals(perksDTO.getPrimeDiscountPerPassenger().getCurrency(), seContext.getCurrency());
        }
    }
}
