package com.odigeo.frontend.resolvers.checkin.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odigeo.checkin.api.v1.model.response.AutomaticCheckInAvailability;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.testng.AssertJUnit.*;

public class CheckinAvailabilityMapperTest {

    private static final String AVAILABLE_JSON = "com/odigeo/frontend/resolvers/checkin/mapper/checkinAvailableResponse.json";
    private static final String NOT_AVAILABLE_JSON = "com/odigeo/frontend/resolvers/checkin/mapper/checkinNotAvailableResponse.json";


    private CheckinAvailabilityMapper mapper;

    @BeforeMethod
    public void init() {
        mapper = new CheckinAvailabilityMapper();
    }

    @Test
    public void testMapCheckinAvailableOk() throws IOException {
        AutomaticCheckInAvailability availabilityResponse = createAvailabilityResponse(AVAILABLE_JSON);
        CheckinAvailability availability = mapper.map(availabilityResponse);
        assertTrue(availability.getAvailable());

        CheckinWindow window = availability.getCheckinWindow();
        assertEquals(window.getClosingTimeInMinutes(), 135);
        assertEquals(window.getDataSyncOffsetInMinutes(), 0);
        assertEquals(window.getOpeningTimeInMinutes(), 1800);

        RequiredAdvancedPaxInfo paxInfo = availability.getPaxInfo();
        assertFalse(paxInfo.getGender());
        assertFalse(paxInfo.getLocale());
        assertFalse(paxInfo.getMobilePhone());
        assertFalse(paxInfo.getEmail());
        assertTrue(paxInfo.getDateOfBirth());
        assertTrue(paxInfo.getCitizenship());
        assertTrue(paxInfo.getCountryOfResidence());

        RequiredEmergencyContact contact = availability.getPaxInfo().getEmergencyContact();
        assertTrue(contact.getFullName());
        assertTrue(contact.getPhone());
        assertTrue(contact.getRelation());

        assertNull(availability.getNationalId());
        RequiredTravelDocument passport = availability.getPassport();
        assertEquals(passport.getType(), TravelDocumentType.PASSPORT);
        assertEquals(passport.getMonthsDocumentValidity(), 6);
        assertTrue(passport.getNumber());
        assertTrue(passport.getExpirationDate());
        assertTrue(passport.getIssuingCountry());
        assertFalse(passport.getIssueDate());

        RequiredCheckinAddress arrivalDetails = availability.getAddress();
        assertTrue(arrivalDetails.getState());
        assertTrue(arrivalDetails.getCity());
        assertTrue(arrivalDetails.getAddress());
        assertTrue(arrivalDetails.getZip());

        RequiredCheckinTravelPermissions permissions = availability.getTravelPermissions();
        assertTrue(permissions.getNoVisaReason());
        RequiredOnwardTicket ticket = permissions.getOnwardTicket();
        assertTrue(ticket.getDestination());
        assertTrue(ticket.getDate());
        assertTrue(ticket.getId());
        RequiredCheckinResidentPermit permit = permissions.getResidentPermit();
        assertTrue(permit.getExpirationDate());
        assertTrue(permit.getExpires());
        assertTrue(permit.getIssuingCountry());
        assertTrue(permit.getNumber());
        RequiredVisaDetails visa = permissions.getVisaDetails();
        assertTrue(visa.getNumber());
        assertTrue(visa.getExpirationDate());
        assertTrue(visa.getIssueCountry());
        assertTrue(visa.getIssueDate());
        assertArrayEquals(visa.getPossibleTypes().toArray(), new String[]{"tourist", "other"});
    }


    @Test
    public void testMapCheckinNotAvailableOk() throws IOException {
        AutomaticCheckInAvailability availabilityResponse = createAvailabilityResponse(NOT_AVAILABLE_JSON);
        CheckinAvailability availability = mapper.map(availabilityResponse);

        assertFalse(availability.getAvailable());
        assertNull(availability.getNationalId());
        assertNull(availability.getPassport());
        assertNull(availability.getTravelPermissions());
        assertNull(availability.getCheckinWindow());
        assertNull(availability.getPaxInfo());
        assertNull(availability.getAddress());
    }

    private AutomaticCheckInAvailability createAvailabilityResponse(String filePath) throws IOException {
        InputStream json = this.getClass().getClassLoader().getResourceAsStream(filePath);
        return new ObjectMapper().readValue(json, AutomaticCheckInAvailability.class);
    }
}
