package com.odigeo.frontend.resolvers.accommodation.search.handlers;


import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationSearchRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.RoomRequest;
import com.odigeo.accommodation.scoring.service.ScoringServiceException;
import com.odigeo.frontend.commons.exception.ServiceException;
import com.odigeo.frontend.commons.util.DateUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccommodationSearchRequestValidatorTest {

    @Mock
    private DateUtils dateUtils;
    @InjectMocks
    private AccommodationSearchRequestValidator testClass;


    @BeforeMethod
    public void init() throws ScoringServiceException {
        MockitoAnnotations.openMocks(this);
        when(dateUtils.convertFromIsoToLocalDate(anyString())).thenCallRealMethod();
    }

    @Test
    public void test() {
        AccommodationSearchRequest request = mock(AccommodationSearchRequest.class);
        RoomRequest roomRequest = mock(RoomRequest.class);
        when(request.getCheckInDate()).thenReturn("2022-12-01");
        when(request.getCheckOutDate()).thenReturn("2022-12-02");
        when(roomRequest.getNumAdults()).thenReturn(3);
        when(request.getRoomRequests()).thenReturn(Collections.singletonList(roomRequest));
        testClass.validateAccommodationSearchRequest(request);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testDateBefore() {
        AccommodationSearchRequest request = mock(AccommodationSearchRequest.class);
        RoomRequest roomRequest = mock(RoomRequest.class);
        when(request.getCheckInDate()).thenReturn("2022-12-01");
        when(request.getCheckOutDate()).thenReturn("2022-11-02");
        when(roomRequest.getNumAdults()).thenReturn(3);
        when(request.getRoomRequests()).thenReturn(Collections.singletonList(roomRequest));
        testClass.validateAccommodationSearchRequest(request);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testMoreThanMaxDays() {
        AccommodationSearchRequest request = mock(AccommodationSearchRequest.class);
        RoomRequest roomRequest = mock(RoomRequest.class);
        when(request.getCheckInDate()).thenReturn("2022-11-01");
        when(request.getCheckOutDate()).thenReturn("2022-12-30");
        when(roomRequest.getNumAdults()).thenReturn(3);
        when(request.getRoomRequests()).thenReturn(Collections.singletonList(roomRequest));
        testClass.validateAccommodationSearchRequest(request);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testMoreThanMaxRooms() {
        AccommodationSearchRequest request = mock(AccommodationSearchRequest.class);
        when(request.getCheckInDate()).thenReturn("2022-12-01");
        when(request.getCheckOutDate()).thenReturn("2022-12-30");
        List<RoomRequest> roomRequests = Stream.generate(this::generateRoomRequest).limit(31).collect(Collectors.toList());
        when(request.getRoomRequests()).thenReturn(roomRequests);
        testClass.validateAccommodationSearchRequest(request);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testMoreThanMaxAdultPerRoom() {
        AccommodationSearchRequest request = mock(AccommodationSearchRequest.class);
        when(request.getCheckInDate()).thenReturn("2022-12-01");
        when(request.getCheckOutDate()).thenReturn("2022-12-30");
        List<RoomRequest> roomRequests = Collections.singletonList(generateRoomRequest(20));
        when(request.getRoomRequests()).thenReturn(roomRequests);
        testClass.validateAccommodationSearchRequest(request);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testMoreThanMaxChildrenPerRoom() {
        AccommodationSearchRequest request = mock(AccommodationSearchRequest.class);
        when(request.getCheckInDate()).thenReturn("2022-12-01");
        when(request.getCheckOutDate()).thenReturn("2022-12-30");
        List<RoomRequest> roomRequests = Collections.singletonList(generateRoomRequest(5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11));
        when(request.getRoomRequests()).thenReturn(roomRequests);
        testClass.validateAccommodationSearchRequest(request);
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testChildrenOlderEnough() {
        AccommodationSearchRequest request = mock(AccommodationSearchRequest.class);
        when(request.getCheckInDate()).thenReturn("2022-12-01");
        when(request.getCheckOutDate()).thenReturn("2022-12-30");
        List<RoomRequest> roomRequests = Collections.singletonList(generateRoomRequest(5, 13));
        when(request.getRoomRequests()).thenReturn(roomRequests);
        testClass.validateAccommodationSearchRequest(request);
    }

    private RoomRequest generateRoomRequest() {
        return generateRoomRequest(1);
    }

    private RoomRequest generateRoomRequest(int nAdults, Integer... childAges) {
        RoomRequest roomRequest = mock(RoomRequest.class);
        when(roomRequest.getNumAdults()).thenReturn(nAdults);
        when(roomRequest.getChildrenAges()).thenReturn(Arrays.asList(childAges));
        return roomRequest;
    }

}
