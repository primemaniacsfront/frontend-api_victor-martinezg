package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering;

import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class ItinerarySteeringComparatorTest extends AirlineSteeringAbstractTest {

    @Test
    public void testOrderByRating() {

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("3", 0.1, AIREUROPA_AIRLINE, 0));
        itineraries.add(searchItineraryDTOMock("2", 0.2, AIREUROPA_AIRLINE, 0));
        itineraries.add(searchItineraryDTOMock("1", 0.3, QATAR_AIRLINE, 0));
        itineraries.add(searchItineraryDTOMock("0", 0.4, QATAR_AIRLINE, 0));

        launchTest(itineraries);

    }

    @Test
    public void testOrderByPrice() {

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("3", 0, AIREUROPA_AIRLINE, 4));
        itineraries.add(searchItineraryDTOMock("2", 0, AIREUROPA_AIRLINE, 3));
        itineraries.add(searchItineraryDTOMock("1", 0, QATAR_AIRLINE, 2));
        itineraries.add(searchItineraryDTOMock("0", 0, QATAR_AIRLINE, 1));

        launchTest(itineraries);

    }

    @Test
    public void testOrderByRatingAndPrice() {

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("3", 0.8, AIREUROPA_AIRLINE, 4));
        itineraries.add(searchItineraryDTOMock("2", 0.8, AIREUROPA_AIRLINE, 3));
        itineraries.add(searchItineraryDTOMock("1", 0.9, QATAR_AIRLINE, 2));
        itineraries.add(searchItineraryDTOMock("0", 0.9, QATAR_AIRLINE, 1));

        launchTest(itineraries);

    }

    @Test
    public void testOrderByRatingAndPriceAndNullRating() {

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("3", 0.8, AIREUROPA_AIRLINE, 4));
        itineraries.add(searchItineraryDTOMock("2", 0.8, AIREUROPA_AIRLINE, 3));
        itineraries.add(searchItineraryDTOMock("1", 0.9, QATAR_AIRLINE, 2));
        itineraries.add(searchItineraryDTOMock("4", 0.9, QATAR_AIRLINE, 2));
        itineraries.add(searchItineraryDTOMock("5", 0.8, AIREUROPA_AIRLINE, 4));
        itineraries.add(searchItineraryDTOMock("0", 0.9, QATAR_AIRLINE, 1));

        itineraries.get(3).setMeRating(null);
        itineraries.get(4).setMeRating(null);

        launchTest(itineraries);

    }

    @Test
    public void testOrderWhenMeRatingIsNull() {

        List<SearchItineraryDTO> itineraries = new ArrayList<>();
        itineraries.add(searchItineraryDTOMock("3", 0, AIREUROPA_AIRLINE, 4));
        itineraries.add(searchItineraryDTOMock("2", 0, AIREUROPA_AIRLINE, 3));
        itineraries.add(searchItineraryDTOMock("1", 0, QATAR_AIRLINE, 2));
        itineraries.add(searchItineraryDTOMock("0", 0, QATAR_AIRLINE, 1));
        itineraries.forEach(i -> i.setMeRating(null));

        launchTest(itineraries);

    }

    private void launchTest(List<SearchItineraryDTO> itineraries) {
        Collections.sort(itineraries, new ItinerarySteeringComparator(FEE_TYPE));

        for (int i = 0; i < itineraries.size(); i++) {
            assertEquals(String.valueOf(i), itineraries.get(i).getId());
        }
    }
}
