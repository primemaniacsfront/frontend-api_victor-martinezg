package com.odigeo.frontend.resolvers.search.handlers.campaign.airlinesteering;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.PaymentMethod;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;

import java.math.BigDecimal;
import java.util.ArrayList;

public abstract class AirlineSteeringAbstractTest {

    static final String QATAR_AIRLINE = "QR";
    static final String AIREUROPA_AIRLINE = "UX";

    static final FeeType FEE_TYPE = new FeeType("id", "name", PaymentMethod.CREDITCARD);

    protected SearchItineraryDTO searchItineraryDTOMock(String id, double meRating, String airline, float price) {
        SearchItineraryDTO searchItineraryDTO = new SearchItineraryDTO();
        searchItineraryDTO.setId(id);
        searchItineraryDTO.setMeRating(BigDecimal.valueOf(meRating));
        searchItineraryDTO.setLegs(new ArrayList<>());
        searchItineraryDTO.getLegs().add(new LegDTO());
        searchItineraryDTO.getLegs().get(0).setSegments(new ArrayList<>());
        searchItineraryDTO.getLegs().get(0).getSegments().add(new SegmentDTO());
        searchItineraryDTO.getLegs().get(0).getSegments().get(0).setSections(new ArrayList<>());
        searchItineraryDTO.getLegs().get(0).getSegments().get(0).getSections().add(new SectionDTO());
        searchItineraryDTO.getLegs().get(0).getSegments().get(0).getSections().get(0).setCarrier(new Carrier(airline, null));
        searchItineraryDTO.setFees(new ArrayList<>());
        searchItineraryDTO.getFees().add(new Fee(new Money(BigDecimal.valueOf(price), "currency"), FEE_TYPE));

        return searchItineraryDTO;
    }

}
