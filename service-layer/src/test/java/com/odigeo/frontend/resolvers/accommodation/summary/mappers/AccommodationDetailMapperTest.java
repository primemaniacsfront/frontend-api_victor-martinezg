package com.odigeo.frontend.resolvers.accommodation.summary.mappers;

import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationDealInformationMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationDetailMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationImagesMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationDetailDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.AccommodationRoomServiceDTO;
import com.odigeo.frontend.resolvers.accommodation.summary.models.FacilityGroupsDTO;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDealInformation;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationDetail;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationFacility;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationImage;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationRoomService;
import com.odigeo.searchengine.v2.responses.accommodation.AccommodationType;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class AccommodationDetailMapperTest {
    @Mock
    private AccommodationDealInformationMapper accommodationDealInformationMapper;
    @Mock
    private AccommodationFacilitiesMapper accommodationFacilitiesMapper;
    @Mock
    private AccommodationImagesMapper accommodationImagesMapper;
    @Mock
    private AccommodationRoomServicesMapper accommodationRoomServicesMapper;
    @InjectMocks
    private AccommodationDetailMapper mapper = new AccommodationDetailMapper();

    @BeforeClass
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testMap() {
        AccommodationDetail accommodationDetail = new AccommodationDetail();
        AccommodationDealInformation accommodationDealInformation = new AccommodationDealInformation();
        accommodationDetail.setAccommodationDealInformation(accommodationDealInformation);
        accommodationDetail.setStateProvince("state province");
        accommodationDetail.setCountry("country");
        accommodationDetail.setPostalCode("postal code");
        accommodationDetail.getMapUrls().add("map url");
        accommodationDetail.setLocationDescription("location description");
        accommodationDetail.setSurroundingAreaInfo("surrounding area info");
        accommodationDetail.setCheckInPolicy("checkIn policy");
        accommodationDetail.setCheckOutPolicy("checkOut policy");
        accommodationDetail.setHotelPolicy("hotel policy");
        accommodationDetail.getCreditCardTypes().add("VISA");
        accommodationDetail.setPaymentMethod("payment method");
        accommodationDetail.setPhoneNumber("phone number");
        accommodationDetail.setMail("mail");
        accommodationDetail.setWeb("web");
        accommodationDetail.setFax("fax");
        AccommodationImage accommodationImage = new AccommodationImage();
        accommodationDetail.getAccommodationImages().add(accommodationImage);
        AccommodationFacility accommodationFacility = new AccommodationFacility();
        accommodationDetail.getAccommodationFacilities().add(accommodationFacility);
        AccommodationRoomService accommodationRoomService = new AccommodationRoomService();
        accommodationDetail.getAccommodationRoomServices().add(accommodationRoomService);
        accommodationDetail.setNumberOfRooms("number of rooms");
        accommodationDetail.setHotelType(AccommodationType.HOTEL);

        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();
        AccommodationImageDTO accommodationImageDTO = new AccommodationImageDTO();
        AccommodationRoomServiceDTO accommodationRoomServiceDTO = new AccommodationRoomServiceDTO();
        List<FacilityGroupsDTO> facilityGroupsDTOS = new ArrayList<>();

        Mockito.when(accommodationDealInformationMapper.map(accommodationDealInformation)).thenReturn(accommodationDealInformationDTO);
        Mockito.when(accommodationImagesMapper.map(accommodationImage)).thenReturn(accommodationImageDTO);
        Mockito.when(accommodationFacilitiesMapper.mapFacilityGroups(anyList())).thenReturn(facilityGroupsDTOS);
        Mockito.when(accommodationRoomServicesMapper.map(accommodationRoomService)).thenReturn(accommodationRoomServiceDTO);

        AccommodationDetailDTO dto = mapper.map(accommodationDetail);

        assertSame(dto.getAccommodationDealInformation(), accommodationDealInformationDTO);
        assertEquals(dto.getStateProvince(), accommodationDetail.getStateProvince());
        assertEquals(dto.getCountry(), accommodationDetail.getCountry());
        assertEquals(dto.getPostalCode(), accommodationDetail.getPostalCode());
        assertEquals(dto.getMapUrls().size(), accommodationDetail.getMapUrls().size());
        assertEquals(dto.getMapUrls().get(0), accommodationDetail.getMapUrls().get(0));
        assertEquals(dto.getLocationDescription(), accommodationDetail.getLocationDescription());
        assertEquals(dto.getSurroundingAreaInfo(), accommodationDetail.getSurroundingAreaInfo());
        assertEquals(dto.getCheckInPolicy(), accommodationDetail.getCheckInPolicy());
        assertEquals(dto.getCheckOutPolicy(), accommodationDetail.getCheckOutPolicy());
        assertEquals(dto.getHotelPolicy(), accommodationDetail.getHotelPolicy());
        assertEquals(dto.getCreditCardTypes().size(), accommodationDetail.getCreditCardTypes().size());
        assertEquals(dto.getCreditCardTypes().get(0), accommodationDetail.getCreditCardTypes().get(0));
        assertEquals(dto.getPaymentMethod(), accommodationDetail.getPaymentMethod());
        assertEquals(dto.getPhoneNumber(), accommodationDetail.getPhoneNumber());
        assertEquals(dto.getMail(), accommodationDetail.getMail());
        assertEquals(dto.getWeb(), accommodationDetail.getWeb());
        assertEquals(dto.getFax(), accommodationDetail.getFax());
        assertEquals(dto.getAccommodationImages().size(), accommodationDetail.getAccommodationImages().size());
        assertSame(dto.getAccommodationImages().get(0), accommodationImageDTO);
        assertSame(dto.getFacilityGroups(), facilityGroupsDTOS);
        assertEquals(dto.getAccommodationRoomServices().size(), accommodationDetail.getAccommodationRoomServices().size());
        assertSame(dto.getAccommodationRoomServices().get(0), accommodationRoomServiceDTO);
        assertEquals(dto.getNumberOfRooms(), accommodationDetail.getNumberOfRooms());
        assertEquals(dto.getHotelType().name(), accommodationDetail.getHotelType().value());
    }

    @Test
    public void testMapNullValues() {
        AccommodationDetail accommodationDetail = new AccommodationDetail();
        AccommodationDealInformation accommodationDealInformation = new AccommodationDealInformation();
        accommodationDetail.setAccommodationDealInformation(accommodationDealInformation);
        AccommodationImage accommodationImage = new AccommodationImage();
        accommodationDetail.getAccommodationImages().add(accommodationImage);
        AccommodationFacility accommodationFacility = new AccommodationFacility();
        accommodationDetail.getAccommodationFacilities().add(accommodationFacility);
        AccommodationRoomService accommodationRoomService = new AccommodationRoomService();
        accommodationDetail.getAccommodationRoomServices().add(accommodationRoomService);

        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();
        AccommodationImageDTO accommodationImageDTO = new AccommodationImageDTO();
        AccommodationRoomServiceDTO accommodationRoomServiceDTO = new AccommodationRoomServiceDTO();

        Mockito.when(accommodationDealInformationMapper.map(accommodationDealInformation)).thenReturn(accommodationDealInformationDTO);
        Mockito.when(accommodationImagesMapper.map(accommodationImage)).thenReturn(accommodationImageDTO);
        Mockito.when(accommodationRoomServicesMapper.map(accommodationRoomService)).thenReturn(accommodationRoomServiceDTO);

        AccommodationDetailDTO dto = mapper.map(accommodationDetail);

        assertSame(dto.getAccommodationDealInformation(), accommodationDealInformationDTO);
        assertEquals(dto.getStateProvince(), accommodationDetail.getStateProvince());
        assertEquals(dto.getCountry(), accommodationDetail.getCountry());
        assertEquals(dto.getPostalCode(), accommodationDetail.getPostalCode());
        assertTrue(dto.getMapUrls().isEmpty());
        assertEquals(dto.getLocationDescription(), accommodationDetail.getLocationDescription());
        assertEquals(dto.getSurroundingAreaInfo(), accommodationDetail.getSurroundingAreaInfo());
        assertEquals(dto.getCheckInPolicy(), accommodationDetail.getCheckInPolicy());
        assertEquals(dto.getCheckOutPolicy(), accommodationDetail.getCheckOutPolicy());
        assertEquals(dto.getHotelPolicy(), accommodationDetail.getHotelPolicy());
        assertTrue(dto.getCreditCardTypes().isEmpty());
        assertEquals(dto.getPaymentMethod(), accommodationDetail.getPaymentMethod());
        assertEquals(dto.getPhoneNumber(), accommodationDetail.getPhoneNumber());
        assertEquals(dto.getMail(), accommodationDetail.getMail());
        assertEquals(dto.getWeb(), accommodationDetail.getWeb());
        assertEquals(dto.getFax(), accommodationDetail.getFax());
        assertEquals(dto.getAccommodationImages().size(), accommodationDetail.getAccommodationImages().size());
        assertSame(dto.getAccommodationImages().get(0), accommodationImageDTO);
        assertTrue(dto.getFacilityGroups().isEmpty());
        assertEquals(dto.getAccommodationRoomServices().size(), accommodationDetail.getAccommodationRoomServices().size());
        assertSame(dto.getAccommodationRoomServices().get(0), accommodationRoomServiceDTO);
        assertEquals(dto.getNumberOfRooms(), accommodationDetail.getNumberOfRooms());
        assertEquals(dto.getHotelType(), accommodationDetail.getHotelType());
    }

}
