package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CarbonFootprint;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.monitoring.MeasureConstants;
import com.odigeo.frontend.monitoring.MetricsHandler;
import com.odigeo.frontend.resolvers.search.handlers.SearchDebugHandler;
import com.odigeo.frontend.resolvers.search.models.SearchItineraryDTO;
import com.odigeo.frontend.resolvers.search.models.SearchResponseDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CarbonFootprintHandlerTest {

    @Mock
    private SearchDebugHandler debugHandler;
    @Mock
    private SearchResponseDTO searchResponse;

    @Mock
    private MetricsHandler metricsHandler;
    @Mock
    private ResolverContext context;
    @Mock
    private VisitInformation visit;

    @InjectMocks
    private CarbonFootprintHandler handler;

    private CarbonFootprintResponse carbonFootprintResponse;

    private List<SearchItineraryDTO> itineraries;

    private static final BigDecimal AVERAGE_KILOS = BigDecimal.TEN;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(context.getVisitInformation()).thenReturn(visit);
        carbonFootprintResponse = getCarbonFootprintResponse(true);
    }

    @Test
    public void testZeroKilos() {
        handler.addCarbonFootprintInfo(new CarbonFootprintResponse(), searchResponse, context);
        verifyNoInteractions(metricsHandler);
    }

    @Test
    public void testPopulateResponse() {
        SearchResponseDTO searchResponse = new SearchResponseDTO();
        searchResponse.setItineraries(initSearchItineraryDTOList());
        handler.addCarbonFootprintInfo(carbonFootprintResponse, searchResponse, context);

        assertNotNull(searchResponse);
        assertNotNull(searchResponse.getItineraries());
        assertEquals(searchResponse.getItineraries().size(), 10);
        assertEquals(searchResponse.getItineraries().stream().map(SearchItineraryDTO::getCarbonFootprint).filter(Objects::isNull).count(), 9);
        assertEquals(searchResponse.getItineraries().stream().map(SearchItineraryDTO::getCarbonFootprint).filter(Objects::nonNull).count(), 1);
        verify(metricsHandler).trackCounter(MeasureConstants.SEARCH_STATIC_FOOTPRINT);
    }

    @Test
    public void testAddDebugInfo() {
        initMocks();

        handler.addCarbonFootprintInfo(carbonFootprintResponse, searchResponse, context);

        verify(debugHandler).addQaModeCarbonFootprintInfo(context, itineraries, AVERAGE_KILOS);
    }

    @Test
    public void testAddMetric() {
        launchTestMetric(true);
        verify(metricsHandler).trackCounter(MeasureConstants.SEARCH_STATIC_FOOTPRINT);
    }

    @Test
    public void testNotAddMetric() {
        launchTestMetric(false);
        verifyNoInteractions(metricsHandler);
    }

    private void launchTestMetric(boolean isEco) {
        SearchResponseDTO searchResponse = new SearchResponseDTO();
        searchResponse.setItineraries(initSearchItineraryDTOList());
        handler.addCarbonFootprintInfo(getCarbonFootprintResponse(isEco), searchResponse, context);
    }

    private void initMocks() {
        String visitCode = "visit";
        when(visit.getVisitCode()).thenReturn(visitCode);

        itineraries = Collections.singletonList(mock(SearchItineraryDTO.class));
        when(searchResponse.getItineraries()).thenReturn(itineraries);

    }

    private List<SearchItineraryDTO> initSearchItineraryDTOList() {
        List<SearchItineraryDTO> searchItineraryDTOList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            searchItineraryDTOList.add(new SearchItineraryDTO());
        }
        return searchItineraryDTOList;

    }

    private CarbonFootprint getCarbonFootprint(boolean isEco) {
        CarbonFootprint carbonFootprint = new CarbonFootprint();
        carbonFootprint.setIsEco(isEco);
        carbonFootprint.setEcoPercentageThanAverage(80);
        carbonFootprint.setTotalCo2eKilos(BigDecimal.TEN);
        carbonFootprint.setTotalCo2eKilos(BigDecimal.ONE);
        return carbonFootprint;
    }

    private CarbonFootprintResponse getCarbonFootprintResponse(boolean isEco) {
        return new CarbonFootprintResponse(Collections.singletonMap(0, getCarbonFootprint(isEco)), AVERAGE_KILOS);
    }
}
