package com.odigeo.frontend.resolvers.accommodation.commons.handlers;

import com.odigeo.accommodation.scoring.service.ScoringServiceException;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;
import com.odigeo.frontend.resolvers.accommodation.search.mappers.HotelDetailResponseMapper;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationProviderKeyDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.AccommodationSearchResponseDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.model.AccommodationStaticContentDTO;
import com.odigeo.frontend.resolvers.accommodation.staticcontent.model.AccommodationStaticContentResponseDTO;
import com.odigeo.frontend.services.GeoService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AccommodationStaticContentHandlerTest {

    @Mock
    private AccommodationDataHandler accommodationDataHandler;
    @Mock
    private HotelDetailResponseMapper hotelDetailResponseMapper;
    @Mock
    private ContentKeyMapper contentKeyMapper;
    @Mock
    private GeoService geoService;
    @Mock
    private VisitInformation visit;


    @InjectMocks
    private AccommodationStaticContentHandler testClass;


    @BeforeMethod
    public void init() throws ScoringServiceException {
        MockitoAnnotations.openMocks(this);
    }

    private AccommodationSearchResponseDTO getAccommodationSearchResponseDTO() {
        AccommodationSearchResponseDTO response = new AccommodationSearchResponseDTO();
        AccommodationResponseDTO accommodationResponse = new AccommodationResponseDTO();
        SearchAccommodationDTO searchAccommodationDTO = new SearchAccommodationDTO();
        AccommodationDealInformationDTO accommodationDeal = new AccommodationDealInformationDTO();
        accommodationDeal.setCoordinates(new GeoCoordinatesDTO());
        searchAccommodationDTO.setAccommodationDeal(accommodationDeal);
        accommodationResponse.setAccommodations(Collections.singletonList(searchAccommodationDTO));
        response.setAccommodationResponse(accommodationResponse);
        return response;
    }

    @Test
    public void testPopulateSearchAccommodationStaticContent() {
        AccommodationSearchResponseDTO response = getAccommodationSearchResponseDTO();
        testClass.populateSearchAccommodationStaticContent(visit, response);
        verify(accommodationDataHandler).getAccommodationSummary(any(), any());
        verify(hotelDetailResponseMapper).remapSummaryToAccommodation(any(), anyList());
    }

    @Test
    public void testRetrieveStaticContent() {
        AccommodationStaticContentDTO accommodationStaticContentDTO = new AccommodationStaticContentDTO();
        when(hotelDetailResponseMapper.mapSummaries(any())).thenReturn(Collections.singletonList(accommodationStaticContentDTO));
        AccommodationStaticContentResponseDTO accommodationStaticContentResponseDTO = testClass.retrieveStaticContent(visit, Collections.singletonList(new AccommodationProviderKeyDTO("a", "b")));
        assertNotNull(accommodationStaticContentResponseDTO.getAccommodationsStaticContent());
        AccommodationStaticContentDTO response = accommodationStaticContentResponseDTO.getAccommodationsStaticContent().get(0);
        assertNotNull(response);
        assertEquals(response, accommodationStaticContentDTO);
    }

}
