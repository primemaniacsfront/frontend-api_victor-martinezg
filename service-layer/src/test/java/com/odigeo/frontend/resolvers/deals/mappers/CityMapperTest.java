package com.odigeo.frontend.resolvers.deals.mappers;

import com.odigeo.geoapi.v4.responses.City;
import com.odigeo.geoapi.v4.utils.LocalizedText;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Locale;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CityMapperTest {

    private static final String IATA = "ARR";
    private static final String NAME = "Destination";

    @Mock
    private City destination;
    @Mock
    private LocalizedText destinationName;

    private final CityMapper cityMapper = new CityMapper();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testMapModelToContract() {
        when(destination.getIataCode()).thenReturn(IATA);
        when(destination.getCity()).thenReturn(destination);
        when(destinationName.getText(Locale.ENGLISH)).thenReturn(NAME);
        when(destination.getName()).thenReturn(destinationName);

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.City city =
            cityMapper.map(destination, Locale.ENGLISH);

        assertEquals(city.getIata(), IATA);
        assertEquals(city.getName(), NAME);
    }

    @Test
    public void testMapModelToContractNullCity() {
        when(destination.getIataCode()).thenReturn(IATA);
        when(destination.getCity()).thenReturn(destination);
        when(destination.getName()).thenReturn(destinationName);

        com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.City city =
                cityMapper.map(destination, Locale.ENGLISH);

        assertEquals(city.getIata(), IATA);
        assertEquals(city.getName(), null);
    }

}
