package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ThreeValuedLogic;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationImageDTO;
import com.odigeo.frontend.resolvers.accommodation.commons.model.GeoCoordinatesDTO;

public class AccommodationDealInformationDTOTest extends BeanTest<AccommodationDealInformationDTO> {

    public AccommodationDealInformationDTO getBean() {
        AccommodationDealInformationDTO bean = new AccommodationDealInformationDTO();
        bean.setName("name");
        bean.setAddress("address");
        bean.setCityName("cityName");
        bean.setDescription("description");
        bean.setLocation("location");
        bean.setChain("chain");
        bean.setType(AccommodationType.UNKNOWN);
        bean.setMainAccommodationImage(new AccommodationImageDTO());
        bean.setCancellationPolicyDescription("cancellationPolicyDescription");
        bean.setCoordinates(new GeoCoordinatesDTO());
        bean.setCategory(AccommodationCategory.UNKNOWN);
        bean.setCancellationFree(ThreeValuedLogic.UNKNOWN);
        bean.setPaymentAtDestination(false);

        return bean;
    }
}
