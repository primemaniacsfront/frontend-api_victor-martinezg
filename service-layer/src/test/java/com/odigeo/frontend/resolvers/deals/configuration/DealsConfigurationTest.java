package com.odigeo.frontend.resolvers.deals.configuration;

import bean.test.BeanTest;

import java.util.Collections;

public class DealsConfigurationTest extends BeanTest<DealsConfiguration> {

    @Override
    protected DealsConfiguration getBean() {
        DealsConfiguration bean = new DealsConfiguration();
        bean.setMaxPopularDestinations(0);
        bean.setAggregateBy("AGGREGATE_BY");
        bean.setMaxDistanceInKm(0D);
        bean.setSize(0);
        bean.setMaxSearchAgeDays(0);
        bean.setTripType("TRIP_TYPE");
        bean.setPriceType("PRICE_TYPE");
        bean.setBaseDestinations(Collections.emptyList());
        return bean;
    }

}
