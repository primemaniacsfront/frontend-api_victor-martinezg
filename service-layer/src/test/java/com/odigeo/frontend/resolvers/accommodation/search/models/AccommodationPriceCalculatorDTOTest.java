package com.odigeo.frontend.resolvers.accommodation.search.models;

import bean.test.BeanTest;

import java.math.BigDecimal;

public class AccommodationPriceCalculatorDTOTest extends BeanTest<AccommodationPriceCalculatorDTO> {

    @Override
    protected AccommodationPriceCalculatorDTO getBean() {
        AccommodationPriceCalculatorDTO bean = new AccommodationPriceCalculatorDTO();
        bean.setFeeInfo(new FeeInfoDTO());
        bean.setMarkup(BigDecimal.TEN);
        bean.setSortPrice(BigDecimal.TEN);
        return bean;
    }
}
