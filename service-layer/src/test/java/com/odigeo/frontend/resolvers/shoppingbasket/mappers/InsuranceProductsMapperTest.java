package com.odigeo.frontend.resolvers.shoppingbasket.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsuranceProduct;
import com.google.inject.Singleton;
import com.odigeo.shoppingbasket.v2.model.Product;
import com.odigeo.shoppingbasket.v2.model.ShoppingBasket;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

@Singleton
public class InsuranceProductsMapperTest {

    private static final InsuranceProduct INSURANCE_PRODUCT = new InsuranceProduct();

    @Mock
    private InsuranceProductMapper insuranceProductMapper;

    @InjectMocks
    private InsuranceProductsMapper insuranceProductsMapper;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(insuranceProductMapper.map(Mockito.any())).thenReturn(INSURANCE_PRODUCT);
    }

    @Test
    public void map() {
        ShoppingBasket shoppingBasket = new ShoppingBasket();
        shoppingBasket.setProducts(Arrays.asList(new Product(), new Product()));

        List<InsuranceProduct> insuranceProducts = insuranceProductsMapper.map(shoppingBasket);

        assertEquals(insuranceProducts.size(), shoppingBasket.getProducts().size());
        assertEquals(insuranceProducts.get(0), INSURANCE_PRODUCT);
    }
}
