package com.odigeo.frontend.resolvers.accommodation.rooms.models;

import bean.test.BeanTest;

import java.util.Collections;

public class AccommodationRoomsResponseDTOTest  extends BeanTest<AccommodationRoomsResponseDTO> {

    @Override
    protected AccommodationRoomsResponseDTO getBean() {
        AccommodationRoomsResponseDTO bean = new AccommodationRoomsResponseDTO();
        bean.setRooms(Collections.singletonList(new RoomDealDTO()));
        bean.setCurrency("EUR");
        bean.setSearchId(123L);
        return bean;
    }
}
