package com.odigeo.frontend.resolvers.accommodation.product.handlers.productapi;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectRoomOfferResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.services.accommodation.AccommodationOfferService;
import graphql.GraphQLContext;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AccommodationProductSelectRoomOfferTest {

    @Mock
    private AccommodationOfferService accommodationOfferService;
    @Mock
    private ResolverContext context;
    @Mock
    private GraphQLContext gqlContext;

    @InjectMocks
    private AccommodationProductSelectRoomOffer testClass;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    public void test() {
        when(accommodationOfferService.checkAvailability("123", "1", "1")).thenReturn("dealId");
        SelectRoomOfferResponse offerResponse = testClass.selectRoomOffer("123", "1", "1", context, gqlContext);
        assertNotNull(offerResponse);
        assertEquals("dealId", offerResponse.getDealId());
    }


}
