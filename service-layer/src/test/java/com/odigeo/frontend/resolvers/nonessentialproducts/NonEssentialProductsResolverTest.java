package com.odigeo.frontend.resolvers.nonessentialproducts;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductOffer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsIdRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsOfferResponse;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductId;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductSelection;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ProductType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectNonEssentialProductsRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.SelectNonEssentialProductsResponse;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.commons.util.JsonUtils;
import com.odigeo.frontend.resolvers.nonessentialproducts.handlers.NonEssentialProductsHandler;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.apache.commons.collections4.CollectionUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class NonEssentialProductsResolverTest {

    private static final long BOOKING_ID = 1L;
    private static final String GET_AVAILABLE_NON_ESSENTIAL_PRODUCTS_QUERY = "getAvailableNonEssentialProducts";
    private static final String SELECT_NON_ESSENTIAL_PRODUCTS_MUTATION = "selectNonEssentialProducts";
    private static final String QUERY = "Query";
    private static final String MUTATION = "Mutation";
    private static final String POLICY = "POLICY";

    @Mock
    private NonEssentialProductsHandler nonEssentialProductsHandler;
    @Mock
    private JsonUtils jsonUtils;
    @Mock
    private DataFetchingEnvironment dataFetchingEnvironment;
    @Mock
    private NonEssentialProductsRequest nonEssentialProductsRequest;
    @Mock
    private ResolverContext context;
    @InjectMocks
    private NonEssentialProductsResolver nonEssentialProductsResolver;
    @Mock
    private SelectNonEssentialProductsRequest selectNonEssentialProductsRequest;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        when(dataFetchingEnvironment.getContext()).thenReturn(context);
        VisitInformation visitInformation = mock(VisitInformation.class);
        when(context.getVisitInformation()).thenReturn(visitInformation);
        when(nonEssentialProductsRequest.getBookingId()).thenReturn(BOOKING_ID);
    }

    @Test
    public void addToBuilderQueries() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        nonEssentialProductsResolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(QUERY);
        Map<String, DataFetcher> mutations = runtimeWiring.getDataFetchers().get(MUTATION);

        assertNotNull(queries.get(GET_AVAILABLE_NON_ESSENTIAL_PRODUCTS_QUERY));
        assertNotNull(mutations.get(SELECT_NON_ESSENTIAL_PRODUCTS_MUTATION));
    }

    @Test
    public void testFilteredResults() {
        when(jsonUtils.fromDataFetching(dataFetchingEnvironment, NonEssentialProductsRequest.class))
            .thenReturn(nonEssentialProductsRequest);
        setPolicyFilter(POLICY);
        NonEssentialProductsOfferResponse nonEssentialProductsOfferResponse = getNonEssentialProductsOfferResponse();
        when(nonEssentialProductsHandler.getAllNonEssentialProductsOffers(BOOKING_ID, dataFetchingEnvironment.getContext()))
                .thenReturn(nonEssentialProductsOfferResponse);

        NonEssentialProductsOfferResponse response = nonEssentialProductsResolver.nonEssentialProductsOffersFetcher(dataFetchingEnvironment);

        assertEquals(response, nonEssentialProductsOfferResponse);
        assertTrue(CollectionUtils.isNotEmpty(nonEssentialProductsOfferResponse.getGuarantees()));
        assertTrue(CollectionUtils.isNotEmpty(nonEssentialProductsOfferResponse.getRebooking()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getServiceOptions()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getInsurances()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getAdditions()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getFareRules()));
    }

    @Test
    public void testNotFilteredResults() {
        when(jsonUtils.fromDataFetching(dataFetchingEnvironment, NonEssentialProductsRequest.class))
            .thenReturn(nonEssentialProductsRequest);
        setPolicyFilter("A");
        NonEssentialProductsOfferResponse nonEssentialProductsOfferResponse = getNonEssentialProductsOfferResponse();
        when(nonEssentialProductsHandler.getAllNonEssentialProductsOffers(BOOKING_ID, dataFetchingEnvironment.getContext()))
            .thenReturn(nonEssentialProductsOfferResponse);

        NonEssentialProductsOfferResponse response = nonEssentialProductsResolver.nonEssentialProductsOffersFetcher(dataFetchingEnvironment);

        assertEquals(response, nonEssentialProductsOfferResponse);
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getGuarantees()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getRebooking()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getServiceOptions()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getInsurances()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getAdditions()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getFareRules()));
    }

    @Test
    public void testNotFilterResults() {
        when(jsonUtils.fromDataFetching(dataFetchingEnvironment, NonEssentialProductsRequest.class))
                .thenReturn(nonEssentialProductsRequest);
        NonEssentialProductsOfferResponse nonEssentialProductsOfferResponse = getNonEssentialProductsOfferResponse();
        when(nonEssentialProductsHandler.getAllNonEssentialProductsOffers(BOOKING_ID, dataFetchingEnvironment.getContext()))
                .thenReturn(nonEssentialProductsOfferResponse);

        NonEssentialProductsOfferResponse response = nonEssentialProductsResolver.nonEssentialProductsOffersFetcher(dataFetchingEnvironment);

        assertEquals(response, nonEssentialProductsOfferResponse);
        assertTrue(CollectionUtils.isNotEmpty(nonEssentialProductsOfferResponse.getGuarantees()));
        assertTrue(CollectionUtils.isNotEmpty(nonEssentialProductsOfferResponse.getRebooking()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getServiceOptions()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getInsurances()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getAdditions()));
        assertTrue(CollectionUtils.isEmpty(nonEssentialProductsOfferResponse.getFareRules()));
    }

    @Test
    public void selectNonEssentialProductsTest() {
        when(jsonUtils.fromDataFetching(dataFetchingEnvironment, SelectNonEssentialProductsRequest.class))
                .thenReturn(selectNonEssentialProductsRequest);

        List<ProductSelection> productSelections = Collections.singletonList(new ProductSelection(POLICY));
        when(selectNonEssentialProductsRequest.getProducts()).thenReturn(productSelections);

        ProductId productId = new ProductId(POLICY, ProductType.INSURANCE);
        SelectNonEssentialProductsResponse selectNonEssentialProductsResponse = new SelectNonEssentialProductsResponse(Collections.singletonList(productId));

        when(nonEssentialProductsHandler.getSelectNonEssentialProductsResponse(Mockito.any())).thenReturn(selectNonEssentialProductsResponse);

        SelectNonEssentialProductsResponse response = nonEssentialProductsResolver.selectNonEssentialProducts(dataFetchingEnvironment);
        assertEquals(selectNonEssentialProductsResponse, response);
        assertEquals(response.getProductId().size(), 1);
        assertEquals(response.getProductId().get(0).getId(), POLICY);
        assertEquals(ProductType.INSURANCE, response.getProductId().get(0).getType());
    }

    private void setPolicyFilter(String filter) {
        NonEssentialProductsIdRequest nonEssentialProductsIdRequest = new NonEssentialProductsIdRequest();
        nonEssentialProductsIdRequest.setId(filter);
        when(nonEssentialProductsRequest.getProducts()).thenReturn(Arrays.asList(nonEssentialProductsIdRequest));
    }

    private NonEssentialProductsOfferResponse getNonEssentialProductsOfferResponse() {
        NonEssentialProductsOfferResponse nonEssentialProductsOfferResponse = new NonEssentialProductsOfferResponse();

        NonEssentialProductOffer nonEssentialProductOffer = new NonEssentialProductOffer();
        nonEssentialProductOffer.setProduct(getNonEssentialProduct());
        nonEssentialProductsOfferResponse.setGuarantees(Arrays.asList(nonEssentialProductOffer));
        nonEssentialProductsOfferResponse.setRebooking(Arrays.asList(nonEssentialProductOffer));

        return nonEssentialProductsOfferResponse;
    }

    private NonEssentialProduct getNonEssentialProduct() {
        NonEssentialProduct nonEssentialProduct = new NonEssentialProduct();
        nonEssentialProduct.setOfferId(POLICY);
        return nonEssentialProduct;
    }

}
