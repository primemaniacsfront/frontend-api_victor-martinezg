package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;

public class AccommodationRoomServiceDTOTest extends BeanTest<AccommodationRoomServiceDTO> {

    public AccommodationRoomServiceDTO getBean() {
        AccommodationRoomServiceDTO bean = new AccommodationRoomServiceDTO();
        bean.setType("type");
        bean.setDescription("description");

        return bean;
    }
}
