package com.odigeo.frontend.resolvers.search.handlers.legend;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.odigeo.frontend.contract.itinerary.leg.LegDTO;
import com.odigeo.frontend.contract.itinerary.section.SectionDTO;
import com.odigeo.frontend.contract.itinerary.segment.SegmentDTO;
import com.odigeo.frontend.contract.location.LocationDTO;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class TransportHandlerTest {

    @Mock
    private LocationDTO departure;
    @Mock
    private LocationDTO destination;

    private TransportHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new TransportHandler();
    }

    @Test
    public void testCalculateSectionTransportTypePlane() {
        when(departure.getLocationType()).thenReturn(LocationType.AIRPORT);
        when(destination.getLocationType()).thenReturn(LocationType.AIRPORT);

        assertSame(handler.calculateSectionTransportType(departure, destination), TransportType.PLANE);
    }

    @Test
    public void testCalculateSectionTransportTypeTrainDestination() {
        when(departure.getLocationType()).thenReturn(LocationType.AIRPORT);
        when(destination.getLocationType()).thenReturn(LocationType.TRAIN_STATION);

        assertSame(handler.calculateSectionTransportType(departure, destination), TransportType.TRAIN);
    }

    @Test
    public void testCalculateSectionTransportTypeTrainDeparture() {
        when(departure.getLocationType()).thenReturn(LocationType.TRAIN_STATION);
        when(destination.getLocationType()).thenReturn(LocationType.AIRPORT);

        assertSame(handler.calculateSectionTransportType(departure, destination), TransportType.TRAIN);
    }

    @Test
    public void testCalculateSegmentTransportTypes() {
        SectionDTO section1 = mock(SectionDTO.class);
        SectionDTO section2 = mock(SectionDTO.class);
        SectionDTO section3 = mock(SectionDTO.class);
        TransportType type1 = mock(TransportType.class);
        TransportType type2 = mock(TransportType.class);

        List<SectionDTO> segmentSections = Arrays.asList(section1, section2, section3);

        when(section1.getTransportType()).thenReturn(type1);
        when(section2.getTransportType()).thenReturn(type2);
        when(section3.getTransportType()).thenReturn(type1);

        Set<TransportType> transportTypes = handler.calculateSegmentTransportTypes(segmentSections);

        assertEquals(transportTypes.size(), 2);
        assertTrue(transportTypes.contains(type1));
        assertTrue(transportTypes.contains(type2));
    }

    @Test
    public void testCalculateItineraryTransportTypes() {
        LegDTO leg = mock(LegDTO.class);
        SegmentDTO segment1 = mock(SegmentDTO.class);
        SegmentDTO segment2 = mock(SegmentDTO.class);
        Set<TransportType> segmentTransportTypes1 = new HashSet<>(Arrays.asList(TransportType.PLANE));
        Set<TransportType> segmentTransportTypes2 = new HashSet<>(Arrays.asList(TransportType.TRAIN));

        List<LegDTO> itineraryLegs = Arrays.asList(leg);

        when(segment1.getTransportTypes()).thenReturn(segmentTransportTypes1);
        when(segment2.getTransportTypes()).thenReturn(segmentTransportTypes2);
        when(leg.getSegments()).thenReturn(Arrays.asList(segment1, segment2));

        Set<TransportType> transportTypes = handler.calculateItineraryTransportTypes(itineraryLegs);

        assertEquals(transportTypes.size(), 2);
        assertTrue(transportTypes.contains(TransportType.PLANE));
        assertTrue(transportTypes.contains(TransportType.TRAIN));
    }

}
