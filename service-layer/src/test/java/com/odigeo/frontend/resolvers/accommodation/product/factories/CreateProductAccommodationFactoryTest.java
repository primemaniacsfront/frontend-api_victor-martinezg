package com.odigeo.frontend.resolvers.accommodation.product.factories;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ShoppingType;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.CreateProductAccommodationHandler;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.dapi.ShoppingCartCreateProductAccommodation;
import com.odigeo.frontend.resolvers.accommodation.product.handlers.productapi.CreateAccommodationProduct;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CreateProductAccommodationFactoryTest {


    @Mock
    private ShoppingCartCreateProductAccommodation shoppingCartCreateProductAccommodation;
    @Mock
    private CreateAccommodationProduct createAccommodationProduct;

    @InjectMocks
    private CreateProductAccommodationFactory testClass;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetInstance() {
        CreateProductAccommodationHandler instanceV3 = testClass.getInstance(ShoppingType.BASKET_V3);
        assertEquals(createAccommodationProduct, instanceV3);

        CreateProductAccommodationHandler instanceV2 = testClass.getInstance(ShoppingType.BASKET_V2);
        assertEquals(shoppingCartCreateProductAccommodation, instanceV2);

        CreateProductAccommodationHandler defaultInstance = testClass.getInstance(null);
        assertEquals(shoppingCartCreateProductAccommodation, defaultInstance);
    }

}
