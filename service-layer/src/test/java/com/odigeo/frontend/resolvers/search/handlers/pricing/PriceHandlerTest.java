package com.odigeo.frontend.resolvers.search.handlers.pricing;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ExternalSelectionRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Fee;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.FeeType;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.ItineraryRequest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Money;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.common.ItinerarySortCriteria;
import com.odigeo.searchengine.v2.requests.ItinerarySearchRequest;
import com.odigeo.searchengine.v2.responses.itinerary.FareItinerary;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class PriceHandlerTest {

    private static final BigDecimal NUM_PASSENGERS = BigDecimal.ONE;
    private static final String CURRENCY_CODE = "USD";

    @Mock
    private FareItinerary fareItinerary;
    @Mock
    private SearchEngineContext seContext;
    @Mock
    private ResolverContext context;
    @Mock
    private MinPricePolicy minPricePolicy;
    @Mock
    private PrimePricePolicy primePricePolicy;
    @Mock
    private ItinerarySearchRequest itineraryRequest;
    @Mock
    private ItineraryRequest itineraryRequestDTO;

    @InjectMocks
    private PriceHandler priceHandler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCalculateItineraryPricesMinPolicy() {
        testCalculateItineraryPrices(false);
    }

    @Test
    public void testCalculateItineraryPricesMemberPolicy() {
        testCalculateItineraryPrices(true);
    }

    private void testCalculateItineraryPrices(boolean isPrimeMarket) {
        AbstractPricePolicy policy = isPrimeMarket ? primePricePolicy : minPricePolicy;
        spy(policy);
        List<Fee> fees = Stream.of(buildDefaultFee(), buildDefaultFee()).collect(Collectors.toList());

        when(policy.calculateFees(seContext, fareItinerary)).thenReturn(fees);
        when(seContext.getCurrency()).thenReturn(CURRENCY_CODE);

        assertSame(priceHandler.calculateItineraryPrices(seContext, fareItinerary, NUM_PASSENGERS, policy), fees);
        verify(policy, times(1)).populatePricePerPassenger(fees.get(0), NUM_PASSENGERS);
        verify(policy, times(1)).populatePricePerPassenger(fees.get(1), NUM_PASSENGERS);
        verify(policy, times(1)).populateCurrency(fees.get(0), CURRENCY_CODE);
        verify(policy, times(1)).populateCurrency(fees.get(1), CURRENCY_CODE);
    }

    @Test
    public void testPopulatePricePolicy() {
        checkPricePolicy(false, ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
    }

    @Test
    public void testPopulatePricePolicyPrime() {
        checkPricePolicy(true, ItinerarySortCriteria.MINIMUM_PURCHASABLE_MEMBERSHIP_PRICE);
    }

    @Test
    public void testPopulatePricePolicyMeta() {
        when(itineraryRequestDTO.getExternalSelection()).thenReturn(mock(ExternalSelectionRequest.class));
        checkPricePolicy(true, ItinerarySortCriteria.MINIMUM_PURCHASABLE_PRICE);
        verify(itineraryRequest).setMembershipFee(true);
    }

    private void checkPricePolicy(boolean isPrimeMarket, ItinerarySortCriteria sortCriteria) {
        priceHandler.populatePricePolicy(itineraryRequest, itineraryRequestDTO, isPrimeMarket);
        verify(itineraryRequest).setSortCriteria(sortCriteria);
    }

    private Fee buildDefaultFee() {
        return new Fee(new Money(BigDecimal.TEN, "EUR"), new FeeType());
    }

}
