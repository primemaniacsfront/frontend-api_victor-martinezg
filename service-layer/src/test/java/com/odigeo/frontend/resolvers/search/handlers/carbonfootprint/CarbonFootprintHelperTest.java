package com.odigeo.frontend.resolvers.search.handlers.carbonfootprint;

import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.model.ItineraryCo2FootprintResult;
import com.edreamsodigeo.itinerary.itineraryfootprintcalculator.api.contract.response.ItineraryFootprintCalculatorResponse;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class CarbonFootprintHelperTest {

    private CarbonFootprintHelper helper;

    @Mock
    private ItineraryFootprintCalculatorResponse response;

    @BeforeMethod
    public void init() {
        helper = new CarbonFootprintHelper();
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetSearchKilosAverageWhitNull() {
        List<ItineraryCo2FootprintResult> itineraries = new ArrayList();
        Collections.addAll(itineraries,
                initItinerary(0, BigDecimal.valueOf(1)),
                initItinerary(3, BigDecimal.valueOf(3)),
                initItinerary(null, null));
        when(response.getItineraryCo2FootprintResults()).thenReturn(itineraries);

        assertEquals(helper.getSearchKilosAverage(response).doubleValue(), 2D);
    }

    @Test
    public void testGetSearchKilosAverage() {
        List<ItineraryCo2FootprintResult> itineraries = new ArrayList();
        Collections.addAll(itineraries,
                initItinerary(0, BigDecimal.valueOf(1)),
                initItinerary(2, BigDecimal.valueOf(2)),
                initItinerary(3, BigDecimal.valueOf(3)));
        when(response.getItineraryCo2FootprintResults()).thenReturn(itineraries);

        assertEquals(helper.getSearchKilosAverage(response).doubleValue(), 2D);
    }

    @Test
    public void testCalculatePercentageEco() {
        verifyCalculatePercentageEco(20, 50, 60);
        verifyCalculatePercentageEco(5, 5, 0);
        verifyCalculatePercentageEco(100, 5, 0);
        verifyCalculatePercentageEco(55, 50, 0);
        verifyCalculatePercentageEco(30, 110, 72);
    }

    private ItineraryCo2FootprintResult initItinerary(Integer index, BigDecimal kilosCo2e) {
        ItineraryCo2FootprintResult itinerary = new ItineraryCo2FootprintResult();
        itinerary.setItineraryIndex(index);
        itinerary.setKilosCo2e(kilosCo2e);
        return itinerary;
    }

    private void verifyCalculatePercentageEco(double totalKilosItinerary, double averageKilosSearch, int calculatedPercentage) {
        assertEquals(helper.calculatePercentageEco(BigDecimal.valueOf(totalKilosItinerary), BigDecimal.valueOf(averageKilosSearch)), calculatedPercentage);
    }
}
