package com.odigeo.frontend.resolvers.search.handlers.vin;

import com.odigeo.frontend.resolvers.search.handlers.legend.SearchEngineContextBuilder.SearchEngineContext;
import com.odigeo.searchengine.v2.responses.itinerary.CrossFaringItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.HubItinerary;
import com.odigeo.searchengine.v2.responses.itinerary.ItineraryProvider;
import com.odigeo.searchengine.v2.responses.itinerary.Section;
import com.odigeo.searchengine.v2.responses.itinerary.SectionResult;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class VinFlightNumberHandlerTest {

    private static final Integer SIMPLE_ID = 0;
    private static final Integer CROSS_SIMPLE_IN_ID = 1;
    private static final Integer CROSS_SIMPLE_OUT_ID = 2;
    private static final Integer CROSS_FARING_ID = 3;

    private static final String SIMPLE_SECTION_ID = "simple";
    private static final String CROSS_SIMPLE_IN_SECTION_ID = "cross in";
    private static final String CROSS_SIMPLE_OUT_SECTION_ID = "cross out";

    @Mock
    private SearchEngineContext seContext;
    @Mock
    private HubItinerary hub;

    private VinFlightNumberHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new VinFlightNumberHandler();

        ItineraryProvider simple = mock(ItineraryProvider.class);
        ItineraryProvider crossSimpleIn = mock(ItineraryProvider.class);
        ItineraryProvider crossSimpleOut = mock(ItineraryProvider.class);

        Map<Integer, ItineraryProvider> simplesMap = Stream.of(
            new SimpleEntry<>(SIMPLE_ID, simple),
            new SimpleEntry<>(CROSS_SIMPLE_IN_ID, crossSimpleIn),
            new SimpleEntry<>(CROSS_SIMPLE_OUT_ID, crossSimpleOut)
        ).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));

        when(seContext.getSimplesMap()).thenReturn(simplesMap);

        CrossFaringItinerary crossFaring = mock(CrossFaringItinerary.class);
        when(crossFaring.getIn()).thenReturn(CROSS_SIMPLE_IN_ID);
        when(crossFaring.getOut()).thenReturn(CROSS_SIMPLE_OUT_ID);

        when(seContext.getCrossFaringMap())
            .thenReturn(Collections.singletonMap(CROSS_FARING_ID, crossFaring));

        Integer simpleSectionResultId = 0;
        Integer crossSimpleInSectionResultId = 2;
        Integer crossSimpleOutSectionResultId = 4;

        SectionResult simpleSectionResult = mock(SectionResult.class);
        SectionResult crossSimpleInSectionResult = mock(SectionResult.class);
        SectionResult crossSimpleOutSectionResult = mock(SectionResult.class);
        Section simpleSection = mock(Section.class);
        Section crossSimpleInSection = mock(Section.class);
        Section crossSimpleOutSection = mock(Section.class);

        when(simpleSectionResult.getSection()).thenReturn(simpleSection);
        when(crossSimpleInSectionResult.getSection()).thenReturn(crossSimpleInSection);
        when(crossSimpleOutSectionResult.getSection()).thenReturn(crossSimpleOutSection);

        when(simpleSection.getId()).thenReturn(SIMPLE_SECTION_ID);
        when(crossSimpleInSection.getId()).thenReturn(CROSS_SIMPLE_IN_SECTION_ID);
        when(crossSimpleOutSection.getId()).thenReturn(CROSS_SIMPLE_OUT_SECTION_ID);

        when(simple.getSections()).thenReturn(Collections.singletonList(simpleSectionResultId));
        when(crossSimpleIn.getSections()).thenReturn(Collections.singletonList(crossSimpleInSectionResultId));
        when(crossSimpleOut.getSections()).thenReturn(Collections.singletonList(crossSimpleOutSectionResultId));

        Map<Integer, SectionResult> sectionResultMap = Stream.of(
            new SimpleEntry<>(simpleSectionResultId, simpleSectionResult),
            new SimpleEntry<>(crossSimpleInSectionResultId, crossSimpleInSectionResult),
            new SimpleEntry<>(crossSimpleOutSectionResultId, crossSimpleOutSectionResult)
        ).collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));

        when(seContext.getSectionResultMap()).thenReturn(sectionResultMap);

        when(seContext.getHubs()).thenReturn(Collections.singletonList(hub));
    }

    @Test
    public void testFindOriginHubFlightNumbers() {
        when(hub.getOth()).thenReturn(SIMPLE_ID);
        Set<String> flights = handler.findOriginHubFlightNumbers(seContext);

        assertEquals(flights.size(), 1);
        assertTrue(flights.contains(SIMPLE_SECTION_ID));
    }

    @Test
    public void testFindOriginHubFlightNumbersCross() {
        when(hub.getOth()).thenReturn(CROSS_FARING_ID);
        Set<String> flights = handler.findOriginHubFlightNumbers(seContext);

        assertEquals(flights.size(), 2);
        assertTrue(flights.contains(CROSS_SIMPLE_IN_SECTION_ID));
        assertTrue(flights.contains(CROSS_SIMPLE_OUT_SECTION_ID));
    }

    @Test
    public void testFindDestinationHubFlightNumbers() {
        when(hub.getHtd()).thenReturn(SIMPLE_ID);
        Set<String> flights = handler.findDestinationHubFlightNumbers(seContext);

        assertEquals(flights.size(), 1);
        assertTrue(flights.contains(SIMPLE_SECTION_ID));
    }

    @Test
    public void testFindDestinationHubFlightNumbersCross() {
        when(hub.getHtd()).thenReturn(CROSS_FARING_ID);
        Set<String> flights = handler.findDestinationHubFlightNumbers(seContext);

        assertEquals(flights.size(), 2);
        assertTrue(flights.contains(CROSS_SIMPLE_IN_SECTION_ID));
        assertTrue(flights.contains(CROSS_SIMPLE_OUT_SECTION_ID));
    }

}
