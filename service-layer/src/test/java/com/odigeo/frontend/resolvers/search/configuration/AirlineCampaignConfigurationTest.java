package com.odigeo.frontend.resolvers.search.configuration;

import bean.test.BeanTest;
import org.apache.commons.lang.RandomStringUtils;

import java.util.Collections;

public class AirlineCampaignConfigurationTest extends BeanTest<AirlineCampaignConfiguration> {

    @Override
    protected AirlineCampaignConfiguration getBean() {
        AirlineCampaignConfiguration bean = new AirlineCampaignConfiguration();
        bean.setAirlines(Collections.singletonList(RandomStringUtils.random(10)));
        return bean;
    }
}
