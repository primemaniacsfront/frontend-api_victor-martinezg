package com.odigeo.frontend.resolvers.accommodation.summary.models;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationReview;

import java.util.Collections;

public class AccommodationSummaryResponseDTOTest extends BeanTest<AccommodationSummaryResponseDTO> {

    public AccommodationSummaryResponseDTO getBean() {
        AccommodationSummaryResponseDTO bean = new AccommodationSummaryResponseDTO();
        bean.setAccommodationDetail(new AccommodationDetailDTO());
        bean.setRoomDeals(Collections.EMPTY_LIST);
        bean.setAccommodationReview(new AccommodationReview());
        
        return bean;
    }
}
