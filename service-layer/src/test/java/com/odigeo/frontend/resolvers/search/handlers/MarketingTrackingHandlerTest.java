package com.odigeo.frontend.resolvers.search.handlers;

import com.odigeo.frontend.commons.SiteCookies;
import com.odigeo.frontend.commons.context.RequestInfo;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.searchengine.v2.searchresults.criteria.marketing.tracking.MarketingTrackingInfo;
import javax.servlet.http.Cookie;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MarketingTrackingHandlerTest {

    private static final String DEFAULT_GOOGLE_UTM_SOURCE_VALUE = "google";
    private static final String DEFAULT_GOOGLE_UTM_MEDIUM_VALUE = "cpc";
    private static final String DEFAULT_UTM_NO_GA = "no_GA";

    private static final String SOME_KIND_OF_COOKIE_VALUE = "Some kind of cookie value";
    private static final String SOME_KIND_OF_COOKIE_ENCODED = "Some%20kind%20of%20cookie%20value%20encoded";
    private static final int NON_EXPIRED_COOKIE_AGE = -1;
    private static final int EXPIRED_COOKIE_AGE = 5;

    @Mock
    private ResolverContext context;
    @Mock
    private Cookie cookie;
    @Mock
    private VisitInformation visit;
    @Mock
    private RequestInfo requestInfo;

    private MarketingTrackingHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        handler = new MarketingTrackingHandler();

        when(requestInfo.getCookie(SiteCookies.MKT_TRACKING)).thenReturn(cookie);
        when(context.getVisitInformation()).thenReturn(visit);
        when(context.getRequestInfo()).thenReturn(requestInfo);
    }

    @Test
    public void testCreateMktTrackingInfoNoMktTrack() {
        MarketingTrackingInfo trackingInfo = handler.createMktTrackingInfo(context);
        testEmptyTracking(trackingInfo);
    }

    @Test
    public void testCreateMktTrackingInfoNullMktTrack() {
        when(requestInfo.getCookie(SiteCookies.MKT_TRACKING)).thenReturn(null);

        MarketingTrackingInfo trackingInfo = handler.createMktTrackingInfo(context);
        testEmptyTracking(trackingInfo);
    }

    @Test
    public void testCreateMktTrackingInfoWithMktTrack() {
        setupCookie("1.1400858380.37.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)", NON_EXPIRED_COOKIE_AGE);

        MarketingTrackingInfo trackingInfo = handler.createMktTrackingInfo(context);

        assertEquals(trackingInfo.getUtmSource(), "(direct)");
        assertEquals(trackingInfo.getUtmCampaign(), "(direct)");
    }

    @Test
    public void testCreateMktTrackingInfoWithMktTrackEncoded() {
        setupCookie("87361868.1482855139.106.65.utmcsr=bing|utmccn=(organic)|utmcmd=organic"
                + "|utmctr=vuelos%20barcelona%20par%C3%ADs%20baratos", NON_EXPIRED_COOKIE_AGE);

        MarketingTrackingInfo trackingInfo = handler.createMktTrackingInfo(context);

        assertNull(trackingInfo.getUtmClickId());
        assertEquals(trackingInfo.getUtmSource(), "bing");
        assertEquals(trackingInfo.getUtmCampaign(), "(organic)");
        assertEquals(trackingInfo.getUtmMedium(), "organic");
        assertEquals(trackingInfo.getUtmTerm(), "vuelos barcelona parís baratos");
        assertNull(trackingInfo.getUtmContent());
    }

    @Test
    public void testCreateMktTrackingInfoWithMktTrackWithGclid() {
        setupCookie("87361868.1482916662.107.66.utmgclid=CNm4wITHltECFUieGwodWk4GNQ"
                + "|utmccn=(not%20set)|utmcmd=(not%20set)", NON_EXPIRED_COOKIE_AGE);

        MarketingTrackingInfo trackingInfo = handler.createMktTrackingInfo(context);

        assertEquals(trackingInfo.getUtmClickId(), "CNm4wITHltECFUieGwodWk4GNQ");
        assertEquals(trackingInfo.getUtmSource(), DEFAULT_GOOGLE_UTM_SOURCE_VALUE);
        assertEquals(trackingInfo.getUtmMedium(), DEFAULT_GOOGLE_UTM_MEDIUM_VALUE);
        assertEquals(trackingInfo.getUtmCampaign(), "(not set)");
        assertNull(trackingInfo.getUtmTerm());
        assertNull(trackingInfo.getUtmContent());
    }

    @Test
    public void testCreateMktTrackingInfoWithMktTrackExpired() {
        setupCookie(SOME_KIND_OF_COOKIE_VALUE, EXPIRED_COOKIE_AGE);

        MarketingTrackingInfo trackingInfo = handler.createMktTrackingInfo(context);

        assertEquals(trackingInfo.getUtmSource(), DEFAULT_UTM_NO_GA);
        assertEquals(trackingInfo.getUtmCampaign(), StringUtils.EMPTY);
    }

    @Test
    public void testGetMktTrackCookieValue() {
        testGetMktTrackCookie(SOME_KIND_OF_COOKIE_VALUE);
    }

    @Test
    public void testGetMktTrackCookieEncoded() {
        testGetMktTrackCookie(SOME_KIND_OF_COOKIE_ENCODED);
    }

    @Test
    public void testGetMktTrackMarketingPortal() {
        String code = "code";

        setupCookie(SOME_KIND_OF_COOKIE_VALUE, NON_EXPIRED_COOKIE_AGE);
        when(visit.getMktPortal()).thenReturn(code);

        MarketingTrackingInfo trackingInfo = handler.createMktTrackingInfo(context);
        assertEquals(trackingInfo.getMarketingPortal(), code);
    }

    private void setupCookie(String value, int maxAge) {
        when(cookie.getValue()).thenReturn(value);
        when(cookie.getMaxAge()).thenReturn(maxAge);
    }

    private void testEmptyTracking(MarketingTrackingInfo trackingInfo) {
        assertEquals(trackingInfo.getUtmSource(), DEFAULT_UTM_NO_GA);
        assertEquals(trackingInfo.getUtmMedium(), DEFAULT_UTM_NO_GA);
    }

    private void testGetMktTrackCookie(String cookieValue) {
        setupCookie(cookieValue, NON_EXPIRED_COOKIE_AGE);

        MarketingTrackingInfo trackingInfo = handler.createMktTrackingInfo(context);
        assertEquals(trackingInfo.getMarketingTrackingCookie(), cookieValue);
    }

}
