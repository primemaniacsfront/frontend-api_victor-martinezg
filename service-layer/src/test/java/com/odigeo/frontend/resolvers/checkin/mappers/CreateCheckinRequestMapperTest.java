package com.odigeo.frontend.resolvers.checkin.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.CheckinRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odigeo.checkin.api.v1.model.*;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDate;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNull;

public class CreateCheckinRequestMapperTest {

    private static final String PATH_TO_JSON = "com/odigeo/frontend/resolvers/checkin/mapper/createCheckinRequest.json";


    @Test
    public void testMap() throws IOException {
        CheckinRequest request = createCheckinRequest(PATH_TO_JSON);
        AutomaticCheckIn automaticCheckIn = new CreateCheckinRequestMapper().map(request, "es_ES");

        assertEquals(automaticCheckIn.getBookingId(), 7729996062L);
        assertEquals(automaticCheckIn.getTravellers().size(), 1);
        Traveller traveller = automaticCheckIn.getTravellers().get(0);
        assertEquals(traveller.getNumPassenger(), "0");
        assertEquals(traveller.getGender(), Gender.MALE);
        assertEquals(traveller.getDateOfBirth(), LocalDate.parse("1990-12-11"));
        assertEquals(traveller.getMobilePhone(), "681487609");
        assertEquals(traveller.getEmail(), "john.doe@edreamsodigeo.com");
        assertEquals(traveller.getCitizenship(), "ES");
        assertEquals(traveller.getCountryOfResidence(), "ES");

        Contact contact = traveller.getContact();
        assertEquals(contact.getFullName(), "John Doe");
        assertEquals(contact.getPhone(), "681487609");
        assertEquals(contact.getRelation(), "PARENT");

        ArrivalDetails address = traveller.getArrivalDetails();
        assertEquals(address.getAddress(), "calle random 123");
        assertEquals(address.getCity(), "Malaga");
        assertEquals(address.getState(), "Andalucia");
        assertEquals(address.getZip(), "29618");

        TravelDocument document = traveller.getTravelDocument();
        assertEquals(document.getTravelDocumentType(), TravelDocumentType.NATIONAL_ID);
        assertEquals(document.getNumber(), "34457876Y");
        assertEquals(document.getIssued(), LocalDate.parse("2020-03-10"));
        assertEquals(document.getExpiration(), LocalDate.parse("2030-04-17"));
        assertEquals(document.getIssuingCountry(), "ES");

        TravelPermission permission = traveller.getTravelPermission();
        assertEquals(permission.getNoVisaReason(), NoVisaReason.VISA_NOT_NEEDED);
        assertNull(permission.getResidentPermit());
        assertNull(permission.getVisa());

        OnwardFlight flight = permission.getOnwardFlight();
        assertEquals(flight.getAirportDestination(), "MAD");
        assertEquals(flight.getDate(), Instant.parse("2020-10-20T22:00:00Z"));
        assertEquals(flight.getNumber(), "0123456789");

    }

    private CheckinRequest createCheckinRequest(String jsonFile) throws IOException {
        InputStream json = this.getClass().getClassLoader().getResourceAsStream(jsonFile);
        return new ObjectMapper().readValue(json, CheckinRequest.class);
    }
}
