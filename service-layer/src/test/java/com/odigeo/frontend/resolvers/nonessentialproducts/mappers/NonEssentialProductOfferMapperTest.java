package com.odigeo.frontend.resolvers.nonessentialproducts.mappers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProduct;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductOffer;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.NonEssentialProductsUIConfig;
import com.odigeo.dapi.client.Insurance;
import com.odigeo.dapi.client.InsuranceOffer;
import com.odigeo.frontend.commons.Logger;
import com.odigeo.frontend.commons.context.visit.VisitInformation;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.NonEssentialProductsConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryConfiguration;
import com.odigeo.frontend.resolvers.nonessentialproducts.configuration.beans.AncillaryPolicyConfiguration;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class NonEssentialProductOfferMapperTest {

    private static final String POLICY = "policy";
    private static final BigDecimal TOTAL_PRICE = BigDecimal.TEN;
    private static final String CURRENCY = "EUR";

    @Mock
    private NonEssentialProductsConfiguration nonEssentialProductsConfiguration;
    @Mock
    private NonEssentialProductsUIConfigMapper nonEssentialProductsUIConfigMapper;
    @Mock
    private Logger logger;
    @Mock
    private AncillaryPolicyConfiguration ancillaryPolicyConfiguration;
    @Mock
    private AncillaryConfiguration ancillaryConfiguration;
    @Mock
    private InsuranceOffer insuranceOffer;
    @Mock
    private VisitInformation visitInformation;
    @Mock
    private NonEssentialProductMapper productMapper;
    @InjectMocks
    private NonEssentialProductOfferMapper mapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        when(ancillaryPolicyConfiguration.get(POLICY)).thenReturn(ancillaryConfiguration);
    }

    @Test
    public void mapShouldReturnNullWithOutInsuranceOffer() {
        assertNull(mapper.map(null, visitInformation));
    }

    @Test
    public void mapShouldReturnNullWithNullInsurances() {
        when(insuranceOffer.getInsurances()).thenReturn(null);
        assertNull(mapper.map(insuranceOffer, visitInformation));
    }

    @Test
    public void mapShouldReturnNullWithoutInsurances() {
        when(insuranceOffer.getInsurances()).thenReturn(Collections.emptyList());
        assertNull(mapper.map(insuranceOffer, visitInformation));
    }

    @Test
    public void mapShouldReturnNullWhenGetConfigurationThrowsAnError() throws IOException {
        when(insuranceOffer.getInsurances()).thenReturn(Arrays.asList(new Insurance()));
        when(nonEssentialProductsConfiguration.getConfiguration(visitInformation)).thenThrow(new IOException());

        assertNull(mapper.map(insuranceOffer, visitInformation));
    }

    @Test
    public void mapShouldReturnNullWhenConfigurationHasNotThePolicy() throws IOException {
        Insurance insurance = new Insurance();
        when(insuranceOffer.getInsurances()).thenReturn(Arrays.asList(insurance));
        when(nonEssentialProductsConfiguration.getConfiguration(visitInformation)).thenReturn(new AncillaryPolicyConfiguration());

        assertNull(mapper.map(insuranceOffer, visitInformation));
    }

    @Test
    public void mapShouldReturnMappedData() throws IOException {
        List<Insurance> insuranceList = getInsuranceList();
        when(insuranceOffer.getInsurances()).thenReturn(insuranceList);
        when(insuranceOffer.getTotalPrice()).thenReturn(TOTAL_PRICE);
        when(visitInformation.getCurrency()).thenReturn(CURRENCY);

        when(nonEssentialProductsConfiguration.getConfiguration(visitInformation)).thenReturn(ancillaryPolicyConfiguration);
        NonEssentialProduct product = new NonEssentialProduct();
        NonEssentialProductsUIConfig nonEssentialProductsUIConfig = new NonEssentialProductsUIConfig();
        when(nonEssentialProductsUIConfigMapper.map(ancillaryConfiguration)).thenReturn(nonEssentialProductsUIConfig);
        when(ancillaryConfiguration.getMandatory()).thenReturn(Boolean.TRUE);

        when(productMapper.map(insuranceOffer, visitInformation)).thenReturn(product);

        NonEssentialProductOffer nonEssentialProductOffer = mapper.map(insuranceOffer, visitInformation);

        assertEquals(nonEssentialProductOffer.getProduct(), product);
        assertEquals(nonEssentialProductOffer.getUiConfig(), nonEssentialProductsUIConfig);
        assertEquals(nonEssentialProductOffer.getMandatory(), ancillaryConfiguration.getMandatory());
    }

    private List<Insurance> getInsuranceList() {
        Insurance insurance = new Insurance();
        insurance.setPolicy(POLICY);
        List<Insurance> insuranceList = Arrays.asList(insurance);
        return insuranceList;
    }

}
