package com.odigeo.frontend.resolvers.user.handlers;

import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Membership;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.UserTraveller;
import com.google.inject.Guice;
import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.user.mappers.TravellerMapper;
import com.odigeo.frontend.resolvers.user.mappers.TravellerMapperImpl;
import com.odigeo.frontend.services.UserDescriptionService;
import graphql.GraphQLContext;
import graphql.schema.DataFetchingEnvironment;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class UserTravellersHandlerTest {

    @Mock
    private UserDescriptionService userService;
    @Mock
    private MembershipHandler membershipHandler;
    @Mock
    private com.odigeo.userprofiles.api.v2.model.User user;
    @Mock
    private com.odigeo.userprofiles.api.v2.model.Traveller traveller;
    @Mock
    private DataFetchingEnvironment env;
    @Mock
    private ResolverContext context;
    @Mock
    private TravellerMapper mapper;
    @Mock
    private GraphQLContext graphQLContext;
    @Mock
    private Membership membership;
    @InjectMocks
    private UserTravellersHandler handler;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        mapper = new TravellerMapperImpl();

        Guice.createInjector(binder -> {
            binder.bind(TravellerMapper.class).toProvider(() -> mapper);
            binder.bind(UserDescriptionService.class).toProvider(() -> userService);
            binder.bind(MembershipHandler.class).toProvider(() -> membershipHandler);
        }).injectMembers(handler);
        String userEmail = "test@test.com";
        when(env.getGraphQlContext()).thenReturn(graphQLContext);
        when(env.getGraphQlContext().get("Membership")).thenReturn(membership);
        when(env.getContext()).thenReturn(context);
        when(userService.getUserByToken(context)).thenReturn(user);
        when(user.getEmail()).thenReturn(userEmail);
        when(membership.getName()).thenReturn("Name");
        when(membership.getLastNames()).thenReturn("Last");
    }

    @Test
    public void testGetTravellers() {
        long travellerId = 1L;
        String travellerEmail = "test@test.com";
        when(traveller.getId()).thenReturn(travellerId);
        when(traveller.getEmail()).thenReturn(travellerEmail);
        List<com.odigeo.userprofiles.api.v2.model.Traveller> travellers = Collections.singletonList(traveller);
        when(user.getTraveller()).thenReturn(travellers);
        List<UserTraveller> userTravellers = handler.getTravellers(env);
        assertEquals((Long) userTravellers.get(0).getId(), traveller.getId());
    }

    @Test
    public void testGetTravellersEmptyList() {
        when(user.getTraveller()).thenReturn(Collections.emptyList());
        List<UserTraveller> userTravellers = handler.getTravellers(env);
        assertEquals((Long) userTravellers.get(0).getId(), traveller.getId());
    }

    @Test
    public void testGetTravellersNoMatch() {
        long travellerId = 1L;
        String travellerEmail = "test@test2.com";
        when(traveller.getId()).thenReturn(travellerId);
        when(traveller.getEmail()).thenReturn(travellerEmail);
        List<com.odigeo.userprofiles.api.v2.model.Traveller> travellers = Collections.singletonList(traveller);
        when(user.getTraveller()).thenReturn(travellers);
        List<UserTraveller> userTravellers = handler.getTravellers(env);
        assertEquals((Long) userTravellers.get(0).getId(), traveller.getId());
    }

    @Test
    public void testOnlyOneTravellerIsPrimeOwner() {
        long travellerId = 1L;
        String travellerEmail = "test@test2.com";
        when(traveller.getId()).thenReturn(travellerId);
        when(traveller.getEmail()).thenReturn(travellerEmail);
        List<com.odigeo.userprofiles.api.v2.model.Traveller> travellers = Arrays.asList(traveller, traveller, traveller);
        when(user.getTraveller()).thenReturn(travellers);
        List<UserTraveller> userTravellers = handler.getTravellers(env);
        assertEquals(1, userTravellers.stream().filter(UserTraveller::getIsPrimeOwner).count());
    }
}
