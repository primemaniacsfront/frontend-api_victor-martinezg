package com.odigeo.frontend.resolvers.accommodation.search.mappers;

import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationCategoryMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.AccommodationImagesMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.mappers.ContentKeyMapper;
import com.odigeo.frontend.resolvers.accommodation.commons.model.AccommodationDealInformationDTO;
import com.odigeo.frontend.resolvers.accommodation.search.models.SearchAccommodationDTO;
import com.odigeo.hcsapi.v9.beans.HotelStarRating;
import com.odigeo.hcsapi.v9.beans.HotelSummary;
import com.odigeo.hcsapi.v9.beans.HotelSummaryResponse;
import com.odigeo.hcsapi.v9.beans.HotelSupplierKey;
import com.odigeo.hcsapi.v9.beans.HotelType;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationCategory.STARS_0;
import static com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.AccommodationType.HOTEL;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class HotelDetailResponseMapperTest {

    @Mock
    private AccommodationProviderKeyMapper accommodationProviderKeyMapper;
    @Mock
    private AccommodationImagesMapper accommodationImagesMapper;
    @Mock
    private AccommodationCategoryMapper accommodationCategoryMapper;
    @Mock
    private ContentKeyMapper contentKeyMapper;


    @InjectMocks
    private final HotelDetailResponseMapper testClass = new HotelDetailResponseMapper();

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(accommodationProviderKeyMapper.map(any(HotelSupplierKey.class))).thenCallRealMethod();
        when(accommodationCategoryMapper.mapStarToCategory(any(HotelStarRating.class))).thenCallRealMethod();
        when(contentKeyMapper.buildContentKey(anyString(), anyString())).thenCallRealMethod();
        when(contentKeyMapper.extractProviderKeyFromContentKey(anyString())).thenCallRealMethod();
    }

    @Test
    public void test() {
        List<SearchAccommodationDTO> accommodations = buildSearchAccommodationList();
        testClass.remapSummaryToAccommodation(buildHotelSummaryResponse(), accommodations);
        assertEquals(accommodations.size(), 1);
        SearchAccommodationDTO searchAccommodationDTO = accommodations.get(0);
        assertEquals(searchAccommodationDTO.getKey(), "0");
        AccommodationDealInformationDTO accommodationDeal = searchAccommodationDTO.getAccommodationDeal();
        assertEquals(accommodationDeal.getType(), HOTEL);
        assertEquals(accommodationDeal.getCategory(), STARS_0);
        assertEquals(accommodationDeal.getCoordinates().getLatitude(), BigDecimal.valueOf(1f));
        assertEquals(accommodationDeal.getCoordinates().getLongitude(), BigDecimal.valueOf(1f));
    }

    private HotelSummaryResponse buildHotelSummaryResponse() {
        HotelSummaryResponse hotelSummaryResponse = new HotelSummaryResponse();
        Map<HotelSupplierKey, HotelSummary> hotelSupplierKeyHotelSummaryMap = new HashMap<>();
        HotelSummary hotelSummary = getHotelSummary();
        hotelSupplierKeyHotelSummaryMap.put(new HotelSupplierKey("1", "BC"), hotelSummary);
        hotelSummaryResponse.setHotelSummary(hotelSupplierKeyHotelSummaryMap);
        return hotelSummaryResponse;
    }

    private HotelSummary getHotelSummary() {
        HotelSummary hotelSummary = new HotelSummary();
        hotelSummary.setName("HotelName");
        hotelSummary.setType(HotelType.HOTEL);
        hotelSummary.setStarRating(HotelStarRating.H0);
        hotelSummary.setLatitude(1f);
        hotelSummary.setLongitude(1f);
        return hotelSummary;
    }

    private List<SearchAccommodationDTO> buildSearchAccommodationList() {
        List<SearchAccommodationDTO> searchAccommodationDTOS = new ArrayList<>();
        SearchAccommodationDTO searchAccommodationDTO = new SearchAccommodationDTO();
        searchAccommodationDTO.setKey("0");
        searchAccommodationDTO.setContentKey("BC#1");
        AccommodationDealInformationDTO accommodationDealInformationDTO = new AccommodationDealInformationDTO();
        searchAccommodationDTO.setAccommodationDeal(accommodationDealInformationDTO);

        SearchAccommodationDTO searchAccommodationDTONotFound = new SearchAccommodationDTO();
        searchAccommodationDTONotFound.setKey("1");
        searchAccommodationDTONotFound.setContentKey("BC#2");

        searchAccommodationDTOS.add(searchAccommodationDTO);
        searchAccommodationDTOS.add(searchAccommodationDTONotFound);
        return searchAccommodationDTOS;
    }

}
