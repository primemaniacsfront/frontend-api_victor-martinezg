package com.odigeo.frontend.resolvers.search.configuration;

import bean.test.BeanTest;

import java.util.HashMap;

public class PrimeDayMarketsConfigurationTest extends BeanTest<PrimeDayMarketsConfiguration> {

    @Override
    protected PrimeDayMarketsConfiguration getBean() {
        PrimeDayMarketsConfiguration bean = new PrimeDayMarketsConfiguration();
        bean.setMarketsCarriers(new HashMap<>());
        return bean;
    }

}
