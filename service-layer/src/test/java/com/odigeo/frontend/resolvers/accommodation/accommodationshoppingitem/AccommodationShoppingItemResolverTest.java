package com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem;

import com.odigeo.frontend.commons.context.ResolverContext;
import com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.handlers.AccommodationShoppingItemHandler;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.RuntimeWiring;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.AccommodationShoppingItemResolver.ACCOMMODATION_ITEM_FIELD;
import static com.odigeo.frontend.resolvers.accommodation.accommodationshoppingitem.AccommodationShoppingItemResolver.SHOPPING_CART_TYPE;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class AccommodationShoppingItemResolverTest {

    @Mock
    private DataFetchingEnvironment environment;
    @Mock
    private AccommodationShoppingItemHandler handler;
    @Mock
    private ResolverContext resolverContext;
    @Mock
    private GraphQLContext graphQLContext;

    @InjectMocks
    private AccommodationShoppingItemResolver resolver;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.openMocks(this);
        when(environment.getContext()).thenReturn(resolverContext);
        when(environment.getGraphQlContext()).thenReturn(graphQLContext);
    }


    @Test
    public void testAddToBuilder() {
        RuntimeWiring.Builder resolverBuilder = RuntimeWiring.newRuntimeWiring();
        resolver.addToBuilder(resolverBuilder);

        RuntimeWiring runtimeWiring = resolverBuilder.build();
        Map<String, DataFetcher> queries = runtimeWiring.getDataFetchers().get(SHOPPING_CART_TYPE);

        assertNotNull(queries.get(ACCOMMODATION_ITEM_FIELD));
    }

    @Test
    public void testAccommodationShoppingItemDataFetcher() {
        resolver.accommodationShoppingItemDataFetcher(environment);
        verify(handler).map(eq(graphQLContext));
    }
}
