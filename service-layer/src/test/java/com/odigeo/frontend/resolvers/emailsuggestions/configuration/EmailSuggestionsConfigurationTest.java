package com.odigeo.frontend.resolvers.emailsuggestions.configuration;

import bean.test.BeanTest;

import java.util.ArrayList;
import java.util.HashMap;

public class EmailSuggestionsConfigurationTest extends BeanTest<EmailSuggestionsConfiguration> {

    @Override
    protected EmailSuggestionsConfiguration getBean() {
        EmailSuggestionsConfiguration bean = new EmailSuggestionsConfiguration();
        bean.setSuggestEmailGenericDomains(new ArrayList<>());
        bean.setEmailProvidersAutocomplete(new HashMap<>());
        return bean;
    }

}
