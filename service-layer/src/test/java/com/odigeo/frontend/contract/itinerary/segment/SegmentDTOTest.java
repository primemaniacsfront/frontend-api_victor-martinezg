package com.odigeo.frontend.contract.itinerary.segment;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.BaggageCondition;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;

import java.util.ArrayList;
import java.util.HashSet;

import static org.mockito.Mockito.mock;

public class SegmentDTOTest extends BeanTest<SegmentDTO> {

    public SegmentDTO getBean() {
        SegmentDTO bean = new SegmentDTO();
        bean.setId(1);
        bean.setSections(new ArrayList<>(0));
        bean.setCarrier(mock(Carrier.class));
        bean.setBaggageCondition(mock(BaggageCondition.class));
        bean.setDuration(2L);
        bean.setSeats(2);
        bean.setTransportTypes(new HashSet<>(0));

        return bean;
    }
}
