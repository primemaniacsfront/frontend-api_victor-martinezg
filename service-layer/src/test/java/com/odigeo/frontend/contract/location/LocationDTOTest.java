package com.odigeo.frontend.contract.location;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.LocationType;

import static org.mockito.Mockito.mock;

public class LocationDTOTest extends BeanTest<LocationDTO> {

    public LocationDTO getBean() {
        LocationDTO bean = new LocationDTO();
        bean.setId(1);
        bean.setCityName("city");
        bean.setName("name");
        bean.setIata("iata");
        bean.setCityIata("cityIata");
        bean.setLocationType(mock(LocationType.class));
        bean.setCountryCode("country");
        bean.setCountryName("countryName");

        return bean;
    }
}
