package com.odigeo.frontend.contract.ancillaries;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.InsurancePolicy;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;

public class InsuranceOfferDTOTest extends BeanTest<InsuranceOfferDTO> {

    public InsuranceOfferDTO getBean() {
        InsuranceOfferDTO bean = new InsuranceOfferDTO();
        bean.setId(0);
        bean.setUrl("url");
        bean.setPolicy(mock(InsurancePolicy.class));

        return bean;
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(InsuranceOfferDTO.class)
            .suppress(Warning.NONFINAL_FIELDS)
            .withIgnoredFields("id")
            .usingGetClass()
            .verify();
    }

}
