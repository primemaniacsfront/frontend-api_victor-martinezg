package com.odigeo.frontend.contract.itinerary;

import bean.test.BeanTest;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.HashSet;

public class ItineraryDTOTest extends BeanTest<ItineraryDTO> {

    @Override
    protected ItineraryDTO getBean() {
        ItineraryDTO bean = new ItineraryDTO();
        bean.setFreeCancellation(ZonedDateTime.now());
        bean.setLegs(Collections.emptyList());
        bean.setTransportTypes(new HashSet<>(0));
        return bean;
    }

}
