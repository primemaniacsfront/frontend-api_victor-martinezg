package com.odigeo.frontend.contract.itinerary.leg;

import bean.test.BeanTest;

import java.util.Collections;

public class LegDTOTest extends BeanTest<LegDTO> {

    public LegDTO getBean() {
        LegDTO bean = new LegDTO();
        bean.setSegments(Collections.emptyList());
        bean.setSegmentKeys(Collections.emptyList());

        return bean;
    }
}
