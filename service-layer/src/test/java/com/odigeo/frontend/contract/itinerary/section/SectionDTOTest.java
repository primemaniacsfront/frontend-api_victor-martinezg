package com.odigeo.frontend.contract.itinerary.section;

import bean.test.BeanTest;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.Carrier;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TechnicalStop;
import com.edreamsodigeo.retail.frontendgraphql.contract.frontendapi.TransportType;
import com.odigeo.frontend.contract.ancillaries.InsuranceOfferDTO;
import com.odigeo.frontend.contract.location.LocationDTO;
import com.odigeo.searchengine.v2.common.CabinClass;

import java.time.ZonedDateTime;
import java.util.Collections;

import static org.mockito.Mockito.mock;

public class SectionDTOTest extends BeanTest<SectionDTO> {

    public SectionDTO getBean() {
        SectionDTO bean = new SectionDTO();
        bean.setId("id");
        bean.setKey("key");
        bean.setDepartureDate(mock(ZonedDateTime.class));
        bean.setArrivalDate(mock(ZonedDateTime.class));
        bean.setDeparture(mock(LocationDTO.class));
        bean.setDestination(mock(LocationDTO.class));
        bean.setCarrier(mock(Carrier.class));
        bean.setOperatingCarrier(mock(Carrier.class));
        bean.setCabinClass(mock(CabinClass.class));
        bean.setFlightCode("flightCode");
        bean.setArrivalTerminal("arrivalTerminal");
        bean.setDepartureTerminal("departureTerminal");
        bean.setTechnicalStops(Collections.singletonList(mock(TechnicalStop.class)));
        bean.setBaggageAllowance(2);
        bean.setVehicleModel("model");
        bean.setTransportType(mock(TransportType.class));
        bean.setInsuranceOffer(mock(InsuranceOfferDTO.class));
        bean.setDuration(Long.valueOf(120));
        return bean;
    }
}
